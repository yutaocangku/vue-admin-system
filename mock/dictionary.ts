import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'
const getEnableBooleanData = () => {
  return [
    { id: 1, pid: 0, label: '启用', value: true, sort: 1, status: true, valueType: 'boolean', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '禁用', value: false, sort: 2, status: true, valueType: 'boolean', tagColor: '', remark: '', icon: '' }
  ]
}
const getEnableNumberData = () => {
  return [
    { id: 1, pid: 0, label: '全部', value: 1, sort: 1, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '已启用', value: 2, sort: 2, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '已禁用', value: 3, sort: 3, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' }
  ]
}
const getDeviceData = () => {
  return [
    { id: 1, pid: 0, label: '桌面端', value: 'pc', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '移动端', value: 'wap', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '小程序', value: 'app', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getAppListData = () => {
  return [
    { id: 1, pid: 0, label: '微信', value: 'wechat', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '百度', value: 'baidu', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '头条', value: 'bytedance', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '支付宝', value: 'alipay', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 5, pid: 0, label: '钉钉', value: 'dingtalk', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 6, pid: 0, label: '安卓App', value: 'Android', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 7, pid: 0, label: '苹果App', value: 'IOS', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getModuleTypeData = () => {
  return [
    { id: 1, pid: 0, label: '基础模块', value: 'baseModule', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 1, label: '页头', value: 'header', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 3, pid: 1, label: '导航', value: 'navigation', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:organization-chart' },
    { id: 4, pid: 1, label: 'AD', value: 'ad', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:advertisement-line' },
    { id: 5, pid: 1, label: '标题', value: 'title', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:h-2' },
    { id: 6, pid: 1, label: '图文列表', value: 'card', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:gallery-line' },
    { id: 7, pid: 1, label: '文本列表', value: 'text', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:file-list-line' },
    { id: 8, pid: 1, label: '分类', value: 'category', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:stack-line' },
    { id: 9, pid: 1, label: '页脚', value: 'footer', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-bottom-line' },
    { id: 10, pid: 0, label: '高级模块', value: 'advancedModule', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 11, pid: 10, label: '地图', value: 'map', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 12, pid: 10, label: '表单', value: 'form', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 13, pid: 0, label: '容器', value: 'containerModule', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 14, pid: 13, label: '栅格', value: 'grid', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-masonry-line' },
    { id: 15, pid: 13, label: '版心套件', value: 'wrap', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 16, pid: 0, label: '自定义模块', value: 'customerModule', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 17, pid: 16, label: '简介', value: 'about', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 18, pid: 16, label: '服务', value: 'server', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' }
  ]
}
const getPageTypeData = () => {
  return [
    { id: 1, pid: 0, label: '首页', value: 'index', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '产品中心', value: 'productCenter', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '产品列表', value: 'productList', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '产品详情', value: 'productDetail', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 5, pid: 0, label: '新闻中心', value: 'newsCenter', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 6, pid: 0, label: '新闻列表', value: 'newsList', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 7, pid: 0, label: '新闻详情', value: 'newsDetail', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 8, pid: 0, label: '案例中心', value: 'solutionCenter', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 9, pid: 0, label: '案例列表', value: 'solutionList', sort: 9, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 10, pid: 0, label: '案例详情', value: 'solutionDetail', sort: 10, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 11, pid: 0, label: '关于我们', value: 'about', sort: 11, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 12, pid: 0, label: '服务支持', value: 'server', sort: 12, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 13, pid: 0, label: '联系我们', value: 'contactUs', sort: 13, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 14, pid: 0, label: '专题页', value: 'special', sort: 14, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 15, pid: 0, label: '通用页', value: 'universal', sort: 15, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 16, pid: 0, label: '页头', value: 'header', sort: 16, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 17, pid: 0, label: '页脚', value: 'footer', sort: 17, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getGenderData = () => {
  return [
    { id: 1, pid: 0, label: '男', value: 1, sort: 1, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '女', value: 2, sort: 2, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '保密', value: 3, sort: 3, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' }
  ]
}
const getSystemButtonData = () => {
  return [
    { id: 1, pid: 0, label: '新增', value: 'Create', sort: 1, status: true, valueType: 'text', tagColor: 'success', remark: '', icon: '' },
    { id: 2, pid: 0, label: '编辑', value: 'Update', sort: 2, status: true, valueType: 'text', tagColor: 'primary', remark: '', icon: '' },
    { id: 3, pid: 0, label: '删除', value: 'Delete', sort: 3, status: true, valueType: 'text', tagColor: 'danger', remark: '', icon: '' },
    { id: 4, pid: 0, label: '查看', value: 'View', sort: 4, status: true, valueType: 'text', tagColor: 'info', remark: '', icon: '' },
    { id: 5, pid: 0, label: '查询', value: 'Search', sort: 5, status: true, valueType: 'text', tagColor: 'primary', remark: '', icon: '' },
    { id: 6, pid: 0, label: '保存', value: 'Save', sort: 6, status: true, valueType: 'text', tagColor: 'success', remark: '', icon: '' },
    { id: 7, pid: 0, label: '导入', value: 'Import', sort: 7, status: true, valueType: 'text', tagColor: 'primary', remark: '', icon: '' },
    { id: 8, pid: 0, label: '导出', value: 'Export', sort: 8, status: true, valueType: 'text', tagColor: 'primary', remark: '', icon: '' },
    { id: 9, pid: 0, label: '上传', value: 'Upload', sort: 9, status: true, valueType: 'text', tagColor: 'primary', remark: '', icon: '' },
    { id: 10, pid: 0, label: '下载', value: 'Download', sort: 10, status: true, valueType: 'text', tagColor: 'primary', remark: '', icon: '' },
    { id: 11, pid: 0, label: '重置', value: 'Reset', sort: 11, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 12, pid: 0, label: '预览', value: 'Preview', sort: 12, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 13, pid: 0, label: '复制', value: 'Copy', sort: 13, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getWhetherBooleanData = () => {
  return [
    { id: 1, pid: 0, label: '是', value: true, sort: 1, status: true, valueType: 'boolean', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '否', value: false, sort: 2, status: true, valueType: 'boolean', tagColor: '', remark: '', icon: '' }
  ]
}
const getWhetherNumberData = () => {
  return [
    { id: 1, pid: 0, label: '全部', value: 1, sort: 1, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '是', value: 2, sort: 2, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '否', value: 3, sort: 3, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' }
  ]
}
const getPluginsData = () => {
  return [
    { id: 1, pid: 0, label: '桌面端', value: 'pc', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 1, label: 'jquery', value: 'jquery', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 1, label: 'superslide', value: 'superslide', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 1, label: 'swiper', value: 'swiper', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 5, pid: 1, label: 'slick', value: 'slick', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 6, pid: 1, label: 'lazyload', value: 'lazyload', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 7, pid: 1, label: 'gsap', value: 'gsap', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 8, pid: 1, label: 'ScrollTrigger', value: 'ScrollTrigger', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 9, pid: 0, label: '移动端', value: 'wap', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 10, pid: 9, label: 'jquery', value: 'jquery', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 11, pid: 9, label: 'superslide', value: 'superslide', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 12, pid: 9, label: 'swiper', value: 'swiper', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 13, pid: 9, label: 'slick', value: 'slick', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 14, pid: 9, label: 'lazyload', value: 'lazyload', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 15, pid: 9, label: 'gsap', value: 'gsap', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 16, pid: 9, label: 'ScrollTrigger', value: 'ScrollTrigger', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 17, pid: 0, label: '小程序', value: 'app', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 18, pid: 17, label: 'vue', value: 'vue', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 19, pid: 17, label: 'vite', value: 'vite', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 20, pid: 17, label: 'vue-router', value: 'vue-router', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 21, pid: 17, label: 'uniapp', value: 'uniapp', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getIndustryData = () => {
  return [
    { id: 1, pid: 0, label: '机械', value: 'machinery', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '教育', value: 'education', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '证券', value: 'securities', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '母婴', value: 'mother', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 5, pid: 0, label: '数码', value: 'digital', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 6, pid: 0, label: '服装', value: 'clothing', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 7, pid: 0, label: '装修', value: 'fitment', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 8, pid: 0, label: '宠物', value: 'pet', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 9, pid: 0, label: '通用', value: 'general', sort: 9, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getRequestTypeData = () => {
  return [
    { id: 1, pid: 0, label: 'POST', value: 'post', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: 'GET', value: 'get', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: 'PUT', value: 'put', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: 'DELETE', value: 'delete', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getPermissionsDataTypeData = () => {
  return [
    { id: 1, pid: 0, label: '本人数据', value: 1, sort: 1, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '本部门数据', value: 2, sort: 2, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '全部数据', value: 3, sort: 3, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '自定义数据', value: 4, sort: 4, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' }
  ]
}
const getHomeModuleTypeData = () => {
  return [
    { id: 1, pid: 0, label: '数据总览', value: 'DataShow', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '任务进度', value: 'TaskProgress', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '最新消息', value: 'News', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '快捷导航', value: 'QuickNavigation', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getComprehensiveFormFieldData = () => {
  return [
    { id: 1, pid: 0, label: '活动级别', value: 'level', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '活动类型', value: 'type', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '受众群体', value: 'pepole', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '活动风格', value: 'style', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}

const getCutsceneTypeData = () => {
  return [
    { id: 1, pid: 0, label: 'fade', value: 'fade', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '淡入淡出', icon: '' },
    { id: 2, pid: 0, label: 'fade-left', value: 'fade-left', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '左侧淡入淡出', icon: '' },
    { id: 3, pid: 0, label: 'fade-right', value: 'fade-right', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '右侧淡入淡出', icon: '' },
    { id: 4, pid: 0, label: 'fade-top', value: 'fade-top', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '顶部淡入淡出', icon: '' },
    { id: 5, pid: 0, label: 'fade-bottom', value: 'fade-bottom', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '底部淡入淡出', icon: '' },
    { id: 6, pid: 0, label: 'zoom-in', value: 'zoom-in', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '放大淡入淡出', icon: '' },
    { id: 7, pid: 0, label: 'zoom-out', value: 'zoom-out', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '缩小淡入淡出', icon: '' },
    { id: 8, pid: 0, label: 'zoom', value: 'zoom', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '缩放淡入淡出', icon: '' }
  ]
}
const getLanguageData = () => {
  return [
    { id: 1, pid: 0, label: '中文', value: 'zh-CN', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '英文', value: 'en', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '俄文', value: 'ru', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
}
const getDictionaryEntryData = (data) => {
  let result = []
  switch (data.type) {
    case 'enable_boolean':
      result = getEnableBooleanData()
      break
    case 'enable_number':
      result = getEnableNumberData()
      break
    case 'device':
      result = getDeviceData()
      break
    case 'app':
      result = getAppListData()
      break
    case 'moduleType':
      result = getModuleTypeData()
      break
    case 'pageType':
      result = getPageTypeData()
      break
    case 'gender':
      result = getGenderData()
      break
    case 'systemButton':
      result = getSystemButtonData()
      break
    case 'whether_boolean':
      result = getWhetherBooleanData()
      break
    case 'whether_number':
      result = getWhetherNumberData()
      break
    case 'plugins':
      result = getPluginsData()
      break
    case 'industry':
      result = getIndustryData()
      break
    case 'requestType':
      result = getRequestTypeData()
      break
    case 'permissionsDataType':
      result = getPermissionsDataTypeData()
      break
    case 'homeModuleType':
      result = getHomeModuleTypeData()
      break
    case 'comprehensiveFormField':
      result = getComprehensiveFormFieldData()
      break
    case 'cutsceneType':
      result = getCutsceneTypeData()
      break
    case 'language':
      result = getLanguageData()
      break
    default:
      for (let i = 0; i < 20; i++) {
        result.push({
          id: Random.id(),
          pid: 0,
          label: '@ctitle(5, 12)',
          value: Random.word(5, 12),
          remark: '@csentence()',
          sort: i,
          'status|1': true,
          valueType: 'text',
          tagColor: '',
          icon: '',
          author: '@cname()'
        })
      }
      break
  }
  return result
}
const dictionaryListMock = (data) => {
  const result: any[] = [
    {
      id: Random.id(),
      name: '启用/禁用-布尔类型',
      code: 'enable_boolean',
      sort: 1,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '启用/禁用-数字类型',
      code: 'enable_number',
      sort: 1,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '平台类型',
      code: 'device',
      sort: 2,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: 'app类型',
      code: 'app',
      sort: 3,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '模块类型',
      code: 'moduleType',
      sort: 4,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '页面类型',
      code: 'pageType',
      sort: 5,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '性别',
      code: 'gender',
      sort: 6,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '系统按钮',
      code: 'systemButton',
      sort: 7,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '是/否-布尔类型',
      code: 'whether_boolean',
      sort: 8,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '是/否-数字类型',
      code: 'whether_number',
      sort: 9,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '站点插件',
      code: 'plugins',
      sort: 10,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '行业',
      code: 'industry',
      sort: 11,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '请求类型',
      code: 'requestType',
      sort: 12,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '权限数据',
      code: 'permissionsDataType',
      sort: 13,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '首页模块类型',
      code: 'homeModuleType',
      sort: 14,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '综合表单字段',
      code: 'comprehensiveFormField',
      sort: 15,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '过场动画类型',
      code: 'cutsceneType',
      sort: 16,
      isEnable: true,
      remark: ''
    },
    {
      id: Random.id(),
      name: '语言',
      code: 'language',
      sort: 17,
      isEnable: true,
      remark: ''
    }
  ]
  for (let i = 0; i < data.pageSize - 18; i++) {
    result.push({
      id: Random.id(),
      name: '@ctitle(5, 12)',
      code: Random.word(5, 12),
      remark: '@csentence()',
      sort: 10 + i + 1,
      'isEnable|1': true,
      author: '@cname()'
    })
  }
  return result
}
const dictionaryEntryListMock = (data) => {
  const result: any[] = getDictionaryEntryData(data)
  return result
}
export default [
  {
    url: '/getDictionaryListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '字典数据获取成功！',
        data: dictionaryListMock(data.body),
        total: 400
      }
    }
  },
  {
    url: '/getDictionaryEntryListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '字典项数据获取成功！',
        data: dictionaryEntryListMock(data.body)
      }
    }
  }
] as MockMethod[]
