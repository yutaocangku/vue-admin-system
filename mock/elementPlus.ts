﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

const paginationListMock = (pageSize) => {
  const result: any[] = []
  for (let i = 0; i < pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle(5, 12)',
      description: '@csentence()',
      date: Random.datetime(),
      'percentage|10-100': 100,
      'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
      'img|1-115': 115,
      color: '@color()',
      author: '@cname()'
    })
  }
  return result
}

const infiniteScrollListMock = (pageSize, type) => {
  const result: any[] = []
  for (let i = 0; i < pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle(5, 12)',
      description: '@csentence()',
      date: Random.datetime(),
      'type|1': type.length > 0 ? type : ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
      'img|1-115': 115,
      author: '@cname()'
    })
  }
  return result
}
const articleMock = () => {
  const result: any[] = []
  for (let i = 0; i < 6; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle(5, 12)',
      description: '@csentence()',
      date: Random.datetime(),
      'type|1': ['beauty', 'cartoon', 'game'],
      'img|1-115': 115
    })
  }
  return result
}
export default [
  {
    url: '/getPaginationListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '列表数据获取成功！',
        data: paginationListMock(data.body.pageSize),
        total: 400
      }
    }
  },
  {
    url: '/getInfiniteScrollListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '列表数据获取成功！',
        data: infiniteScrollListMock(data.body.pageSize, data.body.type),
        total: 400
      }
    }
  },
  {
    url: '/getArticleData',
    method: 'post',
    response: () => {
      return {
        code: 200,
        status: true,
        info: '详情数据获取成功！',
        data: articleMock()
      }
    }
  }
] as MockMethod[]
