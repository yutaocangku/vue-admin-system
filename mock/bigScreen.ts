import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

/**
 * 生成多层tree结构的列表数据
 * @param template Mockjs数据模板
 * @param number {?number} 一级树结构的数量
 * @param max {?number} 最深层级数量
 * @param min {?number} 最浅层级数量
 */
function treeCallback(template, number = 5, max = 5, min = 0) {
  const treeNodeList: any = []
  let index = 0
  while (index < Math.round(Math.random() * number || 1)) {
    const len = Math.round(Math.random() * (max - min)) + min
    const tempTemplate = { ...template }
    if (len) {
      tempTemplate[`children|1-${Math.round(Math.random() * number)}`] = treeCallback(template, 1, len - 1)
    } else {
      tempTemplate['children'] = []
    }
    treeNodeList[index++] = tempTemplate
  }
  return treeNodeList
}

Random.extend({
  diyTreeNode: treeCallback
})

function bigScreenGroupMock(v) {
  return Random.diyTreeNode(
    {
      id: '@id',
      title: '@ctitle',
      parentId: '@id',
      'sort|10-500': 500
    },
    v.number
  )
}

function bigScreenListMock(v) {
  const result: any[] = []
  for (let i = 0; i < v.pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle',
      group: Random.id(),
      'status|1': true,
      'img|1-41': 41,
      sort: i + 1
    })
  }
  return result
}

export default [
  {
    url: '/getBigScreenGroup',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏分组数据获取成功！',
        data: bigScreenGroupMock(data.body)
      }
    }
  },
  {
    url: '/addBigScreenGroup',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏分组添加成功！'
      }
    }
  },
  {
    url: '/editBigScreenGroup',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏分组编辑成功！'
      }
    }
  },
  {
    url: '/removeBigScreenGroup',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏分组删除成功！'
      }
    }
  },
  {
    url: '/getBigScreenList',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏数据获取成功！',
        data: bigScreenListMock(data.body),
        total: 400
      }
    }
  },
  {
    url: '/addBigScreen',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏添加成功！'
      }
    }
  },
  {
    url: '/editBigScreen',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏编辑成功！'
      }
    }
  },
  {
    url: '/removeBigScreen',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏删除成功！'
      }
    }
  },
  {
    url: '/bigScreenInfo',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '大屏数据获取成功！'
      }
    }
  },
  {
    url: '/releaseBigScreen',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: data.body.status ? '大屏已取消发布！' : '大屏发布成功'
      }
    }
  }
] as MockMethod[]
