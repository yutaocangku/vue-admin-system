﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

const listMock = () => {
  const result: any[] = []
  for (let i = 0; i < 20; i++) {
    result.push({
      id: Random.id(),
      title: Random.cparagraph(1, 3),
      date: Random.datetime(),
      img: Random.image('200x100')
    })
  }
  return result
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}
export default [
  {
    url: '/getSeamlessScrollData',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: listMock()
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
