import { MockMethod } from 'vite-plugin-mock'
import { getMenuData, getMenuButtonList } from './public'

// 获取指定菜单的权限按钮
const getMenuPermissionsButtonData = (data) => {
  const result = getMenuButtonList()
  const resultList = []
  if (data.menuID === '') {
    result.forEach((item) => {
      resultList.push(item)
    })
  } else {
    result.forEach((item) => {
      if (item.menuID === data.menuID) {
        resultList.push(item)
      }
    })
  }
  return resultList
}

// 获取当前用户的权限菜单
const getMenuList = (data) => {
  const result = []
  const menuData = getMenuData()
  if (data.id === '2') {
    const menuIdList = [1, 5, 8, 9, 70]
    menuData.forEach((item) => {
      const index = menuIdList.findIndex((v) => v === item.id)
      if (index > 0) {
        result.push(item)
      }
    })
  } else if (data.id === '3') {
    const menuIdList = [2, 5, 8, 12, 70]
    menuData.forEach((item) => {
      const index = menuIdList.findIndex((v) => v === item.id)
      if (index > 0) {
        result.push(item)
      }
    })
  } else {
    menuData.forEach((item) => {
      result.push(item)
    })
  }
  return result
}

// 获取当前用户的所有权限
const getUserPermitList = (data) => {
  const result = []
  const menuData = getMenuData()
  if (data.id === '2') {
    const menuIdList = [1, 5, 8, 9, 70]
    menuData.forEach((item) => {
      if (item.permit && menuIdList.includes(item.id)) {
        result.push(item.permit)
      }
    })
    // 这里将前端管理的动态路由的权限标识返回给前端
    result.push('AsyncRouter')
  } else if (data.id === '3') {
    const menuIdList = [2, 5, 8, 12, 70]
    menuData.forEach((item) => {
      if (item.permit && menuIdList.includes(item.id)) {
        result.push(item.permit)
      }
    })
  } else {
    menuData.forEach((item) => {
      if (item.permit) {
        result.push(item.permit)
      }
    })
    // 这里将前端管理的动态路由的权限标识返回给前端
    result.push('AsyncRouter')
  }
  return result
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}

export default [
  {
    url: '/Api/getmenu',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '菜单数据获取成功！',
            data: getMenuList(data.body)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/Api/getpermit',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '用户权限钮数据获取成功！',
            data: getUserPermitList(data.body)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/Api/currentmenupermit',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '菜单按钮数据获取成功！',
            data: getMenuPermissionsButtonData(data.body)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
