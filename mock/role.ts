import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'
const roleListMock = (data) => {
  const result: any[] = [
    { id: '1', name: '超级管理员', code: 'admin', remark: '管理员专用角色', sort: 1, status: true, isAdmin: true, author: '@cname()' },
    { id: '2', name: '人事专员', code: 'hr', remark: '', sort: 2, status: true, isAdmin: false, author: '@cname()' },
    { id: '3', name: '后台维护', code: 'maintenance', remark: '', sort: 3, status: true, isAdmin: false, author: '@cname()' },
    { id: '4', name: '技术开发', code: 'developer', remark: '', sort: 4, status: true, isAdmin: false, author: '@cname()' },
    { id: '5', name: '销售人员', code: 'saler', remark: '', sort: 5, status: true, isAdmin: false, author: '@cname()' },
    { id: '6', name: '销售组长', code: 'salerLeader', remark: '', sort: 6, status: true, isAdmin: false, author: '@cname()' },
    { id: '7', name: '技术组长', code: 'developerLeader', remark: '', sort: 7, status: true, isAdmin: false, author: '@cname()' },
    { id: '8', name: '测试人员', code: 'QA', remark: '', sort: 8, status: true, isAdmin: false, author: '@cname()' }
  ]
  for (let i = 0; i < 12; i++) {
    result.push({
      id: Random.id(),
      name: '@ctitle(3, 8)',
      code: Random.word(5, 12),
      remark: '@csentence()',
      sort: i + 8 + 1,
      'status|1': true,
      'isAdmin|1': true,
      author: '@cname()'
    })
  }
  return result
}
export default [
  {
    url: '/getRoleListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '角色数据获取成功！',
        data: roleListMock(data.body)
      }
    }
  }
] as MockMethod[]
