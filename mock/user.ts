﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

const departmentList: any[] = [
  { id: 1, pid: 0, name: '总公司', sort: 1, status: true, remark: '' },
  { id: 2, pid: 1, name: '分公司一', sort: 1, status: true, remark: '' },
  { id: 3, pid: 1, name: '分公司二', sort: 2, status: true, remark: '' },
  { id: 4, pid: 1, name: '分公司三', sort: 3, status: true, remark: '' },
  { id: 5, pid: 2, name: '销售部', sort: 1, status: true, remark: '' },
  { id: 6, pid: 2, name: '技术部', sort: 2, status: true, remark: '' },
  { id: 7, pid: 2, name: '推广部', sort: 3, status: true, remark: '' },
  { id: 8, pid: 2, name: '运维组', sort: 4, status: true, remark: '' },
  { id: 9, pid: 5, name: '销售一组', sort: 1, status: true, remark: '' },
  { id: 10, pid: 5, name: '销售二组', sort: 2, status: true, remark: '' },
  { id: 11, pid: 5, name: '销售三组', sort: 3, status: true, remark: '' },
  { id: 12, pid: 6, name: '前端组', sort: 1, status: true, remark: '' },
  { id: 13, pid: 6, name: '程序组', sort: 2, status: true, remark: '' },
  { id: 14, pid: 7, name: '推广一组', sort: 1, status: true, remark: '' },
  { id: 15, pid: 7, name: '推广二组', sort: 2, status: true, remark: '' },
  { id: 16, pid: 3, name: '销售部', sort: 1, status: true, remark: '' },
  { id: 17, pid: 3, name: '研发部', sort: 2, status: true, remark: '' },
  { id: 18, pid: 3, name: '生产部', sort: 3, status: true, remark: '' },
  { id: 19, pid: 16, name: '销售一组', sort: 1, status: true, remark: '' },
  { id: 20, pid: 16, name: '销售二组', sort: 2, status: true, remark: '' },
  { id: 21, pid: 16, name: '销售三组', sort: 3, status: true, remark: '' },
  { id: 22, pid: 17, name: '研发一组', sort: 1, status: true, remark: '' },
  { id: 23, pid: 17, name: '研发二组', sort: 2, status: true, remark: '' },
  { id: 24, pid: 18, name: '生产一组', sort: 1, status: true, remark: '' },
  { id: 25, pid: 18, name: '生产二组', sort: 2, status: true, remark: '' },
  { id: 26, pid: 4, name: '销售部', sort: 1, status: true, remark: '' },
  { id: 27, pid: 4, name: '开发部', sort: 2, status: true, remark: '' },
  { id: 28, pid: 4, name: 'QA测试部', sort: 3, status: true, remark: '' },
  { id: 36, pid: 4, name: '实施部', sort: 4, status: true, remark: '' },
  { id: 29, pid: 26, name: '销售一组', sort: 1, status: true, remark: '' },
  { id: 30, pid: 26, name: '销售二组', sort: 2, status: true, remark: '' },
  { id: 31, pid: 26, name: '销售三组', sort: 3, status: true, remark: '' },
  { id: 32, pid: 27, name: '开发一组', sort: 1, status: true, remark: '' },
  { id: 33, pid: 27, name: '开发二组', sort: 2, status: true, remark: '' },
  { id: 34, pid: 28, name: '测试一组', sort: 1, status: true, remark: '' },
  { id: 35, pid: 28, name: '测试二组', sort: 2, status: true, remark: '' },
  { id: 37, pid: 36, name: '实施一组', sort: 1, status: true, remark: '' },
  { id: 38, pid: 36, name: '实施二组', sort: 2, status: true, remark: '' }
]

const roleList: any[] = [
  { id: '1', name: '超级管理员', code: 'admin', remark: '管理员专用角色', sort: 1, status: true, isAdmin: true },
  { id: '2', name: '人事专员', code: 'hr', remark: '', sort: 2, status: true, isAdmin: false },
  { id: '3', name: '后台维护', code: 'maintenance', remark: '', sort: 3, status: true, isAdmin: false },
  { id: '4', name: '技术开发', code: 'developer', remark: '', sort: 4, status: true, isAdmin: false },
  { id: '5', name: '销售人员', code: 'saler', remark: '', sort: 5, status: true, isAdmin: false },
  { id: '6', name: '销售组长', code: 'salerLeader', remark: '', sort: 6, status: true, isAdmin: false },
  { id: '7', name: '技术组长', code: 'developerLeader', remark: '', sort: 7, status: true, isAdmin: false },
  { id: '8', name: '测试人员', code: 'QA', remark: '', sort: 8, status: true, isAdmin: false }
]
// http://mockjs.com/examples.html#Object

const userListDataMock = (data) => {
  const result = [
    {
      id: '1',
      account: 'admin',
      password: '123456',
      userName: '超级管理员',
      avatar: 'https://img1.baidu.com/it/u=1919046953,770482211&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500',
      department: '总公司',
      departmentID: 1,
      role: ['超级管理员'],
      roleID: ['1'],
      phone: /1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}/,
      email: Random.email(),
      status: true,
      remark: '@csentence()'
    },
    {
      id: '2',
      account: 'test1',
      password: '123456',
      userName: '小红',
      avatar: 'https://img2.baidu.com/it/u=2353293941,2044590242&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500',
      department: '销售一组',
      departmentID: 9,
      role: ['人事专员'],
      roleID: ['2'],
      phone: /1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}/,
      email: Random.email(),
      status: true,
      remark: '@csentence()'
    },
    {
      id: '3',
      account: 'test2',
      password: '123456',
      userName: '小黑',
      avatar: 'https://img1.baidu.com/it/u=63401368,3081051561&fm=253&fmt=auto&app=138&f=JPEG?w=360&h=360',
      department: '研发二组',
      departmentID: 23,
      role: ['后台维护'],
      roleID: ['3'],
      phone: /1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}/,
      email: Random.email(),
      status: true,
      remark: '@csentence()'
    }
  ]
  for (let i = 0; i < data.pageSize - 4; i++) {
    const lengthNum = Random.integer(0, departmentList.length - 1)
    const booleanNum = Random.integer(1, 2)
    const roleNum = Random.integer(1, 3)
    const roleStrArr = []
    const roleIDArr = []
    for (let j = 0; j < roleNum; j++) {
      const currentRoleNum = Random.integer(0, 7)
      if (!roleStrArr.includes(roleList[currentRoleNum].name)) {
        roleStrArr.push(roleList[currentRoleNum].name)
        roleIDArr.push(roleList[currentRoleNum].id)
      }
    }
    result.push({
      id: Random.id(),
      account: Random.word(5, 12),
      password: '123456',
      userName: '@cname()',
      avatar: Random.word(5, 12),
      department: departmentList[lengthNum].name,
      departmentID: departmentList[lengthNum].id,
      role: roleStrArr,
      roleID: roleIDArr,
      phone: /1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}/,
      email: Random.email(),
      status: booleanNum === 1,
      remark: '@csentence()'
    })
  }
  return result
}

// 经过一系列操作生成了accessToken、refreshToken
const createdToken = () => {
  return {
    accessToken: 'abcdef',
    refreshToken: 'qwerty'
  }
}
// 更新token接口中接收到的前端返回的refreshToken,判断refreshToken是否有效，假定这里已获取到前端回传的refreshToken
const isTokenValid = () => {
  return true
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}
export default [
  {
    url: '/Login/login',
    method: 'post',
    response: (data) => {
      let isUser = false
      let isLogin = false
      let userInfo = {}
      const userData = userListDataMock(data)
      userData.forEach(function (item) {
        if (item.account === data.body.account) {
          isUser = true
          if (item.password === data.body.password) {
            isLogin = true
            userInfo = item
          }
        }
      })
      if (isUser) {
        if (isLogin) {
          const token = createdToken()
          return {
            code: 200,
            status: true,
            info: '登录成功！',
            data: userInfo,
            accessToken: token.accessToken,
            refreshToken: token.refreshToken
          }
        } else {
          return {
            code: 200,
            status: false,
            info: '密码错误！'
          }
        }
      } else {
        return {
          code: 200,
          status: false,
          info: '用户名不存在！'
        }
      }
    }
  },
  {
    url: '/refreshToken',
    method: 'post',
    response: () => {
      // 这里接收前端接口参数中回传的refreshToken，需要判断refreshToken是否也失效了，如果失效了，需要返回登录超时，如果未失效，则返回新的accessToken、expireTime、refreshToken
      if (isTokenValid()) {
        const token = createdToken()
        return {
          code: 200,
          status: true,
          info: 'token更新成功！',
          data: {
            accessToken: token.accessToken,
            refreshToken: token.refreshToken
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: {}
        }
      }
    }
  },
  {
    url: '/logout',
    method: 'post',
    response: () => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '退出登录成功！'
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/register',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '模拟注册成功！'
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/getCode',
    method: 'post',
    response: () => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '模拟获取验证码成功！'
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/recover',
    method: 'post',
    response: () => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: false,
            info: '本系统暂无法真实找回密码！'
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/getUserListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '用户数据获取成功！',
        data: userListDataMock(data.body),
        total: 400
      }
    }
  }
] as MockMethod[]
