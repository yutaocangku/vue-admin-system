﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

const listMock = (pageSize) => {
  const result: any[] = []
  for (let i = 0; i < pageSize; i++) {
    const tags = []
    const lengthNum = Random.integer(2, 5)
    for (let i = 0; i < lengthNum; i++) {
      tags.push(Random.cword(2, 6))
    }
    result.push({
      id: Random.id(),
      title: '@ctitle()',
      description: '@cparagraph()',
      date: Random.datetime(),
      'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
      'img|1-115': 115,
      tags: tags,
      'like|100-5000': 5000,
      'collect|10-500': 500,
      'message|10-500': 500,
      author: '@cname()'
    })
  }
  return result
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}
const getDetail = (id) => {
  const tags = []
  const lengthNum = Random.integer(2, 5)
  for (let i = 0; i < lengthNum; i++) {
    tags.push(Random.cword(2, 6))
  }
  return {
    id: id,
    title: '@ctitle()',
    description: '@cparagraph()',
    date: Random.datetime(),
    'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
    'img|1-115': 115,
    tags: tags,
    'like|100-5000': 5000,
    'collect|10-500': 500,
    'message|10-500': 500,
    author: '@cname()'
  }
}

const getDetailInfo = (obj) => {
  const tags = []
  const lengthNum = Random.integer(2, 5)
  for (let i = 0; i < lengthNum; i++) {
    tags.push(Random.cword(2, 6))
  }
  return {
    id: Random.id(),
    title: '@ctitle()',
    description: '@cparagraph()',
    date: Random.datetime(),
    'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
    'img|1-115': 115,
    tags: tags,
    'like|100-5000': 5000,
    'collect|10-500': 500,
    'message|10-500': 500,
    author: '@cname()',
    key1: obj && obj.key1 ? (obj.key1 === '' ? '参数为空' : obj.key1) : null,
    key2: obj && obj.key2 ? (obj.key2 === '' ? '参数为空' : obj.key2) : null
  }
}

const getRichTextDetail = (obj) => {
  const tags = []
  const lengthNum = Random.integer(2, 5)
  for (let i = 0; i < lengthNum; i++) {
    tags.push(Random.cword(2, 6))
  }
  return {
    id: Random.id(),
    title: '@ctitle()',
    description: '@cparagraph()',
    date: Random.datetime(),
    'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
    'img|1-115': 115,
    tags: tags,
    'like|100-5000': 5000,
    'collect|10-500': 500,
    'message|10-500': 500,
    author: '@cname()',
    key: obj && obj.key ? (obj.key === '' ? '参数为空' : obj.key) : null,
    content:
      '<p><a href="#a1">清风</a><a href="#a2">细雨</a><a href="#a3">独木桥</a><a href="#a4">凭栏映婀娜</a></p><p><a name="a1"></a>散尽离愁，携得清风出画楼。————晏几道《采桑子·高吟烂醉淮西月》</p><p><a name="a2"></a>海棠不惜胭脂色，独立蒙蒙细雨中。————陈与义《春寒》</p><p><a name="a3"></a>瘦藤忽梦寻梅去，袅袅寒溪独木桥。————程公许《除夕和唐人张继张佑即事四绝句·经世无才慕管萧》</p><p><a name="a4"></a>醉酒不知归，凭栏映婀娜。</p>'
  }
}
export default [
  {
    url: '/getList',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: listMock(data.body.pageSize),
            total: 400
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/getDetail',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getDetail(data.body.id)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/getDetailInfo',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getDetailInfo(data.body)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/getRichTextDetail',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getRichTextDetail(data.body)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
