import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}

const getLotteryResult = (count, column) => {
  if (column === 1) {
    const lengthNum = Random.integer(0, count)
    const result = {
      index: lengthNum
    }
    return result
  } else {
    const arr = []
    for (let i = 0; i < column; i++) {
      const lengthNum = Random.integer(0, count)
      arr.push(lengthNum)
    }
    const result = {
      index: arr
    }
    return result
  }
}
export default [
  {
    url: '/getLotteryResult',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getLotteryResult(data.body.count, data.body.column)
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
