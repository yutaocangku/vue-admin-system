import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'
const departmentListMock = (data) => {
  const result: any[] = [
    { id: 1, pid: 0, name: '总公司', sort: 1, status: true, remark: '' },
    { id: 2, pid: 1, name: '分公司一', sort: 1, status: true, remark: '' },
    { id: 3, pid: 1, name: '分公司二', sort: 2, status: true, remark: '' },
    { id: 4, pid: 1, name: '分公司三', sort: 3, status: true, remark: '' },
    { id: 5, pid: 2, name: '销售部', sort: 1, status: true, remark: '' },
    { id: 6, pid: 2, name: '技术部', sort: 2, status: true, remark: '' },
    { id: 7, pid: 2, name: '推广部', sort: 3, status: true, remark: '' },
    { id: 8, pid: 2, name: '运维组', sort: 4, status: true, remark: '' },
    { id: 9, pid: 5, name: '销售一组', sort: 1, status: true, remark: '' },
    { id: 10, pid: 5, name: '销售二组', sort: 2, status: true, remark: '' },
    { id: 11, pid: 5, name: '销售三组', sort: 3, status: true, remark: '' },
    { id: 12, pid: 6, name: '前端组', sort: 1, status: true, remark: '' },
    { id: 13, pid: 6, name: '程序组', sort: 2, status: true, remark: '' },
    { id: 14, pid: 7, name: '推广一组', sort: 1, status: true, remark: '' },
    { id: 15, pid: 7, name: '推广二组', sort: 2, status: true, remark: '' },
    { id: 16, pid: 3, name: '销售部', sort: 1, status: true, remark: '' },
    { id: 17, pid: 3, name: '研发部', sort: 2, status: true, remark: '' },
    { id: 18, pid: 3, name: '生产部', sort: 3, status: true, remark: '' },
    { id: 19, pid: 16, name: '销售一组', sort: 1, status: true, remark: '' },
    { id: 20, pid: 16, name: '销售二组', sort: 2, status: true, remark: '' },
    { id: 21, pid: 16, name: '销售三组', sort: 3, status: true, remark: '' },
    { id: 22, pid: 17, name: '研发一组', sort: 1, status: true, remark: '' },
    { id: 23, pid: 17, name: '研发二组', sort: 2, status: true, remark: '' },
    { id: 24, pid: 18, name: '生产一组', sort: 1, status: true, remark: '' },
    { id: 25, pid: 18, name: '生产二组', sort: 2, status: true, remark: '' },
    { id: 26, pid: 4, name: '销售部', sort: 1, status: true, remark: '' },
    { id: 27, pid: 4, name: '开发部', sort: 2, status: true, remark: '' },
    { id: 28, pid: 4, name: 'QA测试部', sort: 3, status: true, remark: '' },
    { id: 36, pid: 4, name: '实施部', sort: 4, status: true, remark: '' },
    { id: 29, pid: 26, name: '销售一组', sort: 1, status: true, remark: '' },
    { id: 30, pid: 26, name: '销售二组', sort: 2, status: true, remark: '' },
    { id: 31, pid: 26, name: '销售三组', sort: 3, status: true, remark: '' },
    { id: 32, pid: 27, name: '开发一组', sort: 1, status: true, remark: '' },
    { id: 33, pid: 27, name: '开发二组', sort: 2, status: true, remark: '' },
    { id: 34, pid: 28, name: '测试一组', sort: 1, status: true, remark: '' },
    { id: 35, pid: 28, name: '测试二组', sort: 2, status: true, remark: '' },
    { id: 37, pid: 36, name: '实施一组', sort: 1, status: true, remark: '' },
    { id: 38, pid: 36, name: '实施二组', sort: 2, status: true, remark: '' }
  ]
  return result
}
export default [
  {
    url: '/getDepartmentListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '部门数据获取成功！',
        data: departmentListMock(data.body)
      }
    }
  }
] as MockMethod[]
