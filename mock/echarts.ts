﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'
const worldCountryData = [
  { name: 'Somalia', country: '索马里', value: 0 },
  { name: 'Liechtenstein', country: '列支敦士登', value: 0 },
  { name: 'Morocco', country: '摩洛哥', value: 0 },
  { name: 'W. Sahara', country: '西撒哈拉', value: 0 },
  { name: 'Serbia', country: '塞尔维亚共和国', value: 0 },
  { name: 'Afghanistan', country: '阿富汗', value: 0 },
  { name: 'Angola', country: '安哥拉', value: 0 },
  { name: 'Albania', country: '阿尔巴尼亚', value: 0 },
  { name: 'Aland', country: '奥兰群岛', value: 0 },
  { name: 'Andorra', country: '安道尔共和国', value: 0 },
  { name: 'United Arab Emirates', country: '阿拉伯联合酋长国', value: 0 },
  { name: 'Argentina', country: '阿根廷', value: 0 },
  { name: 'Armenia', country: '亚美尼亚', value: 0 },
  { name: 'American Samoa', country: '美属萨摩亚群岛', value: 0 },
  { name: 'Fr. S. Antarctic Lands', country: '南极洲', value: 0 },
  { name: 'Antigua and Barb.', country: '安提瓜和巴布达', value: 0 },
  { name: 'Australia', country: '澳大利亚', value: 0 },
  { name: 'Austria', country: '奥地利', value: 0 },
  { name: 'Azerbaijan', country: '阿塞拜疆', value: 0 },
  { name: 'Burundi', country: '布隆迪', value: 0 },
  { name: 'Belgium', country: '比利时', value: 0 },
  { name: 'Benin', country: '贝宁', value: 0 },
  { name: 'Burkina Faso', country: '布基纳法索', value: 0 },
  { name: 'Bangladesh', country: '孟加拉国', value: 0 },
  { name: 'Bulgaria', country: '保加利亚', value: 0 },
  { name: 'Bahrain', country: '巴林', value: 0 },
  { name: 'Bahamas', country: '巴哈马', value: 0 },
  { name: 'Bosnia and Herz.', country: '波斯尼亚和黑塞哥维那', value: 0 },
  { name: 'Belarus', country: '白俄罗斯', value: 0 },
  { name: 'Belize', country: '伯利兹', value: 0 },
  { name: 'Bermuda', country: '百慕大', value: 0 },
  { name: 'Bolivia', country: '玻利维亚', value: 0 },
  { name: 'Brazil', country: '巴西', value: 0 },
  { name: 'Barbados', country: '巴巴多斯', value: 0 },
  { name: 'Brunei', country: '文莱', value: 0 },
  { name: 'Bhutan', country: '不丹', value: 0 },
  { name: 'Botswana', country: '博茨瓦纳', value: 0 },
  { name: 'Central African Rep.', country: '中非共和国', value: 0 },
  { name: 'Canada', country: '加拿大', value: 0 },
  { name: 'Switzerland', country: '瑞士', value: 0 },
  { name: 'Chile', country: '智利', value: 0 },
  { name: 'China', country: '中国', value: 0 },
  { name: "Côte d'Ivoire", country: '科特迪瓦', value: 0 },
  { name: 'Cameroon', country: '喀麦隆', value: 0 },
  { name: 'Dem. Rep. Congo', country: '刚果民主共和国', value: 0 },
  { name: 'Congo', country: '刚果共', value: 0 },
  { name: 'Colombia', country: '哥伦比亚', value: 0 },
  { name: 'Comoros', country: '科摩罗', value: 0 },
  { name: 'Cape Verde', country: '佛得角', value: 0 },
  { name: 'Costa Rica', country: '哥斯达黎加', value: 0 },
  { name: 'Cuba', country: '古巴', value: 0 },
  { name: 'Curaçao', country: '库拉索岛', value: 0 },
  { name: 'Cayman Is.', country: '开曼群岛', value: 0 },
  { name: 'N. Cyprus', country: '北塞浦路斯', value: 0 },
  { name: 'Cyprus', country: '塞浦路斯', value: 0 },
  { name: 'Czech Rep.', country: '捷克共和国', value: 0 },
  { name: 'Germany', country: '德国', value: 0 },
  { name: 'Djibouti', country: '吉布提', value: 0 },
  { name: 'Dominica', country: '多米尼克', value: 0 },
  { name: 'Denmark', country: '丹麦', value: 0 },
  { name: 'Dominican Rep.', country: '多米尼加共和国', value: 0 },
  { name: 'Algeria', country: '阿尔及利亚', value: 0 },
  { name: 'Ecuador', country: '厄瓜多尔', value: 0 },
  { name: 'Egypt', country: '埃及', value: 0 },
  { name: 'Eritrea', country: '厄立特里亚', value: 0 },
  { name: 'Spain', country: '西班牙', value: 0 },
  { name: 'Estonia', country: '爱沙尼亚', value: 0 },
  { name: 'Ethiopia', country: '埃塞俄比亚', value: 0 },
  { name: 'Finland', country: '芬兰', value: 0 },
  { name: 'Fiji', country: '斐济', value: 0 },
  { name: 'Falkland Is.', country: '福克兰', value: 0 },
  { name: 'France', country: '法国', value: 0 },
  { name: 'Faeroe Is.', country: '法罗群岛', value: 0 },
  { name: 'Micronesia', country: '密克罗尼西亚', value: 0 },
  { name: 'Gabon', country: '加蓬', value: 0 },
  { name: 'United Kingdom', country: '英国', value: 0 },
  { name: 'Georgia', country: '格鲁吉亚', value: 0 },
  { name: 'Ghana', country: '加纳', value: 0 },
  { name: 'Guinea', country: '几内亚', value: 0 },
  { name: 'Gambia', country: '冈比亚', value: 0 },
  { name: 'Guinea-Bissau', country: '几内亚比绍', value: 0 },
  { name: 'Eq. Guinea', country: '赤道几内亚', value: 0 },
  { name: 'Greece', country: '希腊', value: 0 },
  { name: 'Grenada', country: '格林纳达', value: 0 },
  { name: 'Greenland', country: '格陵兰', value: 0 },
  { name: 'Guatemala', country: '危地马拉', value: 0 },
  { name: 'Guam', country: '关岛', value: 0 },
  { name: 'Guyana', country: '圭亚那', value: 0 },
  { name: 'Heard I. and McDonald Is.', country: '赫德岛和麦克唐纳群岛', value: 0 },
  { name: 'Honduras', country: '洪都拉斯', value: 0 },
  { name: 'Croatia', country: '克罗地亚', value: 0 },
  { name: 'Haiti', country: '海地', value: 0 },
  { name: 'Hungary', country: '匈牙利', value: 0 },
  { name: 'Indonesia', country: '印度尼西亚', value: 0 },
  { name: 'Isle of Man', country: '曼岛', value: 0 },
  { name: 'India', country: '印度', value: 0 },
  { name: 'Br. Indian Ocean Ter.', country: '英属印度洋领土', value: 0 },
  { name: 'Ireland', country: '爱尔兰', value: 0 },
  { name: 'Iran', country: '伊朗', value: 0 },
  { name: 'Iraq', country: '伊拉克', value: 0 },
  { name: 'Iceland', country: '冰岛', value: 0 },
  { name: 'Israel', country: '以色列', value: 0 },
  { name: 'Italy', country: '意大利', value: 0 },
  { name: 'Jamaica', country: '牙买加', value: 0 },
  { name: 'Jersey', country: '泽西岛', value: 0 },
  { name: 'Jordan', country: '约旦', value: 0 },
  { name: 'Japan', country: '日本', value: 0 },
  { name: 'Siachen Glacier', country: '锡亚琴冰川', value: 0 },
  { name: 'Kazakhstan', country: '哈萨克斯坦', value: 0 },
  { name: 'Kenya', country: '肯尼亚', value: 0 },
  { name: 'Kyrgyzstan', country: '吉尔吉斯斯坦', value: 0 },
  { name: 'Cambodia', country: '柬埔寨', value: 0 },
  { name: 'Kiribati', country: '基里巴斯', value: 0 },
  { name: 'Korea', country: '韩国', value: 0 },
  { name: 'Kuwait', country: '科威特', value: 0 },
  { name: 'Lao PDR', country: '老挝', value: 0 },
  { name: 'Lebanon', country: '黎巴嫩', value: 0 },
  { name: 'Liberia', country: '利比里亚', value: 0 },
  { name: 'Libya', country: '利比亚', value: 0 },
  { name: 'Saint Lucia', country: '圣卢西亚岛', value: 0 },
  { name: 'Sri Lanka', country: '斯里兰卡', value: 0 },
  { name: 'Lesotho', country: '莱索托', value: 0 },
  { name: 'Lithuania', country: '立陶宛', value: 0 },
  { name: 'Luxembourg', country: '卢森堡', value: 0 },
  { name: 'Latvia', country: '拉脱维亚', value: 0 },
  { name: 'Moldova', country: '摩尔多瓦', value: 0 },
  { name: 'Madagascar', country: '马达加斯加', value: 0 },
  { name: 'Mexico', country: '墨西哥', value: 0 },
  { name: 'Macedonia', country: '马其顿', value: 0 },
  { name: 'Mali', country: '马里', value: 0 },
  { name: 'Malta', country: '马耳他', value: 0 },
  { name: 'Myanmar', country: '缅甸', value: 0 },
  { name: 'Montenegro', country: '黑山', value: 0 },
  { name: 'Mongolia', country: '蒙古', value: 0 },
  { name: 'N. Mariana Is.', country: '北马里亚纳群岛', value: 0 },
  { name: 'Mozambique', country: '莫桑比克', value: 0 },
  { name: 'Mauritania', country: '毛里塔尼亚', value: 0 },
  { name: 'Montserrat', country: '蒙特塞拉特', value: 0 },
  { name: 'Mauritius', country: '毛里求斯', value: 0 },
  { name: 'Malawi', country: '马拉维', value: 0 },
  { name: 'Malaysia', country: '马来西亚', value: 0 },
  { name: 'Namibia', country: '纳米比亚', value: 0 },
  { name: 'New Caledonia', country: '新喀里多尼亚', value: 0 },
  { name: 'Niger', country: '尼日尔', value: 0 },
  { name: 'Nigeria', country: '尼日利亚', value: 0 },
  { name: 'Nicaragua', country: '尼加拉瓜', value: 0 },
  { name: 'Niue', country: '纽埃岛', value: 0 },
  { name: 'Netherlands', country: '荷兰', value: 0 },
  { name: 'Norway', country: '挪威', value: 0 },
  { name: 'Nepal', country: '尼泊尔', value: 0 },
  { name: 'New Zealand', country: '新西兰', value: 0 },
  { name: 'Oman', country: '阿曼', value: 0 },
  { name: 'Pakistan', country: '巴基斯坦', value: 0 },
  { name: 'Panama', country: '巴拿马', value: 0 },
  { name: 'Peru', country: '秘鲁', value: 0 },
  { name: 'Philippines', country: '菲律宾', value: 0 },
  { name: 'Palau', country: '帕劳', value: 0 },
  { name: 'Papua New Guinea', country: '巴布亚新几内亚', value: 0 },
  { name: 'Poland', country: '波兰', value: 0 },
  { name: 'Puerto Rico', country: '波多黎各', value: 0 },
  { name: 'Dem. Rep. Korea', country: '朝鲜', value: 0 },
  { name: 'Portugal', country: '葡萄牙', value: 0 },
  { name: 'Paraguay', country: '巴拉圭', value: 0 },
  { name: 'Palestine', country: '巴勒斯坦', value: 0 },
  { name: 'Fr. Polynesia', country: '法属波利尼西亚', value: 0 },
  { name: 'Qatar', country: '卡塔尔', value: 0 },
  { name: 'Romania', country: '罗马尼亚', value: 0 },
  { name: 'Russia', country: '俄罗斯', value: 0 },
  { name: 'Rwanda', country: '卢旺达', value: 0 },
  { name: 'Saudi Arabia', country: '沙特阿拉伯', value: 0 },
  { name: 'Sudan', country: '苏丹', value: 0 },
  { name: 'S. Sudan', country: '南苏丹', value: 0 },
  { name: 'Senegal', country: '塞内加尔', value: 0 },
  { name: 'Singapore', country: '新加坡', value: 0 },
  { name: 'S. Geo. and S. Sandw. Is.', country: '南乔治亚岛和南桑威奇群岛', value: 0 },
  { name: 'Saint Helena', country: '圣赫勒拿岛', value: 0 },
  { name: 'Solomon Is.', country: '所罗门群岛', value: 0 },
  { name: 'Sierra Leone', country: '塞拉利昂', value: 0 },
  { name: 'El Salvador', country: '萨尔瓦多', value: 0 },
  { name: 'St. Pierre and Miquelon', country: '圣皮埃尔和密克隆群岛', value: 0 },
  { name: 'São Tomé and Principe', country: '圣多美和普林西比', value: 0 },
  { name: 'Suriname', country: '苏里南', value: 0 },
  { name: 'Slovakia', country: '斯洛伐克', value: 0 },
  { name: 'Slovenia', country: '斯洛文尼亚', value: 0 },
  { name: 'Sweden', country: '瑞典', value: 0 },
  { name: 'Swaziland', country: '斯威士兰', value: 0 },
  { name: 'Seychelles', country: '塞舌尔共和国', value: 0 },
  { name: 'Syria', country: '叙利亚', value: 0 },
  { name: 'Turks and Caicos Is.', country: '特克斯和凯科斯群岛', value: 0 },
  { name: 'Chad', country: '乍得', value: 0 },
  { name: 'Togo', country: '多哥', value: 0 },
  { name: 'Thailand', country: '泰国', value: 0 },
  { name: 'Tajikistan', country: '塔吉克斯坦', value: 0 },
  { name: 'Turkmenistan', country: '土库曼斯坦', value: 0 },
  { name: 'Timor-Leste', country: '东帝汶', value: 0 },
  { name: 'Tonga', country: '汤加', value: 0 },
  { name: 'Trinidad and Tobago', country: '特里尼达和多巴哥', value: 0 },
  { name: 'Tunisia', country: '突尼斯', value: 0 },
  { name: 'Turkey', country: '土耳其', value: 0 },
  { name: 'Tanzania', country: '坦桑尼亚', value: 0 },
  { name: 'Uganda', country: '乌干达', value: 0 },
  { name: 'Ukraine', country: '乌克兰', value: 0 },
  { name: 'Uruguay', country: '乌拉圭', value: 0 },
  { name: 'United States', country: '美国', value: 0 },
  { name: 'Uzbekistan', country: '乌兹别克斯坦', value: 0 },
  { name: 'St. Vin. and Gren.', country: '圣文森特和格林纳丁斯', value: 0 },
  { name: 'Venezuela', country: '委内瑞拉', value: 0 },
  { name: 'U.S. Virgin Is.', country: '美属维尔京群岛', value: 0 },
  { name: 'Vietnam', country: '越南', value: 0 },
  { name: 'Vanuatu', country: '瓦努阿图', value: 0 },
  { name: 'Samoa', country: '萨摩亚群岛', value: 0 },
  { name: 'Yemen', country: '也门', value: 0 },
  { name: 'South Africa', country: '南非', value: 0 },
  { name: 'Zambia', country: '赞比亚', value: 0 },
  { name: 'Zimbabwe', country: '津巴布韦', value: 0 }
]
const initChinaData = [
  { name: '台湾', value: 0 },
  { name: '河北', value: 0 },
  { name: '山西', value: 0 },
  { name: '内蒙古', value: 0 },
  { name: '辽宁', value: 0 },
  { name: '吉林', value: 0 },
  { name: '黑龙江', value: 0 },
  { name: '江苏', value: 0 },
  { name: '浙江', value: 0 },
  { name: '安徽', value: 0 },
  { name: '福建', value: 0 },
  { name: '江西', value: 0 },
  { name: '山东', value: 0 },
  { name: '河南', value: 0 },
  { name: '湖北', value: 0 },
  { name: '湖南', value: 0 },
  { name: '广东', value: 0 },
  { name: '广西', value: 0 },
  { name: '海南', value: 0 },
  { name: '四川', value: 0 },
  { name: '贵州', value: 0 },
  { name: '云南', value: 0 },
  { name: '西藏', value: 0 },
  { name: '陕西', value: 0 },
  { name: '甘肃', value: 0 },
  { name: '青海', value: 0 },
  { name: '宁夏', value: 0 },
  { name: '新疆', value: 0 },
  { name: '北京', value: 0 },
  { name: '天津', value: 0 },
  { name: '上海', value: 0 },
  { name: '重庆', value: 0 },
  { name: '香港', value: 0 },
  { name: '澳门', value: 0 }
]
const chartListMock = () => {
  const result = {
    oneLineData: [],
    oneColumnData: [],
    oneBarData: [],
    lineData: [],
    columnData: [],
    barData: [],
    groupStackData: [],
    pieData: [],
    worldData: [],
    chinaData: [],
    radar4Data: [],
    radar5Data: [],
    radar6Data: [],
    funnelData: [],
    gaugeProgressData: {},
    gaugeData: {},
    lineColumnData: []
  }
  const typeList = ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other']
  const inquiryList = ['搜索', '信息流']
  const searchList = ['百度', '360', '搜狗']
  const infoFlowList = ['百度', '头条', '抖音', '腾讯']
  const beautifyAttr = ['颜值', '身高', '性格', '嗓音', '形体', '气质']
  for (let j = 0; j < 12; j++) {
    result.oneLineData.push({
      id: Random.id(),
      date: j + 1 + '月',
      type: typeList[0],
      'num|1-115': 115
    })
  }
  for (let j = 0; j < 8; j++) {
    result.pieData.push({
      id: Random.id(),
      date: parseInt(Random.now('year', 'yyyy')),
      type: typeList[j],
      'num|1-115': 115
    })
  }
  for (let j = 0; j < 6; j++) {
    result.oneColumnData.push({
      id: Random.id(),
      date: j + 1 + '月',
      type: typeList[0],
      'num|1-115': 115
    })
  }
  for (let j = 0; j < 4; j++) {
    result.oneBarData.push({
      id: Random.id(),
      date: parseInt(Random.now('year', 'yyyy')),
      type: typeList[j],
      'num|1-115': 115
    })
  }
  const typeIndex = Random.integer(0, 6)
  for (let i = 0; i < 2; i++) {
    for (let j = 0; j < 12; j++) {
      result.lineData.push({
        id: Random.id(),
        date: j + 1 + '月',
        type: i === 0 ? typeList[typeIndex] : typeList[typeIndex + 1],
        'num|1-115': 115
      })
    }
    for (let j = 0; j < 6; j++) {
      result.columnData.push({
        id: Random.id(),
        date: j + 1 + '月',
        type: i === 0 ? typeList[typeIndex] : typeList[typeIndex + 1],
        'num|1-115': 115
      })
    }
    for (let j = 0; j < 4; j++) {
      result.barData.push({
        id: Random.id(),
        date: i === 0 ? parseInt(Random.now('year', 'yyyy')) - 1 + '' : Random.now('year', 'yyyy'),
        type: typeList[j],
        'num|1-115': 115
      })
    }
  }
  for (let k = 0; k < 2; k++) {
    if (k === 0) {
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 6; j++) {
          result.groupStackData.push({
            id: Random.id(),
            date: j + 1 + '月',
            class: inquiryList[k],
            type: searchList[i],
            'num|1-115': 115
          })
        }
      }
    } else {
      for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 6; j++) {
          result.groupStackData.push({
            id: Random.id(),
            date: j + 1 + '月',
            class: inquiryList[k],
            type: infoFlowList[i],
            'num|1-115': 115
          })
        }
      }
    }
  }
  worldCountryData.forEach((item) => {
    result.worldData.push({
      id: Random.id(),
      name: item.name,
      country: item.country,
      'num|1-1000': 1000
    })
  })
  initChinaData.forEach((item) => {
    result.chinaData.push({
      id: Random.id(),
      name: item.name,
      'num|1-1000': 1000
    })
  })
  for (let i = 0; i < 1; i++) {
    result.radar4Data.push({
      id: Random.id(),
      name: '女子',
      type: beautifyAttr[0],
      'num|1-100': 100
    })
    result.radar4Data.push({
      id: Random.id(),
      name: '女子',
      type: beautifyAttr[1],
      'num|1-100': 100
    })
    result.radar4Data.push({
      id: Random.id(),
      name: '女子',
      type: beautifyAttr[2],
      'num|1-100': 100
    })
    result.radar4Data.push({
      id: Random.id(),
      name: '女子',
      type: beautifyAttr[3],
      'num|1-100': 100
    })
  }
  for (let i = 0; i < 2; i++) {
    result.radar5Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[0],
      'num|1-100': 100
    })
    result.radar5Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[1],
      'num|1-100': 100
    })
    result.radar5Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[2],
      'num|1-100': 100
    })
    result.radar5Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[3],
      'num|1-100': 100
    })
    result.radar5Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[4],
      'num|1-100': 100
    })
  }
  for (let i = 0; i < 3; i++) {
    result.radar6Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[0],
      'num|1-100': 100
    })
    result.radar6Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[1],
      'num|1-100': 100
    })
    result.radar6Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[2],
      'num|1-100': 100
    })
    result.radar6Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[3],
      'num|1-100': 100
    })
    result.radar6Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[4],
      'num|1-100': 100
    })
    result.radar6Data.push({
      id: Random.id(),
      name: '女子' + (i + 1),
      type: beautifyAttr[5],
      'num|1-100': 100
    })
  }
  for (let i = 0; i < 1; i++) {
    result.funnelData.push({
      id: Random.id(),
      type: '展现',
      'num|300-500': 500
    })
    result.funnelData.push({
      id: Random.id(),
      type: '点击',
      'num|200-300': 300
    })
    result.funnelData.push({
      id: Random.id(),
      type: '访问',
      'num|100-200': 200
    })
    result.funnelData.push({
      id: Random.id(),
      type: '咨询',
      'num|50-100': 100
    })
    result.funnelData.push({
      id: Random.id(),
      type: '成交',
      'num|1-50': 50
    })
  }
  result.gaugeProgressData = {
    'num|0-100': 100,
    max: 100
  }
  result.gaugeData = {
    'num|0-240': 240,
    max: 240
  }
  for (let j = 0; j < 6; j++) {
    result.lineColumnData.push({
      id: Random.id(),
      date: j + 1 + '月',
      'num|100-1000': 1000,
      'percentage|1-100': 100
    })
  }
  return result
}

export default [
  {
    url: '/getChartListData',
    method: 'post',
    response: () => {
      return {
        code: 200,
        status: true,
        info: '列表数据获取成功！',
        data: chartListMock()
      }
    }
  }
] as MockMethod[]
