﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}

const getUploadData = () => {
  const result = {
    imageUpload: [],
    audioUpload: [],
    videoUpload: [],
    applicationUpload: [],
    fileUpload: []
  }
  const type = ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other']
  for (let j = 0; j < 2; j++) {
    const name = Random.integer(1, 115)
    result.imageUpload.push({
      id: Random.id(),
      name: name + '.jpg',
      'url': 'https://cdn.jsdelivr.net/gh/kaivin/images/list/' + type[Random.integer(0, 7)] + '/' + name + '.jpg'
    })
  }
  return result
}

export default [
  {
    url: '/uploadDataInFormData',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getUploadData()
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
