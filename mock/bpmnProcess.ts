import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

const listMock = (pageSize) => {
  const result: any[] = []
  for (let i = 0; i < pageSize; i++) {
    result.push({
      id: Random.id(),
      name: '@ctitle(5, 12)',
      'type|1': ['资产', '合同', '请假'],
      date: Random.datetime(),
      remark: '@csentence()',
      'status|1': true,
      author: '@cname()',
      sort: i + 1
    })
  }
  return result
}

const listSonMock = (pageSize) => {
  const result: any[] = []
  for (let i = 0; i < pageSize; i++) {
    result.push({
      id: Random.id(),
      name: '@ctitle(5, 12)',
      'type|1': ['资产', '合同', '请假'],
      'process|1': ['camunda', 'activiti', 'flowable'],
      date: Random.datetime(),
      remark: '@csentence()',
      'status|1': true,
      author: '@cname()',
      sort: i + 1
    })
  }
  return result
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}
export default [
  {
    url: '/getBpmnProcessListData',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: listMock(data.body.pageSize),
            total: 400
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  },
  {
    url: '/getBpmnProcessSonListData',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: listSonMock(data.body.pageSize),
            total: 400
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
