﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'
import { isString } from '@/utils/is'

const getDeviceData = (v) => {
  const deviceList = [
    { id: 1, pid: 0, label: '桌面端', value: 'pc', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '移动端', value: 'wap', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '小程序', value: 'app', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
  const result = []
  if (isString(v)) {
    deviceList.forEach((item) => {
      if (item.value === v) {
        result.push(item.label)
      }
    })
    return result
  } else {
    if (v.length === 0) {
      deviceList.forEach((item) => {
        result.push(item.label)
      })
    } else {
      deviceList.forEach((item) => {
        v.forEach((current) => {
          if (item.value === current) {
            result.push(item.label)
          }
        })
      })
    }
  }
  return result
}
const getModuleTypeData = (v) => {
  const moduleTypeListData = [
    { id: 1, pid: 0, label: '基础模块', value: 'baseModule', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 1, label: '页头', value: 'header', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 3, pid: 1, label: '导航', value: 'navigation', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:organization-chart' },
    { id: 4, pid: 1, label: 'AD', value: 'ad', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:advertisement-line' },
    { id: 5, pid: 1, label: '标题', value: 'title', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:h-2' },
    { id: 6, pid: 1, label: '图文列表', value: 'card', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:gallery-line' },
    { id: 7, pid: 1, label: '文本列表', value: 'text', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:file-list-line' },
    { id: 8, pid: 1, label: '分类', value: 'category', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:stack-line' },
    { id: 9, pid: 1, label: '页脚', value: 'footer', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-bottom-line' },
    { id: 10, pid: 0, label: '高级模块', value: 'advancedModule', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 11, pid: 10, label: '地图', value: 'map', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 12, pid: 10, label: '表单', value: 'form', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 13, pid: 0, label: '容器', value: 'containerModule', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 14, pid: 13, label: '栅格', value: 'grid', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-masonry-line' },
    { id: 15, pid: 13, label: '版心套件', value: 'wrap', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 16, pid: 0, label: '自定义模块', value: 'customerModule', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 17, pid: 16, label: '简介', value: 'about', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' },
    { id: 18, pid: 16, label: '服务', value: 'server', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: 'ri:layout-top-line' }
  ]
  const moduleTypeList = dataToTree(moduleTypeListData)
  const result = []
  if (isString(v)) {
    moduleTypeList.forEach((item) => {
      item.children.forEach((current) => {
        if (current.value === v) {
          result.push(current.label)
        }
      })
    })
  } else {
    if (v.length === 0) {
      moduleTypeList.forEach((item) => {
        item.children.forEach((current) => {
          result.push(current.label)
        })
      })
    } else if (v.length === 1) {
      moduleTypeList.forEach((item) => {
        if (item.value === v[0]) {
          item.children.forEach((current) => {
            result.push(current.label)
          })
        }
      })
    } else {
      moduleTypeList.forEach((item) => {
        if (item.value === v[0]) {
          item.children.forEach((current) => {
            if (current.value === v[1]) {
              result.push(current.label)
            }
          })
        }
      })
    }
  }
  return result
}
const dataToTree = (data) => {
  const parents = data.filter(function (item: any) {
    return item.pid === 0
  })
  const children = data.filter(function (item: any) {
    return item.pid !== 0
  })
  // 递归处理动态路由层级
  convert(parents, children)
  return parents
}
const convert = (parents: any, children: any) => {
  parents.forEach(function (item: any) {
    item.children = []
    children.forEach(function (current: any, index: any) {
      if (current.pid === item.id) {
        const temp = children.filter(function (v: any, idx: any) {
          return idx !== index
        }) // 删除已匹配项，这里未使用JSON.parse(JSON.stringify(children))是因为路由的component已经是函数形式，再格式化后，模块导入功能丢失，会导致找不到模块
        item.children.push(current)
        convert([current], temp) // 递归
      }
    })
  })
}
const getPageTypeData = (v) => {
  const pageTypeList = [
    { id: 1, pid: 0, label: '首页', value: 'index', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '产品中心', value: 'productCenter', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '产品列表', value: 'productList', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '产品详情', value: 'productDetail', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 5, pid: 0, label: '新闻中心', value: 'newsCenter', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 6, pid: 0, label: '新闻列表', value: 'newsList', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 7, pid: 0, label: '新闻详情', value: 'newsDetail', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 8, pid: 0, label: '案例中心', value: 'solutionCenter', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 9, pid: 0, label: '案例列表', value: 'solutionList', sort: 9, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 10, pid: 0, label: '案例详情', value: 'solutionDetail', sort: 10, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 11, pid: 0, label: '关于我们', value: 'about', sort: 11, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 12, pid: 0, label: '服务支持', value: 'server', sort: 12, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 13, pid: 0, label: '联系我们', value: 'contactUs', sort: 13, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 14, pid: 0, label: '专题页', value: 'special', sort: 14, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 15, pid: 0, label: '通用页', value: 'universal', sort: 15, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 16, pid: 0, label: '页头', value: 'header', sort: 16, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 17, pid: 0, label: '页脚', value: 'footer', sort: 17, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
  const result = []
  if (v === '') {
    pageTypeList.forEach((item) => {
      result.push(item.label)
    })
  } else {
    pageTypeList.forEach((item) => {
      if (item.value === v) {
        result.push(item.label)
      }
    })
  }
  return result
}
const getIndustryData = (v) => {
  const industryList = [
    { id: 1, pid: 0, label: '机械', value: 'machinery', sort: 1, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '教育', value: 'education', sort: 2, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '证券', value: 'securities', sort: 3, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 4, pid: 0, label: '母婴', value: 'mother', sort: 4, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 5, pid: 0, label: '数码', value: 'digital', sort: 5, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 6, pid: 0, label: '服装', value: 'clothing', sort: 6, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 7, pid: 0, label: '装修', value: 'fitment', sort: 7, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 8, pid: 0, label: '宠物', value: 'pet', sort: 8, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' },
    { id: 9, pid: 0, label: '通用', value: 'general', sort: 9, status: true, valueType: 'text', tagColor: '', remark: '', icon: '' }
  ]
  const result = []
  if (isString(v)) {
    if (v === '') {
      industryList.forEach((item) => {
        result.push(item.label)
      })
    } else {
      industryList.forEach((item) => {
        if (item.value === v) {
          result.push(item.label)
        }
      })
    }
  } else {
    industryList.forEach((item) => {
      v.forEach((current) => {
        if (item.value === current) {
          result.push(item.label)
        }
      })
    })
  }
  return result
}
const getEnableNumberData = (v) => {
  const statusList = [
    { id: 1, pid: 0, label: '全部', value: 1, sort: 1, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 2, pid: 0, label: '已启用', value: 2, sort: 2, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' },
    { id: 3, pid: 0, label: '已禁用', value: 3, sort: 3, status: true, valueType: 'number', tagColor: '', remark: '', icon: '' }
  ]
  let result = true
  if (v === 1) {
    result = Random.boolean()
  } else if (v === 2) {
    result = true
  } else {
    result = false
  }
  return result
}
const websiteListMock = (data) => {
  const result: any[] = []
  for (let i = 0; i < data.pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle(5, 12)' + ' - 第' + (i + 1) + '个',
      description: '@csentence()',
      date: Random.datetime(),
      'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
      'industry|1': getIndustryData(data.industry),
      'width|1': ['1024', '1440', '1280', '1360', '1500', '1600'],
      'device|1': getDeviceData(data.device),
      'img|1-115': 115,
      src: Random.image(),
      color: '@color()',
      author: '@cname()'
    })
  }
  return result
}
const websitePageListMock = (data) => {
  console.log(data.industry)
  const result: any[] = []
  for (let i = 0; i < data.pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle(5, 12)' + ' - 第' + (i + 1) + '个',
      description: '@csentence()',
      date: Random.datetime(),
      filename: Random.word(5, 12),
      'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
      'industry|1': getIndustryData(data.industry),
      'pageType|1': getPageTypeData(data.pageType),
      'device|1': getDeviceData(data.device),
      'img|1-115': 115,
      src: Random.image(),
      status: getEnableNumberData(data.status),
      author: '@cname()'
    })
  }
  return result
}
const pageListMock = (data) => {
  const result: any[] = []
  for (let i = 0; i < data.pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle(5, 12)' + ' - 第' + (i + 1) + '个',
      description: '@csentence()',
      date: Random.datetime(),
      filename: Random.word(5, 12),
      'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
      'pageType|1': getPageTypeData(data.pageType),
      'industry|1': getIndustryData(data.industry),
      'device|1': getDeviceData(data.device),
      'img|1-115': 115,
      src: Random.image(),
      author: '@cname()'
    })
  }
  return result
}
const moduleListMock = (data) => {
  const result: any[] = []
  if (data.isDesigner) {
    if (data.moduleType === 'header') {
      for (let i = 0; i < 2; i++) {
        result.push({
          id: Random.id(),
          title: '@ctitle(5, 12)' + ' - 第' + (i + 1) + '个',
          description: '@csentence()',
          date: Random.datetime(),
          'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
          'industry|1': getIndustryData(data.industry),
          'module|1': data.moduleType,
          'img|1-115': 115,
          src: Random.image(),
          package: 'package' + (i + 1),
          author: '@cname()'
        })
      }
    }
  } else {
    for (let i = 0; i < data.pageSize; i++) {
      result.push({
        id: Random.id(),
        title: '@ctitle(5, 12)' + ' - 第' + (i + 1) + '个',
        description: '@csentence()',
        date: Random.datetime(),
        'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
        'industry|1': getIndustryData(data.industry),
        'module|1': getModuleTypeData(data.moduleType),
        'img|1-115': 115,
        src: Random.image(),
        package: 'package' + (i + 1),
        author: '@cname()'
      })
    }
  }
  return result
}
export default [
  {
    url: '/getWebsiteListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '站点数据获取成功！',
        data: websiteListMock(data.body),
        total: 400
      }
    }
  },
  {
    url: '/getWebsitePageListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '页面数据获取成功！',
        data: websitePageListMock(data.body),
        total: 400
      }
    }
  },
  {
    url: '/getPageListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '页面数据获取成功！',
        data: pageListMock(data.body),
        total: 400
      }
    }
  },
  {
    url: '/getModuleListData',
    method: 'post',
    response: (data) => {
      return {
        code: 200,
        status: true,
        info: '模块数据获取成功！',
        data: moduleListMock(data.body),
        total: data.body.isDesigner ? (data.body.moduleType === 'header' ? 2 : 0) : 400
      }
    }
  }
] as MockMethod[]
