﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}

const getBarcodeListData = () => {
  const result = []
  for (let j = 0; j < 20; j++) {
    const textVal = /[a-z][A-Z][0-9]{8}/
    result.push({
      id: Random.id(),
      text: textVal,
      'tagType|1': ['img', 'svg', 'canvas'],
      options: {
        'format|1': ['CODE128'],
        width: 3,
        height: 100,
        displayValue: true,
        'fontOptions|1': ['bold', 'italic', 'bold italic'],
        fontSize: 16,
        text: textVal,
        'textAlign|1': ['left', 'center', 'right'],
        'textPosition|1': ['bottom', 'top'],
        textMargin: 2,
        'background|1': ['#ffffff', '#e7faf0', '#f4f4f5', '#e7faf0', '#fff8e6'],
        'lineColor|1': ['#000000', '#1890ff', '#41B584', '#e83015', '#f2be45'],
        margin: 10
      }
    })
  }
  return result
}
export default [
  {
    url: '/getBarcodeData',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getBarcodeListData()
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
