﻿import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}

const getPrintDetail = () => {
  const tags = []
  const lengthNum = Random.integer(2, 5)
  for (let i = 0; i < lengthNum; i++) {
    tags.push(Random.cword(2, 6))
  }
  const result = {
    id: Random.id(),
    title: '@ctitle()',
    description: '@cparagraph()',
    date: Random.datetime(),
    'type|1': ['landscape', 'animal', 'food', 'beauty', 'car', 'cartoon', 'game', 'other'],
    'img|1-115': 115,
    tags: tags,
    'like|100-5000': 5000,
    'collect|10-500': 500,
    'message|10-500': 500,
    author: '@cname()',
    content:
      '<p>散尽离愁，携得清风出画楼。————晏几道《采桑子·高吟烂醉淮西月》</p><p>海棠不惜胭脂色，独立蒙蒙细雨中。————陈与义《春寒》</p><p>瘦藤忽梦寻梅去，袅袅寒溪独木桥。————程公许《除夕和唐人张继张佑即事四绝句·经世无才慕管萧》</p><p>醉酒不知归，凭栏映婀娜。</p><p>红酥手，黄縢酒，满城春色宫墙柳。东风恶，欢情薄。一怀愁绪，几年离索。错、错、错。</p><p>春如旧，人空瘦，泪痕红浥鲛绡透。桃花落，闲池阁。山盟虽在，锦书难托。莫、莫、莫</p>',
    lineColumnData: [],
    tableData: []
  }
  for (let j = 0; j < 12; j++) {
    result.lineColumnData.push({
      id: Random.id(),
      date: j + 1 + '月',
      'num|100-1000': 1000,
      'percentage|1-100': 100
    })
  }
  for (let j = 0; j < 40; j++) {
    result.tableData.push({
      id: Random.id(),
      title: Random.csentence(),
      role: Random.cword(3, 5),
      age: Random.integer(18, 150),
      date: Random.date(),
      name: Random.cname(),
      city: Random.city(true)
    })
  }
  return result
}
export default [
  {
    url: '/getPrintDetail',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: getPrintDetail()
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
