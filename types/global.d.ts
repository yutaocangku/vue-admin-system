﻿declare type Nullable<T> = T | null

declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>

declare type Recordable<T = any> = Record<string, T>
declare interface ImportMetaEnv extends ViteEnv {
  __: unknown
}
declare interface ViteEnv {
  VITE_PORT: number
  VITE_OUTDIR: string
  VITE_PUBLIC_PATH: string
  VITE_PROXY_API_PATH: string
  VITE_BASE_API_PATH: string
  VITE_LEGACY: boolean
  VITE_WEBSOCKET_WSS: string
}
declare interface ServerConfigs {
  Version?: string
  Title?: string
  Locale?: string
  Size?: string
  Layout?: string
  PrimaryColor?: string
  MenuBgColor?: string
  HeaderBgColor?: string
  IsLight?: boolean
  DarkTheme?: string
  IsFixedHeader?: boolean
  IsFixedSidebar?: boolean
  IsSidebarAccordion?: boolean
  IsEnforceShowColumn?: boolean
  IsColumnInline?: boolean
  IsTagCache?: boolean
  IsKeepAlive?: boolean
  IsLockScreen?: boolean
  AutoLockScreenTime?: number
  LockScreenPassword?: string
  IsLogin?: boolean
  AutoSignOutTime?: number
  IsBreadcrumbEnable?: boolean
  IsLockScreenEnable?: boolean
  IsNoticeEnable?: boolean
  IsFullScreenEnable?: boolean
  IsGlobalizationEnable?: boolean
  IsSizeEnable?: boolean
  IsTagEnable?: boolean
  IsTagContextMenuEnable?: boolean
  IsTagRefreshEnable?: boolean
  IsTagOperateEnable?: boolean
  IsFullContentEnable?: boolean
  SidebarCollapseButtonPosition?: string
  ColumnCollapseButtonPosition?: string
  HeaderMenuAlign?: string
  HeaderMenuStyle?: string
  ColumnStyle?: string
  TagBarStyle?: string
  Space?: number
  IsFooterEnable?: boolean
  IsGreyPatternEnable?: boolean
  IsColorWeaknessPatternEnable?: boolean
  IsProgressEnable?: boolean
  IsLoadingEnable?: boolean
  IsCutscenesEnable?: boolean
  CutsceneType?: string
  IsSidebarCollapse?: boolean
  IsSidebarHidden?: boolean
  IsColumnCollapse?: boolean
  IsNoColumn?: boolean
  Device?: string
}

declare interface Window {
  WebKitMutationObserver: any
  MozMutationObserver: any
  webkitCancelAnimationFrame: (handle: number) => void
  mozCancelAnimationFrame: (handle: number) => void
  oCancelAnimationFrame: (handle: number) => void
  msCancelAnimationFrame: (handle: number) => void
  webkitRequestAnimationFrame: (callback: FrameRequestCallback) => number
  mozRequestAnimationFrame: (callback: FrameRequestCallback) => number
  oRequestAnimationFrame: (callback: FrameRequestCallback) => number
  msRequestAnimationFrame: (callback: FrameRequestCallback) => number
  // 键盘按键记录
  $KeyboardActive?: { [T: string]: boolean }
  onKeySpacePressHold?: Function
}
