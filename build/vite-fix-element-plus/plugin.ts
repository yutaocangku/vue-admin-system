﻿import { Plugin } from 'vite'
import path from 'path'
import fs from 'fs'

const pathResolve = (..._path: string[]) => path.resolve(__dirname, ..._path)
const replaceFile = (sourcePath: string, targetPath: string) => fs.writeFileSync(targetPath, fs.readFileSync(sourcePath, 'utf-8'), 'utf-8')

// 修复 element-plus  中 affix bug
export default function fixElementPlus(): Plugin {
  return {
    name: 'vite-fix-element-plus',
    enforce: 'pre',
    configResolved() {
      const libTarget = pathResolve('../../node_modules/element-plus/lib/components/affix/src/affix2.js')
      const libSource = pathResolve('./lib/affix/src/affix2.js')
      const esTarget = pathResolve('../../node_modules/element-plus/es/components/affix/src/affix2.mjs')
      const esSource = pathResolve('./es/affix/src/affix2.mjs')
      replaceFile(libSource, libTarget)
      replaceFile(esSource, esTarget)
    }
  }
}
