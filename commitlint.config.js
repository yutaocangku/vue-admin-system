﻿module.exports = {
  ignores: [(commit) => commit.includes('init')],
  extends: ['@commitlint/config-conventional'],
  rules: {
    'body-leading-blank': [2, 'always'],
    'footer-leading-blank': [1, 'always'],
    'header-max-length': [2, 'always', 108],
    'subject-empty': [2, 'never'],
    'type-empty': [2, 'never'],
    'subject-case': [0, 'never'],
    'subject-full-stop': [0, 'never'],
    'type-enum': [
      2,
      'always',
      [
        'feat', // 新功能(feature)
        'fix', // 修补bug
        'docs', // 文档方面的修改(documentation)
        'style', // 格式、样式(不影响代码运行的变动)
        'refactor', // 重构(即不是新增功能，也不是修改BUG的代码)
        'perf', // 优化相关，比如提升性能、体验
        'test', // 添加或修改测试代码
        'build', // 构建工具或外部依赖包的修改。比如更新依赖包的版本等
        'ci', // 持续集成的配置修改或脚本修改
        'chore', // 杂项。构建过程或辅助工具的变动
        'revert' // 撤销某次提交
      ]
    ]
  }
}
