﻿module.exports = {
  root: true,
  customSyntax: 'postcss-scss',
  plugins: ['stylelint-scss', 'stylelint-order', 'stylelint-config-rational-order/plugin'],
  extends: ['stylelint-config-standard', 'stylelint-config-standard-scss', 'stylelint-config-recommended-scss', 'stylelint-config-recommended-vue', 'stylelint-config-recess-order', 'stylelint-config-rational-order', 'stylelint-config-prettier'],
  rules: {
    // 配置空格数
    indentation: 2,
    // 颜色指定小写（避免与prettier配置冲突）
    'color-hex-case': 'lower',
    // 禁止空块
    'block-no-empty': null,
    // 颜色6位长度
    'color-hex-length': 'long',
    // 兼容自定义标签名
    'selector-type-no-unknown': [
      true,
      {
        'ignoreTypes': []
      }
    ],
    'color-function-notation': ['legacy', { 'ignore': ['with-var-inside'] }],
    'alpha-value-notation': ['number'],
    // 禁止低优先级的选择器出现在高优先级的选择器之后。
    'no-descending-specificity': null,
    // 禁止空注释
    'comment-no-empty': true,
    // 禁止简写属性的冗余值
    'shorthand-property-no-redundant-values': true,
    // 禁止值的浏览器引擎前缀
    'value-no-vendor-prefix': true,
    // property-no-vendor-prefix
    'property-no-vendor-prefix': true,
    // 禁止小于 1 的小数有一个前导零（避免与prettier冲突）
    'number-leading-zero': 'always',
    // 禁止空第一行
    'no-empty-first-line': true,
    // 关系选择符之前要求有一个空格
    'selector-combinator-space-before': 'never',
    // 关系选择符之后要求有一个空格
    'selector-combinator-space-after': 'never',
    'selector-class-pattern': null,
    'selector-pseudo-class-no-unknown': [
      true,
      {
        ignorePseudoClasses: ['global']
      }
    ],
    'selector-pseudo-element-no-unknown': [
      true,
      {
        ignorePseudoElements: ['v-deep']
      }
    ],
    // 'at-rule-no-unknown': [
    //   true,
    //   {
    //     ignoreAtRules: ['tailwind', 'apply', 'variants', 'responsive', 'screen', 'function', 'if', 'each', 'include', 'mixin', 'extend', 'use', 'import']
    //   }
    // ],
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': true,
    'function-no-unknown': null,
    'no-duplicate-selectors': null,
    'no-empty-source': null,
    'named-grid-areas-no-invalid': null,
    'unicode-bom': 'never',
    'font-family-no-missing-generic-family-keyword': null,
    'declaration-colon-space-after': 'always-single-line',
    'declaration-colon-space-before': 'never',
    'declaration-block-trailing-semicolon': null,
    'rule-empty-line-before': [
      'always',
      {
        ignore: ['after-comment', 'first-nested']
      }
    ],
    'unit-no-unknown': [
      true,
      {
        ignoreUnits: ['rpx']
      }
    ],
    'order/order': [
      [
        'dollar-variables',
        'custom-properties',
        'at-rules',
        'declarations',
        {
          type: 'at-rule',
          name: 'supports'
        },

        {
          type: 'at-rule',
          name: 'media'
        },

        'rules'
      ],
      {
        severity: 'warning'
      }
    ],
    'plugin/rational-order': [
      true,
      {
        'border-in-box-model': false,
        'empty-line-between-groups': false
      }
    ],
    'declaration-block-single-line-max-declarations': 20
  },

  ignoreFiles: ['**/*.js', '**/*.jsx', '**/*.tsx', '**/*.ts'],
  overrides: [
    {
      files: ['**/*.(scss|sass|css)'],
      customSyntax: 'postcss-scss',
      plugins: ['stylelint-scss']
    },

    {
      files: ['**/*.(vue|html)'],
      customSyntax: 'postcss-html',
      // extends: ['stylelint-config-html'],
      rules: {
        'keyframes-name-pattern': null,
        'selector-pseudo-class-no-unknown': [
          true,
          {
            ignorePseudoClasses: ['deep', 'global']
          }
        ],
        'selector-pseudo-element-no-unknown': [
          true,
          {
            ignorePseudoElements: ['v-deep', 'v-global', 'v-slotted']
          }
        ]
      }
    }
  ]
}
