const usePublic = () => {
  const initInfo = ref({
    list: {
      data: [],
      total: 0
    },
    knowledgeTypeList: [],
    knowledgeTagList: [],
    search: {
      page: 1,
      limit: 20,
      title: '',
      tag: '',
      type: ''
    }
  })
  return {
    initInfo
  }
}
export default usePublic
