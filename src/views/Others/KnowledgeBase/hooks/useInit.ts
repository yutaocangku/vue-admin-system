import { knowledge } from '@/api/knowledgeBase'
const useInit = (initInfo) => {
  // 初始化页面
  const initPage = () => {
    getListData()
  }

  // 获取当前查询条件下的数据
  const getListData = () => {
    resetPage()
    const result: any = {}
    result.pageIndex = initInfo.value.search.page
    result.pageSize = initInfo.value.search.limit
    result.name = initInfo.value.search.title
    knowledge.list(result, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
      if (res && res.status) {
        initInfo.value.list.total = res.allcount
        if (res.data && res.data.length > 0) {
          initInfo.value.list.data = res.data
        }
      }
    })
  }

  // 页面数据条数改变重新获取数据
  const handleSizeChange = (val: number) => {
    initInfo.value.search.limit = val
    initInfo.value.search.page = 1
    getListData()
  }

  // 当前页改变重新获取数据
  const handleCurrentChange = (val: number) => {
    initInfo.value.search.page = val
    getListData()
  }

  // 重置页面数据
  const resetPage = () => {
    initInfo.value.list.data = []
  }

  // 重置
  const resetSearch = () => {
    initInfo.value.search.page = 1
    initInfo.value.search.limit = 20
    initInfo.value.search.title = ''
    initInfo.value.search.tag = ''
    initInfo.value.search.type = ''
    getListData()
  }

  return {
    initPage,
    getListData,
    handleSizeChange,
    handleCurrentChange,
    resetSearch
  }
}
export default useInit
