const useDraggableAction = () => {
  const widgetModules = ref({})
  const widgetProperty = ref({})
  const defaultConfig = ref({})
  onMounted(() => {
    widgetModules.value = import.meta.globEager(`../components/widget/**/**/**/source/index.vue`)
    widgetProperty.value = import.meta.globEager(`../components/widget/**/**/**/property/index.vue`)
    defaultConfig.value = import.meta.globEager(`../components/widget/**/**/**/index.ts`)
    console.log(defaultConfig.value, '部件组件')
  })
  return {
    widgetModules,
    widgetProperty,
    defaultConfig
  }
}
export default useDraggableAction
