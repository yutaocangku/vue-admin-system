import { cloneDeep } from 'lodash-es'
const useComponentTab = (domRef) => {
  const activeCollapse = ref([])
  const moduleType = ref('')
  const componentTab = ref('widgetComponent')
  const handleComponentTypeChange = (type) => {
    componentTab.value = type
    nextTick(() => {
      domRef.value.changeModuleTypeHandler(moduleType.value)
    })
  }
  const initWidgetSelectedType = (data) => {
    const dataArr = cloneDeep(data)
    console.log(dataArr, '初始化')
    dataArr.forEach((item, index) => {
      activeCollapse.value.push(item.value)
      if (item.children.length > 0) {
        item.children.forEach((current, idx) => {
          if (index === 0 && idx === 0) {
            current.isActive = true
            moduleType.value = current.value
            domRef.value.changeModuleTypeHandler(current.value)
          } else {
            current.isActive = false
          }
        })
      }
    })
    return dataArr
  }
  return {
    moduleType,
    activeCollapse,
    componentTab,
    handleComponentTypeChange,
    initWidgetSelectedType
  }
}
export default useComponentTab
