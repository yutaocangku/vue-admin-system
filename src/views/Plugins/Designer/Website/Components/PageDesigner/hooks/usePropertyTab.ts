const usePropertyTab = () => {
  const propertyTab = ref('pageProperty')
  const handlePropertyTypeChange = (type) => {
    propertyTab.value = type
  }
  return {
    propertyTab,
    handlePropertyTypeChange
  }
}
export default usePropertyTab
