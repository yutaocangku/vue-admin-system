const useAsideStyle = () => {
  const isWidgetButtonHover = ref(false)
  const isWidgetAsideShow = ref(false)
  const isPropertyButtonHover = ref(false)
  const isPropertyAsideShow = ref(false)
  // PC端装修侧边栏鼠标经过状态样式
  const onMouseenter = (v) => {
    if (v === 'widget') {
      isWidgetButtonHover.value = true
    } else {
      isPropertyButtonHover.value = true
    }
  }
  // PC端装修侧边栏鼠标离开状态样式
  const onMouseleave = (v) => {
    if (v === 'widget') {
      isWidgetButtonHover.value = false
    } else {
      isPropertyButtonHover.value = false
    }
  }
  return {
    isWidgetAsideShow,
    isWidgetButtonHover,
    isPropertyAsideShow,
    isPropertyButtonHover,
    onMouseenter,
    onMouseleave
  }
}
export default useAsideStyle
