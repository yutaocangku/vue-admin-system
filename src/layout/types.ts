﻿export type childrenType = {
  path?: string
  noShowingChildren?: boolean
  children?: childrenType[]
  value: unknown
  meta?: {
    icon?: string
    title?: string
    i18n?: boolean
    level?: number
    badge?: string
    dot?: boolean
  }
  showTooltip?: boolean
}
export type tagOperateType = {
  icon: string
  text: string
  divided: boolean
  disabled: boolean
  show: boolean
}
export type tagRouteConfig = {
  path?: string
  meta?: {
    title?: string
    icon?: string
    showLink?: boolean
    i18n?: boolean
    showTag?: number
    isFixed?: boolean
    badge?: string
    dot?: boolean
  }
  children?: tagRouteConfig[]
  name?: string
}
export type settingSelectType = {
  label: string
  value: string
  disabled?: boolean
}
