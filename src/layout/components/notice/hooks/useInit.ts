import { base } from '@/api/public'
import { storeToRefs } from 'pinia'
import { useUserInfoStoreWithOut } from '@/store/storage/userInfoStore'
import dayjs from 'dayjs'
const useInit = (initInfo, noticeSocket, initSocket) => {
  const userInfoStore = useUserInfoStoreWithOut()
  const { userInfo } = storeToRefs(userInfoStore)
  const getInitPage = () => {
    base.notice(null, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
      if (res && res.status) {
        console.log(res)
        initInfo.value.activeTab = res.data[0].name
        initInfo.value.list.data = res.data
        initInfo.value.list.count = 0
        initInfo.value.list.data.forEach((notice: any) => {
          initInfo.value.list.count += notice.list.length
        })
        initInfo.value.t = new Date(dayjs().format()).getTime()
        noticeSocket.value.message = {
          type: 'notice',
          uid: userInfo.value.id,
          sendtime: initInfo.value.t
        }
        initSocket(noticeSocket)
      }
    })
  }

  return {
    getInitPage
  }
}
export default useInit
