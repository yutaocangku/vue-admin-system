const usePublic = () => {
  const initInfo: any = ref({
    t: 0,
    activeTab: '',
    list: {
      data: [],
      count: 0
    }
  })

  return {
    initInfo
  }
}
export default usePublic
