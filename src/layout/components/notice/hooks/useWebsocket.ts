import useSocket from '@/utils/websocket/useSocket'
import { socketOptions } from '@/utils/websocket/utils'
import { cloneDeep } from 'lodash-es'
const useWebsocket = (initInfo) => {
  const onMessageCallback = (res: any) => {
    console.log(res)
    if (res.type === 'notice' && res.sendtime === initInfo.value.t) {
      // 执行相关逻辑
      initInfo.value.list.data = res.data
      initInfo.value.list.count = 0
      initInfo.value.list.data.forEach((notice: any) => {
        initInfo.value.list.count += notice.list.length
      })
    }
  }
  const { initSocket } = useSocket(onMessageCallback)
  const noticeSocket: any = ref(null)
  noticeSocket.value = cloneDeep(socketOptions())

  return {
    noticeSocket,
    initSocket
  }
}
export default useWebsocket
