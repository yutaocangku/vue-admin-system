import { useTagCacheStore } from '@/store/storage/tagCacheStore'
import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useTagCacheChangeHandle = () => {
  const tagCacheStore = useTagCacheStore()
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  // 获取被缓存的标签页数据
  const tagCacheChangeHandle = () => {
    if (!systemConfig.value.isTagCache) {
      tagCacheStore.removeAllTagCache()
    } else {
      tagCacheStore.setAllTagCache()
    }
    systemConfigStore.setSystemConfigCache()
  }
  return {
    tagCacheChangeHandle
  }
}
export default useTagCacheChangeHandle
