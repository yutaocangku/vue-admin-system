﻿import { settingSelectType } from '../../../types'
import { $t as t } from '@/plugins/i18n'
import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useInterfaceDisplay = () => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  // 侧边栏折叠按钮展示位置
  const sidebarCollapseButtonPosition = ref<Array<settingSelectType>>([
    { label: t('settings.settingCollapseButtonPositionHidden'), value: 'hidden', disabled: false },
    { label: t('settings.settingCollapseButtonPositionHeader'), value: 'header', disabled: false },
    { label: t('settings.settingCollapseButtonPositionTagBar'), value: 'tagBar', disabled: false },
    { label: t('settings.settingCollapseButtonPositionFloat'), value: 'float', disabled: false },
    { label: t('settings.settingCollapseButtonPositionBottom'), value: 'bottom', disabled: false }
  ])
  // 分栏折叠按钮展示位置
  const columnCollapseButtonPosition = ref<Array<settingSelectType>>([
    { label: t('settings.settingCollapseButtonPositionHidden'), value: 'hidden', disabled: false },
    { label: t('settings.settingCollapseButtonPositionHeader'), value: 'header', disabled: false },
    { label: t('settings.settingCollapseButtonPositionTagBar'), value: 'tagBar', disabled: false },
    { label: t('settings.settingCollapseButtonPositionFloat'), value: 'float', disabled: false }
  ])
  // 顶栏菜单布局
  const headerMenuAlign = ref<Array<settingSelectType>>([
    { label: t('settings.settingAlignLeft'), value: 'left' },
    { label: t('settings.settingAlignCenter'), value: 'center' },
    { label: t('settings.settingAlignRight'), value: 'right' }
  ])
  // 顶栏菜单风格
  const headerMenuStyle = ref<Array<settingSelectType>>([
    { label: t('settings.settingHeaderMenuStyleDefault'), value: 'default' },
    { label: t('settings.settingHeaderMenuStyleCard'), value: 'card' },
    { label: t('settings.settingHeaderMenuStyleSmart'), value: 'smart' }
  ])
  // 分栏风格
  const columnStyle = ref<Array<settingSelectType>>([
    { label: t('settings.settingColumnStyleHorizontal'), value: 'horizontal' },
    { label: t('settings.settingColumnStyleVertical'), value: 'vertical' },
    { label: t('settings.settingColumnStyleCard'), value: 'card' },
    { label: t('settings.settingColumnStyleArrow'), value: 'arrow' }
  ])
  // 标签风格
  const tagBarStyle = ref<Array<settingSelectType>>([
    { label: t('settings.settingTagBarStyleDefault'), value: 'default' },
    { label: t('settings.settingTagBarStyleCard'), value: 'card' },
    { label: t('settings.settingTagBarStyleSmart'), value: 'smart' },
    { label: t('settings.settingTagBarStyleSmooth'), value: 'smooth' }
  ])
  const layoutChangeInitSidebarCollapseButtonPosition = () => {
    sidebarCollapseButtonPosition.value.forEach((item) => {
      if (item.value === 'header' || item.value === 'tagBar' || item.value === 'float') {
        item.disabled = !!(systemConfig.value.layout === 'column' || systemConfig.value.layout === 'vertical__column')
      }
    })
    if ((systemConfig.value.layout === 'column' || systemConfig.value.layout === 'vertical__column') && systemConfig.value.sidebarCollapseButtonPosition !== 'hidden' && systemConfig.value.sidebarCollapseButtonPosition !== 'bottom') {
      systemConfig.value.sidebarCollapseButtonPosition = 'bottom'
      systemConfigStore.setSystemConfigCache()
    }
  }
  // 监听布局主题切换事件
  watch(
    () => systemConfig.value.layout,
    () => {
      layoutChangeInitSidebarCollapseButtonPosition()
    }
  )
  onMounted(() => {
    layoutChangeInitSidebarCollapseButtonPosition()
  })
  return {
    sidebarCollapseButtonPosition,
    columnCollapseButtonPosition,
    headerMenuAlign,
    headerMenuStyle,
    columnStyle,
    tagBarStyle
  }
}
export default useInterfaceDisplay
