﻿import { settingSelectType } from '../../../types'
import { $t as t } from '@/plugins/i18n'
const useAnimationSetting = () => {
  // 过场动画类型
  const cutsceneType = ref<Array<settingSelectType>>([
    { label: t('settings.settingCutsceneTypeDefault'), value: 'default' },
    { label: t('settings.settingCutsceneTypeFade'), value: 'fade' },
    { label: t('settings.settingCutsceneTypeFadeLeft'), value: 'fade-left' },
    { label: t('settings.settingCutsceneTypeFadeRight'), value: 'fade-right' },
    { label: t('settings.settingCutsceneTypeFadeTop'), value: 'fade-top' },
    { label: t('settings.settingCutsceneTypeFadeBottom'), value: 'fade-bottom' },
    { label: t('settings.settingCutsceneTypeZoomIn'), value: 'zoom-in' },
    { label: t('settings.settingCutsceneTypeZoomOut'), value: 'zoom-out' },
    { label: t('settings.settingCutsceneTypeZoom'), value: 'zoom' }
  ])

  return {
    cutsceneType
  }
}
export default useAnimationSetting
