﻿import { storeToRefs } from 'pinia'
import { usePublicStore } from '@/store/public/publicStore'
const useSettingShow = () => {
  const publicStore = usePublicStore()
  const { showSettings } = storeToRefs(publicStore)
  const handleClose = () => {
    showSettings.value = false
  }
  return {
    showSettings,
    handleClose
  }
}
export default useSettingShow
