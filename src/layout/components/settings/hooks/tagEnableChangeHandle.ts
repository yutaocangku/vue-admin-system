﻿import useAffixTopHeight from '@/hooks/affixTopHeight'
import { storeToRefs } from 'pinia'
import { usePublicStore } from '@/store/public/publicStore'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useTagEnableChangeHandle = () => {
  const publicStore = usePublicStore()
  const { affixTop } = storeToRefs(publicStore)
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const { getAffixTop } = useAffixTopHeight()
  const tagEnableChangeHandle = () => {
    if (!systemConfig.value.isTagEnable) {
      systemConfig.value.isTagCache = false
      systemConfig.value.isKeepAlive = false
    }
    systemConfigStore.setSystemConfigCache()
    affixTop.value = getAffixTop()
  }
  return {
    tagEnableChangeHandle
  }
}
export default useTagEnableChangeHandle
