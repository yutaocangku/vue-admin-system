﻿import useAffixTopHeight from '@/hooks/affixTopHeight'
import { storeToRefs } from 'pinia'
import { usePublicStore } from '@/store/public/publicStore'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useFixedSwitchChangeHandle = () => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const { getAffixTop } = useAffixTopHeight()
  const publicStore = usePublicStore()
  const { affixTop } = storeToRefs(publicStore)
  const fixedSwitchChangeHandle = (type) => {
    if (systemConfig.value.layout === 'common' || systemConfig.value.layout === 'comprehensive' || systemConfig.value.layout === 'column') {
      if (type === 'sidebar') {
        systemConfig.value.isFixedHeader = !systemConfig.value.isFixedHeader
      } else {
        systemConfig.value.isFixedSidebar = !systemConfig.value.isFixedSidebar
      }
    }
    systemConfigStore.setSystemConfigCache()
    affixTop.value = getAffixTop()
  }
  return {
    fixedSwitchChangeHandle
  }
}
export default useFixedSwitchChangeHandle
