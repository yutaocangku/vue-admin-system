﻿import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
import { useKeepAliveStore } from '@/store/keepAlive/keepAliveStore'
const useKeepAliveChangeHandle = () => {
  const keepAliveStore = useKeepAliveStore()
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const keepAliveChangeHandle = () => {
    if (!systemConfig.value.isKeepAlive) {
      keepAliveStore.removeAllKeepAlive()
    }
    systemConfigStore.setSystemConfigCache()
  }
  return {
    keepAliveChangeHandle
  }
}
export default useKeepAliveChangeHandle
