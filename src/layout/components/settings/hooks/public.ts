﻿import { setThemeColor } from '@/utils/theme'
import { toggleClass } from '@/utils/operate'
import { systemConfigEnum, locale } from '@/enums/systemConfigEnums'
import { getDevice } from '@/utils/deviceDetection'
import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
import useLogout from '@/hooks/useLogout'
const usePublic = (router) => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig, language } = storeToRefs(systemConfigStore)
  const { Logout } = useLogout(router)

  const settingChangeHandle = () => {
    systemConfigStore.setSystemConfigCache()
  }

  // 暗系主题切换事件时，设置明暗主题未选中状态
  const darkThemeChangeHandle = () => {
    systemConfig.value.isLight = false
    systemConfigStore.setSystemConfigCache()
    setThemeColor(systemConfig.value)
  }
  // 明暗主题改变事件
  watch(
    () => systemConfig.value.isLight,
    () => {
      setThemeColor(systemConfig.value)
    }
  )
  // 系统主题改变事件
  watch(
    () => systemConfig.value.primaryColor,
    () => {
      setThemeColor(systemConfig.value)
    }
  )
  // 菜单主题改变事件
  watch(
    () => systemConfig.value.menuBgColor,
    () => {
      setThemeColor(systemConfig.value)
    }
  )
  // 顶栏主题改变事件
  watch(
    () => systemConfig.value.headerBgColor,
    () => {
      setThemeColor(systemConfig.value)
    }
  )
  // 暗系主题改变事件
  watch(
    () => systemConfig.value.darkTheme,
    () => {
      setThemeColor(systemConfig.value)
    }
  )
  // 模块间距改变事件
  watch(
    () => systemConfig.value.space,
    () => {
      setThemeColor(systemConfig.value)
    }
  )
  // 监听灰色模式切换事件
  watch(
    () => systemConfig.value.isGreyPatternEnable,
    () => {
      toggleClass(systemConfig.value.isGreyPatternEnable, 'html-grey', document.querySelector('html'))
    }
  )
  // 监听色弱模式切换事件
  watch(
    () => systemConfig.value.isColorWeaknessPatternEnable,
    () => {
      toggleClass(systemConfig.value.isColorWeaknessPatternEnable, 'html-color-weakness', document.querySelector('html'))
    }
  )
  // 恢复默认
  const resetConfig = () => {
    // 循环对象
    for (const key in systemConfig.value) {
      systemConfig.value[key] = systemConfigEnum[key]
    }
    language.value = locale
    systemConfig.value.device = getDevice()
    systemConfigStore.setSystemConfigCache()
    systemConfigStore.setLanguageCache()
    setThemeColor(systemConfig.value)
  }
  // 清空缓存返回登录页
  const clearCacheToLogin = () => {
    resetConfig()
    Logout()
  }
  return {
    darkThemeChangeHandle,
    resetConfig,
    clearCacheToLogin,
    settingChangeHandle
  }
}
export default usePublic
