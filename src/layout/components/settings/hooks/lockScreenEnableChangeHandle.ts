﻿import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useLockScreenEnableChangeHandle = () => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const lockScreenEnableChangeHandle = () => {
    if (!systemConfig.value.isLockScreenEnable) {
      systemConfig.value.isLockScreen = false
      systemConfig.value.autoLockScreenTime = 0
    }
    systemConfigStore.setSystemConfigCache()
  }
  return {
    lockScreenEnableChangeHandle
  }
}
export default useLockScreenEnableChangeHandle
