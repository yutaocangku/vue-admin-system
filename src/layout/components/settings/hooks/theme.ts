﻿import { settingSelectType } from '../../../types'
import { $t as t } from '@/plugins/i18n'
import { primaryColorSource, menuBgColorSource, headerBgColorSource, darkThemeData } from '@/utils/theme'
const useTheme = () => {
  // 项目布局主题
  const layoutList = ref<Array<settingSelectType>>([
    { label: t('settings.settingLayoutVertical'), value: 'vertical' },
    { label: t('settings.settingLayoutHorizontal'), value: 'horizontal' },
    { label: t('settings.settingLayoutCommon'), value: 'common' },
    { label: t('settings.settingLayoutComprehensive'), value: 'comprehensive' },
    { label: t('settings.settingLayoutVerticalComprehensive'), value: 'vertical__comprehensive' },
    { label: t('settings.settingLayoutColumn'), value: 'column' },
    { label: t('settings.settingLayoutVerticalColumn'), value: 'vertical__column' }
  ])
  // 项目主题色
  const primaryThemeList = ref<Array<settingSelectType>>([
    { label: t('settings.settingThemeBlue'), value: primaryColorSource().blue },
    { label: t('settings.settingThemeGreen'), value: primaryColorSource().green },
    { label: t('settings.settingThemeRed'), value: primaryColorSource().red },
    { label: t('settings.settingThemeAmber'), value: primaryColorSource().amber },
    { label: t('settings.settingThemePink'), value: primaryColorSource().pink },
    { label: t('settings.settingThemePurple'), value: primaryColorSource().purple },
    { label: t('settings.settingThemeGlass'), value: primaryColorSource().glass },
    { label: t('settings.settingThemeCyan'), value: primaryColorSource().cyan },
    { label: t('settings.settingThemeOrange'), value: primaryColorSource().orange },
    { label: t('settings.settingThemeGold'), value: primaryColorSource().gold }
  ])
  // 侧边菜单主题
  const menuBgList = ref<Array<settingSelectType>>([])
  const getMenuBgList = () => {
    for (const key in menuBgColorSource()) {
      menuBgList.value.push({
        label: key,
        value: menuBgColorSource()[key]
      })
    }
  }
  getMenuBgList()
  // 顶栏主题
  const headerBgList = ref<Array<settingSelectType>>([])
  const getHeaderBgList = () => {
    for (const key in headerBgColorSource()) {
      headerBgList.value.push({
        label: key,
        value: headerBgColorSource()[key]
      })
    }
  }
  getHeaderBgList()
  // 暗系主题
  const darkThemeList = ref<Array<settingSelectType>>([])
  const getDarkThemeList = () => {
    darkThemeData().forEach((item) => {
      darkThemeList.value.push({
        label: item.darkColor,
        value: item.darkColor
      })
    })
  }
  getDarkThemeList()
  return {
    layoutList,
    primaryThemeList,
    menuBgList,
    headerBgList,
    darkThemeList
  }
}
export default useTheme
