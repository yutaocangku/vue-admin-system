﻿import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useLayoutChangeHandle = () => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const layoutChangeHandle = () => {
    if (systemConfig.value.layout === 'common' || systemConfig.value.layout === 'comprehensive' || systemConfig.value.layout === 'column') {
      if ((systemConfig.value.isFixedHeader && !systemConfig.value.isFixedSidebar) || (!systemConfig.value.isFixedHeader && systemConfig.value.isFixedSidebar)) {
        systemConfig.value.isFixedHeader = false
        systemConfig.value.isFixedSidebar = false
      }
    }
    systemConfigStore.setSystemConfigCache()
  }
  return {
    layoutChangeHandle
  }
}
export default useLayoutChangeHandle
