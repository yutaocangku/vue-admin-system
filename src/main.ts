import { createApp } from 'vue'
import App from '@/App.vue'
import { setupStore } from './store'
import router from './router'
import { MotionPlugin } from '@vueuse/motion'
import { usI18n } from './plugins/i18n'
import VueLuckyCanvas from '@lucky-canvas/vue'
import 'virtual:windi.css'
import 'virtual:windi-devtools'
// import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/src/base.scss'
import 'element-plus/theme-chalk/src/notification.scss'
import 'element-plus/theme-chalk/src/message.scss'
import 'element-plus/theme-chalk/src/message-box.scss'
import 'element-plus/theme-chalk/src/loading.scss'

import * as ElIconModules from '@element-plus/icons-vue'

import SvgIcon from '@/components/SvgIcon/index.vue'

import contextmenu from 'v-contextmenu'
import 'v-contextmenu/dist/themes/default.css'

import 'bpmn-js/dist/assets/diagram-js.css' // 基础样式
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn.css' // 节点基础图标
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css' // 节点完整图标
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css'
import '@bpmn-io/properties-panel/dist/assets/properties-panel.css' // 系统属性面板样式

// 小地图
import 'diagram-js-minimap/assets/diagram-js-minimap.css'
// 流程模拟
import 'bpmn-js-token-simulation/assets/css/bpmn-js-token-simulation.css'
// 流程校验
import 'bpmn-js-bpmnlint/dist/assets/css/bpmn-js-bpmnlint.css'

import '@/style/properties-panel.scss'

import '@/style/index.scss'
// 导入字体图标
import '@/assets/iconfont/iconfont.js'

import { setupProdMockServer } from './mockProdServer'

import print from 'vue3-print-nb'

// production mock server
if (process.env.NODE_ENV === 'production') {
  setupProdMockServer()
}

const app = createApp(App)
setupStore(app)
app.use(router as any)
usI18n(app)
app.use(MotionPlugin)
app.use(contextmenu)
app.use(VueLuckyCanvas)
app.use(print)
// app.use(ElementPlus)
app.component('SvgIcon', SvgIcon) // 全局注册svg图标组件
// 统一注册element-plus的图标，主要用于element-plus自身组件的icon绑定
for (const iconName in ElIconModules) {
  if (Reflect.has(ElIconModules, iconName)) {
    const item = ElIconModules[iconName]
    app.component(iconName, item)
  }
}
app.mount('#app')
