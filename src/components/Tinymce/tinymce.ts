﻿export const pluginsList =
  'advlist anchor autolink autosave charmap code codesample directionality emoticons fullscreen help image insertdatetime link lists media nonbreaking pagebreak preview quickbars save searchreplace table visualblocks visualchars wordcount formatpainter indent2em importword layout letterspacing'

export const toolbarList = [
  'code | undo redo | layout searchreplace formatpainter | blocks | fontfamily | fontsize | lineheight | letterspacing | forecolor | backcolor | bold italic underline strikethrough subscript superscript codeformat blockquote removeformat | alignleft aligncenter alignright alignjustify | outdent indent indent2em | bullist | numlist | link anchor image media importword | table | nonbreaking hr pagebreak | codesample template | charmap emoticons | insertdatetime | preview | fullscreen'
]
