﻿export interface FileItem {
  thumbUrl?: string
  name: string
  size: string | number
  type?: string
  percent: number
  file: File
  status?: number
  uuid: string
  responseData?: any
  index?: number
}

export interface PreviewFileItem {
  url: string
  name: string
  type: string
}
