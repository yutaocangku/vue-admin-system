﻿import { ElMessageBox } from 'element-plus'
import { transformI18n } from '@/plugins/i18n'

/**
 * 处理异常
 * @param {*} error
 */
export const httpErrorStatusHandler = (error) => {
  let message = ''
  if (error && error.response) {
    switch (error.response.status) {
      case 302:
        message = transformI18n('errors.errorCode302', true)
        break
      case 400:
        message = transformI18n('errors.errorCode400', true)
        break
      case 401:
        message = transformI18n('errors.errorCode401', true)
        break
      case 403:
        message = transformI18n('errors.errorCode403', true)
        break
      case 404:
        message = transformI18n('errors.errorCode404', true) + `${error.response.config.url}`
        break // 在正确域名下
      case 408:
        message = transformI18n('errors.errorCode408', true)
        break
      case 409:
        message = transformI18n('errors.errorCode409', true)
        break
      case 500:
        message = transformI18n('errors.errorCode500', true)
        break
      case 501:
        message = transformI18n('errors.errorCode501', true)
        break
      case 502:
        message = transformI18n('errors.errorCode502', true)
        break
      case 503:
        message = transformI18n('errors.errorCode503', true)
        break
      case 504:
        message = transformI18n('errors.errorCode504', true)
        break
      case 505:
        message = transformI18n('errors.errorCode505', true)
        break
      default:
        message = transformI18n('errors.errorUnknownMistake', true)
        break
    }
  }
  if (error.message.includes('timeout')) message = transformI18n('errors.errorTimeout', true)
  if (error.message.includes('Network')) {
    message = window.navigator.onLine ? transformI18n('errors.errorNetworkOnline', true) : transformI18n('errors.errorNetworkOffline', true)
    ElMessageBox.alert(message, transformI18n('moduleTitle.moduleTitleWarningAlert', true), {
      confirmButtonText: transformI18n('buttons.buttonAllRight', true),
      type: 'warning'
    })
  }
}
