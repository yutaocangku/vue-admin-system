import Axios, { AxiosRequestConfig, AxiosResponse, Method } from 'axios'

// 定义请求类型的数据类型
export type RequestMethods = Extract<Method, 'get' | 'post' | 'put' | 'delete' | 'patch' | 'option' | 'head'>
// 定义自定义回调中获取的数据的类型
export interface AxiosHttpResponse extends AxiosResponse {
  config: AxiosHttpRequestConfig
}
// 定义自定义回调中请求的数据类型
export interface AxiosHttpRequestConfig extends AxiosRequestConfig {
  config?: {
    showLoading?: boolean
    loadingOptions?: {
      target: object | string
      body: boolean
      fullscreen: boolean
      lock: boolean
      text: string
      spinner: string
      background: string
      'custom-class': string
    }
    cancelRepeatDisabled?: boolean
    mockEnable?: boolean
  }
  beforeRequestCallback?: (request: AxiosHttpRequestConfig) => void // 请求发送之前
  beforeResponseCallback?: (response: AxiosHttpResponse) => void // 相应返回之前
}
// 定义声明的AxiosHttp类的数据类型
export default class AxiosHttp {
  request<T>(method: RequestMethods, url: string, param?: AxiosRequestConfig, axiosConfig?: AxiosHttpRequestConfig): Promise<T>
  post<T>(url: string, params?: T, config?: AxiosHttpRequestConfig): Promise<T>
  get<T>(url: string, params?: T, config?: AxiosHttpRequestConfig): Promise<T>
}
