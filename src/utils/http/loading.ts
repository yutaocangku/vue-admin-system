﻿/**
 * 全局loading效果：合并多次loading请求，避免重复请求
 * 当调用一次startLoading，则次数+1；当次数为0时，则显示loading
 * 当调用一次endLoading，则次数-1; 当次数为0时，则结束loading
 */
import { transformI18n } from '@/plugins/i18n'
import _ from 'lodash-es'
import { storeToRefs } from 'pinia'
import { useSystemConfigStoreWithOut } from '@/store/storage/systemConfigStore'
const systemConfigStore = useSystemConfigStoreWithOut()

// 定义一个请求次数的变量，用来记录当前页面总共请求的次数
let loadingRequestCount = 0
// 初始化loading实例
let loadingInstance

// 编写一个显示loading的函数 并且记录请求次数 ++
const startLoading = (opt) => {
  if (loadingRequestCount === 0) {
    const { systemConfig } = storeToRefs(systemConfigStore)
    // 以服务的方式调用loading，默认是全屏loading，这里也可以继续封装自定义传参，控制loading的样式，具体参数见element-plus的loading组件
    const options = {
      lock: true,
      text: transformI18n('settings.settingLoadingText', true),
      background: systemConfig.value.isLight ? 'rgba(255, 255, 255, 0.7)' : 'rgba(0, 0, 0, 0.7)'
    }
    if (opt) {
      for (const key in opt) {
        options[key] = opt[key]
      }
    }
    loadingInstance = ElLoading.service(options)
  }
  loadingRequestCount++
}

// 编写一个隐藏loading的函数，并且记录请求次数 --
const endLoading = () => {
  // 以服务的方式调用的 Loading 需要异步关闭
  nextTick(() => {
    loadingRequestCount--
    loadingRequestCount = Math.max(loadingRequestCount, 0) // 保证大于等于0
    if (loadingRequestCount === 0) {
      if (loadingInstance) {
        hideLoading()
      }
    }
  })
}

// 防抖：将 300ms 间隔内的关闭 loading 合并为一次。防止连续请求时， loading闪烁的问题。
// 因为有时会碰到在一次请求完毕后又立刻又发起一个新的请求的情况（比如删除一个表格行后立刻进行刷新）
// 这种情况会造成连续 loading 两次，并且中间有一次极短的闪烁。通过防抖可以让 300ms 间隔内的 loading 合并为一次，避免闪烁的情况。
const hideLoading = _.debounce(() => {
  loadingInstance.close()
  loadingInstance = null
  loadingRequestCount = 0
}, 300)

export { startLoading, endLoading }
