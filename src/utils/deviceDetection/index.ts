﻿interface deviceInter {
  // eslint-disable-next-line no-undef
  match: Fn
}

interface BrowserInter {
  browser: string
  version: string
}

// 根据userAgent检测设备类型(手机返回true,反之)
export const deviceDetection = () => {
  const sUserAgent: deviceInter = navigator.userAgent.toLowerCase()
  // const bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
  const bIsIphoneOs = sUserAgent.match(/iphone os/i) === 'iphone os'
  const bIsMidp = sUserAgent.match(/midp/i) === 'midp'
  const bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) === 'rv:1.2.3.4'
  const bIsUc = sUserAgent.match(/ucweb/i) === 'ucweb'
  const bIsAndroid = sUserAgent.match(/android/i) === 'android'
  const bIsCE = sUserAgent.match(/windows ce/i) === 'windows ce'
  const bIsWM = sUserAgent.match(/windows mobile/i) === 'windows mobile'
  return bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM
}
// 根据浏览器窗口宽度检测设备类型
export const getDevice = () => {
  const { body } = document
  const rect = body.getBoundingClientRect()
  let device
  if (rect.width > 0) {
    if (rect.width <= 1024) {
      device = 'mobile'
    } else if (rect.width > 1024 && rect.width <= 1920) {
      device = 'desktop'
    } else {
      device = 'iMax'
    }
  }
  return device
}
// 为body标签添加设备类型属性
export const setDevice = (device) => {
  const { body } = document
  body.setAttribute('device', device)
}

// 获取浏览器型号以及版本
export const getBrowserInfo = () => {
  const ua = navigator.userAgent.toLowerCase()
  const re = /(msie|firefox|chrome|opera|version).*?([\d.]+)/
  const m = ua.match(re)
  const Sys: BrowserInter = {
    browser: m[1].replace(/version/, "'safari"),
    version: m[2]
  }

  return Sys
}
