import { transformI18n } from '@/plugins/i18n'
export const accountRule = (rule, value, callback) => {
  const reg = /^[a-zA-Z0-9_\-\@\.]{3,16}$/
  if (!value) {
    callback(new Error(transformI18n('verifies.' + '请输入账号！', true)))
  } else if (!reg.test(value)) {
    callback(new Error(transformI18n('verifies.' + '账号格式错误！', true)))
  } else {
    callback()
  }
}

export const emailRule = (rule, value, callback) => {
  const reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/
  if (!value) {
    callback(new Error(transformI18n('verifies.' + '请输入邮箱！', true)))
  } else if (!reg.test(value)) {
    callback(new Error(transformI18n('verifies.' + '邮箱格式错误！', true)))
  } else {
    callback()
  }
}

export const phoneRule = (rule, value, callback) => {
  const reg = /^1((34[0-8])|(8\d{2})|(([35][0-35-9]|4[579]|66|7[35678]|9[1389])\d{1}))\d{7}$/
  if (!value) {
    callback(new Error(transformI18n('verifies.' + '请输入手机号！', true)))
  } else if (!reg.test(value)) {
    callback(new Error(transformI18n('verifies.' + '手机号格式错误！', true)))
  } else {
    callback()
  }
}
