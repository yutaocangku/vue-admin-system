﻿import { getObjKey } from '../index'
import { addClass, removeClass } from '@/utils/operate'

// 动态设置主题色，用来切换主题色，实现项目的换肤功能 primaryColor,menuBgColor,headerBgColor,isLight,systemConfig.darkTheme
export const setThemeColor = (systemConfig) => {
  document.documentElement.style.setProperty('color-scheme', systemConfig.isLight ? 'light' : 'dark')

  document.documentElement.style.setProperty('--el-space', systemConfig.space + 'px')

  // 背景色
  document.documentElement.style.setProperty('--el-bg-color', systemConfig.isLight ? '#ffffff' : colorMix(themeColor().white, systemConfig.darkTheme, 0.935))
  document.documentElement.style.setProperty('--el-bg-color-page', systemConfig.isLight ? '#f6f8f9' : systemConfig.darkTheme)
  document.documentElement.style.setProperty('--el-bg-color-overlay', systemConfig.isLight ? '#ffffff' : colorMix(themeColor().white, systemConfig.darkTheme, 0.93))

  // 文字类颜色
  document.documentElement.style.setProperty('--el-text-color-primary', systemConfig.isLight ? '#515a6e' : colorMix(darkThemeColor(systemConfig.darkTheme).textColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.2))
  document.documentElement.style.setProperty('--el-text-color-regular', systemConfig.isLight ? '#606266' : colorMix(darkThemeColor(systemConfig.darkTheme).textColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.3))
  document.documentElement.style.setProperty('--el-text-color-secondary', systemConfig.isLight ? '#909399' : colorMix(darkThemeColor(systemConfig.darkTheme).textColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.4))
  document.documentElement.style.setProperty('--el-text-color-placeholder', systemConfig.isLight ? '#a8abb2' : colorMix(darkThemeColor(systemConfig.darkTheme).textColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.6))
  document.documentElement.style.setProperty('--el-text-color-disabled', systemConfig.isLight ? '#c0c4cc' : colorMix(darkThemeColor(systemConfig.darkTheme).textColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.7))

  // 边框类颜色
  document.documentElement.style.setProperty('--el-border-color-darker', systemConfig.isLight ? '#cdd0d6' : colorMix(darkThemeColor(systemConfig.darkTheme).borderColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.92))
  document.documentElement.style.setProperty('--el-border-color-dark', systemConfig.isLight ? '#d4d7de' : colorMix(darkThemeColor(systemConfig.darkTheme).borderColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.86))
  document.documentElement.style.setProperty('--el-border-color', systemConfig.isLight ? '#dcdfe6' : colorMix(darkThemeColor(systemConfig.darkTheme).borderColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.8))
  document.documentElement.style.setProperty('--el-border-color-light', systemConfig.isLight ? '#e4e7ed' : colorMix(darkThemeColor(systemConfig.darkTheme).borderColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.78))
  document.documentElement.style.setProperty('--el-border-color-lighter', systemConfig.isLight ? '#ebeef5' : colorMix(darkThemeColor(systemConfig.darkTheme).borderColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.78))
  document.documentElement.style.setProperty('--el-border-color-extra-light', systemConfig.isLight ? '#f2f6fc' : colorMix(darkThemeColor(systemConfig.darkTheme).borderColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.74))

  // 模块填充类颜色
  document.documentElement.style.setProperty('--el-fill-color-darker', systemConfig.isLight ? '#e6e8eb' : colorMix(darkThemeColor(systemConfig.darkTheme).fillColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.98))
  document.documentElement.style.setProperty('--el-fill-color-dark', systemConfig.isLight ? '#ebedf0' : colorMix(darkThemeColor(systemConfig.darkTheme).fillColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.96))
  document.documentElement.style.setProperty('--el-fill-color', systemConfig.isLight ? '#f0f2f5' : colorMix(darkThemeColor(systemConfig.darkTheme).fillColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.92))
  document.documentElement.style.setProperty('--el-fill-color-light', systemConfig.isLight ? '#f5f7fa' : colorMix(darkThemeColor(systemConfig.darkTheme).fillColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.88))
  document.documentElement.style.setProperty('--el-fill-color-lighter', systemConfig.isLight ? '#fafafa' : colorMix(darkThemeColor(systemConfig.darkTheme).fillColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.84))
  document.documentElement.style.setProperty('--el-fill-color-extra-light', systemConfig.isLight ? '#fafcff' : colorMix(darkThemeColor(systemConfig.darkTheme).fillColor, darkThemeColor(systemConfig.darkTheme).darkColor, 0.8))
  document.documentElement.style.setProperty('--el-fill-color-blank', systemConfig.isLight ? '#ffffff' : colorMix(themeColor().white, systemConfig.darkTheme, 0.96))

  // 设置主题色及衍生色
  document.documentElement.style.setProperty('--el-color-primary', systemConfig.primaryColor)
  document.documentElement.style.setProperty('--el-color-primary-rgb', hexToRgb(systemConfig.primaryColor))
  document.documentElement.style.setProperty('--el-color-primary-light-3', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, systemConfig.primaryColor, 0.7))
  document.documentElement.style.setProperty('--el-color-primary-light-5', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, systemConfig.primaryColor, 0.5))
  document.documentElement.style.setProperty('--el-color-primary-light-7', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, systemConfig.primaryColor, 0.3))
  document.documentElement.style.setProperty('--el-color-primary-light-8', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, systemConfig.primaryColor, 0.2))
  document.documentElement.style.setProperty('--el-color-primary-light-9', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, systemConfig.primaryColor, 0.1))
  document.documentElement.style.setProperty('--el-color-primary-dark-2', colorMix(systemConfig.isLight ? themeColor().black : themeColor().white, systemConfig.primaryColor, 0.8))

  // 设置成功色及衍生色
  document.documentElement.style.setProperty('--el-color-success', themeColor().success)
  document.documentElement.style.setProperty('--el-color-success-rgb', hexToRgb(themeColor().success))
  document.documentElement.style.setProperty('--el-color-success-light-3', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().success, 0.7))
  document.documentElement.style.setProperty('--el-color-success-light-5', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().success, 0.5))
  document.documentElement.style.setProperty('--el-color-success-light-7', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().success, 0.3))
  document.documentElement.style.setProperty('--el-color-success-light-8', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().success, 0.2))
  document.documentElement.style.setProperty('--el-color-success-light-9', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().success, 0.1))
  document.documentElement.style.setProperty('--el-color-success-dark-2', colorMix(systemConfig.isLight ? themeColor().black : themeColor().white, themeColor().success, 0.8))

  // 设置警告色及衍生色
  document.documentElement.style.setProperty('--el-color-warning', themeColor().warning)
  document.documentElement.style.setProperty('--el-color-warning-rgb', hexToRgb(themeColor().warning))
  document.documentElement.style.setProperty('--el-color-warning-light-3', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().warning, 0.7))
  document.documentElement.style.setProperty('--el-color-warning-light-5', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().warning, 0.5))
  document.documentElement.style.setProperty('--el-color-warning-light-7', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().warning, 0.3))
  document.documentElement.style.setProperty('--el-color-warning-light-8', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().warning, 0.2))
  document.documentElement.style.setProperty('--el-color-warning-light-9', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().warning, 0.1))
  document.documentElement.style.setProperty('--el-color-warning-dark-2', colorMix(systemConfig.isLight ? themeColor().black : themeColor().white, themeColor().warning, 0.8))

  // 设置危险色及衍生色
  document.documentElement.style.setProperty('--el-color-danger', themeColor().danger)
  document.documentElement.style.setProperty('--el-color-danger-rgb', hexToRgb(themeColor().danger))
  document.documentElement.style.setProperty('--el-color-danger-light-3', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().danger, 0.7))
  document.documentElement.style.setProperty('--el-color-danger-light-5', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().danger, 0.5))
  document.documentElement.style.setProperty('--el-color-danger-light-7', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().danger, 0.3))
  document.documentElement.style.setProperty('--el-color-danger-light-8', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().danger, 0.2))
  document.documentElement.style.setProperty('--el-color-danger-light-9', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().danger, 0.1))
  document.documentElement.style.setProperty('--el-color-danger-dark-2', colorMix(systemConfig.isLight ? themeColor().black : themeColor().white, themeColor().danger, 0.8))

  // 设置错误色及衍生色
  document.documentElement.style.setProperty('--el-color-error', themeColor().error)
  document.documentElement.style.setProperty('--el-color-error-rgb', hexToRgb(themeColor().error))
  document.documentElement.style.setProperty('--el-color-error-light-3', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().error, 0.7))
  document.documentElement.style.setProperty('--el-color-error-light-5', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().error, 0.5))
  document.documentElement.style.setProperty('--el-color-error-light-7', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().error, 0.3))
  document.documentElement.style.setProperty('--el-color-error-light-8', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().error, 0.2))
  document.documentElement.style.setProperty('--el-color-error-light-9', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().error, 0.1))
  document.documentElement.style.setProperty('--el-color-error-dark-2', colorMix(systemConfig.isLight ? themeColor().black : themeColor().white, themeColor().error, 0.8))

  // 设置信息色及衍生色
  document.documentElement.style.setProperty('--el-color-info', themeColor().info)
  document.documentElement.style.setProperty('--el-color-info-rgb', hexToRgb(themeColor().info))
  document.documentElement.style.setProperty('--el-color-info-light-3', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().info, 0.7))
  document.documentElement.style.setProperty('--el-color-info-light-5', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().info, 0.5))
  document.documentElement.style.setProperty('--el-color-info-light-7', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().info, 0.3))
  document.documentElement.style.setProperty('--el-color-info-light-8', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().info, 0.2))
  document.documentElement.style.setProperty('--el-color-info-light-9', colorMix(systemConfig.isLight ? themeColor().white : themeColor().black, themeColor().info, 0.1))
  document.documentElement.style.setProperty('--el-color-info-dark-2', colorMix(systemConfig.isLight ? themeColor().black : themeColor().white, themeColor().info, 0.8))

  // 侧边栏的区分设置
  if (systemConfig.menuBgColor.indexOf('_') !== -1) {
    if (systemConfig.menuBgColor.indexOf('light') !== -1) {
      document.documentElement.style.setProperty('--el-sidebar-bg-color', systemConfig.isLight ? systemConfig.menuBgColor : colorMix(themeColor().white, systemConfig.darkTheme, 0.905))
      document.documentElement.style.setProperty('--el-submenu-bg-color', systemConfig.isLight ? themeColor().light : colorMix(themeColor().white, systemConfig.darkTheme, 0.97))
    } else {
      document.documentElement.style.setProperty('--el-sidebar-bg-color', systemConfig.isLight ? systemConfig.menuBgColor : colorMix(themeColor().white, systemConfig.darkTheme, 0.905))
      document.documentElement.style.setProperty('--el-submenu-bg-color', systemConfig.isLight ? themeColor().dark : colorMix(themeColor().white, systemConfig.darkTheme, 0.97))
    }
  } else {
    document.documentElement.style.setProperty('--el-sidebar-bg-color', systemConfig.isLight ? systemConfig.menuBgColor : colorMix(themeColor().white, systemConfig.darkTheme, 0.905))
    document.documentElement.style.setProperty('--el-submenu-bg-color', systemConfig.isLight ? colorMix(themeColor().black, systemConfig.menuBgColor, 0.6) : colorMix(themeColor().white, systemConfig.darkTheme, 0.97))
  }
  document.querySelector('body')?.setAttribute('data-menu-theme', getObjKey(menuBgColorSource(), systemConfig.menuBgColor))

  // 顶栏背景色的区分设置
  document.documentElement.style.setProperty('--el-header-bg-color', systemConfig.isLight ? systemConfig.headerBgColor : colorMix(themeColor().white, systemConfig.darkTheme, 0.865))
  document.querySelector('body')?.setAttribute('data-header-theme', getObjKey(headerBgColorSource(), systemConfig.headerBgColor))

  // 为HTML标签添加主题色自定义属性
  document.querySelector('html')?.setAttribute('data-theme', getObjKey(primaryColorSource(), systemConfig.primaryColor))

  // 为html标签添加明暗主题的类名
  if (systemConfig.isLight) {
    removeClass(document.querySelector('html'), 'dark')
    addClass(document.querySelector('html'), 'light')
  } else {
    removeClass(document.querySelector('html'), 'light')
    addClass(document.querySelector('html'), 'dark')
  }
}

export const setHtmlLanguage = (language) => {
  let htmlLang = ''
  if (language === 'zh') {
    htmlLang = 'zh-Hans-CN'
  } else if (language === 'zh_tw') {
    htmlLang = 'zh-Hant-TW'
  } else {
    htmlLang = language
  }
  document.querySelector('html')?.setAttribute('lang', htmlLang)
}

// 实现颜色混合功能，用来计算主题色的色系
export const colorMix = (c1, c2, ratio) => {
  ratio = Math.max(Math.min(Number(ratio), 1), 0)
  const r1 = parseInt(c1.substring(1, 3), 16)
  const g1 = parseInt(c1.substring(3, 5), 16)
  const b1 = parseInt(c1.substring(5, 7), 16)
  const r2 = parseInt(c2.substring(1, 3), 16)
  const g2 = parseInt(c2.substring(3, 5), 16)
  const b2 = parseInt(c2.substring(5, 7), 16)
  const r = Math.round(r1 * (1 - ratio) + r2 * ratio)
  const g = Math.round(g1 * (1 - ratio) + g2 * ratio)
  const b = Math.round(b1 * (1 - ratio) + b2 * ratio)
  const r3 = ('0' + (r || 0).toString(16)).slice(-2)
  const g3 = ('0' + (g || 0).toString(16)).slice(-2)
  const b3 = ('0' + (b || 0).toString(16)).slice(-2)
  return '#' + r3 + g3 + b3
}
// 将hex格式的颜色值（#000|#000000）转化为rgba格式（rgba(0,0,0,1)）
export const hexToRgb = (color) => {
  const reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
  let sColor = color.toLowerCase()
  if (sColor && reg.test(sColor)) {
    if (sColor.length === 4) {
      let sColorNew = '#'
      for (let i = 1; i < 4; i += 1) {
        sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1))
      }
      sColor = sColorNew
    }
    // 处理六位的颜色值
    const sColorChange = []
    for (let i = 1; i < 7; i += 2) {
      sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)))
    }
    // return "rgba(" + sColorChange.join(",") + ")";
    return sColorChange.join(',')
  } else {
    return sColor
  }
}
// 主题色设置
export const primaryColorSource = () => {
  return {
    blue: themeColor().blue,
    green: themeColor().green,
    red: themeColor().red,
    pink: themeColor().pink,
    purple: themeColor().purple,
    cyan: themeColor().cyan,
    orange: themeColor().orange,
    gold: themeColor().gold,
    amber: themeColor().amber,
    glass: themeColor().glass
  }
}
// 菜单主题色设置
export const menuBgColorSource = () => {
  return {
    light: themeColor().light,
    dark: themeColor().dark,
    blue: colorMix(themeColor().black, themeColor().blue, 0.175),
    green: colorMix(themeColor().black, themeColor().green, 0.175),
    red: colorMix(themeColor().black, themeColor().red, 0.175),
    pink: colorMix(themeColor().black, themeColor().pink, 0.175),
    purple: colorMix(themeColor().black, themeColor().purple, 0.175),
    cyan: colorMix(themeColor().black, themeColor().cyan, 0.175),
    orange: colorMix(themeColor().black, themeColor().orange, 0.175),
    gold: colorMix(themeColor().black, themeColor().gold, 0.175),
    gradient_dark_sidebar1: 'gradient_dark_sidebar1',
    gradient_dark_sidebar2: 'gradient_dark_sidebar2',
    gradient_light_sidebar1: 'gradient_light_sidebar1',
    bg_dark_sidebar1: 'bg_dark_sidebar1',
    bg_light_sidebar1: 'bg_light_sidebar1'
  }
}
// header主题色设置
export const headerBgColorSource = () => {
  return {
    light: themeColor().light,
    dark: themeColor().dark,
    blue: colorMix(themeColor().white, themeColor().blue, 0.825),
    green: colorMix(themeColor().white, themeColor().green, 0.825),
    red: colorMix(themeColor().white, themeColor().red, 0.825),
    pink: colorMix(themeColor().white, themeColor().pink, 0.825),
    purple: colorMix(themeColor().white, themeColor().purple, 0.825),
    cyan: colorMix(themeColor().white, themeColor().cyan, 0.825),
    orange: colorMix(themeColor().white, themeColor().orange, 0.825),
    gold: colorMix(themeColor().white, themeColor().gold, 0.825),
    gradient_dark_header1: 'gradient_dark_header1',
    gradient_dark_header2: 'gradient_dark_header2',
    gradient_light_header1: 'gradient_light_header1',
    bg_dark_header1: 'bg_dark_header1',
    bg_light_header1: 'bg_light_header1'
  }
}

// 系统颜色源，所有颜色都从这里开始衍生
export const themeColor = () => {
  return {
    // 用来做混合使用
    white: '#ffffff', // 白色
    black: '#000000', // 黑色
    // 侧边栏、顶栏除主题色色系外的白色系、黑色系
    light: '#ffffff', // 白色
    dark: '#161b22', // 黑色
    // 主题色色系
    blue: '#3b64fb', // 蓝色1890ff
    green: '#41B584', // 绿色
    red: '#e83015', // 红色
    pink: '#ff5c93', // 粉色
    purple: '#9c27b0', // 紫色
    cyan: '#13c2c2', // 青色
    orange: '#fa541c', // 橙色
    gold: '#f2be45', // 烫金色
    amber: '#ca7a2c', // 琥珀色
    glass: '#005caf', // 琉璃
    // 信息提示类色系
    success: '#13ce66', // 成功
    warning: '#ffba00', // 警告
    danger: '#ff4d4f', // 危险
    error: '#ff4d4f', // 错误
    info: '#909399' // 信息
  }
}
// 暗黑主题颜色
export const darkThemeColor = (darkTheme) => {
  const darkThemeColor = {
    darkColor: '#1e1e1e',
    textColor: '#f0f5ff',
    borderColor: '#f5f8ff',
    fillColor: '#fafcff'
  }
  darkThemeData().forEach((item) => {
    if (darkTheme === item.darkColor) {
      darkThemeColor.darkColor = item.darkColor
      darkThemeColor.textColor = item.textColor
      darkThemeColor.borderColor = item.borderColor
      darkThemeColor.fillColor = item.fillColor
    }
  })
  return darkThemeColor
}
export const darkThemeData = () => {
  return [
    { darkColor: '#1e1e1e', textColor: '#f0f5ff', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#1e2227', textColor: '#abb2bf', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#272022', textColor: '#bbaab0', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#051b29', textColor: '#becfda', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#291d35', textColor: '#b3a5c0', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#0e1920', textColor: '#96a8b6', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#161821', textColor: '#eaf2f1', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#21252b', textColor: '#cccccc', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#161b1e', textColor: '#f2fffc', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#011627', textColor: '#d6deeb', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#191515', textColor: '#fff1f3', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#1c2128', textColor: '#adbac7', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#1e2030', textColor: '#c8d3f5', borderColor: '#f5f8ff', fillColor: '#fafcff' },
    { darkColor: '#1f1d30', textColor: '#c5c2d6', borderColor: '#f5f8ff', fillColor: '#fafcff' }
  ]
}

export const randomColor = () => {
  const colorList = [themeColor().blue, themeColor().green, themeColor().red, themeColor().pink, themeColor().purple, themeColor().cyan, themeColor().orange, themeColor().gold, themeColor().success, themeColor().warning, themeColor().danger]
  const index = Math.floor(Math.random() * colorList.length)
  return colorList[index]
}
