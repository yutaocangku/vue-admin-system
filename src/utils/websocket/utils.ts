export const socketOptions = () => {
  return {
    websocket: null,
    url: '',
    message: null,
    manual: false,
    reConnectNum: 0,
    reconnectLimit: 10,
    reconnectInterval: 3000,
    reconnectTimer: null,
    heartbeatCheck: {
      timeout: 15000,
      isInit: true,
      timeoutTimer: null,
      serverTimeoutTimer: null
    }
  }
}
