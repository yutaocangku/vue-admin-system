﻿import CircularJSON from 'circular-json'
interface ProxyStorage {
  getItem(key: string): any
  setItem(Key: string, value: string): void
  removeItem(key: string): void
  clear(): void
}

// sessionStorage operate
class SessionStorageProxy implements ProxyStorage {
  protected storage: ProxyStorage

  constructor(storageModel: ProxyStorage) {
    this.storage = storageModel
  }

  // 存
  public setItem(key: string, value: any): void {
    this.storage.setItem(key, CircularJSON.stringify(value))
  }

  // 取
  public getItem(key: string): any {
    return CircularJSON.parse(this.storage.getItem(key))
  }

  // 删
  public removeItem(key: string): void {
    this.storage.removeItem(key)
  }

  // 清空
  public clear(): void {
    this.storage.clear()
  }
}

// localStorage operate
class LocalStorageProxy extends SessionStorageProxy implements ProxyStorage {
  // eslint-disable-next-line no-useless-constructor
  constructor(localStorage: ProxyStorage) {
    super(localStorage)
  }
}

export const storageSession = new SessionStorageProxy(sessionStorage)

export const storageLocal = new LocalStorageProxy(localStorage)
