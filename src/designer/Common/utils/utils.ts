import { WinKeyboard } from '../enums/editPageEnum'
/**
 * * 设置按下键盘按键的底部展示
 * @param keyCode
 * @returns
 */
export const setKeyboardDressShow = (keyCode?: number) => {
  const code = new Map([
    [17, WinKeyboard.CTRL],
    [32, WinKeyboard.SPACE]
  ])

  const dom = document.getElementById('keyboard-dress-show')
  console.log(keyCode)
  // if (!dom) return
  if (!keyCode) {
    window.onKeySpacePressHold?.(false)
    if (dom) {
      dom.innerText = ''
    }
    return
  }
  if (keyCode && code.has(keyCode)) {
    if (keyCode === 32) window.onKeySpacePressHold?.(true)
    if (dom) {
      dom.innerText = `按下了「${code.get(keyCode)}」键`
    }
  }
}
