// 获取指定路径下的 自定义svg图标集
const getSvgIconList = (moduleFiles, pathReg) => {
  const pathList: string[] = []
  for (const path in moduleFiles) {
    pathList.push(path)
  }
  return pathList.reduce((modules: any, modulePath: string) => {
    const moduleName = modulePath.replace(pathReg, '$1')
    const value = moduleFiles[modulePath]
    modules[moduleName] = value
    return modules
  }, {})
}

const getIconSet = (type) => {
  let moduleFiles: any = []
  let pathReg
  switch (type) {
    case 'bp':
      moduleFiles = import.meta.globEager('@/designer/BpmnProcess/assets/svg/*.svg')
      console.log(moduleFiles, 'svg图标')
      pathReg = /^\.\.\/\.\.\/\.\.\/BpmnProcess\/assets\/svg\/(.*)\.\w+$/
      break
  }
  return getSvgIconList(moduleFiles, pathReg)
}

// 获取所有类型图表集的图标数据
export const getIconList = () => {
  type iconType = {
    type: string
    key: string
    value: string
  }
  const tabList = ['bp']
  const iconList = []
  tabList.forEach((item) => {
    const data = getIconSet(item)
    for (const key in data) {
      const itemIcon: iconType = {
        type: item,
        key: key,
        value: data[key].default
      }
      iconList.push(itemIcon)
    }
  })
  return iconList
}
