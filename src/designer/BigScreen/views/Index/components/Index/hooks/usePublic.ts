export default () => {
  const info = ref({
    group: {
      search: {
        name: ''
      },
      data: {
        expandNodes: [],
        list: []
      }
    },
    source: {
      breadcrumbList: [],
      search: {
        groupId: 0,
        name: '',
        pageIndex: 1,
        pageSize: 20
      },
      data: {
        total: 0,
        imagePreviewList: [],
        list: []
      }
    }
  })
  return {
    info
  }
}
