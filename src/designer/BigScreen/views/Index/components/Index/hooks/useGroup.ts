import { bigScreen } from '@/api/bigScreen'
import { treeData, getTreeNodeParents, getActiveData } from '../../utils'
import { cloneDeep } from 'lodash-es'
import useMessageBox from '@/hooks/useMessageBox'
import { successMessage, errorMessage } from '@/utils/message'
export default (info, treeRef, BigScreenGroupAddEditRef) => {
  const { messageBox } = useMessageBox()

  // 获取当前查询条件下的数据
  const getBigScreenGroupData = () => {
    const result: any = {}
    result.name = info.value.group.search.name
    result.number = Math.round(Math.random() * 80)
    bigScreen.group.list(result, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
      console.log(res)
      if (res.data && res.data.length > 0) {
        const rootGroup = [
          {
            id: 0,
            parentId: '',
            title: '全部',
            level: 1,
            active: true,
            children: []
          }
        ]
        rootGroup[0].children = res.data
        treeData(rootGroup, false, [], 0, '')
        info.value.group.data.list = rootGroup
        const activeGroup = getActiveData(info.value.group.data.list, info.value.source.search.groupId)
        treeRef.value.setCheckedKeys([activeGroup.id], false)
        info.value.source.breadcrumbList = getTreeNodeParents(cloneDeep(info.value.group.data.list), activeGroup)
        info.value.source.breadcrumbList.push(activeGroup)
        info.value.group.data.expandNodes = [activeGroup.id]
      }
    })
  }

  // 点击树结构文件夹事件
  const treeNodeClick = (v) => {
    treeRef.value.setCheckedKeys([v.id], false)
    info.value.source.search.groupId = v.id
    info.value.source.breadcrumbList = getTreeNodeParents(cloneDeep(info.value.group.data.list), v)
    info.value.source.breadcrumbList.push(v)
  }

  const filterTreeList = () => {
    treeRef.value.filter(info.value.group.search.name)
  }

  const filterTreeNodeHandler = (value, data) => {
    if (!value) return true
    return data.title.includes(value)
  }

  const nodeExpandChange = (v) => {
    info.value.group.data.expandNodes.push(v.id)
  }

  const nodeCollapseChange = (v) => {
    const index = info.value.group.data.expandNodes.findIndex((item) => item === v.id)
    if (index >= 0) {
      info.value.group.data.expandNodes.splice(index, 1)
    }
  }

  const addGroup = () => {
    BigScreenGroupAddEditRef.value.showDialog()
  }

  const editGroup = (v) => {
    BigScreenGroupAddEditRef.value.showDialog(v)
  }

  const deleteGroup = (v) => {
    const msg = '确认<span>删除</span>该分组吗？'
    const msgTips = ''
    messageBox('danger', true, msg, msgTips, () => {
      removeGroup(v)
    })
  }

  const removeGroup = (v) => {
    bigScreen.group.delete({ id: v.id }, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
      console.log(res)
      if (res.code === 200) {
        successMessage('大屏分组删除成功！')
        getBigScreenGroupData()
      } else {
        errorMessage(res.info)
      }
    })
  }

  return {
    getBigScreenGroupData,
    treeNodeClick,
    filterTreeList,
    filterTreeNodeHandler,
    nodeExpandChange,
    nodeCollapseChange,
    addGroup,
    editGroup,
    deleteGroup
  }
}
