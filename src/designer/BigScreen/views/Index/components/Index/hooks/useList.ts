import { bigScreen } from '@/api/bigScreen'
import useMessageBox from '@/hooks/useMessageBox'
import { successMessage, errorMessage } from '@/utils/message'
export default (info, router) => {
  const { messageBox } = useMessageBox()

  // 页面数据条数改变重新获取数据
  const handleSizeChange = (val: number) => {
    info.value.source.search.pageSize = val
    info.value.source.search.pageIndex = 1
    getBigScreenList()
  }
  // 当前页改变重新获取数据
  const handleCurrentChange = (val: number) => {
    info.value.source.search.pageIndex = val
    getBigScreenList()
  }

  const getBigScreenList = () => {
    const modules = import.meta.globEager('@/designer/BigScreen/assets/temp/*')
    console.log(modules)
    bigScreen.list.list(info.value.source.search, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
      console.log(res)
      if (res.data && res.data.length > 0) {
        res.data.forEach((item) => {
          const path = `../../../../../assets/temp/${item.img}.png`
          item.src = modules[path].default
          info.value.source.data.imagePreviewList.push(item.src)
        })
        info.value.source.data.list = res.data
        info.value.source.data.total = res.total
      }
    })
  }

  const addBigScreen = () => {
    const routerUrl = router.resolve({ path: '/Plugins/Designer/BigScreen/Workstation/0' })
    window.open(routerUrl.href, '_blank')
  }
  const editBigScreen = (v) => {
    const routerUrl = router.resolve({ path: '/Plugins/Designer/BigScreen/Workstation/' + v.id })
    window.open(routerUrl.href, '_blank')
  }
  const removeBigScreen = (v) => {
    const msg = '确认<span>删除</span>该大屏吗？'
    const msgTips = ''
    messageBox('danger', true, msg, msgTips, () => {
      bigScreen.list.delete({ id: v.id }, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
        console.log(res)
        if (res.code === 200) {
          successMessage('大屏删除成功！')
          getBigScreenList()
        } else {
          errorMessage(res.info)
        }
      })
    })
  }
  const previewBigScreen = (v) => {
    const routerUrl = router.resolve({ path: '/Plugins/Designer/BigScreen/Preview/' + v.id })
    window.open(routerUrl.href, '_blank')
  }
  const releaseBigScreen = (v) => {
    bigScreen.list.release({ id: v.id, status: v.status }, { config: { showLoading: false, mockEnable: true } }).then((res: any) => {
      console.log(res)
      if (res.code === 200) {
        successMessage(res.info)
        getBigScreenList()
      } else {
        errorMessage(res.info)
      }
    })
  }

  return {
    handleSizeChange,
    handleCurrentChange,
    getBigScreenList,
    addBigScreen,
    editBigScreen,
    removeBigScreen,
    previewBigScreen,
    releaseBigScreen
  }
}
