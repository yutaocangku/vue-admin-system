import { useDesignerStore } from '@/store/bigScreen/designerStore'
export default () => {
  const designerStore = useDesignerStore()

  const setZoom = (type) => {
    designerStore.setZoom(type)
  }

  const rulerChange = () => {
    designerStore.toggleRuler()
  }

  const rulerLineChange = () => {
    designerStore.toggleAuxiliaryLines()
  }

  const clearRulerLines = () => {
    designerStore.clearAuxiliaryLines()
  }

  return {
    setZoom,
    rulerChange,
    rulerLineChange,
    clearRulerLines
  }
}
