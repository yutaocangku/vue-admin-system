export default () => {
  const visible = ref(false)
  const activeType = ref('')

  const toggleSubMenu = (type) => {
    activeType.value = type
    visible.value = !visible.value
  }

  const enterHandle = (type) => {
    activeType.value = type
  }

  const handleClickOutside = (e) => {
    if (e.target && e.target.className === 'dropdown-link' && visible.value) {
      return
    }
    if (e.target && (e.target.className === 'submenu' || e.target.className === 'divided')) {
      visible.value = true
    } else {
      activeType.value = ''
      if (visible.value) {
        visible.value = false
      }
    }
  }

  return {
    visible,
    activeType,
    toggleSubMenu,
    enterHandle,
    handleClickOutside
  }
}
