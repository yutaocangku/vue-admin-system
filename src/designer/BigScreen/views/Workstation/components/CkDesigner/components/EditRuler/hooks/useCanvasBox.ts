import { storeToRefs } from 'pinia'
import { useDesignerStore } from '@/store/bigScreen/designerStore'
export default () => {
  const designStore = useDesignerStore()

  const { editCanvasConfig } = storeToRefs(designStore)
  // 计算画布大小
  const canvasBox = () => {
    const layoutDom = document.getElementById('ck-designer')
    if (layoutDom) {
      // 此处减去滚动条的宽度和高度
      const scrollW = 20
      return {
        height: layoutDom.clientHeight - scrollW,
        width: layoutDom.clientWidth - scrollW
      }
    }
    return {
      width: editCanvasConfig.value.width,
      height: editCanvasConfig.value.height
    }
  }
  return {
    canvasBox
  }
}
