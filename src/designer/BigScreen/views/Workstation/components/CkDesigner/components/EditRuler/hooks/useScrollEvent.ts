import { useDesignerStore } from '@/store/bigScreen/designerStore'
import { storeToRefs } from 'pinia'
export default (startX, startY, designerRef, canvasRef) => {
  const designStore = useDesignerStore()
  const { editCanvas } = storeToRefs(designStore)
  // 滚动条处理
  const handleScroll = () => {
    if (!designerRef.value) return
    const screensRect = designerRef.value.getBoundingClientRect()
    const canvasRect = canvasRef.value.getBoundingClientRect()
    // 标尺开始的刻度
    startX.value = (screensRect.left - canvasRect.left) / editCanvas.value.scale
    startY.value = (screensRect.top - canvasRect.top) / editCanvas.value.scale
  }

  return {
    handleScroll
  }
}
