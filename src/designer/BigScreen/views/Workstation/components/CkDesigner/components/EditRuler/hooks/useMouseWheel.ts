import { useDesignerStore } from '@/store/bigScreen/designerStore'
import { storeToRefs } from 'pinia'
export default () => {
  const designStore = useDesignerStore()
  const { setScale } = designStore
  const { editCanvas } = storeToRefs(designStore)
  // 处理鼠标拖动
  const handleWheel = (e: any) => {
    if (e.ctrlKey || e.metaKey) {
      e.preventDefault()
      let resScale = editCanvas.value.scale
      // 放大(1000%)
      if (e.wheelDelta >= 0 && editCanvas.value.scale < 10) {
        resScale = editCanvas.value.scale + 0.05
        setScale(parseFloat(resScale.toFixed(2)))
        return
      }
      // 缩小(10%)
      if (e.wheelDelta < 0 && editCanvas.value.scale > 0.1) {
        resScale = editCanvas.value.scale - 0.05
        setScale(parseFloat(resScale.toFixed(2)))
      }
    }
  }

  return {
    handleWheel
  }
}
