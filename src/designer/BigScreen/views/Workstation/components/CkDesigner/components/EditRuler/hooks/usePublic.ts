export default () => {
  const isResetDraw = ref(true)
  const startX = ref(0)
  const startY = ref(0)
  const isPressSpace = ref(false)
  const cursorStyle = ref('auto')

  return {
    isResetDraw,
    startX,
    startY,
    isPressSpace,
    cursorStyle
  }
}
