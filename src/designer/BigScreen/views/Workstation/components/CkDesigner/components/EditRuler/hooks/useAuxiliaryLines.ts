import { storeToRefs } from 'pinia'
import { useDesignerStore } from '@/store/bigScreen/designerStore'
export default () => {
  const designStore = useDesignerStore()

  const { editCanvasConfig } = storeToRefs(designStore)

  const handleCornerClick = (v) => {
    editCanvasConfig.value.auxiliaryLines.isShowReferLine = v
  }

  const linesChangeHandle = (v) => {
    editCanvasConfig.value.auxiliaryLines.lines[v.type] = v.value
  }

  return {
    handleCornerClick,
    linesChangeHandle
  }
}
