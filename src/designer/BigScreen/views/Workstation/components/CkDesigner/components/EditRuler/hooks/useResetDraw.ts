export default (isResetDraw) => {
  // 重绘标尺
  const resetDraw = () => {
    isResetDraw.value = false
    setTimeout(() => {
      isResetDraw.value = true
    }, 10)
  }

  return {
    resetDraw
  }
}
