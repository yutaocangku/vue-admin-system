export default (designerRef, containerRef, canvasBox) => {
  // 滚动居中
  const canvasCenter = () => {
    const { width: containerWidth, height: containerHeight } = containerRef.value.getBoundingClientRect()
    const { width, height } = canvasBox()

    designerRef.value.scrollLeft = containerWidth / 2 - width / 2
    designerRef.value.scrollTop = containerHeight / 2 - height / 2
  }

  return {
    canvasCenter
  }
}
