import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
import { throttle } from 'lodash-es'
import { useDesignerStore } from '@/store/bigScreen/designerStore'
export default (isPressSpace, cursorStyle, resetDraw, handleScroll, canvasCenter) => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)

  const designStore = useDesignerStore()
  const { editCanvas } = storeToRefs(designStore)

  // 处理主题变化
  watch(
    () => systemConfig.value,
    () => {
      throttle(resetDraw, 20)
    },
    {
      deep: true
    }
  )

  // 处理标尺重制大小
  watch(
    () => editCanvas.value.scale,
    (newValue, oldValue) => {
      if (oldValue !== newValue) {
        handleScroll()
        setTimeout(() => {
          canvasCenter()
        }, 100)
      }
    }
  )

  // 处理鼠标样式
  watch(
    () => isPressSpace.value,
    (newValue) => {
      cursorStyle.value = newValue ? 'grab' : 'auto'
    }
  )

  // 处理标尺显示隐藏
  watch(
    () => editCanvas.value.isRulerShow,
    (newValue, oldValue) => {
      if (oldValue !== newValue) {
        handleScroll()
        setTimeout(() => {
          // canvasCenter()
          resetDraw()
        }, 400)
      } else {
        throttle(resetDraw, 20)
      }
    }
  )
}
