import { useDesignerStore } from '@/store/bigScreen/designerStore'
import { storeToRefs } from 'pinia'
export default (router) => {
  const designStore = useDesignerStore()
  const { editCanvasConfig } = storeToRefs(designStore)

  const initData = () => {
    const id: any = parseInt(router.currentRoute.value.params.id as string)
    if (id === 0) {
      // 新建大屏
      const root = getComputedStyle(document.querySelector('html'))
      editCanvasConfig.value.background = root.getPropertyValue('--el-bg-color-page')
    } else {
      // 编辑大屏
    }
  }

  return {
    initData
  }
}
