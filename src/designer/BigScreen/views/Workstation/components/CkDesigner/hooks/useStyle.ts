import { useDesignerStore } from '@/store/bigScreen/designerStore'
import { storeToRefs } from 'pinia'
export default () => {
  const designStore = useDesignerStore()

  const { editCanvasConfig } = storeToRefs(designStore)

  // 背景
  const rangeStyle = computed(() => {
    // 设置背景色和图片背景
    const background = editCanvasConfig.value.background
    const backgroundImage = editCanvasConfig.value.backgroundImage
    const selectColor = editCanvasConfig.value.selectColor
    const backgroundColor = background || undefined

    const computedBackground = selectColor ? { background: backgroundColor } : { background: `url(${backgroundImage}) no-repeat center center / cover !important` }

    return {
      ...computedBackground,
      width: 'inherit',
      height: 'inherit'
    }
  })

  return {
    rangeStyle
  }
}
