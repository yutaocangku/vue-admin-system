const DynamicDragModules = import.meta.globEager('../components/DynamicDrag/*/index.vue')
const DynamicDragDropModules = import.meta.globEager('../components/DynamicDragDrop/*/index.vue')

// 获取组件
export const getComponent = (name) => {
  const result = { ...DynamicDragModules, ...DynamicDragDropModules }
  for (const key in result) {
    const urlSplit = key.split('/')
    if (urlSplit[urlSplit.length - 2] === name) {
      return result[key]
    }
  }
}

// 获取父级
export const getParent = (data, id) => {
  const arr = []
  recursionFindParent(data, id, arr)
  return arr
}

const recursionFindParent = (data, id, arr) => {
  for (const item of data) {
    if (item.id === id) {
      arr.push(item.id)
      return true
    }
    if (item.children && item.children.length > 0) {
      const isFind = recursionFindParent(item.children, id, arr)
      if (isFind) {
        arr.push(item.id)
        return true
      }
    }
  }
  return false
}

// 获取父级
export const getCanRemoveParentId = (data, id) => {
  const res = []
  recursionFindCanRemoveParentId(data, id, res)
  const first = res[0]
  res.splice(0, 1)
  const index = res.findIndex((item) => item.count > 1)
  if (index < 0) {
    return res[res.length - 1]
  } else {
    const result = res[index]
    res.unshift(first)
    result.childIndex = res[index].index
    return result
  }
}

const recursionFindCanRemoveParentId = (data, id, res) => {
  for (const [index, item] of data.entries()) {
    if (item.id === id) {
      res.push({
        id: item.id,
        pid: item.pid,
        index,
        count: item.children.length
      })
      return true
    }
    if (item.children && item.children.length > 0) {
      const isFind = recursionFindCanRemoveParentId(item.children, id, res)
      if (isFind) {
        res.push({
          id: item.id,
          pid: item.pid,
          index,
          count: item.children.length
        })
        return true
      }
    }
  }
  return false
}

// 获取当前拖拽放置层级id
export const getDropId = (layout, dropAsideIndex, dropColumnIndex, dropItem, dropIndex, dragType) => {
  if (layout[dropItem.config.position][dropAsideIndex].children.length === 1) {
    if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children.length === 1) {
      if (dragType === 'tab' || dragType === 'package') {
        return ''
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return ''
        } else {
          return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].id
        }
      }
    } else {
      if (dragType === 'tab') {
        return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].id
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].id
        } else {
          return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].id
        }
      }
    }
  } else {
    if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children.length === 1) {
      if (dragType === 'tab' || dragType === 'package') {
        return layout[dropItem.config.position][dropAsideIndex].id
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return layout[dropItem.config.position][dropAsideIndex].id
        } else {
          return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].id
        }
      }
    } else {
      if (dragType === 'tab') {
        return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].id
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].id
        } else {
          return layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].id
        }
      }
    }
  }
}

// 获取当前拖拽放置层级的子级类型
export const getDropLevel = (layout, dropAsideIndex, dropColumnIndex, dropItem, dropIndex, dragType) => {
  if (layout[dropItem.config.position][dropAsideIndex].children.length === 1) {
    if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children.length === 1) {
      if (dragType === 'tab' || dragType === 'package') {
        return 'aside'
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return 'aside'
        } else {
          return 'item'
        }
      }
    } else {
      if (dragType === 'tab') {
        return 'tab'
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return 'tab'
        } else {
          return 'item'
        }
      }
    }
  } else {
    if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children.length === 1) {
      if (dragType === 'tab' || dragType === 'package') {
        return 'column'
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return 'column'
        } else {
          return 'item'
        }
      }
    } else {
      if (dragType === 'tab') {
        return 'tab'
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return 'tab'
        } else {
          return 'item'
        }
      }
    }
  }
}

// 获取当前拖拽放置层级的子级索引
export const getDropIndex = (layout, dropAsideIndex, dropColumnIndex, dropItem, dropIndex, dragType) => {
  if (layout[dropItem.config.position][dropAsideIndex].children.length === 1) {
    if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children.length === 1) {
      if (dragType === 'tab' || dragType === 'package') {
        return -1
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return -1
        } else {
          return -1
        }
      }
    } else {
      if (dragType === 'tab') {
        return dropIndex
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return dropIndex
        } else {
          return -1
        }
      }
    }
  } else {
    if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children.length === 1) {
      if (dragType === 'tab' || dragType === 'package') {
        return dropColumnIndex
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return dropColumnIndex
        } else {
          return -1
        }
      }
    } else {
      if (dragType === 'tab') {
        return dropIndex
      } else {
        if (layout[dropItem.config.position][dropAsideIndex].children[dropColumnIndex].children[dropIndex].children.length === 1) {
          return dropIndex
        } else {
          return -1
        }
      }
    }
  }
}

// 获取临近放置层级的索引
export const getNearDropIndex = (nearIndex, dropIndex, type) => {
  if (type === 'before') {
    if (nearIndex < 0) {
      return dropIndex
    } else {
      if (nearIndex > dropIndex) {
        return dropIndex
      } else {
        return dropIndex - 1
      }
    }
  } else {
    if (nearIndex < 0) {
      if (nearIndex === -1) {
        return dropIndex + 1
      } else {
        return dropIndex
      }
    } else {
      if (nearIndex > dropIndex) {
        return dropIndex + 1
      } else {
        return dropIndex
      }
    }
  }
}

// 获取临近索引
export const getNearIndex = (data, dragIndex, type) => {
  if (type === 'before') {
    const nearIndex = data.findIndex((v, idx) => idx > dragIndex && v.visible)
    return nearIndex === -1 ? -2 : nearIndex
  } else {
    const res = []
    data.forEach((v, idx) => {
      if (idx < dragIndex && v.visible) {
        res.push(idx)
      }
    })
    if (res.length > 0) {
      return res[res.length - 1]
    } else {
      return -2
    }
  }
}

// 判断当前拖拽项是不是正在经过自己，且自己所在aside|column|tab|item|package是唯一项
export const isAloneSelf = (hoverColumn, dragItem) => {
  if (dragItem.config.dragType === 'tab') {
    if (dragItem.pid === hoverColumn.id && findVisibleLength(hoverColumn.children) === 1) {
      return true
    } else {
      return false
    }
  } else {
    if (dragItem.parent[dragItem.parent.length - 2] === hoverColumn.id && findVisibleLength(hoverColumn.children) === 1 && findVisibleLength(hoverColumn.children[findFirstVisible(hoverColumn.children)].children) === 1) {
      return true
    } else {
      return false
    }
  }
}

// 找到显示的个数
export const findVisibleLength = (data) => {
  let count = 0
  data.forEach((item) => {
    if (item.visible) {
      count += 1
    }
  })
  return count
}

// 找到显示的第一项索引
export const findFirstVisible = (data) => {
  let count = -1
  for (let i = 0; i < data.length; i++) {
    if (data[i].visible) {
      count = i
      break
    }
  }
  return count
}

// 找到显示的最后一项索引
export const findLastVisible = (data) => {
  let count = -1
  for (let i = 0; i < data.length; i++) {
    if (data[i].visible) {
      count = i
    }
  }
  return count
}
