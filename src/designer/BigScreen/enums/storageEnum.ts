import { nameSpace } from '@/enums/storageEnums'
export enum StorageEnum {
  // 工作台布局配置
  CK_CHART_LAYOUT_STORE = nameSpace + '_CHART_LAYOUT',
  // 工作台需要保存的数据
  CK_CHART_STORAGE_LIST = nameSpace + '_CHART_STORAGE_LIST',
  // 用户存储的图片媒体
  CK_USER_MEDIA_PHOTOS = nameSpace + '_USER_MEDIA_PHOTOS'
}
