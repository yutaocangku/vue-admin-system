import { nameSpace } from '@/enums/storageEnums'
export const packageTypeList = [
  {
    icon: 'ri:layout-line',
    title: '布局',
    type: 'layout',
    active: true,
    children: [
      {
        icon: '',
        title: '所有',
        type: 'all',
        active: true,
        children: []
      },
      {
        icon: '',
        title: '边框',
        type: 'border',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '装饰',
        type: 'decorative',
        active: false,
        children: []
      }
    ]
  },
  {
    icon: 'ri:box-3-line',
    title: '控件',
    type: 'control',
    active: false,
    children: [
      {
        icon: '',
        title: '所有',
        type: 'all',
        active: true,
        children: []
      },
      {
        icon: '',
        title: '文本',
        type: 'text',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '表单',
        type: 'form',
        active: false,
        children: []
      }
    ]
  },
  {
    icon: 'ri:bar-chart-2-line',
    title: '图表',
    type: 'chart',
    active: false,
    children: [
      {
        icon: '',
        title: '所有',
        type: 'all',
        active: true,
        children: []
      },
      {
        icon: '',
        title: '柱状图',
        type: 'bar',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '折线图',
        type: 'line',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '饼图',
        type: 'pie',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '地图',
        type: 'map',
        active: false,
        children: []
      }
    ]
  },
  {
    icon: 'ri:remixicon-line',
    title: '图标',
    type: 'icon',
    active: false,
    children: [
      {
        icon: '',
        title: '所有',
        type: 'all',
        active: true,
        children: []
      },
      {
        icon: '',
        title: 'ant design',
        type: 'antDesign',
        active: false,
        children: []
      },
      {
        icon: '',
        title: 'bootstrap',
        type: 'bootstrap',
        active: false,
        children: []
      },
      {
        icon: '',
        title: 'element plus',
        type: 'elementPlus',
        active: false,
        children: []
      },
      {
        icon: '',
        title: 'icon-font',
        type: 'iconFont',
        active: false,
        children: []
      },
      {
        icon: '',
        title: 'remix',
        type: 'remix',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '自定义',
        type: 'custom',
        active: false,
        children: []
      }
    ]
  },
  {
    icon: 'ri:open-source-line',
    title: '资源',
    type: 'resource',
    active: false,
    children: [
      {
        icon: '',
        title: '所有',
        type: 'all',
        active: true,
        children: []
      },
      {
        icon: '',
        title: '个人',
        type: 'person',
        active: false,
        children: []
      },
      {
        icon: '',
        title: '公共',
        type: 'public',
        active: false,
        children: []
      }
    ]
  }
]
