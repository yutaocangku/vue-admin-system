import EmptyXML from './EmptyXML'
import { useModelerStore } from '@/store/bpmnProcess/modelerStore'
import { isAny } from 'bpmn-js/lib/util/ModelUtil'

export function isAppendAction(element) {
  return !element || isAny(element, ['bpmn:Process', 'bpmn:Collaboration', 'bpmn:Participant', 'bpmn:SubProcess'])
}

export const createNewDiagram = async (processEngine, newXml?: string) => {
  try {
    const timestamp = Date.now()
    const newId = `Process_${timestamp}`
    const newName = `业务流程_${timestamp}`
    const xmlString = newXml || EmptyXML(newId, newName, processEngine)
    const modeler = useModelerStore().getModeler
    const { warnings } = await modeler.importXML(xmlString)
    if (warnings && warnings.length) {
      warnings.forEach((warn) => console.warn(warn))
    }
  } catch (e) {
    console.error(`[流程设计器警告]: ${typeof e === 'string' ? e : (e as Error)?.message}`)
  }
}
