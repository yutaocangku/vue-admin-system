// 创建/追加元素插件
import { CreateAppendAnythingModule } from 'bpmn-js-create-append-anything'

// ** 官方流程模拟 module
import TokenSimulationModule from 'bpmn-js-token-simulation'

// 流程图汉化
import translate from './additionalModules/translate/translate'

// 流程图校验
import lintModule from 'bpmn-js-bpmnlint'
import bpmnlint from './additionalModules/Lint/bpmnLint'

// 小地图
import minimapModule from 'diagram-js-minimap'

// 流程图设置元素颜色功能
import BpmnColorPickerModule from 'bpmn-js-color-picker'

// moddle 定义文件
import activitiModdleDescriptors from './moddleExtensions/activiti.json'
import camundaModdleDescriptors from './moddleExtensions/camunda.json'
import flowableModdleDescriptors from './moddleExtensions/flowable.json'

// camunda 官方侧边栏扩展
import { BpmnPropertiesPanelModule, BpmnPropertiesProviderModule, CamundaPlatformPropertiesProviderModule } from 'bpmn-js-properties-panel'

// 自定义 modules 扩展模块
import RewriteRenderer from './additionalModules/Renderer/RewriteRenderer'
import DingTalkRenderer from './additionalModules/Renderer/DingTalkRenderer'

export const getModelerOptions = (settings) => {
  const options: any = {
    additionalModules: [],
    moddleExtensions: {}
  }

  // 配置默认属性栏
  if (settings.panelMode === 'default') {
    options['propertiesPanel'] = {
      parent: '#bpmn-properties'
    }
    options.additionalModules.push(BpmnPropertiesPanelModule)
    options.additionalModules.push(BpmnPropertiesProviderModule)
    options.additionalModules.push(CamundaPlatformPropertiesProviderModule)
  }

  // 配置自定义渲染
  options.additionalModules.push(RewriteRenderer)
  // options.additionalModules.push(DingTalkRenderer)

  if (!settings.editor.readonly) {
    // 新增/追加元素插件
    options.additionalModules.push(CreateAppendAnythingModule)

    // 设置 lint 校验
    if (settings.editor.useLint) {
      options.additionalModules.push(lintModule)
      options['linting'] = {
        active: true,
        bpmnlint: bpmnlint
      }
    }

    // 设置小地图
    if (settings.editor.miniMap) {
      options.additionalModules.push(minimapModule)
      options['minimap'] = {
        open: true
      }
    }
  }

  // 设置汉化
  options.additionalModules.push({
    translate: ['value', translate]
  })

  // 只读模式
  if (settings.editor.readonly) {
    options.additionalModules.push({
      paletteProvider: ['value', ''], // 禁用/清空左侧工具栏
      labelEditingProvider: ['value', ''], // 禁用节点编辑
      contextPadProvider: ['value', ''], // 禁用图形菜单
      bendpoints: ['value', {}], // 禁用连线拖动
      zoomScroll: ['value', ''], // 禁用滚动
      moveCanvas: ['value', ''], // 禁用拖动整个流程图
      move: ['value', ''] // 禁用单个图形拖动
    })
  }

  // 设置键盘事件绑定
  options['keyboard'] = {
    bindTo: document
  }

  if (!settings.editor.readonly) {
    // 设置改变元素颜色功能
    options.additionalModules.push(BpmnColorPickerModule)

    // 设置流程模拟功能
    options.additionalModules.push(TokenSimulationModule)
  }

  // 设置对应的 moddle 解析配置文件
  if (!Object.keys(options.moddleExtensions).length) {
    if (settings.editor.processEngine === 'activiti') options.moddleExtensions['activiti'] = activitiModdleDescriptors
    if (settings.editor.processEngine === 'camunda') options.moddleExtensions['camunda'] = camundaModdleDescriptors
    if (settings.editor.processEngine === 'flowable') options.moddleExtensions['flowable'] = flowableModdleDescriptors
  }

  return options
}
