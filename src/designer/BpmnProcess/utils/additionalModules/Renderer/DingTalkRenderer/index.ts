import DingTalkRenderer from './DingTalkRenderer'
import { ModuleDeclaration } from 'didi'

const dingTalkRenderer: ModuleDeclaration = {
  __init__: ['bpmnRenderer'],
  bpmnRenderer: ['type', DingTalkRenderer]
}

export default dingTalkRenderer
