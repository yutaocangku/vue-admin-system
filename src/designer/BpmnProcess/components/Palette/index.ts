import * as replaceOptions from 'bpmn-js/lib/features/replace/ReplaceOptions'

export const getAllPalettes = () => {
  const result = [
    {
      visible: false,
      key: 'tools',
      actionName: null,
      target: null,
      label: '工具',
      children: [
        {
          visible: true,
          actionName: null,
          target: null,
          className: 'bpmn-icon-hand-tool',
          label: '抓手'
        },
        {
          visible: true,
          actionName: null,
          target: null,
          className: 'bpmn-icon-lasso-tool',
          label: '套索'
        },
        {
          visible: true,
          actionName: null,
          target: null,
          className: 'bpmn-icon-space-tool',
          label: '空间'
        },
        {
          visible: true,
          actionName: null,
          target: null,
          className: 'bpmn-icon-connection-multi',
          label: '全局连接'
        }
      ]
    }
  ]
  for (const key in replaceOptions) {
    result.push({
      visible: false,
      key: key,
      actionName: null,
      target: null,
      label: getCategoryTitle(key),
      children: replaceOptions[key]
    })
  }
  return result
}

export const getCategoryTitle = (key) => {
  let label = ''
  switch (key) {
    case 'BOUNDARY_EVENT':
      label = '边界事件'
      break
    case 'DATA_OBJECT_REFERENCE':
      label = '数据对象引用'
      break
    case 'DATA_STORE_REFERENCE':
      label = '数据存储引用'
      break
    case 'END_EVENT':
      label = '结束事件'
      break
    case 'EVENT_SUB_PROCESS':
      label = '子流程事件'
      break
    case 'EVENT_SUB_PROCESS_START_EVENT':
      label = '子流程事件启动事件'
      break
    case 'GATEWAY':
      label = '网关'
      break
    case 'INTERMEDIATE_EVENT':
      label = '中间事件'
      break
    case 'PARTICIPANT':
      label = '参与者'
      break
    case 'SEQUENCE_FLOW':
      label = '序列流'
      break
    case 'START_EVENT':
      label = '启动事件'
      break
    case 'START_EVENT_SUB_PROCESS':
      label = '子流程启动事件'
      break
    case 'SUBPROCESS_EXPANDED':
      label = '子流程已展开'
      break
    case 'TASK':
      label = '任务'
      break
    case 'TRANSACTION':
      label = '事务'
      break
  }
  return label
}

export const customPalettes = () => {
  return [
    { group: 'tools', type: 'bpmn:HandTool', className: 'bpmn-icon-hand-tool', title: '激活抓手工具', visible: true },
    { group: 'tools', type: 'bpmn:LassoTool', className: 'bpmn-icon-lasso-tool', title: '激活套索工具', visible: true },
    { group: 'tools', type: 'bpmn:SpaceTool', className: 'bpmn-icon-space-tool', title: '激活创建/删除空间工具', visible: true },
    { group: 'tools', type: 'bpmn:GlobalConnectTool', className: 'bpmn-icon-connection-multi', title: '激活全局连接工具', visible: true },
    { group: 'event', type: 'bpmn:StartEvent', className: 'bpmn-icon-start-event-none', title: '创建开始事件', visible: true },
    { group: 'event', type: 'bpmn:IntermediateThrowEvent', className: 'bpmn-icon-intermediate-event-none', title: '创建中间/边界事件', visible: true },
    { group: 'event', type: 'bpmn:EndEvent', className: 'bpmn-icon-end-event-none', title: '创建结束事件', visible: true },
    { group: 'gateway', type: 'bpmn:ExclusiveGateway', className: 'bpmn-icon-gateway-none', title: '创建网关', visible: true },
    { group: 'activity', type: 'bpmn:Task', className: 'bpmn-icon-task', title: '创建任务', visible: true },
    { group: 'data-object', type: 'bpmn:DataObjectReference', className: 'bpmn-icon-data-object', title: '创建数据对象引用', visible: true },
    { group: 'data-store', type: 'bpmn:DataStoreReference', className: 'bpmn-icon-data-store', title: '创建数据存储引用', visible: true },
    { group: 'activity', type: 'bpmn:SubprocessExpanded', className: 'bpmn-icon-subprocess-expanded', title: '创建拓展子过程', visible: true },
    { group: 'collaboration', type: 'bpmn:Participant', className: 'bpmn-icon-participant', title: '创建池/参与者', visible: true },
    { group: 'artifact', type: 'bpmn:Group', className: 'bpmn-icon-group', title: '创建组', visible: true },
    { group: 'create', type: 'bpmn:Create', className: 'bp:bpmn-icon-more', title: '创建元素', visible: true }
  ]
}
