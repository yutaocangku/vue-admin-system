import { getFormFieldsTypeList } from '../../../../utils/formSource'
const useDrawer = (visible, form, formFieldsTypeList, FormFieldsRef) => {
  const DEFINED_TYPE_VALUES = ['boolean', 'date', 'enum', 'long', 'string', undefined]
  const showDrawer = (v) => {
    console.log(v, '表单字段数据')
    console.log(form.value, '表单字段默认数据')
    if (v) {
      form.value.id = v.id
      form.value.label = v.label
      form.value.defaultValue = v.defaultValue
      form.value.type = DEFINED_TYPE_VALUES.includes(v.type) ? v.type : 'custom'
      if (form.value.type === 'custom') {
        form.value.customType = v.type
      }
      if (form.value.type === 'enum' && v.values && v.values.length > 0) {
        form.value.value = v.values
      }
      if (v.properties && v.properties.values && v.properties.values.length > 0) {
        form.value.property = v.properties.values
      }
      if (v.validation && v.validation.constraints && v.validation.constraints.length > 0) {
        form.value.constraint = v.validation.constraints
      }
    }
    visible.value = true
    formFieldsTypeList.value = getFormFieldsTypeList()
  }
  // 关闭抽屉
  const cancel = () => {
    FormFieldsRef.value.resetFields()
    form.value = {
      id: '',
      type: '',
      label: '',
      customType: '',
      defaultValue: '',
      value: [],
      constraint: [],
      property: []
    }
    visible.value = false
  }
  return {
    showDrawer,
    cancel
  }
}
export default useDrawer
