import useMessageBox from '@/hooks/useMessageBox'
const useFieldProperty = (form, editIndex, FieldPropertyAddEditRef) => {
  const { messageBox } = useMessageBox()

  const addFieldProperty = () => {
    editIndex.value = -1
    FieldPropertyAddEditRef.value.showDialog()
  }

  const editFieldProperty = (row, index) => {
    editIndex.value = index
    FieldPropertyAddEditRef.value.showDialog(row)
  }

  const deleteFieldPropertyRow = (index) => {
    const msg = '确认<span>删除</span>该字段属性吗？'
    const msgTips = ''
    messageBox('danger', true, msg, msgTips, () => {
      form.value['property'].splice(index, 1)
    })
  }
  const fieldPropertyChange = (v) => {
    if (editIndex.value === -1) {
      form.value['property'].push(v)
    } else {
      form.value['property'].splice(editIndex.value, 1, v)
    }
  }

  return {
    addFieldProperty,
    editFieldProperty,
    deleteFieldPropertyRow,
    fieldPropertyChange
  }
}
export default useFieldProperty
