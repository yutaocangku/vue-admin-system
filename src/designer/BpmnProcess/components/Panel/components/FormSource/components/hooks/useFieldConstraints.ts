import useMessageBox from '@/hooks/useMessageBox'
const useFieldConstraints = (form, editIndex, FieldConstraintsAddEditRef) => {
  const { messageBox } = useMessageBox()

  const addFieldConstraints = () => {
    editIndex.value = -1
    FieldConstraintsAddEditRef.value.showDialog()
  }

  const editFieldConstraints = (row, index) => {
    editIndex.value = index
    FieldConstraintsAddEditRef.value.showDialog(row)
  }

  const deleteFieldConstraintsRow = (index) => {
    const msg = '确认<span>删除</span>该约束条件吗？'
    const msgTips = ''
    messageBox('danger', true, msg, msgTips, () => {
      form.value['constraint'].splice(index, 1)
    })
  }
  const fieldConstraintsChange = (v) => {
    if (editIndex.value === -1) {
      form.value['constraint'].push(v)
    } else {
      form.value['constraint'].splice(editIndex.value, 1, v)
    }
  }

  return {
    addFieldConstraints,
    editFieldConstraints,
    deleteFieldConstraintsRow,
    fieldConstraintsChange
  }
}
export default useFieldConstraints
