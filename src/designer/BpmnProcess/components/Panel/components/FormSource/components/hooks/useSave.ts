const useSave = (emit, form, FormFieldsRef, cancel) => {
  // 保存表单字段数据
  const save = async () => {
    await FormFieldsRef.value.validate((validate) => {
      if (validate) {
        console.log(form.value, '表单提交数据')
        const result = {
          id: form.value.id,
          type: form.value.type === 'custom' ? form.value.customType : form.value.type,
          label: form.value.label
        }
        if (form.value.defaultValue !== '') {
          result['defaultValue'] = form.value.defaultValue
        }
        if (form.value.value && form.value.value.length > 0) {
          result['value'] = form.value.value
        }
        if (form.value.constraint && form.value.constraint.length > 0) {
          result['constraint'] = form.value.constraint
        }
        if (form.value.property && form.value.property.length > 0) {
          result['property'] = form.value.property
        }
        emit('save', result)
        cancel()
      }
    })
  }

  return {
    save
  }
}
export default useSave
