const usePublic = () => {
  const visible = ref(false)
  const formFieldsTypeList = ref([])
  const form = ref({
    id: '',
    label: '',
    type: '',
    customType: '',
    defaultValue: '',
    value: [],
    constraint: [],
    property: []
  })
  const editIndex = ref(-1)
  return {
    visible,
    formFieldsTypeList,
    form,
    editIndex
  }
}
export default usePublic
