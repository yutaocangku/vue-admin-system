import useMessageBox from '@/hooks/useMessageBox'
const useFieldEnumValue = (form, editIndex, FieldEnumValueAddEditRef) => {
  const { messageBox } = useMessageBox()

  const addFieldEnumValue = () => {
    editIndex.value = -1
    FieldEnumValueAddEditRef.value.showDialog()
  }

  const editFieldEnumValue = (row, index) => {
    editIndex.value = index
    FieldEnumValueAddEditRef.value.showDialog(row)
  }

  const deleteFieldEnumValueRow = (index) => {
    const msg = '确认<span>删除</span>该枚举值吗？'
    const msgTips = ''
    messageBox('danger', true, msg, msgTips, () => {
      form.value['value'].splice(index, 1)
    })
  }
  const fieldEnumValueChange = (v) => {
    if (editIndex.value === -1) {
      form.value['value'].push(v)
    } else {
      form.value['value'].splice(editIndex.value, 1, v)
    }
  }

  return {
    addFieldEnumValue,
    editFieldEnumValue,
    deleteFieldEnumValueRow,
    fieldEnumValueChange
  }
}
export default useFieldEnumValue
