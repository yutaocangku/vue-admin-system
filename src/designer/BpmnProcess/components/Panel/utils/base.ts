import { useModelerStore } from '@/store/bpmnProcess/modelerStore'
import { useSettingStore } from '@/store/bpmnProcess/settingStore'
import { getBusinessObject, is } from 'bpmn-js/lib/util/ModelUtil'

export function hasProcessRef(element) {
  return is(element, 'bpmn:Participant') && element.businessObject.get('processRef')
}

export function hasProcessExecutable(element) {
  return is(element, 'bpmn:Process') || hasProcessRef(element)
}

export function hasVersionTag(element) {
  const businessObject = getBusinessObject(element)
  return is(element, 'bpmn:Process') || (is(element, 'bpmn:Participant') && businessObject.get('processRef'))
}

export function getDescriptionValue(element) {
  const prefix = useSettingStore().editor.processEngine
  return element.businessObject.get(`${prefix}:description`)
}

export function setDescriptionValue(element, value) {
  const prefix = useSettingStore().editor.processEngine
  const modeling = useModelerStore().getModeling
  modeling?.updateProperties(element, { [`${prefix}:description`]: value })
}

export function getProcessNameValue(element) {
  const process = element.businessObject.get('processRef')
  return process.get('name')
}

export function setProcessNameValue(element, value) {
  const modeling = useModelerStore().getModeling
  const process = element.businessObject.get('processRef')
  modeling.updateModdleProperties(element, process, {
    name: value
  })
}

export function getProcessIdValue(element) {
  const process = element.businessObject.get('processRef')
  return process.get('id')
}

export function setProcessIdValue(element, value) {
  const modeling = useModelerStore().getModeling
  const process = element.businessObject.get('processRef')
  modeling.updateModdleProperties(element, process, {
    id: value
  })
}

// 获取流程表是否可执行
export function getProcessExecutable(element): boolean {
  if (is(element, 'bpmn:Participant')) {
    const process = element.businessObject.get('processRef')
    return process.get('isExecutable')
  } else {
    return !!element.businessObject.isExecutable
  }
}

// 设置流程表是否可执行
export function setProcessExecutable(element, value: boolean) {
  const modeling = useModelerStore().getModeling
  if (is(element, 'bpmn:Participant')) {
    const process = element.businessObject.get('processRef')
    modeling.updateModdleProperties(element, process, {
      isExecutable: value
    })
  } else {
    modeling.updateProperties(element, {
      isExecutable: value
    })
  }
}

// 获取流程表版本标签
export function getProcessVersionTag(element): string | undefined {
  const prefix = useSettingStore().editor.processEngine
  const process = getProcess(element)

  return process.get(`${prefix}:versionTag`) || ''
}

// 设置流程表版本标签
export function setProcessVersionTag(element, value: string) {
  const modeling = useModelerStore().getModeling
  const prefix = useSettingStore().editor.processEngine
  const process = getProcess(element)

  modeling.updateModdleProperties(element, process, {
    [`${prefix}:versionTag`]: value
  })
}

function getProcess(element) {
  return is(element, 'bpmn:Process') ? getBusinessObject(element) : getBusinessObject(element).get('processRef')
}

// 获取子流程状态
export function getSubProcessExpend(element) {
  return element.di?.isExpanded
}

// 设置子流程状态
export function setSubProcessExpand(element) {
  const modeling = useModelerStore().getModeling
  modeling.toggleCollapse(element)
}
