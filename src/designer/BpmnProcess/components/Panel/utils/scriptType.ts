import { useModelerStore } from '@/store/bpmnProcess/modelerStore'
import { useSettingStore } from '@/store/bpmnProcess/settingStore'

export function createScript(props) {
  const prefix = useSettingStore().editor.processEngine
  const moddle = useModelerStore().getModdle
  const { scriptFormat, scriptType, value, resource } = props

  if (scriptType === 'inline') {
    return moddle.create(`${prefix}:Script`, { scriptFormat, value })
  } else {
    return moddle.create(`${prefix}:Script`, { scriptFormat, resource })
  }
}

export function getScriptType(script): string {
  if (script.get('resource')) {
    return 'external'
  }
  if (script.get('value')) {
    return 'inline'
  }
  return 'none'
}
