import { getLinkEventDefinition } from './bpmnPropertyUtils/eventDefinition'
import { useModelerStore } from '@/store/bpmnProcess/modelerStore'

export function getLinkName(element) {
  const linkEventDefinition = getLinkEventDefinition(element)
  return linkEventDefinition.get('name')
}
export function setLinkName(element, value) {
  const modeling = useModelerStore().getModeling
  const linkEventDefinition = getLinkEventDefinition(element)
  modeling.updateModdleProperties(element, linkEventDefinition, { name: value })
}
