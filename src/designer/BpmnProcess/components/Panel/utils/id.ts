import { useModelerStore } from '@/store/bpmnProcess/modelerStore'
import { isIdValid } from '../../../utils/validator'
import { warnMessage } from '@/utils/message'
import Ids from 'ids'

export function getIdValue(element): string {
  return element.businessObject.id
}

export function setIdValue(element, value: string) {
  const errorMsg = isIdValid(element.businessObject, value)

  if (errorMsg && errorMsg.length) {
    return warnMessage(errorMsg)
  }

  const modeling = useModelerStore().getModeling

  modeling.updateProperties(element, {
    id: value
  })
}

export function nextId(prefix) {
  const ids = new Ids([32, 32, 1])
  return ids.nextPrefixed(prefix)
}
