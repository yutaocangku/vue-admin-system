import { useSettingStore } from '@/store/bpmnProcess/settingStore'
import { getBusinessObject, is } from 'bpmn-js/lib/util/ModelUtil'
import { useModelerStore } from '@/store/bpmnProcess/modelerStore'

export function getTasklistValue(element) {
  const prefix = useSettingStore().editor.processEngine
  const businessObject = getBusinessObject(element)

  return businessObject.get(`${prefix}:isStartableInTasklist`)
}

export function setTasklistValue(element, value) {
  const prefix = useSettingStore().editor.processEngine
  const modeling = useModelerStore().getModeling
  const businessObject = getBusinessObject(element)
  modeling.updateModdleProperties(element, businessObject, {
    [`${prefix}:isStartableInTasklist`]: value
  })
}

export function isTasklist(element): boolean {
  const businessObject = getBusinessObject(element)
  return is(element, 'bpmn:Process') || (is(element, 'bpmn:Participant') && businessObject.get('processRef'))
}
