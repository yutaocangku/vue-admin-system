import { useSettingStore } from '@/store/bpmnProcess/settingStore'
import { getBusinessObject, is } from 'bpmn-js/lib/util/ModelUtil'
import { useModelerStore } from '@/store/bpmnProcess/modelerStore'

export function isUserAssignmentSupported(element) {
  const prefix = useSettingStore().editor.processEngine
  return is(element, `${prefix}:Assignable`)
}
export function getUserAssignmentData(element) {
  const prefix = useSettingStore().editor.processEngine
  const businessObject = getBusinessObject(element)
  return {
    assignee: businessObject.get(`${prefix}:assignee`) || '',
    candidateUsers: businessObject.get(`${prefix}:candidateUsers`) || '',
    candidateGroups: businessObject.get(`${prefix}:candidateGroups`) || '',
    dueDate: businessObject.get(`${prefix}:dueDate`) || '',
    followUpDate: businessObject.get(`${prefix}:followUpDate`) || '',
    priority: businessObject.get(`${prefix}:priority`) || ''
  }
}
export function setValue(element, type, value) {
  const prefix = useSettingStore().editor.processEngine
  const modeling = useModelerStore().getModeling
  const businessObject = getBusinessObject(element)
  modeling.updateModdleProperties(element, businessObject, { [`${prefix}:${type}`]: value })
}
