export function getScriptTypeList() {
  return [
    { label: '外部资源', value: 'external' },
    { label: '内联脚本', value: 'inline' }
  ]
}
