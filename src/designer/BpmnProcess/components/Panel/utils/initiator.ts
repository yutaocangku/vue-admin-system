import { useSettingStore } from '@/store/bpmnProcess/settingStore'
import { useModelerStore } from '@/store/bpmnProcess/modelerStore'
import { getBusinessObject, is } from 'bpmn-js/lib/util/ModelUtil'

// //////// only in bpmn:StartEvent
export function getInitiatorValue(element): string | undefined {
  const prefix = useSettingStore().editor.processEngine
  const businessObject = getBusinessObject(element)

  return businessObject.get(`${prefix}:initiator`)
}
export function setInitiatorValue(element, value: string | undefined) {
  const prefix = useSettingStore().editor.processEngine
  const modeling = useModelerStore().getModeling
  const businessObject = getBusinessObject(element)
  modeling.updateModdleProperties(element, businessObject, {
    [`${prefix}:initiator`]: value
  })
}

export function isStartInitializable(element): boolean {
  const prefix = useSettingStore().editor.processEngine
  return is(element, `${prefix}:Initiator`) && !is(element.parent, 'bpmn:SubProcess')
}

export function isUserAssignmentSupported(element) {
  return is(element, `${useSettingStore().editor.processEngine}:Assignable`)
}
