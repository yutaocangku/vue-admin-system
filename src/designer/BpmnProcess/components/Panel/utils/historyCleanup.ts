import { useSettingStore } from '@/store/bpmnProcess/settingStore'
import { getBusinessObject, is } from 'bpmn-js/lib/util/ModelUtil'
import { useModelerStore } from '@/store/bpmnProcess/modelerStore'

export function getHistoryTimeToLiveValue(element): string | undefined {
  const prefix = useSettingStore().editor.processEngine
  const businessObject = getBusinessObject(element)

  return businessObject.get(`${prefix}:historyTimeToLive`)
}

export function setHistoryTimeToLiveValue(element, value: string | undefined) {
  const prefix = useSettingStore().editor.processEngine
  const modeling = useModelerStore().getModeling
  const businessObject = getBusinessObject(element)
  modeling.updateModdleProperties(element, businessObject, {
    [`${prefix}:historyTimeToLive`]: value
  })
}

export function isHistoryCleanup(element): boolean {
  const businessObject = getBusinessObject(element)
  return is(element, 'bpmn:Process') || (is(element, 'bpmn:Participant') && businessObject.get('processRef'))
}
