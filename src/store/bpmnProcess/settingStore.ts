import { defineStore } from 'pinia'
import { store } from '../index'

export const useSettingStore = defineStore('setting', {
  state: () => {
    return {
      toolbar: true,
      palette: true,
      panel: true,
      panelMode: 'custom',
      editor: {
        readonly: false,
        miniMap: true,
        useLint: false,
        processEngine: 'camunda'
      }
    }
  },
  actions: {
    updateConfiguration(conf) {
      this.$state = { ...this.$state, ...conf }
    }
  }
})

export const useSettingStoreWithOut = () => {
  return useSettingStore(store)
}
