import { defineStore } from 'pinia'
import { store } from '../index'

export const useModelerStore = defineStore('modeler', {
  state: () => {
    return {
      modeler: null,
      moddle: null,
      modeling: null,
      canvas: null,
      elementRegistry: null,
      activeElement: null,
      activeElementId: null,
      scale: 1,
      canRedo: false,
      canUndo: false,
      xml: null
    }
  },
  getters: {
    getActive: (state) => state.activeElement,
    getActiveId: (state) => state.activeElementId,
    getModeler: (state) => state.modeler,
    getModdle: (state) => state.moddle,
    getModeling: (state) => state.modeling,
    getCanvas: (state) => state.canvas,
    getElRegistry: (state) => state.elementRegistry
  },
  actions: {
    setModeler(modeler) {
      this.modeler = modeler
      if (modeler) {
        this.modeling = modeler.get('modeling')
        this.moddle = modeler.get('moddle')
        this.canvas = modeler.get('canvas')
        this.elementRegistry = modeler.get('elementRegistry')
      } else {
        this.modeling = this.moddle = this.canvas = this.elementRegistry = null
      }
    },
    setActiveElement(element) {
      this.activeElement = element
      this.activeElementId = element.businessObject.id
    }
  }
})

export const useModelerStoreWithOut = () => {
  return useModelerStore(store)
}
