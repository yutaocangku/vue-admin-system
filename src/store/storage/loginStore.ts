import { acceptHMRUpdate, defineStore } from 'pinia'
import { store } from '../index'
import { storageLocal } from '@/utils/storage'
import { decodeAES, encodeAES, aesKey, iv } from '@/utils/aesRsa'
import { StorageEnum } from '@/enums/storageEnums'

export const useLoginStore = defineStore('login', () => {
  const { CK_SIGN_COUNTDOWN_STORE, CK_RECOVER_COUNTDOWN_STORE, CK_REGISTER_COUNTDOWN_STORE, CK_LOGIN_TEMPLATE_STORE, CK_REMEMBER_PASSWORD_STORE } = StorageEnum

  const activeLoginTemplate = ref('1')
  const accountInfo = ref(null)

  const getCountdownCache = (store) => {
    return decodeAES(storageLocal.getItem(store), aesKey, iv)
  }

  const setCountdownCache = (store, value) => {
    storageLocal.setItem(store, encodeAES(value, aesKey, iv))
  }

  const removeCountdownCache = (store) => {
    storageLocal.removeItem(store)
  }

  const getSignCountdownCache = () => {
    return getCountdownCache(CK_SIGN_COUNTDOWN_STORE)
  }

  const setSignCountdownCache = (res) => {
    setCountdownCache(CK_SIGN_COUNTDOWN_STORE, res)
  }

  const removeSignCountdownCache = () => {
    removeCountdownCache(CK_SIGN_COUNTDOWN_STORE)
  }

  const getRecoverCountdownCache = () => {
    return getCountdownCache(CK_RECOVER_COUNTDOWN_STORE)
  }

  const setRecoverCountdownCache = (res) => {
    setCountdownCache(CK_RECOVER_COUNTDOWN_STORE, res)
  }

  const removeRecoverCountdownCache = () => {
    removeCountdownCache(CK_RECOVER_COUNTDOWN_STORE)
  }

  const getRegisterCountdownCache = () => {
    return getCountdownCache(CK_REGISTER_COUNTDOWN_STORE)
  }

  const setRegisterCountdownCache = (res) => {
    setCountdownCache(CK_REGISTER_COUNTDOWN_STORE, res)
  }

  const removeRegisterCountdownCache = () => {
    removeCountdownCache(CK_REGISTER_COUNTDOWN_STORE)
  }

  const getLoginTemplateCache = () => {
    if (storageLocal.getItem(CK_LOGIN_TEMPLATE_STORE)) {
      activeLoginTemplate.value = decodeAES(storageLocal.getItem(CK_LOGIN_TEMPLATE_STORE), aesKey, iv)
    } else {
      activeLoginTemplate.value = '1'
      setLoginTemplateCache()
    }
  }

  const setLoginTemplateCache = () => {
    storageLocal.setItem(CK_LOGIN_TEMPLATE_STORE, encodeAES(activeLoginTemplate.value, aesKey, iv))
  }

  const removeLoginTemplateCache = () => {
    storageLocal.removeItem(CK_LOGIN_TEMPLATE_STORE)
  }

  const getRememberPasswordCache = () => {
    if (storageLocal.getItem(CK_REMEMBER_PASSWORD_STORE)) {
      accountInfo.value = decodeAES(storageLocal.getItem(CK_REMEMBER_PASSWORD_STORE), aesKey, iv)
    }
  }

  const setRememberPasswordCache = () => {
    storageLocal.setItem(CK_REMEMBER_PASSWORD_STORE, encodeAES(accountInfo.value, aesKey, iv))
  }

  const removeRememberPasswordCache = () => {
    storageLocal.removeItem(CK_REMEMBER_PASSWORD_STORE)
    setTimeout(() => {
      accountInfo.value = null
    }, 100)
  }

  getLoginTemplateCache()
  getRememberPasswordCache()

  return {
    activeLoginTemplate,
    accountInfo,
    getSignCountdownCache,
    setSignCountdownCache,
    removeSignCountdownCache,
    getRecoverCountdownCache,
    setRecoverCountdownCache,
    removeRecoverCountdownCache,
    getRegisterCountdownCache,
    setRegisterCountdownCache,
    removeRegisterCountdownCache,
    getLoginTemplateCache,
    setLoginTemplateCache,
    removeLoginTemplateCache,
    getRememberPasswordCache,
    setRememberPasswordCache,
    removeRememberPasswordCache
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useLoginStore, import.meta.hot))
}

export const useLoginStoreWithOut = () => {
  return useLoginStore(store)
}
