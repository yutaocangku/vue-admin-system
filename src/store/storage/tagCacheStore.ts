import { acceptHMRUpdate, defineStore } from 'pinia'
import { store } from '../index'
import { storageSession, storageLocal } from '@/utils/storage'
import { decodeAES, encodeAES, aesKey, iv } from '@/utils/aesRsa'
import { cloneDeep } from 'lodash-es'
import { StorageEnum } from '@/enums/storageEnums'
import { storeToRefs } from 'pinia'
import { useSystemConfigStoreWithOut } from '@/store/storage/systemConfigStore'
import { delAliveRoutes } from '@/router/utils/keepAliveOperate'

export const useTagCacheStore = defineStore('tagCache', () => {
  const { CK_ALL_TAG_STORE, CK_FIXED_TAG_STORE } = StorageEnum
  const systemConfigStore = useSystemConfigStoreWithOut()
  const { systemConfig } = storeToRefs(systemConfigStore)

  const tagList = ref([])
  /**
   * 固定标签缓存规则：系统确定的固定标签，不缓存在fixedTagList中，只有用户设置的固定标签需要被缓存，两者固定后，其数据字段isFixed均为true
   * 通过判断isFixed字段为true，以及是否缓存在fixedTagList中，来判断是系统固定标签，还是用户固定标签
   **/
  const fixedTagList = ref([])

  // 获取默认缓存标签
  const getAllTagCache = () => {
    if (storageSession.getItem(CK_ALL_TAG_STORE)) {
      tagList.value = JSON.parse(decodeAES(storageSession.getItem(CK_ALL_TAG_STORE), aesKey, iv))
    } else {
      tagList.value = []
    }
  }

  // 标签缓存
  const setAllTagCache = () => {
    if (systemConfig.value.isTagCache) {
      storageSession.setItem(CK_ALL_TAG_STORE, encodeAES(tagList.value, aesKey, iv))
    }
  }

  // 移除所有缓存标签
  const removeAllTagCache = () => {
    tagList.value = []
    if (systemConfig.value.isTagCache) {
      storageSession.removeItem(CK_ALL_TAG_STORE)
    }
  }

  // 动态添加标签栏路由信息
  const dynamicRouteTag = (value) => {
    // 获取被缓存的标签页数据
    if (value.name === 'redirect') return
    const hasValue = tagList.value.some((item) => {
      return item.path === value.fullPath
    })
    const tagRoute = {
      path: '',
      name: '',
      meta: {}
    }
    tagRoute.path = value.fullPath
    tagRoute.name = value.name
    tagRoute.meta = cloneDeep(value.meta)
    if (hasValue) {
      updateTagMeta({ value: tagRoute })
    }
    if (!hasValue && value.meta.showTag && value.name !== 'Layout') {
      tagOperateHandle({ mode: 'push', value: tagRoute })
    }
  }

  // 删除动态标签页
  const deleteDynamicTag = (route, router, currentPath: any, mode?: string, toPath?: string) => {
    // 存放被删除的缓存路由
    let delAliveRouteList: any = []
    // 获取当前所在标签页在标签页缓存数据中的索引值，通过索引值以及删除类型，对数据进行删除操作
    const valueIndex: number = tagList.value.findIndex((item: any) => {
      return item.path === currentPath
    })
    let isCurrentTagFixed = false
    tagList.value.forEach((item) => {
      if (item.path === currentPath) {
        isCurrentTagFixed = item.meta.isFixed
      }
    })
    // 根据当前mode类型，执行标签页缓存数据操作方法
    if (mode === 'other') {
      // 删除其他路由
      delAliveRouteList = tagList.value.filter((item, index) => {
        return !item.meta.isFixed && index !== valueIndex
      })
      tagOperateHandle({ mode: 'equal', value: valueIndex })
    } else if (mode === 'left') {
      // 删除左侧路由
      delAliveRouteList = tagList.value.filter((item, index) => {
        return !item.meta.isFixed && index < valueIndex
      })
      tagOperateHandle({ mode: 'left', value: valueIndex })
    } else if (mode === 'right') {
      // 删除右侧路由
      delAliveRouteList = tagList.value.filter((item, index) => {
        return !item.meta.isFixed && index > valueIndex
      })
      tagOperateHandle({ mode: 'right', value: valueIndex })
    } else if (mode === 'all') {
      // 删除所有路由
      delAliveRouteList = tagList.value.filter((item) => {
        return !item.meta.isFixed
      })
      tagOperateHandle({ mode: 'all' })
    } else {
      // 删除当前路由
      // 从当前匹配到的路径中删除
      delAliveRouteList = tagList.value.filter((item, index) => {
        return !item.meta.isFixed && index === valueIndex
      })
      tagOperateHandle({ mode: 'current', value: valueIndex })
    }
    // 在标签页缓存数据做了删除处理后，还需要提取数据中最后一个路由信息，用于做当前展示路由页面
    const currentRouteFuc = tagOperateHandle({ mode: 'slice', router: router })
    const keepAliveNameList = []
    tagList.value.forEach((item) => {
      if (item.meta.showTag && item.meta.showTag > 1) {
        if (!keepAliveNameList.includes(item.name)) {
          keepAliveNameList.push(item.name)
        }
      }
    })

    // 删除缓存路由
    delAliveRoutes(delAliveRouteList, keepAliveNameList)
    // 当前路由页面是点击事件依据的标签路由（右键点击的标签路由或是下拉框事件时的当前路由）
    if (currentPath === route.fullPath) {
      // 删除的是当前路由左侧、右侧、其它的标签路由，此时当前路由不会发生变化
      if (mode === 'left' || mode === 'right' || mode === 'other') return
      // 当前标签页是非固定标签页时，才会出现全部、当前情况被删除当前标签页，此时需要跳转到最后一个路由
      if (!isCurrentTagFixed) {
        nextTick(() => {
          if (toPath) {
            router.push(toPath)
          } else {
            if (tagList.value.length === 0) {
              router.push('/redirect' + currentRouteFuc()[0].path)
            } else {
              router.push(currentRouteFuc()[0].path)
            }
          }
        })
      }
    } else {
      // 这里主要针对右键菜单不是在当前路由上激活，会发生将当前路由删除的情况
      // 如果标签页缓存数据不存在了，则直接返回
      if (!tagList.value.length) return
      // 否则将当前激活的路由找出
      const isHasActiveTag = tagList.value.some((item) => {
        return item.path === route.fullPath
      })
      // 如果当前激活的路由不存在（被删除）则跳转到最后一个缓存的标签页
      !isHasActiveTag && router.push(currentRouteFuc()[0].path)
    }
  }

  // 获取固定标签缓存
  const getFixedTagCache = () => {
    if (storageLocal.getItem(CK_FIXED_TAG_STORE)) {
      fixedTagList.value = JSON.parse(decodeAES(storageLocal.getItem(CK_FIXED_TAG_STORE), aesKey, iv))
    } else {
      fixedTagList.value = []
    }
  }

  // 固定标签缓存
  const setFixedTagCache = () => {
    storageLocal.setItem(CK_FIXED_TAG_STORE, encodeAES(fixedTagList.value, aesKey, iv))
  }

  const removeFixedTagCache = () => {
    fixedTagList.value = []
    storageLocal.removeItem(CK_FIXED_TAG_STORE)
  }

  // 用户设置标签固定
  const userSetTagFixed = (path) => {
    fixedTagList.value.push(path)
    setFixedTagCache()
    const currentRouter = tagList.value.filter((item) => {
      return item.path === path
    })
    if (currentRouter.length > 0) {
      const toRoute = {
        path: currentRouter[0].path,
        meta: currentRouter[0].meta
      }
      toRoute.meta.isFixed = true
      updateTagMeta({ value: toRoute })
    }
  }

  // 用户取消标签固定
  const userCancelTagFixed = (path) => {
    if (fixedTagList.value && fixedTagList.value.length > 0) {
      fixedTagList.value = fixedTagList.value.filter((item) => {
        return item !== path
      })
      setFixedTagCache()
      const currentRouter = tagList.value.filter((item) => {
        return item.path === path
      })
      const toRoute = {
        path: currentRouter[0].path,
        meta: currentRouter[0].meta
      }
      toRoute.meta.isFixedTab = false
      updateTagMeta({ value: toRoute })
    }
  }

  // 标签操作
  const tagOperateHandle = (data) => {
    switch (data.mode) {
      // 关闭其他
      case 'equal':
        tagList.value = tagList.value.filter((item, index) => {
          return item.meta.isFixed || index === data.value
        })
        setAllTagCache()
        break
      // 关闭左侧
      case 'left':
        tagList.value = tagList.value.filter((item, index) => {
          return item.meta.isFixed || index >= data.value
        })
        setAllTagCache()
        break
      // 关闭右侧
      case 'right':
        tagList.value = tagList.value.filter((item, index) => {
          return item.meta.isFixed || index <= data.value
        })
        setAllTagCache()
        break
      // 关闭当前
      case 'current':
        tagList.value = tagList.value.filter((item, index) => {
          return index !== data.value
        })
        setAllTagCache()
        break
      // 关闭所有
      case 'all':
        tagList.value = tagList.value.filter((item) => {
          return item.meta.isFixed
        })
        setAllTagCache()
        break
      // 添加标签数据
      case 'push':
        // eslint-disable-next-line no-case-declarations
        const tagVal = data.value
        // 判断tag是否已存在:
        // eslint-disable-next-line no-case-declarations
        const tagHasExits = tagList.value.some((tag) => {
          return tag.path === tagVal?.path
        })
        // tag已经存在，并且showTag不存在或者值为0或1，说明不需要多开标签页，标签页已存在，也不需要再添加，此时直接返回
        if (tagHasExits && (!tagVal?.meta?.showTag || tagVal?.meta?.showTag === 1)) return
        // 如果标签不存在，并且其相同name的路由已存在，同时该页面又不允许多开，则不能添加新的标签，而需要将原有相同name路由标签的数据替换成新标签的数据
        if (!tagHasExits && tagList.value.filter((e) => e.name === tagVal.name).length > 0 && (!tagVal?.meta?.showTag || tagVal?.meta?.showTag === 1)) {
          const currentTagList = cloneDeep(tagList.value)
          currentTagList.forEach((item) => {
            if (item.name === tagVal.name) {
              item.meta = tagVal?.meta
              item.path = tagVal?.path
            }
          })
          tagList.value = currentTagList
        }
        // 如果标签不存在，并且相同name路由已经存在，且多开页面是允许的，则需要进行进一步逻辑判断
        if (!tagHasExits && tagList.value.filter((e) => e.name === tagVal.name).length > 0 && tagVal?.meta?.showTag && tagVal?.meta?.showTag > 1) {
          // eslint-disable-next-line no-case-declarations
          const meta = tagVal?.meta
          // eslint-disable-next-line no-case-declarations
          const dynamicLevel = meta?.showTag ?? -1
          if (dynamicLevel > 1 && !tagHasExits) {
            // dynamicLevel动态路由可打开的数量
            const realName = tagVal.name
            // 获取到已经打开的动态路由数, 判断是否大于dynamicLevel
            if (tagList.value.filter((e) => e.name === realName).length >= dynamicLevel) {
              // 关闭第一个
              const index = tagList.value.findIndex((item) => item.name === realName)
              index !== -1 && tagList.value.splice(index, 1)
            }
          }
          // 删除已缓存的详情路由，并将新的进行缓存
          // eslint-disable-next-line no-case-declarations
          const tagIndex = tagList.value.findIndex((item) => item.path === tagVal?.path)
          if (tagIndex !== -1) {
            tagList.value.splice(tagIndex, 1, data.value)
          }
        }
        // 标签不存在且同name路由也不存在、或者标签不存在且同name路由存在且多开标签页功能开启时
        if ((!tagHasExits && tagList.value.filter((e) => e.name === tagVal.name).length === 0) || (!tagHasExits && tagList.value.filter((e) => e.name === tagVal.name).length > 0 && tagVal?.meta?.showTag && tagVal?.meta?.showTag > 1)) {
          tagList.value.push(data.value)
          setAllTagCache()
        }
        break
      // 关闭当前需要返回标签栏最后一条数据作为当前路由页面
      case 'slice':
        return () => {
          const toRouter = []
          if (tagList.value.length === 0) {
            const toRoute = {
              path: data.router.options.routes[0].children[0].path,
              name: data.router.options.routes[0].children[0].name,
              meta: data.router.options.routes[0].children[0].meta
            }
            toRouter.push(toRoute)
          } else {
            toRouter.push(tagList.value.slice(-1)[0])
          }
          return toRouter
        }
    }
  }

  // 更新标签信息
  const updateTagMeta = (data) => {
    tagList.value.forEach((item) => {
      if (item.path === data.value.path) {
        item.meta = data.value.meta
        if (fixedTagList.value.length > 0 && fixedTagList.value.includes(data.value.path)) {
          item.meta.isFixed = true
        }
      }
    })
    setAllTagCache()
  }

  getAllTagCache()
  getFixedTagCache()

  return {
    tagList,
    fixedTagList,
    tagOperateHandle,
    dynamicRouteTag,
    deleteDynamicTag,
    updateTagMeta,
    getAllTagCache,
    setAllTagCache,
    removeAllTagCache,
    userSetTagFixed,
    userCancelTagFixed,
    getFixedTagCache,
    setFixedTagCache,
    removeFixedTagCache
  }
})

// pinia默认不使用热更新，容易报错 ReferenceError: Cannot access ‘...‘ before initialization
if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useTagCacheStore, import.meta.hot))
}

// router初始化优先于pinia，在router中使用pinia，需要先进行注册
export const useTagCacheStoreWithOut = () => {
  return useTagCacheStore(store)
}
