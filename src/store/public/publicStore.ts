import { acceptHMRUpdate, defineStore } from 'pinia'
import { store } from '../index'

export const usePublicStore = defineStore('public', () => {
  const showSettings = ref(false)
  const scrollTopHeight = ref(0)
  const affixTop = ref(0)
  const isColumnShow = ref(false)

  return {
    showSettings,
    scrollTopHeight,
    affixTop,
    isColumnShow
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(usePublicStore, import.meta.hot))
}

export const usePublicStoreWithOut = () => {
  return usePublicStore(store)
}
