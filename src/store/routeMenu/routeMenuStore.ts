import { acceptHMRUpdate, defineStore } from 'pinia'
import { store } from '../index'

export const useRouteMenuStore = defineStore('routeMenu', () => {
  const routeMenuList = ref([])

  const getRouteMenuList = (routes) => {
    if (routeMenuList.value.length > 0) {
      return
    }
    routeMenuList.value = routes
  }

  return {
    routeMenuList,
    getRouteMenuList
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useRouteMenuStore, import.meta.hot))
}

export const useRouteMenuStoreWithOut = () => {
  return useRouteMenuStore(store)
}
