export enum ChartModeEnum {
  SINGLE = 'single',
  DOUBLE = 'double'
}

export enum LayerModeEnum {
  THUMBNAIL = 'thumbnail',
  TEXT = 'text'
}

export enum LayoutStoreEnum {
  LAYOUT = 'layout',
  LAYERS = 'layers',
  CHARTS = 'charts',
  DETAILS = 'details',
  Chart_TYPE = 'chartType',
  LAYER_TYPE = 'layerType',
  PERCENTAGE = 'percentage',
  RE_POSITION_CANVAS = 'rePositionCanvas'
}

export interface LayoutType {
  // 图层控制
  [LayoutStoreEnum.LAYERS]: boolean
  // 图表组件
  [LayoutStoreEnum.CHARTS]: boolean
  // 详情设置
  [LayoutStoreEnum.DETAILS]: boolean
  // 组件展示方式
  [LayoutStoreEnum.Chart_TYPE]: ChartModeEnum
  // 层级展示方式
  [LayoutStoreEnum.LAYER_TYPE]: LayerModeEnum
  // 当前正在加载的数量
  [LayoutStoreEnum.PERCENTAGE]: number
  // 是否重置当前画布位置
  [LayoutStoreEnum.RE_POSITION_CANVAS]: boolean
}

export interface draggableLayout {
  // 组件key
  key: string
  // 组件分类
  category?: string
  // 拖拽模式
  mode?: string
  // 组件
  component?: any
  // 拖拽配置参数
  draggableConfig?: draggableConfig
  // 组件是否禁用拖拽
  disabled?: boolean
  // 组件图标
  icon?: string
  // 组件标题
  title?: string
  // 组件嵌套
  children: Array<draggableLayout>
}
export interface draggableConfig {
  // 如果一个页面有多个拖拽区域，通过设置group名称可以实现多个区域之间相互拖拽
  group: string | draggableGroup
  // 是否开启排序,如果设置为false,它所在组无法排序
  sort?: boolean
  // 鼠标按下多少秒之后可以拖拽元素
  delay?: number
  // 鼠标按下移动多少px才能拖动元素
  touchStartThreshold?: number
  // :disabled= "true",是否启用拖拽组件
  disabled?: boolean
  // 拖动时的动画效果，如设置animation=1000表示1秒过渡动画效果
  animation?: number
  // :handle=".mover" 只有当鼠标在class为mover类的元素上才能触发拖到事件
  handle?: string
  // :filter=".unmover" 设置了unmover样式的元素不允许拖动
  filter?: string
  // :draggable=".item" 样式类为item的元素才能被拖动
  draggable?: string
  // :ghost-class="ghostClass" 设置拖动元素的占位符类名,你的自定义样式可能需要加!important才能生效，并把forceFallback属性设置成true
  'ghost-class'?: string
  // :ghost-class="hostClass" 被选中目标的样式，你的自定义样式可能需要加!important才能生效，并把forceFallback属性设置成true
  'chosen-class'?: string
  // :drag-class="dragClass"拖动元素的样式，你的自定义样式可能需要加!important才能生效，并把forceFallback属性设置成true
  'drag-class'?: string
  // 默认false，忽略HTML5的拖拽行为，因为h5里有个属性也是可以拖动，你要自定义ghostClass chosenClass dragClass样式时，建议forceFallback设置为true
  'force-fallback'?: boolean
  // 默认false，克隆选中元素的样式到跟随鼠标的样式
  'fallback-class'?: boolean
  // 默认false，克隆的元素添加到文档的body中
  'fallback-on-body'?: boolean
  // 按下鼠标移动多少个像素才能拖动元素，:fallback-tolerance="8"
  'fallback-tolerance'?: number
  // 默认true,有滚动区域是否允许拖拽
  scroll?: boolean
  // 滚动回调函数
  'scroll-fn'?: Function
  // 距离滚动区域多远时，滚动滚动条
  'scroll-fensitivity'?: number
  // 滚动速度
  'scroll-speed'?: number
}

export interface draggableGroup {
  // 分组名称相同的组之间可以相互拖拽
  name: string
  // true|false|function 是否允许拖入当前组
  pull: boolean | Function
  // true|false|function,是否允许拖出当前组
  put: boolean | Function
}
