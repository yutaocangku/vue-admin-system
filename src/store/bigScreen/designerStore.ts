import { defineStore } from 'pinia'
import { store } from '../index'
import { defaultTheme, globalThemeJson } from '@/designer/Common/settings/chartThemes/index'
import { requestInterval, previewScaleType, requestIntervalUnit } from '@/designer/BigScreen/settings/bigScreen'
import { MenuEnum } from '@/designer/Common/enums/editPageEnum'
export const useDesignerStore = defineStore('designer', () => {
  // 画布属性
  const editCanvas = ref({
    // 编辑区域 Dom
    editLayoutDom: null,
    editContentDom: null,
    // 偏移量，标尺厚度
    thick: 20,
    // 系统控制缩放
    scale: 1,
    // 用户控制的缩放
    userScale: 1,
    // 锁定缩放
    lockScale: false,
    // 初始化
    isCreate: false,
    // 拖拽中
    isDrag: false,
    // 框选中
    isSelect: false,
    // 代码编辑中
    isCodeEdit: false,
    // 标尺是否显示
    isRulerShow: true,
    // 辅助线是否显示
    isAuxiliaryLinesShow: true,
    // 标尺阴影
    shadow: {
      x: 5,
      y: 5,
      width: 5,
      height: 5
    }
  })
  // 右键菜单
  const rightMenuShow = ref(false)
  // 鼠标定位
  const mousePosition = ref({
    startX: 0,
    startY: 0,
    x: 0,
    y: 0
  })
  // 目标图表
  const targetChart = ref({
    hoverId: undefined,
    selectId: []
  })
  // 记录临时数据（复制等）
  const recordChart = ref(undefined)
  // 画布属性（需存储给后端）
  const editCanvasConfig = ref({
    // 项目名称
    projectName: undefined,
    // 默认宽度
    width: 1920,
    // 默认高度
    height: 1080,
    // 启用滤镜
    filterShow: false,
    // 色相
    hueRotate: 0,
    // 饱和度
    saturate: 1,
    // 对比度
    contrast: 1,
    // 亮度
    brightness: 1,
    // 透明度
    opacity: 1,
    // 变换（暂不更改）
    rotateZ: 0,
    rotateX: 0,
    rotateY: 0,
    skewX: 0,
    skewY: 0,
    // 混合模式
    blendMode: 'normal',
    // 默认背景色
    background: undefined,
    backgroundImage: undefined,
    // 是否使用纯颜色
    selectColor: true,
    // chart 主题色
    chartThemeColor: defaultTheme || 'dark',
    // 自定义颜色列表
    chartCustomThemeColorInfo: undefined,
    // 全局配置
    chartThemeSetting: globalThemeJson,
    // 适配方式
    previewScaleType: previewScaleType,
    // 辅助线数据
    auxiliaryLines: {
      isShowReferLine: true, // 显示参考线
      lines: {
        v: [],
        h: []
      }
    }
  })
  // 数据请求处理（需存储给后端）
  const requestGlobalConfig = ref({
    requestDataPond: [],
    requestOriginUrl: '',
    requestInterval: requestInterval,
    requestIntervalUnit: requestIntervalUnit,
    requestParams: {
      Body: {
        'form-data': {},
        'x-www-form-urlencoded': {},
        json: '',
        xml: ''
      },
      Header: {},
      Params: {}
    }
  })
  // 图表数组（需存储给后端）
  const componentList = ref([])
  /**
   * * 设置缩放
   * @param scale 0~1 number 缩放比例;
   * @param force boolean 强制缩放
   */
  const setScale = (scale, force = false) => {
    if (!editCanvas.value.lockScale || force) {
      setPageSize(scale)
      editCanvas.value.userScale = scale
      editCanvas.value.scale = scale
    }
  }
  // 设置页面大小
  const setPageSize = (scale) => {
    setPageStyle('height', `${editCanvasConfig.value.height * scale}px`)
    setPageStyle('width', `${editCanvasConfig.value.width * scale}px`)
  }
  // 设置页面样式
  const setPageStyle = (key, value) => {
    const dom = editCanvas.value.editContentDom
    if (dom) {
      dom.style[key] = value
    }
  }

  // 计算缩放比例
  const computedScale = (type) => {
    if (editCanvas.value.editLayoutDom) {
      // 用户设定的宽高
      const editCanvasWidth = editCanvasConfig.value.width
      const editCanvasHeight = editCanvasConfig.value.height
      // 用户设定的宽高比
      const baseProportion = parseFloat((editCanvasWidth / editCanvasHeight).toFixed(2))

      // 现有可视编辑区宽高; 8代表滚动条占用的空间，200为自定义默认缩放减小值
      const width = editCanvas.value.editLayoutDom.clientWidth - editCanvas.value.thick - 8 - 16
      let height = 0
      if (type === 'zoom0') {
        height = editCanvas.value.editLayoutDom.clientHeight - editCanvas.value.thick - 8 - 16
      } else {
        height = editCanvas.value.editLayoutDom.clientHeight - editCanvas.value.thick - 8 - 16 - 200
      }
      // 现有可视编辑区宽高比
      const currentRate = parseFloat((width / height).toFixed(2))

      if (currentRate > baseProportion) {
        // 表示更宽
        const scaleWidth = parseFloat(((height * baseProportion) / editCanvasWidth).toFixed(2))
        setScale(scaleWidth > 1 ? 1 : scaleWidth)
      } else {
        // 表示更高
        const scaleHeight = parseFloat((width / baseProportion / editCanvasHeight).toFixed(2))
        setScale(scaleHeight > 1 ? 1 : scaleHeight)
      }
    }
  }

  // 视图缩放
  const setZoom = (keyboardValue: MenuEnum) => {
    switch (keyboardValue) {
      case 'zoomOut':
        editCanvas.value.scale += 0.05
        setScale(parseFloat(editCanvas.value.scale.toFixed(2)))
        break
      case 'zoomIn':
        editCanvas.value.scale -= 0.05
        setScale(parseFloat(editCanvas.value.scale.toFixed(2)))
        break
      case 'zoomDefault':
        computedScale(keyboardValue)
        break
      case 'zoom0':
        computedScale(keyboardValue)
        break
      case 'zoom1':
        editCanvas.value.scale = 1
        setScale(editCanvas.value.scale)
        break
      case 'zoom2':
        editCanvas.value.scale = 2
        setScale(editCanvas.value.scale)
        break
    }
  }

  // 标尺显示隐藏
  const toggleRuler = () => {
    editCanvas.value.isRulerShow = !editCanvas.value.isRulerShow
  }

  // 辅助线显示隐藏
  const toggleAuxiliaryLines = () => {
    editCanvasConfig.value.auxiliaryLines.isShowReferLine = !editCanvasConfig.value.auxiliaryLines.isShowReferLine
  }

  // 清空辅助线
  const clearAuxiliaryLines = () => {
    editCanvasConfig.value.auxiliaryLines.lines.h = []
    editCanvasConfig.value.auxiliaryLines.lines.v = []
  }

  return {
    editCanvas,
    rightMenuShow,
    mousePosition,
    targetChart,
    recordChart,
    editCanvasConfig,
    requestGlobalConfig,
    componentList,
    setScale,
    computedScale,
    setZoom,
    toggleRuler,
    toggleAuxiliaryLines,
    clearAuxiliaryLines
  }
})

export const useDesignerStoreWithOut = () => {
  return useDesignerStore(store)
}
