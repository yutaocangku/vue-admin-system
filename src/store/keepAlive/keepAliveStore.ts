import { acceptHMRUpdate, defineStore } from 'pinia'
import { store } from '../index'

export const useKeepAliveStore = defineStore('keepAlive', () => {
  const keepAliveRouteNameList = ref([])
  const scrollPosition = ref([])

  // 添加或删除某个缓存页面
  const keepAliveOperate = ({ mode, name }) => {
    switch (mode) {
      case 'add':
        keepAliveRouteNameList.value.push(name)
        // es6新语法，可进行去重
        keepAliveRouteNameList.value = [...new Set(keepAliveRouteNameList.value)]
        break
      case 'delete':
        // eslint-disable-next-line no-case-declarations
        const delIndex = keepAliveRouteNameList.value.findIndex((v) => v === name)
        delIndex !== -1 && keepAliveRouteNameList.value.splice(delIndex, 1)
        break
    }
  }

  // 清空缓存页面
  const clearAllKeepAlive = (router) => {
    if (router.meta.keepAlive) {
      keepAliveRouteNameList.value = keepAliveRouteNameList.value.filter((item) => {
        return item === router.name
      })
      scrollPosition.value = scrollPosition.value.filter((item) => {
        return item.name === router.name
      })
    } else {
      keepAliveRouteNameList.value = []
      scrollPosition.value = []
    }
  }

  // 移除所有的页面状态缓存以及滚动位置记录
  const removeAllKeepAlive = () => {
    keepAliveRouteNameList.value = []
    scrollPosition.value = []
  }

  // 删除滚动位置记录信息
  const removeScrollPosition = (path) => {
    if (typeof path === 'string') {
      const delIdx = scrollPosition.value.findIndex((v) => v.fullPath === path)
      delIdx !== -1 && scrollPosition.value.splice(delIdx, 1)
    } else {
      let currentName = ''
      scrollPosition.value.forEach((item) => {
        if (item.fullPath === path.path) {
          currentName = item.name
        }
      })
      scrollPosition.value = scrollPosition.value.filter((item) => {
        return item.name !== currentName
      })
    }
  }

  // 保存滚动位置
  const setScrollPosition = (itemPosition) => {
    // 判断是否已经保存过
    let hasRoute = false
    if (scrollPosition.value.length > 0) {
      scrollPosition.value.forEach((item) => {
        // 已保存过该条路由信息，直接改变已保存的滚动位置记录
        if (item.fullPath === itemPosition.fullPath) {
          hasRoute = true
          item.savedPosition = itemPosition.savedPosition
        }
      })
    }
    // 未保存过，新增一条记录
    if (!hasRoute) {
      scrollPosition.value.push(itemPosition)
    }
  }

  return {
    keepAliveRouteNameList,
    scrollPosition,
    keepAliveOperate,
    clearAllKeepAlive,
    removeAllKeepAlive,
    removeScrollPosition,
    setScrollPosition
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useKeepAliveStore, import.meta.hot))
}

export const useKeepAliveStoreWithOut = () => {
  return useKeepAliveStore(store)
}
