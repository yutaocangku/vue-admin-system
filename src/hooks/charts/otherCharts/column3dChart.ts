﻿import { extractType } from '../utils'
import { column3dThemeOptions, linearColor } from './utils'
import { hexToRgb } from '@/utils/theme'
const useColumn3dChartOptions = (data, config, systemConfig) => {
  const initData = (data, config) => {
    const dataArr = []
    data.forEach((item) => {
      dataArr.push(item[config.value])
    })
    return dataArr
  }
  const initOptions: any = ref({
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category',
      axisLabel: {
        margin: 16
      }
    },
    yAxis: {
      type: 'value'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        type: 'bar',
        barWidth: 30,
        itemStyle: {
          normal: {
            color: function (params) {
              return linearColor(systemConfig)
            }
          }
        },
        label: {
          show: true,
          position: [15, -24],
          fontSize: 12,
          color: systemConfig.value.primaryColor,
          align: 'center'
        },
        data: initData(data, config)
      },
      {
        z: 2,
        type: 'pictorialBar',
        data: initData(data, config),
        symbol: 'diamond',
        symbolOffset: [0, '50%'],
        symbolSize: [30, 15],
        itemStyle: {
          normal: {
            color: function (params) {
              return linearColor(systemConfig)
            }
          }
        }
      },
      {
        z: 3,
        type: 'pictorialBar',
        symbolPosition: 'end',
        data: initData(data, config),
        symbol: 'diamond',
        symbolOffset: [0, '-50%'],
        symbolSize: [30, 15],
        itemStyle: {
          normal: {
            borderWidth: 0,
            color: function (params) {
              return 'rgba(' + hexToRgb(systemConfig.value.primaryColor) + ', 1)'
            }
          }
        }
      }
    ]
  })
  const column3dInitOptions: any = column3dThemeOptions(initOptions, systemConfig)
  return {
    column3dInitOptions
  }
}
export default useColumn3dChartOptions
