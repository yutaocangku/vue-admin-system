﻿import { colorMix, hexToRgb } from '@/utils/theme'
import * as echarts from 'echarts'
export const linearColor = (systemConfig) => {
  return {
    type: 'linear',
    x: 0,
    x2: 1,
    y: 0,
    y2: 0,
    colorStops: [
      {
        offset: 0.5,
        color: 'rgba(' + hexToRgb(systemConfig.value.primaryColor) + ', 0.7)'
      },
      {
        offset: 0.5,
        color: 'rgba(' + hexToRgb(systemConfig.value.primaryColor) + ', 0.4)'
      },
      {
        offset: 0.5,
        color: 'rgba(' + hexToRgb(systemConfig.value.primaryColor) + ', 0.6)'
      },
      {
        offset: 1,
        color: 'rgba(' + hexToRgb(systemConfig.value.primaryColor) + ', 0.4)'
      }
    ]
  }
}
export const column3dThemeOptions = (initChartOptions, systemConfig) => {
  const initOptions = initChartOptions.value
  initOptions.series[0].label.color = systemConfig.value.primaryColor
  initOptions.series[0].itemStyle.normal.color = () => {
    return linearColor(systemConfig)
  }
  initOptions.series[1].itemStyle.normal.color = () => {
    return linearColor(systemConfig)
  }
  initOptions.series[2].itemStyle.normal.color = () => {
    return 'rgba(' + hexToRgb(systemConfig.value.primaryColor) + ', 1)'
  }
  return initOptions
}

export const linearGradientColor = (systemConfig) => {
  return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    {
      offset: 1,
      color: 'rgba(' + hexToRgb(colorMix('#ffffff', systemConfig.value.primaryColor, 0.8)) + ', 0.8)'
    },
    {
      offset: 0,
      color: 'rgba(' + hexToRgb(colorMix('#000000', systemConfig.value.primaryColor, 0.8)) + ', 0.8)'
    }
  ])
}
export const linearGradientHeaderColor = (systemConfig) => {
  return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    {
      offset: 1,
      color: 'rgba(' + hexToRgb(colorMix('#ffffff', systemConfig.value.primaryColor, 0.7)) + ', 1)'
    },
    {
      offset: 0,
      color: 'rgba(' + hexToRgb(colorMix('#000000', systemConfig.value.primaryColor, 0.9)) + ', 1)'
    }
  ])
}
export const linearGradientFooterColor = (systemConfig) => {
  return new echarts.graphic.LinearGradient(0, 0, 0, 1, [
    {
      offset: 1,
      color: 'rgba(' + hexToRgb(colorMix('#ffffff', systemConfig.value.primaryColor, 0.7)) + ', 1)'
    },
    {
      offset: 0,
      color: 'rgba(' + hexToRgb(colorMix('#000000', systemConfig.value.primaryColor, 0.8)) + ', 1)'
    }
  ])
}
