﻿import { extractType } from '../utils'
import { transformI18n } from '@/plugins/i18n'
import { linearGradientColor, linearGradientHeaderColor, linearGradientFooterColor } from './utils'
const useLineColumn3dChartOptions = (data, config, systemConfig) => {
  const columnData = (data, config) => {
    const dataArr = []
    data.forEach((item) => {
      dataArr.push(item[config.columnValue])
    })
    return dataArr
  }
  const bottomData = (data) => {
    const dataArr = []
    data.forEach(() => {
      dataArr.push(0)
    })
    return dataArr
  }
  const lineData = (data, config) => {
    const dataArr = []
    data.forEach((item) => {
      dataArr.push(item[config.lineValue])
    })
    return dataArr
  }
  const initOptions: any = ref({
    xAxis: [
      {
        type: 'category',
        data: extractType(data, config.axis),
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        axisLabel: {
          show: true
        },
        splitArea: {
          show: false
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        position: 'left',
        axisLine: {
          show: true
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        axisLabel: {
          show: true
        },
        splitArea: {
          show: false
        }
      },
      {
        type: 'value',
        position: 'right',
        min: 0,
        max: 100,
        axisLine: {
          show: true
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: true
        },
        axisLabel: {
          show: true,
          formatter: '{value}%'
        },
        splitArea: {
          show: true
        }
      }
    ],
    grid: {
      top: 40
    },
    legend: {
      show: true,
      data: [transformI18n('text.textValue', true), transformI18n('text.textPercentage', true)]
    },
    series: [
      {
        name: transformI18n('text.textValue', true),
        type: 'bar',
        stack: 'a',
        barWidth: 20,
        yAxisIndex: 0,
        data: columnData(data, config),
        itemStyle: {
          normal: {
            color: function (params) {
              return linearGradientColor(systemConfig)
            }
          }
        }
      },
      {
        name: transformI18n('text.textPercentage', true),
        type: 'line',
        z: 30,
        yAxisIndex: 1,
        tooltip: {
          valueFormatter: function (value) {
            return value + '%'
          }
        },
        data: lineData(data, config)
      },
      {
        name: '顶部圆片',
        stack: 'a',
        type: 'pictorialBar',
        symbolPosition: 'end',
        symbolSize: [20, 8],
        symbolOffset: [0, -4],
        z: 20,
        tooltip: {
          show: false
        },
        itemStyle: {
          normal: {
            color: function (params) {
              return linearGradientHeaderColor(systemConfig)
            }
          }
        },
        data: columnData(data, config)
      },
      {
        name: '底部圆片',
        stack: 'a',
        type: 'pictorialBar',
        symbolSize: [20, 8],
        symbolOffset: [0, 4],
        z: 20,
        tooltip: {
          show: false
        },
        itemStyle: {
          normal: {
            color: function (params) {
              return linearGradientFooterColor(systemConfig)
            }
          }
        },
        data: bottomData(data)
      }
    ]
  })
  const lineColumn3dInitOptions = initOptions.value
  return {
    lineColumn3dInitOptions
  }
}
export default useLineColumn3dChartOptions
