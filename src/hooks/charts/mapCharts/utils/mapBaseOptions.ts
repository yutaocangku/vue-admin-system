﻿import { rankContinuousVisualMap, rankPiecewiseVisualMap } from '../../utils'
import { chinaData } from './initDataChina'
import { worldData } from './initDataWorld'
import { themeColor } from '@/utils/theme'
import { transformI18n } from '@/plugins/i18n'
export const mapInitOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        if (params.data) {
          if (config.map === 'china' || config.map === 'chinaWorld') {
            return `<div class="echarts-tooltip">
              <div class="tooltip-list">
                <div class="item-tooltip">
                  <span class="icon" style="background:${params.color}"></span>
                  <span class="name">${transformI18n('text.textRegion', true)}</span>
                  <div class="num">${params.data.name}</div>
                </div>
                <div class="item-tooltip">
                  <span class="icon" style="background:${params.color}"></span>
                  <span class="name">${transformI18n('text.textValue', true)}</span>
                  <div class="num">${params.data.value}</div>
                </div>
              </div>
            </div>`
          } else {
            return `<div class="echarts-tooltip">
              <div class="tooltip-list">
                <div class="item-tooltip">
                  <span class="icon" style="background:${params.color}"></span>
                  <span class="name">${transformI18n('text.textCountry', true)}</span>
                  <div class="num">${params.data.country}</div>
                </div>
                <div class="item-tooltip">
                  <span class="icon" style="background:${params.color}"></span>
                  <span class="name">${transformI18n('text.textEnglishName', true)}</span>
                  <div class="num">${params.data.name}</div>
                </div>
                <div class="item-tooltip">
                  <span class="icon" style="background:${params.color}"></span>
                  <span class="name">${transformI18n('text.textValue', true)}</span>
                  <div class="num">${params.data.value}</div>
                </div>
              </div>
            </div>`
          }
        }
      }
    },
    visualMap: {
      show: true
    },
    geo: {
      map: config.map
    },
    series: [
      {
        type: 'map', // 类型
        // 系列名称，用于tooltip的显示，legend 的图例筛选 在 setOption 更新数据和配置项时用于指定对应的系列
        map: config.map, // 地图类型
        showLegendSymbol: false,
        // 地图系列中的数据内容数组 数组项可以为单个数值
        data: config.map === 'china' || config.map === 'chinaWorld' ? chinaData(data, config.type, config.value) : worldData(data, config.type, config.value),
        label: {
          show: false
        },
        itemStyle: {
          areaColor: systemConfig.value.isLight ? '#eeeeee' : 'rgba(0,0,0,.1)'
        },
        emphasis: {
          label: {
            show: false
          },
          itemStyle: {
            areaColor: themeColor().warning
          }
        },
        select: {
          label: {
            show: false
          },
          itemStyle: {
            areaColor: themeColor().warning
          }
        }
      }
    ]
  })
  const mapOptions = config.visualMap === 'piecewise' ? rankPiecewiseVisualMap(initOptions, systemConfig, true, config.isPrimary) : rankContinuousVisualMap(initOptions, systemConfig, true, config.isPrimary)
  return mapOptions
}
