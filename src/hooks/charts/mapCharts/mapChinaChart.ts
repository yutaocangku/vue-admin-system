﻿import { mapInitOptions } from './utils/mapBaseOptions'
import { deepMerge } from '@/utils'
import { transformI18n } from '@/plugins/i18n'
const useChinaMapChartOptions = (data, config, systemConfig) => {
  const initOptions: any = mapInitOptions(data, config, systemConfig)
  const chinaMapBaseOptions = {
    visualMap: {
      inverse: false,
      text: [transformI18n('text.textValue', true), transformI18n('text.textLow', true)]
    }
  }
  const chinaMapInitOptions = deepMerge(initOptions, chinaMapBaseOptions)
  return {
    chinaMapInitOptions
  }
}
export default useChinaMapChartOptions
