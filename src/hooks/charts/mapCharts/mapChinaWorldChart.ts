﻿import { mapInitOptions } from './utils/mapBaseOptions'
import { deepMerge } from '@/utils'
const useChinaWorldMapChartOptions = (data, config, systemConfig) => {
  const initOptions: any = mapInitOptions(data, config, systemConfig)
  initOptions.series.forEach((item) => {
    const currentSeries = item
    item = deepMerge(currentSeries, {
      roam: false,
      zoom: 5,
      center: [105.97, 35.71],
      mapType: config.map
    })
  })
  const chinaWorldMapBaseOptions = {
    visualMap: {
      inverse: false
    },
    geo: {
      show: false
    }
  }
  const chinaWorldMapInitOptions = deepMerge(initOptions, chinaWorldMapBaseOptions)
  return {
    chinaWorldMapInitOptions
  }
}
export default useChinaWorldMapChartOptions
