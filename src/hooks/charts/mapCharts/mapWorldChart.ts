﻿import { mapInitOptions } from './utils/mapBaseOptions'
import { deepMerge } from '@/utils'
const useWorldMapChartOptions = (data, config, systemConfig) => {
  const initOptions: any = mapInitOptions(data, config, systemConfig)
  const worldMapBaseOptions = {
    visualMap: {
      inverse: false
    }
  }
  const worldMapInitOptions = deepMerge(initOptions, worldMapBaseOptions)
  return {
    worldMapInitOptions
  }
}
export default useWorldMapChartOptions
