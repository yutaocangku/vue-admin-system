﻿import { mapInitOptions } from './utils/mapBaseOptions'
import { deepMerge } from '@/utils'
import { transformI18n } from '@/plugins/i18n'
const useWorldZhMapChartOptions = (data, config, systemConfig) => {
  const initOptions: any = mapInitOptions(data, config, systemConfig)
  const worldZhMapBaseOptions = {
    visualMap: {
      inverse: true,
      text: [transformI18n('text.textHigh', true), transformI18n('text.textLow', true)],
      left: 'center',
      orient: 'horizontal'
    }
  }
  const worldZhMapInitOptions = deepMerge(initOptions, worldZhMapBaseOptions)
  return {
    worldZhMapInitOptions
  }
}
export default useWorldZhMapChartOptions
