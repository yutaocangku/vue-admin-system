﻿import { extractType, initPieData, initPieThemeOptions } from '../utils'
const usePieChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    grid: {
      top: 80
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        name: data[0][config.axis],
        type: 'pie',
        radius: '50%',
        top: 40,
        emphasis: {
          focus: 'self'
        },
        data: initPieData(data, config.type, config.value)
      }
    ]
  })
  const pieInitOptions: any = initPieThemeOptions(initOptions, systemConfig)
  return {
    pieInitOptions
  }
}
export default usePieChartOptions
