﻿import { extractType, initPieData, initPieThemeOptions } from '../utils'
const usePieDoughnutChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    grid: {
      top: 80
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        name: data[0][config.axis],
        type: 'pie',
        radius: ['40%', '70%'],
        top: 60,
        emphasis: {
          label: {
            show: true
          }
        },
        data: initPieData(data, config.type, config.value)
      }
    ]
  })
  const pieDoughnutInitOptions: any = initPieThemeOptions(initOptions, systemConfig)
  return {
    pieDoughnutInitOptions
  }
}
export default usePieDoughnutChartOptions
