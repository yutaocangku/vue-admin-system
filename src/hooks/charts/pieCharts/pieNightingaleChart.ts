﻿import { extractType, initPieData, initPieThemeOptions } from '../utils'
const usePieNightingaleChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    grid: {
      top: 80
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        name: data[0][config.axis],
        type: 'pie',
        radius: ['20%', '70%'],
        top: 60,
        roseType: 'area',
        itemStyle: {
          borderRadius: 8
        },
        emphasis: {
          label: {
            show: true
          }
        },
        data: initPieData(data, config.type, config.value)
      }
    ]
  })
  const pieNightingaleInitOptions: any = initPieThemeOptions(initOptions, systemConfig)
  return {
    pieNightingaleInitOptions
  }
}
export default usePieNightingaleChartOptions
