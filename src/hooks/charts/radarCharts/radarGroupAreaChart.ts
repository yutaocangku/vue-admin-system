﻿import { extractType } from '../utils'
import { initRadarData, radarLineThemeChange, radarAreaThemeChange } from './utils'
const useRadarGroupAreaChartOptions = (data, config, systemConfig) => {
  const radarGroupAreaInitOptions: any = {
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    legend: {
      show: true,
      orient: 'vertical',
      align: 'left',
      bottom: 0,
      left: 0,
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item'
    },
    radar: {
      indicator: initRadarData(data, config.type, config.radar, config.value, true).indicator,
      axisLine: {
        lineStyle: {
          color: radarLineThemeChange(systemConfig)
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: radarLineThemeChange(systemConfig)
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: radarAreaThemeChange(systemConfig)
        }
      }
    },
    series: initRadarData(data, config.type, config.radar, config.value, true).series
  }
  return {
    radarGroupAreaInitOptions
  }
}
export default useRadarGroupAreaChartOptions
