﻿import { extractType } from '../utils'
import { initRadarData, radarLineThemeChange, radarAreaThemeChange } from './utils'
const useRadarChartOptions = (data, config, systemConfig) => {
  const radarInitOptions: any = {
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item'
    },
    radar: {
      indicator: initRadarData(data, config.type, config.radar, config.value).indicator,
      axisLine: {
        lineStyle: {
          color: radarLineThemeChange(systemConfig)
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          color: radarLineThemeChange(systemConfig)
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: radarAreaThemeChange(systemConfig)
        }
      }
    },
    series: initRadarData(data, config.type, config.radar, config.value).series
  }
  return {
    radarInitOptions
  }
}
export default useRadarChartOptions
