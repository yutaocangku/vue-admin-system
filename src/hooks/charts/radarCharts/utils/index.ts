﻿import { extractType } from '../../utils'
export const initRadarData = (data, typeKey, radarKey, valueKey, isArea = false) => {
  const legendArr = extractType(data, typeKey)
  const radarKeyArr = extractType(data, radarKey)
  const indicator = []
  const series = []
  radarKeyArr.forEach((item) => {
    const currentRadar = {
      name: item,
      max: 0
    }
    const currentRadarArr = []
    data.forEach((item1) => {
      if (item === item1[radarKey]) {
        currentRadarArr.push(item1[valueKey])
      }
    })
    currentRadar.max = 100 // XEUtils.max(currentRadarArr, '')
    indicator.push(currentRadar)
  })
  const currentSeries = {
    type: 'radar',
    data: [],
    areaStyle: {
      opacity: isArea ? 0.1 : 0
    }
  }
  legendArr.forEach((item) => {
    const currentData = {
      value: [],
      name: item
    }
    data.forEach((item1) => {
      if (item === item1[typeKey]) {
        currentData.value.push(item1[valueKey])
      }
    })
    currentSeries.data.push(currentData)
  })
  series.push(currentSeries)
  return {
    indicator,
    series
  }
}

export const radarLineThemeChange = (systemConfig) => {
  return systemConfig.value.isLight ? 'rgba(230,235,244,1)' : 'rgba(255,255,255,.2)'
}

export const radarAreaThemeChange = (systemConfig) => {
  return systemConfig.value.isLight ? ['rgba(255,255,255,1)', 'rgba(246,248,252,1)'] : ['rgba(0,0,0,0.1)', 'rgba(0,0,0,0.2)']
}

export const radarChartThemeChange = (initChartOptions, systemConfig) => {
  const initOptions = initChartOptions.value
  initOptions.radar.axisLine.lineStyle.color = radarLineThemeChange(systemConfig)
  initOptions.radar.splitLine.lineStyle.color = radarLineThemeChange(systemConfig)
  initOptions.radar.splitArea.areaStyle.color = radarAreaThemeChange(systemConfig)
  return initOptions
}
