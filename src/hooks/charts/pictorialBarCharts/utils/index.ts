﻿import { getThemeColor } from '@/hooks/charts/baseChartOptions'
import { colorMix } from '@/utils/theme'
export const seriesColumnSvgOptions = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  const seriesOptions: any = []
  initOptions.series.forEach((item, index) => {
    seriesOptions.push({
      barCategoryGap: '5%',
      symbol: 'path://M0,10 L10,10 C5.5,10 6.5,5 5,0 C3.5,5 4.5,10 0,10 z',
      label: {
        show: true,
        position: 'top',
        distance: 0,
        color: chartThemeColor[index],
        fontWeight: 'bolder',
        fontSize: 15
      },
      itemStyle: {
        normal: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [
              {
                offset: 0,
                color: chartThemeColor[index]
              },
              {
                offset: 1,
                color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.4)
              }
            ],
            global: false //  缺省为  false
          }
        },
        emphasis: {
          opacity: 1
        }
      }
    })
  })
  return seriesOptions
}
