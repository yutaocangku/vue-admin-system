﻿import { extractType } from '../utils'
import XEUtils from 'xe-utils'
import columnChartBg from '@/assets/columnChartBg.png'
import columnChartFill from '@/assets/columnChartFill.png'
const useColumnImgChartOptions = (data, config) => {
  const initData = (data, config) => {
    const dataArr = []
    data.forEach((item) => {
      dataArr.push({
        name: item[config.axis],
        value: item[config.value]
      })
    })
    return dataArr
  }
  const maxData = (data) => {
    const maxNum: any = XEUtils.max(data, 'value')
    const dataArr = []
    data.forEach((item) => {
      dataArr.push({
        name: item.name,
        value: maxNum.value
      })
    })
    return dataArr
  }
  const columnImgInitOptions: any = {
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    yAxis: {
      type: 'value'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        type: 'pictorialBar',
        z: 3,
        barWidth: '20%',
        data: initData(data, config),
        symbol: 'image://' + columnChartFill,
        symbolSize: [20, '100%'],
        symbolClip: true,
        symbolRepeat: false,
        symbolBoundingData: XEUtils.max(initData(data, config), 'value').value
      },
      {
        name: 'background',
        type: 'pictorialBar',
        barWidth: '20%',
        barGap: '-100%',
        silent: true,
        symbol: 'image://' + columnChartBg,
        symbolSize: [20, '100%'],
        symbolRepeat: false,
        data: maxData(initData(data, config))
      }
    ]
  }
  return {
    columnImgInitOptions
  }
}
export default useColumnImgChartOptions
