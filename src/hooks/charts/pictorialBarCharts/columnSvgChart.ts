﻿import { extractType, initSeriesData } from '../utils'
import { seriesColumnSvgOptions } from './utils'
const useColumnSvgChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    yAxis: {
      type: 'value'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item'
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const columnSvgInitOptions = initOptions.value
  const columnSvgCustomSeriesOptions: any = seriesColumnSvgOptions(initOptions, systemConfig)
  return {
    columnSvgInitOptions,
    columnSvgCustomSeriesOptions
  }
}
export default useColumnSvgChartOptions
