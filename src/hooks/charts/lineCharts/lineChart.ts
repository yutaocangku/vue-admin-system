﻿import { extractType, initSeriesData } from '../utils'
const useLineChartOptions = (data, config) => {
  const lineInitOptions: any = {
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis)
    },
    legend: {
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  return {
    lineInitOptions
  }
}
export default useLineChartOptions
