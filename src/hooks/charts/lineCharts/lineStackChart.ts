﻿import { extractType, initSeriesData } from '../utils'
const useLineStackChartOptions = (data, config) => {
  const lineStackInitOptions: any = {
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis),
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: true,
        interval: 0
      },
      axisLabel: {
        show: true
      }
    },
    yAxis: {
      axisLine: {
        show: true
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      axisLabel: {
        show: true
      }
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'cross'
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  // stack值配置为一样，即为堆叠，某一项不同，则该项不参与堆叠
  const lineStackCustomSeriesOptions: any = [{ stack: 'Total' }, { stack: 'Total' }]
  return {
    lineStackInitOptions,
    lineStackCustomSeriesOptions
  }
}
export default useLineStackChartOptions
