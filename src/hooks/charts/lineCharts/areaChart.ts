﻿import { extractType, initSeriesData } from '../utils'
const useAreaChartOptions = (data, config) => {
  const areaInitOptions: any = {
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis)
    },
    legend: {
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const areaCustomSeriesOptions: any = [
    {
      symbolSize: 8,
      smooth: true,
      areaStyle: {
        opacity: 0.5
      }
    }
  ]
  return {
    areaInitOptions,
    areaCustomSeriesOptions
  }
}
export default useAreaChartOptions
