﻿import { extractType, initSeriesData } from '../utils'
const useAreaStackChartOptions = (data, config) => {
  const areaStackInitOptions: any = {
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis),
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      splitArea: {
        show: true,
        interval: 0
      },
      axisLabel: {
        show: true
      }
    },
    yAxis: {
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      splitArea: {
        show: true,
        interval: 0
      },
      axisLabel: {
        show: true
      }
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'cross'
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  // stack值配置为一样，即为堆叠，某一项不同，则该项不参与堆叠
  const areaStackCustomSeriesOptions: any = [
    {
      stack: 'Total',
      showSymbol: false,
      smooth: true,
      areaStyle: {
        opacity: 0.5
      }
    },
    {
      stack: 'Total',
      showSymbol: false,
      smooth: true,
      areaStyle: {
        opacity: 0.5
      }
    }
  ]
  return {
    areaStackInitOptions,
    areaStackCustomSeriesOptions
  }
}
export default useAreaStackChartOptions
