﻿import { extractType, initSeriesData, seriesAreaGradientOptions } from '../utils'
const useAreaGradientChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis),
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      splitArea: {
        show: true,
        interval: 0
      }
    },
    yAxis: {
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      splitArea: {
        show: false
      }
    },
    grid: {
      top: 40
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })

  const areaGradientInitOptions = initOptions.value
  // stack值配置为一样，即为堆叠，某一项不同，则该项不参与堆叠
  const areaGradientCustomSeriesOptions: any = seriesAreaGradientOptions(initOptions, systemConfig)
  return {
    areaGradientInitOptions,
    areaGradientCustomSeriesOptions
  }
}
export default useAreaGradientChartOptions
