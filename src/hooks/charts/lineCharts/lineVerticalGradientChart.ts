﻿import { verticalGradientVisualMap, extractType, initSeriesData } from '../utils'
const useLineVerticalGradientChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis),
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: true,
        interval: 0,
        lineStyle: {
          type: 'dashed',
          dashOffset: 0
        }
      },
      axisLabel: {
        show: true
      }
    },
    yAxis: {
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: true,
        lineStyle: {
          type: 'dashed',
          dashOffset: 1
        }
      },
      axisLabel: {
        show: true
      }
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'cross'
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const lineVerticalGradientInitOptions = verticalGradientVisualMap(initOptions, systemConfig)
  const lineVerticalGradientCustomSeriesOptions: any = [{ smooth: true }, { smooth: true }]
  return {
    lineVerticalGradientInitOptions,
    lineVerticalGradientCustomSeriesOptions
  }
}
export default useLineVerticalGradientChartOptions
