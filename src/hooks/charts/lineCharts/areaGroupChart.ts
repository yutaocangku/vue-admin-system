﻿import { extractType, initSeriesData } from '../utils'
const useAreaGroupChartOptions = (data, config) => {
  const areaGroupInitOptions: any = {
    xAxis: [
      {
        boundaryGap: false,
        data: extractType(data, config.axis),
        axisLine: {
          show: true
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: true,
          interval: 0
        },
        splitArea: {
          show: true,
          interval: 0
        }
      },
      {
        position: 'top',
        axisLine: {
          show: true
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        splitArea: {
          show: false
        }
      }
    ],
    yAxis: [
      {
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        splitArea: {
          show: false
        }
      }
    ],
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'cross'
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const areaGroupCustomSeriesOptions: any = [
    {
      symbolSize: 8,
      smooth: true,
      areaStyle: {
        opacity: 0.5
      }
    },
    {
      symbolSize: 8,
      smooth: true,
      areaStyle: {
        opacity: 0.5
      }
    }
  ]
  return {
    areaGroupInitOptions,
    areaGroupCustomSeriesOptions
  }
}
export default useAreaGroupChartOptions
