﻿import { horizontalGradientVisualMap, extractType, initSeriesData } from '../utils'
const useLineHorizontalGradientChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis),
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: true,
        interval: 0
      },
      axisLabel: {
        show: true
      }
    },
    yAxis: {
      axisLine: {
        show: true
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: true
      },
      axisLabel: {
        show: true
      }
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'cross'
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const lineHorizontalGradientInitOptions = horizontalGradientVisualMap(initOptions, systemConfig)
  // stack值配置为一样，即为堆叠，某一项不同，则该项不参与堆叠
  const lineHorizontalGradientCustomSeriesOptions: any = [{ stack: 'Total' }, { stack: 'Total' }]
  return {
    lineHorizontalGradientInitOptions,
    lineHorizontalGradientCustomSeriesOptions
  }
}
export default useLineHorizontalGradientChartOptions
