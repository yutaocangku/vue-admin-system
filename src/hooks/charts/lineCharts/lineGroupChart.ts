﻿import { extractType, initSeriesData } from '../utils'
const useLineGroupChartOptions = (data, config) => {
  const lineGroupInitOptions: any = {
    xAxis: {
      boundaryGap: false,
      data: extractType(data, config.axis),
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      axisLabel: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      axisLabel: {
        show: false
      }
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'cross'
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const lineGroupCustomSeriesOptions: any = [{ smooth: true }, { smooth: true }]
  return {
    lineGroupInitOptions,
    lineGroupCustomSeriesOptions
  }
}
export default useLineGroupChartOptions
