﻿import { seriesLineGradientOptions, extractType, initSeriesData } from '../utils'
const useLineBeautifyChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: [
      {
        boundaryGap: false,
        data: extractType(data, config.axis),
        axisLine: {
          show: false
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: false
        },
        axisLabel: {
          show: true
        },
        splitArea: {
          show: false
        }
      }
    ],
    yAxis: [
      {
        position: 'left',
        axisLine: {
          show: true
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: true
        },
        axisLabel: {
          show: true
        },
        splitArea: {
          show: true
        }
      },
      {
        position: 'right',
        axisLine: {
          show: true
        },
        axisTick: {
          show: false
        },
        splitLine: {
          show: true
        },
        axisLabel: {
          show: true
        },
        splitArea: {
          show: true
        }
      }
    ],
    grid: {
      top: 40
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const lineBeautifyInitOptions = initOptions.value
  // stack值配置为一样，即为堆叠，某一项不同，则该项不参与堆叠
  const lineBeautifyCustomSeriesOptions: any = seriesLineGradientOptions(initOptions, systemConfig)
  return {
    lineBeautifyInitOptions,
    lineBeautifyCustomSeriesOptions
  }
}
export default useLineBeautifyChartOptions
