﻿import { textThemeChange, labelThemeChange } from '../../utils'
export const gaugeProgressThemeOptions = (initChartOptions, systemConfig) => {
  const initOptions = initChartOptions.value
  initOptions.series[0].axisLabel.color = labelThemeChange(systemConfig)
  initOptions.series[0].detail.color = textThemeChange(systemConfig)
  return initOptions
}
