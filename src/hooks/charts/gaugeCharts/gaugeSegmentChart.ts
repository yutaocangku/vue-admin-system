﻿import { themeColor } from '@/utils/theme'
const useGaugeSegmentChartOptions = (data, config) => {
  const gaugeSegmentInitOptions: any = {
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        type: 'gauge',
        startAngle: 180,
        endAngle: 0,
        min: 0,
        max: data.max,
        axisLine: {
          lineStyle: {
            width: 14,
            color: [
              [0.3, themeColor().success],
              [0.7, themeColor().warning],
              [1, themeColor().danger]
            ]
          }
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
        axisTick: {
          distance: -14,
          length: 4,
          lineStyle: {
            color: '#fff',
            width: 1
          }
        },
        splitLine: {
          distance: -14,
          length: 14,
          lineStyle: {
            color: '#fff',
            width: 2
          }
        },
        axisLabel: {
          color: 'auto',
          distance: 20,
          fontSize: 12
        },
        detail: {
          valueAnimation: true,
          formatter: '{value}',
          color: 'auto'
        },
        data: [
          {
            value: data[config.value]
          }
        ]
      }
    ]
  }
  return {
    gaugeSegmentInitOptions
  }
}
export default useGaugeSegmentChartOptions
