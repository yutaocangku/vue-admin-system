﻿import { textThemeChange, labelThemeChange } from '../utils'
const useGaugeProgressChartOptions = (data, config, systemConfig) => {
  const gaugeProgressInitOptions: any = {
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        type: 'gauge',
        min: 0,
        max: data.max,
        progress: {
          show: true,
          roundCap: true
        },
        axisLine: {
          roundCap: true
        },
        axisLabel: {
          color: labelThemeChange(systemConfig)
        },
        axisTick: {
          splitNumber: 2
        },
        detail: {
          valueAnimation: true,
          formatter: '{value}',
          color: textThemeChange(systemConfig)
        },
        data: [
          {
            value: data[config.value]
          }
        ]
      }
    ]
  }
  return {
    gaugeProgressInitOptions
  }
}
export default useGaugeProgressChartOptions
