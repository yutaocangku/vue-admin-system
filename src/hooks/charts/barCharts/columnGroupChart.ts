﻿import { extractType, initSeriesData } from '../utils'
const useColumnGroupChartOptions = (data, config) => {
  const columnGroupInitOptions: any = {
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category',
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        show: false
      },
      splitArea: {
        show: true,
        interval: 0
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  return {
    columnGroupInitOptions
  }
}
export default useColumnGroupChartOptions
