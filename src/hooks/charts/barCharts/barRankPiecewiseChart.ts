﻿import { extractType, initSeriesData, rankPiecewiseVisualMap } from '../utils'
const useBarRankPiecewiseChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    visualMap: {
      show: true,
      orient: 'horizontal',
      inverse: true
    },
    grid: {
      bottom: 40
    },
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const barRankPiecewiseInitOptions = rankPiecewiseVisualMap(initOptions, systemConfig)
  return {
    barRankPiecewiseInitOptions
  }
}
export default useBarRankPiecewiseChartOptions
