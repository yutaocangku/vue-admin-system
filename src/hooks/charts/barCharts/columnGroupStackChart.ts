﻿import { extractType, initGroupStackSeriesData } from '../utils'
const useColumnGroupStackChartOptions = (data, config) => {
  const columnGroupStackInitOptions: any = {
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category',
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        show: true
      },
      splitArea: {
        show: true,
        interval: 0
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initGroupStackSeriesData(data, config.class, config.type, config.value, config.chartType)
  }
  return {
    columnGroupStackInitOptions
  }
}
export default useColumnGroupStackChartOptions
