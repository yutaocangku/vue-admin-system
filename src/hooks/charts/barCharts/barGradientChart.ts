﻿import { extractType, initSeriesData, seriesBarGradientOptions } from '../utils'
const useBarGradientChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item',
      axisPointer: {
        type: 'none'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const barGradientInitOptions = initOptions.value
  const barGradientCustomSeriesOptions: any = seriesBarGradientOptions(initOptions, systemConfig)
  return {
    barGradientInitOptions,
    barGradientCustomSeriesOptions
  }
}
export default useBarGradientChartOptions
