﻿import { extractType, initSeriesData, seriesColumnGradientOptions } from '../utils'
const useColumnGradientChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    yAxis: {
      type: 'value'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item',
      axisPointer: {
        type: 'none'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const columnGradientInitOptions = initOptions.value
  const columnGradientCustomSeriesOptions: any = seriesColumnGradientOptions(initOptions, systemConfig)
  return {
    columnGradientInitOptions,
    columnGradientCustomSeriesOptions
  }
}
export default useColumnGradientChartOptions
