﻿import { extractType, initSeriesData } from '../utils'
const useColumnChartOptions = (data, config) => {
  const columnInitOptions: any = {
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    yAxis: {
      type: 'value'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  return {
    columnInitOptions
  }
}
export default useColumnChartOptions
