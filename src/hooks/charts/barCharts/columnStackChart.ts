﻿import { extractType, initSeriesData } from '../utils'
const useColumnStackChartOptions = (data, config) => {
  const columnStackInitOptions: any = {
    xAxis: {
      data: extractType(data, config.axis),
      type: 'category',
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        show: false
      },
      splitArea: {
        show: true,
        interval: 0
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    grid: {
      top: 40
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const columnStackCustomSeriesOptions: any = [
    {
      stack: 'Total'
    },
    {
      stack: 'Total'
    }
  ]
  return {
    columnStackInitOptions,
    columnStackCustomSeriesOptions
  }
}
export default useColumnStackChartOptions
