﻿import { extractType, initGroupStackSeriesData } from '../utils'
const useBarGroupStackChartOptions = (data, config) => {
  const barGroupStackInitOptions: any = {
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    grid: {
      top: 40
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initGroupStackSeriesData(data, config.class, config.type, config.value, config.chartType)
  }
  return {
    barGroupStackInitOptions
  }
}
export default useBarGroupStackChartOptions
