﻿import { extractType, initSeriesData, rankContinuousVisualMap } from '../utils'
const useBarRankChartOptions = (data, config, systemConfig) => {
  const initOptions: any = ref({
    visualMap: {
      show: true,
      orient: 'horizontal',
      inverse: true,
      left: 'center'
    },
    grid: {
      bottom: 40
    },
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  })
  const barRankInitOptions = rankContinuousVisualMap(initOptions, systemConfig)
  return {
    barRankInitOptions
  }
}
export default useBarRankChartOptions
