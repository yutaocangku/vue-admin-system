﻿import { extractType, initSeriesData } from '../utils'
const useBarGroupChartOptions = (data, config) => {
  const barGroupInitOptions: any = {
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    grid: {
      top: 40
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const barGroupCustomSeriesOptions: any = [
    {
      markPoint: {
        data: [
          { type: 'max', name: 'Max' },
          { type: 'min', name: 'Min' }
        ]
      },
      markLine: {
        data: [{ type: 'average', name: 'Avg' }]
      }
    },
    {
      markPoint: {
        data: [
          { type: 'max', name: 'Max' },
          { type: 'min', name: 'Min' }
        ]
      },
      markLine: {
        data: [{ type: 'average', name: 'Avg' }]
      }
    }
  ]
  return {
    barGroupInitOptions,
    barGroupCustomSeriesOptions
  }
}
export default useBarGroupChartOptions
