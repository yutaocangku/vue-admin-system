﻿import { extractType, initSeriesData } from '../utils'
const useBarStackChartOptions = (data, config) => {
  const barStackInitOptions: any = {
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    grid: {
      top: 40
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const barStackCustomSeriesOptions: any = [
    {
      stack: 'Total'
    },
    {
      stack: 'Total'
    }
  ]
  return {
    barStackInitOptions,
    barStackCustomSeriesOptions
  }
}
export default useBarStackChartOptions
