﻿import { extractType, initSeriesData } from '../utils'
const useBarChartOptions = (data, config) => {
  const barInitOptions: any = {
    xAxis: {
      type: 'value'
    },
    yAxis: {
      data: extractType(data, config.axis),
      type: 'category'
    },
    legend: {
      data: extractType(data, config.type)
    },
    tooltip: {
      axisPointer: {
        type: 'shadow'
      }
    },
    series: initSeriesData(data, extractType(data, config.type), config.type, config.value, config.chartType)
  }
  const barCustomSeriesOptions: any = [
    {
      markPoint: {
        data: [
          { type: 'max', name: 'Max' },
          { type: 'min', name: 'Min' }
        ]
      },
      markLine: {
        data: [{ type: 'average', name: 'Avg' }]
      }
    }
  ]
  return {
    barInitOptions,
    barCustomSeriesOptions
  }
}
export default useBarChartOptions
