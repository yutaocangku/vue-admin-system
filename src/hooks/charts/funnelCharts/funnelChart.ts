﻿import { extractType } from '../utils'
const useFunnelChartOptions = (data, config) => {
  const initFunnelSeriesData = (data, config) => {
    const seriesData = []
    data.forEach((item) => {
      seriesData.push({
        name: item[config.type],
        value: item[config.value]
      })
    })
    return seriesData
  }
  const funnelInitOptions: any = {
    xAxis: {
      axisLine: {
        show: false
      }
    },
    yAxis: {
      axisLine: {
        show: false
      }
    },
    legend: {
      show: true,
      data: extractType(data, config.type)
    },
    tooltip: {
      trigger: 'item'
    },
    series: [
      {
        type: 'funnel',
        min: 0,
        max: data[0][config.value],
        minSize: '0%',
        maxSize: '100%',
        sort: 'none',
        gap: 1,
        label: {
          show: true,
          position: 'inside',
          color: '#ffffff'
        },
        data: initFunnelSeriesData(data, config)
      }
    ]
  }
  return {
    funnelInitOptions
  }
}
export default useFunnelChartOptions
