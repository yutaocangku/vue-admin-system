﻿import { getThemeColor } from './baseChartOptions'
import { colorMix, hexToRgb } from '@/utils/theme'
import XEUtils from 'xe-utils'
import * as echarts from 'echarts'
import { deepMerge } from '@/utils'
export const horizontalGradientVisualMap = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  initOptions.visualMap = []
  const chartThemeColor = getThemeColor(systemConfig)
  initOptions.legend.data.forEach((item, index) => {
    initOptions.visualMap.push({
      show: false,
      type: 'continuous',
      seriesIndex: index,
      dimension: 0,
      min: 0,
      max: XEUtils.isArray(initOptions.xAxis) ? initOptions.xAxis[0].data.length - 1 : initOptions.xAxis.data.length - 1,
      inRange: {
        color: [colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.2), chartThemeColor[index]]
      }
    })
  })
  return initOptions
}
export const verticalGradientVisualMap = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  initOptions.visualMap = []
  const chartThemeColor = getThemeColor(systemConfig)
  initOptions.legend.data.forEach((item, index) => {
    initOptions.visualMap.push({
      show: false,
      type: 'continuous',
      seriesIndex: index,
      min: 0,
      max: XEUtils.max(initOptions.series[index].data, ''),
      inRange: {
        color: [colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.2), chartThemeColor[index]]
      }
    })
  })
  return initOptions
}
export const rankContinuousVisualMap = (initChartOptions, systemConfig, isMap = false, isPrimary = false) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  let customVisualMap = {}
  if (XEUtils.isObject(initOptions.visualMap)) {
    customVisualMap = initOptions.visualMap
  }
  const initVisualMap = {
    type: 'continuous',
    dimension: 0,
    min: 0,
    max: isMap ? initOptions.series[0].data[0].value : XEUtils.max(initOptions.series[0].data, ''),
    inRange: {
      color: isMap && isPrimary ? [colorMix('#ffffff', chartThemeColor[0], 0.1), colorMix('#000000', chartThemeColor[0], 0.7)] : [chartThemeColor[4], chartThemeColor[3], chartThemeColor[2], chartThemeColor[1], chartThemeColor[0]]
    },
    textStyle: {
      color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.5)'
    }
  }
  initOptions.visualMap = deepMerge(customVisualMap, initVisualMap)
  return initOptions
}
export const rankPiecewiseVisualMap = (initChartOptions, systemConfig, isMap = false, isPrimary = false) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  const maxNum = isMap ? initOptions.series[0].data[0].value : parseInt(XEUtils.max(initOptions.series[0].data, ''))
  let minAverage = 0
  let pieces = []
  if (maxNum >= 4) {
    const avgNum: number = Math.trunc(maxNum / 4)
    const avgLength = avgNum.toString().length
    const avgFirstNum = parseInt(avgNum.toString().split('')[0])
    if (avgLength >= 2) {
      minAverage = avgFirstNum * Math.pow(10, avgLength - 1)
    } else {
      minAverage = avgFirstNum
    }
    pieces = [
      { gte: minAverage * 4, label: '≥' + minAverage * 4, color: isMap && isPrimary ? colorMix('#000000', chartThemeColor[0], 0.7) : chartThemeColor[0] },
      { gte: minAverage * 3, lt: minAverage * 4, label: minAverage * 3 + '-' + minAverage * 4, color: isMap && isPrimary ? chartThemeColor[0] : chartThemeColor[1] },
      { gte: minAverage * 2, lt: minAverage * 3, label: minAverage * 2 + '-' + minAverage * 3, color: isMap && isPrimary ? colorMix('#ffffff', chartThemeColor[0], 0.7) : chartThemeColor[2] },
      { gte: minAverage, lt: minAverage * 2, label: minAverage + '-' + minAverage * 2, color: isMap && isPrimary ? colorMix('#ffffff', chartThemeColor[0], 0.5) : chartThemeColor[3] },
      { lt: minAverage, label: '0-' + minAverage, color: isMap && isPrimary ? colorMix('#ffffff', chartThemeColor[0], 0.1) : colorMix('#ffffff', chartThemeColor[0], 0.2) }
    ]
  } else {
    minAverage = 4
    pieces = [{ lt: minAverage, label: '0-' + minAverage, color: isMap && isPrimary ? colorMix('#ffffff', chartThemeColor[0], 0.1) : colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[0], 0.2) }]
  }
  let customVisualMap = {}
  if (XEUtils.isObject(initOptions.visualMap)) {
    customVisualMap = initOptions.visualMap
  }
  const initVisualMap = {
    type: 'piecewise',
    dimension: 0,
    pieces: pieces,
    textStyle: {
      color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.5)'
    }
  }
  initOptions.visualMap = deepMerge(customVisualMap, initVisualMap)
  if (isMap) {
    initOptions.series[0].itemStyle.areaColor = systemConfig.value.isLight ? '#eeeeee' : 'rgba(0,0,0,.1)'
  }
  return initOptions
}

export const seriesLineGradientOptions = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  const seriesOptions: any = []
  initOptions.series.forEach((item, index) => {
    seriesOptions.push({
      symbol: 'none',
      smooth: true,
      lineStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 1, 1, [
          {
            offset: 0,
            color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.2)
          },
          {
            offset: 1,
            color: chartThemeColor[index]
          }
        ]),
        width: 4,
        shadowBlur: 6,
        shadowOffsetY: 8,
        shadowColor: 'rgba(' + hexToRgb(chartThemeColor[index]) + ',.3)'
      }
    })
  })
  return seriesOptions
}

export const initPieThemeOptions = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  initOptions.series.forEach((item) => {
    let currentLabel = {}
    if (item.label) {
      currentLabel = item.label
    }
    item.label = deepMerge(currentLabel, { color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.8)' })
  })
  return initOptions
}

export const seriesAreaGradientOptions = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  const seriesOptions: any = []
  initOptions.series.forEach((item, index) => {
    seriesOptions.push({
      symbol: 'none',
      smooth: true,
      lineStyle: {
        width: 0
      },
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: chartThemeColor[index]
          },
          {
            offset: 1,
            color: 'rgba(' + hexToRgb(chartThemeColor[index]) + ', 0)'
          }
        ])
      },
      emphasis: {
        focus: 'series'
      }
    })
  })
  return seriesOptions
}

export const seriesColumnGradientOptions = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  const seriesOptions: any = []
  initOptions.series.forEach((item, index) => {
    seriesOptions.push({
      showBackground: true,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
          {
            offset: 0,
            color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.4)
          },
          {
            offset: 0.5,
            color: chartThemeColor[index]
          },
          {
            offset: 1,
            color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.6)
          }
        ])
      },
      emphasis: {
        itemStyle: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
            {
              offset: 0,
              color: chartThemeColor[index]
            },
            {
              offset: 1,
              color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.4)
            }
          ])
        }
      }
    })
  })
  return seriesOptions
}

export const seriesBarGradientOptions = (initChartOptions, systemConfig) => {
  const initOptions: any = initChartOptions.value
  const chartThemeColor = getThemeColor(systemConfig)
  const seriesOptions: any = []
  initOptions.series.forEach((item, index) => {
    seriesOptions.push({
      showBackground: true,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 1, 1, [
          {
            offset: 0,
            color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.4)
          },
          {
            offset: 0.5,
            color: chartThemeColor[index]
          },
          {
            offset: 1,
            color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.6)
          }
        ])
      },
      emphasis: {
        itemStyle: {
          color: new echarts.graphic.LinearGradient(0, 0, 1, 1, [
            {
              offset: 0,
              color: chartThemeColor[index]
            },
            {
              offset: 1,
              color: colorMix(systemConfig.value.isLight ? '#ffffff' : '#000000', chartThemeColor[index], 0.4)
            }
          ])
        }
      }
    })
  })
  return seriesOptions
}

/**
 * 用来将原始数据转化为图表可展现的数据
 * @param data 数据源
 * @param typeArray 数据分类数组，用来做循环使用
 * @param typeKey 分类字段名
 * @param axisValueKey 值数据字段名
 * @param chartTypeArray 图表类型数组，
 * @returns 返回series数组数据
 */
export const initSeriesData = (data, typeArray, typeKey, axisValueKey, chartTypeArray) => {
  const seriesOptions = []
  if (typeArray.length >= 1) {
    typeArray.forEach((item, index) => {
      const initSeries = {
        name: item,
        type: chartTypeArray[index],
        data: []
      }
      data.forEach((item1) => {
        if (item === item1[typeKey]) {
          initSeries.data.push(item1[axisValueKey])
        }
      })
      seriesOptions.push(initSeries)
    })
  }
  return seriesOptions
}

/**
 * 用来将原始数据转化为图表可展现的数据
 * @param data 数据源
 * @param classKey 分组字段名
 * @param typeKey 分类字段名
 * @param axisValueKey 值数据字段名
 * @param chartTypeArray 图表类型数组，
 * @returns 返回series数组数据
 */
export const initGroupStackSeriesData = (data, classKey, typeKey, axisValueKey, chartTypeArray) => {
  const seriesOptions = []
  const typeList = extractType(data, classKey)
  const groupStackList = []
  typeList.forEach((item) => {
    const itemObj = {
      key: item,
      type: []
    }
    data.forEach((item1) => {
      if (item === item1[classKey]) {
        if (!itemObj.type.includes(item1[typeKey])) {
          itemObj.type.push(item1[typeKey])
        }
      }
    })
    groupStackList.push(itemObj)
  })
  groupStackList.forEach((item) => {
    item.type.forEach((item1) => {
      const initSeries = {
        name: item1,
        type: chartTypeArray[0],
        stack: item.key,
        emphasis: {
          focus: 'series'
        },
        data: []
      }
      data.forEach((item2) => {
        if (item2[classKey] === item.key && item2[typeKey] === item1) {
          initSeries.data.push(item2[axisValueKey])
        }
      })
      if (initSeries.data.length > 0) {
        seriesOptions.push(initSeries)
      }
    })
  })
  return seriesOptions
}

/**
 * 提取数据中某个分类的值，
 * @param data 数据源
 * @param type 提取分类的key
 * @returns 返回数组类型数据
 */
export const extractType = (data, type) => {
  const typeArray = []
  data.forEach((item) => {
    if (!typeArray.includes(item[type])) {
      typeArray.push(item[type])
    }
  })
  return typeArray
}

export const initPieData = (data, typeKey, valueKey) => {
  const initData = []
  data.forEach((item) => {
    initData.push({
      value: item[valueKey],
      name: item[typeKey]
    })
  })
  return initData
}

export const textThemeChange = (systemConfig) => {
  return systemConfig.value.isLight ? '#464646' : 'rgba(255,255,255,.8)'
}
export const labelThemeChange = (systemConfig) => {
  return systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.5)'
}
