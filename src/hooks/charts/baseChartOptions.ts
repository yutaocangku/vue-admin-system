﻿import { themeColor } from '@/utils/theme'
/**
 * echarts图表的基本配置，主要用来配置全局主题类、公共类配置
 */
export const baseChartOptions = (systemConfig) => {
  const chartOptions = {
    // 主题
    color: getThemeColor(systemConfig),
    // 标题
    title: {
      // 主标题
      textStyle: {
        color: systemConfig.value.isLight ? '#464646' : 'rgba(255,255,255,.8)'
      },
      // 副标题
      subtextStyle: {
        color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.6)'
      }
    },
    // 全局文本样式
    textStyle: {
      color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.5)'
    },
    // 图例
    legend: {
      show: false,
      // 公用文本样式
      textStyle: {
        color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.8)'
      }
    },
    grid: {
      left: 20,
      right: 20,
      bottom: 20,
      top: 20,
      containLabel: true
    },
    // 提示框
    tooltip: {
      trigger: 'axis',
      padding: 10
    },
    animationDuration: 2800,
    animationEasing: 'cubicInOut'
  }
  return chartOptions
}

export const getThemeColor = (systemConfig) => {
  const colorTheme = [themeColor().blue, themeColor().green, themeColor().red, themeColor().amber, themeColor().pink, themeColor().purple, themeColor().glass, themeColor().cyan, themeColor().orange, themeColor().gold]
  let currentIndex = 0
  colorTheme.forEach((item, index) => {
    if (item === systemConfig.value.primaryColor) {
      currentIndex = index
    }
  })
  let newColorTheme = []
  newColorTheme = colorTheme.slice(currentIndex)
  newColorTheme = newColorTheme.concat(colorTheme.slice(0, currentIndex))
  return newColorTheme
}
/**
 * 返回echarts图表x轴、y轴的基本配置参数
 * @param systemConfig 项目配置参数
 * @returns
 */
export const axisOptions = (systemConfig) => {
  return {
    // 坐标轴名称（轴线的名称，非轴线标签名称，通过xAxis.name配置轴线名称）
    nameTextStyle: {
      color: systemConfig.value.isLight ? '#eeeeee' : '#ffffff'
    },
    // 坐标轴线
    axisLine: {
      lineStyle: {
        color: systemConfig.value.isLight ? '#e0e6f1' : 'rgba(255,255,255,.1)'
      }
    },
    // 坐标轴刻度
    axisTick: {
      lineStyle: {
        color: systemConfig.value.isLight ? '#e0e6f1' : 'rgba(255,255,255,.1)'
      }
    },
    // 坐标轴在grid区域中的分割线
    splitLine: {
      lineStyle: {
        color: systemConfig.value.isLight ? '#e0e6f1' : 'rgba(255,255,255,.1)'
      }
    },
    // 坐标轴刻度标签
    axisLabel: {
      color: systemConfig.value.isLight ? '#6e7079' : 'rgba(255,255,255,.5)'
    },
    // 坐标轴在grid区域中的分割区域
    splitArea: {
      areaStyle: {
        color: systemConfig.value.isLight ? ['rgba(0,0,0,0.01)', 'rgba(0,0,0,0.03)'] : ['rgba(250,250,250,0.01)', 'rgba(200,200,200,0.03)']
      }
    },
    // 坐标轴次刻度
    minorTick: {
      lineStyle: {
        color: systemConfig.value.isLight ? '#333333' : '#ffffff'
      }
    },
    // 坐标轴在grid区域中的次分割线
    minorSplitLine: {
      lineStyle: {
        color: systemConfig.value.isLight ? '#333333' : '#ffffff'
      }
    }
  }
}
