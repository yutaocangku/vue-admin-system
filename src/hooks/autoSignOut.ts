﻿import useLogout from '@/hooks/useLogout'
import { storeToRefs } from 'pinia'
import { useSystemConfigStoreWithOut } from '@/store/storage/systemConfigStore'

const useAutoSignOut = (router) => {
  const systemConfigStore = useSystemConfigStoreWithOut()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const { Logout } = useLogout(router)
  const timer = ref(0) // 定时器控制字段
  let lastTime = new Date().getTime() // 用户最后一次操作的时间
  let currentTime = new Date().getTime() // 当前时间
  const checkTimeout = () => {
    currentTime = new Date().getTime() // 更新当前时间
    if (currentTime - lastTime > systemConfig.value.autoSignOutTime * 60 * 1000) {
      removeInterval()
      removeUserBehaviorListener()
      Logout()
    }
  }
  // 用户有操作行为后，更新最后一次操作时间
  const userBehavior = () => {
    lastTime = new Date().getTime() // 有任何操作，都需要更新最后一次操作时间
  }
  // 用户行为监听
  const userBehaviorListener = () => {
    window.addEventListener('keydown', userBehavior) // 用户有键盘按键的点击行为
    window.addEventListener('scroll', userBehavior, true) // 用户有鼠标滚轮滚动行为
    window.addEventListener('mousemove', userBehavior) // 用户有移动鼠标的行为
    window.addEventListener('mousedown', userBehavior) // 用户有鼠标点击行为
  }
  // 移除用户行为监听
  const removeUserBehaviorListener = () => {
    window.removeEventListener('keydown', userBehavior) // 用户有键盘按键的点击行为
    window.removeEventListener('scroll', userBehavior, true) // 用户有鼠标滚轮滚动行为
    window.removeEventListener('mousemove', userBehavior) // 用户有移动鼠标的行为
    window.removeEventListener('mousedown', userBehavior) // 用户有鼠标点击行为
  }
  // 启动定时器，监听当前是否已超过超时时间，用户仍未有任何操作行为
  const intervalStart = () => {
    timer.value = window.setInterval(checkTimeout, 1000)
  }
  // 移除定时器
  const removeInterval = () => {
    clearInterval(timer.value)
  }
  // 监听自动退出登录时间设置事件
  watch(
    () => systemConfig.value.autoSignOutTime,
    () => {
      removeUserBehaviorListener()
      removeInterval()
      if (systemConfig.value.autoSignOutTime !== 0) {
        userBehavior()
        intervalStart()
        userBehaviorListener()
      }
    }
  )
  // 监听登录状态
  watch(
    () => systemConfig.value.isLogin,
    () => {
      removeUserBehaviorListener()
      removeInterval()
      if (systemConfig.value.isLogin && systemConfig.value.autoSignOutTime !== 0) {
        userBehavior()
        intervalStart()
        userBehaviorListener()
      }
    }
  )
  onMounted(() => {
    if (systemConfig.value.isLogin && systemConfig.value.autoSignOutTime !== 0) {
      intervalStart()
      userBehaviorListener()
    }
  })
}
export default useAutoSignOut
