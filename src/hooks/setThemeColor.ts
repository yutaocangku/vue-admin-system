﻿import { setThemeColor } from '@/utils/theme'
import { addClass } from '@/utils/operate'
import { setDevice, getDevice } from '@/utils/deviceDetection'
import { storeToRefs } from 'pinia'
import { useSystemConfigStoreWithOut } from '@/store/storage/systemConfigStore'
const useSetThemeColor = () => {
  const systemConfigStore = useSystemConfigStoreWithOut()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const updateDevice = () => {
    systemConfig.value.device = getDevice()
    systemConfigStore.setSystemConfigCache()
    setDevice(systemConfig.value.device)
  }
  onMounted(() => {
    updateDevice()
    addEventListener('resize', updateDevice)
    setThemeColor(systemConfig.value)
    systemConfig.value.isGreyPatternEnable && addClass(document.querySelector('html'), 'html-grey')
    systemConfig.value.isColorWeaknessPatternEnable && addClass(document.querySelector('html'), 'html-color-weakness')
  })
}
export default useSetThemeColor
