﻿import { storeToRefs } from 'pinia'
import { transformI18n } from '@/plugins/i18n'
import { useTagCacheStore } from '@/store/storage/tagCacheStore'
const useUpdateMeta = () => {
  const tagCacheStore = useTagCacheStore()
  // 获取被缓存的标签页数据
  const { tagList } = storeToRefs(tagCacheStore)
  const updateTagMeta = (item, route) => {
    const toRoute = {
      path: route.fullPath,
      meta: route.meta
    }
    toRoute.meta.title = item.title + '-' + transformI18n('menus.' + route.meta.title, true)
    toRoute.meta.i18n = false
    tagCacheStore.updateTagMeta({ value: toRoute })
  }
  const getCurrentRouterMeta = (path) => {
    let currentMeta = {}
    tagList.value.forEach((item) => {
      if (item.path === path) {
        currentMeta = item.meta
      }
    })
    return currentMeta
  }
  return {
    getCurrentRouterMeta,
    updateTagMeta
  }
}
export default useUpdateMeta
