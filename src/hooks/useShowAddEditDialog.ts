﻿const useShowAddEditDialog = (AddEditRef) => {
  // 新增编辑事件
  const addEdit = (v) => {
    AddEditRef.value.showDialog(v)
  }
  return {
    addEdit
  }
}
export default useShowAddEditDialog
