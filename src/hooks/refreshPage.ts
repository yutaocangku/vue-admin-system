﻿import { toggleClass, removeClass } from '@/utils/operate'
const useRefreshPage = (route, router) => {
  const refreshPage = () => {
    // 执行刷新按钮的动画效果
    toggleClass(true, 'vk-refresh-rotate', document.querySelector('.vk-btn-refresh'))
    const { fullPath } = unref(route)
    // 替换路由，到重定向页面，重定向页面会自动跳转到需要跳转的页面
    router.replace('/redirect' + fullPath)
    // 移除刷新按钮的动画效果
    setTimeout(() => {
      removeClass(document.querySelector('.vk-btn-refresh'), 'vk-refresh-rotate')
    }, 600)
  }
  return {
    refreshPage
  }
}
export default useRefreshPage
