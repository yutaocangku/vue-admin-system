﻿import useAffixTopHeight from '@/hooks/affixTopHeight'
import { storeToRefs } from 'pinia'
import { usePublicStore } from '@/store/public/publicStore'
const useAffixPosition = () => {
  const { getAffixTop } = useAffixTopHeight()
  const publicStore = usePublicStore()
  const { affixTop } = storeToRefs(publicStore)
  onMounted(() => {
    affixTop.value = getAffixTop()
  })
}
export default useAffixPosition
