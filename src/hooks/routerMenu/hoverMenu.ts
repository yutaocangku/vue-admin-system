﻿const useHoverMenu = (menuTextRef) => {
  // 存放菜单是否存在showTooltip属性标识
  const hoverMenuMap = new WeakMap()

  const hoverMenu = (key) => {
    // 如果当前菜单showTooltip属性已存在，退出计算
    if (hoverMenuMap.get(key)) return
    nextTick(() => {
      // 如果文本内容的整体宽度大于其可视宽度，则文本溢出
      menuTextRef.value?.scrollWidth > menuTextRef.value?.clientWidth
        ? Object.assign(key, {
            showTooltip: true
          })
        : Object.assign(key, {
            showTooltip: false
          })
      hoverMenuMap.set(key, true)
    })
  }
  return {
    hoverMenu
  }
}
export default useHoverMenu
