﻿const useActiveMenu = (route, isRoot) => {
  // 获取当前选中菜单的path
  const activeMenu = computed((): string => {
    let path = ''
    if (isRoot) {
      if (route.meta.breadcrumb && (route.meta.breadcrumb as any).length > 0) {
        path = route.meta.breadcrumb[0].path
      } else {
        path = route.path
      }
    } else {
      if (!route.meta.showLink) {
        if (route.meta.breadcrumb && (route.meta.breadcrumb as any).length > 0) {
          const newBreadcrumb = (route.meta.breadcrumb as any).filter(function (item, index) {
            return index !== 0 && index !== (route.meta.breadcrumb as any).length - 1
          })
          for (let i = newBreadcrumb.length - 1; i >= 0; i--) {
            if (newBreadcrumb[i].showLink) {
              path = newBreadcrumb[i].path
              break
            }
          }
          if (path === '') {
            path = route.path
          }
        } else {
          path = route.path
        }
      } else {
        path = route.path
      }
    }
    return path
  })
  return {
    activeMenu
  }
}
export default useActiveMenu
