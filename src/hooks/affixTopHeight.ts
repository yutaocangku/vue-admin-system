﻿import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useAffixTopHeight = () => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const getAffixTop = () => {
    let affixTopHeight = 0
    if (systemConfig.value.isFixedHeader) {
      if (systemConfig.value.isSidebarHidden) {
        if (systemConfig.value.isTagEnable) {
          affixTopHeight = parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-tag-height')) + systemConfig.value.space + 1
        } else {
          affixTopHeight = systemConfig.value.space
        }
      } else {
        if (systemConfig.value.isTagEnable) {
          affixTopHeight = parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-header-height')) + parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-tag-height')) + systemConfig.value.space + 1
        } else {
          affixTopHeight = parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-header-height')) + systemConfig.value.space
        }
      }
    } else {
      affixTopHeight = systemConfig.value.space
    }
    return affixTopHeight
  }
  return {
    getAffixTop
  }
}
export default useAffixTopHeight
