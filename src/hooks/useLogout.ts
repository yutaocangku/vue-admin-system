import { user } from '@/api/user'
import { useTagCacheStore } from '@/store/storage/tagCacheStore'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
import { useUserInfoStoreWithOut } from '@/store/storage/userInfoStore'
import { useKeepAliveStoreWithOut } from '@/store/keepAlive/keepAliveStore'
import { resetRouter } from '@/router/utils/routeOperate'
import { storeToRefs } from 'pinia'
const useLogout = (router) => {
  const tagCacheStore = useTagCacheStore()
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const userInfoStore = useUserInfoStoreWithOut()
  const keepAliveStore = useKeepAliveStoreWithOut()
  const Logout = () => {
    user.logout(null, { config: { mockEnable: true } }).then(() => {
      userInfoStore.removeUserInfoCache()
      tagCacheStore.removeAllTagCache()
      tagCacheStore.removeFixedTagCache()
      keepAliveStore.removeAllKeepAlive()
      systemConfig.value.isLogin = false
      systemConfigStore.setSystemConfigCache()
      resetRouter()
      nextTick(() => {
        if (!router.currentRoute.value.fullPath.includes('/Login?redirect=')) {
          router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`)
        }
      })
      // setTimeout(() => {
      //   location.reload()
      // }, 10)
    })
  }
  return {
    Logout
  }
}
export default useLogout
