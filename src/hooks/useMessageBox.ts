﻿import svg from '@/utils/messageBoxSvg'
import { transformI18n } from '@/plugins/i18n'
const useMessageBox = () => {
  const messageBox = (type, isDanger, msgHtml, tipsMsgHtml, callbackFun, cancelFun = undefined) => {
    const result = '<div class="message-box ' + (isDanger ? 'danger' : '') + '"><div class="icon-svg">' + svg[type] + '</div><div class="confirm-text">' + msgHtml + '</div>' + (tipsMsgHtml ? '<div class="tips-text">' + tipsMsgHtml + '</div></div>' : '</div>')

    ElMessageBox.alert(result, '', {
      dangerouslyUseHTMLString: true,
      showCancelButton: true,
      confirmButtonText: transformI18n('buttons.buttonConfirm', true),
      cancelButtonText: transformI18n('buttons.buttonCancel', true),
      callback: (action) => {
        if (action === 'confirm') {
          if (callbackFun) {
            callbackFun()
          }
        } else {
          if (cancelFun) {
            cancelFun()
          }
        }
      }
    })
  }

  return { messageBox }
}
export default useMessageBox
