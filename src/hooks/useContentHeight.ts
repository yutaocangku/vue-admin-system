﻿import { useResizeObserver, useDebounceFn } from '@vueuse/core'
import { storeToRefs } from 'pinia'
import { useSystemConfigStore } from '@/store/storage/systemConfigStore'
const useContentHeight = () => {
  const systemConfigStore = useSystemConfigStore()
  const { systemConfig } = storeToRefs(systemConfigStore)
  const contentHeight = ref(0)
  const getContentHeight = () => {
    const rect = document.body.getBoundingClientRect()
    let currentHeight = rect.height
    if (!systemConfig.value.isSidebarHidden) {
      currentHeight = currentHeight - parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-header-height')) - 1
    }
    if (systemConfig.value.isTagEnable) {
      currentHeight = currentHeight - parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-tag-height'))
    }
    if (systemConfig.value.isFooterEnable) {
      currentHeight = currentHeight - parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-footer-height'))
    }
    return currentHeight
  }
  watch(
    () => systemConfig.value.isSidebarHidden,
    () => {
      contentHeight.value = getContentHeight()
    }
  )
  watch(
    () => systemConfig.value.isTagEnable,
    () => {
      contentHeight.value = getContentHeight()
    }
  )
  watch(
    () => systemConfig.value.isFooterEnable,
    () => {
      contentHeight.value = getContentHeight()
    }
  )
  // 监听元素尺寸变化
  useResizeObserver(
    document.body,
    useDebounceFn(() => {
      contentHeight.value = getContentHeight()
    }, 200)
  )
  return {
    contentHeight
  }
}
export default useContentHeight
