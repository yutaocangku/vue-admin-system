﻿import { http } from '../utils/http'

// 用户相关
export const user = {
  login: (data, config) => http.request('post', 'Login/login', data, config),
  logout: (data, config) => http.request('post', '/logout', data, config),
  refreshToken: (data, config) => http.request('post', '/refreshToken', data, config),
  list: (data, config) => http.request('post', '/getUserListData', data, config),
  register: (data, config) => http.request('post', '/register', data, config),
  code: (data, config) => http.request('post', '/getCode', data, config),
  recover: (data, config) => http.request('post', '/recover', data, config)
}

// 个人中心
export const person = {
  avatar: (data, config) => http.request('post', '/User/headimg', data, config),
  save: (data, config) => http.request('post', '/User/personset', data, config),
  changePwd: (data, config) => http.request('post', '/User/personpwd', data, config),
  info: (data, config) => http.request('post', '/User/userInfo', data, config)
}
