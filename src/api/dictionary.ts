import { http } from '../utils/http'

// 获取字典数据
export const getDictionaryList = (data, config) => {
  return http.request('post', '/getDictionaryListData', data, config)
}

// 获取字典项数据
export const getDictionaryEntryList = (data, config) => {
  return http.request('post', '/getDictionaryEntryListData', data, config)
}
