﻿import { http } from '../utils/http'

// 获取分页列表数据
export const getPaginationList = (data, config) => {
  return http.request('post', '/getPaginationListData', data, config)
}

// 获取分页列表数据
export const getInfiniteScrollList = (data, config) => {
  return http.request('post', '/getInfiniteScrollListData', data, config)
}

// 获取详情页数据
export const getArticleInfo = (data, config) => {
  return http.request('post', '/getArticleData', data, config)
}
