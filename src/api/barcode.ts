﻿import { http } from '../utils/http'

// 获取详情
export const getBarcodeList = (data, config: object) => {
  return http.request('post', '/getBarcodeData', data, config)
}
