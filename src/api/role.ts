import { http } from '../utils/http'

// 获取部门数据
export const getRoleList = (data, config) => {
  return http.request('post', '/getRoleListData', data, config)
}
