import { http } from '../utils/http'

// 获取部门数据
export const getDepartmentList = (data, config) => {
  return http.request('post', '/getDepartmentListData', data, config)
}
