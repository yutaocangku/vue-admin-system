import { http } from '../utils/http'

// 获取部门数据
export const getSystemLogList = (data, config) => {
  return http.request('post', '/getSystemLogListData', data, config)
}
