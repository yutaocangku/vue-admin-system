import { http } from '../utils/http'

export const base = {
  person: (data, config) => http.request('post', '/Api/getpersoninfo', data, config),
  menu: (data, config) => http.request('post', '/Api/getmenu', data, config),
  permit: (data, config) => http.request('post', '/Api/getpermit', data, config),
  menuPermit: (data, config) => http.request('post', '/Api/currentmenupermit', data, config),
  upload: (data, config) => http.request('post', '/Api/uping', data, config),
  uploadMore: (data, config) => http.request('post', '/Api/upingmore', data, config),
  notice: (data, config) => http.request('post', '/noticeData', data, config)
}
