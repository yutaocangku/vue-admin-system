import { http } from '../utils/http'

// 抽奖结果
export const lottery = {
  result: (data, config) => http.request('post', '/getLotteryResult', data, config)
}
