import { http } from '../utils/http'

export const bigScreen = {
  group: {
    list: (data, config) => http.request('post', '/getBigScreenGroup', data, config),
    add: (data, config) => http.request('post', '/addBigScreenGroup', data, config),
    edit: (data, config) => http.request('post', '/editBigScreenGroup', data, config),
    delete: (data, config) => http.request('post', '/removeBigScreenGroup', data, config)
  },
  list: {
    list: (data, config) => http.request('post', '/getBigScreenList', data, config),
    add: (data, config) => http.request('post', '/addBigScreen', data, config),
    edit: (data, config) => http.request('post', '/editBigScreen', data, config),
    delete: (data, config) => http.request('post', '/removeBigScreen', data, config),
    info: (data, config) => http.request('post', '/bigScreenInfo', data, config),
    release: (data, config) => http.request('post', '/releaseBigScreen', data, config)
  }
}
