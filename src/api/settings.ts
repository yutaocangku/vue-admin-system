import { http } from '../utils/http'

// 部门
export const depart = {
  add: (data, config) => http.request('post', '/Depart/add', data, config),
  delete: (data, config) => http.request('post', '/Depart/delete', data, config),
  edit: (data, config) => http.request('post', '/Depart/edit', data, config),
  search: (data, config) => http.request('post', '/Depart/index', data, config)
}

// 用户
export const user = {
  add: (data, config) => http.request('post', '/User/add', data, config),
  delete: (data, config) => http.request('post', '/User/delete', data, config),
  edit: (data, config) => http.request('post', '/User/edit', data, config),
  search: (data, config) => http.request('post', '/User/index', data, config),
  reset: (data, config) => http.request('post', '/User/resetpwd', data, config),
  depart: (data, config) => http.request('post', '/User/userdepart', data, config),
  role: {
    list: (data, config) => http.request('post', '/User/getuserrole', data, config),
    already: (data, config) => http.request('post', '/User/userrole', data, config),
    save: (data, config) => http.request('post', '/User/getrole', data, config)
  }
}

// 角色
export const role = {
  add: (data, config) => http.request('post', '/Role/add', data, config),
  delete: (data, config) => http.request('post', '/Role/delete', data, config),
  edit: (data, config) => http.request('post', '/Role/edit', data, config),
  search: (data, config) => http.request('post', '/Role/index', data, config),
  reset: (data, config) => http.request('post', '/Role/resetpwd', data, config),
  depart: (data, config) => http.request('post', '/Role/userdepart', data, config),
  permit: {
    list: (data, config) => http.request('post', '/Role/userrolepermit', data, config),
    already: (data, config) => http.request('post', '/Role/rolepermit', data, config),
    save: (data, config) => http.request('post', '/Role/getpermit', data, config)
  },
  user: {
    list: (data, config) => http.request('post', '/Role/getmyuser', data, config),
    already: (data, config) => http.request('post', '/Role/roleuser', data, config),
    save: (data, config) => http.request('post', '/Role/getuser', data, config)
  }
}

// 权限
export const permit = {
  add: (data, config) => http.request('post', '/Permit/add', data, config),
  delete: (data, config) => http.request('post', '/Permit/delete', data, config),
  edit: (data, config) => http.request('post', '/Permit/edit', data, config),
  search: (data, config) => http.request('post', '/Permit/index', data, config)
}

// 菜单
export const menu = {
  add: (data, config) => http.request('post', '/Menu/add', data, config),
  delete: (data, config) => http.request('post', '/Menu/delete', data, config),
  edit: (data, config) => http.request('post', '/Menu/edit', data, config),
  search: (data, config) => http.request('post', '/Menu/index', data, config)
}

// 日志
export const logs = {
  action: (data, config) => http.request('post', '/Logs/getallaction', data, config),
  search: (data, config) => http.request('post', '/Logs/index', data, config)
}
