import { http } from '../utils/http'

// 获取部门数据
export const getBpmnProcessListData = (data, config) => {
  return http.request('post', '/getBpmnProcessListData', data, config)
}

export const getBpmnProcessSonListData = (data, config) => {
  return http.request('post', '/getBpmnProcessSonListData', data, config)
}
