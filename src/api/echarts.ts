﻿import { http } from '../utils/http'

// 获取echarts数据
export const getEchartsData = (data: object, config: object) => {
  return http.request('post', '/getChartListData', data, config)
}
