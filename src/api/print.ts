﻿import { http } from '../utils/http'

// 获取详情
export const getPrintInfo = (data, config: object) => {
  return http.request('post', '/getPrintDetail', data, config)
}
