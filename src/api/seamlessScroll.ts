﻿import { http } from '../utils/http'

// 获取列表
export const getListData = (data: object, config: object) => {
  return http.request('post', '/getSeamlessScrollData', data, config)
}
