﻿import { http } from '../utils/http'

// 获取详情
export const uploadApi = (data, config: object) => {
  return http.request('post', '/uploadFile', data, config)
}
