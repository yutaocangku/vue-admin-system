﻿import { http } from '../utils/http'

// 获取列表
export const getListData = (data: object, config: object) => {
  return http.request('post', '/getList', data, config)
}
// 获取详情
export const getDetailData = (data: object, config: object) => {
  return http.request('post', '/getDetail', data, config)
}

// 获取详情
export const getDetailInfo = (data: object, config: object) => {
  return http.request('post', '/getDetailInfo', data, config)
}

// 获取详情
export const getRichTextDetailInfo = (data: object, config: object) => {
  return http.request('post', '/getRichTextDetail', data, config)
}
