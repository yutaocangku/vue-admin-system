﻿import { http } from '../utils/http'

// 获取站点数据
export const getWebsiteList = (data, config) => {
  return http.request('post', '/getWebsiteListData', data, config)
}

// 获取网站页面数据
export const getWebsitePageList = (data, config) => {
  return http.request('post', '/getWebsitePageListData', data, config)
}

// 获取页面数据
export const getPageList = (data, config) => {
  return http.request('post', '/getPageListData', data, config)
}

// 获取模块数据
export const getModuleList = (data, config) => {
  return http.request('post', '/getModuleListData', data, config)
}
