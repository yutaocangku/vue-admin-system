﻿import { http } from '../utils/http'

// 获取详情
export const getUploadDataInFormData = (data, config: object) => {
  return http.request('post', '/uploadDataInFormData', data, config)
}
