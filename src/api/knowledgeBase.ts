import { http } from '../utils/http'

// 知识库
export const knowledge = {
  type: (data, config) => http.request('post', 'Login/login', data, config),
  list: (data, config) => http.request('post', '/getUserListData', data, config),
  tag: (data, config) => http.request('post', '/register', data, config),
  info: (data, config) => http.request('post', '/getCode', data, config)
}
