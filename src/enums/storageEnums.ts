export const nameSpace = 'CK'
export enum StorageEnum {
  // 全局设置
  CK_SYSTEM_SETTING_STORE = nameSpace + '_SYSTEM_SETTING',
  // token 等信息
  CK_ACCESS_TOKEN_STORE = nameSpace + '_ACCESS_TOKEN',
  // 用户信息
  CK_USER_INFO_STORE = nameSpace + '_USER_INFO',
  // 固定的标签栏
  CK_FIXED_TAG_STORE = nameSpace + '_FIXED_TAG',
  // 全部标签栏
  CK_ALL_TAG_STORE = nameSpace + '_ALL_TAG',
  // 登录验证码倒计时
  CK_SIGN_COUNTDOWN_STORE = nameSpace + '_SIGN_COUNTDOWN',
  // 找回密码验证码倒计时
  CK_RECOVER_COUNTDOWN_STORE = nameSpace + '_RECOVER_COUNTDOWN',
  // 注册验证码倒计时
  CK_REGISTER_COUNTDOWN_STORE = nameSpace + '_REGISTER_COUNTDOWN',
  // 登录模板
  CK_LOGIN_TEMPLATE_STORE = nameSpace + '_LOGIN_TEMPLATE',
  // 记住密码
  CK_REMEMBER_PASSWORD_STORE = nameSpace + '_REMEMBER_PASSWORD',
  // 语种
  CK_LOCALE_STORE = nameSpace + '_LOCALE'
}
