﻿import { createWebHistory, createRouter } from 'vue-router'
import { routerWhiteList, setPageTitle } from './utils/public'
import { cloneDeep, split } from 'lodash-es'
import { initRouter } from './utils/dataOperate'
import { handleAliveRoute } from './utils/keepAliveOperate'
import { getConstantRoutes } from './utils/routeOperate'
import NProgress from '@/utils/progress'
import { openLink } from '@/utils/link'
import { pendingList, pendingMap } from '@/utils/http/cancelToken'
import { endLoading } from '@/utils/http/loading'
import { initRequest } from '@/utils/http'
import { useTagCacheStoreWithOut } from '@/store/storage/tagCacheStore'
import { useRouteMenuStoreWithOut } from '@/store/routeMenu/routeMenuStore'
import { useKeepAliveStoreWithOut } from '@/store/keepAlive/keepAliveStore'
import { useUserInfoStoreWithOut } from '@/store/storage/userInfoStore'
import { useSystemConfigStoreWithOut } from '@/store/storage/systemConfigStore'
import { storeToRefs } from 'pinia'
import remainingRouter from './remaining'
import { loadEnv } from '^/index'
const { VITE_PUBLIC_PATH } = loadEnv()
const tagCacheStore = useTagCacheStoreWithOut()
const routeMenuStore = useRouteMenuStoreWithOut()
const keepAliveStore = useKeepAliveStoreWithOut()
const userInfoStore = useUserInfoStoreWithOut()
const systemConfigStore = useSystemConfigStoreWithOut()

// 创建路由实例
export const router = createRouter({
  history: createWebHistory(VITE_PUBLIC_PATH),
  routes: getConstantRoutes()
})

router.beforeEach((to, _from, next) => {
  // 获取被缓存的标签页数据
  const { tagList } = storeToRefs(tagCacheStore)
  const { systemConfig } = storeToRefs(systemConfigStore)
  const { routeMenuList } = storeToRefs(routeMenuStore)
  const { scrollPosition } = storeToRefs(keepAliveStore)
  const { userInfo } = storeToRefs(userInfoStore)
  userInfoStore.getUserInfoCache()
  initRequest()
  // 遍历pendingMap，将页面的所有请求取消掉
  if (pendingList.length > 0) {
    endLoading()
    pendingMap.forEach(function (cancel) {
      cancel()
    })
    pendingMap.clear()
  }
  // 标签栏刷新按钮清除保留的滚动记录
  if (_from.name === 'redirect') {
    keepAliveStore.removeScrollPosition({ path: '/' + _from.params.path })
  }
  // 页面滚动位置缓存
  if (systemConfig.value && systemConfig.value.isKeepAlive && _from.meta.keepAlive) {
    let hasRouteTag = false
    tagList.value.forEach((item) => {
      if (item.path === _from.fullPath) {
        hasRouteTag = true
      }
    })
    if (hasRouteTag && document.querySelector('.vk-scroll-wrap')) {
      const itemPosition: any = {}
      itemPosition.name = _from.name
      itemPosition.fullPath = _from.fullPath
      itemPosition.savedPosition = document.querySelector('.vk-scroll-wrap').scrollTop
      keepAliveStore.setScrollPosition(itemPosition)
    }
  }
  if (systemConfig.value && systemConfig.value.isKeepAlive && to.meta?.keepAlive) {
    // 添加页面状态缓存
    const newMatched = to.matched
    handleAliveRoute(newMatched, 'add')
    // 页面整体刷新和点击标签页刷新
    if (_from.name === undefined || _from.name === 'redirect') {
      handleAliveRoute(newMatched)
    }
    if (tagList.value.filter((e) => e.path === to.fullPath).length === 0 && tagList.value.filter((e) => e.name === to.name).length > 0 && (!to.meta.showTag || to.meta.showTag === 1)) {
      handleAliveRoute(newMatched)
      scrollPosition.value.forEach((item) => {
        if (item.name === to.name) {
          keepAliveStore.removeScrollPosition({ path: item.fullPath })
        }
      })
    }
  }
  if (systemConfig.value && systemConfig.value.isProgressEnable) {
    NProgress.start()
  }
  const externalLink = to?.redirectedFrom?.fullPath
  // 配置页面标题
  if (!externalLink || externalLink === '/' || (externalLink && to.path !== '/Exception/404')) {
    setPageTitle(to.meta)
  }
  if (userInfo.value) {
    if (_from?.name === 'Login' || !_from?.name) {
      // 如果是从登录页过来的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
      if (routeMenuList.value.length === 0) {
        initRouter().then(() => {
          let routerPath = ''
          if (to.fullPath === '/') {
            if (tagList.value && tagList.value.length > 0) {
              routerPath = tagList.value[0].path
            } else {
              const remainingRouterName = []
              remainingRouter.forEach((item) => {
                remainingRouterName.push(item.name)
              })
              const rootRouter = cloneDeep(router.options.routes)
              const rootArr = rootRouter.filter((v) => {
                return v.name !== 'Layout' && !remainingRouterName.includes(v.name)
              })
              const index = router.options.routes.findIndex((v) => v.name === 'Layout')
              if (router.options.routes[index].children && router.options.routes[index].children.length > 0) {
                routerPath = router.options.routes[index].children[0].path
              } else {
                if (rootArr.length > 0) {
                  routerPath = rootArr[0].path
                } else {
                  routerPath = '/Exception/403'
                }
              }
            }
          } else {
            routerPath = to.path
          }
          // 解决动态获取的路由在页面刷新时控制台会报警告问题
          if (to.path === '/Exception/404' && to.redirectedFrom !== undefined) {
            next({ path: to.redirectedFrom?.fullPath, query: to.redirectedFrom?.query, replace: true })
          } else {
            // router.push(routerPath)
            if (to.path === routerPath && to.name) {
              next()
            } else {
              next({ path: routerPath, query: to.query, replace: true })
            }
          }
        })
      } else {
        if (to.name) {
          tagCacheStore.dynamicRouteTag(to)
        }
        next()
      }
    } else {
      // 如果是其他页面跳转的，且其他页面的name存在，则再判断当前跳转页是否是外链页面
      if (externalLink && externalLink.includes('http')) {
        openLink(`http${split(externalLink, 'http')[1]}`)
        if (systemConfig.value && systemConfig.value.isProgressEnable) {
          NProgress.done()
        }
      } else {
        tagCacheStore.dynamicRouteTag(to)
        let routerPath = ''
        if (to.fullPath === '/') {
          if (tagList.value && tagList.value.length > 0) {
            routerPath = tagList.value[0].path
          } else {
            const remainingRouterName = []
            remainingRouter.forEach((item) => {
              remainingRouterName.push(item.name)
            })
            const rootRouter = cloneDeep(router.options.routes)
            const rootArr = rootRouter.filter((v) => {
              return v.name !== 'Layout' && !remainingRouterName.includes(v.name)
            })
            const index = router.options.routes.findIndex((v) => v.name === 'Layout')
            if (router.options.routes[index].children && router.options.routes[index].children.length > 0) {
              routerPath = router.options.routes[index].children[0].path
            } else {
              if (rootArr.length > 0) {
                routerPath = rootArr[0].path
              } else {
                routerPath = '/Exception/403'
              }
            }
          }
          next({ path: routerPath, replace: true })
        } else {
          next()
        }
      }
    }
  } else {
    // 未登录
    // 当前访问页面不是登陆页面
    if (to.path !== '/Login') {
      // 当前访问页面包含在路由白名单中，可直接跳转
      if (routerWhiteList.login.includes(to.path)) {
        next()
      } else {
        // 当前访问页面需要登录才能访问，需先跳转到登陆页，并将当前访问路由通过链接保存，在登陆后，获取该路由进行跳转
        next(`/Login?redirect=${to.path}`)
      }
    } else {
      // 当前访问页面本身就是登录页，直接跳转
      next()
    }
  }
})

router.afterEach(() => {
  const { systemConfig } = storeToRefs(systemConfigStore)
  if (systemConfig.value && systemConfig.value.isProgressEnable) {
    NProgress.done()
  }
})
export default router
