/**
 * 静态管理的路由可以与线上后端返的路由数据格式一致，静态管理路由存在需登录才能实例化的路由，可以根据permit字段来区分是静态管理的可直接实例化路由，还是需要登录后获取权限才能实例化的路由
 * 登录后获取后端返回的当前用户所拥有的所有权限标识，去筛选permit字段有值，且用户所有权限标识中包含该值，这是用户可访问的路由页面，需要被实例化
 * 目录层permit字段永远为空，其只在有子级菜单层时，需要被展示及实例化，目录层需要设置url、path、redirect，且也需要实例化，因为面包屑点击目录层需要能进行路由跳转，并通过redirect实现重定向到默认页面
 **/
export const constantRoutes = [
  { id: 6, pid: 0, title: '其它', icon: 'ri:magic-fill', url: '/Others', path: '/Others', redirect: '/Others/Test/A', showLink: true, sort: 5, route: 'Others', transitionName: '', keepAlive: true, isFixed: false, isCatalog: true, badge: '', hot: false, status: true, permit: '' },
  { id: 101, pid: 6, title: '测试', icon: 'ri:bug-fill', url: '/Others/Test', path: '/Others/Test', redirect: '/Others/Test/A', showLink: true, sort: 0, route: 'Test', transitionName: '', keepAlive: true, isFixed: false, isCatalog: true, badge: '', hot: false, status: true, permit: '' },
  { id: 102, pid: 101, title: '测试A', icon: '', url: '/Others/Test/A', path: '/Others/Test/A', redirect: '', showLink: true, sort: 0, route: 'AbTestA', transitionName: '', keepAlive: true, isFixed: false, isCatalog: false, badge: '', hot: false, status: true, permit: '' },
  { id: 103, pid: 101, title: '测试B', icon: '', url: '/Others/Test/B', path: '/Others/Test/B', redirect: '', showLink: true, sort: 1, route: 'AbTestB', transitionName: '', keepAlive: true, isFixed: false, isCatalog: false, badge: '', hot: false, status: true, permit: '' },
  {
    id: 130,
    pid: 6,
    title: '动态路由',
    icon: 'ri:link',
    url: '/Others/AsyncRouter',
    path: '/Others/AsyncRouter',
    redirect: '',
    showLink: true,
    sort: 1,
    route: 'AsyncRouter',
    transitionName: '',
    keepAlive: true,
    isFixed: false,
    isCatalog: false,
    badge: '',
    hot: false,
    status: true,
    permit: 'AsyncRouter'
  },
  {
    id: 131,
    pid: 6,
    title: '知识库',
    icon: 'ri:book-3-fill',
    url: '/Others/KnowledgeBase',
    path: '/Others/KnowledgeBase',
    redirect: '',
    showLink: true,
    sort: 2,
    route: 'KnowledgeBase',
    transitionName: '',
    keepAlive: true,
    isFixed: true,
    isCatalog: false,
    badge: '',
    dot: false,
    status: true,
    permit: ''
  }
]
