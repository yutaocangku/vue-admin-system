﻿const remainingRouter = [
  {
    path: '/Login',
    name: 'Login',
    component: () => import('@/views/Login/index.vue'),
    meta: {
      title: '登录',
      i18n: true,
      showLink: false,
      sort: 10000,
      showTag: 0
    }
  },
  {
    path: '/Exception/403',
    name: 'Exception403',
    component: () => import('@/views/Pages/Exception/403/index.vue'),
    meta: {
      title: '403',
      showLink: false,
      sort: 10001,
      showTag: 0
    }
  },
  {
    path: '/Exception/404',
    name: 'Exception404',
    component: () => import('@/views/Pages/Exception/404/index.vue'),
    meta: {
      title: '404',
      showLink: false,
      sort: 10002,
      showTag: 0
    }
  },
  {
    path: '/Exception/500',
    name: 'Exception500',
    component: () => import('@/views/Pages/Exception/500/index.vue'),
    meta: {
      title: '500',
      showLink: false,
      sort: 10003,
      showTag: 0
    }
  },
  {
    path: '/Exception/NetworkError',
    name: 'NetworkError',
    component: () => import('@/views/Pages/Exception/NetworkError/index.vue'),
    meta: {
      title: 'Network Error',
      showLink: false,
      sort: 10004,
      showTag: 0
    }
  },
  {
    path: '/Exception/Developmenting',
    name: 'Developmenting',
    component: () => import('@/views/Pages/Exception/Developmenting/index.vue'),
    meta: {
      title: 'Developmenting',
      showLink: false,
      sort: 10005,
      showTag: 0
    }
  },
  {
    path: '/Exception/UpgradeRepair',
    name: 'UpgradeRepair',
    component: () => import('@/views/Pages/Exception/UpgradeRepair/index.vue'),
    meta: {
      title: 'Upgrade Repair',
      showLink: false,
      sort: 10006,
      showTag: 0
    }
  },
  {
    path: '/redirect/:path(.*)',
    name: 'redirect',
    component: () => import('@/views/Redirect/index.vue'),
    meta: {
      showLink: false,
      sort: 10007,
      showTag: 0
    }
  }
]

export default remainingRouter
