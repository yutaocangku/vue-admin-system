﻿import { createProdMockServer } from 'vite-plugin-mock/es/createProdMockServer'

const modules = import.meta.globEager('../mock/*.ts')

const mockModules: any[] = []
Object.keys(modules).forEach((key) => {
  modules[key].default.forEach((item) => {
    mockModules.push(item)
  })
})
export function setupProdMockServer() {
  createProdMockServer(mockModules)
}
