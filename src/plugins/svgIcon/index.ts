﻿import antDesign from '@iconify/json/json/ant-design.json' // Ant Design 图标集    1
import bi from '@iconify/json/json/bi.json' // Bootstrap 图标集                    2
import ep from '@iconify/json/json/ep.json' // Element Plus 图标集                 3
import iconfont from '@/assets/iconfont/iconfont.json' // Iconfont 图标集          5
import ri from '@iconify/json/json/ri.json' // Remix 图标集                        6
// 获取src/assets/svg文件夹下的 自定义svg图标集
const customSvgIconList = () => {
  // 自动导入/src/assets/svg文件夹下的所有svg图标
  const modulesFiles = import.meta.globEager('@/assets/svg/*.svg')
  const pathList: string[] = []
  for (const path in modulesFiles) {
    pathList.push(path)
  }
  return pathList.reduce((modules: any, modulePath: string) => {
    const moduleName = modulePath.replace(/^\.\.\/\.\.\/assets\/svg\/(.*)\.\w+$/, '$1')
    const value = modulesFiles[modulePath]
    modules[moduleName] = value
    return modules
  }, {})
}
export const customSvgIconComponents = customSvgIconList() // 自定义svg图标集      7
// 将自定义图标在全局注册
// export function useCustomSvgIcon(app: App) {
//   // 注册图标
//   for(const key in customSvgIconComponents){
//     app.component(key, customSvgIconComponents[key].default);
//   }
// }
// 暴露图标集类型选择数据
export const iconTypeList = () => {
  return [
    { label: 'Ant Design', value: 'ad' },
    { label: 'Bootstrap', value: 'bi' },
    { label: 'Element Plus', value: 'ep' },
    { label: 'IconFont', value: 'ii' },
    { label: 'Remix', value: 'ri' },
    { label: '自定义', value: 'ci' }
  ]
}
// 获取所有类型图表集的图标数据
export const getIconList = () => {
  type iconType = {
    type: string
    key: string
    value: string
  }
  const tabList = ['ad', 'bi', 'ep', 'ii', 'ri', 'ci']
  const iconList = []
  tabList.forEach((item) => {
    let data = {}
    switch (item) {
      case 'ad':
        data = antDesign.icons // Ant Design 图标集
        break
      case 'bi':
        data = bi.icons // Bootstrap 图标集
        break
      case 'ep':
        data = ep.icons // Element Plus 图标集
        break
      case 'ii':
        data = iconfont.glyphs // Iconfont 图标集
        break
      case 'ri':
        data = ri.icons // Remix 图标集
        break
      default: // 自定义svg图标集
        data = customSvgIconComponents
        break
    }
    if (item === 'ii') {
      for (const key in data) {
        const itemIcon: iconType = {
          type: item,
          key: 'icon-' + data[key].font_class,
          value: 'icon-' + data[key].font_class
        }
        iconList.push(itemIcon)
      }
    } else if (item === 'ci') {
      for (const key in data) {
        const itemIcon: iconType = {
          type: item,
          key: key,
          value: data[key].default
        }
        iconList.push(itemIcon)
      }
    } else {
      for (const key in data) {
        const itemIcon: iconType = {
          type: item,
          key: key,
          value: data[key].body
        }
        iconList.push(itemIcon)
      }
    }
  })
  return iconList
}
