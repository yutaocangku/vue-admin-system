export default {
  '月历': 'Month Calendar',
  '周历': 'Week Calendar',
  '上一周': 'Last week',
  '下一周': 'Next week',
  '上一年': 'Last year',
  '上一月': 'Last Month',
  '下一年': 'Next Year',
  '下一月': 'Next Month',
  '返回今天': 'Today',
  '年': 'Year',
  '月': 'Month',
  '日': 'Day'
}
