﻿export default {
  fileName: 'File Name',
  fileType: 'File Type',
  fileTypeXlsx: 'Xlsx(*.xlsx)',
  fileTypeCsv: 'Csv(*.csv)',
  fileTypeHtml: 'Web Page(*.html)',
  fileTypeXml: 'Xml Data(*.xml)',
  fileTypeTxt: 'Text File(*.txt)',
  dataScope: 'Data Scope',
  dataScopeCurrentPage: 'Current Page Data',
  dataScopeSelected: 'Selected Data',
  dataScopeAll: 'All Data',
  headerType: 'Header Type',
  headerTypeDefault: 'Default',
  headerTypeCustom: 'Custom',
  fields: 'Fields',
  allFields: 'All Fields'
}
