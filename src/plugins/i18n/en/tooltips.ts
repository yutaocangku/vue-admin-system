﻿export default {
  tooltipLayout: 'The layout configuration only takes effect under the computer window, and the mobile phone window will default to the vertical layout',
  tooltipEmpty: 'No Data',
  tooltipEnforceShowColumn: 'When the root menu has no children, is it necessary to display the sub-column module',
  tooltipColumnInline: 'Whether the sub-menu column module needs to be embedded in the document flow and occupy the width of the document flow',
  tooltipAutoLockScreenTime: 'When the value is 0, the automatic screen lock function is turned off.Time unit: minutes',
  tooltipAutoSignOutTime: 'When the value is 0, the automatic sign out function is turned off.Time unit: minutes',
  tooltipSidebarCollapseButtonPosition: 'Sidebar collapse button display position',
  tooltipColumnCollapseButtonPosition: 'Column collapse button display position'
}
