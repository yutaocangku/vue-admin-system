﻿export default {
  verifyMailEmpty: 'Please enter your account',
  verifyPasswordEmpty: 'Please enter your password',
  verifyCode: 'Please enter google verification code',
  verifyPasswordError: 'Please enter the correct password',
  verifyPasswordTwice: "Two inputs don't match",
  '请输入账号！': 'Please enter the account number!',
  '账号格式错误！': 'The account format is wrong!',
  '请输入邮箱！': 'Please enter your email address!',
  '邮箱格式错误！': 'Wrong mailbox format!',
  '请输入手机号！': 'Please enter your phone number!',
  '手机号格式错误！': 'The phone number is in the wrong format!',
  '请输入密码！': 'Please enter your password!',
  '密码格式错误！': 'Wrong password format!',
  '请再次输入密码！': 'Please enter your password again!',
  '两次密码输入不一致！': 'The two password entries are inconsistent!',
  '请输入验证码！': 'Please enter the verification code!',
  '请输入邀请码！': 'Please enter the invitation code!'
}
