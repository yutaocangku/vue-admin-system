﻿export default {
  '登录': 'Login',
  '首页': 'Home',
  '功能': 'Functions',
  '插件': 'Plugins',
  '组件': 'Components',
  '页面': 'Pages',
  '其它': 'Others',
  '配置': 'Settings',
  '关于': 'About',
  '工作台': 'Workbench',
  '数据大屏': 'Data Screen',
  '加密解密': 'Encrypt Decode',
  '多级菜单': 'MultiLevel Menu',
  '标签页操作': 'Tag Actions',
  '动态路径参数': 'Dynamic Segment',
  '右键菜单': 'Context Menu',
  '动态锚点': 'Dynamic Anchor',
  '水印': 'Watermark',
  '打印': 'Print',
  '防抖节流': 'Debounce Throttle',
  '条形码': 'Barcode',
  '二维码': 'QRcode',
  '图片裁剪': 'Cropping',
  '数字动画': 'CountTo',
  '剪切板': 'Clipboard',
  '上传': 'Upload',
  '下载': 'Download',
  '无缝滚动': 'Seamless Scroll',
  '拖拽': 'Drag',
  '引导页': 'Guide',
  '点击外部': 'ClickOutSide',
  '菜单1': 'Menu1',
  '菜单2': 'Menu2',
  '菜单1-1': 'Menu1-1',
  '菜单1-2': 'Menu1-2',
  '菜单1-3': 'Menu1-3',
  '菜单1-2-1': 'Menu1-2-1',
  '菜单1-2-2': 'Menu1-2-2',
  'eCharts': 'eCharts',
  'Excel': 'Excel',
  '编辑器': 'Editor',
  '设计器': 'Designer',
  '导入': 'Import',
  '导出': 'Export',
  '富文本编辑器': 'Rich Text Editor',
  'markdown编辑器': 'Markdown Editor',
  'json编辑器': 'Json Editor',
  '流程设计器': 'Bpmn Process',
  '大屏设计器': 'Large Screen Designer',
  '工作空间': 'Workstation',
  '大屏预览': 'Large Screen Preview',
  '表单设计器': 'Form Designer',
  'element-plus': 'Element-plus',
  '图标选择器': 'Icon Select',
  '视频播放器': 'Video',
  '按钮组件': 'Button Component',
  '文本链接': 'Text Link Component',
  '标签组件': 'Tag Component',
  '消息提醒': 'Notice Component',
  '反馈组件': 'Feedback Component',
  '数据': 'Data',
  '表单': 'Form',
  '分页加载': 'Pagination List',
  '滚动加载': 'Infinite Scroll List',
  '详情页': 'Article',
  '日历': 'Calendar',
  '树结构': 'Tree',
  '综合表单': 'Comprehensive Form',
  '分步表单': 'Step Form',
  '图标展示页': 'Icon Page',
  '列表页面': 'List Page',
  '详情页面': 'Detail Page',
  '表单页面': 'Form Page',
  '结果页面': 'Result Page',
  '异常页面': 'Exception Page',
  '个人页面': 'Account Page',
  '外部页面': 'Frame Page',
  '无框页面': 'Frameless Page',
  '标准列表': 'Basic List',
  '卡片列表': 'Card List',
  '文章详情': 'Article Detail',
  '商品详情': 'Product Detail',
  '订单详情': 'Order Detail',
  'CMS表单': 'CMS Form',
  '成功页面': 'Success Page',
  '失败页面': 'Fail Page',
  '403': '403',
  '404': '404',
  '500': '500',
  '网络错误': 'Network Error',
  '研发中': 'Developmenting',
  '升级维护': 'Upgrade Repair',
  '个人中心': 'Personal Center',
  '个人设置': 'Personal Settings',
  'element-plus文档(内嵌)': 'Element-plus Doc (Embedded)',
  'element-plus文档(外链)': 'Element-plus Doc (External)',
  '伪无框': 'Pseudo Frameless',
  '真无框': 'Real Frameless',
  '测试': 'Test',
  '测试A': 'Test A',
  '测试B': 'Test B',
  '动态路由': 'Dynamic Routing',
  '部门管理': 'Department Management',
  '用户管理': 'User Management',
  '角色管理': 'Role Management',
  '权限管理': 'Permissions Management',
  '菜单管理': 'Menu Management',
  '菜单按钮': 'Menu Button Management',
  '字典管理': 'Dictionary Management',
  '字典配置': 'Dictionary Settings',
  '系统日志': 'System Log',
  'Websocket测试': 'Websocket Test',
  '标签详情': 'Tab Detail',
  'Param详情': 'Single Param Detail',
  'Params详情': 'Multi Params Detail',
  'Query详情': 'Query Detail',
  '富文本锚点': 'Rich Text Anchor',
  '监控调整大小': 'Monit Resize',
  '网站设计器': 'Website Designer',
  '站点管理': 'Website Management',
  '站点页面管理': 'Website Page Management',
  '页面管理': 'Page Management',
  '页面装修': 'Page Designer',
  '模块管理': 'Module Management',
  '测试节点': 'Test Node',
  '查看': 'View',
  '新增': 'Create',
  '编辑': 'Update',
  '删除': 'Delete',
  '重置': 'Reset',
  '查询': 'Search',
  '预览': 'Preview',
  '复制': 'Copy',
  '保存': 'Save',
  '复制并释放': 'Copy Release',
  '重置密码': 'Reset Password',
  '知识库': 'Knowledge Base',
  '抽奖': 'Lottery'
}
