﻿export default {
  moduleTitleWarningAlert: 'Tips:',
  moduleTitleAvatarUpload: 'Avatar Upload',
  moduleTitleUpload: 'Upload',
  moduleTitlePreview: 'Preview',
  moduleTitleExport: 'Export Data',
  '警告': 'Warning'
}
