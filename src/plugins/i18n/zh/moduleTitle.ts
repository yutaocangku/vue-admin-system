﻿export default {
  moduleTitleWarningAlert: '温馨提示：',
  moduleTitleAvatarUpload: '头像上传',
  moduleTitleUpload: '上传',
  moduleTitlePreview: '预览',
  moduleTitleExport: '导出数据',
  '警告': '警告'
}
