﻿export default {
  tooltipLayout: '布局配置仅在电脑视窗下生效，手机视窗时将默认为纵向布局',
  tooltipEmpty: '暂无数据',
  tooltipEnforceShowColumn: '根菜单无子级时，是否需要显示分栏模块',
  tooltipColumnInline: '子级菜单分栏模块是否需要内嵌在文档流内，占用文档流宽度',
  tooltipAutoLockScreenTime: '值为0时，自动锁屏功能关闭。时间单位：分钟',
  tooltipAutoSignOutTime: '值为0时，自动退出登录功能关闭。时间单位：分钟',
  tooltipSidebarCollapseButtonPosition: '侧边栏折叠按钮显示位置',
  tooltipColumnCollapseButtonPosition: '分栏折叠按钮显示位置'
}
