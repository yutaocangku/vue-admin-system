﻿export default {
  fileName: '文件名称',
  fileType: '文件类型',
  fileTypeXlsx: 'xlsx(*.xlsx)',
  fileTypeCsv: 'csv(*.csv)',
  fileTypeHtml: '网页(*.html)',
  fileTypeXml: 'xml数据(*.xml)',
  fileTypeTxt: '文本文件(*.txt)',
  dataScope: '选择数据',
  dataScopeCurrentPage: '当前页数据',
  dataScopeSelected: '选中的数据',
  dataScopeAll: '全量后台数据',
  headerType: '表头选择',
  headerTypeDefault: '默认',
  headerTypeCustom: '自定义',
  fields: '字段选择',
  allFields: '全部字段'
}
