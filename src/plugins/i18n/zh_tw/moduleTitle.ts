﻿export default {
  moduleTitleWarningAlert: '溫馨提示：',
  moduleTitleAvatarUpload: '頭像上傳',
  moduleTitleUpload: '上傳',
  moduleTitlePreview: '預覽',
  moduleTitleExport: '匯出數據',
  '警告': '警告'
}
