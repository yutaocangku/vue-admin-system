﻿export default {
  tooltipLayout: '佈局配置僅在電腦視窗下生效，手機視窗時將預設為縱向佈局',
  tooltipEmpty: '暫無數據',
  tooltipEnforceShowColumn: '根菜單無子級時，是否需要顯示分欄模組',
  tooltipColumnInline: '子級菜單分欄模組是否需要內嵌在文檔流內，佔用文檔流寬度',
  tooltipAutoLockScreenTime: '值為0時，自動鎖屏功能關閉。 時間單位：分鐘',
  tooltipAutoSignOutTime: '值為0時，自動退出登錄功能關閉。 時間單位：分鐘',
  tooltipSidebarCollapseButtonPosition: '側邊欄摺疊按鈕顯示位置',
  tooltipColumnCollapseButtonPosition: '分欄摺疊按鈕顯示位置'
}
