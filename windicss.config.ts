﻿import { defineConfig } from 'vite-plugin-windicss'

export default defineConfig({
  extract: {
    include: ['src/**/*.{vue,html,jsx,tsx}'],
    exclude: ['node_modules', '.git']
  },
  // 暗黑模式使用类名控制，也可以是媒体查询模式：media
  darkMode: 'class',
  attributify: false,
  // 自定义样式
  theme: {
    extend: {
      // 将windicss的响应式与element-plus的响应式配置成一致，这里xs属于自定义的响应式，需要注意使用xs时，要与element-plus的xs保持一致，需要添加前缀`<`，`2xl`这个响应式在element-plus中是不存在的，属于windicss特有响应式
      screens: {
        xs: '768px',
        sm: '768px',
        md: '992px',
        lg: '1200px',
        xl: '1920px',
        '2xl': '2560px'
      }
    }
  },
  // 自定义重复性使用工具类合集，当你使用某几个工具类非常频繁时，可以在此处配置成合集，然后在使用时直接使用合集名称即可
  shortcuts: {
    // btn: 'py-2 px-4 font-semibold rounded-lg shadow-md',
    // 'btn-green': 'text-white bg-green-500 hover:bg-green-700'
  },
  // 可以自己开发插件使用
  plugins: []
})
