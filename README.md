# vue-admin-system

#### 介绍

vite2+vue3+vue-router4+pinia+axios+mock+scss+element-plus+typescript 技术栈脚手架

#### 安装教程

1.  克隆：

```
git clone git@gitee.com:ctokevin/vue-admin-system.git
```

2.  安装依赖

```
yarn
```

3.  运行项目

```
yarn start
```

4. 生成生产环境

```
yarn build
```

#### 新增功能

2023-10-09 新增 BPMN 流程设计器，已自定义 camunda 属性面板所有属性模块，并将该插件组件化，可随处调用

#### 功能笔记说明

1.  [01 环境及项目初始化](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/01环境及项目初始化.md) - [已完成]
2.  [02 集成 scss 预处理器](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/02集成scss预处理器.md) - [已完成]
3.  [03 集成 element-plus](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/03集成element-plus.md) - [已完成]
4.  [04 集成 svg 图标组件](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/04集成svg图标组件.md) - [已完成]
5.  [05 集成 mock 模拟后端数据](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/05集成mock模拟后端数据.md) - [已完成]
6.  [06 集成 axios](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/06集成axios.md) - [已完成]
7.  [07 集成 vue-router](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/07集成vue-router.md) - [已完成]
8.  [08 集成 vuex](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/08集成vuex.md) - [已完成]
9.  [09 登录功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/09登录功能.md) - [已完成]
10. [10 双 token 机制的身份验证以及前后端无感刷新 token 的方法](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/10双token机制的身份验证以及前后端无感刷新token的方法.md) - [已完成]
11. [11 框架定型及根路由添加](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/11框架定型及根路由添加.md) - [已完成]
12. [12 侧边菜单展示功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/12侧边菜单展示功能.md) - [已完成]
13. [13 侧边菜单折叠展开功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/13侧边菜单折叠展开功能.md) - [已完成]
14. [14 用户信息展示及退出登录功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/14用户信息展示及退出登录功能.md) - [已完成]
15. [15 标签栏功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/15标签栏功能.md) - [已完成]
16. [16 面包屑功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/16面包屑功能.md) - [已完成]
17. [17 进场动画功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/17进场动画功能) - [已完成]
18. [18keep-alive 功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/18keep-alive功能.md) - [已完成]
19. [19keep-alive、路由、面包屑冲突解决方案](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/19keep-alive、路由、面包屑冲突解决方案.md) - [已完成]
20. [20 全屏功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/20全屏功能.md) - [已完成]
21. [21axios 多请求合并一次 loading 功能封装](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/21axios多请求合并一次loading功能封装.md) - [已完成]
22. [22axios 取消重复请求与未完成请求功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/22axios取消重复请求与未完成请求功能.md) - [已完成]
23. [23axios 处理异常状态码功能封装](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/23axios处理异常状态码功能封装.md) - [已完成]
24. [24 环境变量配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/24环境变量配置.md) - [已完成]
25. [25vite.config.ts 文件配置及 axios 定制化是否启用 mock](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/25vite.config.ts文件配置及axios定制化是否启用mock.md) - [已完成]
26. [26 设备监听、DOM 元素监听功能封装](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/26设备监听、DOM元素监听功能封装.md) - [已完成]
27. [27 消息通知功能组件](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/27消息通知功能组件) - [已完成]
28. [28websocket 功能封装](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/28websocket功能封装) - [已完成]
29. [29 国际化功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/29国际化功能.md) - [已完成]
30. [30 项目配置文件及响应式本地存储](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/30项目配置文件及响应式本地存储.md) - [已完成]
31. [31 页面标题配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/31页面标题配置.md) - [已完成]
32. [32 路由菜单遗留问题修改](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/32路由菜单遗留问题修改.md) - [已完成]
33. [33 滚动条行为、锚点定位、滚动监听](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/33滚动条行为、锚点定位、滚动监听.md) - [已完成]
34. [34 组合式 API 逻辑抽离](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/34组合式API逻辑抽离.md) - [已完成]
35. [35 标签关闭与缓存清除相关问题](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/35标签关闭与缓存清除相关问题.md) - [已完成]
36. [36 组件 size 功能全局配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/36组件size功能全局配置.md) - [已完成]
37. [37 重写 element-plus 样式](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/37重写element-plus样式.md) - [已完成]
38. [38 项目配置-框架类型配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/38项目配置-框架类型配置.md) - [已完成]
39. [39 项目配置-主题、主题色配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/39项目配置-主题、主题色配置.md) - [已完成]
40. [40element-plus 组件使用](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/40element-plus组件使用.md) - [已完成]
41. [41svg 图标功能优化](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/41svg图标功能优化.md) - [已完成]
42. [42element-plus 组件暗系主题配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/42element-plus组件暗系主题配置.md) - [已完成]
43. [43 项目配置-暗系主题切换功能](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/43项目配置-暗系主题切换功能.md) - [已完成]
44. [44 项目配置-界面功能配置-顶栏、侧边栏固定效果](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/44项目配置-界面功能配置-顶栏、侧边栏固定效果.md) - [已完成]
45. [45 项目配置-界面功能配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/45项目配置-界面功能配置.md) - [已完成]
46. [46 项目配置-界面显示配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/46项目配置-界面显示配置.md) - [已完成]
47. [47 项目配置-动画功能配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/47项目配置-动画功能配置.md) - [已完成]
48. [48 项目缓存数据整合与加密](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/48项目缓存数据整合与加密.md) - [已完成]
49. [49 打包生产环境配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/49打包生产环境配置.md) - [已完成]
50. [50 其它辅助配置](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/50其它辅助配置.md) - [已完成]
51. [51 路由菜单结构调整](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/51路由菜单结构调整.md) - [已完成]
52. [52 集成 windicss](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/52集成windicss.md) - [已完成]
53. [53 集成 vxe-table](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/53集成vxe-table.md) - [已完成]
54. [54 集成 echarts](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/54集成echarts.md) - [已完成]
55. [55 功能-标签页操作、多标签、动态路径参数](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/55功能-标签页操作、多标签、动态路径参数.md) - [已完成]
56. [56 关于按需自动导入导致 element-plus 部分组件样式丢失问题](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/56关于按需自动导入导致element-plus部分组件样式丢失问题.md) - [已完成]
57. [57 功能-右键菜单](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/57功能-右键菜单.md) - [已完成]
58. [58 功能-动态锚点](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/58功能-动态锚点.md) - [已完成]
59. [59 功能-水印](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/59功能-水印.md) - [已完成]
60. [60 功能-打印](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/60功能-打印.md) - [已完成]
61. [61 功能-防抖节流](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/61功能-防抖节流.md) - [已完成]
62. [62 功能-条形码](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/62功能-条形码.md) - [已完成]
63. [63 功能-二维码](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/63功能-二维码.md) - [已完成]
64. [64 功能-图片裁剪、图标组件功能调整](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/64功能-图片裁剪、图标组件功能调整.md) - [已完成]
65. [65 功能-数字动画](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/65功能-数字动画.md) - [已完成]
66. [66 功能-剪切板](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/66功能-剪切板.md) - [已完成]
67. [67 功能-上传](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/67功能-上传.md) - [已完成]
68. [68 功能-下载](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/68功能-下载.md) - [已完成]
69. [69 功能-无缝滚动](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/69功能-无缝滚动.md) - [已完成]
70. [70 功能-拖拽](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/70功能-拖拽.md) - [已完成]
71. [71 功能-引导页](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/71功能-引导页.md) - [已完成]
72. [72 功能-ClickOutSide](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/72功能-ClickOutSide.md) - [已完成]
73. [73 功能-websocket 测试、监听调整大小](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/73功能-websocket测试、监听调整大小.md) - [已完成]
74. [74 插件-Excel 导入导出](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/74插件-Excel导入导出.md) - [已完成]
75. [75 插件-富文本编辑器](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/75插件-富文本编辑器.md) - [已完成]
76. [76 插件-markdown 编辑器](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/76插件-markdown编辑器.md) - [已完成]
77. [77 组件-图标选择器](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/77组件-图标选择器.md) - [已完成]
78. [78 插件-网站设计器](https://gitee.com/ctokevin/vue-admin-system/blob/main/public/doc/78插件-网站设计器.md) - [未完成]

#### 界面展示

![界面展示1](/public/doc/images/theme1.jpg)

![界面展示2](/public/doc/images/theme2.jpg)

![界面展示3](/public/doc/images/theme3.jpg)

![界面展示4](/public/doc/images/theme4.jpg)

![界面展示5](/public/doc/images/theme5.jpg)

![界面展示6](/public/doc/images/theme6.jpg)

![界面展示7](/public/doc/images/theme7.jpg)

![界面展示8](/public/doc/images/theme8.jpg)

![界面展示9](/public/doc/images/theme9.jpg)

![界面展示10](/public/doc/images/theme10.jpg)
