﻿# 集成vue-router
```
yarn add vue-router@next
```
在之前项目已经集成了`element-plus`组件、`svg`图标组件、`mock`模拟数据接口功能、封装的`axios`功能，到了集成`vue-router`时，这些都将被串联使用起来。

路由，也就是菜单功能，项目的菜单一般都是由后端返回的数据，前端进行渲染，也会有一部分菜单是不需要在后端数据库中存在的，所以路由菜单数据，一般分为动态路由，和静态路由，静态路由由前端进行维护，动态路由由后端返回具体路由数据，前端进行展示，这里在没有后端参与的情况下，就需要用到`mock`模拟后端返回菜单数据的接口，用`axios`进行请求以获取动态菜单数据，并和前端维护的静态路由进行组合，形成最终菜单路由数据，菜单数据需要展示图标的，还需要`svg`图标组件参与进来，并最终通过使用`element-plus`的菜单组件，将路由数据渲染到页面上。

## mock模拟后端菜单数据接口
前端使用后端给的接口，获取后端返回的数据，对于菜单数据，后端一般返回的都是一维数组，要组件后端数据接口，需要先确定都需要返回什么，或者在后台管理菜单时，都需要管理菜单的什么，也就是需要确定菜单数据的字段：
1. ID
2. 父级ID
3. 菜单名
4. 菜单图标
5. 菜单链接（作为前端路由，也做为组装动态引入页面组件的路径，构建前端项目的页面物理路径结构）
6. 菜单重定向链接
7. 是否隐藏（菜单栏上是否显示）
8. 排序
9. 后端唯一标识名（指向后端具体动作或类，也用来做前端路由的name）

新建`mock/asyncRoutes.ts`文件：
```
import { MockMethod } from "vite-plugin-mock";
// http://mockjs.com/examples.html#Object
const systemRouter = [
  {id:1,pid:0,title:"首页",icon:"icon-shouye",url:"/Home",redirect:"",showLink:true,sort:0,route:"Home"},
  {id:2,pid:0,title:"多级菜单",icon:"icon-daohang",url:"/Nested",redirect:"/Nested/Menu1/Menu1-1",showLink:true,sort:1,route:"Nested"},
  {id:3,pid:2,title:"菜单1",icon:"",url:"/Nested/Menu1",redirect:"/Nested/Menu1/Menu1-1",showLink:true,sort:0,route:"Menu1"},
  {id:4,pid:3,title:"菜单1-1",icon:"",url:"/Nested/Menu1/Menu1-1",redirect:"",showLink:true,sort:0,route:"Menu1-1"},
  {id:5,pid:3,title:"菜单1-2",icon:"",url:"/Nested/Menu1/Menu1-2",redirect:"/Nested/Menu1/Menu1-2/Menu1-2-1",showLink:true,sort:1,route:"Menu1-2"},
  {id:6,pid:5,title:"菜单1-2-1",icon:"",url:"/Nested/Menu1/Menu1-2/Menu1-2-1",redirect:"",showLink:true,sort:0,route:"Menu1-2-1"},
  {id:7,pid:5,title:"菜单1-2-2",icon:"",url:"/Nested/Menu1/Menu1-2/Menu1-2-2",redirect:"",showLink:true,sort:1,route:"Menu1-2-2"},
  {id:8,pid:3,title:"菜单1-3",icon:"",url:"/Nested/Menu1/Menu1-3",redirect:"",showLink:true,sort:3,route:"Menu1-3"},
  {id:9,pid:2,title:"菜单2",icon:"",url:"/Nested/Menu2",redirect:"",showLink:true,sort:1,route:"Menu2"},
  {id:10,pid:0,title:"论坛新闻",icon:"icon-luntan",url:"/News",redirect:"",showLink:true,sort:0,route:"News"},
]
export default [
  {
    url: "/getAsyncRoutes",
    method: "post",
    response: () => {
      return {
        code: 200,
        status:true,
        info:"菜单数据获取成功！",
        data: systemRouter
      };
    }
  }
] as MockMethod[];
```
以上就是后端接口将会返回给前端的数据，给根节点设置图标，非页节点需要设置重定向链接，后端相关的功能已做好，接下来，需要前端调用该接口获取菜单数据

## 前端封装axios调用后端接口
新建`src/api`文件夹，作为项目所有请求文件的存放文件夹，并新建`routes.ts`文件：
```
import { http } from "../utils/http";

// 获取动态菜单数据
export const getAsyncRoutes = () => {
  return http.request("post", "/getAsyncRoutes");
};
```
这里调用封装的`axios`方法，可在`src/utils/http/core.ts`中，找到`request`方法，该方法共接收四个参数，请求类型、请求链接、请求参数、自定义`axios`配置，前两个参数为必填，后两个参数根据请求需求，可填可不填。此时前端的接口调用已经封装完成，接下来需要先处理下静态路由。
## 静态路由模块化
在获取动态路由之前，先将静态路由处理一下，静态路由是由前端来维护的部分路由，不需要在后台进行创建，如登录、404、401等，以及如本例中后台返回了论坛新闻列表的菜单，但并未返新闻详情的菜单路由，这种路由可能不需要后端创建，如果项目页面很多，这种需要前端维护的路由也会有很多，就需要将这部分静态路由做模块化处理，方便后期维护。

如登录、404、401这类为整个项目所使用的页面，需要单独添加到路由中，它们就需要一个单独的模块，而像新闻详情这种属于某一个具体功能的菜单路由，则都可以单独作为一个路由模块进行管理

新建`src/router`文件夹，并在其内新建`remaining.ts`文件：
```
const remainingRouter = [
  {
    path: "/Login",
    name: "Login",
    component: () => import("@/views/Login/index.vue"),
    meta: {
      title: "登录",
      showLink: false,
      sort: 10000
    }
  },
  {
    path: "/Error/401",
    name: "401",
    component: () => import("@/views/Error/401/index.vue"),
    meta: {
      title: "401",
      showLink: false,
      sort: 10001
    }
  },
  {
    path: "/Error/404",
    name: "404",
    component: () => import("@/views/Error/404/index.vue"),
    meta: {
      title: "404",
      showLink: false,
      sort: 10002
    }
  }
];

export default remainingRouter;
```
新建`src/router/modules/news.ts`文件:
```
const modulesRouter = [
  {
    path: "/News/NewsDetail/:id",
    name: "NewsDetail",
    component: () => import("@/views/News/NewsDetail/index.vue"),
    meta: {
      title: "",
      showLink: false,
      pid:10,
      sort:1,
    }
  }
]
export default modulesRouter;
```
这里将新闻详情路由的`pid`与后端返回的菜单数据论坛新闻的`id`进行了对应，以方面后期进行数据结构处理。

**这里可以规定，静态路由如存在嵌套关系，直接写为嵌套的数据结构；所有路由模块都规定为数组数据结构**

静态路由、动态路由数据整理好后，接着需要将路由整合，并实例化路由，在`main.ts`引入路由。

## 实例化静态路由
静态路由的合并、获取后端动态路由，将后端动态路由转化为规范路由、这些路由操作可以单独各自封装成方法，在路由实例化时进行调用，新建`src/router/utils.ts`文件：
```
// 处理静态路由
const constantRoutesFilter = (data:any,isAsync:boolean) => {
  let newData = [];
  if(isAsync){
    // 需要的是动态路由子集的部分
    newData = data.filter((item:any)=>{
      return item.meta.pid != 0;
    })
  }else{
    // 需要的是可直接实例化的静态路由部分
    newData = data.filter((item:any)=>{
      return item.meta.pid == 0;
    })
  }
  return newData;
}

export {
  constantRoutesFilter
};
```
新建`src/router/index.ts`文件：
```
import { Router, createWebHistory, createRouter } from "vue-router";
import {constantRoutesFilter} from "./utils";
import { cloneDeep } from "lodash-es";
// 静态白名单路由
import remainingRouter from "./remaining";
// 自动导入modules文件夹下的所有路由模块
const modules = import.meta.glob("./modules/*.ts");
// 原始静态路由（未做任何处理）
const routes = [];
for(const path in modules){
	const itemModule = await modules[path]();
  // 模块化路由是数组形式数据
  itemModule.default.forEach(function(item: any){
    routes.push(item);
  });
  // 模块化路由是对象形式数据
	//routes.push(itemModule.default);
}
// 获取处理后的静态路由（未获取动态路由的情况下，属于动态路由子集的路由需要先剔除掉，只实例化静态就能展示的路由）
const constantRootRoutes = constantRoutesFilter(cloneDeep(routes),false);

// 创建路由实例
export const router: Router = createRouter({
  history: createWebHistory(),
  routes: constantRootRoutes.concat(...remainingRouter)
});

export default router;
```
这里用到了新的插件，需要安装对应插件：
```
yarn add lodash-es
```
为了让`lodash-es`这类安装的插件不报类型错误，需要修改`tsconfig.json`文件：
```
{
  "compilerOptions": {
    "target": "esnext",
    "useDefineForClassFields": true,
    "module": "esnext",
    "moduleResolution": "node",
    "strict": false,
    "jsx": "preserve",
    "sourceMap": true,
    "resolveJsonModule": true,
    "esModuleInterop": true,
    "lib": ["esnext", "dom"],
    // 新增
    "baseUrl": ".", 
    "paths":{
      "@/*": ["src/*"],
    },
    "allowJs": false,
    "types": ["node", "vite/client", "element-plus/global"],
    "typeRoots": ["./node_modules/@types/", "./types"]
  },
  "include": ["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue"],
  "exclude": ["node_modules", "dist", "**/*.js"]
}
```
这里将`strict`值改为了`false`，并新增了一些配置，此时页面不会再有报错，运行项目能正常运转。路由实例化后，在`src/main.ts`中引入：
```
import { createApp } from 'vue'
import App from '@/App.vue'
import router from "./router";

// element-plus 按需引入
import { useElementPlus } from "@/plugins/element-plus"; // element-plus
import SvgIcon from '@/components/SvgIcon/index.vue';
import { useCustomSvgIcon } from "@/plugins/customSvgIcon";

import "@/style/index.scss"
// 导入字体图标
import "@/assets/iconfont/iconfont.js";

const app = createApp(App)
app.use(router)
app.use(useElementPlus)
app.use(useCustomSvgIcon)
app.component('svg-icon', SvgIcon); // 全局注册svg图标组件
app.mount('#app')
```
然后需要将路由对应页面结构创建出来，以及将`src/App.vue`页面改为`router-view`作为路由匹配使用，使用`vue-router`作为路由跳转，肯定需要`router-view`进行接收。

新建`views`文件夹，其内新建静态路由中指向的所有页面，以及新建一个`home`页面，用来存放原`src/App.vue`文件中的代码。

`src/views/Error/401/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>401</h2>
</template>

<style>
</style>
```
`src/views/Error/404/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>404</h2>
</template>

<style>
</style>
```
`src/views/Home/index.vue`:
```
<script setup lang="ts">
// This starter template is using Vue 3 <script setup> SFCs
// Check out https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup
import HelloWorld from '@/components/HelloWorld.vue';
import { ref } from 'vue'
const value1 = ref("")
</script>

<template>
  <img alt="Vue logo" src="@/assets/logo.png" />
  <HelloWorld msg="Hello Vue 3 + TypeScript + Vite" />
  <p class="demo">测试</p>
  <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  <div class="icon-list">
    <span class="item-icon"><edit></edit></span>
    <span class="item-icon">
      <svg class="icon" aria-hidden="true">
          <use xlink:href="#icon-lock"></use>
      </svg>
    </span>
    <span class="item-icon">
      <svg-icon icon-class="edit"></svg-icon>
    </span>
    <span class="item-icon">
      <svg-icon icon-class="icon-lock"></svg-icon>
    </span>
    <span class="item-icon">
      <svg-icon icon-class="closeRight"></svg-icon>
    </span>
  </div>
  
</template>

<style lang="scss">
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: $primary;
  margin-top: 60px;
}
.icon-list{
  width: 100%;
  text-align: center;
  .item-icon{
    display: inline-block;
    width: 20px;
    height: 20px;
    margin: 5px;
    font-size: 16px;
    color: $pink;
    svg{
      width:100%;
      height: 100%;
      fill: currentColor;
    }
  }
}
</style>
```
`src/views/Login/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>登录</h2>
</template>

<style>
</style>
```
`src/views/News/NewsDetail/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>新闻详情</h2>
</template>

<style>
</style>
```
`src/App.vue`:
```
<script lang="ts">
export default {
  name: "app",
};
</script>

<template>
  <router-view />
</template>
```
此时运行项目，浏览器打开项目，首页是空页面，在路由中输入已存在的路由页面，如`/login`，即可跳转到登录页，静态路由到这里就添加好了，接下来将动态路由也挂载到路由实例上。

## 实例化动态路由
实例化动态路由，需要先知道在什么地方去调用之前已经写好的动态路由获取接口，通常只需要在登录后获取一次即可，获取路由后进行实例化，页面切换路由数据并不会有改变，所以无需在每次页面切换都重新进行请求，但如果点击了浏览器自身的刷新按钮，此时因为页面所有元素都已被销毁，这时，是需要重新请求路由数据的，那么需要请求路由数据的情况就是：
1. 用户登录后
2. 浏览器刷新按钮被点击后
登陆的后续操作是要跳转到登录后的默认页，这里会有路由切换的场景，而浏览器刷新按钮被点击，则会出现当前路由重新加载的场景，而以上两种情况都可以在`vue-router`的**导航守卫**中监测到，所以这里就需要用到**导航守卫**去监控路由变化，从而在需要的地方去重新请求路由数据了，先将动态路由中涉及到的页面在`views`文件夹下创建好：
`src/views/Nested/Menu1/Menu1-1/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>Menu1-1</h2>
</template>

<style>
</style>
```
`src/views/Nested/Menu1/Menu1-2/Menu1-2-1/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>Menu1-2-1</h2>
</template>

<style>
</style>
```
`src/views/Nested/Menu1/Menu1-2/Menu1-2-2/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>Menu1-2-2</h2>
</template>

<style>
</style>
```
`src/views/Nested/Menu1/Menu1-3/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>Menu1-3</h2>
</template>

<style>
</style>
```
`src/views/Nested/Menu2/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>Menu2</h2>
</template>

<style>
</style>
```
`src/views/News/index.vue`:
```
<script setup lang="ts">

</script>
<template>
  <h2>论坛新闻</h2>
</template>

<style>
</style>
```
动态路由指向的页面都创建好后，修改`src/router/utils.ts`文件：
```

import { router } from "./index";

// 处理静态路由
const constantRoutesFilter = (data:any,isAsync:boolean) => {
  let newData = [];
  if(isAsync){
    // 需要的是动态路由子集的部分
    newData = data.filter((item:any)=>{
      return item.meta.pid != 0;
    })
  }else{
    // 需要的是可直接实例化的静态路由部分
    newData = data.filter((item:any)=>{
      return item.meta.pid == 0;
    })
  }
  return newData;
}

// 过滤后端传来的动态路由 重新生成规范路由，并与静态路由中归属于动态路由的子集数据进行合并
const addAsyncRoutes = (arrRoutes:any,constantNotRootRoutes:any) => {
  if (!arrRoutes || !arrRoutes.length) return;
  const modulesRoutes = import.meta.glob("/src/views/**/**.vue");
  var routerData:any = [];
  arrRoutes.forEach((v:any) => {
    var itemData = {
      path:'',
      name:'',
      redirect:'',
      component:{},
      meta:{
        id:0,
        pid:0,
        icon:'',
        title:'',
        showLink:true,
        sort:0,
      }
    };
    itemData.path = v.url;
    itemData.name = v.route;
    if (v.redirect=="") {
      itemData.component = modulesRoutes[`/src/views${v.url}/index.vue`];
    }else{
      itemData.redirect = v.redirect;
    }
    itemData.meta.id = v.id;
    itemData.meta.pid = v.pid;
    itemData.meta.icon = v.icon;
    itemData.meta.title = v.title;
    itemData.meta.showLink = v.showLink;
    itemData.meta.sort = v.sort;
    routerData.push(itemData);
  });
  // 合并动态获取路由与静态路由中从属于动态路由子集的数据
  let allRouterData = routerData.concat(...constantNotRootRoutes);
  console.log(allRouterData,"合并后的动态路由");
  return allRouterData;
};

// 将规范路由的一维数组转为树形结构
const routerToTree = (arrRoutes:any) => {
  var parents = arrRoutes.filter(function (item:any) {
      return item.meta.pid == 0;
  });
  var children = arrRoutes.filter(function (item:any) {
      return item.meta.pid != 0;
  });
  // 递归处理动态路由层级
  convert(parents, children);
  console.log(parents,"转为路由嵌套形式的路由");
  return parents;
}

// 递归处理动态路由层级
const convert = (parents:any,children:any) =>{
  parents.forEach(function (item:any) {
    item.children = [];
    children.forEach(function (current:any, index:any) {
      if (current.meta.pid === item.meta.id) {
        var temp = children.filter(function(v:any,idx:any){
          return idx != index;
        }); // 删除已匹配项，这里未使用JSON.parse(JSON.stringify(children))是因为路由的component已经是函数形式，再格式化后，模块导入功能丢失，会导致找不到模块
        item.children.push(current);
        convert([current], temp); // 递归
      }
    });
  });
}

// 引入动态路由获取接口
import { getAsyncRoutes } from "@/api/routes";
// 初始化路由
const initRouter = (constantNotRootRoutes:any) => {
  return new Promise(resolve => {
    // 调用接口获取动态路由
    getAsyncRoutes().then((data:any) => {
      if (data.data.length > 0) {
        // 获取的动态路由处理成规范路由后，通过map方法，循环加入到路由实例中
        routerToTree(addAsyncRoutes(data.data,constantNotRootRoutes))?.map((v: any) => {
          // 防止重复添加路由
          if (router.options.routes.findIndex(value => value.path === v.path) !== -1) {
            return;
          } else {
            // 切记将路由push到routes后还需要使用addRoute，这样路由才能正常跳转
            router.options.routes.push(v);
            router.addRoute(v.name, v);
          }
          resolve(router);
        });
      }
      // 404匹配规则需在最后加入到路由实例中
      router.addRoute({path: "/:pathMatch(.*)",redirect: "/Error/404"});
    });
  });
};

export {
  constantRoutesFilter,
  initRouter
};
```
这里将调用接口获取动态路由、动态路由规范化、动态路由嵌套关系实现，都封装成了方法，并将整个流程的其实方法暴露出去。然后修改`src/router/index.ts`文件，在该文件中配置导航守卫，并在导航守卫中调用动态路由处理的封装方法：
```

import { Router, createWebHistory, createRouter } from "vue-router";
import {constantRoutesFilter,initRouter} from "./utils";
import NProgress from "@/utils/progress";
import { cloneDeep } from "lodash-es";
// 静态白名单路由
import remainingRouter from "./remaining";
// 自动导入modules文件夹下的所有路由模块
const modules = import.meta.glob("./modules/*.ts");
// 原始静态路由（未做任何处理）
const routes = [];
for(const path in modules){
  const itemModule = await modules[path]();
  routes.push(itemModule.default);
}
// 获取处理后的静态路由（未获取动态路由的情况下，属于动态路由子集的路由需要先剔除掉，只实例化静态就能展示的路由）
const constantRootRoutes = constantRoutesFilter(cloneDeep(routes),false);
const constantNotRootRoutes = constantRoutesFilter(cloneDeep(routes),true);

// 创建路由实例
export const router: Router = createRouter({
  history: createWebHistory(),
  routes: constantRootRoutes.concat(...remainingRouter)
});

router.beforeEach((to, _from, next) => {
  NProgress.start();
  // 如果是从登录页面跳转的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
  if(_from?.path=="/Login"||!_from?.name){
    initRouter(constantNotRootRoutes).then(() => {
      router.push(to.path);
    });
    next();
  }else{
    // 如果是其他页面跳转的，且其他页面的name存在，则直接进行下一步
    if (_from?.name) {
      next();
    }
  }
});

router.afterEach(() => {
  NProgress.done();
});
export default router;
```
这个时候运行项目，地址栏中输入动态路由具体页面，就可以打开页面看到页面内容了，而随便输入一个不存在的路由地址，也会自动跳转到404页面，但是当输入的是多级菜单相关的路由时，页面并不能正常显示，并且控制台进行了报错`Invalid route component`，这里是因为路由是嵌套层级，嵌套的路由，其对应的页面每嵌套一层都需要有对应的`router-view`接收对应的子页面，它是父级页面的一部分，只是从表现上看起来，其占了父页面所有空间，因此，这里需要为每个嵌套层级，分别新建一个页面，存放`router-view`用来进行页面接收：

新建`src/Nested/index.vue`文件：
```
<script lang="ts">
</script>
<template>
  <router-view />
</template>
```
新建`src/Nested/Menu1/index.vue`文件：
```
<script lang="ts">
</script>
<template>
  <router-view />
</template>
```
新建`src/Nested/Menu1/Menu1-2/index.vue`文件：
```
<script lang="ts">
</script>
<template>
  <router-view />
</template>
```
然后修改`src/router/utils.ts`文件中的`addAsyncRoutes`方法：
```
if (v.redirect=="") {
  itemData.component = modulesRoutes[`/src/views${v.url}/index.vue`];
}else{
  itemData.redirect = v.redirect;
}
# 修改为
itemData.component = modulesRoutes[`/src/views${v.url}/index.vue`];
if (v.redirect!="") {
  itemData.redirect = v.redirect;
}
```
此时再运行项目，地址栏输入多级菜单相关路由，页面就能正常显示了，不过此时项目还是有一些问题：
1. 地址栏输入动态路由相关的地址时，控制台会报警告`No match found for location with path "/***"`;
2. 动态路由页面打开时，控制台的导航守卫走了两遍，动态路由请求了两次；
以上问题出现的原因：
1. 地址栏输入地址，相当于刷新页面，刷新页面后，重新开始走导航守卫相关代码，而在还未走到初始化路由之前，导航守卫监测到当前地址栏的地址不存在于已实例化的路由（动态路由此时还未获取），因此会有警告提示。
2. 当走完初始化路由后，动态路由已获取，这里进行了`router.push(to.path);`，因为动态路由在初始化前还未获取到，所以需要在获取到动态路由后，重新进入该页面。但也因此，在走完此句，又重新走了一遍导航守卫，这时再次走到初始化路由的地方时，因未作条件限制，这里就又走了一遍初始化路由。
问题1只在刷新以及直接输入动态路由进行访问时，会有此警告，这种情况下，动态路由还未获取，因此这两种情况下的警告是无法消除的，但次警告并不影响使用。

问题2则需要加上侧边菜单，菜单数据进行状态管理，在走初始化路由时，判断一下菜单数据是否已存在，如已存在，则说明已经请求过一次，无需再次请求。

`vue-router`的基础功能这里就初步实现了，后续会再进行完善。
