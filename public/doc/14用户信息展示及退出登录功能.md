﻿# 用户信息展示
用户信息在登录时，已经在会话缓存中进行了存储，所以展示的地方只需要获取缓存信息，进行展示即可，修改`src/layout/components/headerPanel.vue`文件：
```
<script setup lang="ts">
import { computed } from "vue";
import { storageSession } from "@/utils/storage";
import Hamburger from "./hamBurger/index.vue";
import { useStore } from 'vuex';
import SvgIcon from "@/components/SvgIcon/index.vue";
const store = useStore();
const sidebar = computed(() => {
  return store.state.app.sidebar;
});
function toggleSideBar() {
  store.dispatch("app/toggleSideBar")
}
let userInfo = storageSession.getItem("userInfo");
</script>

<template>
  <div class="header">
    <div class="header-left">
      <Hamburger :is-active="sidebar.opened" class="hamburger-container" @toggleClick="toggleSideBar" />
    </div>
    <div class="header-center"></div>
    <div class="header-right">
      <!-- 退出登陆 -->
      <el-dropdown>
        <span class="el-dropdown-link">
          <img :src="userInfo.avatar" />
          <p>{{ userInfo.userName }}</p>
        </span>
        <template #dropdown>
          <el-dropdown-menu class="logout">
            <el-dropdown-item><span class="icon-span"><SvgIcon icon-class="SwitchButton"></SvgIcon></span><span class="txt-span">退出系统</span></el-dropdown-item>
          </el-dropdown-menu>
        </template>
      </el-dropdown>
    </div>
  </div>
</template>
<style lang="scss" scoped></style>
```
添加相应的样式代码，`src/style/layout.scss`内其相关的部分代码：
```
.header-right {
  display: flex;
  min-width: 280px;
  height: 48px;
  align-items: center;
  color: rgba(0,0,0,.9);
  justify-content: flex-end;
  .el-dropdown-link {
    height: 48px;
    padding: 10px;
    display: flex;
    align-items: center;
    justify-content: space-around;
    cursor: pointer;
    color: rgba(0,0,0,.9);
    &:hover {
      background: #f6f6f6;
    }
    p {
      font-size: 14px;
    }
    img {
      width: 32px;
      height: 32px;
      border-radius: 50%;
      margin-right: 10px;
    }
  }
}



.logout {
  max-width: 120px;
  .el-dropdown-menu__item {
    min-width: 100%;
    display: inline-flex;
    flex-wrap: nowrap;
    padding: 0 18px !important;
    height: 22px;
    line-height: 0;
    vertical-align: middle;
    span{
      height: 22px;
      line-height: 22px;
    }
    .icon-span{
      width: 22px;
      text-align: center;
      line-height: 22px;
      vertical-align: middle;
      i{
        margin:4px;
      }
    }
    .txt-span{
      flex: 1;
    }
  }
  .el-dropdown-menu__item:focus,
  .el-dropdown-menu__item:not(.is-disabled):hover {
    color: #606266;
    background: #f0f0f0;
  }
}
```
此时用户信息的展示就成功了。
# 退出登录功能
退出登录前后端都要有相应的操作，对于后端这里就需要新增一个接口，前端则需要调用接口，并在接口返回退出成功相关状态码后，将缓存的`token`以及用户信息清除，然后跳转回登录页面。

## 新增退出登录接口
修改`mock/user.ts`文件：
```
import { MockMethod } from "vite-plugin-mock";

// http://mockjs.com/examples.html#Object
const userData = [
  {id:1,account:"admin",password:"123456",userName:"超级管理员",avatar:"https://img1.baidu.com/it/u=1919046953,770482211&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"},
  {id:2,account:"test1",password:"123456",userName:"小红",avatar:"https://img2.baidu.com/it/u=2353293941,2044590242&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"},
  {id:3,account:"test2",password:"123456",userName:"小黑",avatar:"https://img1.baidu.com/it/u=63401368,3081051561&fm=253&fmt=auto&app=138&f=JPEG?w=360&h=360"},
]

// 经过一系列操作生成了accessToken、refreshToken
const createdToken = ()=>{
  return {
    accessToken:"abcdef",
    refreshToken:"qwerty"
  }
}
// 更新token接口中接收到的前端返回的refreshToken,判断refreshToken是否有效，假定这里已获取到前端回传的refreshToken
const isTokenValid = ()=>{
  return true;
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = ()=>{
  return true;
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = ()=>{
  return true;
}
export default [
  {
    url: "/login",
    method: "post",
    response: (data) => {
      var isUser = false;
      var isLogin = false;
      var userInfo = {};
      userData.forEach(function(item,index){
        if(item.account == data.body.account){
          isUser = true;
          if(item.password === data.body.password){
            isLogin = true;
            userInfo = item;
          }
        }
      });
      if(isUser){
        if(isLogin){
          var token = createdToken();
          return {
            code: 200,
            status:true,
            info:"登录成功！",
            data:userInfo,
            accessToken:token.accessToken,
            refreshToken:token.refreshToken,
          }
        }else{
          return {
            code: 200,
            status:false,
            info:"密码错误！",
          }
        }
      }else{
        return {
          code: 200,
          status:false,
          info:"用户名不存在！",
        }
      }
    }
  },
  {
    url: "/refreshToken",
    method: "post",
    response: ({refreshToken}) => {
      // 这里接收前端接口参数中回传的refreshToken，需要判断refreshToken是否也失效了，如果失效了，需要返回登录超时，如果未失效，则返回新的accessToken、expireTime、refreshToken
      if(isTokenValid()){
        var token = createdToken();
        return {
          code: 200,
          status:true,
          info:"token更新成功！",
          data:{
            accessToken:token.accessToken,
            refreshToken:token.refreshToken,
          },
        }
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data:{},
        }
      }
    }
  },
  {
    url: "/logout",
    method: "post",
    response: () => {
      if(isRefreshTokenValid()){
        if(isAccessTokenValid()){
          return {
            code: 200,
            status:true,
            info:"退出登录成功！"
          };
        }else{
          return {
            code: 201004,
            status:false,
            info:"token已失效，请更新token！",
            data: []
          };
        }
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data: []
        };
      }
    }
  }
] as MockMethod[];

```
这里是新增了`logout`接口，其也需要进行`token`验证，后端接口写好后，前端封装调用接口方法，修改`src/api/user.ts`文件，新增以下方法：
```
// 退出登陆
export const logout = () => {
  return http.request("post", "/logout");
};
```
此时接口封装已完成，接下来就是调用接口
## 调用退出登录接口
修改`src/layout/components/headerPanel.vue`文件：
```
<script setup lang="ts">
import { computed } from "vue";
import { storageLocal } from "@/utils/storage";
import Hamburger from "./hamBurger/index.vue";
import { useStore } from 'vuex';
import { useRouter } from "vue-router";
import SvgIcon from "@/components/SvgIcon/index.vue";
import {logout} from "@/api/user";
import { removeToken } from "@/utils/auth";
const store = useStore();
const router = useRouter();
const sidebar = computed(() => {
  return store.state.app.sidebar;
});
function toggleSideBar() {
  store.dispatch("app/toggleSideBar")
}
let userInfo = storageLocal.getItem("userInfo");
// 退出登录
const Logout = () => {
  logout().then((res:any)=>{
    storageLocal.removeItem("userInfo");
    removeToken();
    router.push("/login");
  });
  
};
</script>

<template>
  <div class="header">
    <div class="header-left">
      <Hamburger :is-active="sidebar.opened" class="hamburger-container" @toggleClick="toggleSideBar" />
    </div>
    <div class="header-center"></div>
    <div class="header-right">
      <!-- 退出登陆 -->
      <el-dropdown>
        <span class="el-dropdown-link">
          <img :src="userInfo.avatar" />
          <p>{{ userInfo.userName }}</p>
        </span>
        <template #dropdown>
          <el-dropdown-menu class="logout">
            <el-dropdown-item @click="Logout"><span class="icon-span"><SvgIcon icon-class="SwitchButton"></SvgIcon></span><span class="txt-span">退出系统</span></el-dropdown-item>
          </el-dropdown-menu>
        </template>
      </el-dropdown>
    </div>
  </div>
</template>
<style lang="scss" scoped></style>
```
此时还需要将`/Login`路由修改以下，将其首字母改为小写，因为在退出登录，或者直接访问登录页时，导航守卫中会自动把`Login`路由首字母转为小写，路由本身对字母大小写不敏感，但导航守卫中的判断，对大小写是敏感的，所以这里需要将导航守卫中关于`/Login`路由的判断修改以下：
```
// 路由白名单 这里将login路由首字母改为小写，导航守卫中，/Login路由被自动转为了小写
const whiteList = ["/login"];
router.beforeEach((to, _from, next) => {
  const token = getToken();
  NProgress.start();
  const externalLink = to?.redirectedFrom?.fullPath;
  if(token){
    if(_from?.name==="Login"||!_from?.name){
      // 如果是从登录页过来的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
      if (store.state.routeMenus.routeMenus.length === 0){
        initRouter(constantNotRootRoutes).then(() => {
          router.push(to.path);
        });
      }
      next();
    }else{
      // 如果是其他页面跳转的，且其他页面的name存在，则再判断当前跳转页是否是外链页面
      if(externalLink&&externalLink.includes("http")){
        openLink(`http${split(externalLink, "http")[1]}`);
        NProgress.done();
      }else{
        next();
      }
    }
  }else{// 未登录
    // 当前访问页面不是登陆页面
    if (to.path !== "/login") {
      // 当前访问页面包含在路由白名单中，可直接跳转
      if (whiteList.includes(to.path)) {
        next();
      } else {
        // 当前访问页面需要登录才能访问，需先跳转到登陆页，并将当前访问路由通过链接保存，在登陆后，获取该路由进行跳转
        next(`/Login?redirect=${to.path}`);
      }
    } else {
      // 当前访问页面本身就是登录页，直接跳转
      next();
    }
  }
});
```
`src/router/remaining.ts`文件中`/Login`路由也改为小写，此时退出登录功能就完善了。