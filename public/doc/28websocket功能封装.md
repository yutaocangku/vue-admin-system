﻿# websocket功能封装
在`src/utils`文件夹下新建`websocket/index.ts`路径文件：
```
import {warnMessage} from "@/utils/message";
import { getToken } from "@/utils/auth";
import { loadEnv } from "^/index";
const { VITE_WEBSOCKET_WSS } = loadEnv();
interface socket {
	websocket: any
	wssURL: string
	sendData:object|null
	lockReconnect: boolean
	reconnectTimer: any
	connectWebsocket:(url, agentData, successCallback, errCallback) => any
	createWebSocket:()=>void
	initWebSocketEventHandler: () => any
	onWebsocketOpen:(e)=>void
	onWebsocketMessage:(e)=>void
	onWebsocketClose:(e)=>void
	onWebsocketError:(e)=>void
	closeWebsocket: () => void
	reconnect: () => void
	messageCallback:(e)=>void
	errorCallback:()=>void
	heartbeatCheck: {
		timeout:number
		timeoutTimer:any
		serverTimeoutTimer:any
		reset:()=>void
		stop:()=>void
		start:()=>void
	}
}

const socket: socket = {
	// websocket实例
	websocket: null,
	// websocket链接地址
	wssURL: VITE_WEBSOCKET_WSS,
	// 发送给后台的数据
	sendData:null,
	// 是否执行重连（true:不执行；false:执行）
	lockReconnect: false,
	// 重连定时器
	reconnectTimer: null,
	// 获取后端数据成功的回调函数
	messageCallback: null,
	// 获取后端数据失败的回调函数
	errorCallback: null,
	/**
	 * 发起websocket连接请求函数
	 * @param {string} url ws连接地址
	 * @param {Object} agentData 传给后台的参数
	 * @param {function} successCallback 接收到ws数据，对数据进行处理的回调函数
	 * @param {function} errCallback ws连接错误的回调函数
	 */
	connectWebsocket:(url, agentData, successCallback, errCallback)=>{
		socket.wssURL = socket.wssURL+url;
		socket.sendData = agentData;
		socket.createWebSocket();
		socket.messageCallback = successCallback;
		socket.errorCallback = errCallback;
	},
	// 创建ws链接函数
	createWebSocket:()=>{
		if (!('WebSocket' in window)) {
			warnMessage('浏览器不支持WebSocket')
			return null
		}
		try{
			// websocket实例化
			socket.websocket = new WebSocket(socket.wssURL);
			// 初始化websocket事件函数
			socket.initWebSocketEventHandler();
		}catch(e){
			console.log("websocket连接异常，开始重连");
			socket.reconnect();
		}
	},
	// 初始化websocket连接事件工具方法
	initWebSocketEventHandler: () => {
		try{
			// 连接成功
			socket.websocket.onopen = (event) => {
				socket.onWebsocketOpen(event);
				// 开启心跳检测
				socket.heartbeatCheck.start();
			}
			// 监听服务器端返回的信息
			socket.websocket.onmessage = (e: any) => {
				socket.onWebsocketMessage(e);
				// 开启心跳检测
				socket.heartbeatCheck.start();
			}
			// 执行关闭websocket通信事件
			socket.websocket.onclose = (e: any) => {
				console.log("onclose执行关闭事件");
				socket.onWebsocketClose(e);
			}
			// 连接发生错误
			socket.websocket.onerror = function(e:any) {
				console.log('onerror执行error事件，开始重连');
				socket.onWebsocketError(e);
				socket.reconnect();
			}
		}catch(e){
			console.log("websocket连接绑定事件失败，开始重连");
			socket.reconnect();
		}
	},
	// websocket进行连接
	onWebsocketOpen:(event)=>{
		console.log('websocket通信开启');
		// 客户端与服务器端通信
		// socket.websocket.send('我发送消息给服务端');
		// 添加状态判断，当为OPEN时，发送消息
		if (socket.websocket.readyState === socket.websocket.OPEN) { // socket.websocket.OPEN = 1 
			// 发给后端的数据需要字符串化
			socket.websocket.send(JSON.stringify(socket.sendData));
		}
		if (socket.websocket.readyState === socket.websocket.CLOSED) { // socket.websocket.CLOSED = 3 
			console.log('socket.websocket.readyState=3, ws连接异常，开始重连');
			// 开始进行重连
			socket.reconnect();
			// 执行websocket通信失败的回调函数
			socket.errorCallback();
		}
	},
	// websocket监听服务器端返回消息
	onWebsocketMessage:(event)=>{
		const jsonStr = event.data;
		console.log('onWebsocketMessage接收到服务器的数据: ', jsonStr);
		// 执行websocket通信成功的回调函数
		socket.messageCallback(jsonStr);
	},
	// websocket执行关闭事件
	onWebsocketClose:(event)=>{
		console.log("websocket通信关闭");
		// e.code === 1000  表示正常关闭。 无论为何目的而创建, 该链接都已成功完成任务。
		// e.code !== 1000  表示非正常关闭。
		console.log('onclose event: ', event)
		if (event && event.code !== 1000) {
			console.log('非正常关闭');
			// 执行websocket通信失败的回调函数
			socket.errorCallback();
			// 如果不是手动关闭，这里的重连会执行；如果调用了手动关闭函数，这里重连不会执行
			socket.reconnect();
		}
	},
	// websocket执行连接错误事件，
	onWebsocketError:(event)=>{
		console.log('onWebsocketError：',event.data);
		// 执行websocket通信失败的回调函数
		socket.errorCallback();
	},
	// 手动关闭websocket
	closeWebsocket: () => {
		if(socket.websocket){
			console.log("手动关闭了websocket通信");
			socket.websocket.close(); // 关闭websocket
			// socket.websocket.onclose() // 关闭websocket(如果上面的关闭不生效就加上这一条)
			// 关闭重连
			socket.lockReconnect = true;
			socket.reconnectTimer && clearTimeout(socket.reconnectTimer);
			// 关闭心跳检查
			socket.heartbeatCheck.stop();
		}
	},
	// 重新连接
	reconnect: () => {
		if (socket.lockReconnect) {
			return;
		}
		console.log("3秒后进行重连");
		socket.lockReconnect = true;
		// 没连接上会一直重连，设置延迟避免请求过多
		socket.reconnectTimer && clearTimeout(socket.reconnectTimer);
		socket.reconnectTimer = setTimeout(() => {
			console.log('重连...' + socket.wssURL);
			socket.createWebSocket();
			socket.lockReconnect = false;
			console.log('重连完成');
		}, 3000);
	},
	// 心跳检查，查看websocket是否还在正常链接中，心跳是用来测试链接是否存在和对方是否在线
	heartbeatCheck: {
		timeout:15000,
		timeoutTimer:null,
		serverTimeoutTimer:null,
		// 重启
		reset:()=>{
			clearTimeout(socket.heartbeatCheck.timeoutTimer);
			clearTimeout(socket.heartbeatCheck.serverTimeoutTimer);
			socket.heartbeatCheck.start();
		},
		// 停止
		stop:()=>{
			clearTimeout(socket.heartbeatCheck.timeoutTimer);
    	clearTimeout(socket.heartbeatCheck.serverTimeoutTimer);
		},
		// 开始
		start:()=>{
			socket.heartbeatCheck.timeoutTimer && clearTimeout(socket.heartbeatCheck.timeoutTimer);
			socket.heartbeatCheck.serverTimeoutTimer && clearTimeout(socket.heartbeatCheck.serverTimeoutTimer);
			// 15s之内如果没有收到后台的消息，则认为是连接断开了，需要重连
			socket.heartbeatCheck.timeoutTimer = setTimeout(() => {
				console.log("心跳检查，发送ping到后台");
				try {
					// 这里需要看后台程序需要的是什么类型数据字段，是否需要回传token，也需要看后台程序是否做了websocket发送ping帧的功能
					const data = { 
						ping: true,
						token:getToken()
					};
					socket.websocket.send(JSON.stringify(data));
				} catch (err) {
					console.log("发送ping异常");
				}
				console.log("内嵌定时器socket.heartbeatCheck.serverTimeoutTimer: ", socket.heartbeatCheck.serverTimeoutTimer)
				// 内嵌定时器
				socket.heartbeatCheck.serverTimeoutTimer = setTimeout(() => {
					console.log("没有收到后台的数据，重新连接");
					socket.reconnect();
				}, socket.heartbeatCheck.timeout)
			}, socket.heartbeatCheck.timeout)
		}
	},
}

export default socket
```
然后在三个环境变量文件中，都先加上`websocket`后端连接地址的配置:
```
VITE_WEBSOCKET_WSS = ""
```
修改`types/global.d.ts`文件的类型定义：
```
declare interface ViteEnv {
  VITE_PORT: number;
  VITE_OUTDIR: string;
  VITE_PUBLIC_PATH: string;
  VITE_PROXY_API_PATH: string;
  VITE_BASE_API_PATH: string;
  VITE_LEGACY:boolean;
  VITE_WEBSOCKET_WSS:string;
}
```
修改`build/index.ts`文件中环境变量的默认值：
```
// 此处为默认值，无需修改
const ret: ViteEnv = {
  VITE_PORT: 11564,
  VITE_OUTDIR: "",
  VITE_PUBLIC_PATH: "",
  VITE_PROXY_API_PATH: "",
  VITE_BASE_API_PATH: "",
  VITE_LEGACY:false,
  VITE_WEBSOCKET_WSS:''
};
```
此时关于`websocket`通信功能就封装好，接下来是在要做的地方进行调用。

# websocket使用
`websocket`功能在开启后，是一直处在和后端的通信状态，其使用是作为需要实时更新数据的地方，如上一张的消息通知功能，或者某一个页面的实时信息功能，消息通知组件在框架层，不存在切换页面的情况，所以该组件使用`websocket`不会存在手动关闭通信状态的情况，只需要建立连接即可，而如果是某个页面内的实时信息功能，就需要在切换路由页面时，手动关闭通信状态，使用方法与**DOM元素监听**类似，需要区分页面是否开启了状态缓存功能，根据结果使用不同的生命周期函数开启通信，以及关闭通信

## 框架层使用
修改`src/layout/components/notice/index.vue`文件：
```
import socket from '@/utils/websocket';

socket.connectWebsocket(
  // websocket连接地址
  "",
  // 传递给后台的数据
  {},
  // 成功拿到后台返回的数据的回调函数
  (data)=>{
    console.log("成功接收数据：",data);
  },
  // websocket连接失败的回调函数
  ()=>{
    console.log("websocket连接失败");
  }
);
```
添加了`websocket`通信连接后，消息提醒通知组件的数据就是通过`websocket`通信返回的了，之前做的接口相关返回数据，就可以删除了，这里只记录`websocket`使用方法，暂时未和后端走通通信，先保留`mock`获取数据。

## 页面内使用
页面内使用，就需要考虑路由切换时需要主动关闭`websocket`通信，以及在页面状态缓存功能是否开启时，启动以及关闭通信需要在哪个生命周期内进行执行，这里修改`src/views/FrontEndLibrary/ViewsLibrary/ReDemo/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'ReDemo'
}
</script>
<script setup lang="ts">
import { ref,unref,onMounted,onBeforeUnmount,onActivated,onDeactivated,computed } from 'vue';
import { addResizeListener, removeResizeListener } from '@/utils/resize';
import { useRoute } from "vue-router";
import socket from '@/utils/websocket';
const route = useRoute();
const reDemo = ref();
const update = () => {
  console.log("调用了监听事件1111");
};
// 获取当前页面的缓存状态是否被开启
const isKeepAlive = route.meta.keepAlive;
// 在组件挂载到页面后执行
onMounted(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if(!isKeepAlive){
    console.log("组件被挂载");
    addResizeListener(unref(reDemo), update);
    // 启动websocket通信
    socket.connectWebsocket(
      // websocket连接地址
      "",
      // 传递给后台的数据
      {},
      // 成功拿到后台返回的数据的回调函数
      (data)=>{
        console.log("成功接收数据：",data);
      },
      // websocket连接失败的回调函数
      ()=>{
        console.log("websocket连接失败");
      }
    );
  }
});
// 页面未启用keep-alive时，则需要在此生命周期函数内（组件被卸载之前执行）移除监听
onBeforeUnmount(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if(!isKeepAlive){
    console.log("组件即将被卸载");
    removeResizeListener(unref(reDemo),update);
    // 关闭websocket通信
    socket.closeWebsocket();
  }
});

// keep-alive启用时，页面被激活时使用
onActivated(() => {
  // 开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if(isKeepAlive){
    console.log("组件被激活");
    addResizeListener(unref(reDemo), update);
    // 启动websocket通信
    socket.connectWebsocket(
      // websocket连接地址
      "",
      // 传递给后台的数据
      {},
      // 成功拿到后台返回的数据的回调函数
      (data)=>{
        console.log("成功接收数据：",data);
      },
      // websocket连接失败的回调函数
      ()=>{
        console.log("websocket连接失败");
      }
    );
  }
});
// 页面启用keep-alive时，则需要在此生命周期函数内（组件切换，老组件消失的时候执行）移除监听事件
onDeactivated(()=>{
  // 开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if(isKeepAlive){
    console.log("组件已被切换");
    removeResizeListener(unref(reDemo),update);
    // 关闭websocket通信
    socket.closeWebsocket();
  }
});
const value1 = ref("")
</script>
<template>
  <div class="re-demo-page" ref="reDemo">
    <h2>测试专用页面</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
以上修改完成后，运行项目，打开页面，终端能正常打印连接过程，不过目前没有与后端通信，`websocket`并未被实例化，所以在切换页面时，未走手动关闭的逻辑。`websocket`通信的封装与使用至此全部完成。