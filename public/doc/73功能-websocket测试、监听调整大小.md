﻿# websocket测试
没有真实的websocket链接，这里只是模拟测试，通过websocket连接的失败回调显示连接的过程。

修改`src/layout/components/notice/index.vue`文件，失败回调添加参数返回：
```
socket.connectWebsocket(
  // websocket连接地址
  '',
  // 传递给后台的数据
  {},
  // 成功拿到后台返回的数据的回调函数
  (data) => {
    console.log('成功接收数据：', data)
  },
  // websocket连接失败的回调函数
  (e) => {
    console.log('websocket连接失败', e)
  }
)
```

修改`src/views/Functions/WebsocketTest/index.vue`文件：
```
<script setup lang="ts" name="WebsocketTest">
import socket from '@/utils/websocket'
import useVkConfig from '@/hooks/vkConfig'
const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型
const route = useRoute()
// 获取当前页面的缓存状态是否被开启
const isKeepAlive = route.meta.keepAlive && vkConfig.value.isKeepAlive

const processTextList = ref([])
// 在组件挂载到页面后执行
onMounted(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if (!isKeepAlive) {
    processTextList.value.push('开始尝试进行websocket连接')
    // 启动websocket通信
    socket.connectWebsocket(
      // websocket连接地址
      '',
      // 传递给后台的数据
      {},
      // 成功拿到后台返回的数据的回调函数
      (data) => {
        console.log('成功接收数据：', data)
      },
      // websocket连接失败的回调函数
      (e) => {
        processTextList.value.push(e)
        console.log('websocket连接失败', e)
      }
    )
  }
})
// 页面未启用keep-alive时，则需要在此生命周期函数内（组件被卸载之前执行）移除监听
onBeforeUnmount(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if (!isKeepAlive) {
    // 关闭websocket通信
    socket.closeWebsocket()
  }
})

// keep-alive启用时，页面被激活时使用
onActivated(() => {
  // 开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if (isKeepAlive) {
    processTextList.value.push('开始尝试进行websocket连接')
    // 启动websocket通信
    socket.connectWebsocket(
      // websocket连接地址
      '',
      // 传递给后台的数据
      {},
      // 成功拿到后台返回的数据的回调函数
      (data) => {
        processTextList.value.push('websocket连接成功')
        console.log('成功接收数据：', data)
      },
      // websocket连接失败的回调函数
      (e) => {
        processTextList.value.push(e)
        console.log(e)
      }
    )
  }
})
// 页面启用keep-alive时，则需要在此生命周期函数内（组件切换，老组件消失的时候执行）移除监听事件
onDeactivated(() => {
  // 开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if (isKeepAlive) {
    // 关闭websocket通信
    socket.closeWebsocket()
  }
})
</script>
<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <h2>websocket测试</h2>
      <p v-for="(item, index) in processTextList" :key="index">{{item}}</p>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}

h2{
  font-weight: bold;
  font-size: 24px;
}

.el-card{
  display: flex;

  :deep(.el-card__body){
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    p{
      margin-top: 10px;
      text-align: center;
    }
  }
}
</style>
```
该页面将在打开后尝试连接websocket，会直接找到失败，并尝试重连，重连失败，完成一次websocket的连接失败过程

修改`src/utils/websocket/index.ts`文件：
```
import { warnMessage } from '@/utils/message'
import { loadEnv } from '^/index'
import { transformI18n } from '@/plugins/i18n'
import { decodeAES, aesKey, iv } from '@/utils/aesRsa'
const { VITE_WEBSOCKET_WSS } = loadEnv()
interface socketType {
  websocket: any
  wssURL: string
  sendData: object | null
  lockReconnect: boolean
  reconnectTimer: any
  connectWebsocket: (url, agentData, successCallback, errCallback) => any
  createWebSocket: () => void
  initWebSocketEventHandler: () => any
  onWebsocketOpen: (e) => void
  onWebsocketMessage: (e) => void
  onWebsocketClose: (e) => void
  onWebsocketError: (e) => void
  closeWebsocket: () => void
  reconnect: () => void
  messageCallback: (e) => void
  errorCallback: (e) => void
  heartbeatCheck: {
    timeout: number
    timeoutTimer: any
    serverTimeoutTimer: any
    reset: () => void
    stop: () => void
    start: () => void
  }
}

const socket: socketType = {
  // websocket实例
  websocket: null,
  // websocket链接地址
  wssURL: VITE_WEBSOCKET_WSS,
  // 发送给后台的数据
  sendData: null,
  // 是否执行重连（true:不执行；false:执行）
  lockReconnect: false,
  // 重连定时器
  reconnectTimer: null,
  // 获取后端数据成功的回调函数
  messageCallback: null,
  // 获取后端数据失败的回调函数
  errorCallback: null,
  /**
   * 发起websocket连接请求函数
   * @param {string} url ws连接地址
   * @param {Object} agentData 传给后台的参数
   * @param {function} successCallback 接收到ws数据，对数据进行处理的回调函数
   * @param {function} errCallback ws连接错误的回调函数
   */
  connectWebsocket: (url, agentData, successCallback, errCallback) => {
    socket.wssURL = socket.wssURL + url
    socket.sendData = agentData
    socket.createWebSocket()
    socket.messageCallback = successCallback
    socket.errorCallback = errCallback
  },
  // 创建ws链接函数
  createWebSocket: () => {
    if (!('WebSocket' in window)) {
      warnMessage(transformI18n('errors.errorDontSupportWebSocket', true))
      return null
    }
    try {
      // websocket实例化
      socket.websocket = new WebSocket(socket.wssURL)
      // 初始化websocket事件函数
      socket.initWebSocketEventHandler()
    } catch (e) {
      // 开始重连
      socket.reconnect()
    }
  },
  // 初始化websocket连接事件工具方法
  initWebSocketEventHandler: () => {
    try {
      // 连接成功
      socket.websocket.onopen = (event) => {
        socket.onWebsocketOpen(event)
        // 开启心跳检测
        socket.heartbeatCheck.start()
      }
      // 监听服务器端返回的信息
      socket.websocket.onmessage = (e: any) => {
        socket.onWebsocketMessage(e)
        // 开启心跳检测
        socket.heartbeatCheck.start()
      }
      // 执行关闭websocket通信事件
      socket.websocket.onclose = (e: any) => {
        socket.onWebsocketClose(e)
      }
      // 连接发生错误
      socket.websocket.onerror = function (e: any) {
        socket.onWebsocketError(e)
        socket.reconnect()
      }
    } catch (e) {
      // 执行websocket通信失败的回调函数
      socket.errorCallback('websocket连接绑定事件失败，开始重连')
      socket.reconnect()
    }
  },
  // websocket进行连接
  onWebsocketOpen: () => {
    // 客户端与服务器端通信
    // socket.websocket.send('我发送消息给服务端');
    // 添加状态判断，当为OPEN时，发送消息
    if (socket.websocket.readyState === socket.websocket.OPEN) {
      // socket.websocket.OPEN = 1
      // 发给后端的数据需要字符串化
      socket.websocket.send(JSON.stringify(socket.sendData))
    }
    if (socket.websocket.readyState === socket.websocket.CLOSED) {
      // socket.websocket.CLOSED = 3
      // 执行websocket通信失败的回调函数
      socket.errorCallback('socket.websocket.readyState=3, ws连接异常，开始重连')
      // 开始进行重连
      socket.reconnect()
    }
  },
  // websocket监听服务器端返回消息
  onWebsocketMessage: (event) => {
    const jsonStr = event.data
    console.log('onWebsocketMessage接收到服务器的数据: ', jsonStr)
    // 执行websocket通信成功的回调函数
    socket.messageCallback(jsonStr)
  },
  // websocket执行关闭事件
  onWebsocketClose: (event) => {
    // e.code === 1000  表示正常关闭。 无论为何目的而创建, 该链接都已成功完成任务。
    // e.code !== 1000  表示非正常关闭。
    console.log('onclose event: ', event)
    if (event && event.code !== 1000) {
      // 执行websocket通信失败的回调函数
      socket.errorCallback('socket非正常关闭，正在尝试重连')
      // 如果不是手动关闭，这里的重连会执行；如果调用了手动关闭函数，这里重连不会执行
      socket.reconnect()
    }
  },
  // websocket执行连接错误事件，
  onWebsocketError: (event) => {
    console.log('onWebsocketError：', event.data)
    // 执行websocket通信失败的回调函数
    socket.errorCallback(event.data)
  },
  // 手动关闭websocket
  closeWebsocket: () => {
    if (socket.websocket) {
      socket.websocket.close() // 关闭websocket
      // socket.websocket.onclose() // 关闭websocket(如果上面的关闭不生效就加上这一条)
      // 关闭重连
      socket.lockReconnect = true
      socket.reconnectTimer && clearTimeout(socket.reconnectTimer)
      // 关闭心跳检查
      socket.heartbeatCheck.stop()
    }
  },
  // 重新连接
  reconnect: () => {
    if (socket.lockReconnect) {
      socket.errorCallback('未能正常连接，请刷新重试')
      return
    }
    socket.lockReconnect = true
    // 没连接上会一直重连，设置延迟避免请求过多
    socket.reconnectTimer && clearTimeout(socket.reconnectTimer)
    socket.reconnectTimer = setTimeout(() => {
      socket.errorCallback('连接失败，正在重新建立连接...')
      socket.createWebSocket()
      socket.lockReconnect = false
    }, 3000)
  },
  // 心跳检查，查看websocket是否还在正常链接中，心跳是用来测试链接是否存在和对方是否在线
  heartbeatCheck: {
    timeout: 15000,
    timeoutTimer: null,
    serverTimeoutTimer: null,
    // 重启
    reset: () => {
      clearTimeout(socket.heartbeatCheck.timeoutTimer)
      clearTimeout(socket.heartbeatCheck.serverTimeoutTimer)
      socket.heartbeatCheck.start()
    },
    // 停止
    stop: () => {
      clearTimeout(socket.heartbeatCheck.timeoutTimer)
      clearTimeout(socket.heartbeatCheck.serverTimeoutTimer)
    },
    // 开始
    start: () => {
      socket.heartbeatCheck.timeoutTimer && clearTimeout(socket.heartbeatCheck.timeoutTimer)
      socket.heartbeatCheck.serverTimeoutTimer && clearTimeout(socket.heartbeatCheck.serverTimeoutTimer)
      // 15s之内如果没有收到后台的消息，则认为是连接断开了，需要重连
      socket.heartbeatCheck.timeoutTimer = setTimeout(() => {
        try {
          // 这里需要看后台程序需要的是什么类型数据字段，是否需要回传token，也需要看后台程序是否做了websocket发送ping帧的功能
          const data = {
            ping: true,
            token: JSON.parse(decodeAES(localStorage.getItem('userInfo'), aesKey, iv)).accessToken
          }
          socket.websocket.send(JSON.stringify(data))
        } catch (err) {
          console.log('发送ping异常')
        }
        console.log('内嵌定时器socket.heartbeatCheck.serverTimeoutTimer: ', socket.heartbeatCheck.serverTimeoutTimer)
        // 内嵌定时器
        socket.heartbeatCheck.serverTimeoutTimer = setTimeout(() => {
          console.log('没有收到后台的数据，重新连接')
          socket.errorCallback('没有收到后台的数据，重新连接')
          socket.reconnect()
        }, socket.heartbeatCheck.timeout)
      }, socket.heartbeatCheck.timeout)
    }
  }
}

export default socket
```
这里将失败回调函数都做了参数传递，能让前端接收到连接失败的回调信息，用来显示连接过程。


# 监听调整大小
项目中经常用到要监听元素大小改变，并在改变时做一些其它操作，这里主要封装和使用监听事件

修改`mock/asyncRoutes.ts`文件，添加监听调整大小页面菜单数据：
```
  {
    id: 118,
    pid: 2,
    title: '监控调整大小',
    enTitle: 'Monit Resize',
    icon: 'ri:shape-2-line',
    url: '/Functions/MonitResize',
    path: '/Functions/MonitResize',
    redirect: '',
    showLink: true,
    sort: 21,
    route: 'MonitResize',
    transitionName: '',
    keepAlive: true,
    isHideTab: false,
    multiTab: 0,
    isFixedTab: false
  }
```
新建`src/views/Functions/MonitResize/index.vue`文件：
```
<script setup lang="ts" name="MonitResize">
import { templateRef, useResizeObserver, useDebounceFn } from '@vueuse/core'
import { addResizeListener, removeResizeListener } from '@/utils/resize'
import useVkConfig from '@/hooks/vkConfig'
const route = useRoute()
const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型
const resizeDom = ref()
const itemWidth = ref(0)
const itemHeight = ref(0)
const update = () => {
  itemWidth.value = resizeDom.value.offsetWidth
  itemHeight.value = resizeDom.value.offsetHeight
}
// 获取当前页面的缓存状态是否被开启
const isKeepAlive = route.meta.keepAlive && vkConfig.value.isKeepAlive
// 在组件挂载到页面后执行
onMounted(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if (!isKeepAlive) {
    addResizeListener(unref(resizeDom), update)
  }
})
// 页面未启用keep-alive时，则需要在此生命周期函数内（组件被卸载之前执行）移除监听
onBeforeUnmount(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if (!isKeepAlive) {
    removeResizeListener(unref(resizeDom), update)
  }
})

// keep-alive启用时，页面被激活时使用
onActivated(() => {
  // 开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if (isKeepAlive) {
    addResizeListener(unref(resizeDom), update)
  }
})
// 页面启用keep-alive时，则需要在此生命周期函数内（组件切换，老组件消失的时候执行）移除监听事件
onDeactivated(() => {
  // 开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if (isKeepAlive) {
    removeResizeListener(unref(resizeDom), update)
  }
})

const bodyWidth = ref(0)
const bodyHeight = ref(0)
const monitBodyResize = () => {
  const rect = document.body.getBoundingClientRect()
  bodyWidth.value = rect.width
  bodyHeight.value = rect.height
}
// 监听元素尺寸变化
useResizeObserver(
  document.body,
  useDebounceFn(() => {
    monitBodyResize()
  }, 200)
)

const resizeCoreDom = templateRef<HTMLElement | null>('resizeCoreDom', null)
const coreWidth = ref(0)
const coreHeight = ref(0)
const monitCoreResize = () => {
  coreWidth.value = resizeCoreDom.value.offsetWidth
  coreHeight.value = resizeCoreDom.value.offsetHeight
}
// 监听元素尺寸变化
useResizeObserver(
  resizeCoreDom,
  useDebounceFn(() => {
    monitCoreResize()
  }, 200)
)
</script>

<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <div class="resize-container resize-body">
        <p>
          useResizeObserver监听body宽高变化: 宽：
          <span>{{bodyWidth}}</span>
          、高：
          <span>{{bodyHeight}}</span>
        </p>
      </div>
      <div class="resize-container resize-core">
        <div ref="resizeCoreDom" class="resize-core-content">
          <p>
            useResizeObserver监听元素宽高变化: 宽：
            <span>{{coreWidth}}</span>
            、高：
            <span>{{coreHeight}}</span>
          </p>
        </div>
      </div>
      <div ref="resizeDom" class="resize-container resize-item">
        <p>
          自定义封装ResizeObserver监听元素宽高变化: 宽：
          <span>{{itemWidth}}</span>
          、高：
          <span>{{itemHeight}}</span>
        </p>
      </div>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}

.el-card{
  display: flex;

  :deep(.el-card__body){
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .resize-body{
      flex: 0 0 20%;
    }

    .resize-core{
      flex: 0 0 30%;
      padding: 0 10%;

      .resize-core-content{
        flex: 1;
      }
    }

    .resize-item{
      flex: 0 0 50%;
    }

    .resize-container,.resize-core-content{
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      width: 100%;

      p{
        font-weight: bold;
        font-size: 24px;
        text-align: center;

        span{
          color: var(--el-color-primary);
        }
      }
    }
  }
}
</style>
```
这里主要展示了使用`vueuse/core`提供的监听事件，以及自定义封装的监听事件，两种方法对页面元素进行监听都能正常执行。