﻿# element-plus组件深色主题配置
`element-plus`的组件使用，都已经形成页面，现在就看哪些组件在深色主题时，需要进行特定配置，在`element-plus`升级到`2.1.10`版本后，配置深色主题就变得很简单了。

1. 修改`src/utils/theme/index.ts`文件：
```
import { getObjKey } from "../index";
import {addClass,removeClass} from "@/utils/operate"

// 动态设置主题色，用来切换主题色，实现项目的换肤功能
export const setThemeColor = (color,menuBgColor,headerBgColor,darkTheme) => {

    document.documentElement.style.setProperty('color-scheme',!darkTheme?'dark':'light');

    // 背景色
    document.documentElement.style.setProperty('--el-bg-color',!darkTheme?colorMix(themeColor().white,darkThemeColor().darkColor,.935):'#ffffff');
    document.documentElement.style.setProperty('--el-bg-color-page',!darkTheme?darkThemeColor().darkColor:'#f6f8f9');
    document.documentElement.style.setProperty('--el-bg-color-overlay',!darkTheme?colorMix(themeColor().white,darkThemeColor().darkColor,.93):'#ffffff');

    // 文字类颜色
    document.documentElement.style.setProperty('--el-text-color-primary',!darkTheme?'rgba('+hexToRgb(darkThemeColor().textColor)+',.95)':'#515a6e');
    document.documentElement.style.setProperty('--el-text-color-regular',!darkTheme?'rgba('+hexToRgb(darkThemeColor().textColor)+',.85)':'#606266');
    document.documentElement.style.setProperty('--el-text-color-secondary',!darkTheme?'rgba('+hexToRgb(darkThemeColor().textColor)+',.65)':'#909399');
    document.documentElement.style.setProperty('--el-text-color-placeholder',!darkTheme?'rgba('+hexToRgb(darkThemeColor().textColor)+',.55)':'#a8abb2');
    document.documentElement.style.setProperty('--el-text-color-disabled',!darkTheme?'rgba('+hexToRgb(darkThemeColor().textColor)+',.4)':'#c0c4cc');

    // 边框类颜色
    document.documentElement.style.setProperty('--el-border-color-darker',!darkTheme?'rgba('+hexToRgb(darkThemeColor().borderColor)+',.35)':'#cdd0d6');
    document.documentElement.style.setProperty('--el-border-color-dark',!darkTheme?'rgba('+hexToRgb(darkThemeColor().borderColor)+',.3)':'#d4d7de');
    document.documentElement.style.setProperty('--el-border-color',!darkTheme?'rgba('+hexToRgb(darkThemeColor().borderColor)+',.25)':'#dcdfe6');
    document.documentElement.style.setProperty('--el-border-color-light',!darkTheme?'rgba('+hexToRgb(darkThemeColor().borderColor)+',.2)':'#e4e7ed');
    document.documentElement.style.setProperty('--el-border-color-lighter',!darkTheme?'rgba('+hexToRgb(darkThemeColor().borderColor)+',.15)':'#ebeef5');
    document.documentElement.style.setProperty('--el-border-color-extra-light',!darkTheme?'rgba('+hexToRgb(darkThemeColor().borderColor)+',.1)':'#f2f6fc');

    // 模块填充类颜色
    document.documentElement.style.setProperty('--el-fill-color-darker',!darkTheme?'rgba('+hexToRgb(darkThemeColor().fillColor)+',.2)':'#e6e8eb');
    document.documentElement.style.setProperty('--el-fill-color-dark',!darkTheme?'rgba('+hexToRgb(darkThemeColor().fillColor)+',.16)':'#ebedf0');
    document.documentElement.style.setProperty('--el-fill-color',!darkTheme?'rgba('+hexToRgb(darkThemeColor().fillColor)+',.12)':'#f0f2f5');
    document.documentElement.style.setProperty('--el-fill-color-light',!darkTheme?'rgba('+hexToRgb(darkThemeColor().fillColor)+',.08)':'#f5f7fa');
    document.documentElement.style.setProperty('--el-fill-color-lighter',!darkTheme?'rgba('+hexToRgb(darkThemeColor().fillColor)+',.04)':'#fafafa');
    document.documentElement.style.setProperty('--el-fill-color-extra-light',!darkTheme?'rgba('+hexToRgb(darkThemeColor().fillColor)+',.02)':'#fafcff');
    document.documentElement.style.setProperty('--el-fill-color-blank',!darkTheme?colorMix(themeColor().white,darkThemeColor().darkColor,.96):'#ffffff');

  // 设置主题色及衍生色
  document.documentElement.style.setProperty('--el-color-primary',color);
  document.documentElement.style.setProperty('--el-color-primary-rgb',hexToRgb(color));
  document.documentElement.style.setProperty('--el-color-primary-light-3',colorMix(!darkTheme?themeColor().black:themeColor().white,color,.7));
  document.documentElement.style.setProperty('--el-color-primary-light-5',colorMix(!darkTheme?themeColor().black:themeColor().white,color,.5));
  document.documentElement.style.setProperty('--el-color-primary-light-7',colorMix(!darkTheme?themeColor().black:themeColor().white,color,.3));
  document.documentElement.style.setProperty('--el-color-primary-light-8',colorMix(!darkTheme?themeColor().black:themeColor().white,color,.2));
  document.documentElement.style.setProperty('--el-color-primary-light-9',colorMix(!darkTheme?themeColor().black:themeColor().white,color,.1));
  document.documentElement.style.setProperty('--el-color-primary-dark-2',colorMix(!darkTheme?themeColor().black:themeColor().white,color,.8));

  // 设置成功色及衍生色
  document.documentElement.style.setProperty('--el-color-success',themeColor().success);
  document.documentElement.style.setProperty('--el-color-success-rgb',hexToRgb(themeColor().success));
  document.documentElement.style.setProperty('--el-color-success-light-3',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().success,.7));
  document.documentElement.style.setProperty('--el-color-success-light-5',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().success,.5));
  document.documentElement.style.setProperty('--el-color-success-light-7',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().success,.3));
  document.documentElement.style.setProperty('--el-color-success-light-8',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().success,.2));
  document.documentElement.style.setProperty('--el-color-success-light-9',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().success,.1));
  document.documentElement.style.setProperty('--el-color-success-dark-2',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().success,.8));

  // 设置警告色及衍生色
  document.documentElement.style.setProperty('--el-color-warning',themeColor().warning);
  document.documentElement.style.setProperty('--el-color-warning-rgb',hexToRgb(themeColor().warning));
  document.documentElement.style.setProperty('--el-color-warning-light-3',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().warning,.7));
  document.documentElement.style.setProperty('--el-color-warning-light-5',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().warning,.5));
  document.documentElement.style.setProperty('--el-color-warning-light-7',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().warning,.3));
  document.documentElement.style.setProperty('--el-color-warning-light-8',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().warning,.2));
  document.documentElement.style.setProperty('--el-color-warning-light-9',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().warning,.1));
  document.documentElement.style.setProperty('--el-color-warning-dark-2',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().warning,.8));

  // 设置危险色及衍生色
  document.documentElement.style.setProperty('--el-color-danger',themeColor().danger);
  document.documentElement.style.setProperty('--el-color-danger-rgb',hexToRgb(themeColor().danger));
  document.documentElement.style.setProperty('--el-color-danger-light-3',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().danger,.7));
  document.documentElement.style.setProperty('--el-color-danger-light-5',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().danger,.5));
  document.documentElement.style.setProperty('--el-color-danger-light-7',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().danger,.3));
  document.documentElement.style.setProperty('--el-color-danger-light-8',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().danger,.2));
  document.documentElement.style.setProperty('--el-color-danger-light-9',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().danger,.1));
  document.documentElement.style.setProperty('--el-color-danger-dark-2',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().danger,.8));
  
  // 设置错误色及衍生色
  document.documentElement.style.setProperty('--el-color-error',themeColor().error);
  document.documentElement.style.setProperty('--el-color-error-rgb',hexToRgb(themeColor().error));
  document.documentElement.style.setProperty('--el-color-error-light-3',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().error,.7));
  document.documentElement.style.setProperty('--el-color-error-light-5',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().error,.5));
  document.documentElement.style.setProperty('--el-color-error-light-7',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().error,.3));
  document.documentElement.style.setProperty('--el-color-error-light-8',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().error,.2));
  document.documentElement.style.setProperty('--el-color-error-light-9',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().error,.1));
  document.documentElement.style.setProperty('--el-color-error-dark-2',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().error,.8));
  
  // 设置信息色及衍生色
  document.documentElement.style.setProperty('--el-color-info',themeColor().info);
  document.documentElement.style.setProperty('--el-color-info-rgb',hexToRgb(themeColor().info));
  document.documentElement.style.setProperty('--el-color-info-light-3',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().info,.7));
  document.documentElement.style.setProperty('--el-color-info-light-5',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().info,.5));
  document.documentElement.style.setProperty('--el-color-info-light-7',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().info,.3));
  document.documentElement.style.setProperty('--el-color-info-light-8',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().info,.2));
  document.documentElement.style.setProperty('--el-color-info-light-9',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().info,.1));
  document.documentElement.style.setProperty('--el-color-info-dark-2',colorMix(!darkTheme?themeColor().black:themeColor().white,themeColor().info,.8));

  // 侧边栏的区分设置
  document.documentElement.style.setProperty('--el-sidebar-bg-color',!darkTheme?colorMix(themeColor().white,darkThemeColor().darkColor,.905):menuBgColor);
  document.documentElement.style.setProperty('--el-submenu-bg-color',!darkTheme?colorMix(themeColor().white,darkThemeColor().darkColor,.97):colorMix(themeColor().black,menuBgColor,.6));
  document.querySelector("body")?.setAttribute("data-menu-theme", getObjKey(menuBgColorSource(),menuBgColor));

  // 顶栏背景色的区分设置
  document.documentElement.style.setProperty('--el-header-bg-color',!darkTheme?colorMix(themeColor().white,darkThemeColor().darkColor,.865):headerBgColor);
  document.querySelector("body")?.setAttribute("data-header-theme", getObjKey(headerBgColorSource(),headerBgColor));

  // 为HTML标签添加主题色自定义属性
  document.querySelector("html")?.setAttribute("data-theme", getObjKey(primaryColorSource(),color));

  // 为html标签添加明暗主题的类名
  if(!darkTheme){
    removeClass(document.querySelector("html"), "light");
    addClass(document.querySelector("html"), "dark");
  }else{
    removeClass(document.querySelector("html"), "dark");
    addClass(document.querySelector("html"), "light");
  }
}
// 实现颜色混合功能，用来计算主题色的色系
export const colorMix = (c1, c2, ratio) => {
  ratio = Math.max(Math.min(Number(ratio), 1), 0)
  let r1 = parseInt(c1.substring(1, 3), 16)
  let g1 = parseInt(c1.substring(3, 5), 16)
  let b1 = parseInt(c1.substring(5, 7), 16)
  let r2 = parseInt(c2.substring(1, 3), 16)
  let g2 = parseInt(c2.substring(3, 5), 16)
  let b2 = parseInt(c2.substring(5, 7), 16)
  let r = Math.round(r1 * (1 - ratio) + r2 * ratio)
  let g = Math.round(g1 * (1 - ratio) + g2 * ratio)
  let b = Math.round(b1 * (1 - ratio) + b2 * ratio)
  let r3 = ('0' + (r || 0).toString(16)).slice(-2)
  let g3 = ('0' + (g || 0).toString(16)).slice(-2)
  let b3 = ('0' + (b || 0).toString(16)).slice(-2)
  return '#' + r3 + g3 + b3;
}
// 将hex格式的颜色值（#000|#000000）转化为rgba格式（rgba(0,0,0,1)）
export const hexToRgb = (color) => {
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
    var sColor = color.toLowerCase();
    if(sColor && reg.test(sColor)){
        if(sColor.length === 4){
            var sColorNew = "#";
            for(var i=1; i<4; i+=1){
                sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));	
            }
            sColor = sColorNew;
        }
        //处理六位的颜色值
        var sColorChange = [];
        for(var i=1; i<7; i+=2){
            sColorChange.push(parseInt("0x"+sColor.slice(i,i+2)));	
        }
        // return "rgba(" + sColorChange.join(",") + ")";
        return sColorChange.join(",");
    }else{
        return sColor;	
    }
}
// 主题色设置
export const primaryColorSource = () => {
  return {
    blue:themeColor().blue,
    green:themeColor().green,
    red:themeColor().red,
    pink:themeColor().pink,
    purple:themeColor().purple,
    cyan:themeColor().cyan,
    orange:themeColor().orange,
    gold:themeColor().gold,
  }
}
// 菜单主题色设置
export const menuBgColorSource = () => {
  return {
    light:themeColor().light,
    dark:themeColor().dark,
    blue:colorMix(themeColor().black,themeColor().blue,.175),
    green:colorMix(themeColor().black,themeColor().green,.175),
    red:colorMix(themeColor().black,themeColor().red,.175),
    pink:colorMix(themeColor().black,themeColor().pink,.175),
    purple:colorMix(themeColor().black,themeColor().purple,.175),
    cyan:colorMix(themeColor().black,themeColor().cyan,.175),
    orange:colorMix(themeColor().black,themeColor().orange,.175),
    gold:colorMix(themeColor().black,themeColor().gold,.175),
    gradient_dark_sidebar1:"gradient_dark_sidebar1",
    gradient_dark_sidebar2:"gradient_dark_sidebar2",
    gradient_light_sidebar1:"gradient_light_sidebar1",
    bg_dark_sidebar1:"bg_dark_sidebar1",
    bg_light_sidebar1:"bg_light_sidebar1",
  }
}
// header主题色设置
export const headerBgColorSource = () => {
  return {
    light:themeColor().light,
    dark:themeColor().dark,
    blue:colorMix(themeColor().white,themeColor().blue,.825),
    green:colorMix(themeColor().white,themeColor().green,.825),
    red:colorMix(themeColor().white,themeColor().red,.825),
    pink:colorMix(themeColor().white,themeColor().pink,.825),
    purple:colorMix(themeColor().white,themeColor().purple,.825),
    cyan:colorMix(themeColor().white,themeColor().cyan,.825),
    orange:colorMix(themeColor().white,themeColor().orange,.825),
    gold:colorMix(themeColor().white,themeColor().gold,.825),
    gradient_dark_header1:"gradient_dark_header1",
    gradient_dark_header2:"gradient_dark_header2",
    gradient_light_header1:"gradient_light_header1",
    bg_dark_header1:"bg_dark_header1",
    bg_light_header1:"bg_light_header1",
  }
}

// 系统颜色源，所有颜色都从这里开始衍生
export const themeColor = () => {
  return {
    // 用来做混合使用
    white:"#ffffff", // 白色
    black:"#000000", // 黑色
    // 侧边栏、顶栏除主题色色系外的白色系、黑色系
    light:"#ffffff", // 白色
    dark:"#161b22", // 黑色
    // 主题色色系
    blue:"#1890ff", // 蓝色
    green:"#41B584", // 绿色
    red:"#f34d37", // 红色
    pink:"#ff5c93", // 粉色
    purple:"#9c27b0", // 紫色
    cyan:"#13c2c2", // 青色
    orange:"#fa541c", // 橙色
    gold:"#f2be45", // 烫金色
    // 信息提示类色系
    success:"#13ce66", // 成功
    warning:"#ffba00", // 警告
    danger:"#ff4d4f", // 危险
    error:"#ff4d4f", // 错误
    info:"#909399", // 信息
  }
}
// 暗黑主题颜色
export const darkThemeColor = () => {
  return {
    darkColor:"#1e1e1e",
    textColor:"#f0f5ff",
    borderColor:"#f5f8ff",
    fillColor:"#fafcff",
  }
}

export const randomColor = () => {
  let colorList = [themeColor().blue,themeColor().green,themeColor().red,themeColor().pink,themeColor().purple,themeColor().cyan,themeColor().orange,themeColor().gold,themeColor().success,themeColor().warning,themeColor().danger]
  let index = Math.floor(Math.random()*colorList.length);
  return colorList[index];
}
```
改文件改动较大，主要是增加了暗系主题的颜色配置，以及`hex`色值转为`rgb`设置，菜单与顶栏的`white`,`black`字段改为了`light`,`dark`，另外主题色等色值根据`element-plus@2.1.9`版本的设置，重新设定了各自的衍生色值。暗系主题相关颜色也都根据`--el-bg-color-page`的值进行衍生生成。

2. 修改`src/utils/operate/index.ts`文件：
```
declare type RefType<T> = T | null;

export const hasClass = (ele: RefType<any>, cls: string): any => {
  return !!ele.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"));
};

export const addClass = (
  ele: RefType<any>,
  cls: string,
  extracls?: string
): any => {
  if (!hasClass(ele, cls)) ele.className += " " + cls;
  if (extracls) {
    if (!hasClass(ele, extracls)) ele.className += " " + extracls;
  }
  ele.className = ele.className.replace(/^\s*/,'');
  ele.className = ele.className.replace(/\s*$/,'');
  ele.className = ele.className.replace(/\s{2,}/g,' ');
};

export const removeClass = (
  ele: RefType<any>,
  cls: string,
  extracls?: string
): any => {
  if (hasClass(ele, cls)) {
    const reg = new RegExp("(\\s|^)" + cls + "(\\s|$)");
    ele.className = ele.className.replace(reg, " ").trim();
  }
  if (extracls) {
    if (hasClass(ele, extracls)) {
      const regs = new RegExp("(\\s|^)" + extracls + "(\\s|$)");
      ele.className = ele.className.replace(regs, " ").trim();
    }
  }
  ele.className = ele.className.replace(/^\s*/,'');
  ele.className = ele.className.replace(/\s*$/,'');
  ele.className = ele.className.replace(/\s{2,}/g,' ');
};

export const toggleClass = (
  flag: boolean,
  clsName: string,
  target?: RefType<any>
): any => {
  const targetEl = target || document.body;
  let { className } = targetEl;
  className = className.replace(clsName, "");
  targetEl.className = flag ? `${className} ${clsName} ` : className;
  targetEl.className = targetEl.className.replace(/^\s*/,'');
  targetEl.className = targetEl.className.replace(/\s*$/,'');
  targetEl.className = targetEl.className.replace(/\s{2,}/g,' ');
};
```
这里将类名操作的方法增加了移除多余空格的操作。

3. 修改`src/layout/components/setings/index.vue`文件：
```
<script setup lang="ts">
import { ref,computed,getCurrentInstance,reactive, onMounted } from "vue";
import { useStore } from 'vuex';
import { layoutType } from "../../types";
import { $t as t } from "@/plugins/i18n";
import { setThemeColor,primaryColorSource,menuBgColorSource,headerBgColorSource } from "@/utils/theme";
import sun from "@/assets/svg/sun.svg";
import moon from "@/assets/svg/moon.svg";
const instance = getCurrentInstance().appContext.app.config.globalProperties.$storage;
const store = useStore();
const device = computed(() => {
  return store.state.app.device;
});
const showSetting = ref(store.state.app.showSettings)
let showSettings = computed({
    get () {
        return store.state.app.showSettings;
    },
    set () {
    }
});
let vkConfig = computed(() => {
  return {
    layout:device.value==='mobile'?'vertical':instance.vkConfig.layout,
    primaryColor:instance.vkConfig.primaryColor,
    menuBgColor:instance.vkConfig.menuBgColor,
    headerBgColor:instance.vkConfig.headerBgColor,
    darkTheme:!instance.vkConfig.darkTheme,
  }
});
const handleClose = () => {
  store.dispatch("app/toggleSettings",false);
}
const layoutList = ref<Array<layoutType>>([
  {label:t('settings.settingLayoutVertical'),value:'vertical'},
  {label:t('settings.settingLayoutHorizontal'),value:'horizontal'},
  {label:t('settings.settingLayoutCommon'),value:'common'},
  {label:t('settings.settingLayoutComprehensive'),value:'comprehensive'},
  {label:t('settings.settingLayoutVerticalComprehensive'),value:'vertical__comprehensive'},
  {label:t('settings.settingLayoutColumn'),value:'column'},
  {label:t('settings.settingLayoutVerticalColumn'),value:'vertical__column'},
]);
const layoutChangeHandle = () => {
  instance.vkConfig = {
    layout:vkConfig.value.layout,
    primaryColor:instance.vkConfig.primaryColor,
    menuBgColor:instance.vkConfig.menuBgColor,
    headerBgColor:instance.vkConfig.headerBgColor,
    darkTheme:instance.vkConfig.darkTheme,
  }
}
const primaryThemeList = ref<Array<layoutType>>([
  {label:t('settings.settingThemeBlue'),value:primaryColorSource().blue},
  {label:t('settings.settingThemeGreen'),value:primaryColorSource().green},
  {label:t('settings.settingThemeRed'),value:primaryColorSource().red},
  {label:t('settings.settingThemePink'),value:primaryColorSource().pink},
  {label:t('settings.settingThemePurple'),value:primaryColorSource().purple},
  {label:t('settings.settingThemeCyan'),value:primaryColorSource().cyan},
  {label:t('settings.settingThemeOrange'),value:primaryColorSource().orange},
  {label:t('settings.settingThemeGold'),value:primaryColorSource().gold},
]);
const primaryThemeChangeHandle = () => {
  setThemeColor(vkConfig.value.primaryColor,vkConfig.value.menuBgColor,vkConfig.value.headerBgColor,vkConfig.value.darkTheme);
  instance.vkConfig = {
    layout:instance.vkConfig.layout,
    primaryColor:vkConfig.value.primaryColor,
    menuBgColor:instance.vkConfig.menuBgColor,
    headerBgColor:instance.vkConfig.headerBgColor,
    darkTheme:instance.vkConfig.darkTheme,
  }
}
onMounted(()=>{
  setThemeColor(vkConfig.value.primaryColor,vkConfig.value.menuBgColor,vkConfig.value.headerBgColor,vkConfig.value.darkTheme);
})
const menuBgList = ref<Array<layoutType>>([
  {label:'light',value:menuBgColorSource().light},
  {label:'dark',value:menuBgColorSource().dark},
  {label:"blue",value:menuBgColorSource().blue},
  {label:"green",value:menuBgColorSource().green},
  {label:"red",value:menuBgColorSource().red},
  {label:"pink",value:menuBgColorSource().pink},
  {label:"purple",value:menuBgColorSource().purple},
  {label:"cyan",value:menuBgColorSource().cyan},
  {label:"orange",value:menuBgColorSource().orange},
  {label:"gold",value:menuBgColorSource().gold},
  {label:'gradient_dark_sidebar1',value:menuBgColorSource().gradient_dark_sidebar1},
  {label:'gradient_dark_sidebar2',value:menuBgColorSource().gradient_dark_sidebar2},
  {label:'gradient_light_sidebar1',value:menuBgColorSource().gradient_light_sidebar1},
  {label:'bg_dark_sidebar1',value:menuBgColorSource().bg_dark_sidebar1},
  {label:'bg_light_sidebar1',value:menuBgColorSource().bg_light_sidebar1},
]);
const menuBgChangeHandle = () => {
  setThemeColor(vkConfig.value.primaryColor,vkConfig.value.menuBgColor,vkConfig.value.headerBgColor,vkConfig.value.darkTheme);
  instance.vkConfig = {
    layout:instance.vkConfig.layout,
    primaryColor:instance.vkConfig.primaryColor,
    menuBgColor:vkConfig.value.menuBgColor,
    headerBgColor:instance.vkConfig.headerBgColor,
    darkTheme:instance.vkConfig.darkTheme,
  }
}
const headerBgList = ref<Array<layoutType>>([
  {label:'light',value:headerBgColorSource().light},
  {label:'dark',value:headerBgColorSource().dark},
  {label:'blue',value:headerBgColorSource().blue},
  {label:'green',value:headerBgColorSource().green},
  {label:'red',value:headerBgColorSource().red},
  {label:'pink',value:headerBgColorSource().pink},
  {label:'purple',value:headerBgColorSource().purple},
  {label:'cyan',value:headerBgColorSource().cyan},
  {label:'orange',value:headerBgColorSource().orange},
  {label:'gold',value:headerBgColorSource().gold},
  {label:'gradient_dark_header1',value:headerBgColorSource().gradient_dark_header1},
  {label:'gradient_dark_header2',value:headerBgColorSource().gradient_dark_header2},
  {label:'gradient_light_header1',value:headerBgColorSource().gradient_light_header1},
  {label:'bg_dark_header1',value:headerBgColorSource().bg_dark_header1},
  {label:'bg_light_header1',value:headerBgColorSource().bg_light_header1},
]);
const headerBgChangeHandle = () => {
  setThemeColor(vkConfig.value.primaryColor,vkConfig.value.menuBgColor,vkConfig.value.headerBgColor,vkConfig.value.darkTheme);
  instance.vkConfig = {
    layout:instance.vkConfig.layout,
    primaryColor:instance.vkConfig.primaryColor,
    menuBgColor:instance.vkConfig.menuBgColor,
    headerBgColor:vkConfig.value.headerBgColor,
    darkTheme:instance.vkConfig.darkTheme,
  }
}
const skinThemeChangeHandle = () => {
  setThemeColor(vkConfig.value.primaryColor,vkConfig.value.menuBgColor,vkConfig.value.headerBgColor,vkConfig.value.darkTheme);
  instance.vkConfig = {
    layout:instance.vkConfig.layout,
    primaryColor:instance.vkConfig.primaryColor,
    menuBgColor:instance.vkConfig.menuBgColor,
    headerBgColor:instance.vkConfig.headerBgColor,
    darkTheme:!vkConfig.value.darkTheme,
  }
}
</script>

<template>
  <el-drawer
    custom-class="vk-drawer"
    v-model="showSettings" 
    direction="rtl"
    size="280px"
    :z-index="1500"
    :before-close="handleClose">
    <template #title>
      <span>{{$t("settings.settingPanelTitle")}}</span>
    </template>
    <template #default>
      <el-scrollbar>
        <el-form label-position="left">
          <el-divider>{{ $t('settings.settingModuleThemeTitle') }}</el-divider>
          <el-form-item :label="$t('settings.settingSkinTheme')">
            <el-switch
              v-model="vkConfig.darkTheme"
              class="vk-skin"
              inline-prompt
              :active-icon="sun"
              :inactive-icon="moon"
              @change="skinThemeChangeHandle" />
          </el-form-item>
          <el-form-item>
            <template #label>{{$t('settings.settingLayout')}}
              <el-tooltip effect="dark" placement="top">
                <template #content>{{$t('tooltips.tooltipLayout')}}</template>
                <span class="vk-icon"><svg-icon icon-class="ii:icon-question-circle"></svg-icon></span><!--小问号提示-->
              </el-tooltip>
            </template>
            <el-select v-model="vkConfig.layout" :disabled="device==='mobile'" @change="layoutChangeHandle">
              <el-option v-for="item in layoutList" :label="$t(item.label)" :value="item.value"></el-option>
            </el-select>
          </el-form-item>
          <el-form-item :label="$t('settings.settingSystemTheme')">
            <el-select v-model="vkConfig.primaryColor" @change="primaryThemeChangeHandle" popper-class="vk-primary-color">
              <el-option v-for="item in primaryThemeList" :label="$t(item.label)" :value="item.value">
                <span class="vk-icon" :style="{'background':item.value}"><svg-icon icon-class="ep:check"></svg-icon></span>{{$t(item.label)}}
              </el-option>
            </el-select>
          </el-form-item>
          <el-form-item :label="$t('settings.settingMenuTheme')">
            <el-select v-model="vkConfig.menuBgColor" @change="menuBgChangeHandle" :disabled="!vkConfig.darkTheme" popper-class="vk-menu-color" class="vk-menu-select">
              <template #prefix>
                <span :class="['vk-select-mark',vkConfig.menuBgColor]" :style="vkConfig.menuBgColor.indexOf('gradient')===-1&&vkConfig.menuBgColor.indexOf('bg')===-1?{'background':vkConfig.menuBgColor}:''"></span>
              </template>
              <el-option v-for="item in menuBgList" :label="item.label" :value="item.value">
                <span :class="['vk-icon',item.label]" :style="item.value.indexOf('gradient')===-1?{'background':item.value}:''"><svg-icon icon-class="ep:check"></svg-icon></span>
              </el-option>
            </el-select>
          </el-form-item>
          <el-form-item :label="$t('settings.settingHeaderTheme')">
            <el-select v-model="vkConfig.headerBgColor" @change="headerBgChangeHandle" :disabled="!vkConfig.darkTheme" popper-class="vk-header-color" class="vk-header-select">
              <template #prefix>
                <span :class="['vk-select-mark',vkConfig.headerBgColor]" :style="vkConfig.headerBgColor.indexOf('gradient')===-1&&vkConfig.headerBgColor.indexOf('bg')===-1?{'background':vkConfig.headerBgColor}:''"></span>
              </template>
              <el-option v-for="item in headerBgList" :label="item.label" :value="item.value">
                <span :class="['vk-icon',item.label]" :style="item.value.indexOf('gradient')===-1?{'background':item.value}:''"><svg-icon icon-class="ep:check"></svg-icon></span>
              </el-option>
            </el-select>
          </el-form-item>
        </el-form>
      </el-scrollbar>
    </template>
    <template #footer>
      <el-button type="warning" icon="Refresh">{{$t('buttons.buttonDefault')}}</el-button>
      <el-button type="danger" icon="Brush">{{$t('buttons.buttonClearCacheToLogin')}}</el-button>
    </template>
  </el-drawer>
</template>
<style lang="scss" scoped></style>
```
这里`element-plus`组件自身引入自定义`svg`图标，需要额外进行`import`，修复了在设置弹层关闭时，控制台关于`computed`的警告，以及为设置弹层直接设置`z-index`，解决弹层多次打开情况下，下拉框弹层层级小于设置弹层，导致下拉框弹层不可见的BUG。

4. 修改`src/layout/index.vue`文件：
```
const sidebarClass = computed(()=>{
    if(!instance.vkConfig.darkTheme){
        if(instance.vkConfig.menuBgColor.indexOf('_')!=-1){
            return instance.vkConfig.menuBgColor;
        }else{
            return "";
        }
    }else{
        return "";
    }
})


<div :class="['vk-layout-sidebar__wrap',sidebarClass]">
...

```
这里主要增加了侧边栏类名绑定，在侧边栏为渐变色背景或者背景图的情况下，通过绑定类名进行设置。

5. 修改`src/layout/components/headerPanel.vue`文件：
```
const headerClass = computed(()=>{
    if(!instance.vkConfig.darkTheme){
        if(instance.vkConfig.headerBgColor.indexOf('_')!=-1){
            return instance.vkConfig.headerBgColor;
        }else{
            return "";
        }
    }else{
        return "";
    }
})

<div :class="['vk-header',headerClass]">
...

```
这里是为顶栏绑定类名，在顶栏为渐变色背景或者背景图的情况下，通过绑定类名进行设置。

6. `src/style/element-plus.scss`文件：
```
:root{
  --el-border-radius-base:2px;
}
.el-tag{
  --el-tag-border-radius: var(--el-border-radius-base);
}
.el-notification{
  --el-notification-radius: var(--el-border-radius-base)!important;
}
.el-input__prefix,.el-input__suffix{
  align-items: center;
}
.el-message__closeBtn{
  position: absolute;
  color: var(--el-message-close-icon-color);
}
.el-overlay{
  background:rgba(0,0,0,.25);
}
.el-select-dropdown__item{
  padding: 0 20px;
}
.el-form{
  .el-form-item{margin-bottom: 0;}
  &.el-form--inline{
    .el-form-item+.el-form-item{
      margin-top: 0;
      margin-left: 0;
    }
    .el-form-item{
      margin-right:0;
    }
    .el-form-item__label{
      padding: 6px;
    }
  }
  &.el-form--label-top{
    margin-left: -6px;
    margin-right: -6px;
    .el-form-item{
      .el-form-item__label{
        padding: 6px;
        margin-bottom:0;
      }

    }
  }
  .el-form-item+.el-form-item{
    margin-top: 6px;
  }
  .el-form-item__label{
    padding: 6px 6px 6px 0;
  }
  .el-form-item__content{
    >.el-button,>.el-link,>.el-tag,>.el-check-tag,>.el-select,>.el-input,>.el-checkbox,>.el-checkbox-group >.el-checkbox,>.el-checkbox-group.checkbox-button,>.el-radio,>.el-radio-group >.el-radio,>.el-radio-group.radio-button,>.el-switch,>.el-textarea,>.el-cascader,>.el-color-picker,>.el-select-v2,>.el-input-number,>.el-date-editor,>.el-slider,>.el-rate,>.el-transfer,>.el-upload-wrap{
      margin: 6px;
    }
    >.el-form-item__error{
      left:6px;
      margin-top: -5px;
    }
    .el-select,.el-cascader,.el-select-v2{
        flex: 1;
    }
  }
  .el-link{
    line-height: 20px;
    >.el-icon{
      margin-right: 5px;
    }
  }
  .el-button-group{
    margin: 6px;
    .el-button{
      margin:0;
    }
  }
  .el-cascader__tags{
    z-index: 1;
  }
  .el-rate__item{
    display: inline-flex;
  }
}
.el-check-tag{
  font-weight: normal;
}
.el-alert--success.is-light{
  border: 1px solid var(--el-color-success);
}
.el-alert--warning.is-light{
  border: 1px solid var(--el-color-warning);
}
.el-alert--error.is-light{
  border: 1px solid var(--el-color-error);
}
.el-alert--info.is-light{
  border: 1px solid var(--el-color-info);
}
.el-alert{
  --el-alert-title-font-size: 14px;
}
.el-steps{
  .el-step__head.is-success{
    .el-step__line{
      background-color: var(--el-color-success);
    }
  }
}
.el-select--disabled{
  .el-input__prefix{
    cursor: not-allowed!important;
  }
}
.el-picker-panel .el-time-panel{
    background-color: var(--el-bg-color-overlay);
}
```
该页面内有部分样式修改，直接覆盖即可。

7. `src/style/public.scss`文件：
```
body {
  width: 100%;
  height: 100vh;
  margin: 0;
  padding: 0;
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  font-family: PingFang SC,Arial,Microsoft YaHei,sans-serif;
  background-color: var(--el-bg-color-page);
  color: var(--el-text-color-primary);
  font-size:var(--el-font-size-base);
  position: relative;
}
```
该文件中`body`的样式做了部分修改。

8. `src/style/layout/header.scss`文件：
```
// 顶部header样式
.vk-header {
  width: 100%;
  height: var(--el-header-height);
  overflow: hidden;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.12);
  display: flex;
  justify-content:space-around;
  align-items:center;
  position: relative;
  background-color: var(--el-header-bg-color);
  .vk-header-left{
    display: flex;
    align-items: center;
  }
  .vk-header-center{
    flex:1;
    align-items: center;
    height: 100%;
    min-width: 0;
    .el-menu{
      background-color: transparent;
    }
  }
  .vk-header-right {
    display: flex;
    min-width: 280px;
    height: var(--el-header-height);
    align-items: center;
    justify-content: flex-end;
    .vk-icon{
      font-size: 16px;
      padding: 0 10px;
    }
    .vk-dropdown-badge,.vk-full-screen,.vk-globalization,.vk-size,.vk-settings,.vk-dropdown-link {
      color: var(--el-button-text-color);
      position: relative;
      height: var(--el-header-height);
      cursor: pointer;
      display: flex;
      align-items: center;
      justify-content: space-around;
      .el-icon,img,p{
        position: relative;
        z-index: 2;
      }
      .el-badge__content{
        z-index: 2;
      }
    }
    .vk-dropdown-badge {
      padding: 0 20px;
    }
    // 用户信息样式
    .vk-dropdown-link {
      padding: 10px;
      p {
        margin-left: 10px;
      }
      img {
        width: 32px;
        height: 32px;
        border-radius: 50%;
      }
    }
  }
}
.vk-header-center{
    .el-menu{
      .vk-icon{
        margin-left:0;
      }
      .vk-menu-link .el-menu-item,.el-sub-menu .el-sub-menu__title{
        &:before{
          content:'';
          position: absolute;
          left: 50%;
          top: 50%;
          width: 0;
          height:0;
          transform: translate(-50%,-50%);
          transition: all .28s;
          background-color: var(--el-menu-active-color);
        }
      }
      .vk-menu-link{
        .el-menu-item{
          &.is-active,&:hover{
            background-color: transparent;
            &:before{
              width: 100%;
              height: 100%;
            }
          }
        }
      }
      .el-sub-menu{
        &.is-opened,&.is-active,&:hover{
          .el-sub-menu__title{
            background-color: transparent;
            &:before{
              width: 100%;
              height: 100%;
            }
          }
        }
        .el-sub-menu__title{
          border-bottom: none;
          padding-right: 20px;
          .el-sub-menu__icon-arrow{
            display: none;
          }
        }
      }
    }
}

// .vk-header-center{
//     .el-menu{
//         .vk-icon{
//             margin-left:0;
//         }
//         .vk-menu-link .el-menu-item,.el-sub-menu .el-sub-menu__title{
//             &:before{
//             content:'';
//             display: block;
//             position: absolute;
//             left:50%;
//             top:inherit;
//             bottom:0;
//             height:2px;
//             background-color: var(--el-menu-active-color);
//             transition: all .28s;
//             width:0;
//             transform: translate(-50%);
//             }
//         }
//         .vk-menu-link{
//             padding: 0 20px;
//             .el-menu-item{
//                 padding:0;
//                 &.is-active,&:hover{
//                     color: var(--el-menu-active-color);
//                     background-color: transparent;
//                     &:before{
//                         width: 100%;
//                     }
//                 }
//             }
//         }
//         .el-sub-menu{
//             padding: 0 20px;
//             &.is-opened,&.is-active,&:hover{
//                 .el-sub-menu__title{
//                     color: var(--el-menu-active-color);
//                     background-color: transparent;
//                     &:before{
//                         width: 100%;
//                     }
//                 }
//             }
//             .el-sub-menu__title{
//                 padding:0;
//                 border-bottom: none;
//                 .el-sub-menu__icon-arrow{
//                     display: none;
//                 }
//             }
//         }
//     }
// }
// 折叠按钮组件样式
.vk-hamburger-container {
  line-height: var(--el-fold-button-height);
  height: 100%;
  cursor: pointer;
  transition: background 0.3s;
  -webkit-tap-highlight-color: transparent;
  padding: 0 15px;
  position: relative;
  .el-icon{
    display: inline-block;
    vertical-align: middle;
    line-height:0;
    width: 20px;
    height: 20px;
    font-size: 20px;
    color: var(--el-button-text-color);
    position: relative;
    z-index: 2;
    .svg-icon{
      transform: rotate(180deg);
    }
    .is-active {
      transform: rotate(0);
    }
  }
}
// 面包屑组件样式
.vk-breadcrumb-container {
  flex:1;
  height: var(--el-breadcrumb-height);
  white-space: nowrap;
  line-height: var(--el-breadcrumb-height);
  position: relative;
  padding: 0 10px;
  .no-redirect {
    color: var(--el-breadcrumb-text-color);
    cursor: text;
  }
  .el-breadcrumb__item{
    float:none;
    display: inline-block;
  }
  .el-breadcrumb__separator{
    color: var(--el-breadcrumb-separator-color);
  }
  .el-breadcrumb__inner a, .el-breadcrumb__inner.is-link{
    color: var(--el-breadcrumb-link-color);
    font-weight: normal;
    &:hover{
      color: var(--el-breadcrumb-link-hover-color);
    }
  }
}

// 退出登录
.vk-logout {
  max-width: 120px;
  .el-dropdown-menu__item {
    .vk-icon{
      width: 22px;
      text-align: center;
      height: 22px;
      vertical-align: middle;
      margin-right: 5px;
      font-size: 18px;
      .el-icon{
        margin:2px;
      }
    }
    .vk-text{
      flex: 1;
    }
  }
  .el-dropdown-menu__item:focus,
  .el-dropdown-menu__item:not(.is-disabled):hover {
    color: var(--el-color-primary);
    background: var(--el-color-primary-light-9);
  }
}

// 消息提醒通知样式
.vk-dropdown-tabs {
  width: 300px;
  border-radius: 4px;
  .el-tabs__header {
    margin: 0;
    .el-tabs__item{
      color: var(--el-text-color-regular);
      &.is-active{
        color: var(--el-color-primary);
      }
    }
  }
  .el-tabs__nav-scroll {
    display: flex;
    justify-content: center;
  }
  .el-tabs__nav-wrap::after {
    height: 1px;
  }
  .vk-notice-list__container {
    padding: 15px 24px 0 24px;
    .vk-item-title__popper {
      max-width: 238px;
    }
    .vk-item-notice {
      display: flex;
      align-items: flex-start;
      justify-content: space-between;
      padding: 12px 0;
      border-bottom: 1px solid var(--el-border-color-extra-light);
      .vk-item-avatar {
        margin-right: 16px;
        background: var(--el-bg-color-overlay);
      }
      .vk-item-text {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        flex: 1;
        width: 206px;
        .vk-item-title {
          display: flex;
          margin-bottom: 8px;
          font-weight: 400;
          font-size: 14px;
          line-height: 1.5715;
          color: var(--el-text-color-primary);
          cursor: pointer;
          .vk-item-title__content {
            flex: 1;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
          }
          .vk-item-title__extra {
            float: right;
            margin-top: -1.5px;
            font-weight: 400;
          }
        }
        .vk-item-text__description,
        .vk-item-text__datetime {
          font-size: 12px;
          line-height: 1.5715;
          color: var(--el-text-color-secondary);
        }
        .vk-item-text__description {
          display: -webkit-box;
          text-overflow: ellipsis;
          overflow: hidden;
          -webkit-line-clamp: 2;
          -webkit-box-orient: vertical;
        }
        .vk-item-text__datetime {
          margin-top: 4px;
        }
      }
    }
  }
}

// 国际化样式
.vk-translation{
  .el-dropdown-menu__item{
    padding: 0;
    .vk-item-translation{
      padding: 5px 40px;
      position: relative;
      display: flex;
      width: 100%;
      align-items: center;
      color: var(--el-text-color-regular);
      &.is-active{
        color: var(--el-color-primary)!important;
      }
    }
  }
  .vk-check{
    position: absolute;
    left: 20px;
  }
}
// 组件尺寸样式
.vk-size-list{
  .el-dropdown-menu__item{
    padding: 0;
    .vk-item-size{
      padding: 5px 30px;
      position: relative;
      display: flex;
      width: 100%;
      align-items: center;
      color: var(--el-text-color-regular);
      &.is-active{
        color: var(--el-color-primary)!important;
      }
    }
  }
  .el-dropdown-menu__item:focus,
  .el-dropdown-menu__item:not(.is-disabled):hover {
    color: var(--el-color-primary);
    background: var(--el-color-primary-light-9);
  }
  .vk-check{
    position: absolute;
    left: 10px;
  }
}
```
改文件删除了部分样式，修改了部分`css`变量的引用。

9. `src/style/layout/index.scss`文件：
```
@import './layout.scss';
.vue-kevin-admin{
  position: relative;
  width: 100%;
  height: 100%;
  >[class*='vk-layout-']{
    position: relative;
    transition: all .28s;
    height: 100%;
    &.is-header-fixed{
      .vk-layout-main{
        padding-top: calc(var(--el-header-height) + var(--el-tag-height));
        .vk-layout-header{
          position: fixed;
          top:0;
          right:0;
        }
      }
    }
    &.is-sidebar-fixed{
      .vk-layout-sidebar{
        position: fixed;
        bottom: 0;
        z-index: 1020;
        top: 0;
      }
    }
  }
  .vk-mask {
    background: #000;
    opacity: 0.25;
    width: 100%;
    top: 0;
    height: 100%;
    position: absolute;
    z-index: 1010;
  }
  .vk-layout-sidebar {
    transition: all 0.28s;
    font-size: 0;
    overflow: hidden;
    box-shadow: 0 4px 4px rgba(0, 0, 0, .12);
    position: relative;
    .vk-layout-sidebar__wrap{
      width: 100%;
      height: 100%;
      position: relative;
      transition: all 0.28s;
      z-index: 3;
      background-color: var(--el-sidebar-bg-color);
      box-shadow: 0 4px 4px rgba(0, 0, 0, .12);
      .vk-collapse-button{
        position: absolute;
        left:0;
        bottom: 0;
        width: 100%;
        height: var(--el-sidebar-fold-button-height);
      }
    }
    .vk-layout-sub__menu{
      transition: all 0.28s;
      z-index: 2;
      background-color: var(--el-submenu-bg-color);
    }
    .vk-menu-scroll{
      position: absolute;
      left:0;
      top: var(--el-header-height);
      bottom:0;
      width: 100%;
      .el-scrollbar{
        .el-scrollbar__wrap {
          overflow-x: hidden !important;
        }
      }
    }
  }
  .vk-layout-main {
    height: 100%;
    position: relative;
    transition: all 0.28s;
    .vk-layout-header{
      position: relative;
      box-shadow: 0 1px 4px rgba(0, 0, 0, 0.12);
      transition: all 0.28s;
      z-index: 1002;
    }
    .vk-layout-content{
      height: 100%;
      transition: all 0.28s;
      position: relative;
      .vk-layout-view{
        position: relative;
        min-height: calc(100vh - var(--el-header-height) - var(--el-tag-height));
      }
    }
  }
  .vk-layout-column{
    .vk-layout-sidebar__wrap{
      .vk-menu-scroll{
        top:0;
      }
    }
  }
  .vk-layout-common,.vk-layout-comprehensive,.vk-layout-column{
      .vk-layout-sidebar__wrap{
        &:before{
          content:'';
          display: block;
          width: 100%;
          height: 1px;
          background-color: var(--el-border-color-extra-light);
          position: absolute;
          left:0;
          top:0;
        }
      }
  }
  .vk-layout-column,.vk-layout-vertical__column{
    .vk-layout-sidebar__wrap{
      .vk-menu-scroll{
        bottom: var(--el-sidebar-fold-button-height);
      }
      .vk-collapse-button{
        position: absolute;
        left:0;
        bottom: 0;
        width: 100%;
        height: var(--el-sidebar-fold-button-height);
        box-shadow: -2px 0 4px rgba(0, 0, 0, .12);
        .vk-hamburger-container{
          line-height: var(--el-sidebar-fold-button-height);
          text-align: center;
          background-color: var(--el-sidebar-fold-button-bg-color);
          .el-icon{
              color:  var(--el-sidebar-fold-button-text-color);
              transition: all .28s;
          }
          &:hover{
              .el-icon{
                  color: var(--el-color-primary);
              }
          }
        }
      }
    }
    .vk-layout-sub__menu{
        &:before{
          content:'';
          display: block;
          width: 100%;
          height: 1px;
          background-color: var(--el-border-color-extra-light);
          position: absolute;
          left:0;
          top:0;
        }
    }
  }
  &.mobile{
    >[class*='vk-layout-']{
      &.is-sidebar-fixed{
        .vk-layout-sidebar{
          left:0;
          width: var(--el-sidebar-width);
          &.is-collapse{
            width: var(--el-sidebar-width);
            left: calc(0px - var(--el-sidebar-width));
          }
          .vk-router-menu{
            .vk-menu-link{
              width: 100%;
            }
          }
        }
        .vk-layout-main{
          padding-left:0;
          .vk-layout-header{
            left:0;
          }
        }
      }
    }
  }
}

// logo 样式
.vk-logo {
  position: relative;
  width: 100%;
  height: var(--el-header-height);
  overflow: hidden;
  .vk-logo-link {
    display: inline-flex;
    width: 100%;
    height: 100%;
    text-align: left;
    padding-left: 8px;
    align-items: center;
    position: relative;
    .vk-logo-img{
      width: 32px;
      height: 32px;
      display: inline-block;
      margin: 0 8px;
      vertical-align: top;
      fill: currentColor;
      position: relative;
      z-index: 2;
      transition: all .28s;
      color: var(--el-logo-img-color);
    }
    .vk-logo-title {
      display: inline-block;
      margin: 0;
      font-size: 18px;
      line-height: 24px;
      position: relative;
      z-index: 2;
      transition: all .56s;
      flex: 1;
      color: var(--el-logo-title-color);
    }
  }
}

.vk-logo .vk-logo-link,.vk-hamburger-container,.vk-header .vk-header-right .vk-dropdown-badge,.vk-full-screen,.vk-globalization,.vk-size,.vk-settings,.vk-dropdown-link{
  position: relative;
  &:before{
    content:'';
    position: absolute;
    left: 50%;
    top: 50%;
    width: 0;
    height:0;
    transform: translate(-50%,-50%);
    transition: all .28s;
    background-color: var(--el-button-hover-bg-color);
  }
  &:hover{
    &:before{
      width: 100%;
      height: 100%;
    }
    color: var(--el-button-hover-text-color);
    .el-icon{
        color: var(--el-button-hover-text-color);
    }
  }
}

@import './header.scss';
@import './tag.scss';
@import './routerMenu.scss';
@import './setting.scss';
@import './types/vertical.scss';
@import './types/horizontal.scss';
@import './types/common.scss';
@import './types/comprehensive.scss';
@import './types/verticalComprehensive.scss';
@import './types/column.scss';
@import './types/verticalColumn.scss';
```
该页面删除了部分样式，增加了部分样式，修改了部分`css`变量的引用。

10. `src/style/layout/routerMenu.scss`文件：
```
.el-menu{
  // 无子级（hover）
  .el-menu-item{
    &:hover{
      color: var(--el-menu-hover-text-color);
    }
  }
  // 有子级（hover）
  .el-sub-menu{
    >.el-sub-menu__title{
      &:hover{
        color: var(--el-submenu-hover-text-color);
        background-color: var(--el-submenu-hover-bg-color);
      }
    }
    &:hover{
      >.el-sub-menu__title{
        color: var(--el-submenu-hover-text-color);
      }
    }
  }
  //  无子级（active）
  .el-menu-item.is-active{
    color: var(--el-menu-active-text-color);
    background-color: var(--el-menu-active-color);
  }
  // 有子级（active）
  .el-sub-menu.is-active{
    >.el-sub-menu__title{
      color: var(--el-submenu-active-text-color);
      &:hover{
        color: var(--el-submenu-active-text-color);
        background-color: var(--el-submenu-hover-bg-color);
      }
    }
  }
  // 菜单折叠后样式
  &.el-menu--collapse {
    .el-sub-menu{
      &.is-opened{
        >.el-sub-menu__title{
          color: var(--el-submenu-hover-text-color);
          background-color: var(-el-submenu-hover-bg-color);
        }
      }
      &.is-active{
        >.el-sub-menu__title{
          color: var(--el-menu-active-text-color);
          background-color: var(--el-submenu-active-bg-color);
        }
      }
    }
  }
}
.vk-router-menu{
  border: none;
  height: 100%;
  // 菜单图标
  .vk-icon{
    display: inline-block;
    width: 18px;
    height: 18px;
    line-height:0;
    font-size:0;
    vertical-align: middle;
    margin-right: 10px;
    margin-left: 3px;
    text-align: center;
    svg{
      width: 1em !important;
      height: 1em !important;
      font-size: 18px !important;
      text-align: center;
      vertical-align: middle;
      line-height: 0;
      display: inline-block;
    }
  }
  // 设置一级菜单的高度、行高
  .el-menu-item,>.el-sub-menu>.el-sub-menu__title{
    height: var(--el-menu-item-height);
    line-height: var(--el-menu-item-height);
  }
  // 设置其他层级菜单的高度、行高
  >.el-sub-menu .el-menu{
    .el-menu-item,.el-sub-menu__title{
      height: var(--el-submenu-item-height);
      line-height: var(--el-submenu-item-height);
    }
  }
  // 菜单链接
  .vk-menu-link{
    display: inline-block;
  }
  .el-sub-menu__title{
    padding-right: 44px;
  }
  // 菜单文字样式
  .vk-menu-title{
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: space-between;
    overflow: hidden;
    >span{
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
  // 菜单折叠后样式
  &.el-menu--collapse {
    .el-menu-item{
      .icon-font{
        margin:0;
        width: 24px;
      }
    }
    .el-sub-menu__title{
      padding-right: 20px;
    }
    .el-tooltip__trigger{
      display: flex!important;
      align-items: center!important;
    }
  }
}
// 横向布局时次级菜单样式控制、非横向布局侧边导航折叠时子菜单样式 
.el-popper{
  .el-menu--horizontal,.el-menu--vertical{
    border:none;
    .el-menu--popup{
      background-color: var(--el-menu-bg-color);
      // 菜单样式（default）
      .el-menu-item,.el-sub-menu__title {
        font-size: var(--el-menu-font-size);
        height: var(--el-menu-item-height);
        line-height: var(--el-menu-item-height);
      }
      // 菜单样式（default）
      .el-sub-menu__title {
        padding-right: 44px!important;
      }
      .el-menu-item.is-active{
        color: var(--el-menu-active-text-color);
      }
    }
  }
}
```
改文件删除了部分样式。

11. `src/style/layout/tag.scss`文件：
```
// 标签页样式
.vk-tag-panel {
  width: 100%;
  display: flex;
  align-items: center;
  color: var(--el-text-color-regular);
  background: var(--el-bg-color-overlay);
  position: relative;
  border-top: 1px solid var(--el-border-color-extra-light);
  transition: all 0.28s;
  .vk-hamburger-container{
    &:before{
      display: none;
    }
    &:hover{
      .el-icon{
        color: var(--el-color-primary);
      }
    }
  }
  >.el-icon{
    width: 40px;
    height: var(--el-tag-height);
    line-height: var(--el-tag-height);
    text-align: center;
    font-size: 16px;
    color: var(--el-text-color-regular);
    &:hover{
      color: var(--el-color-primary);
    }
    &.vk-btn-left{
      box-shadow: 5px 0 5px -6px rgba(0,0,0,.3);
      &:hover {
        cursor: w-resize;
      }
    }
    &.vk-btn-right{
      box-shadow: -5px 0 5px -6px rgba(0,0,0,.3);
      &:hover {
        cursor: e-resize;
      }
    }
  }
  .vk-tag-scroll__container {
    flex: 1;
    overflow: hidden;
    white-space: nowrap;
    position: relative;
    .vk-tag-wrap {
      position: relative;
      float: left;
      list-style: none;
      overflow: visible;
      white-space: nowrap;
      transition: transform 0.5s ease-in-out;
      .vk-item-tag {
        padding: 0 15px;
        position: relative;
        height: var(--el-tag-height);
        display: inline-block;
        line-height: var(--el-tag-height);
        border-right: 1px solid var(--el-border-color-extra-light);
        cursor: pointer;
        transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
        .vk-tag-link {
          color: var(--el-text-color-regular);
          padding: 0 4px 0 4px;
        }
        >.el-icon{
          width: 16px;
          height: 16px;
          font-size: 14px;
          cursor: pointer;
          position: absolute;
          top: 50%;
          transform: translate(0, -50%);
          border-radius: 50%;
          transition: all 0.28s;
          margin-left: 2px;
          &:hover {
            border-radius: 2px;
            color: var(--el-color-primary);
            background: var(--el-border-color-lighter);
          }
        }
        &.is-active {
          position: relative;
          background-color: var(--el-bg-color-page);
          &:not(:first-child) {
            padding-right: 32px;
          }
          >.el-icon {
            transform: translate(0, -50%);
          }
          .vk-tag-link {
            color: var(--el-color-primary);
          }
        }
        &.card-in {
          color: var(--el-color-primary);
          .vk-tag-link {
            color: var(--el-color-primary);
          }
        }
        &.card-out {
          color: var(--el-text-color-regular);
          .vk-tag-link {
            color: var(--el-text-color-regular);
          }
        }
        &:not(:first-child) {
          &:hover {
            padding-right: 32px;
            &:not(.is-active) {
              >.el-icon {
                animation: close 200ms ease-in forwards;
              }
            }
          }
        }
      }
    }
  }
  .vk-right-button {
    display: flex;
    font-size: 16px;
    border-left: 1px solid var(--el-border-color-extra-light);
    li {
      display: flex;
      align-items: center;
      justify-content: center;
      border-right: 1px solid var(--el-border-color-extra-light);
      cursor: pointer;
      .el-icon{
        width: 40px;
        height: var(--el-tag-height);
        &:hover{
          color: var(--el-color-primary);
        }
      }
    }
  }
  /* 右键菜单 */
  .vk-contextmenu {
    margin: 0;
    background: var(--el-bg-color-overlay);
    position: absolute;
    list-style-type: none;
    padding: 5px 0;
    border-radius: 4px;
    color: var(--el-text-color-regular);
    font-weight: normal;
    font-size: 13px;
    white-space: nowrap;
    outline: 0;
    box-shadow: 0 2px 8px rgba(0, 0, 0, .15);
    li {
      width: 100%;
      margin: 0;
      padding: 7px 12px;
      cursor: pointer;
      display: flex;
      align-items: center;
      &:hover {
        background: var(--el-color-primary-light-9);
        color: var(--el-color-primary);
      }
      .el-icon{
        width: 14px;
        height: 14px;
        margin-right: 0.5em;
      }
    }
  }
}

/* 标签栏刷新按钮动画效果 */
.vk-refresh-rotate {
  -webkit-animation: rotate 600ms linear infinite;
  -moz-animation: rotate 600ms linear infinite;
  -o-animation: rotate 600ms linear infinite;
  animation: rotate 600ms linear infinite;
}
@keyframes rotate {
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
}
// 标签栏关闭按钮动画效果
@keyframes close {
  from {
    transform: translate(-50%, -50%);
  }
  to {
    transform: translate(0, -50%);
  }
}
```
该页面主要修改了部分`css`变量的引用。

以上样式部分的修改，让所有主题类的样式全部依托于`css`变量完成。主题的`css`变量也进行了重新的定义。

12. 删除`src/style/theme/darkLight.scss`样式文件，新增`src/style/theme/header.scss`文件：
```
html{
    // 顶栏深色渐变背景色
    .gradient_dark_header1{
        background: linear-gradient(to right,#7b4391,#dc2430 40%);
    }
    // 顶栏深色渐变背景色
    .gradient_dark_header2{
        background: linear-gradient(to bottom,#b6cd85,#8aa651);
    }
    // 顶栏浅色渐变背景色
    .gradient_light_header1{
        background: linear-gradient(45deg,#fcedbe,#fefefe,#fcedbe);
    }
    
    // 顶栏深色背景图
    .bg_dark_header1{
        background-image: url(@/assets/header_dark_bg1.gif);
        background-size: cover;
        background-position: center bottom;
    }
    // 顶栏浅色背景图
    .bg_light_header1{
        background-image: url(@/assets/header_light_bg1.gif);
        background-size: cover;
        background-position: center bottom;
    }
    &.light{
        body{
            // 顶栏白背景时样式控制
            &[data-header-theme*='light']{
                .vk-layout-header{
                    --el-button-hover-bg-color:rgba(0, 0, 0, 0.05); // 顶栏、侧边栏按钮鼠标经过背景色
                    --el-button-text-color: var(--el-text-color-primary); // 顶栏、侧边栏按钮文本色
                    --el-button-hover-text-color: var(--el-text-color-primary); // 顶栏、侧边栏按钮鼠标经过文本色
                    --el-breadcrumb-text-color: var(--el-text-color-secondary); // 面包屑文本色
                    --el-breadcrumb-link-color: var(--el-text-color-primary); // 面包屑链接文本色
                    --el-breadcrumb-link-hover-color: var(--el-color-primary); // 面包屑链接鼠标经过文本色
                    --el-breadcrumb-separator-color: var(--el-text-color-placeholder); // 面包屑分隔符文本色
                    // 主菜单变量控制
                    .vk-header-center{
                        .el-menu{
                            --el-menu-text-color: var(--el-text-color-regular); // 无子级菜单的默认文本色
                            --el-menu-hover-text-color: var(--el-color-primary); // 无子级菜单的hover文本色
                            --el-menu-active-text-color: var(--el-color-primary); // 无子级菜单的active文本色
                            --el-submenu-text-color: var(--el-text-color-regular); // 有子级菜单的默认文本色
                            --el-submenu-hover-text-color: var(--el-color-primary); // 有子级菜单的hover文本色
                            --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                            --el-menu-bg-color: transparent; // 默认菜单背景色
                            --el-menu-hover-bg-color: rgba(0, 0, 0, 0.05); // 无子级菜单hover背景色
                            --el-menu-active-color: rgba(0, 0, 0, 0.05); // 无子级菜单active背景色
                            --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                            --el-submenu-active-bg-color: rgba(0, 0, 0, 0.05); // 有子级active背景色
                        }
                    }
                }
                // 折叠时，次级菜单变量控制
                .el-popper{
                    .el-menu--horizontal{
                        .el-menu{
                            --el-menu-text-color: var(--el-text-color-regular); // 无子级菜单的默认文本色
                            --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                            --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                            --el-submenu-text-color: var(--el-text-color-regular); // 有子级菜单的默认文本色
                            --el-submenu-hover-text-color: var(--el-text-color-primary); // 有子级菜单的hover文本色
                            --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                            --el-menu-bg-color: transparent; // 默认菜单背景色
                            --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                            --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                            --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                            --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                        }
                    }
                }
            }
            // 顶栏非白背景时样式控制
            &:not([data-header-theme*='light']){
                .vk-layout-header{
                    --el-button-hover-bg-color:rgba(255, 255, 255, 0.1); // 顶栏、侧边栏按钮鼠标经过背景色
                    --el-button-text-color: rgba(255, 255, 255, 0.9); // 顶栏、侧边栏按钮文本色
                    --el-button-hover-text-color: rgba(255, 255, 255, 0.9); // 顶栏、侧边栏按钮鼠标经过文本色
                    --el-breadcrumb-text-color: rgba(255, 255, 255, 0.6); // 面包屑文本色
                    --el-breadcrumb-link-color: rgba(255, 255, 255, 0.8); // 面包屑链接文本色
                    --el-breadcrumb-link-hover-color: var(--el-color-white); // 面包屑链接鼠标经过文本色
                    --el-breadcrumb-separator-color: rgba(255, 255, 255, 0.6); // 面包屑分隔符文本色
                    // 主菜单变量控制
                    .vk-header-center{
                        .el-menu{
                            --el-menu-text-color: rgba(255, 255, 255, 0.8); // 无子级菜单的默认文本色
                            --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                            --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                            --el-submenu-text-color: rgba(255, 255, 255, 0.8); // 有子级菜单的默认文本色
                            --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                            --el-submenu-active-text-color: var(--el-color-white); // 有子级菜单的active文本色
                            --el-menu-bg-color: var(--el-submenu-bg-color); // 默认菜单背景色
                            --el-menu-hover-bg-color: rgba(255, 255, 255, 0.1);  // 无子级菜单hover背景色
                            --el-menu-active-color:rgba(255, 255, 255, 0.2);  // 无子级菜单active背景色
                            --el-submenu-hover-bg-color: rgba(255, 255, 255, 0.1); // 有子级hover背景色
                            --el-submenu-active-bg-color: rgba(255, 255, 255, 0.2); // 有子级active背景色
                        }
                    }
                    // 分栏、综合、常规模式下标签栏的折叠按钮颜色
                    .vk-tag-panel{
                        --el-button-text-color: var(--el-text-color-regular); // 顶栏、侧边栏按钮文本色
                        --el-button-hover-text-color: var(--el-text-color-regular); // 顶栏、侧边栏按钮鼠标经过文本色
                    }
                }
                // 折叠时，次级菜单变量控制
                .el-popper{
                    .el-menu--horizontal{
                        .el-menu{
                            --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                            --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                            --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                            --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                            --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                            --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                            --el-menu-bg-color: #0d1014; // 默认菜单背景色
                            --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                            --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                            --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                            --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                        }
                    }
                }
            }
        }
    }
    &.dark{
        body{
            .vk-layout-header{
                --el-button-hover-bg-color:rgba(255, 255, 255, 0.1); // 顶栏、侧边栏按钮鼠标经过背景色
                --el-button-text-color: var(--el-text-color-primary); // 顶栏、侧边栏按钮文本色
                --el-button-hover-text-color: var(--el-text-color-primary); // 顶栏、侧边栏按钮鼠标经过文本色
                --el-breadcrumb-text-color: var(--el-text-color-secondary); // 面包屑文本色
                --el-breadcrumb-link-color: var(--el-text-color-primary); // 面包屑链接文本色
                --el-breadcrumb-link-hover-color: var(--el-color-primary); // 面包屑链接鼠标经过文本色
                --el-breadcrumb-separator-color: var(--el-text-color-disabled); // 面包屑分隔符文本色
                // 主菜单变量控制
                .vk-header-center{
                    .el-menu{
                        --el-menu-text-color: rgba(255, 255, 255, 0.8); // 无子级菜单的默认文本色
                        --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                        --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                        --el-submenu-text-color: rgba(255, 255, 255, 0.8); // 有子级菜单的默认文本色
                        --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                        --el-submenu-active-text-color: var(--el-color-white); // 有子级菜单的active文本色
                        --el-menu-bg-color: var(--el-submenu-bg-color); // 默认菜单背景色
                        --el-menu-hover-bg-color: rgba(255, 255, 255, 0.1);  // 无子级菜单hover背景色
                        --el-menu-active-color:rgba(255, 255, 255, 0.1);  // 无子级菜单active背景色
                        --el-submenu-hover-bg-color: rgba(255, 255, 255, 0.1); // 有子级hover背景色
                        --el-submenu-active-bg-color: rgba(255, 255, 255, 0.1); // 有子级active背景色
                    }
                }
            }
            // 折叠时，次级菜单变量控制
            .el-popper{
                .el-menu--horizontal{
                    .el-menu{
                        --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                        --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                        --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                        --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                        --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                        --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                        --el-menu-bg-color: var(--el-submenu-bg-color); // 默认菜单背景色
                        --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                        --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                        --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                        --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                    }
                }
            }
        }
    }
}
```
改文件主要控制顶栏的主题色配置。

13. 修改`src/style/theme/sidebar.scss`文件：
```
html{
    // 侧边栏深色渐变色
    .gradient_dark_sidebar1{
        background: linear-gradient(to right,#524e4d,#2e2e2e);
    }
    // 侧边栏深色渐变色
    .gradient_dark_sidebar2{
        background: linear-gradient(to bottom,#24253c,#2f2a62,#0f0c2d);
    }
    // 侧边栏浅色渐变色
    .gradient_light_sidebar1{
        background: linear-gradient(90deg,#e7e6eb,#cdc9e1);
    }
    // 侧边栏深色背景图
    .bg_dark_sidebar1{
        background-image: url(@/assets/sidebar_dark_bg1.gif);
        background-size: cover;
        background-position: center bottom;
    }
    // 侧边栏浅色背景图
    .bg_light_sidebar1{
        background-image: url(@/assets/sidebar_light_bg1.gif);
        background-size: cover;
        background-position: center center;
    }
    &.light{
        body{
            // 菜单白背景时变量控制
            &[data-menu-theme*='light']{
                .vk-layout-sidebar__wrap{
                    --el-button-hover-bg-color:rgba(0, 0, 0, 0.05);  // logo、底部折叠按钮鼠标经过背景色
                    .el-menu{
                        --el-menu-text-color: var(--el-text-color-regular); // 无子级菜单的默认文本色
                        --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                        --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                        --el-submenu-text-color: var(--el-text-color-regular); // 有子级菜单的默认文本色
                        --el-submenu-hover-text-color: var(--el-text-color-primary); // 有子级菜单的hover文本色
                        --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                        --el-menu-bg-color: transparent; // 默认菜单背景色
                        --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                        --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                        --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                        --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                        // 子菜单
                        .el-menu{
                            --el-menu-bg-color: var(--el-color-white); // 子级默认背景色
                        }
                    }
                }
                // 次级菜单变量控制
                .el-popper{
                    .el-menu--vertical{
                        .el-menu{
                            --el-menu-text-color: var(--el-text-color-regular); // 无子级菜单的默认文本色
                            --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                            --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                            --el-submenu-text-color: var(--el-text-color-regular); // 有子级菜单的默认文本色
                            --el-submenu-hover-text-color: var(--el-text-color-primary); // 有子级菜单的hover文本色
                            --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                            --el-menu-bg-color: transparent; // 默认菜单背景色
                            --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                            --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                            --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                            --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                        }
                    } 
                } 
            }
            // 菜单非白背景时变量控制
            &:not([data-menu-theme*='light']){
                // 主菜单变量控制
                .vk-layout-sidebar__wrap{
                    --el-button-hover-bg-color:rgba(255, 255, 255, 0.1); // logo、底部折叠按钮鼠标经过背景色
                    --el-border-color-extra-light: transparent; // 侧边栏非白背景时上边框色
                    .el-menu{
                        --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                        --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                        --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                        --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                        --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                        --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                        --el-menu-bg-color: transparent; // 默认菜单背景色
                        --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                        --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                        --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                        --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                        // 次级菜单
                        .el-menu{
                            --el-menu-bg-color: var(--el-submenu-bg-color); // 子级默认背景色
                        }
                    }
                }
                // 次级菜单变量控制
                .el-popper{
                    .el-menu--vertical{
                        .el-menu{
                            --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                            --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                            --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                            --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                            --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                            --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                            --el-menu-bg-color: var(--el-submenu-bg-color); // 默认菜单背景色
                            --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                            --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                            --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                            --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                        }
                    }
                }
            }
            // 分栏模式次级菜单变量控制
            .vk-layout-sub__menu{
                --el-submenu-bg-color: #ffffff;
                .el-menu{
                    --el-menu-text-color: var(--el-text-color-regular); // 无子级菜单的默认文本色
                    --el-menu-hover-text-color: var(--el-color-primary); // 无子级菜单的hover文本色
                    --el-menu-active-text-color: var(--el-color-primary); // 无子级菜单的active文本色
                    --el-submenu-text-color: var(--el-text-color-regular); // 有子级菜单的默认文本色
                    --el-submenu-hover-text-color: var(--el-text-color-primary); // 有子级菜单的hover文本色
                    --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                    --el-menu-bg-color: transparent; // 默认菜单背景色
                    --el-menu-hover-bg-color: var(--el-color-primary-light-9); // 无子级菜单hover背景色
                    --el-menu-active-color:var(--el-color-primary-light-9); // 无子级菜单active背景色
                    --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                    --el-submenu-active-bg-color: transparent; // 有子级active背景色
                }
            }
            // 渐变色、背景图时次级菜单背景色变量控制
            &[data-menu-theme*='gradient_'],&[data-menu-theme*='bg_']{
                .vk-layout-sidebar__wrap{
                    .el-menu{
                        .el-menu{
                            --el-menu-bg-color: transparent !important; // 子级默认背景色
                        }
                    }
                }
            }
        }
    }
    &.dark{
        body{
            // 主菜单变量控制
            .vk-layout-sidebar__wrap{
                --el-button-hover-bg-color:rgba(255, 255, 255, 0.1); // logo、底部折叠按钮鼠标经过背景色
                .el-menu{
                    --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                    --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                    --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                    --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                    --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                    --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                    --el-menu-bg-color: transparent; // 默认菜单背景色
                    --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                    --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                    --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                    --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                    // 次级菜单
                    .el-menu{
                        --el-menu-bg-color: var(--el-submenu-bg-color); // 子级默认背景色
                    }
                }
            }
            // 分栏模式变量控制
            .vk-layout-sub__menu{
                .el-divider--horizontal{
                    .el-divider__text{
                        --el-bg-color: var(--el-submenu-bg-color); // 分栏模式分割线背景色
                    }
                }
                .el-menu{
                    --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                    --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                    --el-menu-active-text-color: var(--el-color-primary); // 无子级菜单的active文本色
                    --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                    --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                    --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                    --el-menu-bg-color: transparent; // 默认菜单背景色
                    --el-menu-hover-bg-color: rgba(255,255,255,.1); // 无子级菜单hover背景色
                    --el-menu-active-color: rgba(255,255,255,.1); // 无子级菜单active背景色
                    --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                    --el-submenu-active-bg-color: transparent; // 有子级active背景色
                }
            }
            // 次级菜单变量控制
            .el-popper{
                .el-menu--vertical{
                    .el-menu{
                        --el-menu-text-color: rgba(254, 254, 254, 0.65); // 无子级菜单的默认文本色
                        --el-menu-hover-text-color: var(--el-color-white); // 无子级菜单的hover文本色
                        --el-menu-active-text-color: var(--el-color-white); // 无子级菜单的active文本色
                        --el-submenu-text-color: rgba(254, 254, 254, 0.65); // 有子级菜单的默认文本色
                        --el-submenu-hover-text-color: var(--el-color-white); // 有子级菜单的hover文本色
                        --el-submenu-active-text-color: var(--el-color-primary); // 有子级菜单的active文本色
                        --el-menu-bg-color: var(--el-submenu-bg-color); // 默认菜单背景色
                        --el-menu-hover-bg-color: var(--el-color-primary); // 无子级菜单hover背景色
                        --el-menu-active-color:var(--el-color-primary); // 无子级菜单active背景色
                        --el-submenu-hover-bg-color: transparent; // 有子级hover背景色
                        --el-submenu-active-bg-color: var(--el-color-primary); // 有子级active背景色
                    }
                }
            } 
        }
    }
}
```
该页面主要控制侧边栏的主题色配置。

14. 新增`src/style/theme/public.scss`文件：
```
.vk-skin{
    --el-switch-off-color:var(--el-color-primary); // 项目配置明暗主题选中背景色
}

html{
    &.light{
        body{
            // 顶栏白背景时样式控制
            &[data-header-theme*='light']{
                .vk-header{
                    --el-logo-img-color:var(--el-color-primary); // logo图片文本色
                    --el-logo-title-color:var(--el-color-primary); // logo文字文本色
                }
            }
            // 顶栏非白背景时样式控制
            &:not([data-header-theme*='light']){
                .vk-header{
                    --el-logo-img-color:var(--el-color-white); // logo图片文本色
                    --el-logo-title-color:var(--el-color-white); // logo文字文本色
                }
            }
            // 菜单栏白背景时样式控制
            &[data-menu-theme*='light']{
                .vk-layout-sidebar__wrap{
                    --el-logo-img-color:var(--el-color-primary); // logo图片文本色
                    --el-logo-title-color:var(--el-color-primary); // logo文字文本色
                }
                .vk-collapse-button{
                    --el-sidebar-fold-button-bg-color: var(--el-color-white); // 侧边栏底部折叠按钮背景色
                    --el-sidebar-fold-button-text-color: var(--el-text-color-regular); // 侧边栏底部折叠按钮文本色
                }
            }
            // 菜单栏白背景时样式控制
            &[data-menu-theme*='_light_']{
                .vk-collapse-button{
                    --el-sidebar-fold-button-bg-color: transparent; // 侧边栏底部折叠按钮背景色
                    --el-sidebar-fold-button-text-color: var(--el-text-color-regular); // 侧边栏底部折叠按钮文本色
                }
            }
            // 菜单栏非白背景时样式控制
            &:not([data-menu-theme*='light']){
                .vk-layout-sidebar__wrap{
                    --el-logo-img-color:var(--el-color-white); // logo图片文本色
                    --el-logo-title-color:var(--el-color-white); // logo文字文本色
                }
                .vk-layout-vertical__column{
                    .vk-layout-sidebar__wrap{
                        --el-logo-title-color:var(--el-color-primary); // logo文字文本色
                    }
                }
                .vk-collapse-button{
                    --el-sidebar-fold-button-bg-color: rgba(255,255,255,.05); // 侧边栏底部折叠按钮背景色
                    --el-sidebar-fold-button-text-color: rgba(255,255,255,.65); // 侧边栏底部折叠按钮文本色
                }
            }
        }
    }
    &.dark{
        body{
            .vk-header,.vk-layout-sidebar__wrap{
                --el-logo-img-color:var(--el-color-white); // logo图片文本色
                --el-logo-title-color:var(--el-color-white); // logo文字文本色
            }
            .vk-collapse-button{
                --el-sidebar-fold-button-bg-color: rgba(255,255,255,.05); // 侧边栏底部折叠按钮背景色
                --el-sidebar-fold-button-text-color: rgba(255,255,255,.65); // 侧边栏底部折叠按钮文本色
            }
        }
    }
}
```
改文件主要控制公共的一些主题色配置，以及暗系主题的顶栏、侧边栏的主题配置，`logo`以及分栏模式侧边栏内的折叠按钮的主题配置。

15. `src/style/theme/index.scss`文件：
```
@import "./public.scss";
@import "./header.scss";
@import "./sidebar.scss";

```
改文件只作为主题色控制文件的输出文件，将其他几个主题色文件在该文件内引入。

以上就完善了主题色配置功能，并对`element-plus`的暗系主题配置进行了兼容。现在切换明暗主题色，系统的主题就能正常跟随切换了。