﻿# Excel导入导出
安装`excel`导入导出需要的依赖：
```
yarn add xlsx
```

## 导入
1. 新建`src/components/Excel/importExcel.vue`文件：
```vue
<script setup lang="ts" name="ImportExcel">
import * as XLSX from 'xlsx'
import { dateUtil } from '@/utils/dateFormat'
import type { ExcelData } from './type'

const props = defineProps({
  // 日期时间格式。如果不提供或者提供空值，将返回原始Date对象
  dateFormat: {
    type: String
  },
  // 时区调整。实验性功能，仅为了解决读取日期时间值有偏差的问题。目前仅提供了+08:00时区的偏差修正值
  // https://github.com/SheetJS/sheetjs/issues/1470#issuecomment-501108554
  timeZone: {
    type: Number,
    default: 8
  }
})

const emit = defineEmits(['success', 'error'])
const inputRef = ref<HTMLInputElement | null>(null)
const loadingRef = ref<boolean>(false)

/**
 *
 * @param sheet excel表某一个sheet
 * @description 获取第一行作为表头使用
 */
const getHeaderRow = (sheet:XLSX.WorkSheet) => {
  if (!sheet || !sheet['!ref']) return []
  const headers: string[] = []
  // A3:B7=>{s:{c:0, r:2}, e:{c:1, r:6}}
  const range = XLSX.utils.decode_range(sheet['!ref'])

  const R = range.s.r
  /* start in the first row */
  for (let C = range.s.c; C <= range.e.c; ++C) {
    /* walk every column in the range */
    const cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })]
    /* find the cell in the first row */
    let hdr = 'UNKNOWN ' + C // <-- replace with your desired default
    if (cell && cell.t) hdr = XLSX.utils.format_cell(cell)
    headers.push(hdr)
  }
  return headers
}

/**
 * @params workbook excel表文件数据
 * @description 获取excel表所有sheet的数据
 */
const getExcelData = (workbook:XLSX.WorkBook) => {
  const excelData: ExcelData[] = []
  const { dateFormat, timeZone } = props
  for (const sheetName of workbook.SheetNames) {
    const worksheet = workbook.Sheets[sheetName]
    const header: string[] = getHeaderRow(worksheet)
    let results = XLSX.utils.sheet_to_json(worksheet, {
      raw: true,
      dateNF: dateFormat // Not worked
    }) as object[]
    results = results.map((row: object) => {
      for (const field in row) {
        if (row[field] instanceof Date) {
          if (timeZone === 8) {
            row[field].setSeconds(row[field].getSeconds() + 43)
          }
          if (dateFormat) {
            row[field] = dateUtil(row[field]).format(dateFormat)
          }
        }
      }
      return row
    })
    excelData.push({
      header,
      results,
      meta: {
        sheetName
      }
    })
  }
  return excelData
}

/**
 * @params rawFile excel表文件
 * @description 读取excel表文件数据
 */
const readerData = (rawFile: File) => {
  loadingRef.value = true
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = async (e) => {
      try {
        const data = e.target && e.target.result
        const workbook = XLSX.read(data, { type: 'array', cellDates: true })
        // console.log(workbook);
        /* DO SOMETHING WITH workbook HERE */
        const excelData = getExcelData(workbook)
        emit('success', excelData)
        resolve('')
      } catch (error) {
        reject(error)
        emit('error')
      } finally {
        loadingRef.value = false
      }
    }
    reader.readAsArrayBuffer(rawFile)
  })
}

/**
 * @params rawFile excel表文件
 * @description 上传excel表文件
 */
const upload = async (rawFile: File) => {
  const inputRefDom = unref(inputRef)
  if (inputRefDom) {
    // fix can't select the same excel
    inputRefDom.value = ''
  }
  await readerData(rawFile)
}

/**
 * @description 点击上传按钮，触发上传文件的点击事件
 */
const handleUpload = () => {
  const inputRefDom = unref(inputRef)
  inputRefDom && inputRefDom.click()
}

/**
 * @params e 点击事件对象
 * @description 触发选择文件管理器
 */
const handleInputClick = (e:Event) => {
  const files = e && (e.target as HTMLInputElement).files
  const rawFile = files && files[0] // only setting files[0]
  if (!rawFile) return
  upload(rawFile)
}
</script>

<template>
  <div>
    <input ref="inputRef" type="file" v-show="false" accept=".xlsx, .xls" @change="handleInputClick" />
    <div @click="handleUpload">
      <slot></slot>
    </div>
  </div>
</template>

<style scoped lang="scss"></style>

```
该文件封装了导入`excel`文件，获取表中数据的功能

新建`src/components/Excel/type.ts`文件：
```ts
import type { JSON2SheetOpts, WritingOptions, BookType } from 'xlsx'

export interface ExcelData<T = any> {
  header: string[]
  results: T[]
  meta: { sheetName: string }
}
```
添加相关的类型声明

修改`src/utils/dateFormat.ts`文件：
```ts

const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss'
const DATE_FORMAT = 'YYYY-MM-DD'

export function formatToDateTime(date: dayjs.Dayjs | undefined = undefined, format = DATE_TIME_FORMAT): string {
  return dayjs(date).format(format)
}

export function formatToDate(date: dayjs.Dayjs | undefined = undefined, format = DATE_FORMAT): string {
  return dayjs(date).format(format)
}

export const dateUtil = dayjs
```
增加时间格式化相关的封装函数，将之前的`formatDate`封装函数删除。

修改`src/views/Components/ElementPlus/Data/Calendar/index.vue`文件：
```vue
import { formatToDate } from '@/utils/dateFormat'


<el-form-item>
  <el-button @click="backToday" :disabled="isToday(formatToDate(dayjs(new Date(selectedDate))))">{{$t('buttons.buttonBackToday')}}</el-button>
</el-form-item>
```
这里将之前引用的`formatDate`函数替换为新的封装函数

修改`src/views/Plugins/Excel/Import/index.vue`文件：
```vue
<script setup lang="ts" name="Import">
import useScrollPosition from '@/hooks/scrollPosition'
import useVkConfig from '@/hooks/vkConfig'
import type { ExcelData } from '@/components/Excel/type'
const store = useStore()
const route = useRoute()

const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型

// 滚动行为
useScrollPosition(route, store, vkConfig)

const tableListRef = ref<
  {
    title: string;
    columns?: any[];
    dataSource?: any[];
    currentPageDataSource?: any[];
    pageIndex: number;
    pageSize: number;
    count: number;
  }[]
>([])

const loadDataSuccess = (excelDataList: ExcelData[]) => {
  tableListRef.value = []
  for (const excelData of excelDataList) {
    const {
      header,
      results,
      meta: { sheetName }
    } = excelData
    const columns = []
    for (const title of header) {
      columns.push({ title, dataIndex: title })
    }
    const currentPageDataSource = results.slice(10 * 0, 10 * 0 + 10)
    tableListRef.value.push({ title: sheetName, dataSource: results, currentPageDataSource, columns, pageIndex: 1, pageSize: 10, count: results.length })
  }
}

const handleSizeChange = (idx, val) => {
  tableListRef.value.forEach((item, index) => {
    if (idx === index) {
      item.pageSize = val
      item.pageIndex = 1
      item.currentPageDataSource = item.dataSource.slice(item.pageSize * (item.pageIndex - 1), item.pageSize * (item.pageIndex - 1) + item.pageSize)
    }
  })
}
const handleCurrentChange = (idx, val) => {
  tableListRef.value.forEach((item, index) => {
    if (idx === index) {
      item.pageIndex = val
      item.currentPageDataSource = item.dataSource.slice(item.pageSize * (item.pageIndex - 1), item.pageSize * (item.pageIndex - 1) + item.pageSize)
    }
  })
}
</script>

<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <template #header>
        <div class="card-header">
          <span>Excel导入功能</span>
          <ImportExcel @success="loadDataSuccess" dateFormat="YYYY-MM-DD"><el-button type="primary">导入Excel</el-button></ImportExcel>
        </div>
      </template>
      <div class="table-list" v-for="(item, index) in tableListRef" :key="index">
        <h2>{{item.title}}</h2>
        <vxe-table border stripe :column-config="{resizable: true}" :row-config="{isHover: true}" :data="item.currentPageDataSource" :key="index">
          <vxe-column type="seq" width="60"></vxe-column>
          <vxe-column v-for="(td, idx) in item.columns" :key="idx" :field="td.dataIndex" :title="td.title"></vxe-column>
        </vxe-table>
        <el-pagination
          v-if="item.dataSource.length>0"
          v-model:currentPage="item.pageIndex"
          v-model:page-size="item.pageSize"
          :page-sizes="[10, 20, 40, 80]"
          :background="true"
          layout="total, sizes, prev, pager, next, jumper"
          :total="item.count"
          @size-change="(val) => handleSizeChange(index, val)"
          @current-change="(val) => handleCurrentChange(index, val)" />
      </div>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}

h2{
  margin-bottom: 10px;
  font-weight: bold;
  font-size: 24px;
  text-align: center;
}

.el-card{
  display: flex;
  flex-direction: column;

  :deep(.el-card__header){
    .card-header{
      display: flex;
      align-items: center;
      justify-content: space-between;

    }
  }

  :deep(.el-card__body){
    flex: 1;
  }

  .table-list{
    width: 100%;
    margin-top: 20px;
  }

  .table-list+.table-list{
    h2{
      margin-top: 20px;
    }
  }
}

.el-pagination{
  justify-content: flex-end;
  margin-top: 10px;
}
</style>
```
该页面展示了如何使用封装的`excel`导入功能

## 导出

新建`mock/export2Excel.ts`文件：
```
import { MockMethod } from 'vite-plugin-mock'
import { Random } from 'mockjs'

const listMock = (pageSize) => {
  const result: any[] = []
  for (let i = 0; i < pageSize; i++) {
    result.push({
      id: Random.id(),
      title: '@ctitle()',
      description: '@cparagraph()',
      date: Random.datetime(),
      'like|10-500': 500,
      author: '@cname()'
    })
  }
  return result
}

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = () => {
  return true
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = () => {
  return true
}
export default [
  {
    url: '/getExcelList',
    method: 'post',
    response: (data) => {
      if (isRefreshTokenValid()) {
        if (isAccessTokenValid()) {
          return {
            code: 200,
            status: true,
            info: '数据获取成功！',
            data: listMock(data.body.pageSize),
            total: 200
          }
        } else {
          return {
            code: 201004,
            status: false,
            info: 'token已失效，请更新token！',
            data: []
          }
        }
      } else {
        return {
          code: 201002,
          status: false,
          info: '登录已超时，请重新登录！',
          data: []
        }
      }
    }
  }
] as MockMethod[]
```

新建`src/api/export2Excel.ts`文件：
```
import { http } from '../utils/http'

// 获取列表
export const getListData = (data: object, config: object) => {
  return http.request('post', '/getExcelList', data, config)
}
```

新建`src/components/Excel/Export2Excel.ts`文件：
```
import * as xlsx from 'xlsx'
import type { WorkBook } from 'xlsx'
import type { JsonToSheet, AoAToSheet } from './type'

const { utils, writeFile } = xlsx

export const DEF_FILE_NAME = 'excel-list.xlsx'

export function jsonToSheetXlsx<T = any>({ data, header, filename = DEF_FILE_NAME, json2sheetOpts = {}, write2excelOpts = { bookType: 'xlsx' } }: JsonToSheet<T>) {
  const arrData = [...data]
  if (header) {
    arrData.unshift(header)
    json2sheetOpts.skipHeader = true
  }

  const worksheet = utils.json_to_sheet(arrData, json2sheetOpts)

  /* add worksheet to workbook */
  const workbook: WorkBook = {
    SheetNames: [filename],
    Sheets: {
      [filename]: worksheet
    }
  }
  /* output format determined by filename */
  writeFile(workbook, filename, write2excelOpts)
  /* at this point, out.xlsb will have been downloaded */
}

export function aoaToSheetXlsx<T = any>({ data, header, filename = DEF_FILE_NAME.split('.')[0], write2excelOpts = { bookType: 'xlsx' } }: AoAToSheet<T>) {
  const arrData = [...data]
  if (header) {
    arrData.unshift(header)
  }

  const worksheet = utils.aoa_to_sheet(arrData)

  /* add worksheet to workbook */
  const workbook: WorkBook = {
    SheetNames: [filename],
    Sheets: {
      [filename]: worksheet
    }
  }
  /* output format determined by filename */
  writeFile(workbook, filename, write2excelOpts)
  /* at this point, out.xlsb will have been downloaded */
}
```

修改`src/components/Excel/type.ts`文件：
```
import type { JSON2SheetOpts, WritingOptions } from 'xlsx'

export interface ExcelData<T = any> {
  header: string[]
  results: T[]
  meta: { sheetName: string }
}

export interface JsonToSheet<T = any> {
  data: T[]
  header?: T
  filename?: string
  json2sheetOpts?: JSON2SheetOpts
  write2excelOpts?: WritingOptions
}

export interface AoAToSheet<T = any> {
  data: T[][]
  header?: T[]
  filename?: string
  write2excelOpts?: WritingOptions
}
```

新建`src/components/Excel/exportModal.vue`文件：
```
<script setup lang="ts" name="">
import { useResizeObserver, useDebounceFn } from '@vueuse/core'
import useVkConfig from '@/hooks/vkConfig'
import draggable from 'vuedraggable'
import { DEF_FILE_NAME } from './Export2Excel'

const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型
const dialogVisible = ref(false)

const props = defineProps({
  fieldList: { type: Array as any },
  hasSelected: { type: Boolean }
})

// 打开弹窗
const handleOpenDialog = () => {
  dialogVisible.value = true
  setTimeout(() => {
    getModalHeight()
  }, 200)
}
// 关闭弹窗
const handleCloseDialog = () => {
    dialogVisible.value = false
}
// 子组件将方法暴露给父组件
defineExpose({ handleOpenDialog })

// 最大化效果
const fullscreen = ref(false)
// 最大化点击事件
const maxSizeHandle = () => {
  fullscreen.value = !fullscreen.value
}
// 监听最大化事件
watch(
  () => fullscreen.value,
  () => {
    getModalHeight()
  }
)
const modalHeight = ref(0)
// 监听元素尺寸变化
useResizeObserver(
  document.body,
  useDebounceFn(() => {
    getModalHeight()
  }, 200)
)
const modalHeader = ref(null)
const modalFooter = ref(null)
// 获取当前的表格的最大高度
const getModalHeight = () => {
  if (dialogVisible.value) {
    const rect = document.body.getBoundingClientRect()
    if (fullscreen.value) {
      modalHeight.value = rect.height - modalHeader.value.offsetHeight - modalFooter.value.offsetHeight - 15 * 2 - vkConfig.value.space * 2 - 2
    } else {
      modalHeight.value = rect.height * 0.7 - modalHeader.value.offsetHeight - modalFooter.value.offsetHeight - 15 * 2 - vkConfig.value.space * 2 - 2
    }
  }
}

const emit = defineEmits(['success'])

const handleOk = () => {
  const sortArr = []
  const headerObj = {}
  fieldsList.value.forEach((item) => {
    if (checkedFields.value.includes(item.key)) {
      sortArr.push(item.key)
      headerObj[item.key] = item.value
    }
  })
  emit('success', {
          filename: `${exportInfo.value.filename === '' ? DEF_FILE_NAME.split('.')[0] : exportInfo.value.filename}.${exportInfo.value.fileType}`,
          fileType: exportInfo.value.fileType,
          dataScope: exportInfo.value.dataScope,
          header: headerObj,
          sort: sortArr,
          headerType: exportInfo.value.headerType
        })
  handleCloseDialog()
}

const exportInfo = ref({
  filename: '',
  fileType: 'xlsx',
  dataScope: 1,
  headerType: 'default'
})

const checkAllField = ref(false)
const isIndeterminate = ref(false)
const checkedFields = ref([])
const fieldsList = ref(props.fieldList || [])

const handleCheckAllChange = (val: boolean) => {
  checkedFields.value = []
  fieldsList.value.forEach((item) => {
    if (val) {
      checkedFields.value.push(item.key)
    }
  })
  isIndeterminate.value = false
  console.log(checkedFields.value)
}
const handleCheckedFieldsChange = (value: string[]) => {
  const checkedCount = value.length
  checkAllField.value = checkedCount === props.fieldList.length
  isIndeterminate.value = checkedCount > 0 && checkedCount < props.fieldList.length
  console.log(checkedFields.value)
}
</script>

<template>
  <el-dialog v-model="dialogVisible" width="600px" append-to-body destroy-on-close :fullscreen="fullscreen" :show-close="false" :before-close="handleCloseDialog">
    <template #header>
      <div class="modal-header" ref="modalHeader">
        <div class="header-title">{{$t('moduleTitle.moduleTitleExport')}}</div>
        <el-tooltip :content="fullscreen?$t('buttons.buttonRestoreDown'):$t('buttons.buttonMaximize')" placement="bottom">
          <el-button size="small" @click="maxSizeHandle"><svg-icon :icon="fullscreen?'bi:fullscreen-exit':'bi:fullscreen'"></svg-icon></el-button>
        </el-tooltip>
        <el-tooltip :content="$t('buttons.buttonClose')" placement="bottom">
          <el-button size="small" @click="handleCloseDialog"><svg-icon icon="ep:close"></svg-icon></el-button>
        </el-tooltip>
      </div>
    </template>
    <div class="export-modal__body">
      <el-scrollbar :style="{height: modalHeight+'px'}">
        <el-form>
          <el-form-item :label="$t('exportModal.fileName')" prop="filename">
            <el-input v-model="exportInfo.filename" clearable />
          </el-form-item>
          <el-form-item :label="$t('exportModal.fileType')">
            <el-select v-model="exportInfo.fileType" placeholder="">
              <el-option :label="$t('exportModal.fileTypeXlsx')" value="xlsx" />
              <el-option :label="$t('exportModal.fileTypeCsv')" value="csv" />
              <el-option :label="$t('exportModal.fileTypeHtml')" value="html" />
              <el-option :label="$t('exportModal.fileTypeXml')" value="xml" />
              <el-option :label="$t('exportModal.fileTypeTxt')" value="txt" />
            </el-select>
          </el-form-item>
          <el-form-item :label="$t('exportModal.dataScope')">
            <el-select v-model="exportInfo.dataScope" placeholder="">
              <el-option :label="$t('exportModal.dataScopeCurrentPage')" :value="1" />
              <el-option :label="$t('exportModal.dataScopeSelected')" :value="2" :disabled="!props.hasSelected" />
              <el-option :label="$t('exportModal.dataScopeAll')" :value="3" />
            </el-select>
          </el-form-item>
          <el-form-item :label="$t('exportModal.headerType')">
            <el-radio-group class="radio-button" v-model="exportInfo.headerType">
              <el-radio-button label="default">{{$t('exportModal.headerTypeDefault')}}</el-radio-button>
              <el-radio-button label="custom">{{$t('exportModal.headerTypeCustom')}}</el-radio-button>
            </el-radio-group>
          </el-form-item>
          <el-form-item :label="$t('exportModal.fields')">
            <div class="field-list flex-1 m-6px">
              <div class="all-field">
                <el-checkbox v-model="checkAllField" :indeterminate="isIndeterminate" @change="handleCheckAllChange">{{$t('exportModal.allFields')}}</el-checkbox>
              </div>
              <el-checkbox-group v-model="checkedFields" @change="handleCheckedFieldsChange">
                <draggable :list="fieldsList" :item-key="(key) => key" animation="300" handle=".move">
                  <template #item="{ element }">
                    <div class="item-field">
                      <span class="move"><svg-icon icon="ri:drag-move-2-fill"></svg-icon></span>
                      <el-checkbox :label="element.key">{{element.value}}</el-checkbox>
                    </div>
                  </template>
                </draggable>
              </el-checkbox-group>
            </div>
          </el-form-item>
        </el-form>
      </el-scrollbar>
    </div>
    <template #footer>
      <span class="modal-footer" ref="modalFooter">
        <el-button @click="handleCloseDialog">{{ $t('buttons.buttonCancel') }}</el-button>
        <el-button type="primary" @click="handleOk">{{ $t('buttons.buttonConfirm') }}</el-button>
      </span>
    </template>
  </el-dialog>
</template>

<style scoped lang="scss">
.modal-header{
  display: flex;
  margin: -15px calc(0px - var(--el-space));
  padding: 15px var(--el-space);

  .header-title{
    flex: 1;
    height: 24px;
    line-height: 24px;
  }

  .el-button{
    margin-left: 0;
    background: none!important;
    border: none!important;

    &:hover,&:focus{
      background: none;
    }
  }
}

.modal-footer{
  display: inline-flex;
  overflow: hidden;
}

.vxe-table--render-default.size--mini{
  font-size: 14px;
}

.el-dialog{
  :deep(.el-dialog__body){
    padding: 0;
  }
}

.export-modal__body{
  margin: calc(0px - var(--el-space));
  overflow: hidden;

  :deep(.el-scrollbar__view){
    padding: var(--el-space);
  }
}

.field-list{
  border: 1px solid var(--el-border-color-lighter);

  .all-field{
    padding-left: 36px;
    background: var(--el-fill-color-light);
    border-bottom: 1px solid var(--el-border-color-lighter);
  }

  .el-checkbox-group{
    padding: 6px 0;
  }

  .item-field{
    display: flex;
    align-items: center;

    .move{
      padding: 0 10px;
      font-size: 14px;
      cursor: move;

      .el-icon{
        color: var(--el-text-color-secondary);
      }
    }
  }
}
</style>
```

新建`src/plugins/i18n/zh-CN/exportModal.ts`、`src/plugins/i18n/en/exportModal.ts`文件：
```
export default {
  fileName: '文件名称',
  fileType: '文件类型',
  fileTypeXlsx: 'xlsx(*.xlsx)',
  fileTypeCsv: 'csv(*.csv)',
  fileTypeHtml: '网页(*.html)',
  fileTypeXml: 'xml数据(*.xml)',
  fileTypeTxt: '文本文件(*.txt)',
  dataScope: '选择数据',
  dataScopeCurrentPage: '当前页数据',
  dataScopeSelected: '选中的数据',
  dataScopeAll: '全量后台数据',
  headerType: '表头选择',
  headerTypeDefault: '默认',
  headerTypeCustom: '自定义',
  fields: '字段选择',
  allFields: '全部字段'
}

export default {
  fileName: 'File Name',
  fileType: 'File Type',
  fileTypeXlsx: 'Xlsx(*.xlsx)',
  fileTypeCsv: 'Csv(*.csv)',
  fileTypeHtml: 'Web Page(*.html)',
  fileTypeXml: 'Xml Data(*.xml)',
  fileTypeTxt: 'Text File(*.txt)',
  dataScope: 'Data Scope',
  dataScopeCurrentPage: 'Current Page Data',
  dataScopeSelected: 'Selected Data',
  dataScopeAll: 'All Data',
  headerType: 'Header Type',
  headerTypeDefault: 'Default',
  headerTypeCustom: 'Custom',
  fields: 'Fields',
  allFields: 'All Fields'
}
```

修改`src/plugins/i18n/zh-CN/moduleTitle.ts`、`src/plugins/i18n/en/moduleTitle.ts`：
```
  moduleTitleExport: '导出数据'

  moduleTitleExport: 'Export Data'
```

修改`src/views/Plugins/Excel/Export/index.vue`文件：
```
<script setup lang="ts" name="Export">
import useScrollPosition from '@/hooks/scrollPosition'
import useVkConfig from '@/hooks/vkConfig'
import { jsonToSheetXlsx } from '@/components/Excel/Export2Excel'
import { getListData } from '@/api/export2Excel'
import { VxeTableInstance, VxeTableEvents } from 'vxe-table'
import { warnMessage } from '@/utils/message'
const store = useStore()
const route = useRoute()

const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型

// 滚动行为
useScrollPosition(route, store, vkConfig)

const pageIndex = ref(1)
const pageSize = ref(20)
const listData: any = reactive({
  data: [],
  total: 0
})

const getInitData = () => {
   getListData({ pageIndex: pageIndex.value, pageSize: pageSize.value }, { config: { showLoading: false, mockEnable: true } }).then((res:any) => {
    if (res.data && res.data.length > 0) {
      setTimeout(() => {
        listData.data = []
        listData.data.push(...res.data)
        listData.total = res.total
      }, 2000)
    }
  })
}
onMounted(() => {
  getInitData()
})

const handleSizeChange = (val) => {
  pageSize.value = val
  pageIndex.value = 1
  getInitData()
}
const handleCurrentChange = (val) => {
  pageIndex.value = val
  getInitData()
}

const defaultHeader = () => {
  console.log(listData.data)
  jsonToSheetXlsx({ data: listData.data, filename: '使用key做默认头部.xlsx' })
}
const customHeader = () => {
  jsonToSheetXlsx({
    data: listData.data,
    header: {
      id: 'ID',
      title: '标题',
      description: '描述',
      date: '发布日期',
      like: '评论',
      author: '作者'
    },
    filename: '自定义头部.xlsx',
    json2sheetOpts: {
      // 指定顺序
      header: ['id', 'title', 'description', 'author', 'like', 'date']
    }
  })
}

const xTable = ref({} as VxeTableInstance)

const selectAllChangeEvent: VxeTableEvents.CheckboxAll = ({ checked }) => {
  const $table = xTable.value
  const records = $table.getCheckboxRecords()
  console.log(checked ? '所有勾选事件' : '所有取消事件', records)
}

const selectChangeEvent: VxeTableEvents.CheckboxChange = ({ checked }) => {
  const $table = xTable.value
  const records = $table.getCheckboxRecords()
  console.log(checked ? '勾选事件' : '取消事件', records)
}

const getSelectEvent = () => {
  const $table = xTable.value
  const selectRecords = $table.getCheckboxRecords()
  return selectRecords
}

const exportSelected = () => {
  const selectedData = getSelectEvent()
  if (selectedData.length === 0) {
    warnMessage('请先选择要导出的数据')
    return false
  }
  jsonToSheetXlsx({
    data: getSelectEvent(),
    header: {
      id: 'ID',
      title: '标题',
      description: '描述',
      date: '发布日期',
      like: '评论',
      author: '作者'
    },
    filename: '导出选中.xlsx',
    json2sheetOpts: {
      // 指定顺序
      header: ['id', 'title', 'description', 'author', 'like', 'date']
    }
  })
}

const exportAllData = () => {
  const selectedData = []
  getListData({ pageIndex: 1, pageSize: listData.total }, { config: { showLoading: false, mockEnable: true } }).then((res:any) => {
    if (res.data && res.data.length > 0) {
      selectedData.push(...res.data)
      jsonToSheetXlsx({
        data: selectedData,
        header: {
          id: 'ID',
          title: '标题',
          description: '描述',
          date: '发布日期',
          like: '评论',
          author: '作者'
        },
        filename: '全量导出.xlsx',
        json2sheetOpts: {
          // 指定顺序
          header: ['id', 'title', 'description', 'author', 'like', 'date']
        }
      })
    }
  })
}

const hasSelected = ref(false)
const exportModalRef = ref(null)
const openModal = () => {
  const exportModalRefDom = unref(exportModalRef)
  if (!exportModalRefDom) return
  if (getSelectEvent().length > 0) {
    hasSelected.value = true
  } else {
    hasSelected.value = false
  }
  exportModalRefDom.handleOpenDialog()
}

const fieldList = ref([
  { key: 'id', value: 'ID' },
  { key: 'title', value: '标题' },
  { key: 'description', value: '描述' },
  { key: 'date', value: '发布日期' },
  { key: 'like', value: '评论' },
  { key: 'author', value: '作者' }
  ])

// 高级导出
const customExport = (obj) => {
  const filename = obj.filename
  const customData = []
  let header = null
  const bookType = obj.fileType
  const headerSort = obj.sort
  if (obj.headerType === 'custom') {
    header = obj.header
  }
  if (obj.dataScope === 1) {
    listData.data.forEach((item) => {
      const itemObj = {}
      headerSort.forEach((current) => {
        itemObj[current] = item[current]
      })
      customData.push(itemObj)
    })
    jsonToSheetXlsx({
      data: customData,
      header: header,
      filename: filename,
      json2sheetOpts: {
        // 指定顺序
        header: headerSort
      },
      write2excelOpts: {
        bookType
      }
    })
  } else if (obj.dataScope === 2) {
    getSelectEvent().forEach((item) => {
      const itemObj = {}
      headerSort.forEach((current) => {
        itemObj[current] = item[current]
      })
      customData.push(itemObj)
    })
    jsonToSheetXlsx({
      data: customData,
      header: header,
      filename: filename,
      json2sheetOpts: {
        // 指定顺序
        header: headerSort
      },
      write2excelOpts: {
        bookType
      }
    })
  } else {
    getListData({ pageIndex: 1, pageSize: listData.total }, { config: { showLoading: false, mockEnable: true } }).then((res:any) => {
      if (res.data && res.data.length > 0) {
        res.data.forEach((item) => {
          const itemObj = {}
          headerSort.forEach((current) => {
            itemObj[current] = item[current]
          })
          customData.push(itemObj)
        })
        jsonToSheetXlsx({
          data: customData,
          header: header,
          filename: filename,
          json2sheetOpts: {
            // 指定顺序
            header: headerSort
          },
          write2excelOpts: {
            bookType
          }
        })
      }
    })
  }
}
</script>

<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <template #header>
        <div class="card-header">
          <span>Excel导出功能</span>
        </div>
      </template>
      <div class="button-list">
        <el-button @click="defaultHeader">导出当前页（默认头部）</el-button>
        <el-button @click="customHeader">导出当前页（自定义头部）</el-button>
        <el-button @click="exportSelected">导出选中</el-button>
        <el-button @click="exportAllData">全量导出后台数据</el-button>
        <el-button @click="openModal">高级导出</el-button>
      </div>
      <vxe-table ref="xTable" border stripe :column-config="{resizable: true}" :row-config="{isHover: true}" :data="listData.data" :checkbox-config="{labelField: 'id', highlight: true, range: true}" @checkbox-all="selectAllChangeEvent" @checkbox-change="selectChangeEvent">
        <vxe-column type="seq" width="60"></vxe-column>
        <vxe-column type="checkbox" title="ID"></vxe-column>
        <vxe-column field="title" title="标题"></vxe-column>
        <vxe-column field="description" title="描述"></vxe-column>
        <vxe-column field="date" title="发布日期"></vxe-column>
        <vxe-column field="like" title="评论"></vxe-column>
        <vxe-column field="author" title="作者"></vxe-column>
      </vxe-table>
      <el-pagination
        v-if="listData.total>0"
        v-model:currentPage="pageIndex"
        v-model:page-size="pageSize"
        :page-sizes="[20, 40, 100, 200]"
        :background="true"
        layout="total, sizes, prev, pager, next, jumper"
        :total="listData.total"
        @size-change="handleSizeChange"
        @current-change="handleCurrentChange" />
      <ExportModal ref="exportModalRef" :fieldList="fieldList" :hasSelected="hasSelected" @success="customExport"></ExportModal>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}

.el-card{
  display: flex;
  flex-direction: column;

  :deep(.el-card__header){
    .card-header{
      display: flex;
      align-items: center;
      justify-content: space-between;

    }
  }

  :deep(.el-card__body){
    flex: 1;
  }

  .button-list{
    width: 100%;
    margin-bottom: 20px;
  }
}

.el-pagination{
  justify-content: flex-end;
  margin-top: 10px;
}
</style>
```