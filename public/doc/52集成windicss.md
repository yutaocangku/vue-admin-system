﻿# 集成windicss
windicss可以更便捷的书写样式，相当于预设了全场景的类名，只需要根据需要添加对应类名即可。

## 安装
安装依赖插件：
```
yarn add vite-plugin-windicss windicss -D
```

修改`vite.config.ts`文件：
```
import WindiCSS from 'vite-plugin-windicss'

plugins: [

  ...

  WindiCSS(),

  ...
]
```

修改`src/main.ts`文件：
```
import 'virtual:windi.css'
import 'virtual:windi-devtools'
```
以上修改完成，就可以在项目中使用`windicss`了

## 配置
不过还可以为`windicss`做配置，扩展文件类型，排除检测文件，设置默认样式等操作，需要在根目录新建`windicss.config.ts`文件：
```
import { defineConfig } from 'vite-plugin-windicss'

export default defineConfig({
  extract: {
    include: ['src/**/*.{vue,html,jsx,tsx}'],
    exclude: ['node_modules', '.git']
  },
  // 暗黑模式使用类名控制，也可以是媒体查询模式：media
  darkMode: 'class',
  attributify: false,
  // 自定义样式
  theme: {
    extend: {
      // 将windicss的响应式与element-plus的响应式配置成一致，这里xs属于自定义的响应式，需要注意使用xs时，要与element-plus的xs保持一致，需要添加前缀`<`，`2xl`这个响应式在element-plus中是不存在的，属于windicss特有响应式
      screens: {
        xs: '768px',
        sm: '768px',
        md: '992px',
        lg: '1200px',
        xl: '1920px',
        '2xl': '2560px'
      }
    }
  },
  // 自定义重复性使用工具类合集，当你使用某几个工具类非常频繁时，可以在此处配置成合集，然后在使用时直接使用合集名称即可
  shortcuts: {
    // btn: 'py-2 px-4 font-semibold rounded-lg shadow-md',
    // 'btn-green': 'text-white bg-green-500 hover:bg-green-700'
  },
  // 可以自己开发插件使用
  plugins: []
})
```
以上是关于`windicss`的最简单配置，使用只需要根据官网工具类中的用法示例使用即可。

## 分析
工具类的使用还可以进行分析，分析项目中，各个工具类使用的次数，在哪些页面使用的频率。

安装分析器依赖包：
```
yarn add windicss-analysis -D
```

修改`package.json`文件，增加分析命令：
```
"css:analysis": "windicss-analysis"
```
执行命令后，就可以生成分析报告了。

## 使用
修改`src/components/VkFooter/index.vue`文件：
```
<script setup lang="ts">
import dayjs from 'dayjs'
const currentYear = ref(dayjs().year())
</script>

<template>
  <footer class="flex overflow-hidden p-20px">
    <p class="flex-1 items-center justify-center m-0 opacity-50 leading-normal text-center">Copyright © {{currentYear}} {{ $t('settings.settingProjectTitle') }}</p>
  </footer>
</template>

<style lang="scss" scoped>
footer{
  height: var(--el-footer-height);
  background-color: var(--el-fill-color-blank);
  border-top: 1px dashed var(--el-border-color);

  p{
    color: var(--el-text-color-placeholder);
  }
}
</style>
```
这里将除了需要引用css变量的样式保留外，其它样式都通过`windicss`的工具类直接在标签上写类名的方式获得具体的样式控制效果。运行项目后，具体的样式控制依然有效，说明`windicss`配置成功。