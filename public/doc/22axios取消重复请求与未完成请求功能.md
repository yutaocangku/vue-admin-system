﻿# axios取消重复请求与未完成请求
## 重复请求场景与处理方法
请求地址、请求方式、参数都相同的请求，连续请求多次，可以界定为重复请求

**重复请求出现的场景：**
1. 表单按钮双击或者频繁点击，会出现重复请求，这种情况前端需要分别再按钮事件部分做限制，请求部分也要做限制，而后端同样也需要做限制
2. `tab`页切换请求数据，切换频繁，请求数据返回又需要时间的情况下，同样会出现重复请求

**重复请求的处理方法：**
重复请求因参数未发生改变，对于后端返回的结果没有影响，所以重复请求的处理方式应该是保留第一次请求，后续的重复请求全部从请求队列中移除，这样能保证数据以最快的速度返回
## 未完成请求场景与处理方法

**未完成请求出现的场景：**
1. 频繁进行多条件筛选，如前端为每个筛选条件都加上了在操作后就调用接口，或者在上一次筛选条件结果未返回前，又进行了新的筛选条件请求数据，这种情况下产生的多次请求，请求地址、请求方式相同，但请求参数不同
2. 页面请求未完成前，进行了路由页面切换

**未完成请求的处理方法**
1. 针对场景1的处理与重复请求的处理方式差不多，区别在于重复请求保留的是第一次，而未完成请求场景1的处理方式是保留最后一次请求，因为最终要呈现给用户的结果需要与用户最后一次查询条件保持一致，防止返回结果与查询条件不一致情况出现。
2. 针对场景2的处理方式需要在路由切换走导航守卫时，在导航守卫中将上一个页面相关的请求，全部从请求队列中移除，而当前页面的请求还需要保留

## 解决思路
项目使用的请求库是`axios`，且已经封装了请求拦截、响应拦截，那么可以在请求的时候拦截请求，判断当前的请求是否重复，如果重复就再判断是需要删除掉哪一个，大致过程如下：
1. 把目前处于`pending`的请求存储放在一个数组中
2. 存储请求应当将请求的方法（GET/POST）、请求链接、请求参数全部存储，将链接与方法拼接成一个字符串，参数则字符串化，存为另一个字段（GET请求需拆分接口与参数）
3. 每个请求发送前，都要判断当前这个请求是否已经存在与这个数组，判断条件上述两个字段
4. 先判断当前请求的链接是否存在于数组，如不存在，说明是新请求，正常把当前请求加入到数组队列中，请求结束之后，删除数组中的这个请求
5. 如判断请求已存在，再判断参数是否相同，如参数也相同，说明是重复请求，重复请求需要取消的是当前请求
6. 如判断请求已存在，但参数不同，说明是新的筛选请求，那需要将数组中已存在的请求取消掉，并移除数组，将新的请求加入到数组队列中
`axios`对于取消请求有两种方法，一是使用`cancelToken.source`工厂方法生成取消令牌`cancelToken`和取消方法`cancel`
```
const CancelToken = axios.CancelToken;
const source = CancelToken.source();
axios.get('/user/12345', {
  cancelToken: source.token
}).catch(function(thrown) {
  if (axios.isCancel(thrown)) {
    console.log('Request canceled', thrown.message);
  } else {
    // 处理错误
  }
});
axios.post('/user/12345', {
  name: 'new name'
}, {
  cancelToken: source.token
})
// 取消请求 (消息参数是可选的)
source.cancel('Operation canceled by the user.');
```
另一种是通过传递一个`executor`函数到`CancelToken`的构造函数来生成取消令牌`cancelToken`和取消方法`cancel`
```
const CancelToken = axios.CancelToken;
let cancel;
axios.get('/user/12345', {
  cancelToken: new CancelToken(function executor(c) {
    // executor 函数接收一个 cancel 函数作为参数
    cancel = c;
  })
});
// 取消请求
cancel();
```
## 封装取消请求功能
这里使用`cancelToken.source`的方式来做取消请求功能，将取消请求功能进行封装，在`src/utils/http`文件夹下新建`cancelToken.ts`文件：
```
import Axios from "axios";

// 全局是否取消重复请求、未完成请求的开关
const cancelDuplicated = true;
// 存储处于pending状态的请求信息，用于比对移除哪一个
const pendingList = [];
// 存储处于pending状态请求的map，用于取消请求
const pendingMap = new Map();

/**
 * 生成每个请求唯一的键
 * @param {*} config 
 * @returns string
 */
const getPendingKey = (config) => {
  let {url, method, params, data} = config;
  let getParams = null;
  let urlPath = url;
  if(url.indexOf("?")!=-1){
    getParams = url.split("?")[1];
    urlPath = url.split("?")[0];
  }
  if(typeof data === 'string') data = JSON.parse(data); // response里面返回的config.data是个字符串对象
  let pendingInfo = {
    pendingKey:[urlPath, method, JSON.stringify(params), JSON.stringify(data), getParams].join('&'),
    pendingPath:[urlPath, method].join('&'),
    pendingParams:[JSON.stringify(params), JSON.stringify(data), getParams].join('&')
  }
  return pendingInfo;
}

/**
 * 储存每个请求唯一值, 也就是cancel()方法, 用于取消请求
 * @param {*} config 
 */
const addPending = (config) => {
  const pendingInfo = getPendingKey(config);
  config.cancelToken = config.cancelToken || new Axios.CancelToken((cancel) => {
    if (!pendingMap.has(pendingInfo.pendingKey)) {
      pendingMap.set(pendingInfo.pendingKey, cancel);
      pendingList.push(pendingInfo);
    }
  });
}
/**
 * 删除请求对列中的请求
 * @param {*} config 
 */
 const removePending = (config) => {
  if(!cancelDuplicated) return;
  const pendingInfo = getPendingKey(config);
  if (pendingMap.has(pendingInfo.pendingKey)) {
    let pendingKeyIdx;
    pendingList.forEach(function(item,index){
      if(item.pendingKey == pendingInfo.pendingKey){
        pendingKeyIdx = index;
      }
    })
    const cancelToken = pendingMap.get(pendingInfo.pendingKey);
    console.log("走删除请求逻辑");
    cancelToken(pendingInfo.pendingKey);
    pendingMap.delete(pendingInfo.pendingKey);
    pendingList.splice(pendingKeyIdx,1);
  }
}
/**
 * 请求拦截中根据请求的可能类型，做不同的逻辑操作
 * @param {*} config 
 */
const judgeRending = (config) => {
  if(!cancelDuplicated) return;
  let pendingType = pendingRequestType(config);
  console.log(pendingType,"请求类型");
  switch(pendingType){
    case "repeat":
      let cancelFuc;
      config.cancelToken = new Axios.CancelToken((cancel) => {
        cancelFuc = cancel;
      });
      let errorMsg = `${config.url} 请求被中断`;
      // 阻止当前请求
      cancelFuc(errorMsg);
      break;
    case "filter":
      removePending(config);
      addPending(config);
      break;
    default:
      addPending(config);
      break;
  }
}

// 判断请求类型
const pendingRequestType = (config) =>{
  const pendingInfo = getPendingKey(config);
  let pendingType = 'normal';
  if(pendingList.length>0){
    pendingList.forEach(function(item){
      // 请求地址、请求方法相同时
      if(item.pendingPath == pendingInfo.pendingPath){
        if(item.pendingParams==pendingInfo.pendingParams){
          pendingType = "repeat";// 重复类型请求
        }else{
          pendingType = "filter";// 筛选类多次请求
        }
      }
    });
  }
  return pendingType;
}

export {
  judgeRending,
  removePending
}
```
然后修改`src/utils/http/index.ts`文件：
```
import { judgeRending, removePending } from './cancelToken';


  // 请求拦截
  private httpInterceptorsRequest(): void {
    AxiosHttp.axiosInstance.interceptors.request.use(
      (config: AxiosHttpRequestConfig) => {
        const $config = config;
        console.log($config.config,"请求拦截");
        judgeRending($config);
        if($config.config.showLoading){
          startLoading($config.config.loadingStyle);
        }
        // 断网提示
        if (!navigator.onLine) {
          ElMessageBox.alert(
            '您的网络故障，请检查!',
            '温馨提示',
            {
              confirmButtonText: '好的',
              type: 'warning'
            }
          )
        }
        // 开启进度条动画
        NProgress.start();
        // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
        if (typeof config.beforeRequestCallback === "function") {
          config.beforeRequestCallback($config);
          this.beforeRequestCallback = undefined;
          return $config;
        }
        // 判断初始化状态中有没有回调函数，没有的话
        if (AxiosHttp.initConfig.beforeRequestCallback) {
          AxiosHttp.initConfig.beforeRequestCallback($config);
          return $config;
        }
        // 确保config.url永远不会是undefined，增加断言
        if(!config.url){
          config.url = ""
        }
        // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
        if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
          return $config
        }
        // 回传token
        const token = getToken();
        // 判断token是否存在
        if (token) {
          const data = JSON.parse(token);
          // 确保config.headers永远不会是undefined，增加断言
          if(!config.headers){
            config.headers = {}
          }
          config.headers["Authorization"] = "Bearer " + data.accessToken;
          return $config;
        } else {
          return $config;
        }
      },
      error => {
        // 当前请求出错，当前请求加1的loading也需要减掉
        if(error.config.config.showLoading){
          endLoading();
        }
        return Promise.reject(error);
      }
    );
  }

  // 响应拦截
  private httpInterceptorsResponse(): void {
    const instance = AxiosHttp.axiosInstance;
    instance.interceptors.response.use(
      (response: AxiosHttpResponse) => {
        console.log(response,"请求响应数据");
        const $config = response.config;
        // 增加延时，相同请求再完成后，不得在短时间内重复请求
        setTimeout(() => {
          removePending($config);
        }, 500);
        // 关闭进度条动画
        NProgress.done();
        if(response.data.code==200){
          // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
          if (typeof $config.beforeResponseCallback === "function") {
            $config.beforeResponseCallback(response);
            this.beforeResponseCallback = undefined;
            return response.data;
          }
          if (AxiosHttp.initConfig.beforeResponseCallback) {
            AxiosHttp.initConfig.beforeResponseCallback(response);
            return response.data;
          }
        }else{
          if(response.data.code==201002){
            errorMessage("登录已超时，请重新登录！");
            // 清除cookie中的token
            removeToken();
            // 清除缓存中的用户信息
            storageSession.removeItem("userInfo");
            router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
          }
          if(response.data.code==201004){
            const token = getToken();
            if(token){
              const data = JSON.parse(token);
              if(!isRefreshing){
                isRefreshing = true;
                return refreshToken({refreshToken:data.refreshToken}).then((res:any)=>{
                  if(res.status){
                    setToken(res.data);
                    // 确保config.headers永远不会是undefined，增加断言
                    if(!$config.headers){
                      $config.headers = {}
                    }
                    $config.headers['Authorization'] = "Bearer " + res.data.accessToken;
                    $config.baseURL = ''
                    // 已经刷新了token，将所有队列中的请求进行重试
                    requests.forEach((callback:any) => callback(token))
                    requests = [];
                    return instance($config);
                  }
                }).catch((res:any)=>{
                  // 清除cookie中的token
                  removeToken();
                  // 清除缓存中的用户信息
                  storageSession.removeItem("userInfo");
                  router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
                }).finally(()=>{
                  isRefreshing = false;
                });
              }else{
                // 正在刷新token，将返回一个未执行resolve的promise
                return new Promise((resolve) => {
                  // 将resolve放进队列，用一个函数形式来保存，等token刷新后直接执行
                  requests.push((accessToken:string) => {
                    // 响应以后，请求接口的$config.url已经包含了baseURL部分，这里需要将baseURL清空，防止再次请求时，再次组装，导致url错误
                    $config.baseURL = '';
                    // 确保config.headers永远不会是undefined，增加断言
                    if(!$config.headers){
                      $config.headers = {}
                    }
                    $config.headers['Authorization'] = "Bearer " + accessToken;
                    resolve(instance($config));
                  })
                })
              }
            }
          }
        }
        if(response.config.config.showLoading){
          endLoading();
        }
        return response.data;
      },
      (error) => {
        const $error = error;
        if (Axios.isCancel(error)) {
          console.log(error.message);
        }else{
          if($error.config){
            // 增加延时，相同请求再完成后，不得在短时间内重复请求
            setTimeout(() => {
              removePending($error.config);
            }, 500);
          }
          if($error.config&&(!$error.config.config||($error.config.config&&$error.config.config.showLoading))){
            endLoading();
          }
          // 关闭进度条动画
          NProgress.done();
          // 请求超时处理
          if ($error.message.indexOf('timeout') !== -1) {
            ElMessageBox.alert(
              '请求超时，请重新刷新页面！',
              '温馨提示',
              {
                confirmButtonText: '好的',
                type: 'warning'
              }
            )
          }
          // 所有的响应异常 区分来源为取消请求/非取消请求
          return Promise.reject($error);
        }
      }
    );
  }
```
以上将封装的`cancelToken`方法引入，并在请求拦截、响应拦截中进行引入，响应拦截中对于完成的请求进行删除时，做了延时操作，这样做实现在上一个请求完成后的一定时间内禁止再发起相同请求，因这个时间内，该完成的请求还未从请求队列中删除，它会在请求拦截中走`repeat`从而进行拦截。这样未完成请求的重复，和已完成后短时间内再次请求造成的重复，就都规避了。

以上代码实现的功能：
1. 实现控制全局是否启用取消重复请求功能
2. 实现重复请求保留最初那个请求，而阻止新的请求发起，保证请求数据能更快返回
3. 实现相同地址请求，参数不同时的多次请求，只保留最新请求，而删除之前所有未完成请求，保证返回数据为用户最新筛选结果

可以修改`src/views/News/Index/index.vue`文件，增加两次请求，看是否能被拦截并取消请求：
```
getInitData();
setTimeout(()=>{
  getInitData();
},200);
setTimeout(()=>{
  getInitData();
},400);
```
此时再运行项目，打开新闻列表页，控制台可以看到有两次请求被中断的异常，`network`中`getNewsList`接口也只执行了一遍，可以将`src/utils/http/cancelToken.ts`文件中的`cancelDuplicated`值改为`false`，取消启用全局取消重复请求功能，再次刷新新闻列表页，此时`network`中请求就执行了三遍，获取了三遍请求结果。

## 配置页面是否启用取消重复请求功能
上文已配置全局的取消重复请求功能，但有些页面有些功能可能是需要实时获取数据的，这种频繁请求是不应该被拦截的，在上一章中，已经为`loading`添加了自定义参数配置，这里为取消重复请求功能添加自定义配置逻辑是相同的，修改`src/views/News/Index/index.vue`文件：
```
function getInitData(){
  getNewsListData({config:{showLoading:false,cancelRepeatDisabled:true}}).then((res:any)=>{
    if(res.data&&res.data.length>0){
      initData.push(...res.data);
    }
  });
}
```
这里新增了`cancelRepeatDisabled`字段，用来控制接口是否需要启用取消重复请求功能，然后修改`src/utils/http/index.ts`文件中，请求拦截和响应拦截中有关取消重复请求的代码：
```
# 请求拦截中
// 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
if(!$config.config||($config.config&&!$config.config.cancelRepeatDisabled)){
  judgeRending($config);
}



# 响应拦截中
// 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
if(!$config.config||($config.config&&!$config.config.cancelRepeatDisabled)){
  // 增加延时，相同请求再完成后，不得在短时间内重复请求
  setTimeout(() => {
    removePending($config);
  }, 500);
}

# 异常处理
// 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
if($error.config&&(!$error.config.config||($error.config.config&&!$error.config.config.cancelRepeatDisabled))){
  // 增加延时，相同请求再完成后，不得在短时间内重复请求
  setTimeout(() => {
    removePending($error.config);
  }, 500);
}
```
然后修改`src/utils/http/types.d.ts`文件，增加自定义参数配置的类型定义：
```

// 定义自定义回调中请求的数据类型
export interface AxiosHttpRequestConfig extends AxiosRequestConfig {
  config?:{
    showLoading?:boolean;
    loadingStyle?:{
      target:object|string;
      body:boolean;
      fullscreen:boolean;
      lock:boolean;
      text:string;
      spinner:string;
      background:string;
      'custom-class':string;
    },
    cancelRepeatDisabled?:boolean;
  };
  beforeRequestCallback?: (request: AxiosHttpRequestConfig) => void; // 请求发送之前
  beforeResponseCallback?: (response: AxiosHttpResponse) => void; // 相应返回之前
}
```
最后再将`src/utils/http/cancelToken.ts`中的`cancelDuplicated`字段值重新设置为`true`，再次运行项目，全局的设置取消重复请求功能是启用的，而新闻列表页面内接口**禁用取消重复请求**的字段`cancelRepeatDisabled`值为`true`，该页面接口实际不启用取消重复请求功能，刷新新闻列表页，可以发现，请求发起了三次。将页面内`cancelRepeatDisabled`字段值改为`false`，页面请求就只会有一次。

## 路由跳转取消上一个页面所有未完成的请求
重复请求功能已完成，路由跳转时上个页面未完成的请求也需要进行处理，这里需要在导航守卫中进行处理，需要在`router.beforeEach()`中进行处理，此时页面还未跳转，当前页面的所有未完成请求都需要取消请求，修改`src/router/index.ts`文件：
```
import {pendingList,pendingMap} from "@/utils/http/cancelToken";


router.beforeEach((to, _from, next) => {
  // 遍历pendingMap，将页面的所有请求取消掉
  if(pendingList.length>0){
    pendingMap.forEach(function(cancel){
      cancel();
    });
    pendingMap.clear();
  }
  ...
})
```
这里只能将`pendingMap`数组中的数据清空，还需要修改`src/utils/http/cancelToken.ts`中的`judgeRending`方法：
```

/**
 * 请求拦截中根据请求的可能类型，做不同的逻辑操作
 * @param {*} config 
 */
const judgeRending = (config) => {
  if(!cancelDuplicated) return;
  // 判断是否存在列表中有，但是取消请求队列中没有的请求，这种属于路由切换被清空，但pendingList在监听路由中无法清空，需在这里将被清空的请求队列，移除数组
  if(pendingList.length>0){
    for(var i=0;i<pendingList.length;i++){
      if(!pendingMap.has(pendingList[i].pendingKey)){
        pendingList.splice(i,1);
        i--;
      }
    }
  }
  let pendingType = pendingRequestType(config);
  console.log(pendingType,"请求类型");
  switch(pendingType){
    case "repeat":
      let cancelFuc;
      config.cancelToken = new Axios.CancelToken((cancel) => {
        cancelFuc = cancel;
      });
      let errorMsg = `${config.url} 请求被中断`;
      // 阻止当前请求
      cancelFuc(errorMsg);
      break;
    case "filter":
      removePending(config);
      addPending(config);
      break;
    default:
      addPending(config);
      break;
  }
}
```
## keep-alive页面状态缓存页面在路由切换时被取消所有未完成请求处理办法
以上虽然完成了`axios`取消重复请求和路由切换时，取消上个页面未完成请求，但这个在页面开启`keep-alive`的情况，取消未完成请求的状态被保存，再次回到该页面，请求未完成，会导致页面数据丢失，渲染失败，要解决这个问题，需要用到`vue`的生命周期函数`onActivated`，用于在`keep-alive`被激活时，执行一些事情，有了这个声明周期函数，只需要在其内重新请求数据即可。

这里可以做个测试，修改`mock/asyncRoutes.ts`文件，将新闻列表的`keepAlive`值改为`true`，让其可以被缓存状态，然后修改`src/views/News/Index/index.vue`文件：
```
import { reactive, onActivated } from 'vue';

onActivated(()=>{
  console.log("keepAlive被激活");
})
```
再修改一下`src/views/News/NewsDetail/index.vue`文件，也增加上`onActivated`生命周期函数，做个对比，新闻详情页未启用页面状态缓存，在标签栏切换到该页面时，是不会调用`onActivated`生命周期函数的：
```
import { reactive,onActivated } from 'vue';

onActivated(()=>{
  console.log("keepAlive被激活");
});
```
做完以上步骤，运行项目，分别打开新闻列表页，和一个新闻详情页，在标签栏来回切换，控制台能看到两个页面初次打开都未调用`onActivated`，标签栏切换到新闻列表页时，是调用了`onActivated`的，新闻详情页未调用`onActivated`。

要实现完整的路由页面切换，取消全部未完成请求，这里的`keep-alive`状态问题也是需要考虑的，所以不仅需要在页面初始化时，直接调用接口获取数据，还需要在`onActivated`时，也调用接口获取数据。