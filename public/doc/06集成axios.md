﻿# 集成 axios
```
yarn add axios
# 请求调用进度条
yarn add nprogress
yarn add @types/nprogress -D
# 对数据进行处理
yarn add qs
yarn add @types/qs -D
```
## 进度条工具函数封装相关文件
`src/utils/progress/index.ts`：
```
import NProgress from "nprogress";
import "nprogress/nprogress.css";

NProgress.configure({
  // 动画方式
  easing: "ease",
  // 递增进度条的速度
  speed: 500,
  // 是否显示加载ico
  showSpinner: true,
  // 自动递增间隔
  trickleSpeed: 200,
  // 初始化时的最小百分比
  minimum: 0.3
});

export default NProgress;
```
## axios封装相关文件
新建`src/utils/http/index.ts`文件：
```
import Axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import {
  RequestMethods,
  AxiosHttpResponse,
  AxiosHttpRequestConfig
} from "./types.d";
import qs from "qs";
import NProgress from "../progress";

// 详细配置可参考：www.axios-js.com/zh-cn/docs/#axios-request-config-1
const defaultConfig: AxiosRequestConfig = {
  baseURL: "",
  timeout: 10000,
  headers: {
    Accept: "application/json, text/plain, */*",
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest"
  },
  // get请求中数组格式的参数进行序列化
  paramsSerializer: params => qs.stringify(params, { indices: false })
};

class AxiosHttp {
  constructor(){
    this.httpInterceptorsRequest();
    this.httpInterceptorsResponse();
  }
  // 初始化配置对象
  private static initConfig: AxiosHttpRequestConfig = {};
  // 保存当前Axios实例对象
  private static axiosInstance: AxiosInstance = Axios.create(defaultConfig);
  // 设置请求前的回调
  private beforeRequestCallback: AxiosHttpRequestConfig["beforeRequestCallback"] = undefined;
  // 设置响应前的回调
  private beforeResponseCallback: AxiosHttpRequestConfig["beforeResponseCallback"] = undefined;

  private static transformConfigByMethod(
    params: any,
    config: AxiosHttpRequestConfig
  ): AxiosHttpRequestConfig {
    const { method } = config;
    const props = ["delete", "get", "head", "options"].includes(
      method!.toLocaleLowerCase()
    )
      ? "params"
      : "data";
    return {
      ...config,
      [props]: params
    };
  }
  // 请求拦截
  private httpInterceptorsRequest(): void {
    AxiosHttp.axiosInstance.interceptors.request.use(
      (config: AxiosHttpRequestConfig) => {
        const $config = config;
        // 开启进度条动画
        NProgress.start();
        // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
        if (typeof config.beforeRequestCallback === "function") {
          config.beforeRequestCallback($config);
          this.beforeRequestCallback = undefined;
          return $config;
        }
        // 判断初始化状态中有没有回调函数，没有的话
        if (AxiosHttp.initConfig.beforeRequestCallback) {
          AxiosHttp.initConfig.beforeRequestCallback($config);
          return $config;
        }
        return $config;
      },
      error => {
        return Promise.reject(error);
      }
    );
  }

  // 响应拦截
  private httpInterceptorsResponse(): void {
    const instance = AxiosHttp.axiosInstance;
    instance.interceptors.response.use(
      (response: AxiosHttpResponse) => {
        const $config = response.config;
        // 关闭进度条动画
        NProgress.done();
        // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
        if (typeof $config.beforeResponseCallback === "function") {
          $config.beforeResponseCallback(response);
          this.beforeResponseCallback = undefined;
          return response.data;
        }
        if (AxiosHttp.initConfig.beforeResponseCallback) {
          AxiosHttp.initConfig.beforeResponseCallback(response);
          return response.data;
        }
        return response.data;
      },
      (error) => {
        const $error = error;
        // 关闭进度条动画
        NProgress.done();
        // 所有的响应异常 区分来源为取消请求/非取消请求
        return Promise.reject($error);
      }
    );
  }

  // 通用请求工具函数
  public request<T>(
    method: RequestMethods,
    url: string,
    param?: AxiosRequestConfig,
    axiosConfig?: AxiosHttpRequestConfig
  ): Promise<T> {
    // post与get请求的参数需要用不同的key去接收，这里需要做判断，在请求中get的参数是params接收的，post的参数是data接收的
    const config = AxiosHttp.transformConfigByMethod(param, {
      method,
      url,
      ...axiosConfig
    } as AxiosHttpRequestConfig);
    // 单独处理自定义请求/响应回调
    if (axiosConfig?.beforeRequestCallback) {
      this.beforeRequestCallback = axiosConfig.beforeRequestCallback;
    }
    if (axiosConfig?.beforeResponseCallback) {
      this.beforeResponseCallback = axiosConfig.beforeResponseCallback;
    }
    // 单独处理自定义请求/响应回掉
    return new Promise((resolve, reject) => {
      AxiosHttp.axiosInstance.request(config).then((response:any) => {
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  }

  // 单独抽离的post工具函数
  public post<T>(
    url: string,
    params?: T,
    config?: AxiosHttpRequestConfig
  ): Promise<T> {
    return this.request<T>("post", url, params, config);
  }

  // 单独抽离的get工具函数
  public get<T>(
    url: string,
    params?: T,
    config?: AxiosHttpRequestConfig
  ): Promise<T> {
    return this.request<T>("get", url, params, config);
  }
}

export const http = new AxiosHttp();
```
再新建类型定义文件`src/utils/http/types.d.ts`：
```
import Axios, {
  AxiosRequestConfig,
  AxiosResponse,
  Method,
} from "axios";
// 定义请求类型的数据类型
export type RequestMethods = Extract<
  Method,
  "get" | "post" | "put" | "delete" | "patch" | "option" | "head"
>;
// 定义自定义回调中获取的数据的类型
export interface AxiosHttpResponse extends AxiosResponse {
  config: AxiosHttpRequestConfig;
}
// 定义自定义回调中请求的数据类型
export interface AxiosHttpRequestConfig extends AxiosRequestConfig {
  beforeRequestCallback?: (request: AxiosHttpRequestConfig) => void; // 请求发送之前
  beforeResponseCallback?: (response: AxiosHttpResponse) => void; // 相应返回之前
}
// 定义声明的AxiosHttp类的数据类型
export default class AxiosHttp {
  request<T>(
    method: RequestMethods,
    url: string,
    param?: AxiosRequestConfig,
    axiosConfig?: AxiosHttpRequestConfig
  ): Promise<T>;
  post<T>(
    url: string,
    params?: T,
    config?: AxiosHttpRequestConfig
  ): Promise<T>;
  get<T>(
    url: string,
    params?: T,
    config?: AxiosHttpRequestConfig
  ): Promise<T>;
}
```
基础版的`axios`集成到这里就算完成了，具体使用将在接下来的步骤中介绍。