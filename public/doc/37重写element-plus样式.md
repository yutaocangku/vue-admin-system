﻿# 重写element-plus样式
因为组件的引入是手动按需引入的，可能引入的顺序不对，导致部分样式变形，以及如果想对某些样式做修改，都需要对组件本身的样式做覆盖，可以新建一个专门用来重写`element-plus`组件样式的文件，在`src/style/index.scss`文件的最后引入，就能覆盖组件本身的样式，在`src/style`文件夹下新建`element-plus.scss`文件：
```
.el-input{
  --el-input-border-radius:var(--el-border-radius-small);
}
.el-input__prefix,.el-input__suffix{
  align-items: center;
}
.el-message__closeBtn{
  position: absolute;
  color: var(--el-message-close-icon-color);
}
```
修改`src/style/index.scss`文件：
```
.sass-error{}
@import './transition.scss';
@import './public.scss';
@import './layout.scss';
@import './element-plus.scss';
.demo {
  color: red;
}
```
这样`element-plus`组件的样式就可以被重写了，这里将`input`组件的圆角改变为`2px`，是通过改变变量实现，这里只是改变了`input`的圆角，其他组件的默认圆角引用的变量都还是`--el-border-radius-base`，其默认值是`4px`，如果想要改变所有组件的圆角为`2px`可以直接改变`:root`中的`--el-border-radius-base`变量，如：
```
:root{
  --el-border-radius-base:2px;
}
```
这样就是将所有引用该变量的组件都做了修改。
