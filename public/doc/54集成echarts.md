﻿# 集成echarts
安装依赖插件：
```
yarn add echarts
```
封装`echarts`插件中使用的类型定义需要依赖另一个插件：
```
yarn add vue-types
```

修改`types/global.d.ts`文件：
```
declare type Nullable<T> = T | null

declare type ElRef<T extends HTMLElement = HTMLDivElement> = Nullable<T>
```

修改`.eslintrc.js`文件，增加`global`配置，用来让全局的类型定义不报错：
```
  // 全局类型定义需要再这里配置，可以无视no-undef规则
  globals: {
    // global.d.ts
    Nullable: 'readonly',
    ElRef: 'readonly',
    Recordable: 'readonly',
    ImportMetaEnv: 'readonly',
    ViteEnv: 'readonly',
    ServerConfigs: 'readonly'
  },
```

新建`src/utils/propTypes.ts`文件：
```
import { createTypes, VueTypesInterface, VueTypeValidableDef } from 'vue-types'
import { CSSProperties } from 'vue'

// 自定义扩展vue-types
type PropTypes = VueTypesInterface & {
  readonly style: VueTypeValidableDef<CSSProperties>
}

const propTypes = createTypes({
  func: undefined,
  bool: undefined,
  string: undefined,
  number: undefined,
  object: undefined,
  integer: undefined
}) as PropTypes

// 需要自定义扩展的类型
// see: https://dwightjack.github.io/vue-types/advanced/extending-vue-types.html#the-extend-method
propTypes.extend([
  {
    name: 'style',
    getter: true,
    type: [String, Object],
    default: undefined
  }
])

export { propTypes }
```

修改`src/utils/index.ts`文件：
```

/**
 * 两个对象的深度合并
 */
export const deepMerge = (obj1, obj2) => {
  let key
  for (key in obj2) {
    // 如果target(也就是obj1[key])存在，且是对象的话再去调用deepMerge，否则就是obj1[key]里面没这个对象，需要与obj2[key]合并
    // 如果obj2[key]没有值或者值不是对象，此时直接替换obj1[key]
    obj1[key] = obj1[key] && obj1[key].toString() === '[object Object]' && obj2[key] && obj2[key].toString() === '[object Object]' ? deepMerge(obj1[key], obj2[key]) : (obj1[key] = obj2[key])
  }
  return obj1
}

// 正序排列
export function sortByAsc(i) {
  return function (a, b) {
    return a[i] - b[i]
  }
}

// 倒序排列
export function sortByDesc(i) {
  return function (a, b) {
    return b[i] - a[i]
  }
}
```
这里新增了对象深度合并的封装方法以及正序、倒序排序封装方法。

主题色又增加了两种，将主题色数据确定为10种，并增加了对应的国际化文案，确定为10种主题色主要是为了能在`echarts`中进行复用，`echarts`中数据的一组颜色默认是由10中色值组成。

`src/components/EChart/index.vue`文件是封装的`echarts`组件，可以进行复用

`src/hooks/charts`文件夹下，封装了40种较为常用的`echarts`图表参数配置，配合封装的`echarts`组件，可以直接复用展现图表。

`src/views/Plugins/Charts/index.vue`文件将封装的40种常用`echarts`图表进行了展示

`mock/echarts.ts`文件为展现图表提供数据支持

`src/api/echarts.ts`获取`echarts`的`mock`数据的`api`封装文件。

这里封装的`echarts`实现了主题色改变、明暗主题色改变以及国际化支持功能，具体使用方法可参考`src/views/Plugins/Charts/index.vue`文件中的使用方法。

到这里项目整体大方向上的功能就全部完成了，此时的项目是可以直接用来做脚手架使用的，当然项目这里的路由需要根据自己的项目进行配置，如果有不需要的功能，也可以单独剔除某种功能，只是会比较麻烦，集成的功能较多，逻辑交织就不可避免了，接下来项目的丰富，会根据路由页面进行一一实现，先将功能、组件、插件这些项目中较为常用的一些小功能实现，然后再去实现页面、首页、关于等作为复合页面的部分。