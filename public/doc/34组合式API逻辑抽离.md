﻿# 组合式API逻辑抽离
上一章为实现滚动条行为、滚动监听事件，为每个页面都添加了差不多相同的逻辑代码，这在项目庞大的时候会有大量的冗余代码，这里会有vue3的组合式API特性，将公共逻辑代码抽离，页面内只需引用即可节省代码量。

组合式API的特点是将特定功能相关的所有逻辑代码放到一起维护，在一个逻辑非常复杂的页面内，在vue2时，所有功能逻辑代码都是混在一起的，后期维护相当困难，而vue3则可以通过组合式API的特点，将特定功能相关代码抽离，并在页面内引用，这里可以将滚动条行为、滚动监听事件做为两个独立的功能，抽离出去，因为是多个页面都能使用，可以作为项目的公共逻辑。

在`src`文件夹下新建`hooks`文件夹，并在其内新建`scrollPosition.ts`文件作为滚动行为的逻辑文件，`scrollListener.ts`文件作为滚动监听事件的逻辑文件：

`scrollPosition.ts`文件：
```
import { onMounted,onActivated,computed } from 'vue'

const useScrollPosition = (store,route) => {
  // 获取保存的滚动位置数据
  const savedPosition = computed(()=>{
    return store.state.keepAlivePages.savedPosition;
  });
  // 获取当前页面的缓存状态是否被开启
  const isKeepAlive = route.meta.keepAlive;
  // 当前路由名
  const currentName = route.name;
  // 默认返回页面顶部
  const scrollToTop = () => {
    if(!isKeepAlive){
      document.querySelector(".app-root .el-scrollbar__wrap").scrollTo(0,0);
    }
  }
  // 页面滚动到记录的位置
  const scrollToPosition = () => {
    // 开启页面状态缓存的情况下，监听事件在此声明周期内开启
    if(isKeepAlive){
      console.log("组件被激活");
      savedPosition.value.forEach((item)=>{
        if(item.name === currentName){
          document.querySelector(".app-root .el-scrollbar__wrap").scrollTo(0,item.savedPosition);
        }
      });
    }
  }
  // 在组件挂载到页面后执行
  onMounted(scrollToTop);
  // keep-alive启用时，页面被激活时使用
  onActivated(scrollToPosition);
}
export default useScrollPosition;
```
`scrollListener.ts`文件：
```

import { onMounted,onActivated,onBeforeUnmount,onDeactivated } from 'vue'

const useScrollListener = (route) => {
  // 获取当前页面的缓存状态是否被开启
  const isKeepAlive = route.meta.keepAlive;
  // 非缓存页面开启监听
  const mountedScrollListener = () => {
    // 未开启页面状态缓存的情况下，监听事件在此声明周期内开启
    if(!isKeepAlive){
      console.log("组件被挂载");
      // 开启滚动监听事件
      window.addEventListener('scroll',handleScroll,true);
    }
  }
  // 非缓存页面移除监听
  const beforeUnmountScrollListener = () => {
    // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
    if(!isKeepAlive){
      console.log("组件即将被卸载");
      // 移除滚动监听事件
      window.removeEventListener('scroll', handleScroll,true);
    }
  }
  // 缓存页面开启监听
  const activatedScrollListener = () => {
    // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
    if(isKeepAlive){
      console.log("组件被激活");
      // 开启滚动监听事件
      window.addEventListener('scroll',handleScroll,true);
    }
  }
  // 缓存页面移除监听
  const deactivatedScrollListener = () => {
    // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
    if(isKeepAlive){
      console.log("组件已被切换");
      // 移除滚动监听事件
      window.removeEventListener('scroll', handleScroll,true);
    }
  }
  // 在组件挂载到页面后执行
  onMounted(mountedScrollListener);
  // 页面未启用keep-alive时，则需要在此生命周期函数内（组件被卸载之前执行）移除监听
  onBeforeUnmount(beforeUnmountScrollListener);
  // keep-alive启用时，页面被激活时使用
  onActivated(activatedScrollListener);
  // 页面启用keep-alive时，则需要在此生命周期函数内（组件切换，老组件消失的时候执行）移除监听事件
  onDeactivated(deactivatedScrollListener);
  // 滚动监听执行事件
  const handleScroll = () =>{
    var scrollTop = document.querySelector(".app-root .el-scrollbar__wrap").scrollTop;
    console.log(scrollTop,"公共滚动事件")
  }
}
export default useScrollListener;
```
功能抽离后，修改页面，引用抽离的功能，修改`src/views/Home/index.vue`文件：
```
<script setup lang="ts">
// This starter template is using Vue 3 <script setup> SFCs
// Check out https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup
import HelloWorld from '@/components/HelloWorld.vue';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
import { ref } from 'vue'
import useScrollPosition from '@/hooks/scrollPosition';
import useScrollListener from '@/hooks/scrollListener';
const store = useStore();
const route = useRoute();
const value1 = ref("");
// 滚动行为
useScrollPosition(store,route);

// 滚动监听事件
useScrollListener(route);

const jumpAnchor = ()=>{
  document.querySelector("#demo").scrollIntoView(true);
}
</script>
```
修改`src/views/News/Index/index.vue`文件：
```
<script setup lang="ts">
import {getNewsListData} from "@/api/news";
import { useRoute } from "vue-router";
import { reactive } from 'vue'
import { useStore } from 'vuex';
import useScrollPosition from '@/hooks/scrollPosition';
import useScrollListener from '@/hooks/scrollListener';
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

// 滚动监听事件
useScrollListener(route);

let initData = reactive([]);
function getInitData(){
  getNewsListData({config:{showLoading:false,cancelRepeatDisabled:false,mockEnable:true}}).then((res:any)=>{
    if(res.data&&res.data.length>0){
      initData.push(...res.data);
    }
  });
}
getInitData();
// setTimeout(()=>{
//   getInitData();
// },200);
// setTimeout(()=>{
//   getInitData();
// },400);
</script>
```
修改`src/views/System/UserManagement/index.vue`文件：
```
<script setup lang="ts">
import { useRoute } from "vue-router";
import { ref } from 'vue'
import { useStore } from 'vuex';
import useScrollPosition from '@/hooks/scrollPosition';
import useScrollListener from '@/hooks/scrollListener';
const store = useStore();
const value1 = ref("");
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

// 滚动监听事件
useScrollListener(route);

</script>
```
此时运行项目，打开相关页面，滚动行为、滚动位置保存，缓存状态保存、都依然没有问题，这样只需要在对应页面引入公共逻辑暴露的方法即可。也可以将页面自身的逻辑按功能进行抽离，可以将`src/views/News/Index/index.vue`文件中的获取列表数据抽离出去，在`src/views/News/Index`文件夹下新建`hooks`文件夹，并在其内新建`newsData.ts`文件：
```
import { reactive } from 'vue'
import {getNewsListData} from "@/api/news";
const useNewsData = () => {
  let initData = reactive([]);
  const getInitData = () => {
    getNewsListData({config:{showLoading:false,cancelRepeatDisabled:false,mockEnable:true}}).then((res:any)=>{
      if(res.data&&res.data.length>0){
        initData.push(...res.data);
      }
    });
  }
  return {
    initData,
    getInitData
  }
}
export default useNewsData;
```
修改`src/views/News/Index/index.vue`文件：
```
<script setup lang="ts">
import { useRoute } from "vue-router";
import { useStore } from 'vuex';
import useScrollPosition from '@/hooks/scrollPosition';
import useScrollListener from '@/hooks/scrollListener';
import useNewsData from './hooks/newsData';
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

// 滚动监听事件
useScrollListener(route);

// 页面数据渲染
let {initData,getInitData} = useNewsData();
getInitData();
</script>
```
运行项目，打开新闻列表页，数据是能正常渲染的。页面的所有逻辑都按功能进行抽离后，后期对页面进行维护将会非常方便，页面的条理也更为清晰。