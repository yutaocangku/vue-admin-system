﻿# clickOutSide
点击外部区域功能在很多场景下会用到，这里实现该功能主要通过`@vueuse/core`的`onClickOutside`，新建`src/components/ClickOutSide/index.vue`文件：
```
<script lang="ts" setup>
import { ref, onMounted } from 'vue'
import { onClickOutside } from '@vueuse/core'
const emit = defineEmits(['mounted', 'clickOutside'])
const wrap = ref<ElRef>(null)

onClickOutside(wrap, () => {
  emit('clickOutside')
})

onMounted(() => {
  emit('mounted')
})
</script>

<template>
  <div ref="wrap">
    <slot></slot>
  </div>
</template>

```

修改`src/views/Functions/ClickOutSide/index.vue`文件：
```
<script setup lang="ts" name="ClickOutSide">
import useVkConfig from '@/hooks/vkConfig'
const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型

const text = ref('点击查看变化')

const handleClickOutside = () => {
  text.value = '点击外部区域'
}

const innerClick = () => {
  text.value = '点击内部区域'
}
</script>

<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <p>可以点击色块内与色块外的任意地方查看变化</p>
      <ClickOutSide @clickOutside="handleClickOutside" class="flex justify-center">
        <div @click="innerClick" class="inner-box">
          {{ text }}
        </div>
      </ClickOutSide>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}

.el-card{
  display: flex;

  :deep(.el-card__body){
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    p{
      margin-bottom: var(--el-space);
    }
  }
}

.inner-box {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 300px;
  height: 300px;
  color: var(--el-color-white);
  font-size: 24px;
  background-color: var(--el-color-primary);
  border-radius: 10px;
}
</style>
```
这里展示了点击外部区域的用法