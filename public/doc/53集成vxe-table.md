﻿# 集成vxe-table
后台管理项目对表格功能的要求会比较高，`element-plus`的表格组件相对来说功能能满足简单项目的需求，但功能并不全面，且问题也会较多，表格方面可以额外引入`vxe-table`插件，用来专门面对表格的复杂使用场景。

## 安装依赖
```
yarn add xe-utils vxe-table@next
```
`vxe-table`依赖了`xe-utils`，该包中包含了大量的已实现的较常用的公共方法。

`vix-table`包中除了包含了表格相关组件，还有表单相关组件、弹窗组件等。暂时只需要表格相关的组件，所以这里采用按需加载的形式引入该包。

```
yarn add vite-plugin-style-import consola -D
```
按需加载需要借助以上插件，进行配置。

1. 修改`vite.config.ts`文件：
```
import { createStyleImportPlugin, VxeTableResolve } from 'vite-plugin-style-import'


createStyleImportPlugin({
  resolves: [VxeTableResolve()]
}),
```

2. 新建`src/plugins/vxeTable/index.ts`文件：
```
import 'xe-utils'
import { App } from 'vue'
import XEUtils from 'xe-utils'
import { i18n } from '@/plugins/i18n'
import { storageLocal } from '@/utils/storage'
import zh from 'vxe-table/lib/locale/lang/zh-CN'
import en from 'vxe-table/lib/locale/lang/en-US'

import {
  // 核心
  VXETable,
  // 表格功能
  Header,
  Footer,
  Icon,
  Filter,
  Edit,
  Menu,
  Export,
  Keyboard,
  Validator,

  // 可选组件
  Column,
  Colgroup,
  Grid,
  Tooltip,
  Toolbar,
  Pager,
  // Form,
  // FormItem,
  // FormGather,
  // Checkbox,
  // CheckboxGroup,
  // Radio,
  // RadioGroup,
  // RadioButton,
  // Switch,
  // Input,
  // Select,
  // Optgroup,
  // Option,
  // Textarea,
  // Button,
  // Modal,
  List,
  Pulldown,

  // 表格
  Table
} from 'vxe-table'

export const getTableSize = () => {
  const vkConfig = storageLocal.getItem('responsive-vkConfig')
  if (!vkConfig || (vkConfig && vkConfig.size === 'default')) {
    return 'medium'
  } else {
    if (vkConfig.size === 'large') {
      return null
    } else {
      return 'mini'
    }
  }
}
// 全局默认参数配置
VXETable.setup({
  size: getTableSize(),
  version: 0,
  zIndex: 1002,
  table: {
    // 自动监听父元素的变化去重新计算表格
    autoResize: true,
    // 鼠标移到行是否要高亮显示
    highlightHoverRow: true
  },
  input: {
    clearable: true
  },
  // 支持国际化
  i18n: (key, args) => {
    return unref(i18n.global.locale) === 'zh' ? XEUtils.toFormatString(XEUtils.get(zh, key), args) : XEUtils.toFormatString(XEUtils.get(en, key), args)
  },
  // 支持全局自动翻译
  translate(key) {
    // 只翻译指定key开头的键值
    const NAMESPACED = ['errors.', 'buttons.', 'moduleTitle.', 'placeholders.', 'successes.', 'text.', 'tooltips.', 'verifies.']
    if (key && NAMESPACED.findIndex((v) => key.includes(v)) !== -1) {
      return i18n.global.t.call(i18n.global.locale, key)
    }
    return key
  }
})

export const useTable = (app: App) => {
  // 表格功能
  app.use(Header)
  app.use(Footer)
  app.use(Icon)
  app.use(Filter)
  app.use(Edit)
  app.use(Menu)
  app.use(Export)
  app.use(Keyboard)
  app.use(Validator)

  // 可选组件
  app.use(Column)
  app.use(Colgroup)
  app.use(Grid)
  app.use(Tooltip)
  app.use(Toolbar)
  app.use(Pager)
  // app.use(Form)
  // app.use(FormItem)
  // app.use(FormGather)
  // app.use(Checkbox)
  // app.use(CheckboxGroup)
  // app.use(Radio)
  // app.use(RadioGroup)
  // app.use(RadioButton)
  // app.use(Switch)
  // app.use(Input)
  // app.use(Select)
  // app.use(Optgroup)
  // app.use(Option)
  // app.use(Textarea)
  // app.use(Button)
  // app.use(Modal)
  app.use(List)
  app.use(Pulldown)

  // 安装表格
  app.use(Table)

  // 给 vue 实例挂载内部对象，例如：
  // app.config.globalProperties.$XModal = VXETable.modal
  // app.config.globalProperties.$XPrint = VXETable.print
  // app.config.globalProperties.$XSaveFile = VXETable.saveFile
  // app.config.globalProperties.$XReadFile = VXETable.readFile
}
```
该文件引入了除表单组件外的所有其它组件，并在该文件内配置了表格的全局参数、实现了表格尺寸与element-plus的统一、国际化配置相关功能。

3. 修改`src/main.ts`文件：
```
import { useTable } from './plugins/vxeTable'


useTable(app)
```
在入口文件内引入`vxe-table`插件的配置文件。

4. 修改`src/views/Components/VxeTable/Basic/index.vue`文件：
```
<script setup lang="ts" name="VtBasic">
import useScrollPosition from '@/hooks/scrollPosition'
import useVkConfig from '@/hooks/vkConfig'
import { VxeColumnPropTypes } from 'vxe-table'
const store = useStore()
const route = useRoute()
const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型

// 滚动行为
useScrollPosition(route, store, vkConfig)

const demo1 = reactive({
  loading: false,
  tableData: [] as any[],
  sexList: [
    {
      label: '女',
      value: '0'
    },
    {
      label: '男',
      value: '1'
    }
  ]
})

const formatterSex: VxeColumnPropTypes.Formatter = ({ cellValue }) => {
  const item = demo1.sexList.find(item => item.value === cellValue)
  return item ? item.label : ''
}

const filterAgeMethod: VxeColumnPropTypes.FilterMethod = ({ value, row }) => {
  return row.age >= value
}
onMounted(() => {
  demo1.loading = true
  setTimeout(() => {
    demo1.tableData = [
      { id: 10001, name: 'Test1', role: 'Develop', sex: '0', age: 28, address: 'test abc' },
      { id: 10002, name: 'Test2', role: 'Test', sex: '1', age: 22, address: 'Guangzhou' },
      { id: 10003, name: 'Test3', role: 'PM', sex: '0', age: 32, address: 'Shanghai' },
      { id: 10004, name: 'Test4', role: 'Designer', sex: '1', age: 23, address: 'test abc' },
      { id: 10005, name: 'Test5', role: 'Develop', sex: '1', age: 30, address: 'Shanghai' },
      { id: 10006, name: 'Test6', role: 'Designer', sex: '1', age: 21, address: 'test abc' },
      { id: 10007, name: 'Test7', role: 'Test', sex: '0', age: 29, address: 'test abc' },
      { id: 10008, name: 'Test8', role: 'Develop', sex: '0', age: 35, address: 'test abc' },
      { id: 10009, name: 'Test9', role: 'Test', sex: '1', age: 21, address: 'test abc' },
      { id: 10010, name: 'Test10', role: 'Develop', sex: '0', age: 28, address: 'test abc' },
      { id: 10011, name: 'Test11', role: 'Test', sex: '0', age: 29, address: 'test abc' },
      { id: 10012, name: 'Test12', role: 'Develop', sex: '1', age: 27, address: 'test abc' },
      { id: 10013, name: 'Test13', role: 'Test', sex: '0', age: 24, address: 'test abc' },
      { id: 10014, name: 'Test14', role: 'Develop', sex: '1', age: 34, address: 'test abc' },
      { id: 10015, name: 'Test15', role: 'Test', sex: '1', age: 21, address: 'test abc' },
      { id: 10016, name: 'Test16', role: 'Develop', sex: '0', age: 20, address: 'test abc' },
      { id: 10017, name: 'Test17', role: 'Test', sex: '1', age: 31, address: 'test abc' },
      { id: 10018, name: 'Test18', role: 'Develop', sex: '0', age: 32, address: 'test abc' },
      { id: 10019, name: 'Test19', role: 'Test', sex: '1', age: 37, address: 'test abc' },
      { id: 10020, name: 'Test20', role: 'Develop', sex: '1', age: 41, address: 'test abc' }
    ]
    demo1.loading = false
  }, 500)
})
</script>

<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <template #header>
        <div class="card-header">
          <span>基础用法</span>
        </div>
      </template>
      <vxe-table border stripe :loading="demo1.loading" :column-config="{resizable: true}" :row-config="{isHover: true}" :checkbox-config="{labelField: 'id', highlight: true, range: true}" :data="demo1.tableData">
        <vxe-column type="seq" width="60"></vxe-column>
        <vxe-column type="checkbox" title="ID" width="140"></vxe-column>
        <vxe-column field="name" title="Name" sortable></vxe-column>
        <vxe-column field="sex" title="Sex" :filters="demo1.sexList" :filter-multiple="false" :formatter="formatterSex"></vxe-column>
        <vxe-column field="age" title="Age" sortable :filters="[{label: '大于16岁', value: 16}, {label: '大于26岁', value: 26}, {label: '大于30岁', value: 30}]" :filter-method="filterAgeMethod"></vxe-column>
        <vxe-column field="address" title="Address" show-overflow></vxe-column>
      </vxe-table>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}
</style>
```
这里对`vxe-table`表格的基础使用进行展示。

5. 修改`src/layout/components/headerPanel.vue`文件：
```
import VXETable from 'vxe-table'
import { getTableSize } from '@/plugins/vxeTable'
import useRefreshPage from '@/hooks/refreshPage'

const { refreshPage } = useRefreshPage(route, router)


// 切换组件尺寸大小
const sizeChange = (size) => {
  vkConfig.value.size = size
  settingChangeHandle()
  VXETable.setup({
    size: getTableSize()
  })
  refreshPage()
}
```
这里主要是组件大小切换时，需要对表格的全局参数进行修改，全局参数修改后，需要刷新页面，参数才能重新运用与页面内的表格。

6. 新建`src/style/windicss.scss`文件：
```
*,
::before,
::after {
  box-sizing: border-box;
  border-color: inherit;
  border-style: solid;
  border-width: 0;
}
```
对`windicss`的默认样式进行重写。

7. 新建`src/style/vxe-table.scss`文件：
```
/* stylelint-disable scss/percent-placeholder-pattern */
@import "vxe-table/styles/variable";
@import "vxe-table/styles/modules";

.vue-kevin-admin {

  .vxe-table .vxe-sort--asc-btn.sort--active,
  .vxe-table .vxe-sort--desc-btn.sort--active,
  .is--filter-active .vxe-cell--filter .vxe-filter--btn {
    color: var(--el-color-primary);
  }

  .vxe-table--render-default.border--full .vxe-header--column.col--last,
  .vxe-table--render-default.border--full .vxe-body--column.col--last,
  .vxe-table--render-default.border--full .vxe-footer--column.col--last {
    background-image: linear-gradient(#e8eaec, #e8eaec);
    background-repeat: no-repeat;
    background-position: right bottom;
    background-size: 100% 1px;
  }

  .vxe-table--render-default .vxe-body--row.row--checked {
    background-color: var(--el-color-primary-light-9);
  }

  .vxe-primary-color {
    color: var(--el-color-primary);
  }

  .vxe-success-color {
    color: var(--el-color-success);
  }

  .vxe-info-color {
    color: var(--el-color-info);
  }

  .vxe-warning-color {
    color: var(--el-color-warning);
  }

  .vxe-danger-color {
    color: var(--el-color-danger);
  }

  .vxe-table--render-default.size--medium .vxe-cell--checkbox .vxe-checkbox--icon {
    font-size: 14px;
  }

  .vxe-custom--option .vxe-checkbox--icon::before,
  .vxe-table--render-default .vxe-cell--checkbox .vxe-checkbox--icon::before,
  .vxe-table--filter-option .vxe-checkbox--icon::before,
  .vxe-export--panel-column-option .vxe-checkbox--icon::before {
    border: 1px solid #dcdfe6;
  }

  .vxe-custom--option:not(.is--disabled):hover .vxe-checkbox--icon::before,
  .vxe-table--render-default .vxe-cell--checkbox:not(.is--disabled):hover .vxe-checkbox--icon::before,
  .vxe-table--filter-option:not(.is--disabled):hover .vxe-checkbox--icon::before,
  .vxe-export--panel-column-option:not(.is--disabled):hover .vxe-checkbox--icon::before {
    border-color: var(--el-color-primary);
  }

  .is--checked.vxe-custom--option .vxe-checkbox--icon::before,
  .vxe-table--render-default .is--checked.vxe-cell--checkbox .vxe-checkbox--icon::before,
  .is--checked.vxe-table--filter-option .vxe-checkbox--icon::before,
  .is--checked.vxe-export--panel-column-option .vxe-checkbox--icon::before,
  .is--indeterminate.vxe-custom--option .vxe-checkbox--icon::before,
  .vxe-table--render-default .is--indeterminate.vxe-cell--checkbox .vxe-checkbox--icon::before,
  .is--indeterminate.vxe-table--filter-option .vxe-checkbox--icon::before,
  .is--indeterminate.vxe-export--panel-column-option .vxe-checkbox--icon::before {
    background-color: var(--el-color-primary);
    border-color: var(--el-color-primary);
  }

  .vxe-custom--option .vxe-checkbox--indeterminate-icon::after,
  .vxe-table--render-default .vxe-cell--checkbox .vxe-checkbox--indeterminate-icon::after,
  .vxe-table--filter-option .vxe-checkbox--indeterminate-icon::after,
  .vxe-export--panel-column-option .vxe-checkbox--indeterminate-icon::after {
    height: 1px;
  }

  .vxe-custom--option .vxe-checkbox--checked-icon::after,
  .vxe-table--render-default .vxe-cell--checkbox .vxe-checkbox--checked-icon::after,
  .vxe-table--filter-option .vxe-checkbox--checked-icon::after,
  .vxe-export--panel-column-option .vxe-checkbox--checked-icon::after {
    border: 1px solid #ffffff;
    border-top: 0;
    border-left: 0;
    transform: translate(-45%, -60%) rotate(45deg);
  }

  .is--checked.vxe-custom--option,
  .vxe-table--render-default .is--checked.vxe-cell--checkbox,
  .is--checked.vxe-table--filter-option,
  .is--checked.vxe-export--panel-column-option,
  .is--indeterminate.vxe-custom--option,
  .vxe-table--render-default .is--indeterminate.vxe-cell--checkbox,
  .is--indeterminate.vxe-table--filter-option,
  .is--indeterminate.vxe-export--panel-column-option {
    color: var(--el-color-primary);
  }

  .is--filter-active {
    .vxe-cell--filter {
      .vxe-filter--btn {
        color: var(--el-color-primary);
      }
    }
  }

  .vxe-table--filter-wrapper {
    .vxe-table--filter-header,
    .vxe-table--filter-body {
      &>li {
        &.is--checked {
          color: var(--el-color-primary);
        }
      }
    }

    .vxe-table--filter-footer {
      button {
        &:hover {
          color: var(--el-color-primary);
        }
      }
    }
  }

  .vxe-table--render-default .vxe-cell--radio .vxe-radio--icon,
  .vxe-table--render-default .vxe-cell--checkbox .vxe-checkbox--icon,
  .vxe-table--filter-option .vxe-cell--radio .vxe-radio--icon,
  .vxe-table--filter-option .vxe-checkbox--icon {
    top: 50%;
    transform: translate(0, -50%);
  }
}
```
该文件主要用来重新表格的主题样式，目前`vxe-table`插件的样式控制是通过`scss`变量实现的，要实现其主题随项目主题改变，就需要对样式进行重写，目前只重写了基础表格用到的元素涉及到的样式主题修改。该插件已经在计划将`scss`变量改为`css`变量，这个可以在其实现`css`变量后，直接修改`css`变量值，实现主题的搭配会更方便一些，目前只能通过较为麻烦的重写样式的方式实现主题的动态切换。

8. 修改`src/style/index.scss`文件：
```
@use './windicss';
@use './vxe-table.scss';
```
在入口样式文件中引入新建的两个样式文件。

以上配置完成后，`vxe-table`插件的集成就完成了。