﻿# 集成 Mock
```
yarn add mockjs
yarn add vite-plugin-mock @types/mockjs -D
```
这里如果在生产环境也需要用到`mock`模拟数据，需要将其添加到项目依赖中，如果不需要，则添加到开发依赖中即可。

修改`vite.config.ts`文件：
```
import { resolve } from "path";
import { UserConfigExport, ConfigEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import ElementPlus from "unplugin-element-plus/vite";
import { viteMockServe } from "vite-plugin-mock";

// 路径查找
const pathResolve = (dir: string): string => {
  return resolve(__dirname, dir);
};

// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfigExport =>{
  const prodMock = true;
  return {
    plugins: [
      vue(),
      ElementPlus({}),
      viteMockServe({
        mockPath: "./mock", // 模拟接口api文件存放的文件夹
        watchFiles: true, // 将监视文件夹中的文件更改。 并实时同步到请求结果
        localEnabled: command === "serve", // 设置是否启用本地 xxx.ts 文件，不要在生产环境中打开它.设置为 false 将禁用 mock 功能
        prodEnabled: command !== "serve" && prodMock, // 设置生产环境是否启用 mock 功能
        injectCode: `
          import { setupProdMockServer } from './mockProdServer';
          setupProdMockServer();
        `, // 如果生产环境开启了mock功能，该代码会注入到injectFile对应的文件的底部，默认为main.{ts,js}
        injectFile:resolve(process.cwd(),'src/main.{ts,js}'),
        logger: true
      }),
    ],
    resolve: {
      alias: {
        '@': pathResolve('src'),
      }
    },
    css: {
      // css预处理器
      preprocessorOptions: {
        scss: {
          // 引入 var.scss 这样就可以在全局中使用 var.scss中预定义的变量了
          // 给导入的路径最后加上 ; 
          additionalData: `@import "@/style/var.scss";`
        }
      }
    },
  }
}
```
此时开发环境的`mock`配置就已经完成了，还需要配置生产环境获取`mock`数据的配置，在`src`文件夹下，新建`mockProdServer.ts`文件：
```
import { createProdMockServer } from "vite-plugin-mock/es/createProdMockServer";

export const mockModules = [];

export function setupProdMockServer() {
  createProdMockServer(mockModules);
}
```
这里先把框架代码搭建好，配合`vite.config.ts`中配置的`injectCode`中要执行的即是此处被暴露出去的方法，方法中传入`mockModules`将是之后需要用到的所有模拟后端数据接口的集合。

在项目根目录新建`mock`文件夹，用来在接下来存放模拟的后端接口文件，到此`mock`相关配置准备工作就算完成了，接下来，需要先集成`axios`用来调用后端接口，请求数据。