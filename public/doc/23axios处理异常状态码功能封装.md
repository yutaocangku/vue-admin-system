﻿# 异常状态码处理功能封装
目前的代码请求终端，是被主动请求终端，但在控制台，确实一个报错，其实它并不是一个报错，而请求异常的各种状态码对应的问题，也需要集中进行处理，这里可以封装成一个方法，其他地方进行调用，在`src/utils/http`文件夹下新建`errorCode.ts`文件：
```
import Axios from "axios";
import { ElMessageBox } from 'element-plus';

/**
 * 处理异常
 * @param {*} error 
 */
 const httpErrorStatusHandler = (error) => {
  let message = '';
  if (error && error.response) {
    switch(error.response.status) {
      case 302: message = '接口重定向了！';break;
      case 400: message = '参数不正确！';break;
      case 401: message = '您未登录，或者登录已经超时，请先登录！';break;
      case 403: message = '您没有权限操作！'; break;
      case 404: message = `请求地址出错: ${error.response.config.url}`; break; // 在正确域名下
      case 408: message = '请求超时！'; break;
      case 409: message = '系统已存在相同数据！'; break;
      case 500: message = '服务器内部错误！'; break;
      case 501: message = '服务未实现！'; break;
      case 502: message = '网关错误！'; break;
      case 503: message = '服务不可用！'; break;
      case 504: message = '服务暂时无法访问，请稍后再试！'; break;
      case 505: message = 'HTTP版本不受支持！'; break;
      default: message = '异常问题，请联系管理员！'; break
    }
  }
  if (error.message.includes('timeout')) message = '网络请求超时！';
  if (error.message.includes('Network')) message = window.navigator.onLine ? '服务端异常！' : '您断网了！';
  ElMessageBox.alert(
    message,
    '温馨提示',
    {
      confirmButtonText: '好的',
      type: 'warning'
    }
  )
}
export {
  httpErrorStatusHandler,
}
```
修改`src/utils/http/index.ts`文件：
```
import { httpErrorStatusHandler } from './errorCode';


  // 请求拦截
  private httpInterceptorsRequest(): void {
    AxiosHttp.axiosInstance.interceptors.request.use(
      (config: AxiosHttpRequestConfig) => {
        const $config = config;
        console.log($config.config,"请求拦截");
        // 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
        if(!$config.config||($config.config&&!$config.config.cancelRepeatDisabled)){
          judgeRending($config);
        }
        if($config.config.showLoading){
          startLoading($config.config.loadingStyle);
        }
        // 开启进度条动画
        NProgress.start();
        // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
        if (typeof config.beforeRequestCallback === "function") {
          config.beforeRequestCallback($config);
          this.beforeRequestCallback = undefined;
          return $config;
        }
        // 判断初始化状态中有没有回调函数，没有的话
        if (AxiosHttp.initConfig.beforeRequestCallback) {
          AxiosHttp.initConfig.beforeRequestCallback($config);
          return $config;
        }
        // 确保config.url永远不会是undefined，增加断言
        if(!config.url){
          config.url = ""
        }
        // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
        if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
          return $config
        }
        // 回传token
        const token = getToken();
        // 判断token是否存在
        if (token) {
          const data = JSON.parse(token);
          // 确保config.headers永远不会是undefined，增加断言
          if(!config.headers){
            config.headers = {}
          }
          config.headers["Authorization"] = "Bearer " + data.accessToken;
          return $config;
        } else {
          return $config;
        }
      },
      error => {
        // 当前请求出错，当前请求加1的loading也需要减掉
        if(error.config.config.showLoading){
          endLoading();
        }
        return Promise.reject(error);
      }
    );
  }

  // 响应拦截
  private httpInterceptorsResponse(): void {
    const instance = AxiosHttp.axiosInstance;
    instance.interceptors.response.use(
      (response: AxiosHttpResponse) => {
        console.log(response,"请求响应数据");
        const $config = response.config;
        // 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
        if(!$config.config||($config.config&&!$config.config.cancelRepeatDisabled)){
          // 增加延时，相同请求再完成后，不得在短时间内重复请求
          setTimeout(() => {
            removePending($config);
          }, 500);
        }
        // 关闭进度条动画
        NProgress.done();
        if(response.data.code==200){
          // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
          if (typeof $config.beforeResponseCallback === "function") {
            $config.beforeResponseCallback(response);
            this.beforeResponseCallback = undefined;
            return response.data;
          }
          if (AxiosHttp.initConfig.beforeResponseCallback) {
            AxiosHttp.initConfig.beforeResponseCallback(response);
            return response.data;
          }
        }else{
          if(response.data.code==201002){
            errorMessage("登录已超时，请重新登录！");
            // 清除cookie中的token
            removeToken();
            // 清除缓存中的用户信息
            storageSession.removeItem("userInfo");
            router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
          }
          if(response.data.code==201004){
            const token = getToken();
            if(token){
              const data = JSON.parse(token);
              if(!isRefreshing){
                isRefreshing = true;
                return refreshToken({refreshToken:data.refreshToken}).then((res:any)=>{
                  if(res.status){
                    setToken(res.data);
                    // 确保config.headers永远不会是undefined，增加断言
                    if(!$config.headers){
                      $config.headers = {}
                    }
                    $config.headers['Authorization'] = "Bearer " + res.data.accessToken;
                    $config.baseURL = ''
                    // 已经刷新了token，将所有队列中的请求进行重试
                    requests.forEach((callback:any) => callback(token))
                    requests = [];
                    return instance($config);
                  }
                }).catch((res:any)=>{
                  // 清除cookie中的token
                  removeToken();
                  // 清除缓存中的用户信息
                  storageSession.removeItem("userInfo");
                  router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
                }).finally(()=>{
                  isRefreshing = false;
                });
              }else{
                // 正在刷新token，将返回一个未执行resolve的promise
                return new Promise((resolve) => {
                  // 将resolve放进队列，用一个函数形式来保存，等token刷新后直接执行
                  requests.push((accessToken:string) => {
                    // 响应以后，请求接口的$config.url已经包含了baseURL部分，这里需要将baseURL清空，防止再次请求时，再次组装，导致url错误
                    $config.baseURL = '';
                    // 确保config.headers永远不会是undefined，增加断言
                    if(!$config.headers){
                      $config.headers = {}
                    }
                    $config.headers['Authorization'] = "Bearer " + accessToken;
                    resolve(instance($config));
                  })
                })
              }
            }
          }
        }
        if(response.config.config.showLoading){
          endLoading();
        }
        return response.data;
      },
      (error) => {
        if(Axios.isCancel(error)){
          console.log('请求的重复请求：' + error.message); 
          return false;
        }else{
          httpErrorStatusHandler(error);
          const $error = error;
          // 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
          if($error.config&&(!$error.config.config||($error.config.config&&!$error.config.config.cancelRepeatDisabled))){
            // 增加延时，相同请求再完成后，不得在短时间内重复请求
            setTimeout(() => {
              removePending($error.config);
            }, 500);
          }
          if($error.config&&(!$error.config.config||($error.config.config&&$error.config.config.showLoading))){
            endLoading();
          }
          // 关闭进度条动画
          NProgress.done();
          // 所有的响应异常 区分来源为取消请求/非取消请求
          return Promise.reject($error);
        }
      }
    );
  }
```