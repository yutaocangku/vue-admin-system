﻿# markdown编辑器
安装相关依赖：
```
yarn add vditor
```

## 封装组件
新建`src/components/Markdown/index.vue`文件：
```
<script setup lang="ts">
import type { Ref } from 'vue'
import { unref } from 'vue'
import Vditor from 'vditor'
import 'vditor/dist/index.css'
import { onMountedOrActivated } from '@/hooks/onMountedOrActivated'
import useVkConfig from '@/hooks/vkConfig'
import { loadEnv } from '^/index'
const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型
const { VITE_BASE_API_PATH } = loadEnv()

const getTheme = (
    isLight: boolean,
    themeMode: 'default' | 'content' | 'code' = 'default',
  ) => {
    const isDark = !isLight;
    switch (themeMode) {
      case 'default':
        return isDark ? 'dark' : 'classic';
      case 'content':
        return isDark ? 'dark' : 'light';
      case 'code':
        return isDark ? 'dracula' : 'github';
    }
  }

type Lang = 'zh_CN' | 'en_US' | undefined
const props = defineProps({
  height: { type: Number, default: 360 },
  value: { type: String, default: '' }
})
const emits = defineEmits(['change', 'get', 'update:value'])
const wrapRef = ref<ElRef>(null)
const vditorRef = ref(null) as Ref<Nullable<Vditor>>
const initedRef = ref(false)
const valueRef = ref(props.value || '')
watch(
  [() => vkConfig.value.isLight, () => initedRef.value],
  ([val, inited]) => {
    if (!inited) {
      return
    }
    instance.getVditor()?.setTheme(getTheme(val) as any, getTheme(val, 'content'), getTheme(val, 'code'));
  },
  {
    immediate: true,
    flush: 'post'
  }
)
watch(
  () => vkConfig.value.locale,
  () => {
    destroy()
    init()
  }
)

watch(
  () => props.value,
  (v) => {
    if (v !== valueRef.value) {
      instance.getVditor()?.setValue(v)
    }
    valueRef.value = v
  }
)

const getCurrentLang = computed((): 'zh_CN' | 'en_US' => {
  let lang: Lang
  switch (unref(vkConfig.value.locale)) {
    case 'en':
      lang = 'en_US'
      break
    default:
      lang = 'zh_CN'
  }
  return lang
})
function init() {
  const wrapEl = unref(wrapRef) as HTMLElement
  if (!wrapEl) return
  const bindValue = { ...props }
  const insEditor = new Vditor(wrapEl, {
    theme: getTheme(vkConfig.value.isLight) as any,
    lang: unref(getCurrentLang),
    mode: 'sv',
    minHeight: 500,
    fullscreen: {
      index: 2048
    },
    preview: {
      theme: {
        // 设置内容主题
        current: getTheme(vkConfig.value.isLight, 'content'),
      },
      actions: [],
      hljs: {
        style: getTheme(vkConfig.value.isLight, 'code'),
        lineNumber: true
      }
    },
    counter: {
      enable: true
    },
    icon: 'ant',
    toolbar: [
      'undo',
      'redo',
      '|',
      'headings',
      'bold',
      'italic',
      'strike',
      'link',
      '|',
      'list',
      'ordered-list',
      'check',
      'outdent',
      'indent',
      '|',
      'quote',
      'line',
      'code',
      'inline-code',
      'insert-before',
      'insert-after',
      'emoji',
      '|',
      'upload',
      'table',
      '|',
      'fullscreen',
      'edit-mode',
      'export',
      'preview',
      'help'
      // {
      //   name: 'more',
      //   toolbar: [
      //     // 'both',
      //     // 'code-theme',
      //     // 'content-theme',
      //     'export',
      //     'outline',
      //     'preview',
      //     // 'devtools',
      //     // 'info',
      //     'help'
      //   ]
      // }
    ],
    input: (v) => {
      valueRef.value = v
      emits('update:value', v)
      emits('change', v)
    },
    after: () => {
      nextTick(() => {
        insEditor.setValue(valueRef.value)
        vditorRef.value = insEditor
        initedRef.value = true
        emits('get', instance)
      })
    },
    upload: {
      accept: "image/*",
      multiple: false,
      filename(name) {
          return name
              .replace(/[^(a-zA-Z0-9\u4e00-\u9fa5\.)]/g, "")
              .replace(/[\?\\/:|<>\*\[\]\(\)\$%\{\}@~]/g, "")
              .replace("/\\s/g", "");
      },
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      max: 2 * 1024 * 1024, // 上传图片的大小
      format: (files, result) =>{
        const uploadObj = JSON.parse(result)
        console.log(uploadObj)
        const fileName = uploadObj.data.name
        if (uploadObj.code === 200) {
          return JSON.stringify({
            msg: uploadObj.info,
            code: uploadObj.code,
            data: {
              errFiles: [],
              succMap: {
                [fileName]: uploadObj.data.url
              }
            }
          })
        } else {
          console.log(uploadObj.info + '上传失败了')
        }
      },
      url: VITE_BASE_API_PATH + '/uploadFile',
      linkToImgUrl: VITE_BASE_API_PATH + '/uploadFile',
      // 粘贴图片回显处理，如果有图片加了防盗链，则让后台代理替换成自己的图片
      linkToImgFormat (files) {
        const resData = JSON.parse(files)
        const code = resData.code
        const msg = resData.info
        const data = resData.data
        // 上传图片请求状态
        if (code === 200) {
          const succ = {}
          succ[data.name] = data.url
          // 图片回显
          return JSON.stringify({
            msg,
            code,
            data: {
              errFiles: [],
              succMap: succ
            }
          })
        } else {
          console.log(msg + '上传失败了')
        }
      },
    },
    blur: () => {
      // unref(vditorRef)?.setValue(props.value);
    },
    ...bindValue,
    cache: {
      enable: false
    }
  })
}

const instance = {
  getVditor: (): Vditor => vditorRef.value!
}

function destroy() {
  const vditorInstance = unref(vditorRef)
  if (!vditorInstance) return
  try {
    vditorInstance?.destroy?.()
  } catch (error) {
    console.log(error)
  }
  vditorRef.value = null
  initedRef.value = false
}

onMountedOrActivated(init)

onBeforeUnmount(destroy)
onDeactivated(destroy)

</script>

<template>
  <div ref="wrapRef"></div>
</template>
```
该文件封装了`markdown`编辑器组件，可方便项目使用

新建`src/components/Markdown/markdownViewer.vue`文件：
```

<script lang="ts" setup>
  import { Ref } from 'vue';
  import VditorPreview from 'vditor/dist/method.min'
  import { onMountedOrActivated } from '@/hooks/onMountedOrActivated'
  import useVkConfig from '@/hooks/vkConfig'
  const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型

  const viewerRef = ref<ElRef>(null);
  const vditorPreviewRef = ref(null) as Ref<Nullable<VditorPreview>>;
  const props = defineProps({
    value: { type: String },
    class: { type: String }
  })
  const getTheme = (
    isLight: boolean,
    themeMode: 'default' | 'content' | 'code' = 'default',
  ) => {
    const isDark = !isLight;
    switch (themeMode) {
      case 'default':
        return isDark ? 'dark' : 'classic';
      case 'content':
        return isDark ? 'dark' : 'light';
      case 'code':
        return isDark ? 'dracula' : 'github';
    }
  }
  function init() {
    const viewerEl = unref(viewerRef) as HTMLElement;
    vditorPreviewRef.value = VditorPreview.preview(viewerEl, props.value, {
      mode: getTheme(vkConfig.value.isLight, 'content'),
      theme: {
        // 设置内容主题
        current: getTheme(vkConfig.value.isLight, 'content'),
      },
      hljs: {
        // 设置代码块主题
        style: getTheme(vkConfig.value.isLight, 'code'),
      },
    });
  }
  watch(
    () => vkConfig.value.isLight,
    (val) => {
      VditorPreview.setContentTheme(getTheme(val, 'content'));
      VditorPreview.setCodeTheme(getTheme(val, 'code'));
      init();
    },
  );

  watch(
    () => props.value,
    (v, oldValue) => {
      v !== oldValue && init();
    },
  );

  function destroy() {
    const vditorInstance = unref(vditorPreviewRef);
    if (!vditorInstance) return;
    try {
      vditorInstance?.destroy?.();
    } catch (error) {}
    vditorPreviewRef.value = null;
  }

  onMountedOrActivated(init);

  onBeforeUnmount(destroy);
  onDeactivated(destroy);
</script>

<template>
  <div ref="viewerRef" id="markdownViewer" :class="$props.class"></div>
</template>
```
该文件封装了`markdown`的预览视图，可直接调用用来展示`markdown`编辑的内容。

新建`src/style/vditor.scss`文件：
```
.vue-kevin-admin {
  .vditor {
    --border-color: var(--el-border-color-light);
    --second-color: var(--el-text-color-disabled);
    --panel-background-color: var(--el-fill-color-blank);
    --panel-shadow: var(--el-box-shadow);
    --toolbar-background-color: var(--el-fill-color-light);
    --toolbar-icon-color: var(--el-text-color-primary);
    --toolbar-icon-hover-color: var(--el-color-primary);
    --toolbar-height: 35px;
    --toolbar-divider-margin-top: 8px;
    --textarea-background-color: var(--el-fill-color-blank);
    --textarea-text-color: var(--el-text-color-primary);
    --resize-icon-color: var(--toolbar-icon-color);
    --resize-background-color: var(--toolbar-background-color);
    --resize-hover-icon-color: var(--panel-background-color);
    --resize-hover-background-color: var(--toolbar-icon-hover-color);
    --count-background-color: rgb(27 31 35 / 5%);
    --heading-border-color: var(--el-border-color-light);
    --blockquote-color: #6a737d;
    --ir-heading-color: #660e7a;
    --ir-title-color: #808080;
    --ir-bi-color: #0033b3;
    --ir-link-color: #008000;
    --ir-bracket-color: #0000ff;
    --ir-paren-color: #008000;

    border-radius: 0;
  }

  .hljs {
    color: var(--el-text-color-primary);
    background-color: var(--el-fill-color-light);
  }

  .hljs-keyword,
  .hljs-selector-tag,
  .hljs-subst {
    color: var(--el-text-color-placeholder);
  }

  .vditor-ir pre.vditor-reset,.vditor-wysiwyg pre.vditor-reset{
    padding: 10px 40px !important;
  }

  .vditor-reset {
    width: 100%;
    max-width: 100% !important;
    padding: 10px 20px !important;
    color: var(--el-text-color-primary);

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      color: var(--el-text-color-primary);
    }
  }

  .vditor-reset>table {
    border: none;
  }

  .vditor-reset h1,
  .vditor-reset h2 {
    padding-bottom: 0.3em;
    border-bottom: 1px solid var(--el-border-color-light);
  }

  .vditor-reset hr {
    background-color: var(--el-border-color-light);
  }

  .vditor-reset blockquote {
    color: #6a737d;
    border-left: 0.25em solid var(--el-border-color-light);
  }

  .vditor-reset iframe {
    border: 1px solid var(--el-border-color-light);
  }

  .vditor-reset table tr {
    background-color: var(--el-fill-color-blank);
    border-top: 1px solid var(--el-border-color-light);
  }

  .vditor-reset table td,
  .vditor-reset table th {
    border: 1px solid var(--el-border-color-light);
  }

  .vditor-reset table tbody tr:nth-child(2n) {
    background-color: var(--el-fill-color-blank);
  }

  .vditor-reset code:not(.hljs, .highlight-chroma) {
    background-color: rgb(27 31 35 / 5%);
  }

  .vditor-reset kbd {
    color: #24292e;
    background-color: #fafbfc;
    border: solid 1px #d1d5da;
    box-shadow: inset 0 -1px 0 #d1d5da;
  }

  .vditor-speech {
    color: #586069;
    background-color: #f6f8fa;
    border: 1px solid #d1d5da;
  }

  .vditor-speech--current,
  .vditor-speech:hover {
    color: #4285f4;
  }

  .vditor-linkcard a {
    background-color: #f6f8fa;
  }

  .vditor-linkcard a:visited .vditor-linkcard__abstract {
    color: rgb(88 96 105 / 36%);
  }

  .vditor-linkcard__title {
    color: #24292e;
  }

  .vditor-linkcard__abstract {
    color: #586069;
  }

  .vditor-linkcard__site {
    color: #4285f4;
  }

  .vditor-linkcard__image {
    background-color: rgb(88 96 105 / 36%);
  }
}
```
该文件主要修改了`vditor`的默认主题，使其与项目主题色同步

修改`src/style/index.scss`文件：
```
@use './vditor.scss';
```
样式出口文件中引入修改的样式文件

修改`src/views/Plugins/Editor/Markdown/index.vue`文件：
```
<script setup lang="ts" name="MarkdownEditor">
import useScrollPosition from '@/hooks/scrollPosition'
import useVkConfig from '@/hooks/vkConfig'
const store = useStore()
const route = useRoute()

const { vkConfig } = useVkConfig() // 获取项目动态配置参数、设备类型

// 滚动行为
useScrollPosition(route, store, vkConfig)

const markDownRef = ref(null)
const value = ref(`
  # 标题h1

  ##### 标题h5

  **加粗**
  *斜体*
  ~~删除线~~
  [链接](https://gitee.com/ctokevin/vue-admin-system)
  ↓分割线↓

  ---

  * 无序列表1
    * 无序列表1.1

  1. 有序列表1
  2. 有序列表2

  * [ ] 任务列表1
  * [x] 任务列表2

  > 引用示例

  \`\`\`js
  // 代码块:
  (() => {
    var htmlRoot = document.getElementById('htmlRoot');
    var theme = window.localStorage.getItem('__APP__DARK__MODE__');
    if (htmlRoot && theme) {
      htmlRoot.setAttribute('data-theme', theme);
      theme = htmlRoot = null;
    }
  })();
  \`\`\`

  | 表格 | 示例 | 🎉️ |
  | --- | --- | --- |
  | 1 | 2 | 3 |
  | 4 | 5 | 6 |
`)
const handleChange = (v: string) => {
  value.value = v
}
</script>

<template>
  <el-space :size="vkConfig.space" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <Markdown v-model:value="value" @change="handleChange" ref="markDownRef" placeholder="这是占位文本" width="100%" />
      <el-card shadow="never">
        <template #header>
          <div class="card-header">
            <span>Markdown展现视图演示</span>
          </div>
        </template>
        <MarkdownViewer :value="value" />
      </el-card>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  width: 100%;
  padding: var(--el-space) var(--el-space) 0;
}

.no-border{
  border: none;
}

.no-radius{
  border-radius: 0;
}

.el-card{
  display: flex;
  flex-direction: column;
  overflow: inherit;

  :deep(.el-card__header){
    .card-header{
      display: flex;
      align-items: center;
      justify-content: space-between;

    }
  }

  :deep(.el-card__body){
    flex: 1;
  }
  .el-card{
    margin-top: var(--el-space);
  }
}
</style>
```
该文件主要展示了调用`markdown`编辑器组件、以及`markdown`视图展现组件的使用方法。