﻿# 组件size功能全局配置
这里主要针对`element-plus`的组件的尺寸设置，可以先在`public/serverConfig.json`文件中添加全局配置参数：
```
{
  "Version": "1.0.0",
  "Title": "settings.settingProjectTitle",
  "Locale": "zh",
  "Size":"default"
}
```
然后修改`src/utils/storage/responsive.ts`文件：
```
export const injectResponsiveStorage = (app: App, config: ServerConfigs) => {
  const configObj = Object.assign(
    {
      // 国际化 默认中文zh
      locale: {
        type: Object,
        default: Storage.getData(undefined, "locale") ?? {
          locale: config.Locale ?? "zh"
        }
      },
      // 组件尺寸 默认 default
      size: {
        type: Object,
        default: Storage.getData(undefined, "size") ?? {
          size: config.Size ?? "default"
        }
      },
    },
    {}
  );
  app.use(Storage, configObj);
};
```
修改`types/global.d.ts`文件：
```
declare interface ServerConfigs {
  Version?: string;
  Title?: string;
  Locale?: string;
  Size?: string;
}
```
再修改`src/App.vue`文件：
```
<script lang="ts">
import { defineComponent } from "vue";
import { ElConfigProvider } from "element-plus";
import zhCn from "element-plus/lib/locale/lang/zh-cn";
import en from "element-plus/lib/locale/lang/en";
export default defineComponent({
  name: "app",
  components: {
    [ElConfigProvider.name]: ElConfigProvider
  },
  computed: {
    currentLocale() {
      return this.$storage.locale?.locale === "zh" ? zhCn : en;
    },
    currentSize(){
      return this.$storage.size?.size
    }
  }
});
</script>

<template>
  <el-config-provider :locale="currentLocale" :size="currentSize">
    <router-view />
  </el-config-provider>
</template>
```
添加国际化设置，修改`src/plugins/i18n/en/buttons.ts`、`src/plugins/i18n/zh-CN/buttons.ts`文件：
```
buttonSizeLarge:"Large",
buttonSizeDefault:"Default",
buttonSizeSmall:"Small",


buttonSizeLarge:"大",
buttonSizeDefault:"默认",
buttonSizeSmall:"小",
```
在`src/layout/components/headerPanel.vue`文件中增加对应组件及逻辑：
```
let size = computed(() => {
  return instance.size.size;
});
// 切换组件尺寸大小
const sizeChange = (size) => {
  instance.size = { size: size };
  size = size;
}
// 设置组件尺寸选中后的样式
const getDropdownSizeStyle = computed(() => {
  return (size, t) => {
    return {
      background: size === t ? '#409eff' : "",
      color: size === t ? "#f4f4f5" : "#000"
    };
  };
});

<!-- 组件尺寸 -->
<el-dropdown trigger="click">
  <span class="icon-span size"><svg-icon icon-class="icon-font-size"></svg-icon></span>
  <template #dropdown>
    <el-dropdown-menu class="size-list">
      <el-dropdown-item @click="sizeChange('large')">
        <div class="item-size" :style="getDropdownSizeStyle(size, 'large')"><svg-icon class="check" icon-class="check" v-show="size === 'large'"></svg-icon>{{ $t("buttons.buttonSizeLarge") }}</div>
      </el-dropdown-item>
      <el-dropdown-item @click="sizeChange('default')">
        <div class="item-size" :style="getDropdownSizeStyle(size, 'default')"><svg-icon class="check" icon-class="check" v-show="size === 'default'"></svg-icon>{{ $t("buttons.buttonSizeDefault") }}</div>
      </el-dropdown-item>
      <el-dropdown-item @click="sizeChange('small')">
        <div class="item-size" :style="getDropdownSizeStyle(size, 'small')"><svg-icon class="check" icon-class="check" v-show="size === 'small'"></svg-icon>{{ $t("buttons.buttonSizeSmall") }}</div>
      </el-dropdown-item>
    </el-dropdown-menu>
  </template>
</el-dropdown>
```
最后修改`src/style/layout.scss`文件，添加对应的样式代码：
```

# .header-right类中：

.globalization,.size {
  height: 48px;
  width: 40px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-around;
  color: rgba(0, 0, 0, 0.9);
  font-size: 16px;
  &:hover {
    background: #f6f6f6;
  }
}

# 文件底部：

// 组件尺寸样式
.size-list{
  .el-dropdown-menu__item{
    padding: 0;
    .item-size{
      padding: 5px 30px;
      position: relative;
      display: flex;
      width: 100%;
      align-items: center;
    }
  }
  .check{
    position: absolute;
    left: 10px;
  }
}
```
此时需要先清除所有缓存，运行项目登陆后，切换尺寸大小，就可以看到页面中的日期选择插件会根据尺寸切换而有所变化。
# 自定义标签配置尺寸设置
除了`element-plus`的组件可以设置尺寸外，也可以根据获取这个尺寸值，给任意标签添加尺寸设置，修改`src/views/Home/index.vue`文件：
```
import { ref,getCurrentInstance,computed } from 'vue'

const instance = getCurrentInstance().appContext.app.config.globalProperties.$storage;

let size = computed(() => {
  return instance.size.size;
});


<a @click="jumpAnchor" :data-title-size="size">跳转锚点</a>



a[data-title-size='large']{
  font-size: 18px;
}
a[data-title-size='default']{
  font-size: 14px;
}
a[data-title-size='small']{
  font-size: 12px;
}
```
加入以上代码变动后，切换尺寸大小，跳转锚点的字体大小就也会跟随发生变化了。