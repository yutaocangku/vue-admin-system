﻿# token和session
上一节对前端来说，是最简单的登录功能，不需要前端再去做其他操作，关于身份认证，也就是后端程序判断当前请求数据的用户到底是哪一个用户，以及登录超时、用户权限问题等，这些操作不需要前端去关心，后端自己已完成这些操作，并将结果返回给了前端。这种方式通常后端是通过`session`方式去处理的，但是`session`有一定的局限性，所以诞生了基于`token`的身份验证，`token`的使用对于前端来说，需要做的操作就比较多了。

`session`方式后端自己就能实现身份的验证，每个登录用户都有一个唯一的`sessionID`，后端能知道每次请求的`sessionID`，以此实现身份验证，也能知道是否已登录超时，并以此做出相应逻辑判断，返回给前端是登录超时或是其身份应该获取的数据

`token`的方式却需要前端参与，用户在登录成功后，后端会返回给前端这个用户的识别标识`token`，前端需要将其缓存到本地，并在之后的每次请求中，将其回传给后端，后端通过该`token`判断用户是否登录超时、或者该用户的当前次请求应该获取什么权限的数据，并最终返回给前端结果。

具体关于`token`、`session`的介绍可看以下文章讲解：
- [彻底理解cookie，session，token](https://zhuanlan.zhihu.com/p/63061864) 
- [Web中什么是token,token的组成部分详解（jwt Token）](https://blog.csdn.net/qq_42759386/article/details/89552190) 
- [JWT之token机制与双token详解](https://blog.csdn.net/qq_41634455/article/details/112591318)
- [前端篇-axios如何利用promise无痛刷新token](https://segmentfault.com/a/1190000020986592)
- [后端篇-axios如何利用promise无痛刷新token](https://segmentfault.com/a/1190000020210980)

# token 是什么？
`token`的意思是“令牌”，是服务端生成的一串字符串，作为客户端进行请求的一个标识。

当用户第一次登录后，服务器生成一个`token`并将此`token`返回给客户端，以后客户端只需带上这个`token`前来请求数据即可，无需再次带上用户名和密码。

简单`token`的组成；`uid`(用户唯一的身份标识)、`time`(当前时间的时间戳)、`sign`(签名，`token`的前几位以哈希算法压缩成的一定长度的十六进制字符串。为防止token泄露)。

# 基于 token 的身份验证
大概流程如下：
1. 客户端使用用户名跟密码请求登录
2. 向后端发送登录请求
3. 服务端收到请求，去验证用户名与密码
4. 验证成功后，服务端会签发一个`Token`，再把这个`Token`发送给客户端
5. 客户端收到`Token`以后，可以把它存储起来，比如放在`Cookie`里或者`Local Storage`里
6. 客户端每次向服务端请求资源的时候，需要带着服务端签发的`Toke`（不管是在`header`里，还是在请求参数里）它都要通过向后端发送请求，回传给服务端
7. 服务端收到请求，然后去验证客户端请求里面带着的`Token`，如果验证成功，就向客户端返回请求的数据
8. 验证不成功，返回错误原因，客户端根据错误原因，进行相应的客户端逻辑操作

# token 是否需要设置有效期？
从一些现有的例子看，登录密码，一般要求定期改变密码，以防止泄漏，所以密码是有有效期的；再比如安全证书，`SSL` 安全证书都有有效期，目的是为了解决吊销的问题。所以无论是从安全的角度考虑，还是从吊销的角度考虑，`Token` 都需要设有效期。

# token 有效期多长合适？
根据系统的安全需要，尽可能的短，但也不能短得离谱——想像一下手机的自动熄屏时间，如果设置为 10 秒钟无操作自动熄屏，再次点亮需要输入密码，会不会疯？

然后新问题产生了，如果用户在正常操作的过程中，Token 过期失效了，要求用户重新登录...... 用户体验岂不是很糟糕？

# 解决 token 失效问题 双 token 机制
额外增加一个 `RefreshToken`，其也会有过期时间，不一样的点在于`accessToken`的过期时间可以设置的很短，一般以小时或分钟为单位，而`RefreshToken`的过期时间则最低是以小时为单位，且其过期时间必须是`accessToken`的至少两倍以上，且不得低于4个小时，这样才可以在不造成过于糟糕的用户体验的同时，提高安全性，提升被请求劫持的复杂性。

`accessToken`是作为正常请求的身份验证使用，`RefreshToken`则用来在`accessToken`已失效时，用来更新`accessToken`使用。而如果`RefreshToken`失效，就需要用户重新登录了。

# 双 token 机制下新产生的问题
1. `accessToken`过期失效的判断是由前端来判断还是后端判断
2. `accessToken`过期进行更新后，重新请求数据的问题
3. `accessToken`过期更新，如当前有多个并发请求（异步请求），如何保证只有第一个进行更新，后续请求都使用更新后的`accessToken`，并保证请求顺序与更新前保持一致

# token的使用方法
根据个人和后端配合方式，`token`的使用方法目前会分为三类：
1. 单`token`机制
- 优点： 逻辑简单，配置简单
- 缺点： 安全性不高，`token`过期时间需要设置很长，过期后直接跳转登录页
2. 双`token`机制（前端判断`accessToken`失效问题）：
- 优点：在请求前拦截，节省请求、流量
- 缺点：需要后端额外返回一个`accessToken`过期时间的字段；使用本地时间判断是否过期，若本地时间被篡改，特别是本地时间改为过去时间时，`accessToken`将很难失效
> `accessToken`的有效时间建议是时间段，即以秒为单位的一个数值，而不要使用具体某一个时间日期，当本地时间与服务器时间不一致时，具体时间日期会有问题
3. 双`token`机制（后端判断`accessToken`失效问题）：
- 优点：不需要额外给前端返回`accessToken`过期时间的字段
- 缺点：会有额外多一次的请求，耗流量

方法二和方法三的优缺点是互补的，方法二有校验失败的风险（本地时间被篡改），不会多消耗一次请求，方法二则在知道服务器返回过期后再重试了一次，多了一次请求，但保证了验证的安全性。

# 单 token 机制
这种形式就单纯将`token`类比做`sessionID`来使用，只是安全性上来讲，要差一些，这里先实现最简单的单`token`机制

## 后端返回Token
修改`mock/user.ts`文件，添加生成以及返回`accessToken`的相关代码，后端具体怎么实现，这里不做具体讨论：
```
// 经过一系列操作生成了accessToken
const createdToken = ()=>{
  return {
    accessToken:"abcdef",
  }
}

if(isUser){
  if(isLogin){
    var token = createdToken();
    return {
      code: 200,
      status:true,
      info:"登录成功！",
      data:userInfo,
      accessToken:token.accessToken,
    }
  }else{
    return {
      code: 200,
      status:false,
      info:"密码错误！",
    }
  }
}else{
  return {
    code: 200,
    status:false,
    info:"用户名不存在！",
  }
}
```
## 封装缓存token的方法
`token`需要存放在`cookie`中，需要先行安装对应插件：
```
yarn add js-cookie
yarn add @types/js-cookie -D
```
在`src/utils`文件夹下新建`auth.ts`文件：
```
import Cookies from "js-cookie";

const TokenKey = "authorized-token";

type paramsMapType = {
  accessToken: string;
};

// 获取token
export function getToken() {
  // 此处与TokenKey相同，此写法解决初始化时Cookies中不存在TokenKey报错
  return Cookies.get("authorized-token");
}

// 设置token
export function setToken(data:any) {
  const { accessToken } = data;
  // 提取关键信息进行存储
  const paramsMap: paramsMapType = {
    accessToken
  };
  const dataString = JSON.stringify(paramsMap);
  Cookies.set(TokenKey, dataString);
}

// 删除token
export function removeToken() {
  Cookies.remove(TokenKey);
}
```
然后需要在登录成功后，将后端返回的`token`缓存下来，修改`src/views/Login/index.vue`文件：
```
import { setToken } from "@/utils/auth";

// 登录点击事件
const onLogin = () => {
  if(account.value == ""){
    errorMessage("用户名不能为空");
    return false;
  }
  if(password.value == ""){
    errorMessage("密码不能为空");
    return false;
  }
  var loginData = {
    account:"",
    password:"",
  };
  loginData.account = account.value;
  loginData.password = password.value;
  login(loginData).then((res:any)=>{
    storageLocal.setItem("userInfo", res.data);
    setToken(res);
    successMessage(res.info);
    // 如果用户真正访问的页面需要登录权限，在跳转到登陆页用户登陆后，需重新跳转到用户真正要访问的页面
    router.push({ path: route.query.redirect&&route.query.redirect as string || '/' });
  });
};
```
## 路由跳转的判断改为根据token判断
上一节中路由是否跳转登录页，以及是否需要初始化获取动态路由，都是根据缓存的用户信息做的判断，现在`token`作为身份验证的信息，这里可以将用户信息判断替换为`token`判断，修改`src/router/index.ts`文件的导航守卫部分的代码：
```
import { getToken } from "@/utils/auth";

router.beforeEach((to, _from, next) => {
  const token = getToken();
  NProgress.start();
  if(token){
    if(_from?.name==="Login"||!_from?.name){
      // 如果是从登录页过来的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
      initRouter(constantNotRootRoutes).then(() => {
        router.push(to.path);
      });
      next();
    }else{
      // 如果是其他页面跳转的，且其他页面的name存在，则直接进行下一步
      next();
    }
  }else{// 未登录
    // 当前访问页面不是登陆页面
    if (to.path !== "/login") {
      // 当前访问页面包含在路由白名单中，可直接跳转
      if (whiteList.includes(to.path)) {
        next();
      } else {
        // 当前访问页面需要登录才能访问，需先跳转到登陆页，并将当前访问路由通过链接保存，在登陆后，获取该路由进行跳转
        next(`/login?redirect=${to.path}`);
      }
    } else {
      // 当前访问页面本身就是登录页，直接跳转
      next();
    }
  }
});
```
## 前端请求携带token回传给后端
这个时候前端已经在本地缓存了`token`，接下来需要在请求中携带`token`，回传给后端，这里需要在请求拦截中设置`headers`，在`headers`中添加`token`，修改`src/utils/http/index.ts`文件中请求拦截部分的代码：
```
import { getToken } from "@/utils/auth";

// 请求拦截
private httpInterceptorsRequest(): void {
  AxiosHttp.axiosInstance.interceptors.request.use(
    (config: AxiosHttpRequestConfig) => {
      const $config = config;
      // 开启进度条动画
      NProgress.start();
      // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
      if (typeof config.beforeRequestCallback === "function") {
        config.beforeRequestCallback($config);
        this.beforeRequestCallback = undefined;
        return $config;
      }
      // 判断初始化状态中有没有回调函数，没有的话
      if (AxiosHttp.initConfig.beforeRequestCallback) {
        AxiosHttp.initConfig.beforeRequestCallback($config);
        return $config;
      }
      // 回传token
      const token = getToken();
      // 判断token是否存在
      if (token) {
        const data = JSON.parse(token);
        // 确保config.headers永远不会是undefined，增加断言
        if(!config.headers){
          config.headers = {}
        }
        config.headers["Authorization"] = "Bearer " + data.accessToken;
        return $config;
      } else {
        return $config;
      }
    },
    error => {
      return Promise.reject(error);
    }
  );
}
```
`token`回传给后端后，后端通过回传的`token`与其在服务器缓存的`token`进行比对，确认当前用户身份，以及确认`token`的有效性，最终返回给前端对应的结果。后端具体怎么接收回传的`token`，以及如何比对、如何判断这里不做细说，是属于后端范畴，这里说一下后端返回结果的形式。

## 后端返回的数据格式
可以在响应拦截中打印一下`response`，其基本结构大致如下：
```
response = {
  config:{},
  data:{
    code:200,
    status:true,
    info:"",
    data:{}|[],
    ...
  },
  headers:{},
  request:{},
  status:200,
  statusText:"Ok"
}
```
以上结构中`config`、`headers`、`request`是请求的具体信息，`status`、`statusText`为`http`请求本身自带的状态码与状态信息说明字段，`data`中的所有数据为后端返回给前端的数据结构，其中`code`、`status`、`info`为后端返回给前端的当前请求的状态信息，`data`为后端返回的具体数据，这种结构是后端又多加了一层，有些后端可能只有一层，直接使用第一层的`status`、`statusText`、`data`作为状态码、状态信息、数据返回的字段，需要看与自己配合后端具体如何做的来进行选择。

- [HTTP请求的状态码与状态信息对照表](https://blog.csdn.net/qibao16/article/details/78526631)
- [关于用户登录验证的状态码及说明信息设计参考](http://www.xwood.net/_site_domain_/_root/5870/5874/t_c271466.html)

## 根据状态码增加超时跳转登录页的逻辑代码
与回传`token`给后端一样，对于`token`判断失效性这类会发生在所有请求上的逻辑判断，页需要写在封装的`axios`请求上，回传`token`是需要在请求未发送前，拦截请求，将`token`配置到请求内；而后端返回结果后，对结果的判断，则需要在响应拦截中做判断，判断的依据就是后端给的状态码，这个需要前后端约定，每种情况都需要返什么状态码，具体情况又需要前端如何处理逻辑。

根据以上状态码参考，这里假定与后端约定登录超时，后端返回状态码为`201002`，并在确定为该状态码后，做自动跳转回登录页、清除`token`缓存、清除用户信息缓存的操作，修改`src/utils/http/index.ts`文件中的响应拦截部分代码：
```
import router from '@/router';
import {errorMessage} from "@/utils/message";
import { removeToken,getToken } from "@/utils/auth";
import { storageSession } from "@/utils/storage";

// 响应拦截
private httpInterceptorsResponse(): void {
  const instance = AxiosHttp.axiosInstance;
  instance.interceptors.response.use(
    (response: AxiosHttpResponse) => {
      console.log(response,"请求响应数据");
      const $config = response.config;
      // 关闭进度条动画
      NProgress.done();
      if(response.data.code==200){
        // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
        if (typeof $config.beforeResponseCallback === "function") {
          $config.beforeResponseCallback(response);
          this.beforeResponseCallback = undefined;
          return response.data;
        }
        if (AxiosHttp.initConfig.beforeResponseCallback) {
          AxiosHttp.initConfig.beforeResponseCallback(response);
          return response.data;
        }
        return response.data;
      }else{
        if(response.data.code==201002){
          errorMessage("登录已超时，请重新登录！");
          // 清除cookie中的token
          removeToken();
          // 清除缓存中的用户信息
          storageSession.removeItem("userInfo");
          router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
          // 正常返回数据，因为如果超时也是先走接口请求数据，后续的逻辑判断中还用得到返回的数据结构，如不返回data将是undefined，具体请求部分的逻辑就需要加上data存在的判断
          return response.data;
        }
      }
    },
    (error) => {
      const $error = error;
      // 关闭进度条动画
      NProgress.done();
      // 所有的响应异常 区分来源为取消请求/非取消请求
      return Promise.reject($error);
    }
  );
}
```
此时运行项目，前端的逻辑就已经处理完了，这里还需要将后端的逻辑在处理一下，目前有登录和获取动态菜单的接口，在登陆后的所有请求中，都需要回传`token`给后端，获取菜单数据的接口也一样会获取到回传的`token`，这里只修改一下在后端判断后的返回结果，修改`mack/asyncRoutes.ts`文件：
```
// 判断token是否有效，假定这里已获取到前端回传的token
const isTokenValid = ()=>{
  return true;
}
export default [
  {
    url: "/getAsyncRoutes",
    method: "post",
    response: () => {
      if(isTokenValid){
        return {
          code: 200,
          status:true,
          info:"菜单数据获取成功！",
          data: systemRouter
        };
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data: []
        };
      }
    }
  }
] as MockMethod[];
```
此时前后端的关于单`token`的逻辑就全部走通了，前端在导航守卫中监控当前路由打开时是否已存在`token`，不存在则需要跳转到登录页，进行登录操作，登录成功，后端返回给前端`token`，前端进行保存，并跳转到原本该进入的页面，此时会进入获取动态路由的请求；而如果存在，也会进入获取动态路由的请求，该请求就会把已存在的`token`通过请求拦截中的代码，配置到请求`headers`中，后端会获取到该`token`，并在后端做对比、判断有效性等操作，如有效，则返回正常的动态路由数据，前端拿到数据进行页面渲染；而如果无效，则返回对应状态码，动态路由数据的`data`为空，其数据类型应还保持为数组类型，此时前端在请求响应中，拦截到与后端约定的状态码，发现是登录超时状态码，对应的操作就是提示用户登录超时，并删除`cookie`中缓存的失效`token`，删除浏览器会话缓存中的用户信息，并跳转回登录页，进行登录操作，此时整个逻辑形成闭环。

这里进行测试，先运行项目做正常登录操作，登录成功后，正常跳转到`/home`页面，此时修改`mock/asyncRoutes.ts`文件，将判断`token`是否有效的方法返回值改为`false`，再刷新页面，页面会自动跳转回登录页，缓存的`token`以及用户信息也都已被删除，再将判断`token`是否有效的方法返回值改为`true`，再次登录，能正常获取数据，并正常跳转页面。

# 双 token 机制 前端拦截请求判断有效期
双`token`机制下，后端就需要返回两个`token`了（`accessToken`、`RefreshToken`），前端判断`accessToken`有效期，则还需额外返回`accessToken`的有效时间（秒为单位的数值-`expireTime`），并在请求拦截中增加相关逻辑判断代码。

## 后端返回token
先修改登录后，后端返回的数据，修改`mock/user.ts`文件：
```
// 经过一系列操作生成了accessToken、expireTime、refreshToken
const createdToken = ()=>{
  return {
    accessToken:"abcdef",
    expireTime:600,
    refreshToken:"qwerty"
  }
}
// 更新token接口中接收到的前端返回的refreshToken,判断refreshToken是否有效，假定这里已获取到前端回传的refreshToken
const isTokenValid = ()=>{
  return true;
}
export default [
  {
    url: "/login",
    method: "post",
    response: (data) => {
      var isUser = false;
      var isLogin = false;
      var userInfo = {};
      userData.forEach(function(item,index){
        if(item.account == data.body.account){
          isUser = true;
          if(item.password === data.body.password){
            isLogin = true;
            userInfo = item;
          }
        }
      });
      if(isUser){
        if(isLogin){
          var token = createdToken();
          return {
            code: 200,
            status:true,
            info:"登录成功！",
            data:userInfo,
            accessToken:token.accessToken,
            expireTime:token.expireTime,
            refreshToken:token.refreshToken,
          }
        }else{
          return {
            code: 200,
            status:false,
            info:"密码错误！",
          }
        }
      }else{
        return {
          code: 200,
          status:false,
          info:"用户名不存在！",
        }
      }
    }
  },
  {
    url: "/refreshToken",
    method: "post",
    response: ({refreshToken}) => {
      // 这里接收前端接口参数中回传的refreshToken，需要判断refreshToken是否也失效了，如果失效了，需要返回登录超时，如果未失效，则返回新的accessToken、expireTime、refreshToken
      if(isTokenValid()){
        var token = createdToken();
        return {
          code: 200,
          status:true,
          info:"token更新成功！",
          data:{
            accessToken:token.accessToken,
            expireTime:token.expireTime,
            refreshToken:token.refreshToken,
          },
        }
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data:{},
        }
      }
    }
  }
] as MockMethod[];
```
这里新增了`refreshToken`接口，用来更新`accessToken`使用，其需要将`refreshToken`通过**参数**回传给后端，后端接收到`refreshToken`需要判断是否有效，有效则返回给前端新的`accessToken`、`expireTime`、`refreshToken`，前端则将新返回的重新缓存，`refreshToken`只使用一次，每次需要重新更新，安全性会更高；如果无效，则返回给前端登录超时状态码，前端需要跳转回登录页，并删除缓存的`token`信息，以及用户信息。

> 这里返回新的`refreshToken`后，后端是否更新`refreshToken`，是后端决定的事儿，其有两种理解方向，第一是用户活跃性方向，`refreshToken`的作用主要用来判断用户的活跃性，如过在它的有效期内，用户都没有发生过一次请求，那说明该用户已经不属于活跃用户，也就不用再保持该用户的登录状态，有效期过后，用户再有请求，就需要用户重新登录，这种方向的话，后端就需要用户每次请求，都更新一下`refreshToken`的过期时间，保持用户最后一次请求后的过期时间是恒定的；第二是固定一个时间，不主动更新过期时间，用户登录后，不管用户请求多少次，到了固定时间，用户就必须重新登录。两种形式都各有优缺点，第一种用户体验更好，不会出现临界点时，用户正在操作而突然要求重新登录，缺点就是不安全，用户一直操作，就永远不会有被强制退出登录的情况，第二种相反，安全性会好一些，但用户体验会差一些，到时间就必须退出登录，如果用户当时正在做操作，突然被要求退出重新登录，体验会很糟糕。但这是后端需要考虑的事情，前端不用去操心这一块的事情。

## 前端接收token
登录完成后调用的`setToken(data)`方法将接收的数据缓存到`cookie`中，`src/utils/auth.ts`文件中的`setToken`方法需要进行修改：
```
type paramsMapType = {
  accessToken: string;
  expireTime:number;
  refreshToken: string;
};

// 设置token
export function setToken(data:any) {
  const { accessToken,expireTime, refreshToken } = data;
  // 提取关键信息进行存储
  const paramsMap: paramsMapType = {
    accessToken,
    expireTime:Date.now()+expireTime*1000,
    refreshToken,
  };
  const dataString = JSON.stringify(paramsMap);
  Cookies.set(TokenKey, dataString);
}
```
## 封装刷新accessToken的api方法
修改`src/api/user.ts`文件，新增以下方法：
```
// 更新Token
export const refreshToken = (data:object) => {
  return http.request("post", "/refreshToken",data);
};
```
## 前端请求拦截中做逻辑判断
修改`src/utils/http/index.ts`文件：
```
import { removeToken,getToken,setToken } from "@/utils/auth";
import { refreshToken } from "@/api/user";

// 请求拦截
private httpInterceptorsRequest(): void {
  AxiosHttp.axiosInstance.interceptors.request.use(
    (config: AxiosHttpRequestConfig) => {
      const $config = config;
      // 开启进度条动画
      NProgress.start();
      // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
      if (typeof config.beforeRequestCallback === "function") {
        config.beforeRequestCallback($config);
        this.beforeRequestCallback = undefined;
        return $config;
      }
      // 判断初始化状态中有没有回调函数，没有的话
      if (AxiosHttp.initConfig.beforeRequestCallback) {
        AxiosHttp.initConfig.beforeRequestCallback($config);
        return $config;
      }
      // 确保config.url永远不会是undefined，增加断言
      if(!config.url){
        config.url = ""
      }
      // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
      if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
        return $config
      }
      // 回传token
      const token = getToken();
      // 判断token是否存在
      if (token) {
        const data = JSON.parse(token);
        const now = Date.now();
        // 判断当前时间是否已过accessToken有效期
        const expired = parseInt(data.expireTime) - now <= 0;
        // 确保config.headers永远不会是undefined，增加断言
        if(!config.headers){
          config.headers = {}
        }
        // 已过有效期
        if(expired){
          // 当前时间大于过期时间，说明已经过期了，返回一个Promise，执行refreshToken后再return当前的config
          return refreshToken({refreshToken:data.refreshToken}).then((res:any)=>{
            if(res.status){
              setToken(res.data);
              // 确保config.headers永远不会是undefined，增加断言
              if(!config.headers){
                config.headers = {}
              }
              config.headers["Authorization"] = "Bearer " + res.data.accessToken;
              // 上面return一个promise，这里再return config，才能保证先刷新token后，再真正发起请求
              return $config;
            }
          });
        }else{
          config.headers["Authorization"] = "Bearer " + data.accessToken;
          return $config;
        }
      } else {
        return $config;
      }
    },
    error => {
      return Promise.reject(error);
    }
  );
}
```
以上处理了最简单情况下的过期刷新`token`的问题，当情况是一个页面有多个异步请求并发时，这里就会有问题，刷新`token`接口会多次执行，而期望的情况是，只刷新一次，第一个被拦截的请求去更新`token`，其他请求先拦截下来进行等待，等`token`更新后，又能保证请求进入的顺序，重新依次发起请求，这就需要在以上逻辑的基础上，对`axios`请求封装再做修改
## 并发请求情况下，只更新一次token，并将请求在更新完成前，按发起请求顺序拖入队列，更新完成后，再按发起请求顺序，重新发起请求
这里需要引入两个标记：是否正在刷新标记、重试队列标记
### 防止多次刷新
修改`src/utils/http/index.ts`文件：
```
// 是否正在刷新的标记
let isRefreshing = false
# 在全局中声明该变量，defaultConfig同级

// 请求拦截
private httpInterceptorsRequest(): void {
  AxiosHttp.axiosInstance.interceptors.request.use(
    (config: AxiosHttpRequestConfig) => {
      const $config = config;
      // 开启进度条动画
      NProgress.start();
      // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
      if (typeof config.beforeRequestCallback === "function") {
        config.beforeRequestCallback($config);
        this.beforeRequestCallback = undefined;
        return $config;
      }
      // 判断初始化状态中有没有回调函数，没有的话
      if (AxiosHttp.initConfig.beforeRequestCallback) {
        AxiosHttp.initConfig.beforeRequestCallback($config);
        return $config;
      }
      // 确保config.url永远不会是undefined，增加断言
      if(!config.url){
        config.url = ""
      }
      // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
      if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
        return $config
      }
      // 回传token
      const token = getToken();
      // 判断token是否存在
      if (token) {
        const data = JSON.parse(token);
        const now = Date.now();
        // 判断当前时间是否已过accessToken有效期
        const expired = parseInt(data.expireTime) - now <= 0;
        // 确保config.headers永远不会是undefined，增加断言
        if(!config.headers){
          config.headers = {}
        }
        // 已过有效期
        if(expired){
          if(!isRefreshing){
            // 正在刷新
            isRefreshing = true;
            // 当前时间大于过期时间，说明已经过期了，返回一个Promise，执行refreshToken后再return当前的config
            return refreshToken({refreshToken:data.refreshToken}).then((res:any)=>{
              if(res.status){
                setToken(res.data);
                isRefreshing = false //刷新成功，恢复刷新标记
                // 确保config.headers永远不会是undefined，增加断言
                if(!config.headers){
                  config.headers = {}
                }
                config.headers["Authorization"] = "Bearer " + res.data.accessToken;
                // 上面return一个promise，这里再return config，才能保证先刷新token后，再真正发起请求
                return $config;
              }
            });
          }
        }else{
          config.headers["Authorization"] = "Bearer " + data.accessToken;
          return $config;
        }
      } else {
        return $config;
      }
    },
    error => {
      return Promise.reject(error);
    }
  );
}
```
### 更新完成前请求进入队列
当发现有请求是过期，就暂存入队列，包括第一个也先暂存如队列，第一个发现过期，去请求更新`token`，同时将第一个请求暂存入队列，之后在更新`token`未完成前，进来的所有请求，全部都先暂存入队列，等待更新`token`完成，再一次发起队列中的请求，以上的代码中，直接`return`了更新`token`的接口，就是为了再次发起请求，代码再次修改后如下：
```
// 重试队列，每一项将是一个待执行的函数形式
let requests:any = []
# 在全局中声明该变量，defaultConfig同级

// 请求拦截
private httpInterceptorsRequest(): void {
  AxiosHttp.axiosInstance.interceptors.request.use(
    (config: AxiosHttpRequestConfig) => {
      const $config = config;
      // 开启进度条动画
      NProgress.start();
      // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
      if (typeof config.beforeRequestCallback === "function") {
        config.beforeRequestCallback($config);
        this.beforeRequestCallback = undefined;
        return $config;
      }
      // 判断初始化状态中有没有回调函数，没有的话
      if (AxiosHttp.initConfig.beforeRequestCallback) {
        AxiosHttp.initConfig.beforeRequestCallback($config);
        return $config;
      }
      // 确保config.url永远不会是undefined，增加断言
      if(!config.url){
        config.url = ""
      }
      // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
      if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
        return $config
      }
      // 回传token
      const token = getToken();
      // 判断token是否存在
      if (token) {
        const data = JSON.parse(token);
        const now = Date.now();
        // 判断当前时间是否已过accessToken有效期
        const expired = parseInt(data.expireTime) - now <= 0;
        // 确保config.headers永远不会是undefined，增加断言
        if(!config.headers){
          config.headers = {}
        }
        // 已过有效期
        if(expired){
          if(!isRefreshing){
            // 正在刷新
            isRefreshing = true;
            // 当前时间大于过期时间，说明已经过期了，返回一个Promise，执行refreshToken后再return当前的config
            refreshToken({refreshToken:data.refreshToken}).then((res:any)=>{
              if(res.status){
                setToken(res.data);
                isRefreshing = false //刷新成功，恢复刷新标记
                return res.data.accessToken;
              }
            }).then((accessToken)=>{
              // 循环数组，执行队列中暂存的请求
              requests.forEach((callback:any) => callback(accessToken));
              // 执行完成后，清空队列
              requests = [];
            });
          }
          // 声明Promise，将请求暂存到队列中，并依次进行resolve
          const retryOriginalRequest = new Promise((resolve)=>{
            // 这里将执行函数全部push到数组中
            requests.push((accessToken:string)=>{
              // 确保config.headers永远不会是undefined，增加断言
              if(!config.headers){
                config.headers = {}
              }
              config.headers["Authorization"] = "Bearer " + accessToken;
              resolve($config);
            });
          });
          // return 声明的Promise
          return retryOriginalRequest;
        }else{
          config.headers["Authorization"] = "Bearer " + data.accessToken;
          return $config;
        }
      } else {
        return $config;
      }
    },
    error => {
      return Promise.reject(error);
    }
  );
}
```
此时前端拦截判断`token`有效期进行刷新`token`的方法所产生的问题就圆满解决了，项目运行无问题，具体测试需要写后端逻辑，这里不再做测试。

# 双 token 机制 后端判断有效期
后端判断`accessToken`有效期，就不需要返回其有效时间字段了，和前端不同的是，它不是在请求拦截中加逻辑判断代码，而是在响应拦截中增加相关逻辑判断代码，
## 后端修改
第一点是先将`mock/user.ts`中登录后以及刷新`token`后返回的`expireTime`字段取消，不再返回给前端：
```
import { MockMethod } from "vite-plugin-mock";

// http://mockjs.com/examples.html#Object
const userData = [
  {id:1,account:"admin",password:"123456",userName:"超级管理员",avatar:"https://img1.baidu.com/it/u=1919046953,770482211&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"},
  {id:2,account:"test1",password:"123456",userName:"小红",avatar:"https://img2.baidu.com/it/u=2353293941,2044590242&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"},
  {id:3,account:"test2",password:"123456",userName:"小黑",avatar:"https://img1.baidu.com/it/u=63401368,3081051561&fm=253&fmt=auto&app=138&f=JPEG?w=360&h=360"},
]

// 经过一系列操作生成了accessToken、refreshToken
const createdToken = ()=>{
  return {
    accessToken:"abcdef",
    refreshToken:"qwerty"
  }
}
// 更新token接口中接收到的前端返回的refreshToken,判断refreshToken是否有效，假定这里已获取到前端回传的refreshToken
const isTokenValid = ()=>{
  return true;
}

export default [
  {
    url: "/login",
    method: "post",
    response: (data) => {
      var isUser = false;
      var isLogin = false;
      var userInfo = {};
      userData.forEach(function(item,index){
        if(item.account == data.body.account){
          isUser = true;
          if(item.password === data.body.password){
            isLogin = true;
            userInfo = item;
          }
        }
      });
      if(isUser){
        if(isLogin){
          var token = createdToken();
          return {
            code: 200,
            status:true,
            info:"登录成功！",
            data:userInfo,
            accessToken:token.accessToken,
            refreshToken:token.refreshToken,
          }
        }else{
          return {
            code: 200,
            status:false,
            info:"密码错误！",
          }
        }
      }else{
        return {
          code: 200,
          status:false,
          info:"用户名不存在！",
        }
      }
    }
  },
  {
    url: "/refreshToken",
    method: "post",
    response: ({refreshToken}) => {
      // 这里接收前端接口参数中回传的refreshToken，需要判断refreshToken是否也失效了，如果失效了，需要返回登录超时，如果未失效，则返回新的accessToken、expireTime、refreshToken
      if(isTokenValid()){
        var token = createdToken();
        return {
          code: 200,
          status:true,
          info:"token更新成功！",
          data:{
            accessToken:token.accessToken,
            refreshToken:token.refreshToken,
          },
        }
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data:{},
        }
      }
    }
  }
] as MockMethod[];
```
这里还需要修改其他接口中的判断逻辑，其他接口在请求头`headers`中获取到前端传递过来的`accessToken`后，需要先匹配查找到其对应的`refreshToken`，验证`refreshToken`是否有效，如果`refreshToken`失效了，那么无论`accessToken`是否有效，都需要直接给前端返回状态码`201002`，判定当前登录已超时，需重新登录；如果`refreshToken`未失效，则再进行判断`accessToken`是否失效，如果`accessToken`无效，则返回给前端`token`已失效的状态码，根据[登录常见状态码](http://www.xwood.net/_site_domain_/_root/5870/5874/t_c271466.html)中标注，约定`token`失效后端返回状态码为`201004`，前端判断状态码为`201004`后，走更新`token`的逻辑；如果未失效，则直接返回请求真正应该返回的数据。

第二点是修改`mock/asyncRoutes.ts`文件：
```
// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = ()=>{
  return true;
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = ()=>{
  return true;
}

export default [
  {
    url: "/getAsyncRoutes",
    method: "post",
    response: () => {
      if(isRefreshTokenValid()){
        if(isAccessTokenValid()){
          return {
            code: 200,
            status:true,
            info:"菜单数据获取成功！",
            data: systemRouter
          };
        }else{
          return {
            code: 201004,
            status:false,
            info:"token已失效，请更新token！",
            data: []
          };
        }
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data: []
        };
      }
    }
  }
] as MockMethod[];
```
## 前端修改

### token的缓存
缓存中将`expireTime`相关代码删除，修改`src/utils/auth.ts`文件：
```
type paramsMapType = {
  accessToken: string;
  refreshToken: string;
};

// 设置token
export function setToken(data:any) {
  const { accessToken,expireTime, refreshToken } = data;
  // 提取关键信息进行存储
  const paramsMap: paramsMapType = {
    accessToken,
    refreshToken,
  };
  const dataString = JSON.stringify(paramsMap);
  Cookies.set(TokenKey, dataString);
}
```
### axios封装修改
这里与前端不同的点在于逻辑会出现在响应拦截中，而非请求拦截中，并发请求的问题依然存在，所以并发请求的逻辑代码也还依然需要，只是和请求拦截中逻辑代码会有些许不同，修改`src/utils/http/index.ts`文件：
```
import Axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import {
  RequestMethods,
  AxiosHttpResponse,
  AxiosHttpRequestConfig
} from "./types.d";
import qs from "qs";
import NProgress from "../progress";
import router from '@/router';
import {errorMessage} from "@/utils/message";
import { removeToken,getToken,setToken } from "@/utils/auth";
import { storageSession } from "@/utils/storage";
import { refreshToken } from "@/api/user";

// 详细配置可参考：www.axios-js.com/zh-cn/docs/#axios-request-config-1
const defaultConfig: AxiosRequestConfig = {
  baseURL: "",
  timeout: 10000,
  headers: {
    Accept: "application/json, text/plain, */*",
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest"
  },
  // get请求中数组格式的参数进行序列化
  paramsSerializer: params => qs.stringify(params, { indices: false })
};

// 是否正在刷新的标记
let isRefreshing = false
// 重试队列，每一项将是一个待执行的函数形式
let requests:any = []

class AxiosHttp {
  constructor(){
    this.httpInterceptorsRequest();
    this.httpInterceptorsResponse();
  }
  // 初始化配置对象
  private static initConfig: AxiosHttpRequestConfig = {};
  // 保存当前Axios实例对象
  private static axiosInstance: AxiosInstance = Axios.create(defaultConfig);
  // 设置请求前的回调
  private beforeRequestCallback: AxiosHttpRequestConfig["beforeRequestCallback"] = undefined;
  // 设置响应前的回调
  private beforeResponseCallback: AxiosHttpRequestConfig["beforeResponseCallback"] = undefined;

  private static transformConfigByMethod(
    params: any,
    config: AxiosHttpRequestConfig
  ): AxiosHttpRequestConfig {
    const { method } = config;
    const props = ["delete", "get", "head", "options"].includes(
      method!.toLocaleLowerCase()
    )
      ? "params"
      : "data";
    return {
      ...config,
      [props]: params
    };
  }
  // 请求拦截
  private httpInterceptorsRequest(): void {
    AxiosHttp.axiosInstance.interceptors.request.use(
      (config: AxiosHttpRequestConfig) => {
        const $config = config;
        // 开启进度条动画
        NProgress.start();
        // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
        if (typeof config.beforeRequestCallback === "function") {
          config.beforeRequestCallback($config);
          this.beforeRequestCallback = undefined;
          return $config;
        }
        // 判断初始化状态中有没有回调函数，没有的话
        if (AxiosHttp.initConfig.beforeRequestCallback) {
          AxiosHttp.initConfig.beforeRequestCallback($config);
          return $config;
        }
        // 确保config.url永远不会是undefined，增加断言
        if(!config.url){
          config.url = ""
        }
        // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
        if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
          return $config
        }
        // 回传token
        const token = getToken();
        // 判断token是否存在
        if (token) {
          const data = JSON.parse(token);
          // 确保config.headers永远不会是undefined，增加断言
          if(!config.headers){
            config.headers = {}
          }
          config.headers["Authorization"] = "Bearer " + data.accessToken;
          return $config;
        } else {
          return $config;
        }
      },
      error => {
        return Promise.reject(error);
      }
    );
  }

  // 响应拦截
  private httpInterceptorsResponse(): void {
    const instance = AxiosHttp.axiosInstance;
    instance.interceptors.response.use(
      (response: AxiosHttpResponse) => {
        console.log(response,"请求响应数据");
        const $config = response.config;
        // 关闭进度条动画
        NProgress.done();
        if(response.data.code==200){
          // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
          if (typeof $config.beforeResponseCallback === "function") {
            $config.beforeResponseCallback(response);
            this.beforeResponseCallback = undefined;
            return response.data;
          }
          if (AxiosHttp.initConfig.beforeResponseCallback) {
            AxiosHttp.initConfig.beforeResponseCallback(response);
            return response.data;
          }
          return response.data;
        }else{
          if(response.data.code==201002){
            errorMessage("登录已超时，请重新登录！");
            // 清除cookie中的token
            removeToken();
            // 清除缓存中的用户信息
            storageSession.removeItem("userInfo");
            router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
            // 正常返回数据，因为如果超时也是先走接口请求数据，后续的逻辑判断中还用得到返回的数据结构，如不返回，data将是undefined，具体请求部分的逻辑就需要加上data存在的判断
            return response.data;
          }
          if(response.data.code==201004){
            const token = getToken();
            if(token){
              const data = JSON.parse(token);
              if(!isRefreshing){
                isRefreshing = true;
                return refreshToken({refreshToken:data.refreshToken}).then((res:any)=>{
                  if(res.status){
                    setToken(res.data);
                    // 确保config.headers永远不会是undefined，增加断言
                    if(!$config.headers){
                      $config.headers = {}
                    }
                    $config.headers['Authorization'] = "Bearer " + res.data.accessToken;
                    $config.baseURL = ''
                    // 已经刷新了token，将所有队列中的请求进行重试
                    requests.forEach((callback:any) => callback(token))
                    requests = [];
                    return instance($config);
                  }
                }).catch((res:any)=>{
                  // 清除cookie中的token
                  removeToken();
                  // 清除缓存中的用户信息
                  storageSession.removeItem("userInfo");
                  router.push(`/Login?redirect=${router.currentRoute.value.fullPath}`);
                }).finally(()=>{
                  isRefreshing = false;
                });
              }else{
                // 正在刷新token，将返回一个未执行resolve的promise
                return new Promise((resolve) => {
                  // 将resolve放进队列，用一个函数形式来保存，等token刷新后直接执行
                  requests.push((accessToken:string) => {
                    // 响应以后，请求接口的$config.url已经包含了baseURL部分，这里需要将baseURL清空，防止再次请求时，再次组装，导致url错误
                    $config.baseURL = '';
                    // 确保config.headers永远不会是undefined，增加断言
                    if(!$config.headers){
                      $config.headers = {}
                    }
                    $config.headers['Authorization'] = "Bearer " + accessToken;
                    resolve(instance($config));
                  })
                })
              }
            }
            return response.data;
          }
        }
      },
      (error) => {
        const $error = error;
        // 关闭进度条动画
        NProgress.done();
        // 所有的响应异常 区分来源为取消请求/非取消请求
        return Promise.reject($error);
      }
    );
  }

  // 通用请求工具函数
  public request<T>(
    method: RequestMethods,
    url: string,
    param?: AxiosRequestConfig,
    axiosConfig?: AxiosHttpRequestConfig
  ): Promise<T> {
    // post与get请求的参数需要用不同的key去接收，这里需要做判断，在请求中get的参数是params接收的，post的参数是data接收的
    const config = AxiosHttp.transformConfigByMethod(param, {
      method,
      url,
      ...axiosConfig
    } as AxiosHttpRequestConfig);
    // 单独处理自定义请求/响应回调
    if (axiosConfig?.beforeRequestCallback) {
      this.beforeRequestCallback = axiosConfig.beforeRequestCallback;
    }
    if (axiosConfig?.beforeResponseCallback) {
      this.beforeResponseCallback = axiosConfig.beforeResponseCallback;
    }
    // 单独处理自定义请求/响应回掉
    return new Promise((resolve, reject) => {
      AxiosHttp.axiosInstance.request(config).then((response:any) => {
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  }

  // 单独抽离的post工具函数
  public post<T>(
    url: string,
    params?: T,
    config?: AxiosHttpRequestConfig
  ): Promise<T> {
    return this.request<T>("post", url, params, config);
  }

  // 单独抽离的get工具函数
  public get<T>(
    url: string,
    params?: T,
    config?: AxiosHttpRequestConfig
  ): Promise<T> {
    return this.request<T>("get", url, params, config);
  }
}
export const http = new AxiosHttp();
```
以上就是在后端是使用`token`作为身份认证时，前端需要配合做的一些逻辑操作，三种方法需要看配合的后端选择的是哪一种，前端选择对应的处理方式即可，如果后端未选择`token`的方式做身份验证，而是使用的`session`，本章内容可略过，上一节中的内容前端部分逻辑就足够了。