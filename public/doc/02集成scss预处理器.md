﻿# 安装插件
由于这里使用的`vite`，其自带环境依赖的是`sass`，而如果直接使用`node-sass`，在这里`vite`环境不识别会报错，`vite`也自己集成了`sass-loader`，这里只需要下载`sass`依赖包即可：
```
yarn add sass -D
```

# 集成预处理全局scss文件
新建`src/style/var.scss`文件：
```
$primary: #409eff;
$pink: pink;
```
修改`vite.config.ts`文件：
```
import { resolve } from "path";
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// 路径查找
const pathResolve = (dir: string): string => {
  return resolve(__dirname, dir);
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
		alias: {
			'@': pathResolve('src'),
		}
	},
  css: {
    // css预处理器
    preprocessorOptions: {
      scss: {
        // 引入 var.scss 这样就可以在全局中使用 var.scss中预定义的变量了
        // 给导入的路径最后加上 ; 
        additionalData: `@import "@/style/var.scss";`
      }
    }
  },
})
```
然后分别修改`src/App.vue`文件、`src/components/HelloWorld.vue`文件中`style`部分：

1. 在`style`标签中加`lang="scss"`
2. 将`#app`这个类中的`color`值，改为`$primary`
3. 将`.code`这个类中的`color`值，改为`$pink`

此时再次运行项目，对应部分的文字颜色已做出相应改变，说明配置起效。

# 添加公共样式输出文件
项目需求还需要一些其他样式文件，先创建一个初始化的公共样式文件`public.scss`:
```

body {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  font-family: Helvetica Neue, Helvetica, PingFang SC, Hiragino Sans GB,
    Microsoft YaHei, Arial, sans-serif;
}

html {
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  color: $primary;
}

label {
  font-weight: 700;
}

*,
*::before,
*::after {
  box-sizing: inherit;
}

a:focus,
a:active {
  outline: none;
}

a,
a:focus,
a:hover {
  cursor: pointer;
  color: inherit;
  text-decoration: none;
}

div:focus {
  outline: none;
}

ul {
  margin: 0;
  padding: 0;
  list-style: none;
}

.clearfix {
  &::after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
  }
}
```
这里在`html`标签中也调用了变量，测试全局`scss`是否能在样式文件中起作用

然后再新建`index.scss`作为其他样式文件的输出文件：
```
@import './public.scss';
.demo {
  color: $pink;
}
```
在`src/main.ts`中引入该样式文件：
```
import { createApp } from 'vue'
import App from './App.vue'

import '@/style/index.scss';

createApp(App).mount('#app')
```

此时如果项目还在运行状态，会发现项目报错了，这个报错指向的是`scss`文件中的`@import`操作，这个问题在经过研究后，暂未发现比较完美的解决办法，暂时可行方法有两种，都不太完美：

1. 在`src/style/index.scss`文件的第一行，在所有`@import`之上，添加一行空样式，其他配置都不变，报错就能消除
```
.sass-error{}
@import './public.scss';
.demo {
  color: $pink;
}
```
此时报错已经消失，全局变量也正常起效

`src/App.vue`文件修改，增加`demo`类测试代码：
```
<p class="demo">测试</p>
```
样式文件中调用全局变量也能起效。

2. 直接通过`vite`的全局配置`scss`变量方法，引入`index.scss`文件，并将`var.scss`文件在`index.scss`中引入，这样报错就能消除：

`vite.config.ts`修改：
```
css: {
  // css预处理器
  preprocessorOptions: {
    scss: {
      // 引入 var.scss 这样就可以在全局中使用 var.scss中预定义的变量了
      // 给导入的路径最后加上 ; 
      additionalData: `@import "@/style/index.scss";`
    }
  }
},
```
`src/main.ts`修改：
```
import { createApp } from 'vue'
import App from './App.vue'

// import '@/style/index.scss';

createApp(App).mount('#app')
```
`src/style/index.scss`修改：
```
@import './var.scss';
@import './public.scss';
.demo {
  color: $pink;
}
```
此时项目的报错就自动消失了，全局变量也依然起效。样式文件中调用也能起效，当然，`var.scss`的引入必须在`index.scss`的第一条才行。

以上两种方法都不是太完美，但暂时未找到能完美解决该问题的办法，暂时先这样配置，先使用第一种方法的配置




