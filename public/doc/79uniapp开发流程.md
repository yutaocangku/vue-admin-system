本说明文档为记录本人开发流程，留作笔记，以防长时间不用后，无法快速拾起，文档中的创建、使用方法为本人熟悉的流程，其不属于全面教程，一切都只是选择一个方向的流程记录。

# 命令行创建项目
本人习惯于命令行创建项目，所以这里不会记录通过HBuilderX可视化界面创建项目的方法，后续的差异这里也不会记录

## 环境安装
这里默认已经安装了node，node版本选择稳定版最新版即可

node、 npm换源、环境变量配置等在这里都不做介绍

```
npm install @vue/cli -g
```

## 创建项目
这里使用vue3-vite-ts版

```
npx degit dcloudio/uni-preset-vue#vite-ts demo2
```

此时创建项目就是最基础的版本，要投入项目使用，还需要集成很多其它东西。

这里是将项目创建到了`demo2`文件夹下，此时需要使用编辑器打开`demo2`文件夹，并下载项目依赖，下载依赖本人习惯使用`yarn`

```
yarn
```

依赖下载完成后，执行`yarn dev:h5`即可在浏览器运行项目

修改`.gitignore`文件
```
# Logs
logs
*.log
npm-debug.log*
yarn-debug.log*
yarn-error.log*
pnpm-debug.log*
lerna-debug.log*

node_modules
.DS_Store
dist
*.local

# Editor directories and files
.idea
*.suo
*.ntvs*
*.njsproj
*.sln
*.sw?

package-lock.json
yarn.lock
```

## 更新依赖版本
命令行创建的项目，uni-app的依赖插件需要手动更新（package.json文件中@dcloudio开头的相关插件）

* 更新到最新版
```
npx @dcloudio/uvm@latest
```
* 更新到正式版指定版本
```
npx @dcloudio/uvm@latest 3.2.12.20211029
```

# 初始化项目
项目要正式投入使用，前期准备工作不可少，这里记录一下本人前期准备的内容

## 初始化配置
修改vite.config.ts文件：
```
import { defineConfig } from "vite";
import uni from "@dcloudio/vite-plugin-uni";

const path = require('path')

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [uni()],
  resolve: {
	  alias: {
		  '@': path.resolve(__dirname, 'src')
	  }
  },
  build: {
		minify: 'terser',
		terserOptions: {
			compress: {
				drop_console: true,
			},
		},
	}
});
```
修改`tsconfig.json`文件：
```
{
  "extends": "@vue/tsconfig/tsconfig.json",
  "compilerOptions": {
    "skipLibCheck": true,
    "module": "esnext",
    "declaration": true,
    "removeComments": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "allowSyntheticDefaultImports": true,
    "target": "es2017",
    "incremental": true,
    "esModuleInterop": true,
    "resolveJsonModule": true,
    "sourceMap": true,
    "baseUrl": ".",
    "paths": {
      "@/*": ["./src/*"]
    },
    "lib": ["esnext", "dom"],
    "types": ["@dcloudio/types"]
  },
  "include": ["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue"]
}
```

下载`sass`依赖：
```
yarn add sass sass-loader
```

## UI库集成
uniapp的ui库是挺令人头疼的，官方的uni组件少的可怜，样式也不尽如人意，市场上兼容vue3的并且兼容多端的，更是少的可怜，本人目前准备使用的是[wot design uni](https://wot-design-uni.gitee.io/)，支持vue3, 支持app、H5、微信小程序，其它平台好像支持不太彻底，不过本人主攻的就是app和微信小程序，选择它希望能一切顺利~

```
yarn add wot-design-uni
```

下载完成后需要配置`easycom`做自动引入

修改`pages.json`文件：
```
{
	"pages": [
		// ...
	],
	"easycom": {
		"autoscan": true,
		"custom": {
			// "^uni-(.*)": "@dcloudio/uni-ui/lib/uni-$1/uni-$1.vue",
			"^wd-(.*)": "wot-design-uni/components/wd-$1/wd-$1.vue"
		}
	}
}
```

此时就可以直接在项目中使用相关组件，组件使用规则还是要看官方对于组件的使用说明才行，部分组件还是需要在页面引入的，如`Message`,`Toast`等。


## 字体图标
字体图标是必不可少的，系统图标不能满足项目的开发需求，uniapp并不能兼容svg图标，因为还必须使用字体图标，这里可以使用**阿里巴巴**旗下的[iconfont](https://www.iconfont.cn/)收集图标。

### 创建图标项目
在`资源管理-我的项目`中新建项目，每个项目建一个专属的图标项目

### 收集图标
收集图标有多种渠道：
1. iconfont直接搜索自己想要的图标
2. 其它图标站中的图标收集
3. ui提供

以上除第一个，其它都需要是`svg`格式，并且需要上传到自己的`iconfont`图标项目中，可以让`ui`提供`svg`格式的图标，这里对这个不做多解释，以下将说一下，如何从其它图标资源站寻找心仪图标，并上传到自己的`iconfont`图标项目中

**寻找图标**
寻找图标可以到[图标集资源](https://icon-sets.iconify.design/)中寻找，这里汇集了丰富的图标集，各UI框架的图标集在这里也能找得到，本人较为常用的是`Remix Icon`图标集

**下载图标**
资源找到了，心仪图标也找到，如何下载呢？

图标点击后，可进入详情页，选择下载svg，但下载的svg并不能直接使用，这里的svg文件下载后，svg代码中都含有`fill="currentColor"`，这一部分代码必须删除，否则`iconfont`将无法上传成功。

将收集到的所有svg图标都下载完后，再打开编辑器删除每个图标文件代码中的所有`fill="currentColor"`，接下来就可以上传到`iconfont`里了

**上传图标**
在自己新建的图标项目中，点击`上传图标至项目`，进入上传图标页面后，点击`上传图标`，选择所有自己收集的图标上传即可，全部上传完成，需要等待官方审核，审核一般10-15分钟应该就会通过，如果有图标上传失败，可能就是`fill="currentColor"`未删除干净。

### 使用字体图标
图标收集完后，选择`Font Class` 并下载图标项目至本地，解压后的文件夹中，我们只使用两个文件，一个是`iconfont.css`文件，一个是`iconfont.ttf`文件

在`src/static`文件夹下，新建`css`文件夹，并将`iconfont.css`文件放入该文件夹。

修改`src/App.vue`文件，在`style`标签内引入`iconfont.css`文件：
```
<style>
@import 'static/css/iconfont.css';
</style>
```

此时字体图标还无法使用，这里并未将字体文件放入项目，字体文件放入项目，本地开发是没有问题的，但在线上时，图标就无法正常显示，这里不去深究其原因，只记录本人的使用方法，这里不适用字体文件，也不适用线上链接的方式引入字体文件，而是将字体文件转化为base64，并修改`iconfont.css`中字体文件引入相关代码

**字体文件转base64**
字体文件转base64可在[在线字体转换](https://transfonter.org/)中转换，上传字体文件，选择字体格式，点击`convert`进行转换，转换成功后，点击`download`，下载的文件解压后，打开`stylesheet.css`文件，复制`@font-face`，并将`src/static/css/iconfont.css`文件中的`@font-face`替换掉。

至此，项目的字体图标就集成完成了，项目中使用时，添加类名即可`iconfont 图标类名`

## 国际化
越来越多的客户要集成国际化，这玩意现在快成必需品了，创建项目时，`vue-i18n`就已经默认存在于`package.json`文件里了，这里不用再下载
### 封装
在`src`文件夹下，新建`locale`文件，其内新建`en`、`zh`、`zh_tw`文件夹，分别代表英文、简体中文、繁体中文语言包所属文件夹，另外`locale`文件夹下新建`config.ts`、`index.ts`文件

`config.ts`文件
```
import { set } from 'lodash-es'
/**
 * 从模块中抽取国际化
 * @param langs 存放国际化模块
 * @param prefix 语言 默认 en
 * @returns obj 格式：{模块名.**}
 */
export function siphonI18n(langs: any, prefix = 'en') {
  // eslint-disable-next-line no-undef
  const langsObj: any = {}
  Object.keys(langs).forEach((key: string) => {
    let fileName = key.replace(`./${prefix}/`, '').replace(/^\.\//, '')
    fileName = fileName.substring(0, fileName.lastIndexOf('.'))
    const keyList = fileName.split('/')
    const moduleName = keyList.shift()
    const objKey = keyList.join('.')
    const langFileModule = langs[key].default

    if (moduleName) {
      if (objKey) {
        set(langsObj, moduleName, langsObj[moduleName] || {})
        set(langsObj[moduleName], objKey, langFileModule)
      } else {
        set(langsObj, moduleName, langFileModule || {})
      }
    }
  })
  return langsObj
}

// 项目内自定义国际化
const zhModules: any = import.meta.glob('./zh/**/*.ts', { eager: true }) // 中文简体
const enModules: any = import.meta.glob('./en/**/*.ts', { eager: true }) // 英语语言包
const zhTwModules: any = import.meta.glob('./zh_tw/**/*.ts', { eager: true }) // 中文繁体

export const localesConfigs = {
  'zh-Hant': {
    ...siphonI18n(zhTwModules, 'zh_tw')
  },
  'zh-Hans': {
    ...siphonI18n(zhModules, 'zh')
  },
  en: {
    ...siphonI18n(enModules, 'en')
  }
}
```
这里用到了`lodash-es`插件，需要下载该插件
```
yarn add lodash-es
```

`index.ts`文件
```
import { createI18n } from 'vue-i18n'
import { localesConfigs } from './config'
import { strPlaceholderFormat } from '@/utils'
 
/**
 * 国际化转换工具函数
 * @param message message
 * @param isI18n  如果true,获取对应的消息,否则返回本身
 * @returns message
 */
export function transformI18n(message: any, isI18n: boolean | unknown = true, args = []) {
	const _t: any = i18n.global
  if (!message) {
    return ''
  }

  // 处理存储动态路由的title,格式 {zh:"",en:""}
  if (typeof message === 'object') {
    if (args.length > 0) {
      return strPlaceholderFormat(message[i18n.global?.locale.value], args)
    } else {
      return message[i18n.global?.locale.value]
    }
  }

  if (isI18n) {
    if (args.length > 0) {
      return _t.t(message as string, args)
    } else {
      return _t.t(message as string)
    }
  } else {
    return message
  }
}

const lang = uni.getStorageSync('language') || 'en';//获取缓存中的语言
// VueI18n构造函数所需要的配置
const i18nConfig = {
    legacy: false,
    globalInjection: true,
    fallbackLocale: 'en',
    locale: lang,//当前语言
	// 所需要用的语言包
    messages: localesConfigs
}
export const i18n = createI18n(i18nConfig)
```

这里引入了一个处理函数，在`src`文件夹下新建`utils`文件夹，并新建`index.ts`文件：
```
export function strPlaceholderFormat(str: any, args: any) {
  for (let i = 0; i < args.length; i++) {
    str = str.replace(new RegExp('\\{' + i + '\\}', 'g'), i)
  }
  return str
}
```

在`en`、`zh`、`zh_tw`文件夹中，存放我们的语言包，这里各列举一个文件
`en/errors.ts`文件：
```
export default {
  errorCode202: 'You are not authorized to access the page, please contact the administrator!',
  errorCode302: 'Interface redirected!',
  errorCode400: 'The parameter is incorrect!',
  errorCode401: 'You do not have permission to operate!',
  errorCode403: 'resource unavailable, access denied!',
  errorCode404: 'Error requesting address:',
  errorCode408: 'Request timed out!',
  errorCode409: 'The same data already exists in the system!',
  errorCode500: 'Internal server error!',
  errorCode501: 'Service not implemented!',
  errorCode502: 'Gateway error!',
  errorCode503: 'service is not available!',
  errorCode504: 'The service is temporarily unavailable, please try again later!',
  errorCode505: 'HTTP version not supported!',
  errorUnknownMistake: 'Unknown mistake, please contact the administrator',
  errorTimeout: 'The network request timed out!',
  errorNetworkOnline: 'Server exception!',
  errorNetworkOffline: 'You are disconnected!',
  errorRequestBreakOff: 'request was interrupted!',
  errorLoginTimeOut: 'Login has timed out, please log in again!',
  errorDontSupportWebSocket: 'The browser does not support WebSocket!',
  error: 'Error',
  serverConfig: 'Please add the serverConfig.json configuration file to the public folder',
  '上传失败了': 'The upload failed',
  '{0}秒后重试': 'Try again in {0} s',
  '{0}秒后，返回首页': 'After {0} seconds, return to the home page',
  seamlessScroll: 'If step-by-step scrolling is set, the step must be an approximation of the step size, otherwise the position at the end of the step-by-step scrolling cannot be guaranteed.',
  socketFail1: 'The websocket connection binding event fails and the reconnection starts',
  socketFail2: 'socket.websocket.readyState=3, The connection to WS is abnormal and the connection is started',
  socketFail3: 'The socket is closed abnormally and is trying to reconnect',
  socketFail4: 'Failed to connect, refresh and try again',
  socketFail5: 'Connection failed, re-establishing connection...',
  socketFail6: "Didn't receive the data in the background, reconnect",
  errorPage403: "Sorry, you don't have access to this page",
  errorPage404: 'Sorry, the page you are visiting does not exist',
  errorPage500: 'Sorry, there was an error on the server'
}
```
`zh/errors.ts`文件
```
export default {
  errorCode202: '您未被授权访问该页面，请联系管理员！',
  errorCode302: '接口重定向了！',
  errorCode400: '参数不正确！',
  errorCode401: '您没有权限操作！',
  errorCode403: '资源不可用，拒绝访问！',
  errorCode404: '请求地址出错：',
  errorCode408: '请求超时！',
  errorCode409: '系统已存在相同数据！',
  errorCode500: '服务器内部错误！',
  errorCode501: '服务未实现！',
  errorCode502: '网关错误！',
  errorCode503: '服务不可用！',
  errorCode504: '服务暂时无法访问，请稍后再试！',
  errorCode505: 'HTTP版本不受支持！',
  errorUnknownMistake: '未知错误，请联系管理员！',
  errorTimeout: '网络请求超时！',
  errorNetworkOnline: '服务端异常！',
  errorNetworkOffline: '您断网了！',
  errorRequestBreakOff: '请求被中断！',
  errorLoginTimeOut: '登录已超时，请重新登录！',
  errorDontSupportWebSocket: '浏览器不支持WebSocket！',
  error: '错误',
  serverConfig: '请在public文件夹下添加serverConfig.json配置文件',
  '上传失败了': '上传失败了',
  '{0}秒后重试': '{0}秒后重试',
  '{0}秒后，返回首页': '{0}秒后，返回首页',
  seamlessScroll: '如果设置了单步滚动，step需是单步大小的约数，否则无法保证单步滚动结束的位置是否准确。',
  socketFail1: 'websocket连接绑定事件失败，开始重连',
  socketFail2: 'socket.websocket.readyState=3, ws连接异常，开始重连',
  socketFail3: 'socket非正常关闭，正在尝试重连',
  socketFail4: '未能正常连接，请刷新重试',
  socketFail5: '连接失败，正在重新建立连接...',
  socketFail6: '没有收到后台的数据，重新连接',
  errorPage403: '抱歉，您无权访问该页面',
  errorPage404: '抱歉，您访问的页面不存在',
  errorPage500: '抱歉，服务器出错了'
}
```
`zh_tw/errors.ts`文件
```
export default {
  errorCode202: '您未被授權訪問該頁面，請聯繫管理員！',
  errorCode302: '介面重定向了！',
  errorCode400: '參數不正確！',
  errorCode401: '您沒有權限操作！',
  errorCode403: '資源不可用，拒絕訪問！',
  errorCode404: '要求地址發生錯誤：',
  errorCode408: '請求超時！',
  errorCode409: '系統已存在相同數據！',
  errorCode500: '伺服器內部錯誤！',
  errorCode501: '服務未實現！',
  errorCode502: '閘道錯誤！',
  errorCode503: '服務不可用！',
  errorCode504: '服務暫時無法訪問，請稍後再試！',
  errorCode505: 'HTTP版本不受支援！',
  errorUnknownMistake: '未知錯誤，請聯繫管理員！',
  errorTimeout: '網路請求超時！',
  errorNetworkOnline: '服務端異常！',
  errorNetworkOffline: '您斷網了！',
  errorRequestBreakOff: '請求被中斷！',
  errorLoginTimeOut: '登錄已超時，請重新登錄！',
  errorDontSupportWebSocket: '瀏覽器不支援WebSocket！',
  error: '錯誤',
  serverConfig: '請在public資料夾下添加serverConfig.json配置文件',
  '上传失败了': '上傳失敗了',
  '{0}秒后重试': '{0}秒後重試',
  '{0}秒后，返回首页': '{0}秒后，返回首页',
  seamlessScroll: '如果設置了單步滾動，step需是單步大小的約數，否則無法保證單步滾動結束的位置是否準確。',
  socketFail1: 'websocket連接綁定事件失敗，開始重連',
  socketFail2: 'socket.websocket.readyState=3, ws連接異常，開始重連',
  socketFail3: 'socket非正常關閉，正在嘗試重連',
  socketFail4: '未能正常連接，請刷新重試',
  socketFail5: '連線失敗，正在重新建立連線...',
  socketFail6: '沒有收到後台的數據，重新連接',
  errorPage403: '抱歉，您無權訪問該頁面',
  errorPage404: '抱歉，您訪問的頁面不存在',
  errorPage500: '抱歉，伺服器出錯了'
}
```
语言包中的`key`值可以是英文，也可以是中文，这个随意，也可以带占位符带占位符时，需要传递额外的参数

### 使用
修改`src/main.ts`文件
```
import { createSSRApp } from "vue";
import App from "./App.vue";
import { i18n } from './locale/index'

export function createApp() {
  const app = createSSRApp(App);
  app.use(i18n)
  return {
    app,
    i18n
  };
}
```

文件中使用：
```
<template>
  <view class="content">
    <text @click="changeLocale('zh-Hans')">简体中文</text>
    <text @click="changeLocale('zh-Hant')">繁体中文</text>
    <text @click="changeLocale('en')">English</text>
    <view class="text-area">
      <text class="iconfont icon-message"></text>
      <text class="title">{{$t('errors.上传失败了')}}</text>
    </view>
  </view>
</template>

<script setup lang="ts">
import { getCurrentInstance, ref } from "vue"
import { useI18n } from 'vue-i18n'

const { locale } = useI18n()

const changeLocale = (language: string) => {
  uni.setLocale(language)
  uni.setStorageSync('language', language)
  locale.value = language
}

</script>

<style>
.content {
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
}
</style>
```
`pages.json`国际化
```
{
	"pages": [
		{
			"path": "pages/index/index",
			"style": {
				"navigationBarTitleText": "%menus.首页%",
				"navigationStyle": "custom"
			}
		}
	],
	"globalStyle": {
		"navigationBarTextStyle": "black",
		"navigationBarTitleText": ""
	},
	"easycom": {
		"autoscan": true,
		"custom": {
			// "^uni-(.*)": "@dcloudio/uni-ui/lib/uni-$1/uni-$1.vue",
			"^wd-(.*)": "wot-design-uni/components/wd-$1/wd-$1.vue"
		}
	}
}
```
`manifest.json`国际化
```
{
  "name" : "%page.projectTitle%",
  "appid" : "",
  "description" : "",
  "versionName" : "1.0.0",
  "versionCode" : "100",
  "locale": "en" // 设置默认语言，
}
```
该部分国际化可查看[uni-app官网](https://zh.uniapp.dcloud.io/tutorial/i18n.html)

## 请求封装
想让请求与常规`axios`请求一样使用，需要将uniapp的请求api进一步封装。

### 封装请求
在`src/utils`文件夹下新建`http`文件夹，并在其内新建`index.ts`、`tool.ts`文件
`index.ts`文件
```
/**
 * 通用uni-app网络请求
 * 基于 Promise 对象实现更简单的 request 使用方式，支持请求和响应拦截
 */
import tool from './tool' // 这个是我自己封装简化一下uni.showToast提示组件，可以不写
import { transformI18n } from '@/locale'
import qs from 'qs'
export default {
	config: {
		// 请求的公共url
		// baseUrl: "",
		baseUrl: "", // 接口地址基础部分
		header: {
			// 'Content-Type': 'application/json;charset=UTF-8'
			'Content-Type': 'application/x-www-form-urlencoded',
			// 'sessionId': uni.getStorageSync('userInfo') ? uni.getStorageSync('userInfo').session_id : ''
		},
		data: {},
		method: "GET",
		dataType: "json",
		/* 如设为json，会对返回的数据做一次 JSON.parse */
		responseType: "text",
		success() {},
		fail() {},
		complete() {}
	},
	// 请求拦截器
	interceptor: {
		request: null as any,
		response: null as any
	},
	request(method: any, url: any, data: any, options: any) {
		if (!options) {
			options = {}
		}
		// 获取登录后存储在本地的token信息
		let userInfo = uni.getStorageSync('userInfo')
		
		if (userInfo) {
			// 判断筛选出以上三个页面不需要为请求头设置token，根据自己的项目情况而定
			this.config.header['sessionId'] = userInfo.session_id
		}
		options.baseUrl = options.baseUrl || this.config.baseUrl
		options.dataType = options.dataType || this.config.dataType
		options.url = options.baseUrl + url
		options.data = qs.stringify(data, { indices: false }) || {}
		options.method = method
		// 基于 Promise 的网络请求
		return new Promise((resolve, reject) => {
			uni.showLoading()
			let _config: any = null
			options.complete = (response: any) => {
				uni.hideLoading()
				response.config = _config
				if (this.interceptor.response) {
					let newResponse = this.interceptor.response(response)
					if (newResponse) {
						response = newResponse
					}
				}
				console.log('调用接口程序响应信息：', response)
				// 结构出code、msg
				let { status, info } = response.data
				if (status === true) {
					resolve(response.data)
				} else {
					if (status === 201) {
						tool.msg(transformI18n('errors.errorLoginTimeOut', true))
						setTimeout(() => {
							uni.clearStorageSync()
							uni.reLaunch({
								url: '/pages/login/login'
							})
						}, 100)
						reject(response)
					} else {
						tool.msg(info)
						resolve(response.data)
					}
				}
			}
			_config = Object.assign({}, this.config, options)
			_config.requestId = new Date().getTime()

			if (this.interceptor.request) {
				this.interceptor.request(_config)
			}
			uni.request(_config)
		});
	},
	// get请求
	get(url: any, data: any, options: any) {
		if (!options) {
			options = {}
		}
		return this.request('GET', url, data, options)
	},
	// post请求
	post(url: any, data: any, options: any) {
		if (!options) {
			options = {}
		}
		return this.request('POST', url, data, options)
	},
	// put请求
	put(url: any, data: any, options: any) {
		if (!options) {
			options = {}
		}
		return this.request('PUT', url, data, options)
	},
	// delete请求
	delete(url: any, data: any, options: any) {
		if (!options) {
			options = {}
		}
		options.url = url
		options.data = data
		options.method = 'DELETE'
		return this.request('DELETE', url, data, options)
	}
}
```
这里用到了`qs`插件：
```
yarn add qs
```
可能`qs`会报类型错误，修改`src/env.d.ts`文件，增加以下代码：
```
declare module 'qs'
```

`tool.ts`文件：
```
const msg = (title: any, duration = 3000, mask = false, icon: any = 'none') => {
	if (Boolean(title) === false) {
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}

export default {
	msg
}
```
### 封装api
请求封装好了，后端的api也需要集中管理，新建`src/api`文件夹，用来管理后端的api接口，

新建`user.ts`文件
```
import http from '@/utils/http'

// 登录退出
export const user = {
  login: (data: any, config: any) => http.request('post', '/Login/login', data, config),
  logout: (data: any, config: any) => http.request('post', '/Login/loginout', data, config),
  register: (data: any, config: any) => http.request('post', '/Reguser/reg', data, config),
  code: (data: any, config: any) => http.request('post', '/Login/getcode', data, config),
  recover: (data: any, config: any) => http.request('post', '/Login/findpwd', data, config)
}

// 个人中心
export const person = {
  avatar: (data: any, config: any) => http.request('post', '/User/headimg', data, config),
  save: (data: any, config: any) => http.request('post', '/User/personset', data, config),
  changePwd: (data: any, config: any) => http.request('post', '/User/personpwd', data, config),
  info: (data: any, config: any) => http.request('post', '/User/userInfo', data, config)
}
```
以上举例一个文件，在使用时，只需要引用`api`文件夹下对应文件`export`的变量即可

### 使用
```
import user from '@/api/user'

const login = () => {
  user.login({ account: 'abc', password: '123456' }, null).then((res: any) => {
    console.log(res, '登录成功')
  })
}
```

## 集成pinia
vue3项目必然少不了状态管理工具pinia
```
yarn add pinia
```
新建`src/store`文件夹，该文件夹用来存储整个项目的状态管理文件

新建`src/store/index.ts`文件
```
import type { App } from 'vue'
import { createPinia } from 'pinia'

const store = createPinia()

export function setupStore(app: App<Element>) {
  app.use(store)
}

export { store }
```
修改`src/main.ts`文件：
```
import { createSSRApp } from "vue";
import App from "./App.vue";
import * as Pinia from 'pinia'
import i18n from './locale/index'

export function createApp() {
  const app = createSSRApp(App);
  app.use(i18n)
  app.use(Pinia.createPinia())
  return {
    app,
    i18n,
    Pinia
  };
}
```

新建`src/store/globalData/globalDataStore.ts`文件：
```
import { defineStore } from 'pinia'
import {ref} from 'vue'

export const useGlobalDataStore = defineStore('globalData', () => {
	const globalData = ref({
		systemInfo: null as any
	})
	return {
		globalData
	}
})
```
该文件用来存储全局变量相关数据

修改`src/App.vue`文件： 
```
<script setup lang="ts">
import { onLaunch, onShow, onHide } from "@dcloudio/uni-app";
import { storeToRefs } from 'pinia'
import { useGlobalDataStore } from '@/store/globalData/globalDataStore'
const globalDataStore = useGlobalDataStore()
const { globalData } = storeToRefs(globalDataStore)
onLaunch(() => {
  // 处理系统不同兼容性问题
  try {
    globalData.value.systemInfo = uni.getSystemInfoSync()
    console.log(globalData.value, '全局变量')
    
    // #ifdef APP-PLUS
      const statusBarHeight = globalData.value.systemInfo.statusBarHeight;
      document.documentElement.style.setProperty("--statusBarHeight", statusBarHeight + 'px');
      console.log('状态栏的高度', statusBarHeight)

      if (globalData.value.systemInfo.platform === "android") {
      document.body.classList.add('android');
      } else if (globalData.value.systemInfo.platform === "ios") {
      document.body.classList.add('ios');
      // 处理安全区域的距离
      const safeArea: any = globalData.value.systemInfo.safeArea;
      const bottomDistance = globalData.value.systemInfo.screenHeight - safeArea.bottom;
      document.documentElement.style.setProperty("--safe-area", bottomDistance + 'px');
      console.log('底部安全区域距离：', bottomDistance);

      } else {
      document.body.classList.add('other');
      console.log(globalData.value.systemInfo.statusBarHeight, '其它系统状态条高度')
      }
    // #endif
  } catch (e) {
	  console.log(e, '获取系统信息失败')
  }
});
onShow(() => {
  console.log("App Show");
});
onHide(() => {
  console.log("App Hide");
});
</script>
<style>
@import 'static/css/iconfont.css';
</style>
```

新建`src/store/storage/storageStore.ts`文件：
```
import { defineStore } from 'pinia'
import {ref} from 'vue'

export const useStorageDataStore = defineStore('storageData', () => {
	const userInfo: any = ref(null)
	
	const getUserInfo = () => {
		userInfo.value = uni.getStorageSync('userInfo')
	}
	const setUserInfo = () => {
		uni.setStorageSync('userInfo', userInfo.value)
	}
	const removeUserInfo = () => {
		uni.removeStorageSync('userInfo')
	}
	return {
		userInfo,
		getUserInfo,
		setUserInfo,
		removeUserInfo
	}
})
```
该文件用来管理需要做缓存的数据

## 集成dayjs
正常情况只需要下载该插件就可直接使用，但微信小程序内，无法直接使用，会报错，所以需要下载文件，并在项目内引入才能正常使用

前往[dayjs](https://dayjs.fenxianglu.cn/category/#node-js)下载`dayjs.min.js`文件并放到`src/static`文件夹下

### 使用

```
// #ifndef MP-WEIXIN
import dayjs from 'dayjs'
// #endif
// #ifdef MP-WEIXIN
const dayjs = require('../../../static/dayjs.min.js')
// #endif

onLoad(() => {
  console.log(dayjs().format())
})
```
注意这里`require`时，无法使用在`vite.config.ts`配置的`src`的别名`@`，只能通过路径找到这个文件的形式的使用

这里可能`import dayjs from 'dayjs'`会报错，需要修改`tsconfig.json`文件，增加以下代码：
```
"compilerOptions": {
  ...
  "importsNotUsedAsValues": "remove",
  ...
}
```

以上初始化完成后，一个项目的准备工作就算完成了，接下来就可以进入开发阶段，开发完成后，就是打包成各端的应用了，本人目前主攻的是微信小程序、安卓、IOS三端，所以以下只对这三段的打包细节做一些记录。

# 打包微信小程序
无论是打包还是开发，微信小程序都还是需要在`HBuilderX`中开发，并且需要连通微信小程序开发工具。

## 配置项
1. `src/manifest.json`文件中配置微信小程序AppID，由微信小程序生成，这里最好是先创建微信小程序，或者填一个测试APPID，空的容易出错
2. 在HBuilderX中配置微信开发者工具的安装路径，`运行-运行到小程序模拟器-运行配置`，在微信开发者工具路径栏，引入自己的微信开发工具的安装路径，指向`微信开发者工具`文件夹
3. 打开微信开发者工具，未打开项目情况下，点击右上角的设置按钮，找到安全选项，点击后开启服务端口，已打开项目情况下， 找到`设置-安全设置`，找到安全选项，点击后开启服务端口
4. 回到HBuilderX中，点击`运行-运行到小程序模拟器-微信开发者工具`，微信开发者工具后面中括号中显示的是本次运行的是哪个项目，不要运行错了项目。
5. 点击后，HBuilderX会自动编译，并自动打开开发者工具，如果是首次打开，由于还未编译出小程序代码，因此小程序开发者工具会找不到项目路径，打开失败
6. 此时点击微信开发者工具的创建项目或者导入项目都行，找到当前项目文件夹，在经过HBuilderX编译后，项目跟目录已经生成dist文件夹，选择`/dist/dev/mp-weixin`路径，就可以在微信开发者工具中打开项目了
7. 此时修改项目需要在HBuilderX中，HBuilderX会实时编译，并在微信开发者工具中能实时看到修改效果
8. 以上打开微信开发者工具的过程，如微信开发者工具未登录，需要先扫码登录，HBuilderX也需要进行登录

## 发行上传
项目要上线时，在HBuilderX中点击`发行-小程序-微信`，此时会弹窗提示填写小程序名称和AppID，以及其它编译选项，点击发行，HBuilderX会自动进行编译，如未勾选`自动传到到微信平台`，HBuilderX会自动打开微信开发者工具，此时点击微信开发者工具中的上传按钮，即可将项目上传到微信平台

## 提交审核
提交审核与原始微信开发一致，需要登录到微信公众平台，`管理-版本管理-提交审核`，第一次提交要填写小程序信息

以上是uniapp开发微信小程序的整个流程。其它小程序流程与微信小程序基本一致。

# 离线打包安卓

## 准备工作
1. [HBuilderX编辑器](https://hx.dcloud.net.cn/Tutorial/install/windows)
2. [Android Studio](https://developer.android.google.cn/studio?hl=zh-cn)
3. [App离线SDK](https://nativesupport.dcloud.net.cn/AppDocs/download/android.html)
4. [安卓生成apk所需插件](https://services.gradle.org/distributions/) **这个是重点，正常通过android studio打开安卓项目，初始化会下载生成所需插件，但是在编辑器中直接下载，很容易就被墙了，导致下载失败，然后编辑器中是无法找到Generate Signed Bundle / APK选项的，导致无法离线打包，出现这种情况时，需要我们手动下载，并放到对应目录中**
5. [JAVA-JDK1.8.0_201](https://pan.baidu.com/s/1U4SWoYY6Gn4B4PjSJwy9Gg)，提取码：tm85（获取签名使用）
6. [openssl](https://slproweb.com/products/Win32OpenSSL.html) 查看`MD5`信息使用
6. [签名证书生成指南](https://ask.dcloud.net.cn/article/35777)
7. [可获取MD5的签名指南](https://ask.dcloud.net.cn/article/id-38778)

## 开发环境搭建
将uniapp提供的离线SDK下载解压后，里面会有很多文件夹及文件，我们只需要`HBuilder-Integrate-AS`这一个即可，其它都是相同作用，或用来在`android studio`中创建原始app项目，需要移植的文件，那些我们不需要管，`HBuilder-Integrate-AS`是uniapp官方已经配置好的，可以直接在`android studio`中打开并进行打包的最简单示例。

打开已下载好的`android studio`，`File-Open`打开上面下载解压的`HBuilder-Integrate-AS`，此时`android studio`会开始初始化编辑器，下载`HBuilder-Integrate-AS`所需要的生成插件，可以在编辑器底部选项卡中找到`Build`，打开后可以发现，编辑器正在下载一些东西，这里如果可以科学上网，或者`HBuilder-Integrate-AS`所需要的生成插件已经存在于下载路径中，那么这里会非常快的完成，如果下载不顺，或不能科学上网，这里最终会提示`time out`，而当`time out`后，`android studio`就无法进行离线打包，在`Build`工具栏下，你将无法找到`Build Bundle(s) / APK(s)`及`Generate Signed Bundle / APK...`选项，后续的一切操作都无法进行。

那么`android studio`在下载什么？这里影响的是我们离线打包功能，所以它下载的就是打包相关的插件，这个可以打开`HBuilder-Integrate-AS`文件夹内的`gradle/wrapper/gradle-wrapper.properties`文件查看，文件内有一行`distributionUrl=https\://services.gradle.org/distributions/gradle-6.5-all.zip`，这就是`android studio`打开项目后在下载的东西，如果你的电脑上这个版本的插件是首次用在一个项目上，`android studio`在打开这个项目后，就会先将该插件下载下来，无法下载，该项目就无法正常使用打包功能。

这些文件被下载到哪里了？如果未配置环境变量的话，`android studio`下载的所有`gradle`都会默认在C盘，这个可以打开`Tools - SDK Manager`选项，找到`Build,Execution,Deployment - Build Tools - Gradle`，点击打开后，找到上面的`General settings - Gradle user home`这里就是被下载的`gradle`存放的地方。打开这个文件夹，找到`wrapper/dists`文件夹，可以在里面找到被下载的`gradle`版本生成的文件夹，版本文件夹下是一个随机字符串的文件夹，这个文件夹下，就是存放被下载的`gradle`压缩包的地方了。

知道了下载什么，也知道了被下载到哪里了，那么我们可以手动帮它下载好，并放到它指定的目录下就行了，这里就用到了[安卓生成apk所需插件](https://services.gradle.org/distributions/)这个网址了，在这个网页内，找到与上面提到的`distributionUrl=https\://services.gradle.org/distributions/gradle-6.5-all.zip`中，名字完全一致的下载下来，并放到对应文件夹下即可。`uniapp`一般使用的一直是`gradle-6.5-all.zip`

我们将插件下载好放到指定文件夹下后，在`android studio`中，点击上面打开的底部选项卡中的`Build`控制台左侧的`重载`按钮，编辑器就会重新进行插件安装下载操作，这里需要说明的是，即使我们手动帮助下载了它所需要的插件压缩包，它还是需要一个漫长的过程去解压、去下载其它东西，这个过程还是有可能会超时断开，但相对来说不是必然了，耐心等待编辑器下载完所有东西后，再次点击工具栏的`Build`，其下拉选项中，就可以找到`Build Bundle(s) / APK(s)`及`Generate Signed Bundle / APK...`选项了。此时`android studio`的开发环境才算搭建完成

## 开发流程
在使用上面初始化项目利用`HBuilderX`开发完自己的项目后，接下来就需要进行打包`apk`了。

### 创建AppId
打包的第一件事儿就是创建`AppId`，这需要登录[uniapp开发者平台](https://dev.dcloud.net.cn/pages/common/login)，无账号的需要先注册账号，登陆后，在`应用管理-我的应用`页面，点击`创建应用`，选择应用类型、输入应用名称，即可创建一个新应用，应用创建成功后，应用列表已经给新应用分配了`AppId`，这就是接下来要用到的`AppId`了。

### 申请离线打包Key

#### 手动生成签名
这里就需要用到上面**准备工作**中的`5,6,7`项了，在创建完应用后，点击应用名称，会进入应用详情页，其中`各平台信息`选项卡中，是存放各平台配置信息的地方，点击新增，选择安卓平台，可以看到需要安卓应用签名，如果已经安装了[JAVA-JDK1.8.0_201](https://pan.baidu.com/s/1U4SWoYY6Gn4B4PjSJwy9Gg)，可以打开`cmd`，输入以下命令：
```
keytool -genkey -alias testalias -keyalg RSA -keysize 2048 -validity 36500 -keystore test.keystore
```
* testalias是证书别名，可修改为自己想设置的字符，建议使用英文字母和数字
* test.keystore是证书文件名称，可修改为自己想设置的文件名称，也可以指定完整文件路径
* 36500 是证书的有效期，表示100年有效期，单位天，建议时间设置长一点，避免证书过期

上述命令如果输入后，提示`keytool不是内部命令`，大概率是未配置环境变量，右键`我的电脑-属性-高级系统设置-环境变量-系统变量`，找到这里后，需要做以下操作：

1. 新建系统变量，变量名：`JAVA_HOME`，变量值：`D:\java\jdk-1.8`，这个变量值是安装`java-jdk`的路径，找到自己的安装路径复制到这里即可
2. 找到变量名`PATH`的系统变量，点击编辑，增加`%JAVA_HOME%\bin`和`%JAVA_HOME%\jre\bin`两个变量值
3. 新建系统变量，变量名：`CLASSPATH`，变量值：`%JAVA_HOME%\lib;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar`

以上配置完后，重启`cmd`，再次输入上面那个命令，即可正常走入接下来的流程：
```
Enter keystore password:  //输入证书文件密码，输入完成回车  
Re-enter new password:   //再次输入证书文件密码，输入完成回车  
What is your first and last name?  
  [Unknown]:  //输入名字和姓氏，输入完成回车  
What is the name of your organizational unit?  
  [Unknown]:  //输入组织单位名称，输入完成回车  
What is the name of your organization?  
  [Unknown]:  //输入组织名称，输入完成回车  
What is the name of your City or Locality?  
  [Unknown]:  //输入城市或区域名称，输入完成回车  
What is the name of your State or Province?  
  [Unknown]:  //输入省/市/自治区名称，输入完成回车  
What is the two-letter country code for this unit?  
  [Unknown]:  //输入国家/地区代号（两个字母），中国为CN，输入完成回车  
Is CN=XX, OU=XX, O=XX, L=XX, ST=XX, C=XX correct?  
  [no]:  //确认上面输入的内容是否正确，输入y，回车  

Enter key password for <testalias>   // 输入密钥的口令
        (RETURN if same as keystore password):  // 如果和密钥库口令相同，按回车 确认证书密码与证书文件密码一样（HBuilder|HBuilderX要求这两个密码一致），直接回车就可以
```

以上步骤完成后，证书就算成了，控制台会警告进行标准迁移，并给出了命令，这个不用管，如果迁移了标准，后面打包时，会报错keystore格式不对

此时证书生成完成，生成的证书在什么位置？看你执行命令时在什么位置，证书就放在什么位置。可以找到`test.keystore`文件。

这个证书需要保存好，之后打包会要求放到项目中。

证书已经生成了，如何查看uniapp需要的签名呢？输入以下命令：
```
keytool -list -v -keystore test.keystore  
Enter keystore password: //输入密码，回车
```

此时控制台会输入证书的详细信息，里面可以找到`证书指纹`项，其包含`SHA1`,`SHA256`，这两个是都包含的，`uniapp`在需要做一键登录时，还需要`MD5`的，而`MD5`默认是不显示的，这里需要下载[openssl](https://slproweb.com/products/Win32OpenSSL.html)，通过`openssl`获取`MD5`信息，这里下载后，最后的界面要给作者消费~，选项随便选即可。安装完成后就是设置环境变量了。

系统变量中新增变量名：`OPENSSL_HOME`，变量值：`D:\OpenSSL-Win64\bin`，这是看自己安装路径。在`PATH`变量名中新增`%OPENSSL_HOME%`变量值。然后重启电脑，打开`cmd`，输入以下命令：
```
openssl version
```
输出正常，说明安装已经成功，在生成的证书文件所在目录下，执行以下命令：
```
keytool -exportcert -alias test -keystore test.keystore -file output.crt
```
上述命令创建了`output.crt`证书文件，然后使用`openssl`获取`MD5`指纹：
```
openssl x509 -inform der -in output.crt -noout -fingerprint -md5
```
此时控制台即可以输出`MD5`的信息了，保存好`MD5`信息，此时`output.crt`文件就不需要了，可以直接删除
```
rm output.crt
```

#### 在线生成签名
除了上述自己手动生成签名的方法外，还可以通过线上方式生成，可以[点击这里](https://www.mocklib.com/keystore)，根据表单输入对应信息点击生成即可。

#### 生成离线打包key
到这里`uniapp`创建离线打包key所需要的所有签名就都准备就绪了，此时回到`uniapp`开发者平台，进入创建的应用详情，打开各平台信息页面，点击新增，选择安卓平台、正式版、包名一般为反向域名（com.demo.www）格式，及上面获取到的`SHA1`、`MD5`、`SHA256`，提交后，安卓平台信息生成，点击表格列表中`离线打包key`列的`创建`按钮，确认后`离线打包key`即已生成，此时`创建`按钮，变为`查看`按钮，点击`查看`，弹窗中即已显示生成的`离线打包key`，这个后期打包时会用到。

### 应用配置
目前已准备好的东西：
1. AppId
2. 离线打包Key
3. 已开发的uniapp项目源码
4. `HBuilder-Integrate-AS`框架源码（`android studio离线打包使用`）
5. 安卓证书文件（`xxx.keystore`文件）

#### 项目源码打包
项目已开发完成，首先需要把项目打包成app理解的源码，这需要用到`HBuilderX`编辑器，而`HBuilderX`打包本地`App`需要配置`AppId`，打开`src/manifest.json`文件，将上面步骤获取到的`AppId`复制该json文件的`appid`字段内（非`HBuilderX`编辑器打开）。

`appid`更新后，使用`HBuilderX`打开项目，点击`发行-原生APP-本地打包-生成本地App打包资源`，如打包成功，控制台会输出`项目xxx导出成功，路径为：xxxx/dist/resources/appid/www`。

#### 框架源码修改
将`HBuilder-Integrate-AS`框架源码复制一份，放到其它位置。

1. 修改框架源码的文件夹名，将`HBuilder-Integrate-AS`改为自己项目名（英文）。
2. 修改`HBuilder-Integrate-AS/HBuilder-Integrate-AS.iml`文件名，修改为`自己项目名.iml`
3. 修改`simpleDemo`文件夹名，修改为`app名`（英文），也可以改为其它名字，最终打包的apk会以该文件夹名命名，如`xxx-release.apk`
4. 修改`simpleDemo/simpleDemo.iml`文件名，修改为`app名.iml`
5. 将安卓证书文件放置到`项目名/app名`文件夹下，并删除`test.jks`文件
6. 删除`项目名/app名/src/main/assets/apps`文件夹下已存在的`appid`文件夹及其内所有文件
7. 将上一步打包的项目源码中`项目名/dist/resources`文件夹下`appid`的文件夹整个复制，并放到框架源码的`项目名/app名/src/main/assets/apps`文件夹下
8. 编辑器打开框架源码，全局搜索`HBuilder-Integrate-AS`，全局替换为上面修改后的项目名
9. 全局搜索`simpleDemo`，全局替换为`app名`
10. 修改`项目名/app名/src/main/res/values/strings.xml`文件中`app_name`的字段值，修改为与`项目名/app名/src/main/assets/apps/appid/www/manifest.json`文件中的`name`字段值一致，该值为安装到手机上桌面显示的应用名称。
11. 全局搜索`com.android.simple` 全局替换为自己在开发者平台创建的应用中，应用详情-各平台信息中，在上面创建的安卓平台时，有让输入包名，将那个包名在此处做全局替换
12. 修改`项目名/app名/src/main/AndroidManifest.xml`文件，将`开发者需登录https://dev.dcloud.net.cn/申请签名`修改为上面创建好的离线打包`AppKey`
13. 修改`项目名/app名/build.gradle`文件，修改`android.defaultConfig.versionCode`、`android.defaultConfig.versionName`使其与项目源码中的`manifest.json`文件中的这两个字段一致
14. 修改`项目名/app名/build.gradle`文件，修改`android.signingConfigs.config`内的参数，这里就是上面创建的证书相关: `keyAlias`： 创建证书时的别名、`keyPassword`:创建证书时输入的密码、`storeFile`：创建的证书文件，在上面已经让把创建的证书文件放到了`app名`目录下，所以这里引用文件可以直接`file(xxx.keystore)`，`file`函数内直接写证书文件即可，`storePassword`：创建证书时，最后输入的密码，一般与开头的口令密码一致
15. 修改`项目名/app名/src/main/assets/data/dcloud_control.xml`文件中`appid`的值，修改为自己应用的`appid`

以上全部修改后，源码部分的修改就完成了

#### 配置应用图标和启动界面
在`项目名/app/src/main/res/drawable`文件夹下，有`icon.png`、`push.png`、`splash.png`，分别对应应用图标、推送消息的图标、应用启动页，还可以根据分别率配置不同的图片，不同文件夹下有不同的适配，文件夹名是固定的：`drawable`，`drawable-ldpi`，`drawable-mdpi`，`drawable-hdpi`，`drawable-xhdpi`，`drawable-xxhdpi`

<table>
  <tr>
    <th>文件夹名</th>
    <th>密度</th>
    <th>密度范围</th>
    <th>icon建议尺寸</th>
    <th>启动页建议尺寸</th>
  </tr>
  <tr>
    <td>drawable</td>
    <td>默认</td>
    <td>默认</td>
    <td>48 * 48</td>
    <td>360 * 640</td>
  </tr>
  <tr>
    <td>drawable-ldpi</td>
    <td>低密度</td>
    <td>0dpi ~ 120dpi</td>
    <td>36 * 36</td>
    <td>360 * 640</td>
  </tr>
  <tr>
    <td>drawable-mdpi</td>
    <td>中等密度</td>
    <td>120dpi ~ 160dpi</td>
    <td>48 * 48</td>
    <td>360 * 640</td>
  </tr>
  <tr>
    <td>drawable-hdpi</td>
    <td>高密度</td>
    <td>160dpi ~ 240dpi</td>
    <td>72 * 72</td>
    <td>540 * 960</td>
  </tr>
  <tr>
    <td>drawable-xhdpi</td>
    <td>超高密度</td>
    <td>240dpi ~ 320dpi</td>
    <td>96 * 96</td>
    <td>720 * 1280</td>
  </tr>
  <tr>
    <td>drawable-xxhdpi</td>
    <td>超超高密度</td>
    <td>320dpi ~ 480dpi</td>
    <td>144 * 144</td>
    <td>1080 * 1920</td>
  </tr>
  <tr>
    <td>drawable-xxxhdpi</td>
    <td>超超超高密度</td>
    <td>480dpi ~ 640dpi</td>
    <td>192 * 192</td>
    <td>1440 * 2560</td>
  </tr>
  <tr>
    <td>drawable-nodpi</td>
    <td>无缩放</td>
    <td>无缩放</td>
    <td>48 * 48</td>
    <td>360 * 640</td>
  </tr>
</table>

#### xxx.9.png格式启动页制作
本人PS操作熟练，所以这里不介绍比较难用的`android studio`制作`xxx.9.png`格式的启动页方法，用PS知道原理的情况下，制作启动页还是很快的

制作`xxx.9.png`格式启动页，主要是为了解决在不同尺寸的手机上，启动页变形问题，其制作原理很简单，利用PS制作也很简单

启动页主要元素：`logo`图标、一句广告语、背景

1. 根据上面表格提供的启动页尺寸，在PS里新建一个同样尺寸的画布，或直接打开对应尺寸的启动页图片
2. 放大视图，在画布的上下左右各拉一条距离边界1像素的辅助线
3. 利用选框工具，选中中间区域
4. 新建画布的情况下，新建图层，并填充颜色；打开已有的启动图片则是进行反选，反选后，按`delete`键，即删除启动页边界1像素的内容
5. 新建画布上一步制作了中间区域的背景后，将`logo`拖入上半部合适位置，居中显示，并在下半部合适位置放置一句广告语，同样居中显示
6. 以上完成后，开始绘制启动页的特殊内容，即边界留出的1像素区域内容。主要是在这1像素区域内绘制颜色值为`#000000`的黑线
7. 右侧即底部是最简单的，右侧根据上下辅助线利用选框工具框选宽度1像素、高度从上辅助线到下辅助线的区域，填充`#000000`。
8. 底部根据左右辅助线利用选框工具框选高度1像素，宽度从左辅助线到右辅助线的区域，填充`#000000`.
9. 顶部的黑线区域需要再做一条辅助线，以`logo`和广告语两者的左边界更靠近左侧一方的左边界为辅助线位置，根据左辅助线及此次新做辅助线利用选框工具框选高度1像素，宽度为两条辅助线之间宽度的区域，填充`#000000`
10. 左侧的黑线区域需要再做两条辅助线，以`logo`的底边界做一条辅助线，以广告语的顶边界做一条辅助线，根据此次新作两条辅助线利用选框工具框选宽度1像素，高度为两条辅助线之间高度的区域，填充`#000000`

以上`xxx.9.png`格式启动页就制作完成了，导出为`png`格式，命名为`splash.9.png`，根据尺寸，将其放到对应文件夹下即可。

## 打包apk
以上全部配置完成后，再通过`android studio`编辑器打开框架源码项目。打开项目后，大概率会发现初始化时build失败，因为新打开的项目`gradle`配置不对，打开`Tools- SDK Manager`找到`Build,Execution,Deployment-Build Tools - Gradle`，点击打开后，需要看以下`Gradle user home`所指向的路径有没有自己这个项目所需要的`gradle`包，如果没有，将路径指向含有该包的路径，另一个需要看以下`Gradle projects - Gradle JDK`选项是否正确，我们使用的是`uniapp`提供的`HBuilder-Integrate-AS`,其支持的是`gradle-6.5-all.zip`版本的`gradle`包，该包对应的是`jdk`版本是上面我们下载`1.8`版本，所以，这里需要将`jdk`版本路径指向我们安装的`1.8`版本，此时再重载`Build`，就可以正常初始化了。正常初始化后，工具栏`Build`选项下，已出现`generate Signed Bundle / APK...`选项

1. 点击`build-generate Signed Bundle / APK...`选项
2. 弹窗选择APK
3. next后，需要填写证书相关信息，key store path 可以选择项目目录下存放的`xxx.keystore`文件，密码及别名都填入自己生成证书时设置的即可。
4. next后，选择打包输出的位置、选择一个打包版本，一般选择`release`版本，即正式版本，等待打包完成即可
5. 打包完成后，可在`项目名/app/release`文件夹下，或者`项目名/release`文件夹下找到`app-release.apk`文件，这个就可以拿来在手机上直接安装了，也可以拿去应用市场上架了

至此，离线打包安卓apk的整个流程就全部完成了。

**每次重新打包或者项目有更新后，要重新运行时，可以先执行Build-Clean Project操作，否则项目可能获取不到最新代码**

## 自定义基座配置
基座其实就是一个app，新开发的页面，可以在手机上面显示，查看效果，并可以在`HBuilderX`终端查看调试信息，其无法实现热更新，所以有更新就需要重新打包，重新运行。

### 离线打包方式

离线打包方式制作自定义基座，无需将代码上传到云端，需要做以下修改及操作：

1. 修改框架源码`项目名/app名/src/main/assets/data/dcloud_control.xml`文件：
```
<apps debug="true" syncDebug="true">
```
增加`debug`相关代码

1. 添加`debug-server-release.arr`扩展，该扩展在下载的`App离线SDK`包内与`HBuilder-Integrate-AS`同级文件夹`SDK/libs`文件夹内，复制该扩展，粘贴到`项目名/app名/libs`文件夹下

2. 修改框架源码`项目名/app名/build.gradle`文件：
```
dependencies {
  ....
  implementation 'com.squareup.okhttp3:okhttp:3.12.12'
  implementation 'com.squareup.okio:okio:1.15.0'
}
```
在`dependencies`中检查是否有以上两个包，如果没有就加上。

1. 进行离线打包，最后选择打包版本为debug版本，打包路径也可以选择，打包完成后，可以在`选择的打包路径/debug`文件夹下找到`app名-debug.apk`，复制该包，并粘贴到`uniapp`项目的`dist/debug`文件夹下，`dist`下无`debug`文件夹，可自行创建，包名需要修改为`android_debug.apk`

2. 打包完成后，已经获取到`debug.apk`了，如果有新开发页面想在手机端预览及调试，就需要重新打包`debug`版本。

**每次重新打包或者项目有更新后，要重新运行时，可以先执行Build-Clean Project操作，否则项目可能获取不到最新代码**

### `HbuilderX`制作
除了上述离线形式，还可以直接通过`HbuilderX`进行制作，`运行-运行到手机或模拟器-制作自定义基座`，选择安卓，这里主要是证书相关的内容填上，即可进行打包，一般安心打包可能不会成功，只能选择普通打包，打包成功后，会自动在`dist`文件夹下生成`debug/android_debug.apk`包。

### 基座调试

1. 获取到`debug`版本的`apk`后，此时电脑连接手机，并打开手机的`USB调试`（通常在手机的设置-系统和更新-开发人员选项下），开启`USB调试`，开启`仅充电模式下允许ADB调试`，关闭`监控ADB安装应用`

2. `HBuilderX`运行项目，并点击`运行-运行到手机或模拟器-运行到Android App基座`，此时如果一切正常，手机就会下载当前运行项目的app，安装完成打开后，即可与编辑器进行正常调试。

3. 一般都会出现`adb.exe`冲突问题，点击`运行-运行到手机或模拟器-ADB路径设置`，找到`HBuilderX`安装路径，`\HBuilderX\plugins\launcher-tools\tools\adbs\adbs.exe`将编辑器自身的`adbs.exe`路径复制到设置中，并勾选`真机运行时打开调试视图`，重启`HBuilderX`，再次`运行-运行到手机或模拟器-制作自定义基座`，`adb.exe`冲突问题就解决了。

4. 此时可能运行后，页面还是会打开失败，提示`请求的页面无法打开`，这个需要将源码视图的`manifest.json`文件及`pages.json`文件中所有的注释去掉，保存重新运行，手机上就可以正常显示页面了。

# 离线打包IOS