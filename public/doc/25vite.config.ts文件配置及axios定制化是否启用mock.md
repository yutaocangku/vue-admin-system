﻿# 项目配置文件修改
## 开发环境、生产环境配置
项目要以`ip:port`的形式运行开发环境，需要依赖获取`ip`的插件：
```
yarn add ip -D
```
修改`vite.config.ts`文件：
```
import { resolve } from "path";
import { UserConfigExport, ConfigEnv,loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import ElementPlus from "unplugin-element-plus/vite";
import { viteMockServe } from "vite-plugin-mock";
import svgLoader from "vite-svg-loader";
import { wrapperEnv, regExps } from "./build";
import ip from 'ip';

// 获取ip地址
const ipStr = ip.address();
// 路径查找
const pathResolve = (dir: string): string => {
  return resolve(__dirname, dir);
};

// 当前执行node命令时文件夹的地址（工作目录）
const root: string = process.cwd();
// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfigExport =>{
  const {
    VITE_PORT,
    VITE_OUTDIR,
    VITE_PUBLIC_PATH,
    VITE_PROXY_API_PATH,
    VITE_BASE_API_PATH
  } = wrapperEnv(loadEnv(mode, root));
  const prodMock = true;
  return {
    base:VITE_PUBLIC_PATH,
		root,
    // 开发服务
		server:{
			https:false,
			port:VITE_PORT,
			host:ipStr,
			// 本地跨域代理
			proxy:VITE_BASE_API_PATH.length>0?{
				[VITE_PROXY_API_PATH]:{
					target:VITE_BASE_API_PATH,
					changeOrigin:true,
					rewrite:(path:string)=>regExps(path,VITE_PROXY_API_PATH)
				}
			}:null,
			open:`http://${ipStr}:${VITE_PORT}`
		},
    // 生产环境配置
		build:{
			outDir:VITE_OUTDIR,
			sourcemap:false,
			brotliSize:false,
			// 消除打包大小超过500kb警告
			chunkSizeWarningLimit:2000,
		},
    plugins: [
      vue(),
      ElementPlus({}),
      svgLoader(),
      viteMockServe({
        mockPath: "./mock", // 模拟接口api文件存放的文件夹
        watchFiles: true, // 将监视文件夹中的文件更改。 并实时同步到请求结果
        localEnabled: command === "serve", // 设置是否启用本地 xxx.ts 文件，不要在生产环境中打开它.设置为 false 将禁用 mock 功能
        prodEnabled: command !== "serve" && prodMock, // 设置生产环境是否启用 mock 功能
        injectCode: `
          import { setupProdMockServer } from './mockProdServer';
          setupProdMockServer();
        `, // 如果生产环境开启了mock功能，该代码会注入到injectFile对应的文件的底部，默认为main.{ts,js}
        injectFile:resolve(process.cwd(),'src/main.{ts,js}'),
        logger: true
      }),
    ],
    resolve: {
      alias: {
        '@': pathResolve('src'),
        '^': pathResolve('build'),
        '#': pathResolve('types'),
      }
    },
    css: {
      // css预处理器
      preprocessorOptions: {
        scss: {
          // 引入 var.scss 这样就可以在全局中使用 var.scss中预定义的变量了
          // 给导入的路径最后加上 ; 
          additionalData: `@import "@/style/var.scss";`
        }
      }
    },
  }
}
```
## 请求地址配置
开发环境与生产环境的配置完成了，还需要对封装的`axios`请求进行修改。
#### 基本配置
以上环境变量配置完成了，还需要对`axios`的请求地址进行配置，目前项目使用的是`mock`模拟后端请求，环境变量中配置的后端地址暂时都为空，如果项目已经有后端请求接口，就不需要再使用`mock`模拟后端数据了，直接请求后端接口即可，这里就需要对`axios`的封装方法进行修改，修改`src/utils/http/index.ts`文件：
```
import { loadEnv } from "^/index";
const { VITE_BASE_API_PATH } = loadEnv();


baseURL: VITE_BASE_API_PATH,
```
以上在`src/utils/http/index.ts`文件中引入获取环境变量的方法，获取到后端接口的环境变量配置，并赋值给`axios`的`baseURL`，这样在请求接口的地方只需要写接口的具体路径连接部分即可，便于后期维护。

这里配置后，可以先做一个测试，将开发环境的`VITE_BASE_API_PATH`环境变量随意赋值一个`http://ip:端口号`形式的链接，然后重新运行项目，此时会发现项目并不能正常运行，这是因为此时项目已经不再走`mock`模拟的接口，`VITE_BASE_API_PATH`有了值就意味着已经有了后端接口，已不需要再使用`mock`，但在项目开发过程中，是会有这种情况产生的：已有后端接口，但有了新需求，新需求后端接口还未产生，前端需要调用接口数据进行前端调试，这个时候，还是需要使用`mock`，这个时候，就需要去配置哪些请求可以走后端接口，哪些请求需要走`mock`接口。而控制的核心就是走`mock`接口的请求，在请求拦截中，将`baseURL`设置为空即可。
#### 适配mock配置
这里就需要进行单独配置了，和前几章针对`loading`以及重复请求问题的配置一样，这里也需要在自定义请求配置中，添加一个参数，用来控制是否使用`mock`模拟接口。当后端接口给到前端后，前端只需要将调试阶段添加的控制使用`mock`接口的参数删除或者改变其值即可。这里在`VITE_BASE_API_PATH`配置了具体的值后，需要将目前已有的接口全部修改，让它们能正常走`mock`。
1. 修改所有的`api`接口：
`src/api/routes.ts`文件：
```
import { http } from "../utils/http";

// 获取动态菜单数据
export const getAsyncRoutes = (config:object) => {
  return http.request("post", "/getAsyncRoutes",null,config);
};
```
`src/api/user.ts`文件：
```
import { http } from "../utils/http";

// 登陆
export const login = (data:object,config:object) => {
  return http.request("post", "/login",data,config);
};
// 更新Token
export const refreshToken = (data:object,config:object) => {
  return http.request("post", "/refreshToken",data,config);
};
// 退出登陆
export const logout = (config:object) => {
  return http.request("post", "/logout",null,config);
};
```
2. 所有调用接口的地方传入控制是否使用`mock`的自定义参数配置
`src/views/Login/index.vue`文件调用接口：
```
login(loginData,{config:{mockEnable:true}})
```
`src/router/utils.ts`文件调用接口：
```
getAsyncRoutes({config:{mockEnable:true}})
```
`src/layout/components/headerPanel.vue`文件调用接口：
```
logout({config:{mockEnable:true}})
```
`src/views/News/Index/index.vue`文件调用接口：
```
getNewsListData({config:{showLoading:false,cancelRepeatDisabled:false,mockEnable:true}})
```
`src/views/News/NewsDetail/index.vue`文件调用接口：
```
getNewsDetailData({id:currentID},{config:{showLoading:true,loadingStyle:{lock:false,text:"加载中...",background:'rgba(255,255,255,.7)'},mockEnable:true}})
```
`src/utils/http/index.ts`文件调用接口：
```
return refreshToken({refreshToken:data.refreshToken},{config:{mockEnable:true}})
```
先修改以上接口，通过调用接口并传入`mockEnable`这个自定义参数，来决定是否启用`mock`，接下来需要修改`src/utils/http/index.ts`文件中的请求拦截：
```
// 请求拦截
private httpInterceptorsRequest(): void {
  AxiosHttp.axiosInstance.interceptors.request.use(
    (config: AxiosHttpRequestConfig) => {
      const $config = config;
      console.log($config,"请求拦截");
      // 是否使用mock模拟的后端接口
      if($config.config&&$config.config.mockEnable){
        $config.baseURL="";
      }
      // 自定义参数字段不存在，或者自定义参数存在，但是取消重复请求功能未被禁用
      if(!$config.config||($config.config&&!$config.config.cancelRepeatDisabled)){
        judgeRending($config);
      }
      if($config.config.showLoading){
        startLoading($config.config.loadingStyle);
      }
      // 开启进度条动画
      NProgress.start();
      // 优先判断请求是否传入了自定义配置的回调函数，否则执行初始化设置等回调
      if (typeof config.beforeRequestCallback === "function") {
        config.beforeRequestCallback($config);
        this.beforeRequestCallback = undefined;
        return $config;
      }
      // 判断初始化状态中有没有回调函数，没有的话
      if (AxiosHttp.initConfig.beforeRequestCallback) {
        AxiosHttp.initConfig.beforeRequestCallback($config);
        return $config;
      }
      // 确保config.url永远不会是undefined，增加断言
      if(!config.url){
        config.url = ""
      }
      // 登录接口和刷新token接口不需要在headers中回传token，在走刷新token接口走到这里后，需要拦截，否则继续往下走，刷新token接口这里会陷入死循环
      if (config.url.indexOf('/refreshToken') >= 0 || config.url.indexOf('/login') >= 0) {
        return $config
      }
      // 回传token
      const token = getToken();
      // 判断token是否存在
      if (token) {
        const data = JSON.parse(token);
        // 确保config.headers永远不会是undefined，增加断言
        if(!config.headers){
          config.headers = {}
        }
        config.headers["Authorization"] = "Bearer " + data.accessToken;
        return $config;
      } else {
        return $config;
      }
    },
    error => {
      // 当前请求出错，当前请求加1的loading也需要减掉
      if(error.config.config.showLoading){
        endLoading();
      }
      return Promise.reject(error);
    }
  );
}
```
修改`src/utils/http/types.d.ts`文件：
```
// 定义自定义回调中请求的数据类型
export interface AxiosHttpRequestConfig extends AxiosRequestConfig {
  config?:{
    showLoading?:boolean;
    loadingStyle?:{
      target:object|string;
      body:boolean;
      fullscreen:boolean;
      lock:boolean;
      text:string;
      spinner:string;
      background:string;
      'custom-class':string;
    },
    cancelRepeatDisabled?:boolean;
    mockEnable?:boolean;
  };
  beforeRequestCallback?: (request: AxiosHttpRequestConfig) => void; // 请求发送之前
  beforeResponseCallback?: (response: AxiosHttpResponse) => void; // 相应返回之前
}
```
以上修改完，再次重新运行项目，就可以正常使用`mock`接口，而如果后期有了真正的后端接口，这些接口只需要将自定义的`mockEnable`字段删除，或者其值改为`false`即可实现正常走后端接口获取数据。

## 插件配置
#### vite-plugin-live-reload
该插件作用是实时重新加载，用来解决某些路径下的文件修改后，项目不会热更新问题
```
yarn add vite-plugin-live-reload -D
```
修改`vite.config.ts`文件：
```
import liveReload from "vite-plugin-live-reload";

# 在`plugins`内新增以下内容
// 解决某些文件修改后无法进行热更新，页面不会自动刷新问题
liveReload(["src/layout/**/*", "src/router/**/*"]),
```
#### @vitejs/plugin-legacy
该插件主要用来在打包后，实现为文件提供传统浏览器兼容性支持，`vite`的默认浏览器支持较高，`ie11`也不再支持
```
yarn add @vitejs/plugin-legacy -D
```
可以对该功能进行配置，是否需要开启该功能，在`.env.production`文件中配置环境变量：
```
# 是否为打包后的文件提供传统浏览器兼容性支持 支持 true 不支持 false
VITE_LEGACY = true
```
在`.env.staging`文件中配置环境变量
```
# 是否为打包后的文件提供传统浏览器兼容性支持 支持 true 不支持 false
VITE_LEGACY = false
```
在`types/global.d.ts`中添加对应类型定义：
```
VITE_LEGACY:boolean;
```
再修改`build/index.ts`文件，添加对应默认值：
```
// 此处为默认值，无需修改
  const ret: ViteEnv = {
    VITE_PORT: 11564,
    VITE_OUTDIR: "",
    VITE_PUBLIC_PATH: "",
    VITE_PROXY_API_PATH,
    VITE_BASE_API_PATH,
    VITE_LEGACY:false
  };
```
然后修改`vite.config.ts`文件：
```
import legacy from "@vitejs/plugin-legacy";


const {
    VITE_PORT,
    VITE_OUTDIR,
    VITE_PUBLIC_PATH,
    VITE_PROXY_API_PATH,
    VITE_BASE_API_PATH,
    VITE_LEGACY
  } = wrapperEnv(loadEnv(mode, root));



# 在`plugins`内新增以下内容
// 是否为打包后的文件提供传统浏览器兼容性支持
VITE_LEGACY
  ? legacy({
      targets: ["ie >= 11"],
      additionalLegacyPolyfills: ["regenerator-runtime/runtime"]
    })
  : null,
```
#### rollup-plugin-visualizer
打包分析插件，类似于`webpack`的`webpack-bundle-analyzer`
```
yarn add rollup-plugin-visualizer -D
```
修改`vite.config.ts`文件：
```
import { visualizer } from "rollup-plugin-visualizer";


const {
    VITE_PORT,
    VITE_OUTDIR,
    VITE_PUBLIC_PATH,
    VITE_PROXY_API_PATH,
    VITE_BASE_API_PATH,
    VITE_LEGACY
  } = wrapperEnv(loadEnv(mode, root));
  const prodMock = true;
  // 获取当前执行命令事件
  const lifecycle = process.env.npm_lifecycle_event;


# 在`plugins`内新增以下内容
// 打包分析
lifecycle === "report"
? visualizer({ open: true, brotliSize: true, filename: "report.html" })
: null
```
以上修改让只在执行`report`命令时，才会进行打包分析，其他生成生产环境，测试环境时都不会执行打包分析的操作，所以这里需要在`package.json`中再加上`report`命令：
```
"scripts": {
  "start": "cross-env vite --mode development",
  "stage": "rimraf stage && cross-env vite build --mode staging",
  "build": "rimraf dist && cross-env vite build --mode production",
  "report": "rimraf dist && cross-env vite build --mode production",
  "preview": "vite preview"
},
```

#### vite-plugin-remove-console
开发环境往往需要用到大量`console.log`进行调试，手动去除就太麻烦了，该插件主要用于在生成生产环境时，自动去除所有的`console.log`
```
yarn add vite-plugin-remove-console -D
```
修改`vite.config.ts`文件：
```
import removeConsole from "vite-plugin-remove-console";

# 在`plugins`内新增以下内容
// 线上环境删除console
removeConsole(),
```
`vite.config.ts`文件的基础配置差不多就这些，后期还会根据引入的插件，增加一些插件相关配置。