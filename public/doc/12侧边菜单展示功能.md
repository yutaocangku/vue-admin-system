﻿# 侧边菜单栏

路由数据与菜单数据在这里可以理解为两个概念，路由数据在实例化以后，相对应的路由就已经实现了，如同本项目，目前侧边菜单栏展示功能还未做，但可以通过地址栏输入对应路由来跳转页面，侧边菜单栏数据与路由数据有交集部分，也都各自可以有对方可以不包含的部分，菜单数据中可以不包含路由数据中实际存在的路由，但不想在菜单栏中展示的部分数据；路由数据中也可以不包含为了体现层级而无具体路由页面的非页节点路由数据。所以在数据处理上，也都会有侧重，都是通过静态路由与动态获取路由组合后的数据，因路由功能和菜单展示功能，处理成两组略有差异性的数据。前文已经处理过路由数据，本文将会解决菜单数据处理问题。
## 路由数据另存为菜单数据
将路由数据存储为菜单数据，这里需要分别将静态路由和动态获取路由都分别整理一份，最终组合成需要的菜单数据。
### 静态路由数据转存菜单数据
静态路由数据转存为菜单数据处理方式与路由实例化处理方式不同，修改`src/router/utils.ts`文件中的静态路由处理方法：

```
// 处理静态路由
const constantRoutesFilter = (data:any,isAsync:boolean,isMenu:boolean) => {
  let newData = [];
  if(isAsync){
    // 需要的是动态路由子集的部分
    newData = data.filter((item:any)=>{
      return item.meta.pid != 0;
    })
  }else{
    if(isMenu){
      // 作为菜单数据使用，不需要根路由Layout
      newData = data.filter((item:any)=>{
        return item.meta.pid == 0;
      });
    }else{
      // 需要的是可直接实例化的静态路由部分
      newData.push({
        path: "/",
        name: "Layout",
        component: Layout,
        redirect: "/home",
        children:[],
        meta:{
          sort:1
        }
      });
      let sonData = data.filter((item:any)=>{
        return item.meta.pid == 0;
      });
      newData[0].children = sonData;
    }
  }
  return newData;
}
```
然后修改`src/router/index.ts`中的静态路由获取部分代码，以及新增导出静态路由处理成菜单数据的代码：
```
// 获取处理后的静态路由（未获取动态路由的情况下，属于动态路由子集的路由需要先剔除掉，只实例化静态就能展示的路由）
const constantRootRoutes = constantRoutesFilter(cloneDeep(routes),false,false);
const constantNotRootRoutes = constantRoutesFilter(cloneDeep(routes),true,false);

// 导出静态路由作为菜单数据使用
export const constantRouteMenus = constantRoutesFilter(cloneDeep(routes),false,true).concat(...remainingRouter);
```
### 动态路由数据转存菜单数据
动态路由数据部分，需要在动态路由和静态路由中归属动态路由部分的数据整合后，将整合的数据与静态菜单数据进行整合，形成完整的菜单数据，这里需要用到`vuex`对菜单数据进行管理，在`src/store/modules`文件夹下，新建`routeMenus.ts`文件，作为菜单数据的管理文件：
```
import { Module } from 'vuex'
import { RouteMenus } from "../types";
import { constantRouteMenus } from "@/router";

const routeMenusModule: Module<RouteMenus,any> = {
  namespaced: true,
  state: {
    // 路由菜单数据
    routeMenus: [],
  },
  mutations:{},
  actions: {
    // 获取所有路由菜单数据（静态+动态获取部分）
    getWholeRouteMenus({state},routes) {
      if(state.routeMenus.length > 0){
        return;
      }
      state.routeMenus = constantRouteMenus.concat(routes);
      console.log(state.routeMenus,"菜单数据");
    },
  }
};
export default routeMenusModule
```
修改`src/store/types.ts`文件：
```
export type RouteMenus = {
  routeMenus:Object[];
}
```
然后修改`src/router/utils.ts`文件中的`routerToTree`方法：
```
// 将规范路由的一维数组转为树形结构
const routerToTree = (arrRoutes:any) => {
  var parents = arrRoutes.filter(function (item:any) {
      return item.meta.pid == 0;
  });
  var children = arrRoutes.filter(function (item:any) {
      return item.meta.pid != 0;
  });
  // 递归处理动态路由层级
  convert(parents, children);
  console.log(parents,"转为路由嵌套形式的路由");
  // 将转化为树形结构的路由数据传递给vuex去处理成菜单数据
  store.dispatch('routeMenus/getWholeRouteMenus',cloneDeep(parents));
  return parents;
}
```
此时初步的菜单数据整合就完成了，还需要对菜单数据进行处理，过滤掉不需要在菜单中展示的路由，以及菜单根据`sort`进行升序排序，这里需要封装两个方法，在`src/router/utils.ts`新增两个封装函数：
```
// 树形结构数据递归排序
export const treeToAscending = (arr:any) =>{
  var newTree = ascending(arr);
  newTree.forEach(function(item:any){
    if(item.children&&item.children.length>1){
      item.children = ascending(item.children);
    }
  });
  return newTree;
};

// 按照路由中meta下的sort等级升序来排序路由
export const ascending = (arr:any) => {
  return arr.sort((a: any, b: any) => {
      return a?.meta?.sort - b?.meta?.sort;
  });
};

// 过滤meta中showLink为false的路由
export const filterMenuTree = (data:any) => {
  const newTree = data.filter((v:any) => v.meta.showLink);
  newTree.forEach((v:any) => v.children && (v.children = filterMenuTree(v.children)));
  return newTree;
};
```
修改`src/store/modules/routeMenus.ts`文件:
```
import { treeToAscending,filterMenuTree } from "@/router/utils";

// 获取所有路由菜单数据（静态+动态获取部分）
getWholeRouteMenus({state},routes) {
  if(state.routeMenus.length > 0){
    return;
  }
  state.routeMenus = filterMenuTree(treeToAscending(constantRouteMenus.concat(routes)));
  console.log(state.routeMenus,"菜单数据");
},
```
此时运行项目，再次打开控制台查看数据，菜单数据中不存在新闻详情的数据，而最终路由数据中是有新闻详情的路由数据的。
## 解决初始化路由执行多遍问题
在集成`vue-router`章节中遗留了打开控制台看，初始化路由会执行两遍的问题，这里在用`vuex`对菜单数据进行管理后，这个问题就有了解决办法，在导航守卫中执行初始化路由的地方，通过`vuex`获取路由菜单数据，如果路由菜单数据不是空数组，说明已经走过初始化路由，就不用再去执行一遍了，先修改`src/router/index.ts`文件：
```
import store from "@/store";


router.beforeEach((to, _from, next) => {
  const token = getToken();
  NProgress.start();
  if(token){
    if(_from?.name==="Login"||!_from?.name){
      // 如果是从登录页过来的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
      if (store.state.routeMenus.routeMenus.length === 0){
        initRouter(constantNotRootRoutes).then(() => {
          router.push(to.path);
        });
      }
      next();
    }else{
      // 如果是其他页面跳转的，且其他页面的name存在，则直接进行下一步
      next();
    }
  }else{// 未登录
    // 当前访问页面不是登陆页面
    if (to.path !== "/Login") {
      // 当前访问页面包含在路由白名单中，可直接跳转
      if (whiteList.includes(to.path)) {
        next();
      } else {
        // 当前访问页面需要登录才能访问，需先跳转到登陆页，并将当前访问路由通过链接保存，在登陆后，获取该路由进行跳转
        next(`/Login?redirect=${to.path}`);
      }
    } else {
      // 当前访问页面本身就是登录页，直接跳转
      next();
    }
  }
});
```
代码加上后，页面会报`store`类型错误，需要先声明数据类型结构，修改`src/store/types.ts`文件：
```
import { Store } from 'vuex'
// 有新增状态管理模块都需要更新这里
export type State = {
  routeMenus: RouteMenus;
}
export type RouteMenus = {
  routeMenus:Object[];
}
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store<State>
  }
}
```
然后修改`src/store/index.ts`文件：
```
import { createStore } from 'vuex';
import { State } from './types';

// Vite supports importing multiple modules from the file system using the special import.meta.glob function
// see https://cn.vitejs.dev/guide/features.html#glob-import
const modulesFiles = import.meta.globEager('./modules/*.ts');
const pathList: string[] = [];

for (const path in modulesFiles) {
  pathList.push(path);
}

const modules = pathList.reduce((modules: any, modulePath: string) => {
  const moduleName = modulePath.replace(/^\.\/modules\/(.*)\.\w+$/, '$1');
  const value = modulesFiles[modulePath];
  modules[moduleName] = value.default;
  return modules;
}, {});

const store = createStore<State>({
  modules
});

export default store
```
此时报错就消失了，再次运行项目，控制台的打印数据中，初始化路由执行多遍问题就已解决了。路由问题解决，菜单数据也已在`vuex`中进行管理，接下来就是获取`vuex`中保存的菜单数据，并在侧边栏进行展示。
## 侧边栏菜单数据展示

### logo组件
先在侧边栏中增加一个logo组件，在`src/layout/components`文件夹下新建`logo`组件，`src/layout/components/logo/index.vue`：
```
<script setup lang="ts">
const title = "vue admin system"
</script>

<template>
  <div class="logo">
    <router-link :title="title" class="logo-link" to="/">
      <img class="logo-img" src="@/assets/logo.png" />
      <h1 class="logo-title">{{ title }}</h1>
    </router-link>
  </div>
</template>

<style lang="scss" scoped>
</style>
```
修改`src/layout/index.vue`文件，引入`logo`组件：
```
<script setup lang="ts">
import appMain from "./components/appMain.vue";
import Logo from "./components/logo/index.vue"
</script>

<template>
  <div class="app-wrapper">
    <!-- 侧边导航栏 -->
    <div class="sidebar-container">
      <Logo />
    </div>
    <div class="main-container">
      <!-- 顶部导航栏 -->
      <div class="fixed-header"></div>
      <!-- 标签栏 -->
      <div class="fixed-tag"></div>
      <!-- 主体内容 -->
      <div class="app-main"><app-main /></div>
    </div>
  </div>
</template>

<style lang="scss" scoped>

</style>
```
`src/style/layout.scss`中新增logo组件的样式：
```
.sidebar-container{
  .logo {
    position: relative;
    width: 100%;
    height: 48px;
    overflow: hidden;
    .logo-link {
      height: 100%;
      padding-left: 10px;
      text-align: left;
      .logo-img{
        width: 32px;
        height: 32px;
        display: inline-block;
        margin: 8px;
        vertical-align: top;
      }
      .logo-title {
        display: inline-block;
        margin: 0;
        color: #1890ff;
        font-weight: 600;
        font-size: 20px;
        line-height: 24px;
        margin-top: 12px;
        vertical-align: top;
      }
    }
  }
}
```
### 菜单组件
新建`src/layout/components/routeMenu/index.vue`文件：
```
<script setup lang="ts">
import MenuItem from "./menuItem.vue";
import { useStore } from 'vuex'
import { computed } from "vue";
import { useRoute } from "vue-router";
const route = useRoute();
const store = useStore();
// 获取路由菜单数据
const menuList = computed(() => {
  return store.state.routeMenus.routeMenus;
});
// 获取当前选中菜单的path
const activeMenu = computed((): string => {
  const { path } = route;
  return path;
});
</script>
<template>
  <el-menu
    :default-active="activeMenu"
    unique-opened
    ellipsis
    router
    :collapse-transition="false"
    mode="vertical"
    class="router-menu"
  >
    <menu-item
      v-for="routeMenu in menuList"
      :key="routeMenu.path"
      :item="routeMenu"
      class="outer-most"
      :base-path="routeMenu.path"
    />
  </el-menu>
</template>
<style lang="scss" scoped></style>
```
新建`src/layout/components/routeMenu/menuItem.vue`文件：
```
<script lang="ts">
export default {
  name: "MenuItem"
};
</script>
<script setup lang="ts">
import path from 'path-browserify'
import { PropType } from "vue";
import { childrenType } from "../../types";

const props = defineProps({
  item: {
    type: Object as PropType<childrenType>,
    default:{}
  },
  isNest: {
    type: Boolean,
    default: false
  },
  basePath: {
    type: String,
    default: ""
  }
});

// 判断是否是叶节点
function isLeafRouterMenu(children: childrenType[] = []){
  if(children){
    if(children.length>0){
      return false;
    }else{
      return true;
    }
  }else{
    return true;
  }
}
// 判断路由是否是外部链接（项目外部链接带http），返回可绑定的路由标识
function resolvePath(routePath:any) {
  const httpReg = /^http(s?):\/\//;
  if (httpReg.test(routePath)) {
    return "/" + routePath;
  } else {
    return path.resolve(props.basePath, routePath);
  }
}
</script>

<template>
  <template v-if="isLeafRouterMenu(props.item.children)">
    <router-link :to="resolvePath(props.item.path)">
      <el-menu-item :index="resolvePath(props.item.path)" :class="{ 'root-menu-noDropdown': !isNest }" >
        <svg-icon v-if="props.item.meta?.icon" :icon-class="`${props.item.meta && props.item.meta.icon}`"></svg-icon>
        <template #title>
          <div class="menu-title">
              <span>{{ props.item.meta?.title }}</span>
          </div>
        </template>
      </el-menu-item>
    </router-link>
  </template>
  <el-sub-menu v-else ref="subMenu" :index="resolvePath(props.item.path)" teleported>
    <template #title>
      <svg-icon  v-if="props.item.meta?.icon" :icon-class="`${props.item.meta && props.item.meta.icon}`"></svg-icon>
      <div class="menu-title">
        <span>{{ props.item.meta?.title }}</span>
      </div>
    </template>
    <menu-item
      v-for="child in props.item.children"
      :key="child.path"
      :is-nest="true"
      :item="child"
      :base-path="resolvePath(child.path)"
      class="nest-menu"
    />
  </el-sub-menu>
</template>
```
该组件用到了新插件：
```
yarn add path-browserify
```
在`src/env.d.ts`文件中声明其类型：
```
declare module 'path-browserify';
```
再新建`src/layout/types.ts`文件：
```
export type childrenType = {
  path?: string;
  noShowingChildren?: boolean;
  children?: childrenType[];
  value: unknown;
  meta?: {
    icon?: string;
    title?: string;
  }; 
};
```
以及对应的菜单样式，修改`src/style/layout.scss`文件：
```
$sideBarWidth: 210px;
/* 侧边栏背景 */
$sidebarBg: #001529 !default;

/* 菜单选中后字体样式 */
$subMenuActiveText: #fff !default;

/* 菜单背景 */
$menuBg: #001529 !default;

/* 鼠标覆盖到菜单时的背景 */
$menuHover: #4091f7 !default;

/* 子菜单背景 */
$subMenuBg: #0f0303 !default;

/* 有无子集的激活菜单背景 */
$subMenuActiveBg: #4091f7 !default;
$navTextColor: #fff !default;
$menuText: rgba(254, 254, 254, 0.65) !default;

/* logo背景颜色 */
$sidebarLogo: #002140 !default;

/* 鼠标覆盖到菜单时的字体颜色 */
$menuTitleHover: #fff !default;
$menuActiveBefore: #4091f7 !default;


.app-wrapper{
  position: relative;
  width: 100%;
  height: 100%;
  .sidebar-container {
    transition: width 0.28s;
    background-color: $sidebarBg;
    height: 100%;
    position: fixed;
    font-size: 0;
    bottom: 0;
    left: 0;
    z-index: 1001;
    overflow: hidden;
    box-shadow: 0 0 1px #888;
    width: $sideBarWidth;
    top: 0;
  }
  .main-container {
    min-height: 100%;
    transition: padding-left .28s;
    position: relative;
    background: #f0f2f5;
    padding-left: $sideBarWidth;
    .fixed-header,.fixed-tag{
      left: $sideBarWidth;
      background: #fff;
      box-shadow: 0 0 1px #888;
      transition: left 0.28s;
      position: fixed;
      right: 0;
      z-index: 1000;
    }
    .fixed-header {
      top: 0;
      height: 48px;
    }
    .fixed-tag{
      top: 48px;
      height: 38px;
    }
    .app-main{
      padding-top: 86px;
      width: 100%;
      height: 100vh;
      position: relative;
      overflow-x: hidden;
    }
  }  
}
// 菜单默认样式优化
.sidebar-container{
  .logo {
    position: relative;
    width: 100%;
    height: 48px;
    overflow: hidden;
    .logo-link {
      height: 100%;
      padding-left: 10px;
      text-align: left;
      .logo-img{
        width: 32px;
        height: 32px;
        display: inline-block;
        margin: 8px;
        vertical-align: top;
      }
      .logo-title {
        display: inline-block;
        margin: 0;
        color: #1890ff;
        font-weight: 600;
        font-size: 20px;
        line-height: 24px;
        margin-top: 12px;
        vertical-align: top;
      }
    }
  }
  .menu-scroll{
    position: absolute;
    left:0;
    top: 48px;
    bottom:0;
    width: 100%;
    .el-scrollbar{
      .scrollbar-wrapper {
        overflow-x: hidden !important;
      }
    }
  }
  .router-menu{
    border: none;
    height: 100%;
    background-color: transparent !important;
    .el-icon{
      display: inline-block;
      width: 18px;
      height: 18px;
      line-height:0;
      font-size:0;
      vertical-align: middle;
      margin-right: 10px;
      svg{
        width: 1em !important;
        height: 1em !important;
        font-size: 18px !important;
        text-align: center;
        vertical-align: middle;
        line-height: 0;
        display: inline-block;
      }
    }
    .el-menu-item{
      display: flex; 
      align-items: center;
      >.menu-title{
        display: flex;
        flex: 1;
        align-items: center;
        justify-content: space-between;
        overflow: hidden;
        >span{
          overflow: hidden;
          text-overflow: ellipsis;
        }
      }
    }
    .el-sub-menu{
      .el-sub-menu__title{
        >.menu-title{
          display: flex;
          flex: 1;
          align-items: center;
          justify-content: space-between;
          overflow: hidden;
          >span{
            overflow: hidden;
            text-overflow: ellipsis;
          }
        }
      }
    }
    
    .el-sub-menu__icon-arrow{
      position: absolute !important;
    }
  
    .el-menu-item,
    .el-sub-menu__title {
      color: $menuText;
    }
  
    // menu hover
    .root-menu-noDropdown,
    .el-sub-menu__title {
      // background: $menuBg;
      &:hover {
        background-color: $menuHover !important;
      }
    }
  
    .is-active > .el-sub-menu__title,
    .is-active.root-menu-noDropdown {
      color: $subMenuActiveText !important;
      i {
        color: $subMenuActiveText !important;
      }
    }
  
    .is-active {
      transition: color 0.3s;
      color: $subMenuActiveText !important;
    }
  
    .el-menu--inline .el-sub-menu__title,
    .el-sub-menu .el-menu-item {
      font-size: 12px;
      min-width: $sideBarWidth !important;
      background-color: $subMenuBg !important;
    }
  
    a {
      display: inline-block;
      width: 100%;
      overflow: hidden;
    }
  }
}
```

菜单组件完成后，在`src/layout/index.vue`文件中引入菜单组件：
```
<script setup lang="ts">
import appMain from "./components/appMain.vue";
import Logo from "./components/logo/index.vue";
import RouteMenu from "./components/routeMenu/index.vue";
</script>

<template>
  <div class="app-wrapper">
    <!-- 侧边导航栏 -->
    <div class="sidebar-container">
      <Logo />
      <div class="menu-scroll">
        <el-scrollbar wrap-class="scrollbar-wrapper">
          <route-menu />
        </el-scrollbar>
      </div>
    </div>
    <div class="main-container">
      <!-- 顶部导航栏 -->
      <div class="fixed-header"></div>
      <!-- 标签栏 -->
      <div class="fixed-tag"></div>
      <!-- 主体内容 -->
      <div class="app-main"><app-main /></div>
    </div>
  </div>
</template>

<style lang="scss" scoped>
</style>
```
运行项目，侧边菜单栏功能就完成了，路由页面已经可以通过侧边菜单栏进行切换。
# 新增菜单
这个时候我们想要新增菜单的话，只需要在`mock/asyncRoutes.ts`文件增加对应数据，以及在`src/views`中增加对应的菜单页面文件即可，先修改`mock/asyncRoutes.ts`文件：
```
import { MockMethod } from "vite-plugin-mock";

// http://mockjs.com/examples.html#Object
const systemRouter = [
  {id:1,pid:0,title:"首页",icon:"icon-shouye",url:"/Home",redirect:"",showLink:true,sort:0,route:"Home"},
  {id:2,pid:0,title:"多级菜单",icon:"icon-daohang",url:"/Nested",redirect:"/Nested/Menu1/Menu1-1",showLink:true,sort:1,route:"Nested"},
  {id:3,pid:2,title:"菜单1",icon:"",url:"/Nested/Menu1",redirect:"/Nested/Menu1/Menu1-1",showLink:true,sort:0,route:"Menu1"},
  {id:4,pid:3,title:"菜单1-1",icon:"",url:"/Nested/Menu1/Menu1-1",redirect:"",showLink:true,sort:0,route:"Menu1-1"},
  {id:5,pid:3,title:"菜单1-2",icon:"",url:"/Nested/Menu1/Menu1-2",redirect:"/Nested/Menu1/Menu1-2/Menu1-2-1",showLink:true,sort:1,route:"Menu1-2"},
  {id:6,pid:5,title:"菜单1-2-1",icon:"",url:"/Nested/Menu1/Menu1-2/Menu1-2-1",redirect:"",showLink:true,sort:0,route:"Menu1-2-1"},
  {id:7,pid:5,title:"菜单1-2-2",icon:"",url:"/Nested/Menu1/Menu1-2/Menu1-2-2",redirect:"",showLink:true,sort:1,route:"Menu1-2-2"},
  {id:8,pid:3,title:"菜单1-3",icon:"",url:"/Nested/Menu1/Menu1-3",redirect:"",showLink:true,sort:3,route:"Menu1-3"},
  {id:9,pid:2,title:"菜单2",icon:"",url:"/Nested/Menu2",redirect:"",showLink:true,sort:1,route:"Menu2"},
  {id:10,pid:0,title:"论坛新闻",icon:"icon-luntan",url:"/News",redirect:"",showLink:true,sort:2,route:"News"},
  {id:11,pid:0,title:"系统管理",icon:"icon-shezhi",url:"/System",redirect:"/System/UserManagement",showLink:true,sort:3,route:"System"},
  {id:12,pid:11,title:"用户管理",icon:"",url:"/System/UserManagement",redirect:"",showLink:true,sort:0,route:"UserManagement"},
]

// 判断accessToken是否有效，假定这里已获取到前端回传的accessToken
const isAccessTokenValid = ()=>{
  return true;
}
// 判断refreshToken是否有效，假定这里后端已经通过accessToken匹配到了refreshToken
const isRefreshTokenValid = ()=>{
  return true;
}

export default [
  {
    url: "/getAsyncRoutes",
    method: "post",
    response: () => {
      if(isRefreshTokenValid()){
        if(isAccessTokenValid()){
          return {
            code: 200,
            status:true,
            info:"菜单数据获取成功！",
            data: systemRouter
          };
        }else{
          return {
            code: 201004,
            status:false,
            info:"token已失效，请更新token！",
            data: []
          };
        }
      }else{
        return {
          code: 201002,
          status:false,
          info:"登录已超时，请重新登录！",
          data: []
        };
      }
    }
  }
] as MockMethod[];
```
这里新增了系统管理以及用户管理的路由数据，修改了论坛新闻的`sort`，接下来新建`src/views/System/index.vue`文件，用来做路由嵌套：
```
<script lang="ts">
</script>
<template>
  <router-view />
</template>
```
新建`src/views/System/UserManagement/index.vue`文件，做用户管理页面：
```
<script lang="ts">
</script>
<template>
  <h2>用户管理</h2>
</template>

<style>
</style>
```
此时再运行项目，新增路由数据已经在侧边菜单中展示，并能够正常进行跳转。
# 新增外部链接菜单
菜单栏可以放外链类菜单数据，在`mock/asyncRoutes.ts`文件中新增一条数据：
```
{id:13,pid:0,title:"百度一下",icon:"icon-sousuo",url:"https://www.baidu.com",redirect:"",showLink:true,sort:10000,route:""},
```
外链不需要创建对应的前端页面去承载，外链必须以`http`或者`https`开头，以其作为判断路由是否是外链的标识，外链的跳转与路由跳转不同，所以这里需要封装一个外链跳转事件，在`src/utils`文件夹下新建`link.ts`文件：
```
export const openLink = (link: string) => {
  const $a: HTMLElement = document.createElement("a");
  $a.setAttribute("href", link);
  $a.setAttribute("target", "_blank");
  $a.setAttribute("rel", "noreferrer noopener");
  $a.setAttribute("id", "external");
  document.getElementById("external") && document.body.removeChild(document.getElementById("external") as HTMLElement);
  document.body.appendChild($a);
  $a.click();
  $a.remove();
};
```
然后在需要用到的地方调用该事件，路由跳转都会经过导航守卫，所以，在导航守卫中进行拦截，如果当前要去的路由是外链，则调用该事件，修改`src/router/index.ts`文件：
```
import { openLink } from "@/utils/link";
import { split, cloneDeep } from "lodash-es";


router.beforeEach((to, _from, next) => {
  const token = getToken();
  NProgress.start();
  const externalLink = to?.redirectedFrom?.fullPath;
  if(token){
    if(_from?.name==="Login"||!_from?.name){
      // 如果是从登录页过来的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
      if (store.state.routeMenus.routeMenus.length === 0){
        initRouter(constantNotRootRoutes).then(() => {
          router.push(to.path);
        });
      }
      next();
    }else{
      // 如果是其他页面跳转的，且其他页面的name存在，则再判断当前跳转页是否是外链页面
      if(externalLink&&externalLink.includes("http")){
        openLink(`http${split(externalLink, "http")[1]}`);
        NProgress.done();
      }else{
        next();
      }
    }
  }else{// 未登录
    // 当前访问页面不是登陆页面
    if (to.path !== "/Login") {
      // 当前访问页面包含在路由白名单中，可直接跳转
      if (whiteList.includes(to.path)) {
        next();
      } else {
        // 当前访问页面需要登录才能访问，需先跳转到登陆页，并将当前访问路由通过链接保存，在登陆后，获取该路由进行跳转
        next(`/Login?redirect=${to.path}`);
      }
    } else {
      // 当前访问页面本身就是登录页，直接跳转
      next();
    }
  }
});
```
