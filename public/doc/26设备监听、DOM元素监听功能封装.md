﻿# 监听设备类型功能、监听DOM元素尺寸变化功能封装

## 设备类型鉴别功能封装
在`src/utils`文件夹下新建`deviceDetection/index.ts`路径文件：
```
interface deviceInter {
  match: Fn;
}

interface BrowserInter {
  browser: string;
  version: string;
}

// 根据userAgent检测设备类型(手机返回true,反之)
export const deviceDetection = () => {
  const sUserAgent: deviceInter = navigator.userAgent.toLowerCase();
  // const bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
  const bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
  const bIsMidp = sUserAgent.match(/midp/i) == "midp";
  const bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
  const bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
  const bIsAndroid = sUserAgent.match(/android/i) == "android";
  const bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
  const bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
  return (
    bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM
  );
};
// 根据浏览器窗口宽度检测设备类型
export const getDevice = () =>{
  const { body } = document;
  const rect = body.getBoundingClientRect();
  let device;
  if(rect.width>0){
    if(rect.width<=1024){
      device = 'mobile'
    }else if(rect.width>1024&&rect.width<=1920){
      device = 'desktop'
    }else{
      device = 'iMax'
    }
  }
  return device;
}
// 为body标签添加设备类型属性
export const setDevice = (device) =>{
  const { body } = document;
  body.setAttribute("device", device);
}

// 获取浏览器型号以及版本
export const getBrowserInfo = () => {
  const ua = navigator.userAgent.toLowerCase();
  const re = /(msie|firefox|chrome|opera|version).*?([\d.]+)/;
  const m = ua.match(re);
  const Sys: BrowserInter = {
    browser: m[1].replace(/version/, "'safari"),
    version: m[2]
  };

  return Sys;
};
```
这里主要用来判断用户设备类型，针对设备类型会对样式以及框架功能显示做一定调整，设备类型主要分为三类：移动端、PC端、4K，PC端和4K主要是分辨率的不同，4K屏由于分辨率过大，可能需要一些单独的样式配置，所以这里将4K抽离出`desktop`的范畴，单独使用`iMax`进行定义。

以上类型定义中用到`Fn`需进行类型定义，在`types`文件夹下新建`index.d.ts`文件：
```
declare interface Fn<T = any, R = T> {
  (...arg: T[]): R;
}
```
识别设类型的功能封装好后，需要通过`vuex`管理设备类型字段，方便在其他页面调用，修改`src/store/modules/app.ts`文件：
```
import { storageLocal } from "@/utils/storage";
import { Module } from 'vuex'
import { AppState } from "../types";
import { getDevice } from "@/utils/deviceDetection";

const appModule: Module<AppState,any> = {
    namespaced: true,
    state: {
        sidebar:{
            opened:storageLocal.getItem("sidebarStatus")===0 ? false : true,
            hidden:storageLocal.getItem("hiddenSidebar")===0||storageLocal.getItem("hiddenSidebar")===null ? false : true,
        },
        device: getDevice()
    },
    mutations:{
        TOGGLE_SIDEBAR:(state)=> {
            state.sidebar.opened = !state.sidebar.opened;
            if (state.sidebar.opened) {
              storageLocal.setItem("sidebarStatus", 1);
            } else {
              storageLocal.setItem("sidebarStatus", 0);
            }
        },
        CHANGE_SIDEBAR:(state,data)=>{
          state.sidebar.hidden = data.value;
          if (state.sidebar.hidden) {
            storageLocal.setItem("hiddenSidebar", 1);
          } else {
            storageLocal.setItem("hiddenSidebar", 0);
          }
        },
        TOGGLE_DEVICE:(state,device: string)=> {
          state.device = device;
        },
    },
    actions: {
        toggleSideBar({commit}) {
            commit('TOGGLE_SIDEBAR');
        },
        changeSidebar({commit},data) {
          commit('CHANGE_SIDEBAR',data);
        },
        toggleDevice({commit},device) {
          commit('TOGGLE_DEVICE',device);
        },
    }
  };
  export default appModule
```
这里新增了`device`字段，需要在`src/store/types.ts`文件中增加对应的类型定义：
```
// 项目配置数据管理
export type AppState = {
  sidebar: {
    opened: boolean;
    hidden:boolean;
  };
  device:string;
}
```
然后需要修改`src/layout/index.vue`文件：
```
<script setup lang="ts">
import { computed,onMounted,onBeforeUnmount } from "vue";
import appMain from "./components/appMain.vue";
import Logo from "./components/logo/index.vue";
import RouteMenu from "./components/routeMenu/index.vue";
import HeaderPanel from "./components/headerPanel.vue";
import tagsView from "./components/tagsView/index.vue";
import { useStore } from 'vuex';
import { setDevice } from "@/utils/deviceDetection";
const store = useStore();
const sidebar = computed(() => {
  return store.state.app.sidebar;
});
const device = computed(() => {
  return store.state.app.device;
});
const update = () => {
  const { body } = document;
  const rect = body.getBoundingClientRect()
  if(rect.width>0){
    if(rect.width<=1024){
      store.dispatch('app/toggleDevice','mobile');
    }else if(rect.width>1024&&rect.width<=1920){
      store.dispatch('app/toggleDevice','desktop');
    }else{
      store.dispatch('app/toggleDevice','iMax');
    }
  }
  setDevice(device.value);
};
setDevice(device.value);
onMounted(() => {
  addEventListener('resize', update);
});

onBeforeUnmount(() => {
  removeEventListener('resize', update);
});
</script>

<template>
  <div :class="['app-wrapper',sidebar.opened?'unfoldSidebar':'foldSidebar',sidebar.hidden?'hidden-mode':'show-mode',device]">
    <!-- 移动端侧边菜单栏遮罩层 -->
    <div v-show=" device === 'mobile' && sidebar.opened " class="app-mask" @click="store.dispatch('app/toggleSideBar')" ></div>
    <!-- 侧边导航栏 -->
    <div class="sidebar-container">
      <Logo />
      <div class="menu-scroll">
        <el-scrollbar wrap-class="scrollbar-wrapper">
          <route-menu />
        </el-scrollbar>
      </div>
    </div>
    <div class="main-container">
      <!-- 顶部导航栏 -->
      <div class="fixed-header"><header-panel /></div>
      <!-- 标签栏 -->
      <div class="fixed-tag"><tags-view /></div>
      <!-- 主体内容 -->
      <div class="app-main"><app-main /></div>
    </div>
  </div>
</template>

<style lang="scss" scoped>
</style>
```
这里增加了监听浏览器窗口大小变化动态修改设备类型，并调用`vuex`中保管的`device`，增加了移动端侧边导航弹窗的遮罩层，调用完成后，剩下的是需要对样式进行修改，修改`src/style/layout.scss`文件：
```
.app-wrapper{
  position: relative;
  width: 100%;
  height: 100%;
  // 隐藏侧边栏与header栏模式
  &.hidden-mode{
    // 侧边栏折叠模式
    &.foldSidebar{
      .sidebar-container{
        width: 64px;
        left:- 64px;
      }
    }
    // 侧边栏展开模式
    &.unfoldSidebar{
      .sidebar-container{
        width: $sideBarWidth;
        left:-$sideBarWidth;
      }
    }
    .main-container{
      padding-left: 0;
      .fixed-header{
        left:0;
        top: -48px;
      }
      .fixed-tag{
        left:0;
        top:0;
      }
      .app-main{
        padding-top: 38px;
      }
    }
  }
  // 显示侧边栏与header栏模式
  &.show-mode{
    // 侧边栏折叠模式
    &.foldSidebar{
      .sidebar-container{
        width: 64px;
      }
      .main-container{
        padding-left: 64px;
        .fixed-header{
          left:64px;
        }
        .fixed-tag{
          left:64px;
        }
      }
    }
    // 侧边栏展开模式
    &.unfoldSidebar{
      .sidebar-container{
        width: $sideBarWidth;
      }
      .main-container{
        padding-left: $sideBarWidth;
        .fixed-header{
          left:$sideBarWidth;
        }
        .fixed-tag{
          left:$sideBarWidth;
        }
      }
    }
    .sidebar-container{
      left: 0;
    }
    .main-container{
      .fixed-header{
        top: 0;
      }
      .fixed-tag{
        top:48px;
      }
      .app-main{
        padding-top: 86px;
      }
    }
  }
  // 移动端模式
  &.mobile{
    // 隐藏/显示侧边栏与header栏模式
    &.hidden-mode,&.show-mode{
      // 侧边栏折叠模式
      &.foldSidebar{
        .sidebar-container{
          width: $sideBarWidth;
          left:-$sideBarWidth;
        }
      }
      // 侧边栏展开模式
      &.unfoldSidebar{
        .sidebar-container{
          width: $sideBarWidth;
          left:0;
        }
      }
      .main-container{
        padding-left: 0;
        .fixed-header{
          left:0;
        }
        .fixed-tag{
          left:0;
        }
      }
    }
  }
  .app-mask {
    background: #000;
    opacity: 0.3;
    width: 100%;
    top: 0;
    height: 100%;
    position: absolute;
    z-index: 1010;
  }
  .sidebar-container {
    transition: all 0.28s;
    background-color: $sidebarBg;
    height: 100%;
    position: fixed;
    font-size: 0;
    bottom: 0;
    z-index: 1020;
    overflow: hidden;
    box-shadow: 0 0 1px #888;
    top: 0;
  }
  .main-container {
    min-height: 100%;
    transition: padding-left .28s;
    position: relative;
    background: #f0f2f5;
    .fixed-header,.fixed-tag{
      background: #fff;
      box-shadow: 0 0 1px rgba(0,0,0,.3);
      transition: all 0.28s;
      position: fixed;
      right: 0;
      z-index: 1000;
    }
    .fixed-header {
      height: 48px;
    }
    .fixed-tag{
      height: 38px;
    }
    .app-main{
      width: 100%;
      height: 100vh;
      position: relative;
      overflow: hidden;
      .app-root{
        width:100%;
        height: 100vh;
        overflow-x: hidden;
        position: relative;
      }
    }
  }  
}
```
这里对`app-wrapper`类中的样式进行了一定量的修改，此时运行项目，侧边栏在不同设备下就有了不同的样式规则。还需要修改`src/layout/components/headerPanel.vue`文件，移动端默认将`header`中的面包屑隐藏：
```
const device = computed(() => {
  return store.state.app.device;
});


<Breadcrumb class="breadcrumb-container" v-if="device!=='mobile'" />
```
设备类型的设配功能以及如果调用这里就完成了，屏幕适配除了设备类型的大方向的适配外，还有在项目交互过程中，元素的内容可能会因宽高有变化，而需要做一些重新渲染或者其他操作，这就需要对元素进行监听，监听其尺寸变化，这个也是可以进行封装的。
## 监听DOM元素尺寸变化封装函数
在`src/utils`文件夹下新建`resize/index.ts`路径文件：
```
import ResizeObserver from "resize-observer-polyfill";

const isServer = typeof window === "undefined";

const resizeHandler = (entries: any[]): void => {
  for (const entry of entries) {
    const listeners = entry.target.__resizeListeners__ || [];
    if (listeners.length) {
      listeners.forEach((fn: () => any) => {
        fn();
      });
    }
  }
};

export const addResizeListener = (element: any, fn: () => any): any => {
  if (isServer) return;
  if (!element.__resizeListeners__||(element.__resizeListeners__&&element.__resizeListeners__.length===0)) {
    element.__resizeListeners__ = [];
    element.__ro__ = new ResizeObserver(resizeHandler);
    element.__ro__.observe(element);
  }
  element.__resizeListeners__.push(fn);
};

export const removeResizeListener = (element: any, fn: () => any): any => {
  if (!element || !element.__resizeListeners__) return;
  element.__resizeListeners__.splice(
    element.__resizeListeners__.indexOf(fn),
    1
  );
  if (!element.__resizeListeners__.length) {
    element.__ro__.disconnect();
  }
};
```
这里用到一个新的项目依赖插件，需要先进行安装：
```
yarn add resize-observer-polyfill
```
具体使用需要分两种情况，根据页面是否开启了状态缓存功能，监听事件的启用以及移除需要在不同的生命周期函数内进行执行，修改`src/views/FrontEndLibrary/ViewsLibrary/ReDemo/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'ReDemo'
}
</script>
<script setup lang="ts">
import { ref,unref,onMounted,onBeforeUnmount,onActivated,onDeactivated,computed } from 'vue';
import { addResizeListener, removeResizeListener } from '@/utils/resize';
import { useRoute } from "vue-router";
const route = useRoute();
const reDemo = ref();
const update = () => {
  console.log("调用了监听事件1111");
};
// 获取当前页面的缓存状态是否被开启
const isKeepAlive = route.meta.keepAlive;
// 在组件挂载到页面后执行
onMounted(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if(!isKeepAlive){
    console.log("组件被挂载");
    addResizeListener(unref(reDemo), update);
  }
});
// 页面未启用keep-alive时，则需要在此生命周期函数内（组件被卸载之前执行）移除监听
onBeforeUnmount(() => {
  // 未开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if(!isKeepAlive){
    console.log("组件即将被卸载");
    removeResizeListener(unref(reDemo),update);
  }
});

// keep-alive启用时，页面被激活时使用
onActivated(() => {
  // 开启页面状态缓存的情况下，监听事件在此声明周期内开启
  if(isKeepAlive){
    console.log("组件被激活");
    addResizeListener(unref(reDemo), update);
  }
});
// 页面启用keep-alive时，则需要在此生命周期函数内（组件切换，老组件消失的时候执行）移除监听事件
onDeactivated(()=>{
  // 开启页面状态缓存的情况下，监听事件在此声明周期内移除
  if(isKeepAlive){
    console.log("组件已被切换");
    removeResizeListener(unref(reDemo),update);
  }
});
const value1 = ref("")
</script>
<template>
  <div class="re-demo-page" ref="reDemo">
    <h2>测试专用页面</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
页面状态缓存被开启时，需要使用`onActivated`启用监听事件，用`onDeactivated`移除监听事件，而如果页面未启用状态缓存功能，则需要使用`onMounted`启用监听事件，用`onBeforeUnmount`移除监听事件，这里通过获取当前页面路由信息中的`keepAlive`来判断页面是否开启了状态缓存，如果页面是否启用状态缓存是在程序端后台配置的，那么页面里以上四个生命周期都需要写上，且需要根据字段进行判断具体执行哪一套。

