﻿# element-plus 组件使用
上一章已经实现项目的明亮与暗黑系主题切换功能，但是使用的`element-plus`框架的组件还未配置暗黑系主题，在这之前，需要知道`element-plus`有哪些组件需要进行配置，这里会先将`element-plus`的所有组件使用一遍，观察其样式控制规则，需要先更新`element-plus`版本，写到这里时的版本已到`2.1.10`，此版本的`element-plus`对样式有了大调整，`css`变量也有所调整，增加了暗黑主题的一些简单配置，`css`变量也更加细化了，所以这里先把`element-plus`版本升级，本次对项目的插件有了大量调整

1. 现阶段的`package.json`文件：
```
"scripts": {
    "restart": "npm install",
    "start": "cross-env vite --mode development",
    "stage": "rimraf stage && cross-env vite build --mode staging",
    "build": "rimraf dist && cross-env vite build --mode production",
    "report": "rimraf dist && cross-env vite build --mode production",
    "preview": "vite preview",
    "clean:cache": "rimraf node_modules/.cache/ && rimraf node_modules/.vite",
    "clean:lib": "rimraf node_modules",
    "reinstall": "rimraf pnpm-lock.yaml && rimraf package-lock.json && rimraf node_modules && npm run restart"
  },
  "dependencies": {
    "@element-plus/icons-vue": "^0.2.4",
    "@iconify/json": "^2.1.27",
    "@vue/compiler-sfc": "^3.2.27",
    "@vueuse/core": "8.2.4",
    "@vueuse/motion": "^2.0.0-beta.9",
    "@vueuse/shared": "8.2.4",
    "axios": "^0.24.0",
    "china-area-data": "^5.0.1",
    "dayjs": "^1.11.0",
    "element-plus": "^2.1.10",
    "js-cookie": "^3.0.1",
    "lodash-es": "^4.17.21",
    "mockjs": "^1.1.0",
    "nprogress": "^0.2.0",
    "path-browserify": "^1.0.1",
    "qs": "^6.10.3",
    "resize-observer-polyfill": "^1.5.1",
    "responsive-storage": "^1.0.11",
    "vue": "^3.2.33",
    "vue-i18n": "^9.2.0-beta.35",
    "vue-router": "^4.0.12",
    "vuex": "^4.0.2"
  },
  "devDependencies": {
    "@types/js-cookie": "^3.0.1",
    "@types/mockjs": "^1.0.4",
    "@types/node": "^17.0.8",
    "@types/nprogress": "^0.2.0",
    "@types/qs": "^6.9.7",
    "@vitejs/plugin-legacy": "^1.7.1",
    "@vitejs/plugin-vue": "^2.3.1",
    "cross-env": "^7.0.3",
    "esbuild": "0.14.34",
    "ip": "^1.1.5",
    "path": "^0.12.7",
    "rimraf": "^3.0.2",
    "rollup-plugin-visualizer": "^5.5.4",
    "sass": "^1.46.0",
    "typescript": "^4.4.4",
    "vite": "^2.9.1",
    "vite-plugin-live-reload": "^2.1.0",
    "vite-plugin-mock": "^2.9.6",
    "vite-plugin-remove-console": "^0.0.6",
    "vite-svg-loader": "^3.1.1",
    "vue-tsc": "^0.29.8"
  }
```
命令中新增了一些方便操作的命令，项目依赖中新增的`dayjs`是用来处理日期相关功能的，`china-area-data`是用来封装省市区三级联动获取省市区数据用的，`@iconify/json`是加载一些第三方的图标集，丰富项目的图标选择。开发依赖中的`esbuild`版本，以及项目依赖中的`@vueuse/core`，`@vueuse/shared`版本，都需要注意，本次项目调整中，依赖之间的版本冲突问题出现很多，本次更新插件，将项目之前已有的很多插件版本都进行了更新，如果版本不对应，项目运行也许会有一些意想不到的错误发生。

2. `vite.config.ts`文件：
```
import { resolve } from "path";
import { UserConfigExport, ConfigEnv,loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteMockServe } from "vite-plugin-mock";
import svgLoader from "vite-svg-loader";
import { wrapperEnv, regExps } from "./build";
import ip from 'ip';
import liveReload from "vite-plugin-live-reload";
import legacy from "@vitejs/plugin-legacy";
import { visualizer } from "rollup-plugin-visualizer";
import removeConsole from "vite-plugin-remove-console";
import fixElementPlus from './build/vite-fix-element-plus/plugin'

// 获取ip地址
const ipStr = ip.address();
// 路径查找
const pathResolve = (dir: string): string => {
  return resolve(__dirname, dir);
};
// 当前执行node命令时文件夹的地址（工作目录）
const root: string = process.cwd();
// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfigExport =>{
  const {
    VITE_PORT,
    VITE_OUTDIR,
    VITE_PUBLIC_PATH,
    VITE_PROXY_API_PATH,
    VITE_BASE_API_PATH,
    VITE_LEGACY
  } = wrapperEnv(loadEnv(mode, root));
  const prodMock = true;
  // 获取当前执行命令事件
  const lifecycle = process.env.npm_lifecycle_event;
  return {
    base:VITE_PUBLIC_PATH,
		root,
    // 开发服务
		server:{
			https:false,
			port:VITE_PORT,
			host:ipStr,
      hmr:true,
			// 本地跨域代理
			proxy:VITE_BASE_API_PATH.length>0?{
				[VITE_PROXY_API_PATH]:{
					target:VITE_BASE_API_PATH,
					changeOrigin:true,
					rewrite:(path:string)=>regExps(path,VITE_PROXY_API_PATH)
				}
			}:null,
			open:`http://${ipStr}:${VITE_PORT}`
		},
    // 生产环境配置
		build:{
			outDir:VITE_OUTDIR,
			sourcemap:false,
			brotliSize:false,
			// 消除打包大小超过500kb警告
			chunkSizeWarningLimit:2000,
		},
    plugins: [
      vue(),
      // 解决某些文件修改后无法进行热更新，页面不会自动刷新问题
      liveReload(["src/layout/**/*", "src/router/**/*"]),
      // 线上环境删除console
      removeConsole(),
      svgLoader(),
      fixElementPlus(),
      viteMockServe({
        mockPath: "./mock", // 模拟接口api文件存放的文件夹
        watchFiles: true, // 将监视文件夹中的文件更改。 并实时同步到请求结果
        localEnabled: command === "serve", // 设置是否启用本地 xxx.ts 文件，不要在生产环境中打开它.设置为 false 将禁用 mock 功能
        prodEnabled: command !== "serve" && prodMock, // 设置生产环境是否启用 mock 功能
        injectCode: `
          import { setupProdMockServer } from './mockProdServer';
          setupProdMockServer();
        `, // 如果生产环境开启了mock功能，该代码会注入到injectFile对应的文件的底部，默认为main.{ts,js}
        injectFile:resolve(process.cwd(),'src/main.{ts,js}'),
        logger: true
      }),
      // 是否为打包后的文件提供传统浏览器兼容性支持
      VITE_LEGACY
      ? legacy({
          targets: ["ie >= 11"],
          additionalLegacyPolyfills: ["regenerator-runtime/runtime"]
        })
      : null,
      // 打包分析
      lifecycle === "report"
      ? visualizer({ open: true, brotliSize: true, filename: "report.html" })
      : null
    ],
    // 依赖预构建，优化依赖
    optimizeDeps: {
      include: [
        "lodash-es",
        '@vueuse/core',
        '@vueuse/shared',
        "element-plus/lib/locale/lang/en",
        "element-plus/lib/locale/lang/zh-cn"
      ],
    },
    resolve: {
      alias: {
        '@': pathResolve('src'),
        '^': pathResolve('build'),
        '#': pathResolve('types'),
      }
    },
    css: {
      // css预处理器
      preprocessorOptions: {
        scss: {
          // 引入 var.scss 这样就可以在全局中使用 var.scss中预定义的变量了
          // 给导入的路径最后加上 ; 
          additionalData: `@use "@/style/var.scss" as *;`
        }
      }
    },
  }
}
```
插件版本更新后，`vite.config.ts`文件配置也有一些调整，这里主要是去除了`element-plus`的按需加载配置，接下来要将所有组件都是用一下，还是全局加载更方便。另外这里配置了`fixElementPlus`插件，这是由于`element-plus`的`affix`固钉组件无法在自定义滚动条下实现固钉效果，而写的兼容插件，官方目前还未解决此问题，暂时需要添加该插件让固钉组件不会出现BUG问题。另外删除了`optimizeDeps.include`中的`vue-i18n`以及`resolve.alias`中的`"vue-i18n": "vue-i18n/dist/vue-i18n.cjs.js"`解决的是国际化会因为找不到`vue`实例而无法使用，导致项目无法正常运行的问题。这也是版本冲突造成的问题。

3. 在`build`文件夹下新增的`vite-fix-element-plus`文件夹及其内路径结构与文件，为解决`element-plus`的固钉BUG问题而写的插件，插件内容不会有修改，这里不再贴出具体代码，插件中的`es`和`lib`文件夹内的文件与`element-plus`官方插件对应路径文件是一样的，只在第82行增加了解决BUG的一行代码，并通过`plugin.ts`文件，将官方的这两个文件替换为项目中的这两个文件，达到解决BUG的目的。

4. `src/main.ts`文件：
```
import { createApp } from 'vue'
import App from '@/App.vue'
import router from "./router";
import store from './store';
import { MotionPlugin } from "@vueuse/motion";
import { usI18n } from "./plugins/i18n";
import { getServerConfig } from "./config";
import { injectResponsiveStorage } from "@/utils/storage/responsive";

// element-plus 按需引入
// import { useElementPlus } from "@/plugins/element-plus"; // element-plus
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

import * as ElIconModules from '@element-plus/icons-vue'

import SvgIcon from '@/components/SvgIcon/index.vue';

import "@/style/index.scss"
// 导入字体图标
import "@/assets/iconfont/iconfont.js";

const app = createApp(App)
getServerConfig(app).then(async config => {
  injectResponsiveStorage(app, config);
  app.use(router)
  app.use(store)
  usI18n(app)
  app.use(MotionPlugin)
  app.use(ElementPlus)
  app.component('svg-icon', SvgIcon); // 全局注册svg图标组件
  // 统一注册element-plus的图标，主要用于element-plus自身组件的icon绑定
  for (const iconName in ElIconModules) {
    if (Reflect.has(ElIconModules, iconName)) {
      const item = ElIconModules[iconName]
      app.component(iconName, item)
    }
  }
  await router.isReady();
  app.mount('#app')
});
```
这里主要全局引入`element-plus`，以及全局注册所有`element-plus`图标，这两者都不在是之前的按需手动加载，方便后期使用。修正了`usI18n`的使用方法，删除了`useCustomSvgIcon`自定义图标的注册。

5. 这个时候可以把以前的`src/plugins/element-plus`和`customSvgIcon`文件夹都删除了，接下来不会再用到这两个文件。

6. `src/App.vue`文件：
```
<script lang="ts">
export default {
  name: "app",
};
</script>
<script setup lang="ts">
import { getCurrentInstance,computed } from "vue";
import zhCn from "element-plus/lib/locale/lang/zh-cn";
import en from "element-plus/lib/locale/lang/en";
const instance = getCurrentInstance().appContext.app.config.globalProperties.$storage;
const currentLocale = computed(() => {
  return !instance||instance.locale?.locale === "zh" ? zhCn : en;
});
const currentSize = computed(() => {
  return !instance||instance.size?.size==='default'?'':instance.size?.size
});
</script>

<template>
  <el-config-provider :locale="currentLocale" :size="currentSize">
    <router-view />
  </el-config-provider>
</template>
```
这里主要改为`setup`语法糖的格式写逻辑，另外由于`element-plus`修改了`size`默认情况下的值（默认情况下现在`size=''`），这里也需要修改。

7. `src/plugins/i18n/index.ts`文件：
```
// 多组件库的国际化和本地项目国际化兼容
import { App } from "vue";
import { set } from "lodash-es";
import { createI18n } from "vue-i18n";
import { localesConfigs } from "./config";
import { storageLocal } from "@/utils/storage";

/**
 * 国际化转换工具函数
 * @param message message
 * @param isI18n  如果true,获取对应的消息,否则返回本身
 * @returns message
 */
export function transformI18n(
  message: string | unknown | object = "",
  isI18n: boolean | unknown = false
) {
  if (!message) {
    return "";
  }

  // 处理存储动态路由的title,格式 {zh:"",en:""}
  if (typeof message === "object") {
    return message[i18n.global?.locale.value];
  }

  if (isI18n) {
    //@ts-ignore
    return i18n.global.t(message);
  } else {
    return message;
  }
}

/**
 * 从模块中抽取国际化
 * @param langs 存放国际化模块
 * @param prefix 语言 默认 zh-CN
 * @returns obj 格式：{模块名.**}
 */
export function siphonI18n(
  langs: Record<string, Record<string, any>>,
  prefix = "zh-CN"
) {
  const langsObj: Recordable = {};
  Object.keys(langs).forEach((key: string) => {
    let fileName = key.replace(`./${prefix}/`, "").replace(/^\.\//, "");
    fileName = fileName.substring(0, fileName.lastIndexOf("."));
    const keyList = fileName.split("/");
    const moduleName = keyList.shift();
    const objKey = keyList.join(".");
    const langFileModule = langs[key].default;

    if (moduleName) {
      if (objKey) {
        set(langsObj, moduleName, langsObj[moduleName] || {});
        set(langsObj[moduleName], objKey, langFileModule);
      } else {
        set(langsObj, moduleName, langFileModule || {});
      }
    }
  });
  return langsObj;
}

// 此函数只是配合i18n Ally插件来进行国际化智能提示，并无实际意义（只对提示起作用），如果不需要国际化可删除
export const $t = (key: string) => key;

export const i18n = createI18n({
  legacy: false,
  globalInjection:true,
  locale: storageLocal.getItem("responsive-locale")?.locale ?? "zh",
  fallbackLocale: "zh",
  messages: localesConfigs
});

export function usI18n(app: App) {
  app.use(i18n);
}

```
由于`vue-i18n`插件的版本升级，代码也需要有调整。`vue3`在`createI18n`时，必须添加`legacy`字段，且值必须为`false`，这样才能使用`vue3`的组合式`api`，才能不会产生冲突。

8. `src/utils/http/index.ts`文件：
```
timeout: 50000,
```
这里只把超时时间做了调整

9. `src/utils/http/loading.ts`文件：
```
import _ from 'lodash-es';
```
下载的是`lodash-es`插件，这里之前引入有误

10. 新建`src/utils/dateFormat.ts`文件：
```
import dayjs from "dayjs";
/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
 export function parseTime(time, cFormat) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string')) {
      if ((/^[0-9]+$/.test(time))) {
        // support "1548221490638"
        time = parseInt(time)
      } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
        time = time.replace(new RegExp(/-/gm), '/')
      }
    }

    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value ] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

/**
 * @param {string} date YYYY-MM-DD | YYYY-MM-DD HH:mm:ss
 * @returns {string}
 */
export function formatTime(date) {
  const mistiming = Math.round((Date.now() - new Date(date).getTime()) / 1000)
  const tags = ['年', '个月', '星期', '天', '小时', '分钟', '秒']
  const times = [31536000, 2592000, 604800, 86400, 3600, 60, 1]
  for (let i = 0; i < times.length; i++) {
      const inm = Math.floor(mistiming / times[i])
      if (tags[i] === '天') {
          switch (inm) {
              case 0:
                  return '今天'
                  break
              case 1:
                  return '昨天'
                  break
              case 2:
                  return '前天'
                  break
              default:
                  return inm + tags[i] + '前'
                  break;
          }
      }
      if (inm !== 0) {
          return inm + tags[i] + '前'
      }
  }
}

export function formatDate(date) {
  return dayjs(date).format('YYYY-MM-DD');
}
```
该文件主要用来封装一些处理日期时间的公共函数。

11. 新建`src/utils/is.ts`文件：
```
import dayjs from "dayjs";
const toString = Object.prototype.toString;

export function is(val: unknown, type: string) {
  return toString.call(val) === `[object ${type}]`;
}

export function isDef<T = unknown>(val?: T): val is T {
  return typeof val !== 'undefined';
}

export function isUnDef<T = unknown>(val?: T): val is T {
  return !isDef(val);
}

export function isObject(val: any): val is Record<any, any> {
  return val !== null && is(val, 'Object');
}

export function isEmpty<T = unknown>(val: T): val is T {
  if (isArray(val) || isString(val)) {
    return val.length === 0;
  }

  if (val instanceof Map || val instanceof Set) {
    return val.size === 0;
  }

  if (isObject(val)) {
    return Object.keys(val).length === 0;
  }

  return false;
}

export function isDate(val: unknown): val is Date {
  return is(val, 'Date');
}

export function isToday(val:string){
  const nowDate = dayjs().format('YYYY-MM-DD');
  return nowDate === val
}

export function isNull(val: unknown): val is null {
  return val === null;
}

export function isNullAndUnDef(val: unknown): val is null | undefined {
  return isUnDef(val) && isNull(val);
}

export function isNullOrUnDef(val: unknown): val is null | undefined {
  return isUnDef(val) || isNull(val);
}

export function isNumber(val: unknown): val is number {
  return is(val, 'Number');
}

export function isPromise<T = any>(val: unknown): val is Promise<T> {
  return is(val, 'Promise') && isObject(val) && isFunction(val.then) && isFunction(val.catch);
}

export function isString(val: unknown): val is string {
  return is(val, 'String');
}

export function isFunction(val: unknown): val is Function {
  return typeof val === 'function';
}

export function isBoolean(val: unknown): val is boolean {
  return is(val, 'Boolean');
}

export function isRegExp(val: unknown): val is RegExp {
  return is(val, 'RegExp');
}

export function isArray(val: any): val is Array<any> {
  return val && Array.isArray(val);
}

export function isWindow(val: any): val is Window {
  return typeof window !== 'undefined' && is(val, 'Window');
}

export function isElement(val: unknown): val is Element {
  return isObject(val) && !!val.tagName;
}

export function isMap(val: unknown): val is Map<any, any> {
  return is(val, 'Map');
}

export const isServer = typeof window === 'undefined';

export const isClient = !isServer;

export function isUrl(path: string): boolean {
  const reg =
    /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;
  return reg.test(path);
}

```
该文件内主要是一些验证类公共函数。

12. `src/utils/theme/index.ts`文件：
```
export const setThemeColor = (color,menuBgColor,headerBgColor,darkTheme) => {
  ···

  if(!darkTheme){
    document.documentElement.style.setProperty('color-scheme','dark');
  }else{
    document.documentElement.style.setProperty('color-scheme','light');
  }

  ···
}

// 系统颜色源，所有颜色都从这里开始衍生
export const themeColor = () => {
  return {
    // 用来做混合使用
    white:"#ffffff", // 白色
    black:"#000000", // 黑色
    // 侧边栏、顶栏除主题色色系外的白色系、黑色系
    light:"#ffffff", // 白色
    dark:"#161b22", // 黑色
    // 主题色色系
    blue:"#1890ff", // 蓝色
    green:"#41B584", // 绿色
    red:"#f34d37", // 红色
    pink:"#ff5c93", // 粉色
    purple:"#9c27b0", // 紫色
    cyan:"#13c2c2", // 青色
    orange:"#fa541c", // 橙色
    gold:"#f2be45", // 烫金色
    // 信息提示类色系
    success:"#13ce66", // 成功
    warning:"#ffba00", // 警告
    danger:"#ff4d4f", // 危险
    error:"#ff4d4f", // 错误
    info:"#909399", // 信息
  }
}

export const randomColor = () => {
  let colorList = [themeColor().blue,themeColor().green,themeColor().red,themeColor().pink,themeColor().purple,themeColor().cyan,themeColor().orange,themeColor().gold,themeColor().success,themeColor().warning,themeColor().danger]
  let index = Math.floor(Math.random()*colorList.length);
  return colorList[index];
}
```
这里主要在设置主题色时，增加了一个当前主题的明暗`css`变量，以及修改了信息提示类的色值，增加了随机抽取一个主题色/信息提示色的随机函数。

13. `src/router/modules/frontEndLibrary.ts`文件：
```
import { $t } from "@/plugins/i18n";
const modulesRouter = [
    {
      path: "/FrontEndLibrary",
      name: "FrontEndLibrary",
      redirect:"/FrontEndLibrary/ElementPlus/Button",
      meta: {
        id:1001000,
        title: $t("menus.menuFrontEndLibrary"),
        showLink: true,
        icon:"ii:icon-front-end",
        pid:0,
        sort:1001000,
        keepAlive:true,
        transitionName:"",
        i18n:true,
        level:1,
      },
      children:[
        {
          path: "/FrontEndLibrary/ElementPlus",
          name: "ElementPlus",
          redirect:"/FrontEndLibrary/ElementPlus/Button",
          meta: {
            id:1001100,
            title: $t("menus.menuElementPlus"),
            showLink: true,
            icon:"ep:element-plus",
            pid:1001000,
            sort:1,
            keepAlive:true,
            transitionName:"",
            i18n:true,
            level:2,
          },
          children:[
            {
              path: "/FrontEndLibrary/ElementPlus/Button",
              name: "EpButton",
              component: () => import("@/views/FrontEndLibrary/ElementPlus/Button/index.vue"),
              meta: {
                id:1001110,
                title: $t("menus.menuEpButton"),
                showLink: true,
                pid:1001100,
                sort:1,
                keepAlive:true,
                transitionName:"",
                i18n:true,
                level:3,
              },
              children:[]
            },
            {
              path: "/FrontEndLibrary/ElementPlus/Link",
              name: "EpLink",
              component: () => import("@/views/FrontEndLibrary/ElementPlus/Link/index.vue"),
              meta: {
                id:1001120,
                title: $t("menus.menuEpLink"),
                showLink: true,
                pid:1001100,
                sort:2,
                keepAlive:true,
                transitionName:"",
                i18n:true,
                level:3,
              },
              children:[]
            },
            {
              path: "/FrontEndLibrary/ElementPlus/Tag",
              name: "EpTag",
              component: () => import("@/views/FrontEndLibrary/ElementPlus/Tag/index.vue"),
              meta: {
                id:1001130,
                title: $t("menus.menuEpTag"),
                showLink: true,
                pid:1001100,
                sort:3,
                keepAlive:true,
                transitionName:"",
                i18n:true,
                level:3,
              },
              children:[]
            },
            {
              path: "/FrontEndLibrary/ElementPlus/Notice",
              name: "EpNotice",
              component: () => import("@/views/FrontEndLibrary/ElementPlus/Notice/index.vue"),
              meta: {
                id:1001140,
                title: $t("menus.menuEpNotice"),
                showLink: true,
                pid:1001100,
                sort:4,
                keepAlive:true,
                transitionName:"",
                i18n:true,
                level:3,
              },
              children:[]
            },
            {
              path: "/FrontEndLibrary/ElementPlus/Feedback",
              name: "EpFeedback",
              component: () => import("@/views/FrontEndLibrary/ElementPlus/Feedback/index.vue"),
              meta: {
                id:1001150,
                title: $t("menus.menuEpFeedback"),
                showLink: true,
                pid:1001100,
                sort:5,
                keepAlive:true,
                transitionName:"",
                i18n:true,
                level:3,
              },
              children:[]
            },
            {
              path: "/FrontEndLibrary/ElementPlus/Data",
              name: "EpData",
              redirect:"/FrontEndLibrary/ElementPlus/Data/PaginationList",
              meta: {
                id:1001160,
                title: $t("menus.menuEpData"),
                showLink: true,
                pid:1001100,
                sort:7,
                keepAlive:true,
                transitionName:"",
                i18n:true,
                level:3,
              },
              children:[
                {
                    path: "/FrontEndLibrary/ElementPlus/Data/PaginationList",
                    name: "EpPaginationList",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Data/PaginationList/index.vue"),
                    meta: {
                      id:1001161,
                      title: $t("menus.menuEpPaginationList"),
                      showLink: true,
                      pid:1001160,
                      sort:7,
                      keepAlive:false,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                },
                {
                    path: "/FrontEndLibrary/ElementPlus/Data/InfiniteScrollList",
                    name: "EpInfiniteScrollList",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Data/InfiniteScrollList/index.vue"),
                    meta: {
                      id:1001162,
                      title: $t("menus.menuEpInfiniteScrollList"),
                      showLink: true,
                      pid:1001160,
                      sort:7,
                      keepAlive:true,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                },
                {
                    path: "/FrontEndLibrary/ElementPlus/Data/Article",
                    name: "EpArticle",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Data/Article/index.vue"),
                    meta: {
                      id:1001163,
                      title: $t("menus.menuEpArticle"),
                      showLink: true,
                      pid:1001160,
                      sort:7,
                      keepAlive:true,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                },
                {
                    path: "/FrontEndLibrary/ElementPlus/Data/Calendar",
                    name: "EpCalendar",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Data/Calendar/index.vue"),
                    meta: {
                      id:1001164,
                      title: $t("menus.menuEpCalendar"),
                      showLink: true,
                      pid:1001160,
                      sort:7,
                      keepAlive:true,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                },
                {
                    path: "/FrontEndLibrary/ElementPlus/Data/Tree",
                    name: "EpTree",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Data/Tree/index.vue"),
                    meta: {
                      id:1001165,
                      title: $t("menus.menuEpTree"),
                      showLink: true,
                      pid:1001160,
                      sort:7,
                      keepAlive:true,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                },
              ]
            },
            {
                path: "/FrontEndLibrary/ElementPlus/Form",
                name: "EpForm",
                redirect:"/FrontEndLibrary/ElementPlus/Form/ComprehensiveForm",
                meta: {
                  id:1001170,
                  title: $t("menus.menuEpForm"),
                  showLink: true,
                  pid:1001100,
                  sort:8,
                  keepAlive:true,
                  transitionName:"",
                  i18n:true,
                  level:3,
                },
                children:[
                  {
                    path: "/FrontEndLibrary/ElementPlus/Form/ComprehensiveForm",
                    name: "EpComprehensiveForm",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Form/ComprehensiveForm/index.vue"),
                    meta: {
                      id:1001171,
                      title: $t("menus.menuEpComprehensiveForm"),
                      showLink: true,
                      pid:1001170,
                      sort:1,
                      keepAlive:true,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                  },
                  {
                    path: "/FrontEndLibrary/ElementPlus/Form/StepForm",
                    name: "EpStepForm",
                    component: () => import("@/views/FrontEndLibrary/ElementPlus/Form/StepForm/index.vue"),
                    meta: {
                      id:1001172,
                      title: $t("menus.menuEpStepForm"),
                      showLink: true,
                      pid:1001170,
                      sort:1,
                      keepAlive:true,
                      transitionName:"",
                      i18n:true,
                      level:4,
                    },
                    children:[]
                  },
                ]
            },
          ]
        },
        {
          path: "/FrontEndLibrary/ComponentsLibrary",
          name: "ComponentsLibrary",
          redirect:"/FrontEndLibrary/ComponentsLibrary/ReIcon",
          meta: {
            id:1001200,
            title: $t("menus.menuComponentsLibrary"),
            showLink: true,
            icon:"ii:icon-component-library",
            pid:1001000,
            sort:2,
            keepAlive:true,
            transitionName:"",
            i18n:true,
            level:2,
          },
          children:[
              {
                  path: "/FrontEndLibrary/ComponentsLibrary/ReIcon",
                  name: "ReIcon",
                  component: () => import("@/views/FrontEndLibrary/ComponentsLibrary/ReIcon/index.vue"),
                  meta: {
                    id:1001210,
                    title: $t("menus.menuReIcon"),
                    showLink: true,
                    pid:1001200,
                    sort:1,
                    keepAlive:true,
                    transitionName:"",
                    i18n:true,
                    level:3,
                  },
                  children:[]
              },
              {
                  path: "/FrontEndLibrary/ComponentsLibrary/ReDrag",
                  name: "ReDrag",
                  component: () => import("@/views/FrontEndLibrary/ComponentsLibrary/ReDrag/index.vue"),
                  meta: {
                    id:1001220,
                    title: $t("menus.menuReDrag"),
                    showLink: true,
                    pid:1001200,
                    sort:2,
                    keepAlive:true,
                    transitionName:"",
                    i18n:true,
                    level:3,
                  },
                  children:[]
              }
          ]
        },
        {
          path: "/FrontEndLibrary/ViewsLibrary",
          name: "ViewsLibrary",
          redirect:"/FrontEndLibrary/ViewsLibrary/ReDemo",
          meta: {
            id:1001300,
            title: $t("menus.menuViewsLibrary"),
            showLink: true,
            icon:"ii:icon-view-library",
            pid:1001000,
            sort:3,
            keepAlive:true,
            transitionName:"",
            i18n:true,
            level:2,
          },
          children:[
              {
                  path: "/FrontEndLibrary/ViewsLibrary/ReDemo",
                  name: "ReDemo",
                  component: () => import("@/views/FrontEndLibrary/ViewsLibrary/ReDemo/index.vue"),
                  meta: {
                    id:1001310,
                    title: $t("menus.menuReDemo"),
                    showLink: true,
                    pid:1001300,
                    sort:1,
                    keepAlive:true,
                    transitionName:"",
                    i18n:true,
                    level:3,
                  },
                  children:[]
              },
              {
                  path: "/FrontEndLibrary/ViewsLibrary/ReEcharts",
                  name: "ReEcharts",
                  component: () => import("@/views/FrontEndLibrary/ViewsLibrary/ReEcharts/index.vue"),
                  meta: {
                    id:1001320,
                    title: $t("menus.menuReEcharts"),
                    showLink: true,
                    pid:1001300,
                    sort:1,
                    keepAlive:true,
                    transitionName:"",
                    i18n:true,
                    level:3,
                  },
                  children:[]
              },
          ]
        }
      ]
    }
  ]
  export default modulesRouter;
```
该路由文件重写了路由id与pid，并增加了`element-plus`相关的15个路由，其中包括12个路由页面。

14. `src/views/FrontEndLibrary/ElementPlus/Button/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpButton'
}
</script>
<script setup lang="ts">
import useScrollPosition from '@/hooks/scrollPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);
</script>
<template>
  <el-space :size="20" fill>
    <el-row :gutter="20">
      <el-col :md="12" :sm="24" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>基础用法</span>
            </div>
          </template>
          <el-form label-position="right" label-width="100px">
            <el-form-item label="默认按钮">
              <el-button>默认按钮</el-button>
              <el-button type="primary">主要按钮</el-button>
              <el-button type="success">成功按钮</el-button>
              <el-button type="info">信息按钮</el-button>
              <el-button type="warning">警告按钮</el-button>
              <el-button type="danger">危险按钮</el-button>
            </el-form-item>
            <el-form-item label="朴素按钮">
              <el-button plain>默认按钮</el-button>
              <el-button type="primary" plain>主要按钮</el-button>
              <el-button type="success" plain>成功按钮</el-button>
              <el-button type="info" plain>信息按钮</el-button>
              <el-button type="warning" plain>警告按钮</el-button>
              <el-button type="danger" plain>危险按钮</el-button>
            </el-form-item>
            <el-form-item label="圆角按钮">
              <el-button round>默认按钮</el-button>
              <el-button type="primary" round>主要按钮</el-button>
              <el-button type="success" round>成功按钮</el-button>
              <el-button type="info" round>信息按钮</el-button>
              <el-button type="warning" round>警告按钮</el-button>
              <el-button type="danger" round>危险按钮</el-button>
            </el-form-item>
            <el-form-item label="圆形图标按钮">
              <el-button icon="Search" circle />
              <el-button type="primary" icon="Edit" circle />
              <el-button type="success" icon="Check" circle />
              <el-button type="info" icon="Message" circle />
              <el-button type="warning" icon="Star" circle />
              <el-button type="danger" icon="Delete" circle />
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
      <el-col :md="12" :sm="24" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>禁用状态</span>
            </div>
          </template>
          <el-form label-position="right" label-width="100px">
            <el-form-item label="默认按钮">
              <el-button disabled>默认按钮</el-button>
              <el-button type="primary" disabled>主要按钮</el-button>
              <el-button type="success" disabled>成功按钮</el-button>
              <el-button type="info" disabled>信息按钮</el-button>
              <el-button type="warning" disabled>警告按钮</el-button>
              <el-button type="danger" disabled>危险按钮</el-button>
            </el-form-item>
            <el-form-item label="朴素按钮">
              <el-button plain disabled>默认按钮</el-button>
              <el-button type="primary" plain disabled>主要按钮</el-button>
              <el-button type="success" plain disabled>成功按钮</el-button>
              <el-button type="info" plain disabled>信息按钮</el-button>
              <el-button type="warning" plain disabled>警告按钮</el-button>
              <el-button type="danger" plain disabled>危险按钮</el-button>
            </el-form-item>
            <el-form-item label="圆角按钮">
              <el-button round disabled>默认按钮</el-button>
              <el-button type="primary" round disabled>主要按钮</el-button>
              <el-button type="success" round disabled>成功按钮</el-button>
              <el-button type="info" round disabled>信息按钮</el-button>
              <el-button type="warning" round disabled>警告按钮</el-button>
              <el-button type="danger" round disabled>危险按钮</el-button>
            </el-form-item>
            <el-form-item label="圆形图标按钮">
              <el-button icon="Search" circle disabled />
              <el-button type="primary" icon="Edit" circle disabled />
              <el-button type="success" icon="Check" circle disabled />
              <el-button type="info" icon="Message" circle disabled />
              <el-button type="warning" icon="Star" circle disabled />
              <el-button type="danger" icon="Delete" circle disabled />
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>文字按钮</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="text">文字按钮</el-button>
              <el-button type="text" disabled>文字禁用按钮</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>element plus内置图标按钮</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="primary" icon="Edit" />
              <el-button type="primary" icon="Star" />
              <el-button type="primary" icon="Delete" />
              <el-button type="primary" icon="Search">搜索</el-button>
              <el-button type="primary">上传 <svg-icon class="el-icon--right" icon-class="Upload"></svg-icon></el-button>
              <el-button type="text" icon="Search">图标文字按钮</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>自定义图标按钮</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="primary"><svg-icon class="el-icon--left" icon-class="icon-sun"></svg-icon>太阳</el-button>
              <el-button type="primary">月亮 <svg-icon class="el-icon--right" icon-class="icon-moon"></svg-icon></el-button>
              <el-button type="primary"><svg-icon icon-class="icon-sousuo"></svg-icon></el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>按钮组</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button-group>
                <el-button type="primary" icon="ArrowLeft">上一页</el-button>
                <el-button type="primary">下一页<svg-icon class="el-icon--right" icon-class="ArrowRight"></svg-icon></el-button>
              </el-button-group>
              <el-button-group>
                <el-button type="primary" icon="CirclePlus"></el-button>
                <el-button type="primary" icon="Edit"></el-button>
                <el-button type="primary" icon="Delete"></el-button>
              </el-button-group>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>加载中</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="primary" loading>加载中</el-button>
              <el-button type="text" loading>加载中</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>各种尺寸的按钮</span>
            </div>
          </template>
          <el-form>
            <el-form-item label="大型按钮">
              <el-button size="large">默认按钮</el-button>
              <el-button size="large" plain>朴素按钮</el-button>
              <el-button size="large" round>圆角按钮</el-button>
              <el-button size="large" icon="Search">图标按钮</el-button>
              <el-button size="large" icon="Search" circle />
            </el-form-item>
            <el-form-item label="默认按钮">
              <el-button>默认按钮</el-button>
              <el-button plain>朴素按钮</el-button>
              <el-button round>圆角按钮</el-button>
              <el-button icon="Search">图标按钮</el-button>
              <el-button icon="Search" circle />
            </el-form-item>
            <el-form-item label="小型按钮">
              <el-button size="small">默认按钮</el-button>
              <el-button size="small" plain>朴素按钮</el-button>
              <el-button size="small" round>圆角按钮</el-button>
              <el-button size="small" icon="Search">图标按钮</el-button>
              <el-button size="small" icon="Search" circle />
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>自定义按钮颜色</span>
            </div>
          </template>
          <el-form>
            <el-form-item label="基础用法">
              <el-button color="#626aef" style="color: #ffffff;">默认按钮</el-button>
              <el-button color="#626aef" plain>朴素按钮</el-button>
              <el-button color="#626aef" style="color: #ffffff;" round>圆角按钮</el-button>
              <el-button color="#626aef" style="color: #ffffff;" icon="Search" circle />
            </el-form-item>
            <el-form-item label="禁用状态">
              <el-button color="#626aef" style="color: #ffffff;" disabled>默认按钮</el-button>
              <el-button color="#626aef" plain disabled>朴素按钮</el-button>
              <el-button color="#626aef" style="color: #ffffff;" round disabled>圆角按钮</el-button>
              <el-button color="#626aef" style="color: #ffffff;" icon="Search" circle disabled />
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
</style>
```
该文件主要展示`element-plus`的按钮组件的各种用法，`element-plus`组件通过自身的`icon`属性绑定的图标，只能使用`element-plus`自身的图标，所以前面需要先在全局注册`element-plus`的图标组件。否则图标将无法显示。

15. `src/views/FrontEndLibrary/ElementPlus/Link/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpLink'
}
</script>
<script setup lang="ts">
import useScrollPosition from '@/hooks/scrollPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);
</script>
<template>
  <el-space :size="20" fill>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>基础用法</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-link href="https://www.baidu.com" target="_blank">默认链接</el-link>
              <el-link type="primary">主要链接</el-link>
              <el-link type="success">成功链接</el-link>
              <el-link type="warning">警告链接</el-link>
              <el-link type="danger">危险链接</el-link>
              <el-link type="info">信息链接</el-link>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>禁用状态</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-link href="https://www.baidu.com" target="_blank" disabled>默认链接</el-link>
              <el-link type="primary" disabled>主要链接</el-link>
              <el-link type="success" disabled>成功链接</el-link>
              <el-link type="warning" disabled>警告链接</el-link>
              <el-link type="danger" disabled>危险链接</el-link>
              <el-link type="info" disabled>信息链接</el-link>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>下划线</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-link href="https://www.baidu.com" target="_blank" :underline="false">无下划线</el-link>
              <el-link href="https://www.baidu.com" target="_blank">有下划线</el-link>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>带图标链接</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-link icon="Edit">编辑</el-link>
              <el-link>选中<svg-icon class="el-icon--right" icon-class="Check"></svg-icon></el-link>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
</style>
```
这里是`element-plus`的文本链接组件的使用示例。

16. `src/views/FrontEndLibrary/ElementPlus/Tag/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpTag'
}
</script>
<script setup lang="ts">
import { ref,nextTick } from 'vue'
import useScrollPosition from '@/hooks/scrollPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

const inputValue = ref('')
const dynamicTags = ref(['标签1', '标签2', '标签3'])
const inputVisible = ref(false)
const InputRef = ref()


const handleClose = (tag: string) => {
  dynamicTags.value.splice(dynamicTags.value.indexOf(tag), 1)
}
const showInput = () => {
  inputVisible.value = true
  nextTick(() => {
    InputRef.value!.input!.focus()
  })
}
const handleInputConfirm = () => {
  if (inputValue.value) {
    dynamicTags.value.push(inputValue.value)
  }
  inputVisible.value = false
  inputValue.value = ''
}


const checked = ref(false)
const onChange = (status: boolean) => {
  checked.value = status
}
</script>
<template>
  <el-space :size="20" fill>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>基础用法</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-tag>默认标签</el-tag>
              <el-tag type="success">成功标签</el-tag>
              <el-tag type="info">信息标签</el-tag>
              <el-tag type="warning">警告标签</el-tag>
              <el-tag type="danger">危险标签</el-tag>
              <el-tag color="#626aef" style="color: #ffffff;" effect="dark">自定义颜色标签</el-tag>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>可移除标签</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-tag closable>默认标签</el-tag>
              <el-tag closable type="success">成功标签</el-tag>
              <el-tag closable type="info">信息标签</el-tag>
              <el-tag closable type="warning">警告标签</el-tag>
              <el-tag closable type="danger">危险标签</el-tag>
              <el-tag closable color="#626aef" style="color: #ffffff;" effect="dark">自定义颜色标签</el-tag>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>动态添加标签</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-tag
                v-for="tag in dynamicTags"
                :key="tag"
                closable
                :disable-transitions="false"
                @close="handleClose(tag)"
              >{{ tag }}</el-tag>
              <el-input
                v-if="inputVisible"
                ref="InputRef"
                style="width: 70px;display:inline-block;margin:6px;"
                v-model="inputValue"
                size="small"
                @keyup.enter="handleInputConfirm"
                @blur="handleInputConfirm" />
              <el-button v-else size="small" @click="showInput">添加标签</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>各种尺寸的标签</span>
            </div>
          </template>
          <el-form>
            <el-form-item label="大型标签">
              <el-tag size="large" closable>默认标签</el-tag>
              <el-tag size="large" closable type="success">成功标签</el-tag>
              <el-tag size="large" closable type="info">信息标签</el-tag>
              <el-tag size="large" closable type="warning">警告标签</el-tag>
              <el-tag size="large" closable type="danger">危险标签</el-tag>
              <el-tag size="large" closable color="#626aef" style="color: #ffffff;" effect="dark">自定义颜色标签</el-tag>
            </el-form-item>
            <el-form-item label="默认标签">
              <el-tag closable>默认标签</el-tag>
              <el-tag closable type="success">成功标签</el-tag>
              <el-tag closable type="info">信息标签</el-tag>
              <el-tag closable type="warning">警告标签</el-tag>
              <el-tag closable type="danger">危险标签</el-tag>
              <el-tag closable color="#626aef" style="color: #ffffff;" effect="dark">自定义颜色标签</el-tag>
            </el-form-item>
            <el-form-item label="小型标签">
              <el-tag size="small" closable>默认标签</el-tag>
              <el-tag size="small" closable type="success">成功标签</el-tag>
              <el-tag size="small" closable type="info">信息标签</el-tag>
              <el-tag size="small" closable type="warning">警告标签</el-tag>
              <el-tag size="small" closable type="danger">危险标签</el-tag>
              <el-tag size="small" closable color="#626aef" style="color: #ffffff;" effect="dark">自定义颜色标签</el-tag>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>标签主题</span>
            </div>
          </template>
          <el-form>
            <el-form-item label="深色标签">
              <el-tag effect="dark" closable>默认标签</el-tag>
              <el-tag effect="dark" closable type="success">成功标签</el-tag>
              <el-tag effect="dark" closable type="info">信息标签</el-tag>
              <el-tag effect="dark" closable type="warning">警告标签</el-tag>
              <el-tag effect="dark" closable type="danger">危险标签</el-tag>
            </el-form-item>
            <el-form-item label="浅色标签">
              <el-tag closable>默认标签</el-tag>
              <el-tag closable type="success">成功标签</el-tag>
              <el-tag closable type="info">信息标签</el-tag>
              <el-tag closable type="warning">警告标签</el-tag>
              <el-tag closable type="danger">危险标签</el-tag>
            </el-form-item>
            <el-form-item label="朴素标签">
              <el-tag effect="plain" closable>默认标签</el-tag>
              <el-tag effect="plain" closable type="success">成功标签</el-tag>
              <el-tag effect="plain" closable type="info">信息标签</el-tag>
              <el-tag effect="plain" closable type="warning">警告标签</el-tag>
              <el-tag effect="plain" closable type="danger">危险标签</el-tag>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
    <el-row>
      <el-col>
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>可选中的标签</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-check-tag checked>选中的标签</el-check-tag>
              <el-check-tag :checked="checked" @change="onChange">可选中标签</el-check-tag>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
</style>

```
该文件主要展示`element-plus`标签组件的各种用法，其中自定义颜色功能在当前版本，还不成熟，不要使用。

17. `src/views/FrontEndLibrary/ElementPlus/Notice/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpNotice'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
import { ElMessage,ElNotification } from "element-plus";
import { Message, successMessage, warnMessage, errorMessage } from "@/utils/message";
import useScrollPosition from '@/hooks/scrollPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

const open1 = () => {
  ElMessage({
    showClose: true,
    message: '信息消息提示',
    type: 'info',
    duration:0,
  })
}
const open2 = () => {
  ElMessage({
    showClose: true,
    message: '成功消息提示',
    type: 'success',
    duration:0,
  })
}
const open3 = () => {
  ElMessage({
    showClose: true,
    message: '警告消息提示',
    type: 'warning',
    duration:0,
  })
}
const open4 = () => {
  ElMessage({
    showClose: true,
    message: '错误消息提示',
    type: 'error',
    duration:0,
  })
}

const open5 = () => {
  Message('信息消息提示');
}
const open6 = () => {
  successMessage('成功消息提示');
}
const open7 = () => {
  warnMessage('警告消息提示');
}
const open8 = () => {
  errorMessage('错误消息提示');
}


const open9 = () => {
  ElNotification({
    title: '消息',
    message: '信息消息提示',
    type: 'info',
    duration:0,
  })
}
const open10 = () => {
  ElNotification({
    title: '成功',
    message: '成功消息提示',
    type: 'success',
    duration:0,
  })
}
const open11 = () => {
  ElNotification({
    title: '警告',
    message: '警告消息提示',
    type: 'warning',
    duration:0,
  })
}
const open12 = () => {
  ElNotification({
    title: '错误',
    message: '错误消息提示',
    type: 'error',
    duration:0,
  })
}
</script>
<template>
  <el-space :size="20" fill>
    <el-row :gutter="20">
      <el-col :md="12" :sm="24" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>Alert 消息提示（浅色主题）</span>
            </div>
          </template>
          <h3>基础用法</h3>
          <el-alert title="成功提示" type="success" show-icon />
          <el-alert title="信息提示" type="info" show-icon />
          <el-alert title="警告提示" type="warning" show-icon />
          <el-alert title="错误提示" type="error" show-icon />
          <h3>带描述用法</h3>
          <el-alert title="成功提示" description="带描述的提示，显示更多提示文本" type="success" show-icon />
          <el-alert title="信息提示" description="带描述的提示，显示更多提示文本" type="info" show-icon />
          <el-alert title="警告提示" description="带描述的提示，显示更多提示文本" type="warning" show-icon />
          <el-alert title="错误提示" description="带描述的提示，显示更多提示文本" type="error" show-icon />
        </el-card>
      </el-col>
      <el-col :md="12" :sm="24" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>Alert 消息提示（深色主题）</span>
            </div>
          </template>
          <h3>基础用法</h3>
          <el-alert title="成功提示" type="success" show-icon effect="dark" />
          <el-alert title="信息提示" type="info" show-icon effect="dark" />
          <el-alert title="警告提示" type="warning" show-icon effect="dark" />
          <el-alert title="错误提示" type="error" show-icon effect="dark" />
          <h3>带描述用法</h3>
          <el-alert title="成功提示" description="带描述的提示，显示更多提示文本" type="success" show-icon effect="dark" />
          <el-alert title="信息提示" description="带描述的提示，显示更多提示文本" type="info" show-icon effect="dark" />
          <el-alert title="警告提示" description="带描述的提示，显示更多提示文本" type="warning" show-icon effect="dark" />
          <el-alert title="错误提示" description="带描述的提示，显示更多提示文本" type="error" show-icon effect="dark" />
        </el-card>
      </el-col>
    </el-row>
    <el-row :gutter="20">
      <el-col :md="8" :sm="8" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>Message 消息提示</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="info" :plain="true" @click="open1">消息</el-button>
              <el-button type="success" :plain="true" @click="open2">成功</el-button>
              <el-button type="warning" :plain="true" @click="open3">警告</el-button>
              <el-button type="danger" :plain="true" @click="open4">错误</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
      <el-col :md="8" :sm="8" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>Message 消息提示（封装用法）</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="info" :plain="true" @click="open5">消息</el-button>
              <el-button type="success" :plain="true" @click="open6">成功</el-button>
              <el-button type="warning" :plain="true" @click="open7">警告</el-button>
              <el-button type="danger" :plain="true" @click="open8">错误</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
      <el-col :md="8" :sm="8" :xs="24">
        <el-card shadow="hover">
          <template #header>
            <div class="card-header">
              <span>Notification 通知提示</span>
            </div>
          </template>
          <el-form>
            <el-form-item>
              <el-button type="info" :plain="true" @click="open9">消息</el-button>
              <el-button type="success" :plain="true" @click="open10">成功</el-button>
              <el-button type="warning" :plain="true" @click="open11">警告</el-button>
              <el-button type="danger" :plain="true" @click="open12">错误</el-button>
            </el-form-item>
          </el-form>
        </el-card>
      </el-col>
    </el-row>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.el-alert+.el-alert{
  margin-top: 20px;
}
</style>
```
该文件主要是`element-plus`的消息提示，通知相关组件的使用示例。

18. `src/views/FrontEndLibrary/ElementPlus/Feedback/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpFeedback'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
import { ElMessage,ElMessageBox } from 'element-plus'
import error404 from '@/assets/404.png'
import useScrollPosition from '@/hooks/scrollPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

const dialogVisible = ref(false)

const handleClose = (done: () => void) => {
  ElMessageBox.confirm('确认要关闭这个对话框吗？')
    .then(() => {
      done()
    })
    .catch(() => {
      // catch error
    })
}
const open1 = () => {
  ElMessageBox.alert('模拟系统弹窗框的弹出信息', '有新消息！', {
    showCancelButton:true,
    confirmButtonText: '确认',
    cancelButtonText: '取消',
    callback: (action) => {
      if(action === 'confirm'){
        ElMessage({
          type: 'info',
          message: `操作: 点击了确认`,
        })
      }else{
        ElMessage({
          type: 'info',
          message: `操作: 点击了取消`,
        })
      }
    },
  })
}

</script>
<template>
  <div>
    <el-space :size="20" fill>
      <el-row>
        <el-col>
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>反馈组件</span>
              </div>
            </template>
            <el-form>
              <el-form-item>
                <el-button type="primary" :plain="true" @click="dialogVisible = true">打开对话框</el-button>
                <el-button type="primary" :plain="true" @click="open1">打开消息弹出框</el-button>
                <el-popconfirm
                  confirm-button-text="确认"
                  cancel-button-text="不，谢谢"
                  icon="InfoFilled"
                  icon-color="red"
                  title="确认删除此条信息吗？">
                  <template #reference>
                    <el-button type="primary" :plain="true">汽包确认消息框</el-button>
                  </template>
                </el-popconfirm>
                <el-popover
                  placement="bottom"
                  title="提示："
                  :width="200"
                  trigger="hover"
                  content="这里是信息提示，该提示可以通过点击形式展现">
                  <template #reference>
                    <el-button type="primary" :plain="true">信息提示弹出框</el-button>
                  </template>
                </el-popover>
                <el-tooltip
                  effect="dark"
                  content="文字提示信息"
                  placement="right">
                  <el-button type="primary" :plain="true">文字提示</el-button>
                </el-tooltip>
              </el-form-item>
            </el-form>
          </el-card>
        </el-col>
      </el-row>
      <el-row :gutter="20">
        <el-col :sm="12" :lg="6">
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>信息结果反馈</span>
              </div>
            </template>
            <el-result icon="info" title="信息提示">
              <template #sub-title>
                <p>请根据提示进行操作</p>
              </template>
              <template #extra>
                <el-button type="primary">返回</el-button>
              </template>
            </el-result>
          </el-card>
        </el-col>
        <el-col :sm="12" :lg="6">
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>成功结果反馈</span>
              </div>
            </template>
            <el-result
              icon="success"
              title="成功提示"
              sub-title="请根据提示进行操作">
              <template #extra>
                <el-button type="primary">返回</el-button>
              </template>
            </el-result>
          </el-card>
        </el-col>
        <el-col :sm="12" :lg="6">
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>警告结果反馈</span>
              </div>
            </template>
            <el-result
              icon="warning"
              title="警告提示"
              sub-title="请根据提示进行操作">
              <template #extra>
                <el-button type="primary">返回</el-button>
              </template>
            </el-result>
          </el-card>
        </el-col>
        <el-col :sm="12" :lg="6">
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>错误结果反馈</span>
              </div>
            </template>
          <el-result
            icon="error"
            title="错误提示"
            sub-title="请根据提示进行操作">
            <template #extra>
              <el-button type="primary">返回</el-button>
            </template>
          </el-result>
          </el-card>
        </el-col>
      </el-row>
      <el-row :gutter="20">
        <el-col :sm="12" :lg="12">
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>自定义结果反馈</span>
              </div>
            </template>
            <el-result title="404">
              <template #icon>
                <el-image :src="error404" style="width:200px;" />
              </template>
              <template #sub-title>
                <p>请根据提示进行操作</p>
              </template>
              <template #extra>
                <el-button type="primary">返回</el-button>
              </template>
            </el-result>
          </el-card>
        </el-col>
        <el-col :sm="12" :lg="12">
          <el-card shadow="hover">
            <template #header>
              <div class="card-header">
                <span>空数据结果反馈</span>
              </div>
            </template>
            <el-empty description="暂无数据" :image-size="200">
              <el-button type="primary">返回</el-button>
            </el-empty>
          </el-card>
        </el-col>
      </el-row>
    </el-space>
    <el-dialog
      v-model="dialogVisible"
      title="对话框"
      width="30%"
      :before-close="handleClose">
      <span>对话框框信息</span>
      <template #footer>
        <span class="dialog-footer">
          <el-button @click="dialogVisible = false">取消</el-button>
          <el-button type="primary" @click="dialogVisible = false">确认</el-button>
        </span>
      </template>
    </el-dialog>
  </div>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
</style>
```
该文件主要是`element-plus`的各种信息反馈的组件使用示例，包括结果反馈类，弹窗反馈类。

19. `src/views/FrontEndLibrary/ElementPlus/Data/PaginationList/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpPaginationList'
}
</script>
<script setup lang="ts">
import { ref,reactive } from 'vue'
import {getPaginationList} from "@/api/elementPlus";
import useScrollPosition from '@/hooks/scrollPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

let initData:any = reactive({
  data:[],
  total:0,
});
let pageIndex = ref(1)
let pageSize = ref(20)
let loading = ref(false)
const getInitData = () => {
  loading.value = true;
  getPaginationList({pageIndex:pageIndex.value,pageSize:pageSize.value},{config:{showLoading:false,mockEnable:true}}).then((res:any)=>{
    console.log(res);
    if(res.data&&res.data.length>0){
      setTimeout(()=>{
        initData.data=[];
        initData.data.push(...res.data);
        initData.total = res.total;
        console.log(initData)
        loading.value = false
      },2000);
    }
  });
}
getInitData();
const handleSizeChange = (val: number) => {
  pageSize.value = val;
  pageIndex.value = 1;
  getInitData();
}
const handleCurrentChange = (val: number) => {
  pageIndex.value = val;
  getInitData();
}
</script>
<template>
  <el-space :size="20" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <el-alert title="分页类列表展示，本页面主要使用element-plus的骨架屏、图片、进度条、分页、消息提示、空数据状态（数据加载后总条数改为0）组件。" type="success" :closable="false" show-icon />
      <el-skeleton :loading="loading" animated :count="6">
        <template #template>
          <div class="list-item">
            <div class="item-image">
              <el-skeleton-item variant="image" style="width: 76px; height: 76px" />
            </div>
            <div class="item-content">
              <el-skeleton-item variant="text" style="width: 50%;margin:4px 0" />
              <el-skeleton-item variant="text" />
            </div>
            <div class="item-content">
              <el-skeleton-item variant="text" style="width: 20%;margin:4px 0" />
              <el-skeleton-item variant="text" style="width: 40%;" />
            </div>
            <div class="item-content">
              <el-skeleton-item variant="text" style="width: 20%;margin:4px 0" />
              <el-skeleton-item variant="text" style="width: 80%;" />
            </div>
            <div class="item-content">
              <el-skeleton-item variant="text" />
            </div>
          </div>
        </template>
        <template #default>
          <el-empty v-if="initData.total===0" :image-size="200" :description="$t('settings.settingNoData')" />
          <ul v-else class="list-container">
            <li class="list-item" v-for="(item,index) in initData.data">
              <div class="item-image">
                <el-image :src="'https://cdn.jsdelivr.net/gh/kaivin/images/list/'+item.type+'/'+item.img+'.jpg'" fit="cover" lazy>
                  <template #placeholder>
                    <div class="image-slot">
                      <div class="slot-content">
                        <svg-icon icon-class="ep:picture"></svg-icon>
                        <p>{{$t('settings.settingLoadingText')}}</p>
                      </div>
                    </div>
                  </template>
                  <template #error>
                    <div class="image-slot">
                      <div class="slot-content">
                        <svg-icon icon-class="ii:icon-image-failed"></svg-icon>
                        <p>{{$t('settings.settingLoadingFailed')}}</p>
                      </div>
                    </div>
                  </template>
                </el-image>
              </div>
              <div class="item-content">
                <div class="item-title">{{item.title}}</div>
                <div class="item-description">{{item.description}}</div>
              </div>
              <div class="item-content">
                <div class="item-thead">作者</div>
                <div class="item-description">{{item.author}}</div>
              </div>
              <div class="item-content">
                <div class="item-thead">时间</div>
                <div class="item-description">{{item.date}}</div>
              </div>
              <div class="item-content">
                <el-progress v-if="item.percentage>90" :stroke-width="10" :percentage="item.percentage" status="success" />
                <el-progress v-if="item.percentage>70&&item.percentage<=90" :stroke-width="10" :percentage="item.percentage" status="warning" />
                <el-progress v-if="item.percentage>50&&item.percentage<=70" :stroke-width="10" :percentage="item.percentage" status="exception" />
                <el-progress v-if="item.percentage<=50" :stroke-width="10" :percentage="item.percentage" :color="item.color" />
              </div>
            </li>
          </ul>
        </template>
      </el-skeleton>
      <el-pagination v-if="initData.total>0" v-model:currentPage="pageIndex" v-model:page-size="pageSize" :page-sizes="[20, 40, 100, 200]" :background="true" layout="total, sizes, prev, pager, next, jumper" :total="initData.total" @size-change="handleSizeChange"  @current-change="handleCurrentChange" />
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">

.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.no-border{
  border:none;
}
.no-radius{
  border-radius: 0;
}
.el-pagination{
  margin-top: 20px;
  justify-content: center;
}
.list-item{
  display: flex;
  padding: 10px;
  border-bottom: 1px solid var(--el-border-color-lighter);
  .item-image{
    padding: 10px;
    .el-image{
      width: 76px;
      height: 76px;
    }
  }
  .item-content{
    flex: 1;
    padding: 10px;
    display: flex;
    align-items: start;
    flex-direction:column;
    justify-content: center;
    .item-title{
      margin: 4px 0 4px 0;
      color: var(--el-text-color-regular);
    }
    .item-thead{
      margin: 4px 0 4px 0;
      color: var(--el-text-color-placeholder);
    }
    .item-description{
      color: var(--el-text-color-placeholder);
    }
    .el-progress{
      width: 100%;
      :deep(.el-progress__text){
        font-size: 14px!important;
      }
    }
  }
}
</style>
```
该页面主要是分页类列表页的使用，其中使用的到的组件较多，页面小时提示栏有说明，需要说明的是，列表页的图片使用的`github`单独创建的一个存放图片的项目，通过`jsdelivr`做的图片CDN，也就是图床功能。

20. `src/views/FrontEndLibrary/ElementPlus/Data/InfiniteScrollList/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpInfiniteScrollList'
}
</script>
<script setup lang="ts">
import { ref,reactive,onMounted,onActivated } from 'vue'
import {getInfiniteScrollList} from "@/api/elementPlus";
import slot_16x9 from "@/assets/slot_16x9.png";
import useScrollPosition from '@/hooks/scrollPosition';
import useAffixPosition from '@/hooks/affixPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

let {offsetHeight,affixTop} = useAffixPosition();

let initData:any = reactive({
  data:[],
  total:0,
});
let imagePreviewList = ref([]);
let pageIndex = ref(0)
let pageSize = ref(20)
let pageCount = ref(1)
let loading = ref(false)

const formResult = reactive({
  type: [],
  date: '',
  name: '',
})
const onSubmit = () => {
  console.log('submit!');
  initData.data = [];
  imagePreviewList.value = [];
  getInitData();
}
const typeList = [
  {
    value: 'landscape',
    label: '风景',
  },
  {
    value: 'animal',
    label: '动物',
  },
  {
    value: 'food',
    label: '美食',
  },
  {
    value: 'beauty',
    label: '美女',
  },
  {
    value: 'car',
    label: '汽车',
  },
  {
    value: 'cartoon',
    label: '动漫',
  },
  {
    value: 'game',
    label: '游戏',
  },
  {
    value: 'other',
    label: '其他',
  },
]

const getInitData = () => {
  if(pageIndex.value<pageCount.value){
    pageIndex.value += 1;
    loading.value = true;
    getInfiniteScrollList({pageIndex:pageIndex.value,pageSize:pageSize.value,type:formResult.type},{config:{showLoading:false,mockEnable:true}}).then((res:any)=>{
      console.log(res);
      if(res.data&&res.data.length>0){
        setTimeout(()=>{
          res.data.forEach(function(item,index){
            item.src = 'https://cdn.jsdelivr.net/gh/kaivin/images/list/'+item.type+'/'+item.img+'.jpg';
            initData.data.push(item);
            imagePreviewList.value.push(item.src);
          });
          initData.total = res.total;
          pageCount.value = Math.ceil(res.total/pageSize.value);
          console.log(initData)
          loading.value = false;
          affixTop();
        },1000);
      }
    });
  }
}
</script>
<template>
  <div class="infinite-page">
    <div v-infinite-scroll="getInitData" class="infinite-list" id="self-scroll">
      <el-alert title="无限滚动列表展示，本页面主要使用element-plus的无限滚动、固钉、表单组件，实现图片的加载中占位、加载失败占位，图片预览功能。" type="success" :closable="false" show-icon />
      <el-affix :offset="offsetHeight" target="#self-scroll">
        <el-card shadow="hover">
          <el-form :inline="true">
            <el-form-item label="类型">
              <el-select v-model="formResult.type" multiple collapse-tags placeholder="选择类型">
                <el-option
                  v-for="item in typeList"
                  :key="item.value"
                  :label="item.label"
                  :value="item.value" />
              </el-select>
            </el-form-item>
            <el-form-item label="时间">
              <el-date-picker
                v-model="formResult.date"
                type="datetime"
                placeholder="选择日期和时间" />
            </el-form-item>
            <el-form-item label="名称">
              <el-input v-model="formResult.name" placeholder="请输入名称" />
            </el-form-item>
            <el-form-item><el-button type="primary" icon="Search" @click="onSubmit">查询</el-button></el-form-item>
          </el-form>
        </el-card>
      </el-affix>
      <el-row :gutter="20">
        <el-col :xs="24" :sm="12" :md="8" :lg="8" :xl="6" v-for="(item,index) in initData.data" :key="item.id">
          <el-card shadow="hover">
            <el-image 
              :src="item.src" 
              fit="cover"
              :preview-src-list="imagePreviewList"
              :initial-index="index"
              lazy>
              <template #placeholder>
                <div class="image-slot">
                  <img :src="slot_16x9" />
                  <div class="slot-content">
                    <svg-icon icon-class="Picture"></svg-icon>
                    <p>{{$t('settings.settingLoadingText')}}</p>
                  </div>
                </div>
              </template>
              <template #error>
                <div class="image-slot">
                  <img :src="slot_16x9" />
                  <div class="slot-content">
                    <svg-icon icon-class="icon-image-failed"></svg-icon>
                    <p>{{$t('settings.settingLoadingFailed')}}</p>
                  </div>
                </div>
              </template>
            </el-image>
            <div class="item-title">{{item.title}}</div>
            <div class="item-description">{{item.description}}</div>
            <div class="item-date">{{item.date}}</div>
          </el-card>
        </el-col>
      </el-row>
      <el-divider v-if="loading" border-style="dashed"><svg-icon class="is-loading" icon-class="Loading"></svg-icon>{{$t('settings.settingLoadingText')}}</el-divider>
      <el-divider v-if="pageIndex===pageCount&&!loading" border-style="dashed">{{$t('settings.settingNoMore')}}</el-divider>
      <el-backtop :title="$t('buttons.buttonBackTop')" target="#self-scroll"></el-backtop>
    </div>
  </div>
</template>

<style scoped lang="scss">
.infinite-page{
  overflow: hidden;
  height: calc(100vh - var(--el-header-height) - var(--el-tag-height));
}
.infinite-list {
  padding: 20px;
  margin: 0;
  list-style: none;
  overflow: auto;
  height: 100%;
  :deep(.el-divider__text){
    background-color: var(--el-body-bg-color);
    display: flex;
    align-items: center;
    color: var(--el-text-color-placeholder);
    .el-icon{
      margin-right: 5px;
    }
  }
}
.el-row .el-col{
  margin-top: 20px;
  .el-card{
    cursor: pointer;
    :deep(.el-card__body){
      padding:0;
      overflow: hidden;
    }
    :deep(.el-image){
      display: block;
      width: 100%;
    }
  }
  .item-title{
    margin: 15px 20px 10px;
    font-size: 16px;
    font-weight: 700;
  }
  .item-description{
    margin: 0 20px 10px;
    opacity: .7;
  }
  .item-date{
    margin: 0 20px 15px;
    opacity: .5;
  }
}
.el-alert{
  margin-bottom: 20px;
}
</style>
```
这里用到了`element-plus`的无限加载组件，以及固钉组件，另外为了在图片未加载出来之前将图片区域撑开，额外做了一张16:9的占位图片，因为这里使用的页面内自身的滚动条，无限滚动加载需要使用页面自身的滚动条，就需要对套在框架层的滚动条做判断，以及公共滚动组件也需要做修改，路由中监听滚动条也需要做判断，固钉效果这里封装成了一个组合式API，可以在其他页面进行复用。

21. `src/router/utils.ts`文件：
```
// 处理缓存路由（添加、删除、刷新）
const handleAliveRoute = (matched: RouteRecordNormalized[], mode?: string) => {
  switch (mode) {
    case "add":
      matched.forEach(v => {
        store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "add", name: v.name });
      });
      break;
    case "delete":
      store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "delete", name: matched[matched.length - 1].name });
      break;
    default:
      store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "delete", name: matched[matched.length - 1].name });
      setTimeout(() => {
        matched.forEach(v => {
          store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "add", name: v.name });
        });
      }, 10);
  }
};


// 使用自定义路由的页面集合
const selfScrollPage = ['EpInfiniteScrollList']

export {
  constantRoutesFilter,
  treeToAscending,
  filterMenuTree,
  initRouter,
  dynamicRouteTag,
  handleAliveRoute,
  delAliveRoutes,
  routerTreeToFlattening,
  selfScrollPage
};

```
本页面因`@vueuse/core`版本升级，删除了`useTimeoutFn`方法，所以这里将`handleAliveRoute`中移除该方法的使用，改为使用`setTimeout`，另外新增了`selfScrollPage`变量，接收所有使用页面自身滚动条的页面，将页面的`name`集合到该变量内，并将该变量暴露出去。

22. `src/router/index.ts`文件：
```
import {constantRoutesFilter,initRouter,dynamicRouteTag,handleAliveRoute,selfScrollPage} from "./utils";

// 页面滚动位置缓存
if(_from.meta.keepAlive){
  let hasRouteTag = false;
  store.state.multiTags.multiTags.forEach((item)=>{
    if(item.path === _from.fullPath){
      hasRouteTag = true;
    }
  });
  if(hasRouteTag){
    var itemPosition:any = {};
    itemPosition.name = _from.name;
    itemPosition.fullPath = _from.fullPath;
    if(selfScrollPage.includes(_from.name as string)){
      itemPosition.savedPosition = document.querySelector("#self-scroll").scrollTop;
    }else{
      itemPosition.savedPosition = document.querySelector("#scrollbar .el-scrollbar__wrap").scrollTop;
    }
    store.dispatch('keepAlivePages/setPosition',itemPosition);
  }
}
```
这里主要修改逻辑，在记录页面滚动位置的时候，需要判断是页面自身滚动条，还是框架层的滚动条。

23. `src/layout/components/appMain.vue`文件：
```
<script setup lang="ts">
import { computed } from "vue";
import { useStore } from 'vuex'
import { selfScrollPage } from "@/router/utils";
const store = useStore();
const cachePage = computed(()=>{
  return store.state.keepAlivePages.keepAliveNames;
});
</script>
<template>
  <div>
    <router-view>
      <template #default="{ Component, route }">
        <transition v-if="selfScrollPage.includes(route.name)" :name="route.meta?.transitionName&&route.meta?.transitionName!=''?route.meta?.transitionName as string:'fade-slide'" mode="out-in" appear>
          <keep-alive :include="cachePage">
            <component :is="Component" :key="route.fullPath" class="vk-layout-view" />
          </keep-alive>
        </transition>
        <el-scrollbar v-else id="scrollbar">
          <el-backtop :title="$t('buttons.buttonBackTop')" target="#scrollbar .el-scrollbar__wrap"></el-backtop>
          <transition :name="route.meta?.transitionName&&route.meta?.transitionName!=''?route.meta?.transitionName as string:''" mode="out-in" appear>
            <keep-alive :include="cachePage">
              <component :is="Component" :key="route.fullPath" class="vk-layout-view" />
            </keep-alive>
          </transition>
        </el-scrollbar>
      </template>
    </router-view>
  </div>
</template>
<style lang="scss" scoped></style>
```
这里通过路由的`name`属性判断，页面是否是使用了自身的滚动条。

24. `src/hooks/scrollPosition.ts`文件：
```
import { onMounted,onActivated,computed } from 'vue'
import { selfScrollPage } from "@/router/utils";
const useScrollPosition = (store,route) => {
  // 获取保存的滚动位置数据
  const savedPosition = computed(()=>{
    return store.state.keepAlivePages.savedPosition;
  });
  // 获取当前页面的缓存状态是否被开启
  const isKeepAlive = route.meta.keepAlive;
  // 当前路由名
  const currentFullPath = route.fullPath;
  // 默认返回页面顶部
  const scrollToTop = () => {
    if(!isKeepAlive){
      if(selfScrollPage.includes(route.name)){
        document.querySelector("#self-scroll").scrollTo(0,0);
      }else{
        document.querySelector("#scrollbar .el-scrollbar__wrap").scrollTo(0,0);
      }
    }
  }
  // 页面滚动到记录的位置
  const scrollToPosition = () => {
    // 开启页面状态缓存的情况下，监听事件在此声明周期内开启
    if(isKeepAlive){
      console.log(savedPosition.value,"滚动记录");
      let hasRoute = false;
      if(savedPosition.value.length>0){
        savedPosition.value.forEach((item)=>{
          if(item.fullPath === currentFullPath){
            hasRoute = true;
            if(selfScrollPage.includes(route.name)){
              document.querySelector("#self-scroll").scrollTo(0,item.savedPosition);
            }else{
              document.querySelector("#scrollbar .el-scrollbar__wrap").scrollTo(0,item.savedPosition);
            }
          }
        });
        if(!hasRoute){
          if(selfScrollPage.includes(route.name)){
            document.querySelector("#self-scroll").scrollTo(0,0);
          }else{
            document.querySelector("#scrollbar .el-scrollbar__wrap").scrollTo(0,0);
          }
        }
      }
    }
  }
  // 在组件挂载到页面后执行
  onMounted(scrollToTop);
  // keep-alive启用时，页面被激活时使用
  onActivated(scrollToPosition);
}
export default useScrollPosition;
```
该文件也是增加了所使用滚动条的逻辑判断。以上是滚动条相关的逻辑修改所涉及到的页面。

25. 新建`src/hooks/affixPosition.ts`文件：
```
import { ref } from 'vue'
const useAffixPosition = () => {
  const offsetHeight = ref(0);
  const affixTop = () => {
    offsetHeight.value = parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-header-height')) + parseInt(getComputedStyle(document.querySelector('.vue-kevin-admin')).getPropertyValue('--el-tag-height')) + 20;
  }
  return {
    offsetHeight,
    affixTop
  }
}
export default useAffixPosition;
```
这里主要封装了固钉位置在顶部的固钉功能。

26. `src/views/FrontEndLibrary/ElementPlus/Data/Article/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpArticle'
}
</script>
<script setup lang="ts">
import { ref,reactive,onMounted } from 'vue'
import {getArticleInfo} from "@/api/elementPlus";
import useScrollPosition from '@/hooks/scrollPosition';
import useAffixPosition from '@/hooks/affixPosition';
import { useRoute } from "vue-router";
import { useStore } from 'vuex'
const store = useStore();
const route = useRoute();

// 滚动行为
useScrollPosition(store,route);

let {offsetHeight,affixTop} = useAffixPosition();

let initData:any = reactive({
  data:[],
});
const getInitData = () => {
  getArticleInfo(null,{config:{showLoading:false,mockEnable:true}}).then((res:any)=>{
    console.log(res);
    if(res.data&&res.data.length>0){
        initData.data=[];
        initData.data.push(...res.data);
        console.log(initData)
    }
  });
}
getInitData();

const activeTab = ref('detail')

const handleTabClick = (tab, event) => {
  console.log(tab, event)
}

const activeCollapse = ref('1');

onMounted(()=>{
  affixTop();
})

const activities = [
  {
    content: 'Custom icon',
    timestamp: '2018-04-12 20:46',
    size: 'large',
    type: 'primary',
    icon: 'MoreFilled',
  },
  {
    content: 'Custom color',
    timestamp: '2018-04-03 20:46',
    color: '#0bbd87',
  },
  {
    content: 'Custom size',
    timestamp: '2018-04-03 20:46',
    size: 'large',
  },
  {
    content: 'Custom hollow',
    timestamp: '2018-04-03 20:46',
    type: 'primary',
    hollow: true,
  },
  {
    content: 'Default node',
    timestamp: '2018-04-03 20:46',
  },
]
</script>
<template>
<el-space :size="20" fill direction="vertical">
  <el-card shadow="never" class="no-border no-radius">
    <el-affix :offset="offsetHeight"><el-page-header icon="ArrowLeft" content="详情页" /></el-affix>
    <el-alert title="详情页展示，本页面主要使用element-plus的页头组件、标签页组件、走马灯、折叠面板、描述列表、时间线组件。" type="success" :closable="false" show-icon />
    <el-carousel :interval="4000" type="card">
      <el-carousel-item v-for="item in initData.data" :key="item">
        <el-image :src="'https://cdn.jsdelivr.net/gh/kaivin/images/list/'+item.type+'/'+item.img+'.jpg'" fit="cover" lazy>
          <template #placeholder>
            <div class="image-slot">
              <div class="slot-content">
                <svg-icon icon-class="ep:picture"></svg-icon>
                <p>{{$t('settings.settingLoadingText')}}</p>
              </div>
            </div>
          </template>
          <template #error>
            <div class="image-slot">
              <div class="slot-content">
                <svg-icon icon-class="ii:icon-image-failed"></svg-icon>
                <p>{{$t('settings.settingLoadingFailed')}}</p>
              </div>
            </div>
          </template>
        </el-image>
      </el-carousel-item>
    </el-carousel>
    <el-tabs v-model="activeTab" class="demo-tabs" @tab-click="handleTabClick">
      <el-tab-pane label="详情" name="detail">
        <h3>详情数据信息展示标签页</h3>
        <el-collapse v-model="activeCollapse" accordion>
          <el-collapse-item name="1">
            <template #title>
              <svg-icon icon-class="ep:operation"></svg-icon><strong>推销途径</strong>
            </template>
            <div>
              Consistent with real life: in line with the process and logic of real
              life, and comply with languages and habits that the users are used to;
            </div>
            <div>
              Consistent within interface: all elements should be consistent, such
              as: design style, icons and texts, position of elements, etc.
            </div>
          </el-collapse-item>
          <el-collapse-item name="2">
            <template #title>
              <svg-icon icon-class="ep:user"></svg-icon><strong>客户反馈</strong>
            </template>
            <div>
              Operation feedback: enable the users to clearly perceive their
              operations by style updates and interactive effects;
            </div>
            <div>
              Visual feedback: reflect current state by updating or rearranging
              elements of the page.
            </div>
          </el-collapse-item>
          <el-collapse-item name="3">
            <template #title>
              <svg-icon icon-class="ep:star"></svg-icon><strong>有效评论</strong>
            </template>
            <div>
              Simplify the process: keep operating process simple and intuitive;
            </div>
            <div>
              Definite and clear: enunciate your intentions clearly so that the
              users can quickly understand and make decisions;
            </div>
            <div>
              Easy to identify: the interface should be straightforward, which helps
              the users to identify and frees them from memorizing and recalling.
            </div>
          </el-collapse-item>
          <el-collapse-item name="4">
            <template #title>
              <svg-icon icon-class="ep:tools"></svg-icon><strong>技术理论</strong>
            </template>
            <div>
              Decision making: giving advices about operations is acceptable, but do
              not make decisions for the users;
            </div>
            <div>
              Controlled consequences: users should be granted the freedom to
              operate, including canceling, aborting or terminating current
              operation.
            </div>
          </el-collapse-item>
        </el-collapse>
        <p>在手风琴组件结尾加一段结束性文案</p>
      </el-tab-pane>
      <el-tab-pane label="参数" name="params">
        <el-descriptions
          title="产品参数"
          :column="3"
          border >
          <template #extra>
            <el-button type="primary">编辑</el-button>
          </template>
          <el-descriptions-item>
            <template #label>
              <div class="cell-item">
                <svg-icon icon-class="ep:user"></svg-icon>
                产品名
              </div>
            </template>
            kooriookami
          </el-descriptions-item>
          <el-descriptions-item>
            <template #label>
              <div class="cell-item">
                <svg-icon icon-class="ep:rank"></svg-icon>
                产品编码
              </div>
            </template>
            18100000000
          </el-descriptions-item>
          <el-descriptions-item>
            <template #label>
              <div class="cell-item">
                <svg-icon icon-class="ep:clock"></svg-icon>
                生产日期
              </div>
            </template>
            2000-08-08 16:34:32
          </el-descriptions-item>
          <el-descriptions-item>
            <template #label>
              <div class="cell-item">
                <svg-icon icon-class="ep:picture"></svg-icon>
                标签
              </div>
            </template>
            <el-tag>酒精</el-tag>
          </el-descriptions-item>
          <el-descriptions-item>
            <template #label>
              <div class="cell-item">
                <svg-icon icon-class="ep:more-filled"></svg-icon>
                产地
              </div>
            </template>
            No.1188, Wuzhong Avenue, Wuzhong District, Suzhou, Jiangsu Province
          </el-descriptions-item>
        </el-descriptions>
      </el-tab-pane>
      <el-tab-pane label="货源追踪" name="track">
        <div style="padding:20px 20px 0">
          <el-timeline>
            <el-timeline-item
              v-for="(activity, index) in activities"
              :key="index"
              :icon="activity.icon"
              :type="activity.type"
              :color="activity.color"
              :size="activity.size"
              :hollow="activity.hollow"
              :timestamp="activity.timestamp"
              placement="top" >
              <el-card>{{ activity.content }}</el-card>
            </el-timeline-item>
          </el-timeline>
        </div>
      </el-tab-pane>
    </el-tabs>
  </el-card>
</el-space>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.no-border{
  border:none;
}
.no-radius{
  border-radius: 0;
}
.el-alert{
  margin: 20px 0;
}
.el-card{
  :deep(.el-image){
    display: block;
    width: 100%;
    height:100%;
  }
  .el-collapse{
    :deep(.el-icon){
      color: var(--el-text-color-regular);
      margin-right: 5px;
      font-size: var(--el-font-size-base);
    }
  }
}
.cell-item {
  display: flex;
  align-items: center;
  .el-icon{
    margin-right: 5px;
  }
}
.el-affix{
  :deep(.el-affix--fixed){
    min-width: calc(100% - var(--el-sidebar-width) - 80px);
  }
}
</style>
```
该页面只是集合使用了`element-plus`的一些组件，无其他特殊逻辑处理。

27. `mock/elementPlus.ts`文件：
```
import { MockMethod } from "vite-plugin-mock";
import { Random } from 'mockjs'

const paginationListMock = (pageSize) => {
  let result:any[] = [];
  for(let i = 0; i< pageSize; i++){
    result.push({
      id:Random.id(),
      title:'@ctitle(5, 12)',
      description:'@csentence()',
      date: Random.datetime(),
      "percentage|10-100": 100,
      'type|1':['landscape','animal','food','beauty','car','cartoon','game','other',],
      'img|1-115': 115,
      color:'@color()',
      author:'@cname()'
    })
  }
  return result;
};

const infiniteScrollListMock = (pageSize,type) => {
  let result:any[] = [];
  for(let i = 0; i< pageSize; i++){
    result.push({
      id:Random.id(),
      title:'@ctitle(5, 12)',
      description:'@csentence()',
      date: Random.datetime(),
      'type|1':type.length>0?type:['landscape','animal','food','beauty','car','cartoon','game','other',],
      'img|1-115': 115,
      author:'@cname()'
    })
  }
  return result;
};
const articleMock = () => {
  let result:any[] = [];
  for(let i = 0; i< 6; i++){
    result.push({
      id:Random.id(),
      title:'@ctitle(5, 12)',
      description:'@csentence()',
      date: Random.datetime(),
      'type|1':['beauty','cartoon','game'],
      'img|1-115': 115,
    })
  }
  return result;
}
export default [
  {
    url: "/getPaginationListData",
    method: "post",
    response: (data) => {
      return {
        code: 200,
        status:true,
        info:"列表数据获取成功！",
        data: paginationListMock(data.body.pageSize),
        total:400,
      };
    }
  },
  {
    url: "/getInfiniteScrollListData",
    method: "post",
    response: (data) => {
      return {
        code: 200,
        status:true,
        info:"列表数据获取成功！",
        data: infiniteScrollListMock(data.body.pageSize,data.body.type),
        total:400,
      };
    }
  },
  {
    url: "/getArticleData",
    method: "post",
    response: (data) => {
      return {
        code: 200,
        status:true,
        info:"详情数据获取成功！",
        data: articleMock(),
      };
    }
  }
] as MockMethod[];
```
该文件主要为以上的分页加载，滚动加载，详情页所写的接口，该文件另外展示了`mock`生成随机数据的用法。

28. `src/api/elementPlus.ts`文件：
```
import { http } from "../utils/http";

// 获取分页列表数据
export const getPaginationList = (data,config) => {
  return http.request("post", "/getPaginationListData",data,config);
};

// 获取分页列表数据
export const getInfiniteScrollList = (data,config) => {
  return http.request("post", "/getInfiniteScrollListData",data,config);
};

// 获取详情页数据
export const getArticleInfo = (data,config) => {
  return http.request("post", "/getArticleData",data,config);
};
```
封装前端请求接口文件。

29. `src/views/FrontEndLibrary/ElementPlus/Data/Calendar/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpCalendar'
}
</script>
<script setup lang="ts">
import { ref,watch } from 'vue';
import { useI18n } from "vue-i18n";
import calendar from "@/plugins/calendar";
import { isToday } from "@/utils/is";
import { formatDate } from "@/utils/dateFormat";
import { useRouter,useRoute } from "vue-router";
import useRefreshPage from '@/hooks/refreshPage';
import dayjs from "dayjs";
import 'dayjs/locale/zh-cn';
const route = useRoute();
const router = useRouter();
const { refreshPage } = useRefreshPage(route,router);
const { locale } = useI18n();
watch(
  () => locale.value,
  () => {
    if(locale.value === 'zh'){
      dayjs().locale('zh-cn').format(); 
    }else{
      dayjs().locale('en').format(); 
    }
    refreshPage();
  }
);
type lunarDataType = {
  Animal: string;
  IDayCn: string;
  IMonthCn: string;
  Term: null|string;
  cAstro: string;
  astro: string;
  cDay: number;
  cMonth: number;
  cYear: number;
  date: string;
  festival: null|string;
  gzDay: string;
  gzMonth: string;
  gzYear: string;
  isLeap: boolean;
  isTerm: boolean;
  isToday: boolean;
  lDay: number;
  lMonth: number;
  lYear: number;
  lunarDate: string;
  lunarFestival: null|string;
  nWeek: number;
  ncWeek: string;
}
let selectedDate = ref(new Date())
let currentMonth = ref(new Date());
// 日历添加农历内容
const calendarData = (data) => {
  let year = parseInt(data.day.split("-")[0]);
  let month = parseInt(data.day.split("-")[1]);
  let day = parseInt(data.day.split("-")[2]);
  return calendar.solar2lunar(year,month,day) as lunarDataType;
}
// 日历标题添加农历内容
const calendarTitleData = (data) => {
  let dateStr = dayjs(data).format('YYYY-MM-DD');
  let year = parseInt(dateStr.split("-")[0]);
  let month = parseInt(dateStr.split("-")[1]);
  let day = parseInt(dateStr.split("-")[2]);
  return calendar.solar2lunar(year,month,day) as lunarDataType;
}
// 月选择改变触发事件
const monthChangeHandler = () => {
  selectedDate.value = currentMonth.value;
}
// 返回今天点击事件
const backToday = () => {
  currentMonth.value = selectedDate.value = new Date();
}
// 日历点击事件
const calendarClickHandler = (data) => {
  currentMonth.value = data.date;
}
// 上一年
const prevYear = () => {
  currentMonth.value = selectedDate.value = new Date(dayjs(dayjs(new Date(selectedDate.value)).startOf('month').subtract(1,'year')).format('YYYY-MM-DD'))
}
// 上一月
const prevMonth = () => {
  currentMonth.value = selectedDate.value = new Date(dayjs(dayjs(new Date(selectedDate.value)).startOf('month').subtract(1,'month')).format('YYYY-MM-DD'))
}
// 下一月
const nextMonth = () => {
  currentMonth.value = selectedDate.value = new Date(dayjs(dayjs(new Date(selectedDate.value)).startOf('month').add(1,'month')).format('YYYY-MM-DD'));
}
// 下一年
const nextYear = () => {
  currentMonth.value = selectedDate.value = new Date(dayjs(dayjs(new Date(selectedDate.value)).startOf('month').add(1,'year')).format('YYYY-MM-DD'));
}
</script>
<template>
  <el-space :size="20" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <el-alert title="日历页面主要使用日历组件，扩展农历、闰月、星座、生肖、天干地支对照日期、公农历节日、二十四节气、周末展示功能，中英文日历格式切换功能" type="success" :closable="false" show-icon />
      <el-calendar v-model="selectedDate">
        <template #header="{ date }">
          <span>{{ date }} <span v-if="locale==='zh'">{{calendarTitleData(selectedDate).gzYear}}{{calendarTitleData(selectedDate).Animal}}年 {{calendarTitleData(selectedDate).gzMonth}}月</span></span>
          <el-form :inline="true">
            <el-form-item>
              <el-button @click="prevYear">{{$t('buttons.buttonPrevYear')}}</el-button>
              <el-button @click="prevMonth">{{$t('buttons.buttonPrevMonth')}}</el-button>
              <el-date-picker @change="monthChangeHandler" :default-value="new Date()" :clearable="false" :editable="false" v-model="currentMonth" type="month" />
              <el-button @click="nextMonth">{{$t('buttons.buttonNextMonth')}}</el-button>
              <el-button @click="nextYear">{{$t('buttons.buttonNextYear')}}</el-button>
            </el-form-item>
            <el-form-item><el-button @click="backToday" :disabled="isToday(formatDate(selectedDate))">{{$t('buttons.buttonBackToday')}}</el-button></el-form-item>
          </el-form>
        </template>
        <template #dateCell="{ data }">
          <div :class="['item-calendar',data.isSelected ? 'is-selected' : '',calendarData(data).nWeek===6||calendarData(data).nWeek===7?'is-weekend':'']" @click="calendarClickHandler(data)">
            <p>
              <strong>{{ calendarData(data).cDay }}<i v-if="calendarData(data).nWeek===6||calendarData(data).nWeek===7">{{$t('text.textVacation')}}</i></strong>
              <span v-if="locale==='zh'" :class="calendarData(data).Term?'is-primary':''">{{calendarData(data).Term?calendarData(data).Term:calendarData(data).gzDay}}</span>
            </p>
            <p>
              <strong v-if="locale==='zh'" :class="[calendarData(data).lunarFestival?'is-success':'',calendarData(data).festival?'is-warning':'']"><i v-if="calendarData(data).isLeap">{{calendarData(data).IMonthCn}}</i>{{calendarData(data).lunarFestival?calendarData(data).lunarFestival:calendarData(data).festival?calendarData(data).festival:calendarData(data).IDayCn}}</strong>
              <span>{{locale==='zh'?calendarData(data).cAstro:calendarData(data).astro}}</span>
            </p>
          </div>
        </template>
      </el-calendar>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">

.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.no-border{
  border:none;
}
.no-radius{
  border-radius: 0;
}
.el-form{
  .el-form-item{
    .el-button,.el-input{
      margin:0 0 0 -1px;
      border-radius:0;
      &:hover{
        z-index: 1;
      }
    }
    .el-button{
      &:first-child{
        border-top-left-radius: var(--el-border-radius-base);
        border-bottom-left-radius: var(--el-border-radius-base);
      }
      &:last-child{
        border-top-right-radius: var(--el-border-radius-base);
        border-bottom-right-radius: var(--el-border-radius-base);
      }
    }
    :deep(.el-form-item__content){
      >.el-input{
        margin:0 0 0 -1px;
        border-radius: 0;
        .el-input__inner{
          border-radius: 0;
        }
      }
    }
  }
  .el-form-item+.el-form-item{
    margin-left: 12px;
    .el-button{
      border-radius: var(--el-border-radius-base);
    }
  }
}
.el-calendar{
  :deep(.el-calendar__header){
    padding: 12px 0;
    margin: 0 20px;
    align-items: center;
    >span{
      align-items: center;
    }
    .el-form{
      &.el-form--inline{
        .el-input{
          width: 100px;
        }
      }
    }
  }
  :deep(.el-calendar__body){
    padding: 0 20px 20px;
  }
  :deep(.el-calendar-table){
    .el-calendar-day{
      display: flex;
    }
  }
}
.prev,.next{
  .item-calendar{
    &.is-weekend{
      color: var(--el-text-color-placeholder);
    }
  }
}
.item-calendar{
  margin: -8px;
  padding:8px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: 1;
  &.is-weekend{
    color: var(--el-color-success);
  }
  p{
    margin:0;
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex: 1 1 auto;
    strong{
      i{
        font-style: normal;
        margin-left: 5px;
        font-weight: normal;
      }
    }
    span{
      font-size: var(--el-font-size-extra-small);
      &.is-primary{
        color: var(--el-color-primary);
      }
    }
  }
  p+p{
    strong{
      font-size: var(--el-font-size-extra-small);
      i{
        font-style: normal;
        margin-left:0;
        margin-right: 5px;
      }
      &.is-warning{
        color: var(--el-color-warning);
      }
      &.is-success{
        color: var(--el-color-success);
      }
    }
  }
}
</style>
```
该页面主要在`element-plus`的日历组件的基础上，封装了中国农历的日历展示，星座展示，并支持国际化，因为在切换国际化时，国外周日为第一列，国内周一为第一列，需要重新渲染日历，所以这里将标签栏中的刷新页面功能做了封装，并在这里进行了引用。当前日历只是简单封装了农历相关的展示，还可以进一步扩展，添加其他功能，如日程，节假日等功能。

30. 新建`src/plugins/calendar/index.ts`文件：
```
/**
 * @1900-2100区间内的公历、农历互转
 * @charset UTF-8
 * @Author  Jea杨(JJonline@JJonline.Cn)
 * @Time    2014-7-21
 * @Time    2016-8-13 Fixed 2033hex、Attribution Annals
 * @Time    2016-9-25 Fixed lunar LeapMonth Param Bug
 * @Time    2017-7-24 Fixed use getTerm Func Param Error.use solar year,NOT lunar year
 * @Version 1.0.3
 * @公历转农历：calendar.solar2lunar(1987,11,01); //[you can ignore params of prefix 0]
 * @农历转公历：calendar.lunar2solar(1987,09,10); //[you can ignore params of prefix 0]
 */
 const calendar = {

  /**
   * 农历1900-2100的润大小信息表
   * @Array Of Property
   * @return Hex
   */
  lunarInfo: [0x04bd8, 0x04ae0, 0x0a570, 0x054d5, 0x0d260, 0x0d950, 0x16554, 0x056a0, 0x09ad0, 0x055d2,//1900-1909
      0x04ae0, 0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 0x0d6a0, 0x0ada2, 0x095b0, 0x14977,//1910-1919
      0x04970, 0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54, 0x02b60, 0x09570, 0x052f2, 0x04970,//1920-1929
      0x06566, 0x0d4a0, 0x0ea50, 0x16a95, 0x05ad0, 0x02b60, 0x186e3, 0x092e0, 0x1c8d7, 0x0c950,//1930-1939
      0x0d4a0, 0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4, 0x025d0, 0x092d0, 0x0d2b2, 0x0a950, 0x0b557,//1940-1949
      0x06ca0, 0x0b550, 0x15355, 0x04da0, 0x0a5b0, 0x14573, 0x052b0, 0x0a9a8, 0x0e950, 0x06aa0,//1950-1959
      0x0aea6, 0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260, 0x0f263, 0x0d950, 0x05b57, 0x056a0,//1960-1969
      0x096d0, 0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b6a0, 0x195a6,//1970-1979
      0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0af46, 0x0ab60, 0x09570,//1980-1989
      0x04af5, 0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58, 0x05ac0, 0x0ab60, 0x096d5, 0x092e0,//1990-1999
      0x0c960, 0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5,//2000-2009
      0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0, 0x0a5b0, 0x15176, 0x052b0, 0x0a930,//2010-2019
      0x07954, 0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260, 0x0ea65, 0x0d530,//2020-2029
      0x05aa0, 0x076a3, 0x096d0, 0x04afb, 0x04ad0, 0x0a4d0, 0x1d0b6, 0x0d250, 0x0d520, 0x0dd45,//2030-2039
      0x0b5a0, 0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0,//2040-2049
      /**Add By JJonline@JJonline.Cn**/
      0x14b63, 0x09370, 0x049f8, 0x04970, 0x064b0, 0x168a6, 0x0ea50, 0x06b20, 0x1a6c4, 0x0aae0,//2050-2059
      0x092e0, 0x0d2e3, 0x0c960, 0x0d557, 0x0d4a0, 0x0da50, 0x05d55, 0x056a0, 0x0a6d0, 0x055d4,//2060-2069
      0x052d0, 0x0a9b8, 0x0a950, 0x0b4a0, 0x0b6a6, 0x0ad50, 0x055a0, 0x0aba4, 0x0a5b0, 0x052b0,//2070-2079
      0x0b273, 0x06930, 0x07337, 0x06aa0, 0x0ad50, 0x14b55, 0x04b60, 0x0a570, 0x054e4, 0x0d160,//2080-2089
      0x0e968, 0x0d520, 0x0daa0, 0x16aa6, 0x056d0, 0x04ae0, 0x0a9d4, 0x0a2d0, 0x0d150, 0x0f252,//2090-2099
      0x0d520],//2100

  /**
   * 公历每个月份的天数普通表
   * @Array Of Property
   * @return Number
   */
  solarMonth: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],

  /**
   * 天干地支之天干速查表
   * @Array Of Property trans["甲","乙","丙","丁","戊","己","庚","辛","壬","癸"]
   * @return Cn string
   */
  Gan: ["\u7532", "\u4e59", "\u4e19", "\u4e01", "\u620a", "\u5df1", "\u5e9a", "\u8f9b", "\u58ec", "\u7678"],

  /**
   * 天干地支之地支速查表
   * @Array Of Property
   * @trans["子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"]
   * @return Cn string
   */
  Zhi: ["\u5b50", "\u4e11", "\u5bc5", "\u536f", "\u8fb0", "\u5df3", "\u5348", "\u672a", "\u7533", "\u9149", "\u620c", "\u4ea5"],

  /**
   * 天干地支之地支速查表<=>生肖
   * @Array Of Property
   * @trans["鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪"]
   * @return Cn string
   */
  Animals: ["\u9f20", "\u725b", "\u864e", "\u5154", "\u9f99", "\u86c7", "\u9a6c", "\u7f8a", "\u7334", "\u9e21", "\u72d7", "\u732a"],

  /**
   * 阳历节日
   */
  festival: {
      '1-1': {title: '元旦节'},
      '2-14': {title: '情人节'},
      '5-1': {title: '劳动节'},
      '5-4': {title: '青年节'},
      '6-1': {title: '儿童节'},
      '9-10': {title: '教师节'},
      '10-1': {title: '国庆节'},
      '12-25': {title: '圣诞节'},

      '3-8': {title: '妇女节'},
      '3-12': {title: '植树节'},
      '4-1': {title: '愚人节'},
      '5-12': {title: '护士节'},
      '7-1': {title: '建党节'},
      '8-1': {title: '建军节'},
      '12-24': {title: '平安夜'},
  },

  /**
   * 农历节日
   */
  lFestival: {
      '12-30': {title: '除夕'},
      '1-1': {title: '春节'},
      '1-15': {title: '元宵节'},
      '2-2': {title: '龙抬头'},
      '5-5': {title: '端午节'},
      '7-7': {title: '七夕节'},
      '7-15': {title: '中元节'},
      '8-15': {title: '中秋节'},
      '9-9': {title: '重阳节'},
      '10-1': {title: '寒衣节'},
      '10-15': {title: '下元节'},
      '12-8': {title: '腊八节'},
      '12-23': {title: '北方小年'},
      '12-24': {title: '南方小年'},
  },

  /**
   * 返回默认定义的阳历节日
   */
  getFestival() {
      return this.festival
  },

  /**
   * 返回默认定义的内容里节日
   */
  getLunarFestival() {
      return this.lFestival
  },

  /**
   *
   * @param param {Object} 按照festival的格式输入数据，设置阳历节日
   */
  setFestival(param = {}) {
      this.festival = param
  },

  /**
   *
   * @param param {Object} 按照lFestival的格式输入数据，设置农历节日
   */
  setLunarFestival(param = {}) {
      this.lFestival = param
  },

  /**
   * 24节气速查表
   * @Array Of Property
   * @trans["小寒","大寒","立春","雨水","惊蛰","春分","清明","谷雨","立夏","小满","芒种","夏至","小暑","大暑","立秋","处暑","白露","秋分","寒露","霜降","立冬","小雪","大雪","冬至"]
   * @return Cn string
   */
  solarTerm: ["\u5c0f\u5bd2", "\u5927\u5bd2", "\u7acb\u6625", "\u96e8\u6c34", "\u60ca\u86f0", "\u6625\u5206", "\u6e05\u660e", "\u8c37\u96e8", "\u7acb\u590f", "\u5c0f\u6ee1", "\u8292\u79cd", "\u590f\u81f3", "\u5c0f\u6691", "\u5927\u6691", "\u7acb\u79cb", "\u5904\u6691", "\u767d\u9732", "\u79cb\u5206", "\u5bd2\u9732", "\u971c\u964d", "\u7acb\u51ac", "\u5c0f\u96ea", "\u5927\u96ea", "\u51ac\u81f3"],

  /**
   * 1900-2100各年的24节气日期速查表
   * @Array Of Property
   * @return 0x string For splice
   */
  sTermInfo: ['9778397bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e', '97bcf97c3598082c95f8c965cc920f',
      '97bd0b06bdb0722c965ce1cfcc920f', 'b027097bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e',
      '97bcf97c359801ec95f8c965cc920f', '97bd0b06bdb0722c965ce1cfcc920f', 'b027097bd097c36b0b6fc9274c91aa',
      '97b6b97bd19801ec9210c965cc920e', '97bcf97c359801ec95f8c965cc920f', '97bd0b06bdb0722c965ce1cfcc920f',
      'b027097bd097c36b0b6fc9274c91aa', '9778397bd19801ec9210c965cc920e', '97b6b97bd19801ec95f8c965cc920f',
      '97bd09801d98082c95f8e1cfcc920f', '97bd097bd097c36b0b6fc9210c8dc2', '9778397bd197c36c9210c9274c91aa',
      '97b6b97bd19801ec95f8c965cc920e', '97bd09801d98082c95f8e1cfcc920f', '97bd097bd097c36b0b6fc9210c8dc2',
      '9778397bd097c36c9210c9274c91aa', '97b6b97bd19801ec95f8c965cc920e', '97bcf97c3598082c95f8e1cfcc920f',
      '97bd097bd097c36b0b6fc9210c8dc2', '9778397bd097c36c9210c9274c91aa', '97b6b97bd19801ec9210c965cc920e',
      '97bcf97c3598082c95f8c965cc920f', '97bd097bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa',
      '97b6b97bd19801ec9210c965cc920e', '97bcf97c3598082c95f8c965cc920f', '97bd097bd097c35b0b6fc920fb0722',
      '9778397bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e', '97bcf97c359801ec95f8c965cc920f',
      '97bd097bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e',
      '97bcf97c359801ec95f8c965cc920f', '97bd097bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa',
      '97b6b97bd19801ec9210c965cc920e', '97bcf97c359801ec95f8c965cc920f', '97bd097bd07f595b0b6fc920fb0722',
      '9778397bd097c36b0b6fc9210c8dc2', '9778397bd19801ec9210c9274c920e', '97b6b97bd19801ec95f8c965cc920f',
      '97bd07f5307f595b0b0bc920fb0722', '7f0e397bd097c36b0b6fc9210c8dc2', '9778397bd097c36c9210c9274c920e',
      '97b6b97bd19801ec95f8c965cc920f', '97bd07f5307f595b0b0bc920fb0722', '7f0e397bd097c36b0b6fc9210c8dc2',
      '9778397bd097c36c9210c9274c91aa', '97b6b97bd19801ec9210c965cc920e', '97bd07f1487f595b0b0bc920fb0722',
      '7f0e397bd097c36b0b6fc9210c8dc2', '9778397bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e',
      '97bcf7f1487f595b0b0bb0b6fb0722', '7f0e397bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa',
      '97b6b97bd19801ec9210c965cc920e', '97bcf7f1487f595b0b0bb0b6fb0722', '7f0e397bd097c35b0b6fc920fb0722',
      '9778397bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e', '97bcf7f1487f531b0b0bb0b6fb0722',
      '7f0e397bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa', '97b6b97bd19801ec9210c965cc920e',
      '97bcf7f1487f531b0b0bb0b6fb0722', '7f0e397bd07f595b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa',
      '97b6b97bd19801ec9210c9274c920e', '97bcf7f0e47f531b0b0bb0b6fb0722', '7f0e397bd07f595b0b0bc920fb0722',
      '9778397bd097c36b0b6fc9210c91aa', '97b6b97bd197c36c9210c9274c920e', '97bcf7f0e47f531b0b0bb0b6fb0722',
      '7f0e397bd07f595b0b0bc920fb0722', '9778397bd097c36b0b6fc9210c8dc2', '9778397bd097c36c9210c9274c920e',
      '97b6b7f0e47f531b0723b0b6fb0722', '7f0e37f5307f595b0b0bc920fb0722', '7f0e397bd097c36b0b6fc9210c8dc2',
      '9778397bd097c36b0b70c9274c91aa', '97b6b7f0e47f531b0723b0b6fb0721', '7f0e37f1487f595b0b0bb0b6fb0722',
      '7f0e397bd097c35b0b6fc9210c8dc2', '9778397bd097c36b0b6fc9274c91aa', '97b6b7f0e47f531b0723b0b6fb0721',
      '7f0e27f1487f595b0b0bb0b6fb0722', '7f0e397bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa',
      '97b6b7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e397bd097c35b0b6fc920fb0722',
      '9778397bd097c36b0b6fc9274c91aa', '97b6b7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722',
      '7f0e397bd097c35b0b6fc920fb0722', '9778397bd097c36b0b6fc9274c91aa', '97b6b7f0e47f531b0723b0b6fb0721',
      '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e397bd07f595b0b0bc920fb0722', '9778397bd097c36b0b6fc9274c91aa',
      '97b6b7f0e47f531b0723b0787b0721', '7f0e27f0e47f531b0b0bb0b6fb0722', '7f0e397bd07f595b0b0bc920fb0722',
      '9778397bd097c36b0b6fc9210c91aa', '97b6b7f0e47f149b0723b0787b0721', '7f0e27f0e47f531b0723b0b6fb0722',
      '7f0e397bd07f595b0b0bc920fb0722', '9778397bd097c36b0b6fc9210c8dc2', '977837f0e37f149b0723b0787b0721',
      '7f07e7f0e47f531b0723b0b6fb0722', '7f0e37f5307f595b0b0bc920fb0722', '7f0e397bd097c35b0b6fc9210c8dc2',
      '977837f0e37f14998082b0787b0721', '7f07e7f0e47f531b0723b0b6fb0721', '7f0e37f1487f595b0b0bb0b6fb0722',
      '7f0e397bd097c35b0b6fc9210c8dc2', '977837f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721',
      '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e397bd097c35b0b6fc920fb0722', '977837f0e37f14998082b0787b06bd',
      '7f07e7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e397bd097c35b0b6fc920fb0722',
      '977837f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722',
      '7f0e397bd07f595b0b0bc920fb0722', '977837f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721',
      '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e397bd07f595b0b0bc920fb0722', '977837f0e37f14998082b0787b06bd',
      '7f07e7f0e47f149b0723b0787b0721', '7f0e27f0e47f531b0b0bb0b6fb0722', '7f0e397bd07f595b0b0bc920fb0722',
      '977837f0e37f14998082b0723b06bd', '7f07e7f0e37f149b0723b0787b0721', '7f0e27f0e47f531b0723b0b6fb0722',
      '7f0e397bd07f595b0b0bc920fb0722', '977837f0e37f14898082b0723b02d5', '7ec967f0e37f14998082b0787b0721',
      '7f07e7f0e47f531b0723b0b6fb0722', '7f0e37f1487f595b0b0bb0b6fb0722', '7f0e37f0e37f14898082b0723b02d5',
      '7ec967f0e37f14998082b0787b0721', '7f07e7f0e47f531b0723b0b6fb0722', '7f0e37f1487f531b0b0bb0b6fb0722',
      '7f0e37f0e37f14898082b0723b02d5', '7ec967f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721',
      '7f0e37f1487f531b0b0bb0b6fb0722', '7f0e37f0e37f14898082b072297c35', '7ec967f0e37f14998082b0787b06bd',
      '7f07e7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e37f0e37f14898082b072297c35',
      '7ec967f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722',
      '7f0e37f0e366aa89801eb072297c35', '7ec967f0e37f14998082b0787b06bd', '7f07e7f0e47f149b0723b0787b0721',
      '7f0e27f1487f531b0b0bb0b6fb0722', '7f0e37f0e366aa89801eb072297c35', '7ec967f0e37f14998082b0723b06bd',
      '7f07e7f0e47f149b0723b0787b0721', '7f0e27f0e47f531b0723b0b6fb0722', '7f0e37f0e366aa89801eb072297c35',
      '7ec967f0e37f14998082b0723b06bd', '7f07e7f0e37f14998083b0787b0721', '7f0e27f0e47f531b0723b0b6fb0722',
      '7f0e37f0e366aa89801eb072297c35', '7ec967f0e37f14898082b0723b02d5', '7f07e7f0e37f14998082b0787b0721',
      '7f07e7f0e47f531b0723b0b6fb0722', '7f0e36665b66aa89801e9808297c35', '665f67f0e37f14898082b0723b02d5',
      '7ec967f0e37f14998082b0787b0721', '7f07e7f0e47f531b0723b0b6fb0722', '7f0e36665b66a449801e9808297c35',
      '665f67f0e37f14898082b0723b02d5', '7ec967f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721',
      '7f0e36665b66a449801e9808297c35', '665f67f0e37f14898082b072297c35', '7ec967f0e37f14998082b0787b06bd',
      '7f07e7f0e47f531b0723b0b6fb0721', '7f0e26665b66a449801e9808297c35', '665f67f0e37f1489801eb072297c35',
      '7ec967f0e37f14998082b0787b06bd', '7f07e7f0e47f531b0723b0b6fb0721', '7f0e27f1487f531b0b0bb0b6fb0722'],

  /**
   * 数字转中文速查表
   * @Array Of Property
   * @trans ['日','一','二','三','四','五','六','七','八','九','十']
   * @return Cn string
   */
  nStr1: ["\u65e5", "\u4e00", "\u4e8c", "\u4e09", "\u56db", "\u4e94", "\u516d", "\u4e03", "\u516b", "\u4e5d", "\u5341"],

  /**
   * 日期转农历称呼速查表
   * @Array Of Property
   * @trans ['初','十','廿','卅']
   * @return Cn string
   */
  nStr2: ["\u521d", "\u5341", "\u5eff", "\u5345"],

  /**
   * 月份转农历称呼速查表
   * @Array Of Property
   * @trans ['正','一','二','三','四','五','六','七','八','九','十','冬','腊']
   * @return Cn string
   */
  nStr3: ["\u6b63", "\u4e8c", "\u4e09", "\u56db", "\u4e94", "\u516d", "\u4e03", "\u516b", "\u4e5d", "\u5341", "\u51ac", "\u814a"],

  /**
   * 返回农历y年一整年的总天数
   * @param y lunar Year
   * @return Number
   * @eg:var count = calendar.lYearDays(1987) ;//count=387
   */
  lYearDays: function (y) {
      let i, sum = 348;
      for (i = 0x8000; i > 0x8; i >>= 1) {
          sum += (this.lunarInfo[y - 1900] & i) ? 1 : 0;
      }
      return (sum + this.leapDays(y));
  },

  /**
   * 返回农历y年闰月是哪个月；若y年没有闰月 则返回0
   * @param y lunar Year
   * @return Number (0-12)
   * @eg:var leapMonth = calendar.leapMonth(1987) ;//leapMonth=6
   */
  leapMonth: function (y) { //闰字编码 \u95f0
      return (this.lunarInfo[y - 1900] & 0xf);
  },

  /**
   * 返回农历y年闰月的天数 若该年没有闰月则返回0
   * @param y lunar Year
   * @return Number (0、29、30)
   * @eg:var leapMonthDay = calendar.leapDays(1987) ;//leapMonthDay=29
   */
  leapDays: function (y) {
      if (this.leapMonth(y)) {
          return ((this.lunarInfo[y - 1900] & 0x10000) ? 30 : 29);
      }
      return (0);
  },

  /**
   * 返回农历y年m月（非闰月）的总天数，计算m为闰月时的天数请使用leapDays方法
   * @param y lunar Year
   * @param m lunar Month
   * @return Number (-1、29、30)
   * @eg:var MonthDay = calendar.monthDays(1987,9) ;//MonthDay=29
   */
  monthDays: function (y, m) {
      if (m > 12 || m < 1) {
          return -1
      }//月份参数从1至12，参数错误返回-1
      return ((this.lunarInfo[y - 1900] & (0x10000 >> m)) ? 30 : 29);
  },

  /**
   * 返回公历(!)y年m月的天数
   * @param y solar Year
   * @param m solar Month
   * @return Number (-1、28、29、30、31)
   * @eg:var solarMonthDay = calendar.leapDays(1987) ;//solarMonthDay=30
   */
  solarDays: function (y, m) {
      if (m > 12 || m < 1) {
          return -1
      } //若参数错误 返回-1
      const ms = m - 1;
      if (ms === 1) { //2月份的闰平规律测算后确认返回28或29
          return (((y % 4 === 0) && (y % 100 !== 0) || (y % 400 === 0)) ? 29 : 28);
      } else {
          return (this.solarMonth[ms]);
      }
  },

  /**
   * 农历年份转换为干支纪年
   * @param  lYear 农历年的年份数
   * @return Cn string
   */
  toGanZhiYear: function (lYear) {
      var ganKey = (lYear - 3) % 10;
      var zhiKey = (lYear - 3) % 12;
      if (ganKey === 0) ganKey = 10;//如果余数为0则为最后一个天干
      if (zhiKey === 0) zhiKey = 12;//如果余数为0则为最后一个地支
      return this.Gan[ganKey - 1] + this.Zhi[zhiKey - 1];

  },

  /**
   * 公历月、日判断所属星座
   * @param  cMonth [description]
   * @param  cDay [description]
   * 中文星座顺序：魔羯水瓶双鱼白羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯
   * 英文星座顺序：Capricom Aquarius Pisces Aries Taurus Gemini Cacer Leo Virgo Libra Scorpio Sagittarius Capricom
   * @return Cn string
   */
  toAstro: function (cMonth, cDay) {
      const s = "\u9b54\u7faf\u6c34\u74f6\u53cc\u9c7c\u767d\u7f8a\u91d1\u725b\u53cc\u5b50\u5de8\u87f9\u72ee\u5b50\u5904\u5973\u5929\u79e4\u5929\u874e\u5c04\u624b\u9b54\u7faf";
      const arr = [20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22];
      return s.substr(cMonth * 2 - (cDay < arr[cMonth - 1] ? 2 : 0), 2) + "\u5ea7";//座
  },

  /**
   * 公历月、日判断所属星座
   * @param  cAstro [description]
   * 中文星座顺序：魔羯水瓶双鱼白羊金牛双子巨蟹狮子处女天秤天蝎射手
   * 英文星座顺序：Capricom Aquarius Pisces Aries Taurus Gemini Cacer Leo Virgo Libra Scorpio Sagittarius Capricom
   * @return En string
   */
  toEnAstro: function (cAstro) {
    let s;
    switch (cAstro) {
        case '\u9b54\u7faf\u5ea7': // 摩羯
            s = 'Capricorn';
            break;
        case '\u6c34\u74f6\u5ea7': // 水瓶
            s = 'Aquarius';
            break;
        case '\u53cc\u9c7c\u5ea7': // 双鱼
            s = 'Pisces';
            break;
        case '\u767d\u7f8a\u5ea7': // 白羊
            s = 'Aries';
            break;
        case '\u91d1\u725b\u5ea7': // 金牛
            s = 'Taurus';
            break;
        case '\u53cc\u5b50\u5ea7': // 双子
            s = 'Gemini';
            break;
        case '\u5de8\u87f9\u5ea7': // 巨蟹
            s = 'Cancer';
            break;
        case '\u72ee\u5b50\u5ea7': // 狮子
            s = 'Leo';
            break;
        case '\u5904\u5973\u5ea7': // 处女
            s = 'Virgo';
            break;
        case '\u5929\u79e4\u5ea7': // 天秤
            s = 'Libra';
            break;
        case '\u5929\u874e\u5ea7': // 天蝎
            s = 'Scorpio';
            break;
        default : // 射手
            s = 'Sagittarius';
    }
    return (s);

  },
  /**
   * 传入offset偏移量返回干支
   * @param offset 相对甲子的偏移量
   * @return Cn string
   */
  toGanZhi: function (offset) {
      return this.Gan[offset % 10] + this.Zhi[offset % 12];
  },

  /**
   * 传入公历(!)y年获得该年第n个节气的公历日期
   * @param y y公历年(1900-2100)
   * @param n n二十四节气中的第几个节气(1~24)；从n=1(小寒)算起
   * @return day Number
   * @eg:var _24 = calendar.getTerm(1987,3) ;//_24=4;意即1987年2月4日立春
   */
  getTerm: function (y, n) {
      if( y < 1900 || y > 2100 || n < 1 || n > 24) {
          return -1;
      }
      const _table = this.sTermInfo[y - 1900];
      const _calcDay = []
      for (let index = 0; index < _table.length; index += 5) {
          const chunk = parseInt('0x' + _table.substr(index, 5)).toString()
          _calcDay.push(
            chunk[0],
            chunk.substr(1, 2),
            chunk[3],
            chunk.substr(4, 2)
          )
      }
      return parseInt(_calcDay[n - 1]);
  },

  /**
   * 传入农历数字月份返回汉语通俗表示法
   * @param m lunar month
   * @return Cn string
   * @eg:var cnMonth = calendar.toChinaMonth(12) ;//cnMonth='腊月'
   */
  toChinaMonth: function (m) { // 月 => \u6708
      if (m > 12 || m < 1) {
          return -1
      } //若参数错误 返回-1
      let s = this.nStr3[m - 1];
      s += "\u6708";//加上月字
      return s;
  },

  /**
   * 传入农历日期数字返回汉字表示法
   * @param d lunar day
   * @return Cn string
   * @eg:var cnDay = calendar.toChinaDay(21) ;//cnMonth='廿一'
   */
  toChinaDay: function (d) { //日 => \u65e5
      let s;
      switch (d) {
          case 10:
              s = '\u521d\u5341';
              break;
          case 20:
              s = '\u4e8c\u5341';
              break;
          case 30:
              s = '\u4e09\u5341';
              break;
          default :
              s = this.nStr2[Math.floor(d / 10)];
              s += this.nStr1[d % 10];
      }
      return (s);
  },

  /**
   * 年份转生肖[!仅能大致转换] => 精确划分生肖分界线是“立春”
   * @param y year
   * @return Cn string
   * @eg:var animal = calendar.getAnimal(1987) ;//animal='兔'
   */
  getAnimal: function (y) {
      return this.Animals[(y - 4) % 12]
  },

  /**
   * 传入阳历年月日获得详细的公历、农历object信息 <=>JSON
   * !important! 公历参数区间1900.1.31~2100.12.31
   * @param yPara  solar year
   * @param mPara  solar month
   * @param dPara  solar day
   * @return JSON object
   * @eg:console.log(calendar.solar2lunar(1987,11,01));
   */
  solar2lunar: function (yPara, mPara, dPara) {
      let y = parseInt(yPara);
      let m = parseInt(mPara);
      let d = parseInt(dPara);
      //年份限定、上限
      if (y < 1900 || y > 2100) {
          return -1;// undefined转换为数字变为NaN
      }
      //公历传参最下限
      if (y === 1900 && m === 1 && d < 31) {
          return -1;
      }

      //未传参  获得当天
      let objDate;
      if (!y) {
          objDate = new Date();
      } else {
          objDate = new Date(y, m - 1, d);
      }
      let i, leap = 0, temp = 0;
      //修正ymd参数
      y = objDate.getFullYear();
      m = objDate.getMonth() + 1;
      d = objDate.getDate();
      let offset = (Date.UTC(objDate.getFullYear(), objDate.getMonth(), objDate.getDate()) - Date.UTC(1900, 0, 31)) / 86400000;
      for (i = 1900; i < 2101 && offset > 0; i++) {
          temp = this.lYearDays(i);
          offset -= temp;
      }
      if (offset < 0) {
          offset += temp;
          i--;
      }

      //是否今天
      let isTodayObj = new Date(),
          isToday = false;
      if (isTodayObj.getFullYear() === y && isTodayObj.getMonth() + 1 === m && isTodayObj.getDate() === d) {
          isToday = true;
      }
      //星期几
      let nWeek = objDate.getDay(),
          cWeek = this.nStr1[nWeek];
      //数字表示周几顺应天朝周一开始的惯例
      if (nWeek === 0) {
          nWeek = 7;
      }
      //农历年
      const year = i;
      leap = this.leapMonth(i); //闰哪个月
      let isLeap = false;

      //效验闰月
      for (i = 1; i < 13 && offset > 0; i++) {
          //闰月
          if (leap > 0 && i === (leap + 1) && isLeap === false) {
              --i;
              isLeap = true;
              temp = this.leapDays(year); //计算农历闰月天数
          } else {
              temp = this.monthDays(year, i);//计算农历普通月天数
          }
          //解除闰月
          if (isLeap === true && i === (leap + 1)) {
              isLeap = false;
          }
          offset -= temp;
      }
      // 闰月导致数组下标重叠取反
      if (offset === 0 && leap > 0 && i === leap + 1) {
          if (isLeap) {
              isLeap = false;
          } else {
              isLeap = true;
              --i;
          }
      }
      if (offset < 0) {
          offset += temp;
          --i;
      }
      //农历月
      const month = i;
      //农历日
      const day = offset + 1;
      //天干地支处理
      const sm = m - 1;
      const gzY = this.toGanZhiYear(year);

      // 当月的两个节气
      // bugfix-2017-7-24 11:03:38 use lunar Year Param `y` Not `year`
      const firstNode = this.getTerm(y, (m * 2 - 1));//返回当月「节」为几日开始
      const secondNode = this.getTerm(y, (m * 2));//返回当月「节」为几日开始

      // 依据12节气修正干支月
      let gzM = this.toGanZhi((y - 1900) * 12 + m + 11);
      if (d >= firstNode) {
          gzM = this.toGanZhi((y - 1900) * 12 + m + 12);
      }

      //传入的日期的节气与否
      let isTerm = false;
      let Term = null;
      if (firstNode === d) {
          isTerm = true;
          Term = this.solarTerm[m * 2 - 2];
      }
      if (secondNode === d) {
          isTerm = true;
          Term = this.solarTerm[m * 2 - 1];
      }
      //日柱 当月一日与 1900/1/1 相差天数
      const dayCyclical = Date.UTC(y, sm, 1, 0, 0, 0, 0) / 86400000 + 25567 + 10;
      const gzD = this.toGanZhi(dayCyclical + d - 1);
      //该日期所属的星座
      const cAstro = this.toAstro(m,d);
      const astro = this.toEnAstro(cAstro);

      const solarDate = y + '-' + m + '-' + d;
      const lunarDate = year + '-' + month + '-' + day;

      const festival = this.festival;
      const lFestival = this.lFestival;

      const festivalDate = m + '-' + d;
      let lunarFestivalDate = month + '-' + day;

      // bugfix https://github.com/jjonline/calendar.js/issues/29
      // 农历节日修正：农历12月小月则29号除夕，大月则30号除夕
      // 此处取巧修正：当前为农历12月29号时增加一次判断并且把lunarFestivalDate设置为12-30以正确取得除夕
      // 天朝农历节日遇闰月过前不过后的原则，此处取农历12月天数不考虑闰月
      // 农历润12月在本工具支持的200年区间内仅1574年出现
      if (month === 12 && day === 29 && this.monthDays(year, month) === 29) {
          lunarFestivalDate = '12-30';
      }
      return {
          date: solarDate,
          lunarDate: lunarDate,
          festival: festival[festivalDate] ? festival[festivalDate].title : null,
          lunarFestival: lFestival[lunarFestivalDate] ? lFestival[lunarFestivalDate].title : null,
          'lYear': year,
          'lMonth': month,
          'lDay': day,
          'Animal': this.getAnimal(year),
          'IMonthCn': (isLeap ? "\u95f0" : '') + this.toChinaMonth(month),
          'IDayCn': this.toChinaDay(day),
          'cYear': y,
          'cMonth': m,
          'cDay': d,
          'gzYear': gzY,
          'gzMonth': gzM,
          'gzDay': gzD,
          'isToday': isToday,
          'isLeap': isLeap,
          'nWeek': nWeek,
          'ncWeek': "\u661f\u671f" + cWeek,
          'isTerm': isTerm,
          'Term': Term,
          'astro': astro,
          'cAstro':cAstro
      };
  },

  /**
   * 传入农历年月日以及传入的月份是否闰月获得详细的公历、农历object信息 <=>JSON
   * !important! 参数区间1900.1.31~2100.12.1
   * @param y  lunar year
   * @param m  lunar month
   * @param d  lunar day
   * @param isLeapMonth  lunar month is leap or not.[如果是农历闰月第四个参数赋值true即可]
   * @return JSON object
   * @eg:console.log(calendar.lunar2solar(1987,9,10));
   */
  lunar2solar: function (y, m, d, isLeapMonth) {
      y = parseInt(y)
      m = parseInt(m)
      d = parseInt(d)
      isLeapMonth = !!isLeapMonth;
      const leapOffset = 0;
      const leapMonth = this.leapMonth(y);
      const leapDay = this.leapDays(y);
      if (isLeapMonth && (leapMonth !== m)) {
          return -1;
      }//传参要求计算该闰月公历 但该年得出的闰月与传参的月份并不同
      if (y === 2100 && m === 12 && d > 1 || y === 1900 && m === 1 && d < 31) {
          return -1;
      }//超出了最大极限值
      const day = this.monthDays(y, m);
      let _day = day;
      //bugFix 2016-9-25
      //if month is leap, _day use leapDays method
      if (isLeapMonth) {
          _day = this.leapDays(y, m);
      }
      if (y < 1900 || y > 2100 || d > _day) {
          return -1;
      }//参数合法性效验

      //计算农历的时间差
      let offset = 0;
      let i;
      for (i = 1900; i < y; i++) {
          offset += this.lYearDays(i);
      }
      let leap = 0, isAdd = false;
      for (i = 1; i < m; i++) {
          leap = this.leapMonth(y);
          if (!isAdd) {//处理闰月
              if (leap <= i && leap > 0) {
                  offset += this.leapDays(y);
                  isAdd = true;
              }
          }
          offset += this.monthDays(y, i);
      }
      //转换闰月农历 需补充该年闰月的前一个月的时差
      if (isLeapMonth) {
          offset += day;
      }
      //1900年农历正月一日的公历时间为1900年1月30日0时0分0秒(该时间也是本农历的最开始起始点)
      const strap = Date.UTC(1900, 1, 30, 0, 0, 0);
      const calObj = new Date((offset + d - 31) * 86400000 + strap);
      const cY = calObj.getUTCFullYear();
      const cM = calObj.getUTCMonth() + 1;
      const cD = calObj.getUTCDate();

      return this.solar2lunar(cY, cM, cD);
  }
};

export default calendar;
```
该文件是封装的公历转农历，或者农历转公历的方法，以及其他的一些暴露出去的方法。

31. 新建`src/hooks/refreshPage.ts`文件：
```
import { unref } from 'vue';
import { toggleClass, removeClass } from "@/utils/operate";
const useRefreshPage = (route,router) => {
  const refreshPage = () => {
    // 执行刷新按钮的动画效果
    toggleClass(true, "vk-refresh-rotate", document.querySelector(".vk-btn-refresh"));
    const { fullPath } = unref(route);
    // 替换路由，到重定向页面，重定向页面会自动跳转到需要跳转的页面
    router.replace({
      path: "/redirect" + fullPath
    });
    // 移除刷新按钮的动画效果
    setTimeout(() => {
      removeClass(document.querySelector(".vk-btn-refresh"), "vk-refresh-rotate");
    }, 600);
  }
  return {
    refreshPage
  }
}
export default useRefreshPage;
```
这里将标签栏中的刷新页面功能进行了封装。

32. `src/layout/components/tagsView/index.vue`文件：
```
import useRefreshPage from '@/hooks/refreshPage';


// 重新加载
const { refreshPage } = useRefreshPage(route,router);


# 删除onFresh函数以及替换相关函数引用
```

33. `src/views/FrontEndLibrary/ElementPlus/Data/Tree/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpTree'
}
</script>
<script setup lang="ts">
import { computed,getCurrentInstance,ref,nextTick,watch } from "vue";
import { cloneDeep } from "lodash-es";
import { useStore } from 'vuex'
import { transformI18n } from "@/plugins/i18n";
import type Node from 'element-plus/es/components/tree/src/model/node'
import type { DragEvents } from 'element-plus/es/components/tree/src/model/useDragNode'
import type { DropType } from 'element-plus/es/components/tree/src/tree.type'
import type { TreeNode } from 'element-plus/es/components/tree-v2/src/types'
const store = useStore();


const treeData = (data,isLast,hideLine) => {
  data.forEach((item,index)=>{
    item.label = transformI18n(item.meta.title, item.meta.i18n)
    item.id = item.meta.id;
    item.value = item.meta.id;
    if(index === 0){
      item.isFirst = true;
    }else{
        item.isFirst = false;
    }
    if(index === data.length-1){
      item.isLast = true;
    }else{
      item.isLast = false;
    }
    item.hideLine = cloneDeep(hideLine);
    if(isLast){
      item.hideLine.push(item.meta.level-1);
    }
    if(item.children&&item.children.length>0){
      treeData(item.children, item.isLast,item.hideLine)
    }
  });
}
const treeDataBack = (data) => {
  treeData(data,false,[])
  return data;
}
const menuData = ref(treeDataBack(cloneDeep(store.state.routeMenus.routeMenus)));
const menuList:any = computed({
  set:(val) => {
    menuData.value = treeDataBack(val);
    console.log(menuData.value);
  },
  get:()=>menuData.value
});
interface Tree {
  id: number,
  label: string,
  value:number,
  children?: Tree[],
  isLast?:boolean,
  isFirst?:boolean,
  hideLine?:[],
  meta:{
    level:number,
  }
}
const defaultProps = {
  children: 'children',
  label: 'label',
}
const handleNodeClick = (data: Tree) => {
  console.log(data)
}
// 树形控件选择事件
const handleCheckChange = (
  data: Tree,
  checked: boolean,
  indeterminate: boolean
) => {
  console.log(data, checked, indeterminate)
}
// 过滤筛选事件
const filterText = ref('');
const filterNode = (value: string, data:Tree) => {
  if (!value) return true
  return data.label.includes(value)
}
const treeRef = ref()
watch(
  ()=>filterText.value, 
  (val) => {
  treeRef.value!.filter(val)
})
const editMenuData = (data,item,newChild) => {
  data.forEach((current,index)=>{
    if(current.id===item.meta.id){
      if(!current.children){
        current.children = []
      }
      current.children.push(newChild);
    }else{
      if(current.children&&current.children.length>0){
        editMenuData(current.children,item,newChild);
      }
    }
  });
}
let id = 1000
const append = (data: Tree) => {
  const newChild = {id:0,value:0,label:'', children: [],meta:{id:id++,title:{zh:"测试节点"+id++,en:'test node'+id++},level:data.meta.level+1} }
  editMenuData(menuList.value,data,newChild);
  menuList.value = treeDataBack(menuList.value);
}

const remove = (node: Node, data: Tree) => {
  console.log(node,"node");
  console.log(data,"数据");
  const parent = node.parent
  const children: Tree[] = parent.data.children || parent.data
  const index = children.findIndex((d) => d.id === data.id)
  children.splice(index, 1)
  menuList.value = [...menuList.value]
}

const handleDragStart = (node: Node, ev: DragEvents) => {
  console.log('拖拽开始：', node)
}
const handleDragEnter = (
  draggingNode: Node,
  dropNode: Node,
  ev: DragEvents
) => {
  console.log('拖拽进入：', dropNode.label)
}
const handleDragLeave = (
  draggingNode: Node,
  dropNode: Node,
  ev: DragEvents
) => {
  console.log('拖拽离开：', dropNode.label)
}
const handleDragOver = (draggingNode: Node, dropNode: Node, ev: DragEvents) => {
  console.log('拖拽到:', dropNode.label)
}
const handleDragEnd = (
  draggingNode: Node,
  dropNode: Node,
  dropType: DropType,
  ev: DragEvents
) => {
  console.log('拖拽结束:', dropNode && dropNode.label, dropType)
}
const handleDrop = (
  draggingNode: Node,
  dropNode: Node,
  dropType: DropType,
  ev: DragEvents
) => {
  console.log('拖拽落下:', dropNode.label, dropType)
}
// 判断拖拽时，目标节点能否成为拖动的目标位置，如果返回false，拖动节点不能被放到目标节点
const allowDrop = (draggingNode: Node, dropNode: Node, type: DropType) => {
  if (dropNode.data.label === '数据') {
    return type !== 'inner'
  } else {
    return true
  }
}
// 判断当前节点能否被拖拽
const allowDrag = (draggingNode: Node) => {
  return !draggingNode.data.label.includes('表单')
}

const value = ref();


const query = ref('')
const treeV2Ref = ref()

const onQueryChanged = (query: string) => {
  treeV2Ref.value!.filter(query)
}
const filterMethod = (query: string, node: TreeNode) => {
  return node.label!.includes(query)
}
</script>
<template>
<div class="ep-tree-page">
  <el-row :gutter="20">
    <el-col :xs="24" :sm="8">
      <el-card shadow="hover">
        <template #header>
          <div class="card-header">
            <span>树形控件</span>
          </div>
        </template>
        <el-alert title="当前控件可筛选、拖拽、选择、折叠、展开、添加、删除，扩展结构连接线" type="success" :closable="false" show-icon />
        <el-input v-model="filterText" placeholder="筛选菜单" />
        <el-tree 
          ref="treeRef" 
          :data="menuData" 
          :props="defaultProps" 
          @node-click="handleNodeClick" 
          show-checkbox 
          @check-change="handleCheckChange" 
          :filter-node-method="filterNode"
          :allow-drop="allowDrop"
          :allow-drag="allowDrag"
          draggable
          default-expand-all
          :expand-on-click-node="false"
          node-key="id"
          @node-drag-start="handleDragStart"
          @node-drag-enter="handleDragEnter"
          @node-drag-leave="handleDragLeave"
          @node-drag-over="handleDragOver"
          @node-drag-end="handleDragEnd"
          @node-drop="handleDrop" >
            <template #default="{ node, data }">
              <span class="item-tree-node">
                <span>{{ node.label }}</span>
                <template v-for="item in data.meta.level">
                  <span v-if="!data.hideLine.includes(item)" :key="item.value" :class="['item-tree-node-line-vertical',data.isLast&&item==data.meta.level?'is-last':'',data.isFirst&&data.meta.level===1?'is-first':'']" :style="{left:18*(item-1)+12+'px'}"></span>
                </template>
                <span class="item-tree-node-line-horizontal" :style="{left:18*(data.meta.level-1)+12+'px'}"></span>
                <span>
                  <a @click="append(data)"> 添加 </a>
                  <a @click="remove(node, data)"> 删除 </a>
                </span>
              </span>
            </template>
          </el-tree>
      </el-card>
    </el-col>
    <el-col :xs="24" :sm="8">
      <el-card shadow="hover">
        <template #header>
          <div class="card-header">
            <span>树选择</span>
          </div>
        </template>
        <el-alert title="当前控件可筛选、选择、折叠、展开" type="success" :closable="false" show-icon />
        <el-tree-select style="width:100%;" v-model="value" :data="menuList" multiple show-checkbox check-strictly filterable placeholder="请选择菜单节点" />
      </el-card>
    </el-col>
    <el-col :xs="24" :sm="8">
      <el-card shadow="hover">
        <template #header>
          <div class="card-header">
            <span>虚拟化树形控件</span>
          </div>
        </template>
        <el-alert title="当前控件可筛选，选择、折叠、展开；为大数据量专用，需用插件自身滚动条" type="success" :closable="false" show-icon />
        <el-input v-model="query" placeholder="请输入关键词" @input="onQueryChanged" />
        <el-tree-v2 ref="treeV2Ref" :data="menuList" :filter-method="filterMethod" show-checkbox :height="600" />
      </el-card>
    </el-col>
  </el-row>
</div>
</template>

<style scoped lang="scss">
.ep-tree-page{
  padding: 20px;
  display: flex;
  .el-row{
    flex: 1;
    .el-col{
      display: flex;
      .el-card{
        flex: 1;
      }
    }
  }
}
.el-alert{
  margin-bottom: 20px;
}
.el-input{
  margin-bottom: 20px;
}
.el-tree{
  :deep(.el-tree-node__content){
    position: relative;
    .el-icon{
      position: relative;
      z-index: 1;
    }
  }

}
.item-tree-node{
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  .item-tree-node-line-vertical{
    position: absolute;
    top:0;
    left:0;
    display: block;
    height: 100%;
    border-left: 1px dashed var(--el-border-color-light);
    &.is-last{
      height: 50%;
    }
    &.is-first{
        height: 50%;
        top: 50%;
    }
    &.is-last.is-first{
        height:0;
        border-left:none;
    }
  }
  .item-tree-node-line-horizontal{
    position: absolute;
    top: 50%;
    height:0;
    display: block;
    width: 10px;
    left:0;
    border-bottom: 1px dashed var(--el-border-color-light)
  }
}
</style>
```
该文件除了`element-plus`的树形控件的使用示例外，额外添加了属性控件的连接线控制相关逻辑和样式。

34. `src/views/FrontEndLibrary/ElementPlus/Form/ComprehensiveForm/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpComprehensiveForm',
}
</script>
<script setup lang="ts">
import { reactive, ref } from 'vue'
import ChinaArea from '@/plugins/chinaArea';
import type { FormInstance } from 'element-plus'
const ruleFormRef = ref<FormInstance>()
const chinaArea = new ChinaArea({leave:3,isall:false});
const chinaAreaOptions = ref(chinaArea.chinaData());
const chinaData = new ChinaArea().chinaAreaflat;
const labelPosition = ref('right');
const sizeForm = reactive({
  name: '',
  city:'',
  principal: '',
  date1: '',
  date2: '',
  level:[],
  type: [],
  audience:'',
  style:'',
  star:0,
  store:[],
  color:'rgba(255, 69, 0, 0.68)',
  voucher: true,
  voucherType:'',
  denomination:0,
  number:1,
  ValidityPeriod:[],
  startTime:'',
  endTime:'',
  desc: '',
})
const voucherList = [
    {label:'无门槛',value:1},
    {label:'满减券',value:2},
    {label:'抵扣券',value:3},
    {label:'折扣券',value:4},
]

function onSubmit(formEl: FormInstance | undefined) {
  if (!formEl) return;
  formEl.validate((valid) => {
    if (valid) {
      console.log('submit!')
    } else {
      console.log('error submit!')
      return false
    }
  })
}
const handleChinaAreaChange = (e) => {
    console.log(e);
    // const one = chinaData[e[0][0]]
    // const two = chinaData[e[0][1]]
    // const three = chinaData[e[0][2]]
    // console.log(one, two,three)
}
const caderProps = {
    multiple: true
}
const predefineColors = ref([
  '#ff4500',
  '#ff8c00',
  '#ffd700',
  '#90ee90',
  '#00ced1',
  '#1e90ff',
  '#c71585',
  'rgba(255, 69, 0, 0.68)',
  'rgb(255, 120, 0)',
  'hsv(51, 100, 98)',
  'hsva(120, 40, 94, 0.5)',
  'hsl(181, 100%, 37%)',
  'hsla(209, 100%, 56%, 0.73)',
  '#c7158577',
])
const storeList = [
    {key:1,label:'门店1',disabled:false},
    {key:2,label:'门店2',disabled:false},
    {key:3,label:'门店3',disabled:false},
    {key:4,label:'门店4',disabled:false},
    {key:5,label:'门店5',disabled:false},
    {key:6,label:'门店6',disabled:false},
    {key:7,label:'门店7',disabled:false},
    {key:8,label:'门店8',disabled:false},
    {key:9,label:'门店9',disabled:false},
    {key:10,label:'门店10',disabled:false},
    {key:11,label:'门店11',disabled:false},
]
const uploadFileList = ref([
  {
    name: 'food.jpeg',
    url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
  },
  {
    name: 'food2.jpeg',
    url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100',
  },
])
const validateDate1 = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请选择活动日期'))
  } else {
    if (sizeForm.date1 !== '') {
      if (!ruleFormRef.value) return
      ruleFormRef.value.validateField('date1', () => null)
    }
    callback()
  }
}
const validateDate2 = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请选择活动时间'))
  } else {
    if (sizeForm.date2 !== '') {
      if (!ruleFormRef.value) return
      ruleFormRef.value.validateField('date2', () => null)
    }
    callback()
  }
}
const rules = reactive({
  name: [
    { required: true, message: '请输入活动名称', trigger: 'blur' },
  ],
  city: [
    {
      required: true,
      message: '请选择活动覆盖地区',
      trigger: 'change',
    },
  ],
  date1:[{ validator: validateDate1, trigger: 'change' }],
  date2:[{ validator: validateDate2, trigger: 'change' }],
  desc: [
    { required: true, message: '请输入活动备注内容', trigger: 'blur' },
  ],
})
</script>
<template>
    <el-space :size="20" fill direction="vertical">
        <el-card shadow="never" class="no-border no-radius">
            <el-alert title="综合表单页将element-plus表单相关组件全部使用，并添加表单验证，自定义验证规则、集成省市区三级联动插件。" type="success" :closable="false" show-icon />
            <div class="form-header">
                <el-radio-group v-model="labelPosition">
                    <el-radio-button label="left">左对齐</el-radio-button>
                    <el-radio-button label="right">右对齐</el-radio-button>
                    <el-radio-button label="top">顶部对齐</el-radio-button>
                </el-radio-group>
            </div>
            <el-form
                ref="ruleFormRef"
                :model="sizeForm"
                :rules="rules"
                label-width="auto"
                :label-position="labelPosition">
                <el-form-item label="活动名称" prop="name">
                    <el-input v-model="sizeForm.name" />
                </el-form-item>
                <el-form-item label="涵盖地区" prop="city">
                    <el-cascader 
                        v-model="sizeForm.city" 
                        :options="chinaAreaOptions"
                        :props="caderProps"
                        collapse-tags
                        collapse-tags-tooltip
                        clearable @change="handleChinaAreaChange" />
                </el-form-item>
                <el-form-item label="活动负责人">
                    <el-select
                        v-model="sizeForm.principal"
                        placeholder="请选择活动负责人">
                        <el-option label="张三" value="zs" />
                        <el-option label="李四" value="ls" />
                        <el-option label="王五" value="ww" />
                        <el-option label="任六" value="rl" />
                    </el-select>
                </el-form-item>
                <el-form-item label="活动时间" required>
                    <div class="item-form">
                        <div class="item-flex">
                            <el-form-item prop="date1"><el-date-picker v-model="sizeForm.date1" type="date" placeholder="选择日期" /></el-form-item>
                        </div>
                        <div class="item-hyphen">-</div>
                        <div class="item-flex">
                            <el-form-item prop="date2"><el-time-picker v-model="sizeForm.date2" placeholder="选择时间" /></el-form-item>
                        </div>
                    </div>
                </el-form-item>
                <el-form-item label="活动级别">
                    <el-checkbox-group class="checkbox-button" v-model="sizeForm.level">
                        <el-checkbox-button :label="1" name="level" >一线城市门店</el-checkbox-button>
                        <el-checkbox-button :label="2" name="level" >二线城市门店</el-checkbox-button>
                        <el-checkbox-button :label="3" name="level" >三线城市门店</el-checkbox-button>
                    </el-checkbox-group>
                </el-form-item>
                <el-form-item label="活动类型" prop="type">
                    <el-checkbox-group v-model="sizeForm.type">
                        <el-checkbox label="online" name="type" border >线上推广</el-checkbox>
                        <el-checkbox label="offline" name="type" border >线下地推</el-checkbox>
                        <el-checkbox label="private" name="type" border >私域引导</el-checkbox>
                        <el-checkbox label="store" name="type" border >门店促销</el-checkbox>
                    </el-checkbox-group>
                </el-form-item>
                <el-form-item label="受众群体">
                    <el-radio-group class="radio-button" v-model="sizeForm.audience">
                        <el-radio-button label="elderly">老年人</el-radio-button>
                        <el-radio-button label="middle-aged">中年人</el-radio-button>
                        <el-radio-button label="young">年轻人</el-radio-button>
                        <el-radio-button label="kid">小孩子</el-radio-button>
                        <el-radio-button label="men">男人</el-radio-button>
                        <el-radio-button label="women">女人</el-radio-button>
                    </el-radio-group>
                </el-form-item>
                <el-form-item label="活动风格">
                    <el-radio-group v-model="sizeForm.style">
                        <el-radio border :label="'globalization'">国际化路线</el-radio>
                        <el-radio border :label="'popularize'">大众化路线</el-radio>
                        <el-radio border :label="'technology'">技术化路线</el-radio>
                    </el-radio-group>
                </el-form-item>
                <el-form-item label="会员门槛">
                    <el-rate v-model="sizeForm.star" :texts="['一星会员', '二星会员', '三星会员', '四星会员', '五星会员']" show-text />
                </el-form-item>
                <el-form-item label="参与门店">
                    <el-transfer v-model="sizeForm.store" filterable :data="storeList" :titles="['门店列表', '已选门店']" />
                </el-form-item>
                <el-form-item label="物料资源">
                    <el-upload class="el-upload-wrap" drag action="https://jsonplaceholder.typicode.com/posts/" multiple :file-list="uploadFileList">
                        <svg-icon class="el-icon--upload" icon-class="ep:upload-filled"></svg-icon>
                        <div class="el-upload__text">将文件拖拽到这里或者 <em>点击上传</em></div>
                        <template #tip>
                            <div class="el-upload__tip">文件限制大小在10M以内 </div>
                        </template>
                    </el-upload>
                </el-form-item>
                <el-form-item label="活动主题色">
                    <el-color-picker v-model="sizeForm.color" show-alpha :predefine="predefineColors" />
                </el-form-item>
                <el-form-item label="发放礼品券">
                    <el-switch v-model="sizeForm.voucher" />
                </el-form-item>
                <el-form-item label="礼品券类型" v-if="sizeForm.voucher">
                    <el-select-v2 v-model="sizeForm.voucherType" :options="voucherList" placeholder="请选择礼品券类型" />
                </el-form-item>
                <el-form-item label="礼品券面额" v-if="sizeForm.voucher">
                    <el-slider v-model="sizeForm.denomination" :max="999" />
                </el-form-item>
                <el-form-item label="礼品券数量" v-if="sizeForm.voucher">
                    <el-input-number v-model="sizeForm.number" :min="1" />
                </el-form-item>
                <el-form-item label="礼品券有效期" v-if="sizeForm.voucher">
                    <el-date-picker v-model="sizeForm.ValidityPeriod" type="datetimerange" range-separator="-" start-placeholder="开始时间" end-placeholder="结束时间" />
                </el-form-item>
                <el-form-item label="礼品券使用时间" v-if="sizeForm.voucher">
                    <el-time-select v-model="sizeForm.startTime" :max-time="sizeForm.endTime" placeholder="开始时间" start="08:30" step="00:15" end="18:30" />
                    <el-time-select v-model="sizeForm.endTime" :min-time="sizeForm.startTime" placeholder="结束时间" start="08:30" step="00:15" end="18:30" />
                </el-form-item>
                <el-form-item label="活动备注" prop="desc">
                    <el-input v-model="sizeForm.desc" type="textarea" />
                </el-form-item>
                <el-form-item>
                    <el-button type="primary" @click="onSubmit(ruleFormRef)">创建活动</el-button>
                    <el-button>取消</el-button>
                </el-form-item>
            </el-form>
        </el-card>
    </el-space>
</template>

<style scoped lang="scss">

.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.no-border{
  border:none;
}
.no-radius{
  border-radius: 0;
}
.form-header{
    margin: 20px 0;
}
.item-form{
    display: flex;
    flex: 1;
    .item-flex{
        flex: 1;
        :deep(.el-date-editor.el-input){
            width: 100%;
            display: inline-flex;
        }
        .el-form-item{
            :deep(.el-form-item__label-wrap){
                margin:0!important;
            }
        }
    }
    .item-hyphen{
        padding: 6px;
    }
}
</style>
```
该文件涵盖了`element-plus`的所有表单组件的使用，并配置了基础的验证功能。以及封装了省市区三级联动的数据。

35. 新建`src/plugins/chinaArea/index.ts`文件：
```
import chinaAreaData from 'china-area-data'

// 递归子选项
interface Options {
  label: string
  value: string
  leave?: number
  children?: Options[]
}
// 省市区数据结构化定义类型
interface ChinaArr {
  label: string
  parent?: string
  value: string
}
// 递归输入参数
interface InRecursion {
  ssq: { [key: string]: string }
  leave?: number
  custom?: Options
}
interface ChinaAreaInput {
  leave: number
  isall: boolean
  customItem?: Options[]
  after?: boolean
}
interface ChinaAreaflat {
  [key: string]: ChinaArr
}
class ChinaArea {
  chinaAreaData = chinaAreaData
  chinaAreaflat: ChinaAreaflat // 对象改平之后的结构，用于数据取值
  // 将数组结构转化为对象结构
  chinaObj() {
    const list: ChinaAreaflat = {}
    for (const key in chinaAreaData) {
      for (const i in chinaAreaData[key]) {
        const item: ChinaArr = {
          label: chinaAreaData[key][i],
          value: i,
        }
        if (key !== '86') item.parent = key
        list[i] = item
      }
    }
    return list
  }
  leave = 3 // 控制递归层级
  isall = false // 是否需要全部选项
  constructor({ leave = 0, isall = false }: ChinaAreaInput = {} as ChinaAreaInput) {
    this.leave = leave
    this.isall = isall
    this.chinaAreaflat = this.chinaObj()
  }
  // 省市区数据格式化
  chinaData() {
    const province = chinaAreaData[86]
    const opt: InRecursion = { ssq: province }
    if (this.isall) {
      opt.custom = {
        label: '全部',
        value: 'all',
      }
    }
    return this.recursion(opt)
  }
  // 递归得到省市区三级数据
  private recursion({
    ssq,
    leave = 0, // 这个不是外部操作的，不允许修改
    custom,
  }: InRecursion) {
    const layer = leave + 1
    if (layer > this.leave) return
    const reprovince = []
    custom && reprovince.push(custom)
    for (const i in ssq) {
      const item: Options = {
        label: ssq[i],
        value: i,
        leave: layer,
      }
      if (chinaAreaData[i]) {
        item.children = this.recursion({ ssq: chinaAreaData[i], leave: layer, custom })
      }
      reprovince.push(item)
    }
    return reprovince
  }
}

export default ChinaArea
```
该文件封装了省市区三级数据的转化为`element-plus`的联动组件可识别数据的功能，并能根据选择数据，联查其上下级数据。

36. `src/views/FrontEndLibrary/ElementPlus/Form/StepForm/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'EpStepForm'
}
</script>
<script setup lang="ts">
import { reactive, ref } from 'vue'
import type { FormInstance } from 'element-plus'
import {isNumber} from '@/utils/is';
const stepFormRef = ref<FormInstance>()
let step = ref(0);
type stepFormType = {
  payeeAccount:string;
  paymentAccount:string;
  name:string;
  money:string|number;
  password:string;
}
const stepForm:stepFormType = reactive({
  payeeAccount:'test@aliyun.com',
  paymentAccount:'test@qq.com',
  name:"test",
  money:100,
  password:'123456'
})
const lookBill = () => {
  console.log("账单不存在");
}
const againStep = () => {
  step.value =  0;
}
const prevStep = () => {
  step.value = 0;
}
function nextStep(formEl: FormInstance | undefined) {
  if (!formEl) return;
  formEl.validate((valid) => {
    if (valid) {
      console.log('submit!')
      step.value = 1;
    } else {
      console.log('error submit!')
      return false
    }
  })
}
let loading = ref(false);
function onSubmit(formEl: FormInstance | undefined) {
  if (!formEl) return;
  formEl.validate((valid) => {
    if (valid) {
      loading.value = true;
      console.log('submit!')
      setTimeout(()=>{
        loading.value = false;
        step.value = 2;
      },2000);
    } else {
      console.log('error submit!')
      return false
    }
  })
}
const validatePayeeAccount = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请输入付款账户'))
  } else {
    callback()
  }
}
const validatePaymentAccount = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请输入收款账户'))
  } else {
    callback()
  }
}
const validateName = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请输入收款人姓名'))
  } else {
    callback()
  }
}
const validateMoney = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请输入转账金额'))
  } else {
    if (isNumber(stepForm.money)) {
      callback()
    }else{
      callback(new Error('金额只能为正数'));
    }
  }
}
const validatePassword = (rule: any, value: any, callback: any) => {
  if (value === '') {
    callback(new Error('请输入支付密码'))
  } else {
    callback()
  }
}
const rules = reactive({
  payeeAccount:[{ validator: validatePayeeAccount, trigger: 'blur' }],
  paymentAccount:[{ validator: validatePaymentAccount, trigger: 'blur' }],
  name:[{ validator: validateName, trigger: 'blur' }],
  money:[{ validator: validateMoney, trigger: 'blur' }],
  password:[{ validator: validatePassword, trigger: 'blur' }],
})
</script>
<template>
  <el-space :size="20" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <el-row :gutter="20">
        <el-col :span="18" :offset="3">
          <el-steps :active="step" align-center finish-status="success" process-status="finish">
            <el-step title="第一步" description="填写转账信息" />
            <el-step title="第二步" description="确认转账信息" />
            <el-step title="第三步" description="完成" :status="step===2?'success':'wait'" />
          </el-steps>
          <div class="step1" v-if="step===0">
            <el-form ref="stepFormRef" :model="stepForm" :rules="rules" label-width="auto" label-position="right">
              <el-form-item label="付款账户" prop="payeeAccount" required>
                <el-input v-model="stepForm.payeeAccount" clearable></el-input>
              </el-form-item>
              <el-form-item label="收款账户" prop="paymentAccount" required>
                <el-input v-model="stepForm.paymentAccount" clearable />
              </el-form-item>
              <el-form-item label="收款人姓名" prop="name" required>
                <el-input v-model="stepForm.name" clearable  />
              </el-form-item>
              <el-form-item label="转账金额" prop="money" required>
                <el-input v-model="stepForm.money" clearable><template #prepend>￥</template></el-input>
              </el-form-item>
              <el-form-item class="form-button">
                  <el-button type="primary" @click="nextStep(stepFormRef)">下一步</el-button>
              </el-form-item>
            </el-form>
            <div class="tips">
              <h3>转账到支付宝</h3>
              <p>收款账户可填写手机号、支付宝账户，收款人姓名需与支付宝账户或手机号绑定一致，转账金额限定在500以内。</p>
              <h3>转账到微信</h3>
              <p>收款账户可填写手机号、微信号，收款人姓名需真实有效，与实名认证姓名一致，转账金额限定在1000以内。</p>
              <h3>转账到银行卡</h3>
              <p>收款账户需填写收款人的银行卡号，收款人姓名必须与所填写银行卡号归属人姓名一致，转账金额无限制。</p>
            </div>
          </div>
          <div class="step2" v-if="step===1">
            <el-alert title="确认转账后，资金将直接打入对方账户，无法退回，请谨慎操作，谨防上当受骗。" type="warning" show-icon :closable="false" />
            <el-form ref="stepFormRef" :model="stepForm" :rules="rules" label-width="auto">
              <el-form-item label="付款账户：" prop="payeeAccount">
                {{stepForm.payeeAccount}}
              </el-form-item>
              <el-form-item label="收款账户：" prop="paymentAccount">
                {{stepForm.paymentAccount}}
              </el-form-item>
              <el-form-item label="收款人姓名：" prop="name">
                {{stepForm.name}}
              </el-form-item>
              <el-form-item label="转账金额：" prop="money">
                <strong>￥{{stepForm.money}}</strong>
              </el-form-item>
              <el-form-item class="password" label="支付密码" prop="password" required>
                <el-input type="password" v-model="stepForm.password" show-password clearable></el-input>
              </el-form-item>
              <el-form-item class="form-button">
                  <el-button @click="prevStep">上一步</el-button>
                  <el-button type="primary" @click="onSubmit(stepFormRef)" :loading="loading">提交</el-button>
              </el-form-item>
            </el-form>
          </div>
          <div class="step3" v-if="step===2">
            <el-result
              icon="success"
              title="转账成功"
              sub-title="预计两小时内到账">
              <template #extra>
                <el-button type="primary" @click="againStep">再转一笔</el-button>
                <el-button @click="lookBill">查看账单</el-button>
              </template>
            </el-result>
            <el-form ref="stepFormRef">
              <el-form-item label="付款账户：">
                {{stepForm.payeeAccount}}
              </el-form-item>
              <el-form-item label="收款账户：">
                {{stepForm.paymentAccount}}
              </el-form-item>
              <el-form-item label="收款人姓名：">
                {{stepForm.name}}
              </el-form-item>
              <el-form-item label="转账金额：">
                <strong>￥{{stepForm.money}}</strong>
              </el-form-item>
            </el-form>
          </div>
        </el-col>
      </el-row>
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">
.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.no-border{
  border:none;
}
.no-radius{
  border-radius: 0;
}
.el-steps{
  margin-top: 20px;
}
.el-form{
  width: 50%;
  margin: 40px auto;
  .form-button{
    :deep(.el-form-item__content){
      justify-content: center;
    }
  }
  .el-form-item.password{
    margin-top: 30px;
    padding-top: 30px;
    border-top: 1px solid var(--el-border-color-light);
  }
}
.el-alert{
  margin: 40px auto 0;
  width: 50%;
}
.tips{
  width: 70%;
  margin: 0 auto;
  border: 1px solid var(--el-color-primary);
  border-radius: var(--el-border-radius-base);
  padding: 5px 20px 15px;
  background-color: var(--el-color-primary-light-9);
}
.step3{
  .el-form{
    width: 70%;
    border: 1px dashed var(--el-border-color);
    background: var(--el-fill-color-light);
    border-radius: var(--el-border-radius-base);
    padding: 30px;
  }
}
</style>
```
该页面主要是展示分步表单的使用。以上是`element-plus`组件使用的所有页面。相关国际化也有修改。

37. `src/plugins/i18n/en/buttons.ts`,`src/plugins/i18n/zh-CN/buttons.ts`:
```
buttonPrevYear:"Previous Year",
buttonPrevMonth:"Previous Month",
buttonNextYear:"Next Year",
buttonNextMonth:"Next Month",
buttonBackToday:"Today",

buttonPrevYear:"上一年",
buttonPrevMonth:"上一月",
buttonNextYear:"下一年",
buttonNextMonth:"下一月",
buttonBackToday:"返回今天",
```

38. `src/plugins/i18n/en/menus.ts`,`src/plugins/i18n/zh-CN/menus.ts`:
```
menuElementPlus:"Element Plus",
menuEpButton:"Button component",
menuEpLink:"Text Link component",
menuEpTag:"Tag component",
menuEpNotice:"notice component",
menuEpBasic:"Basic component",
menuEpFeedback:"Feedback component",
menuEpData:"Data",
menuEpPaginationList:"Pagination List",
menuEpInfiniteScrollList:"Infinite Scroll List",
menuEpArticle:"Article",
menuEpCalendar:"Calendar",
menuEpTree:"Tree",
menuEpForm:"Form",
menuEpComprehensiveForm:"Comprehensive Form",
menuEpStepForm:"Step Form",

menuElementPlus:"element-plus",
menuEpButton:"按钮组件",
menuEpLink:"文本链接",
menuEpTag:"标签组件",
menuEpNotice:"消息提醒",
menuEpBasic:"基础组件",
menuEpFeedback:"反馈组件",
menuEpData:"数据",
menuEpPaginationList:"分页加载",
menuEpInfiniteScrollList:"滚动加载",
menuEpArticle:"详情页",
menuEpCalendar:"日历",
menuEpTree:"树结构",
menuEpForm:"表单",
menuEpComprehensiveForm:"综合表单",
menuEpStepForm:"分步表单",
```

39. `src/plugins/i18n/en/settings.ts`,`src/plugins/i18n/zh-CN/settings.ts`:
```
settingLoadingFailed:"Loading Failed",
settingNoMore:"No More",
settingNoData:"No Data",

settingLoadingFailed:"加载失败",
settingNoMore:"没有更多了",
settingNoData:"暂无数据",
```

40. `src/plugins/i18n/en/text.ts`,`src/plugins/i18n/zh-CN/text.ts`:
```
textVacation:"Vacation",
textWork:"Work",

textVacation:"休",
textWork:"班",
```

以上是新增的国际化配置。全局样式也有一些修改，以及`element-plus`组件的默认样式修改。

41. `src/style/element-plus.scss`文件：
```
:root{
  --el-border-radius-base:2px;
}
.el-tag{
  --el-tag-border-radius: var(--el-border-radius-base);
}
.el-notification{
  --el-notification-radius: var(--el-border-radius-base)!important;
}
.el-input__prefix,.el-input__suffix{
  align-items: center;
}
.el-message__closeBtn{
  position: absolute;
  color: var(--el-message-close-icon-color);
}
.el-overlay{
  background:rgba(0,0,0,.25);
}
.el-select-dropdown__item{
  padding: 0 20px;
}
.el-form{
  .el-form-item{margin-bottom: 0;}
  &.el-form--inline{
    .el-form-item+.el-form-item{
      margin-top: 0;
      margin-left: 0;
    }
    .el-form-item{
      margin-right:0;
    }
    .el-form-item__label{
      padding: 6px;
    }
  }
  &.el-form--label-top{
    margin-left: -6px;
    margin-right: -6px;
    .el-form-item{
      .el-form-item__label{
        padding: 6px;
        margin-bottom:0;
      }

    }
  }
  .el-form-item+.el-form-item{
    margin-top: 6px;
  }
  .el-form-item__label{
    padding: 6px 6px 6px 0;
  }
  .el-form-item__content{
    >.el-button,>.el-link,>.el-tag,>.el-check-tag,>.el-select,>.el-input,>.el-checkbox,>.el-checkbox-group >.el-checkbox,>.el-checkbox-group.checkbox-button,>.el-radio,>.el-radio-group >.el-radio,>.el-radio-group.radio-button,>.el-switch,>.el-textarea,>.el-cascader,>.el-color-picker,>.el-select-v2,>.el-input-number,>.el-date-editor,>.el-slider,>.el-rate,>.el-transfer,>.el-upload-wrap{
      margin: 6px;
    }
    >.el-form-item__error{
      left:6px;
      margin-top: -5px;
    }
    .el-select,.el-cascader,.el-select-v2{
        flex: 1;
    }
  }
  .el-link{
    line-height: 20px;
    >.el-icon{
      margin-right: 5px;
    }
  }
  .el-button-group{
    margin: 6px;
    .el-button{
      margin:0;
    }
  }
  .el-cascader__tags{
    z-index: 1;
  }
  .el-rate__item{
    display: inline-flex;
  }
}
.el-check-tag{
  font-weight: normal;
}
.el-alert--success.is-light{
  border: 1px solid var(--el-color-success);
}
.el-alert--warning.is-light{
  border: 1px solid var(--el-color-warning);
}
.el-alert--error.is-light{
  border: 1px solid var(--el-color-error);
}
.el-alert--info.is-light{
  border: 1px solid var(--el-color-info);
}
.el-alert{
  --el-alert-title-font-size: 14px;
}
.el-card{
  color: var(--el-text-color-default);
}
.el-steps{
  .el-step__head.is-success{
    .el-step__line{
      background-color: var(--el-color-success);
    }
  }
}
```
这里面主要增加的是对表单组件默认的样式的覆盖修改。

42. `src/style/public.scss`文件：
```
ul,li,ol,dl,dt,dd {
  margin: 0;
  padding: 0;
  list-style: none;
}
label{
  font-weight: normal;
}

.image-slot{
  display: block;
  width:100%;
  img{
    display: block;
    width: 100%;
  }
  .slot-content{
    display: flex;
    position: absolute;
    left:0;
    top:0;
    width: 100%;
    height: 100%;
    background: var(--el-fill-color-light);
    color: var(--el-text-color-secondary);
    font-size: 30px;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    p{
      font-size: var(--el-font-size-base);
      margin: 5px 0 0;
    }
  }
}
```
以上是该文件新增的样式，占位图片相关的样式作为公共代码。

43. `src/style/layout/index.scss`文件：
```
.vk-layout-view{
  position: relative;
  min-height: calc(100vh - var(--el-header-height) - var(--el-tag-height));
}
```
该文件主要给`vk-layout-view`增加最小高度。

44. `src/style/layout/setting.scss`文件：
```
# 这里主要为在切换组件大小时，下拉框因前缀样式被修改，动态高度获取会失败，这里强制固钉各个尺寸下的下拉框高度，保证组件变形。
.el-form-item__content{
  flex: 0 auto;
  .el-input__inner{
    width: 70px;
    height: 32px!important; // 新增样式
  }
  .vk-menu-select,.vk-header-select{
    .el-input__inner{
      font-size:0;
    }
    // 新增样式
    &.el-select--large{
      .el-input--large{
        .el-input__inner{
          height: 40px!important;
        }
      }
    }
    // 新增样式
    &.el-select--small{
      .el-input--small{
        .el-input__inner{
          height: 24px!important;
        }
      }
    }
    .el-input__prefix{
        right: 32px;
        position: absolute;
        left: 10px;
        cursor: pointer;
        .el-input__prefix-inner{
        padding: 8px 0;
        width: 100%;
        height: 100%;
        div{
            width: 100%;
            margin:0;
            .vk-select-mark{
            display: block;
            width: 100%;
            height: 100%;
            border-radius: var(--el-border-radius-base);
            }
        }
        }
    }
  }
}


# 这里修改border的样式
.vk-menu-color,.vk-header-color{
  .el-select-dropdown__item{
    &:first-child{
      .vk-icon{
        border: var(--el-border); // 修改样式
        padding:0;
      }
    }
  }
}


// 菜单栏白背景时样式控制
body[data-menu-theme='white']{
  .vk-drawer{
    .vk-menu-select{
      .vk-select-mark{
        border: var(--el-border); // 修改样式
      }
    }
  }
}
// 顶栏白背景时样式控制
body[data-header-theme='white']{
  .vk-drawer{
    .vk-header-select{
      .vk-select-mark{
        border: var(--el-border); // 修改样式
      }
    }
  }
}
```

以上是本次添加`element-plus`组件使用相关页面所涉及到的新增及修改页面。