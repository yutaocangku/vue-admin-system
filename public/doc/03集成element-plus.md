﻿# 集成element-plus
```
yarn add element-plus
```
# 引入方式-整体引入
修改`src/main.ts`文件：
```
import { createApp } from 'vue'
import App from '@/App.vue'

// elementPlus完整引入
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css'

import "@/style/index.scss"

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
```
修改`src/App.vue`文件，引入`element-plus`组件：
```
<script setup lang="ts">
// This starter template is using Vue 3 <script setup> SFCs
// Check out https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup
import HelloWorld from '@/components/HelloWorld.vue';
import { ref } from 'vue'
const value1 = ref("")
</script>

<template>
  <img alt="Vue logo" src="@/assets/logo.png" />
  <HelloWorld msg="Hello Vue 3 + TypeScript + Vite" />
  <p class="demo">测试</p>
  <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
</template>

<style lang="scss">
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: $primary;
  margin-top: 60px;
}
</style>
```
此时项目运行，组件能正常展示，则整体引入完成

# 引入方式-按需引入
安装按需加载依赖包，这个插件会自动加载组件与其对应的样式
```
yarn add unplugin-element-plus -D
```
这里的按需引入原理是，预先将本项目可能用到的`element-plus`的组件全部挂载到项目的全局组件中，这样在项目的任何地方都可以使用已经被加载组件。

新建`src/plugins/element-plus/index.ts`文件结构，该文件内存放所有本项目会用到的`element-plus`组件：
```
import { App } from "vue";
// 引入element-plus的组件
import {
  ElTag,
  ElAffix,
  ElSkeleton,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElScrollbar,
  ElSubMenu,
  ElButton,
  ElCol,
  ElRow,
  ElSpace,
  ElDivider,
  ElCard,
  ElDropdown,
  ElDialog,
  ElMenu,
  ElMenuItem,
  ElDropdownItem,
  ElDropdownMenu,
  ElIcon,
  ElInput,
  ElDatePicker,
  ElForm,
  ElFormItem,
  ElLoading,
  ElPopover,
  ElPopper,
  ElTooltip,
  ElDrawer,
  ElPagination,
  ElAlert,
  ElRadio,
  ElRadioButton,
  ElRadioGroup,
  ElDescriptions,
  ElDescriptionsItem,
  ElBacktop,
  ElSwitch,
  ElBadge,
  ElTabs,
  ElTabPane,
  ElAvatar,
  ElEmpty,
  ElCollapse,
  ElCollapseItem,
  ElInfiniteScroll
} from "element-plus";

// 声明组件名，用来接收引入的element-plus，两者需要一一对应，使用起来才能像直接使用element-plus组件一样
const components:any[] = [
  ElTag,
  ElAffix,
  ElSkeleton,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElScrollbar,
  ElSubMenu,
  ElButton,
  ElCol,
  ElRow,
  ElSpace,
  ElDivider,
  ElCard,
  ElDropdown,
  ElDialog,
  ElMenu,
  ElMenuItem,
  ElDropdownItem,
  ElDropdownMenu,
  ElIcon,
  ElInput,
  ElDatePicker,
  ElForm,
  ElFormItem,
  ElPopover,
  ElPopper,
  ElTooltip,
  ElDrawer,
  ElPagination,
  ElAlert,
  ElRadio,
  ElRadioButton,
  ElRadioGroup,
  ElDescriptions,
  ElDescriptionsItem,
  ElBacktop,
  ElSwitch,
  ElBadge,
  ElTabs,
  ElTabPane,
  ElAvatar,
  ElEmpty,
  ElCollapse,
  ElCollapseItem
];
const plugins = [ElLoading,ElInfiniteScroll];

export function useElementPlus(app: App) {
  components.forEach((component: any) => {
    app.component(component.name, component);
  });
  plugins.forEach(plugin => {
    app.use(plugin);
  });
}
```
修改`vite.config.ts`文件：
```
import { resolve } from "path";
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import ElementPlus from "unplugin-element-plus/vite";

// 路径查找
const pathResolve = (dir: string): string => {
  return resolve(__dirname, dir);
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    ElementPlus({}),
  ],
  resolve: {
		alias: {
			'@': pathResolve('src'),
		}
	},
  css: {
    // css预处理器
    preprocessorOptions: {
      scss: {
        // 引入 var.scss 这样就可以在全局中使用 var.scss中预定义的变量了
        // 给导入的路径最后加上 ; 
        additionalData: `@import "@/style/var.scss";`
      }
    }
  },
})
```
修改`src/main.ts`文件：
```
import { createApp } from 'vue'
import App from '@/App.vue'

// element-plus 按需引入
import { useElementPlus } from "@/plugins/element-plus"; // element-plus

import "@/style/index.scss"

const app = createApp(App)

app.use(useElementPlus)
app.mount('#app')
```
此时`ts`可能会提示这样的错误：

> 找不到模块“@/plugins/element-plus”或其相应的类型声明

修改`tsconfig.json`文件：
```
{
  "compilerOptions": {
    "target": "esnext",
    "useDefineForClassFields": true,
    "module": "esnext",
    "moduleResolution": "node",
    "strict": true,
    "jsx": "preserve",
    "sourceMap": true,
    "resolveJsonModule": true,
    "esModuleInterop": true,
    "lib": ["esnext", "dom"],
    // 新增
    "baseUrl": ".", 
    "paths":{
      "@/*": ["src/*"],
    },
  },
  "include": ["src/**/*.ts", "src/**/*.d.ts", "src/**/*.tsx", "src/**/*.vue"]
}
```
此时运行项目，`element-plus`组件依然可以正常显示，表示按需加载已配置完成。