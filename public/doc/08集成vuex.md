﻿# 集成vuex
```
yarn add vuex@next
```
这里集成`vuex`是因为之后的功能会用到状态管理，在之前，`vue-router`的静态路由做了模块化管理，`vuex`的状态管理，也同样需要进行模块化处理，并实现自动导入所有模块的状态管理，进行实例化。

新建`src/store`文件夹，其内新建`modules`文件夹，存放模块的状态管理文件，新建`index.ts`文件，用来导入`modules`中的模块文件，并进行实例化：
```
import { createStore } from 'vuex';

// Vite supports importing multiple modules from the file system using the special import.meta.glob function
// see https://cn.vitejs.dev/guide/features.html#glob-import
// 自动导入modules文件夹下的所有文件
const modulesFiles = import.meta.globEager('./modules/*.ts');
const pathList: string[] = [];

for (const path in modulesFiles) {
  pathList.push(path);
}

const modules = pathList.reduce((modules: any, modulePath: string) => {
  const moduleName = modulePath.replace(/^\.\/modules\/(.*)\.\w+$/, '$1');
  const value = modulesFiles[modulePath];
  modules[moduleName] = value.default;
  return modules;
}, {});
// 实例化模块的状态管理
const store = createStore({
  modules
});

export default store
```
在`src/main.ts`中将`store`挂载到全局：
```
import store from './store';

app.use(store)
```
`vuex`的基本配置到此就算完成了，接下来会在具体功能上使用模块的状态管理