﻿# svg图标功能优化
在之前封装的svg图标功能还是较为单一，限制也比较大，这里将会扩展`svg`图标功能，让项目的图标功能更全面。图标插件方面除了`element-plus`的自身图标单独下载并全局注册，方便其自身组件使用外，在`package.json`中还新增了`iconify/json`插件，`iconify`插件有很多种用法，具体用法这里不细说了，该插件最实用的就是将各个平台的图标集都集合在这个插件中，可以任意使用任何该插件中存在的图标集中的图标，本项目的图标功能将会集成基于`iconify`的`Ant Design`图标集，`Bootstrap`图标集，`Element Plus`图标集，`FontAwesome`图标集，`Remix`图标集，除以上5个图标集外，还会将`iconfont`作为一个图标集封装进项目的图标功能组件中，以及自定义图标也作为一个单独的图标集封装进图标组件中。基于以上功能，本项目的图标功能的可操作性就更强了，自由度也更高一些，选择范围也更大了。

新建`src/plugins/svgIcon/index.ts`文件，用来封装获取图标数据：
```
import { App } from "vue";
import antDesign from '@iconify/json/json/ant-design.json'; // Ant Design 图标集    1
import bi from '@iconify/json/json/bi.json'; // Bootstrap 图标集                    2
import ep from '@iconify/json/json/ep.json'; // Element Plus 图标集                 3
import fa6Solid from '@iconify/json/json/fa6-solid.json'; // FontAwesome 图标集     4
import iconfont from '@/assets/iconfont/iconfont.json'; // Iconfont 图标集          5
import ri from '@iconify/json/json/ri.json'; // Remix 图标集                        6
// 获取src/assets/svg文件夹下的 自定义svg图标集
const customSvgIconList = () => {
  // 自动导入/src/assets/svg文件夹下的所有svg图标
  const modulesFiles = import.meta.globEager("@/assets/svg/*.svg");
  const pathList: string[] = [];
  for (const path in modulesFiles) {
    pathList.push(path);
  }
  return pathList.reduce((modules: any, modulePath: string) => {
    const moduleName = modulePath.replace(/^\.\.\/\.\.\/assets\/svg\/(.*)\.\w+$/, '$1');
    const value = modulesFiles[modulePath];
    modules[moduleName] = value;
    return modules;
  }, {});
}
export const customSvgIconComponents = customSvgIconList(); // 自定义svg图标集      7
// 将自定义图标在全局注册
// export function useCustomSvgIcon(app: App) {
//   // 注册图标
//   for(const key in customSvgIconComponents){
//     app.component(key, customSvgIconComponents[key].default);
//   }
// }
// 暴露图标集类型选择数据
export const iconTypeList = () => {
  return [
    {label:"Ant Design",value:"ad"},
    {label:"Bootstrap",value:"bi"},
    {label:"Element Plus",value:"ep"},
    {label:"Font Awesome",value:"fa"},
    {label:"IconFont",value:"ii"},
    {label:"Remix",value:"ri"},
    {label:"自定义",value:"ci"},
  ]
}
// 获取所有类型图表集的图标数据
export const getIconList = () => {
  type iconType = {
    type:string;
    key:string;
    value:string;
  }
  let tabList = ["ad","bi","ep","fa","ii","ri","ci"];
  let iconList = [];
  tabList.forEach((item)=>{
    let data = {};
    switch(item){
      case 'ad':
        data = antDesign.icons;           // Ant Design 图标集
        break;
      case 'bi':
        data = bi.icons;                  // Bootstrap 图标集
        break;
      case 'ep':
        data = ep.icons;                  // Element Plus 图标集
        break;
      case 'fa':
        data = fa6Solid.icons;            // FontAwesome 图标集
        break;
      case 'ii':
        data = iconfont.glyphs;           // Iconfont 图标集
        break;
      case 'ri':
        data = ri.icons;                  // Remix 图标集
        break;
      default:
        data = customSvgIconComponents;   // 自定义svg图标集
        break;
    }
    if(item === 'ii'){
      for(const key in data){
        let itemIcon:iconType = {
          type:item,
          key:'icon-'+data[key].font_class,
          value:'icon-'+data[key].font_class
        };
        iconList.push(itemIcon);
      }
    }else if(item === 'ci'){
      for(const key in data){
        let itemIcon:iconType = {
          type:item,
          key:key,
          value:data[key].default
        };
        iconList.push(itemIcon);
      }
    }else{
      for(const key in data){
        let itemIcon:iconType = {
          type:item,
          key:key,
          value:data[key].body
        };
        iconList.push(itemIcon);
      }
    }
  });
  return iconList;
}
```
该文件为图标增加了分类字段，并定义了分类字段的值，而根据图标集的特性，图标的获取方式也有所不同，基于`iconify`插件的，通过引入`json`文件，获取到的是`svg`图标的源码，这样可以直接在使用的时候插入图标源码，就能让图标显示了，而`iconfont`在我们收集图标后下载的压缩包中，`js`文件将依然作为图标源数据使用，图标的源码在js文件中，使用则是通过图标名作为识别。而`json`文件，就作为获取图标标识，方便图标展示使用，而自定义图标，则通过自动获取`assets/svg`文件夹内的所有`.svg`文件，并动态导入的形式，形成自定义图标集的数据源。该文件主要作用就是将7个图标集的图标获取到并合并成原始图标数据源，供图标组件使用。

图标源数据有了，接下来封装图标组件，在之前的图标组件基础上进行修改,`src/components/SvgIcon/index.vue`文件：
```
<script lang="ts">
import { defineComponent, computed } from 'vue'
import {getIconList} from "@/plugins/svgIcon"
interface Props {
	iconClass: string
	className: string
}

type iconType = {
	type:string;
	key:string;
	value:string;
}
export default defineComponent({
	name: 'SvgIcon',
	props: {
		iconClass: {
			type: String,
			required: true
		},
		className: {
			type: String,
			default: () => ''
		}
	},
	setup(props: Props) {
		const iconInfo = computed(():boolean|iconType => {
			return getSvgIcon(props.iconClass);
		})
		const svgClass = computed((): string => {
			if (props.className) {
				return 'svg-icon ' + props.className
			} else {
				return 'svg-icon'
			}
		});
		const getSvgIcon = (iconName:string):boolean|iconType => {
			let iconList:Array<iconType> = getIconList();
			if(iconName.indexOf(":")!=-1){
				const iconType = iconName.split(":")[0];
				const iconKey = iconName.split(":")[1];
				const iconData = iconList.filter((item:iconType)=>{
					return item.type === iconType && item.key === iconKey
				})[0];
				if(iconData){
					return iconData
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		const valueIsBoolean = (val:any):val is boolean => {
			return typeof val === 'boolean'
		}
		return {
			iconInfo,
			svgClass,
			valueIsBoolean
		}
	}
})
</script>

<template>
  <el-icon v-if="iconInfo">
    <svg v-if="!valueIsBoolean(iconInfo)&&iconInfo.type==='bi'" :class="svgClass" aria-hidden="true" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" v-html="iconInfo.value"></svg>
    <svg v-else-if="!valueIsBoolean(iconInfo)&&iconInfo.type==='fa'" :class="svgClass" aria-hidden="true" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" v-html="iconInfo.value"></svg>
    <svg v-else-if="!valueIsBoolean(iconInfo)&&iconInfo.type==='ri'" :class="svgClass" aria-hidden="true" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" v-html="iconInfo.value"></svg>
    <svg v-else-if="!valueIsBoolean(iconInfo)&&iconInfo.type==='ii'" :class="svgClass" aria-hidden="true" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg">
      <use :xlink:href="'#'+iconInfo.value" />
    </svg>
    <svg v-else-if="!valueIsBoolean(iconInfo)&&iconInfo.type==='ad'||!valueIsBoolean(iconInfo)&&iconInfo.type==='ep'" :class="svgClass" aria-hidden="true" viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg" v-html="iconInfo.value"></svg>
    <component v-else :is="!valueIsBoolean(iconInfo)&&iconInfo.value" :class="svgClass"></component>
  </el-icon>
</template>

<style scoped>
.svg-icon {
	width: 1em;
	height: 1em;
	vertical-align: -0.15em;
	fill: currentColor;
	overflow: hidden;
}
</style>
```
这里需要注意的是，因为图标有了分类，所以在使用组件时需要将分类传递过来，所以这里规则是在传递图标时将分类带上，并以`:`分隔，格式：`分类表示:图标名`，这里可以看到`iconify`的图标都是以`v-html`的形式将图标源码展示出来，`iconfont`则是其特有的使用方式`use`形式，而自定义图标集因为动态引入的是文件本身，其只能以`component`的形式进行显示，`iconify`的图标集又因为图标集的制作标准不同，各平台的`viewBox`设置不一样，所以也需要进行判断。

以上图标源数据以及封装的图标组件就全部完成了，而使用也非常简单，与之前使用图标组件唯一的不同就是在`icon-class`赋值时，需要将图标分类传递进去，各个页面中图标使用的修改代码这里不再进行展示，自行修改即可，下面新增一个图标展示页，修改`src/views/FrontEndLibrary/ComponentsLibrary/ReIcon/index.vue`文件：
```
<script lang="ts">
export default {
  name: 'ReIcon'
}
</script>

<script setup lang="ts">
import { ref,reactive } from 'vue'
import {getIconList,iconTypeList} from "@/plugins/svgIcon"
import { cloneDeep } from "lodash-es";
import {randomColor} from "@/utils/theme";
let iconList = getIconList();
let iconTypeData = iconTypeList();
type searchDataType = {
  pageIndex:number,
  pageSize:number,
  type:string,
  name:string,
  fontSize:number,
  isColor:boolean,
}
let searchData = ref<searchDataType>({
  pageIndex:1,
  pageSize:72,
  type:"",
  name:"",
  fontSize:28,
  isColor:false,
})
const getInitData = () => {
  initData.data = [];
  let resultData = cloneDeep(iconList);
  if(searchData.value.type!=''){
    resultData = resultData.filter((item)=>{return item.type==searchData.value.type})
  }
  if(searchData.value.name!=''){
    resultData = resultData.filter((item)=>{return item.key.includes(searchData.value.name)})
  }
  initData.total = resultData.length;
  initData.data = resultData.slice(
    searchData.value.pageSize*(searchData.value.pageIndex-1),
    searchData.value.pageSize*(searchData.value.pageIndex-1) + searchData.value.pageSize
  )
}
let initData:any = reactive({
  data:[],
  total:0,
});
const handleSizeChange = (val: number) => {
  searchData.value.pageSize = val;
  searchData.value.pageIndex = 1;
  getInitData();
}
const handleCurrentChange = (val: number) => {
  searchData.value.pageIndex = val;
  getInitData();
}
getInitData();
const searchChangeHandler = () => {
  searchData.value.pageIndex = 1;
  getInitData();
}
</script>
<template>
  <el-space :size="20" fill direction="vertical">
    <el-card shadow="never" class="no-border no-radius">
      <el-form :inline="true">
        <el-form-item>
          <el-input v-model="searchData.name" placeholder="请输入图标名称" class="input-with-select" clearable style="width:340px" @change="searchChangeHandler">
            <template #prepend>
              <el-select v-model="searchData.type" placeholder="请选择图标集" style="width: 136px" clearable @change="searchChangeHandler">
                <el-option :label="item.label" :value="item.value" v-for="item in iconTypeData" />
              </el-select>
            </template>
            <template #append>
              <el-button icon="Search" @click="searchChangeHandler" />
            </template>
          </el-input>
        </el-form-item>
        <el-form-item><el-input-number v-model="searchData.fontSize" :min="12" :max="40" style="width:120px" /></el-form-item>
        <el-form-item><el-checkbox v-model="searchData.isColor" label="多彩图标" border /></el-form-item>
      </el-form>
      <el-row :gutter="20">
        <el-col :xs="8" :sm="6" :md="3" :lg="2" :xl="2" v-for="item in initData.data">
          <el-card shadow="hover" :title="item.key">
            <svg-icon :icon-class="item.type+':'+item.key" :style="{fontSize:searchData.fontSize+'px',color:searchData.isColor?randomColor():''}"></svg-icon>
          </el-card>
          <span class="icon-text" :title="item.key">{{item.key}}</span>
        </el-col>
      </el-row>
      <el-pagination v-if="initData.total>0" v-model:currentPage="searchData.pageIndex" v-model:page-size="searchData.pageSize" :page-sizes="[72, 144, 216, 288]" :background="true" layout="total, sizes, prev, pager, next, jumper" :total="initData.total" @size-change="handleSizeChange"  @current-change="handleCurrentChange" />
    </el-card>
  </el-space>
</template>

<style scoped lang="scss">

.el-space{
  padding: 20px 20px 0;
  width: 100%;
}
.no-border{
  border:none;
}
.no-radius{
  border-radius: 0;
}
.el-pagination{
  margin-top: 20px;
  justify-content: center;
}
.el-form{
  margin: -6px -6px 14px;
}
.el-row{
  .el-col{
    cursor: pointer;
    .el-card{
      :deep(.el-card__body){
        padding: 15px;
        height: 70px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    }
    .icon-text{
      font-size: var(--el-font-size-extra-small);
      display: block;
      height: 30px;
      line-height: 30px;
      width: 100%;
      overflow: hidden;
      text-align: center;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
  }
}
</style>
```
该页面将图标展示在页面内，方便搜索，查看。到这里图标功能优化就完成了，后期会添加复制功能，并封装一个图标选择组件。