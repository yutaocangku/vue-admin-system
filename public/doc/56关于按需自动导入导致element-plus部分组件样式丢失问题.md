﻿# 按需自动导入element-plus，导致部分组件样式丢失
这里是根据官网的自动导入提示，通过`unplugin-auto-import/vite`、和`unplugin-vue-components/vite`、以及`unplugin-vue-components/resolvers`三个依赖，在`vite.config.ts`文件配置自动导入组件后，有部分组件样式并未自动导入，导致样式丢失，这里样式丢失的组件主要有`ElLoading`、`ElMessage`、`ElMessageBox`、`ElNotification`，在使用了自动导入后，以上4个组件的样式并不能实现自动导入。

官方对此的解释是，将来会解决，但这个回复在21年就已存在，目前为止（2022.7.12）尚未解决，这个只能暂时通过手动导入这4个组件的样式的形式解决该问题，同时因为自动导入，删除了组件的手动导入引起的`eslint`报错问题也需要解决

1. 在配置了自动导入功能后，以上4个组件的手动导入需要删除
```
import { ElLoading } from 'element-plus'
import { ElMessage } from 'element-plus'
import { ElMessageBox } from 'element-plus'
import { ElNotification } from 'element-plus'
```
这种将用到该组件的页面中手动导入组件的相关代码删除

2. 修改`.eslintrc.js`文件，添加以上4个组件的全局类型配置：
```
  // 全局类型定义需要再这里配置，可以无视no-undef规则
  globals: {
    ElLoading: 'writable',
    ElMessage: 'writable',
    ElMessageBox: 'writable',
    ElNotification: 'writable'
  },
```
3. 修改`src/main.ts`文件：
```
import 'element-plus/theme-chalk/src/base.scss'
import 'element-plus/theme-chalk/src/notification.scss'
import 'element-plus/theme-chalk/src/message.scss'
import 'element-plus/theme-chalk/src/message-box.scss'
import 'element-plus/theme-chalk/src/loading.scss'
```
引入以上4个组件的样式文件

以上修改完成后，重新运行项目，可以在`auto-import.d.ts`文件中看到，在进入了使用以上组件的页面后，该文件中会自动生成对应组件的类型声明导入，此时以上4个组件的样式丢失问题就解决了。

将`element-plus`版本更新到目前的最新版本`2.2.9`，此时`el-affix`组件的不能监听自定义滚动条问题，官方依然未解决，还是需要我们自己写的插件来实现，不过本次版本更新后，官方的`el-affix`组件又发生了变化，原来存在的`affix4.js`、`affix4.mjs`已不存在，所以需要将自己写的插件中的文件名进行更改，更改回`affxi2`即可。