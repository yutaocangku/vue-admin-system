﻿
# keep-alive 功能实现
`keep-alive`的作用是在组件切换过程中将状态保留在内存中，防止重复渲染`DOM`减少加载事件及性能消耗，而作为单页面应用项目本身来说，**组件切换过程**也就是路由页面切换的过程。

其通常是与标签栏搭配使用，在有标签栏功能的前提下，页面打开，在标签栏中未关闭，才有保留页面状态的必要，而如果没有标签栏功能，路由跳转后，页面状态不应该被保留，再次回到页面，应该重新加载，而非保留上一次操作的状态。

## 添加keep-alive控制字段
路由页面是否需要`keep-alive`，需要有一个字段进行控制，修改`mock/asyncRoutes.ts`文件的菜单数据：
```
const systemRouter = [
  {id:1,pid:0,title:"首页",icon:"icon-shouye",url:"/Home",redirect:"",showLink:true,sort:0,route:"Home",transitionName:'',keepAlive:true},
  {id:2,pid:0,title:"多级菜单",icon:"icon-daohang",url:"/Nested",redirect:"/Nested/Menu1/Menu1-1",showLink:true,sort:1,route:"Nested",transitionName:'',keepAlive:false},
  {id:3,pid:2,title:"菜单1",icon:"",url:"/Nested/Menu1",redirect:"/Nested/Menu1/Menu1-1",showLink:true,sort:0,route:"Menu1",transitionName:'',keepAlive:false},
  {id:4,pid:3,title:"菜单1-1",icon:"",url:"/Nested/Menu1/Menu1-1",redirect:"",showLink:true,sort:0,route:"Menu1-1",transitionName:'fade-bottom',keepAlive:true},
  {id:5,pid:3,title:"菜单1-2",icon:"",url:"/Nested/Menu1/Menu1-2",redirect:"/Nested/Menu1/Menu1-2/Menu1-2-1",showLink:true,sort:1,route:"Menu1-2",transitionName:'',keepAlive:false},
  {id:6,pid:5,title:"菜单1-2-1",icon:"",url:"/Nested/Menu1/Menu1-2/Menu1-2-1",redirect:"",showLink:true,sort:0,route:"Menu1-2-1",transitionName:'fade-scale',keepAlive:true},
  {id:7,pid:5,title:"菜单1-2-2",icon:"",url:"/Nested/Menu1/Menu1-2/Menu1-2-2",redirect:"",showLink:true,sort:1,route:"Menu1-2-2",transitionName:'fade-top',keepAlive:true},
  {id:8,pid:3,title:"菜单1-3",icon:"",url:"/Nested/Menu1/Menu1-3",redirect:"",showLink:true,sort:3,route:"Menu1-3",transitionName:'zoom-out',keepAlive:true},
  {id:9,pid:2,title:"菜单2",icon:"",url:"/Nested/Menu2",redirect:"",showLink:true,sort:1,route:"Menu2",transitionName:'zoom-fade',keepAlive:true},
  {id:10,pid:0,title:"论坛新闻",icon:"icon-luntan",url:"/News",redirect:"/News/Index",showLink:true,sort:2,route:"News",transitionName:'',keepAlive:false},
  {id:11,pid:0,title:"系统管理",icon:"icon-shezhi",url:"/System",redirect:"/System/UserManagement",showLink:true,sort:3,route:"System",transitionName:'',keepAlive:false},
  {id:12,pid:11,title:"用户管理",icon:"",url:"/System/UserManagement",redirect:"",showLink:true,sort:0,route:"UserManagement",transitionName:'',keepAlive:false},
  {id:13,pid:0,title:"百度一下你就知道答案并不在百度",icon:"icon-sousuo",url:"https://www.baidu.com",redirect:"",showLink:true,sort:10000,route:"",transitionName:'',keepAlive:false},
  {id:14,pid:10,title:"新闻列表",icon:"",url:"/News/Index",redirect:"",showLink:false,sort:0,route:"NewsIndex",transitionName:'fade',keepAlive:false},
]
```
修改`src/router/modules/news.ts`文件，增加`keepAlive`字段：
```
const modulesRouter = [
  {
    path: "/News/NewsDetail/:id",
    name: "NewsDetail",
    component: () => import("@/views/News/NewsDetail/index.vue"),
    meta: {
      title: "详情信息",
      showLink: false,
      pid:10,
      sort:1,
      dynamicLevel: 8,
      realPath: "/News/NewsDetail",
      transitionName:"",
      keepAlive:false
    }
  }
]
export default modulesRouter;
```
修改`src/router/utils.ts`文件中的`addAsyncRoutes`方法：
```

// 过滤后端传来的动态路由 重新生成规范路由，并与静态路由中归属于动态路由的子集数据进行合并
const addAsyncRoutes = (arrRoutes:any,constantNotRootRoutes:any) => {
  if (!arrRoutes || !arrRoutes.length) return;
  const modulesRoutes = import.meta.glob("/src/views/**/**.vue");
  var routerData:any = [];
  arrRoutes.forEach((v:any) => {
    var itemData = {
      path:'',
      name:'',
      redirect:'',
      component:{},
      meta:{
        id:0,
        pid:0,
        icon:'',
        title:'',
        showLink:true,
        sort:0,
        transitionName:'',
        keepAlive:false,
      }
    };
    itemData.path = v.url;
    itemData.name = v.route;
    itemData.component = modulesRoutes[`/src/views${v.url}/index.vue`];
    if (v.redirect!="") {
      itemData.redirect = v.redirect;
    }
    itemData.meta.id = v.id;
    itemData.meta.pid = v.pid;
    itemData.meta.icon = v.icon;
    itemData.meta.title = v.title;
    itemData.meta.showLink = v.showLink;
    itemData.meta.sort = v.sort;
    itemData.meta.transitionName = v.transitionName;
    itemData.meta.keepAlive = v.keepAlive;
    routerData.push(itemData);
  });
  // 合并动态获取路由与静态路由中从属于动态路由子集的数据
  let allRouterData = routerData.concat(...constantNotRootRoutes);
  console.log(allRouterData,"合并后的动态路由");
  return allRouterData;
};
```
字段控制添加完成之后，`keepAlive`为`true`的是可以被缓存的页面，而`keep-alive`若想实现部门页面可缓存，需要通过`include`识别哪些是需要其缓存的，**其识别的标识就是页面的`name`**，也就是说要使用`include`功能，页面必须定义`name`属性，而使用了语法糖`setup`是无法定义页面`name`属性的，所以如果想实现功能，必须给每个页面另外写一个不带语法糖`setup`的`script`标签，并在其内，单独定义页面的`name`属性，页面的`name`值必须与路由的`name`值保持一致，`include`中是通过路由的`name`保存的数据，因此，所有路由都必须有`name`属性，且必须唯一。这里就需要在打开一个路由页面时，通过判断路由页面`keepAlive`的值，决定是否将路由的`name`作为数据，通过`vuex`进行管理。

## 添加缓存页面数据的vuex状态管理
在`src/store/modules`文件夹下新建`keepAlivePages.ts`文件：
```
import { Module } from 'vuex'
import { keepAlivePageType } from "../types";

const keepAliveModule: Module<keepAlivePageType,any> = {
  namespaced: true,
  state: {
    // 缓存页面keepAlive
    keepAliveNames: [],
  },
  mutations:{},
  actions: {
    // 添加或删除某个缓存页面
    keepAliveOperate({state},{ mode, name }) {
      switch (mode) {
        case "add":
          state.keepAliveNames.push(name);
          state.keepAliveNames = [...new Set(state.keepAliveNames)];
          break;
        case "delete":
          // eslint-disable-next-line no-case-declarations
          const delIndex = state.keepAliveNames.findIndex(v => v === name);
          delIndex !== -1 && state.keepAliveNames.splice(delIndex, 1);
          break;
      }
    },
    // 清空缓存页面
    clearAllKeepAlivePage({state}) {
      state.keepAliveNames = [];
    }
  }
};
export default keepAliveModule
```
修改`src/stroe/types.ts`文件，增加对应的类型定义：
```
// 有新增状态管理模块都需要更新这里
export type State = {
  routeMenus: RouteMenus;
  AppState:AppState;
  multiTags: multiTagsType;
  keepAlivePages:keepAlivePageType;
}


// 路由页面状态缓存数据管理
export type keepAlivePageType = {
  keepAliveNames: any;
};
```
然后修改`src/layout/components/appMain.vue`文件：
```
<script setup lang="ts">
import { computed } from "vue";
import { useStore } from 'vuex'
const store = useStore();
const cachePage = computed(()=>{
  return store.state.keepAlivePages.keepAliveNames;
});
</script>

<template>
  <div class="app-root">
    <router-view>
      <template #default="{ Component, route }">
        <transition :name="route.meta?.transitionName&&route.meta?.transitionName!=''?route.meta?.transitionName as string:'fade-slide'" mode="out-in" appear>
          <keep-alive :include="cachePage">
            <component :is="Component" :key="route.fullPath" class="page-root" />
          </keep-alive>
        </transition>
      </template>
    </router-view>
  </div>
</template>

<style lang="scss" scoped></style>
```
## 添加页面name属性
为所有页面添加`name`属性，`src/views`文件夹下所有能在标签页打开的页面加如下相关代码，页面内的`name`值与路由中的`name`必须保持一致，:
```
<script lang="ts">
export default {
  name: 'Home'
}
</script>
```
## 路由切换时对缓存页面进行处理
和操作标签页一样，这里也同样是需要在导航守卫中进行操作，先将相关操作封装成方法，在`src/router/utils.ts`文件中添加封装方法，并将方法暴露：
```
import { useTimeoutFn } from "@vueuse/core";

// 处理缓存路由（添加、删除、刷新）
const handleAliveRoute = (matched: RouteRecordNormalized[], mode?: string) => {
  switch (mode) {
    case "add":
      matched.forEach(v => {
        store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "add", name: v.name });
      });
      break;
    case "delete":
      store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "delete", name: matched[matched.length - 1].name });
      break;
    default:
      store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "delete", name: matched[matched.length - 1].name });
      // 这里需要设置一个比较小的时间差，时间差太长会造成缓存页面还未添加上，页面已实例化，导致onActivated生命周期不能执行
      useTimeoutFn(() => {
        matched.forEach(v => {
          store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "add", name: v.name });
        });
      }, 10);
  }
};

// 批量删除缓存路由(keepalive)
const delAliveRoutes = (delAliveRouteList: Array<tagRouteConfigs>) => {
  delAliveRouteList.forEach(route => {
    store.dispatch('keepAlivePages/keepAliveOperate',{ mode: "delete", name: route?.name });
  });
};


export {
  constantRoutesFilter,
  treeToAscending,
  filterMenuTree,
  initRouter,
  dynamicRouteTag,
  handleAliveRoute,
  delAliveRoutes
};

```
然后修改`src/router/index.ts`文件：
```
import {constantRoutesFilter,initRouter,dynamicRouteTag,handleAliveRoute} from "./utils";


router.beforeEach((to, _from, next) => {
  if (to.meta?.keepAlive) {
    const newMatched = to.matched;
    handleAliveRoute(newMatched, "add");
    // 页面整体刷新和点击标签页刷新
    if (_from.name === undefined || _from.name === "redirect") {
      handleAliveRoute(newMatched);
    }
  }
  const token = getToken();
  NProgress.start();
  const externalLink = to?.redirectedFrom?.fullPath;
  if(token){
    if(_from?.name==="Login"||!_from?.name){
      // 如果是从登录页过来的，或者name不存在（说明是浏览器刷新事件），则需要请求路由数据，再执行下一步
      if (store.state.routeMenus.routeMenus.length === 0){
        initRouter(constantNotRootRoutes).then(() => {
          router.push(to.path);
          dynamicRouteTag(to);
        });
      }
      next();
    }else{
      // 如果是其他页面跳转的，且其他页面的name存在，则再判断当前跳转页是否是外链页面
      if(externalLink&&externalLink.includes("http")){
        openLink(`http${split(externalLink, "http")[1]}`);
        NProgress.done();
      }else{
        dynamicRouteTag(to);
        next();
      }
    }
  }else{// 未登录
    // 当前访问页面不是登陆页面
    if (to.path !== "/login") {
      // 当前访问页面包含在路由白名单中，可直接跳转
      if (whiteList.includes(to.path)) {
        next();
      } else {
        // 当前访问页面需要登录才能访问，需先跳转到登陆页，并将当前访问路由通过链接保存，在登陆后，获取该路由进行跳转
        next(`/login?redirect=${to.path}`);
      }
    } else {
      // 当前访问页面本身就是登录页，直接跳转
      next();
    }
  }
});
```
此时运行项目，在首页做一些操作，选择一个日期，切换页面后，再回到首页，操作的状态被保留了下来，那么这个功能就算完成了
## 删除页面状态缓存
添加缓存页面完成，删除页面缓存功能是配合标签页来实现，页面打开状态保留，但是当页面被关闭时，也应该将保留状态清除，项目到这里为止，是默认有标签栏功能，默认缓存所有标签路由，在这种情况下， 除了刷新后`vuex`中的数据自动清空了外，在操作标签栏关闭标签时，也需要将标签对应的路由页面的缓存给删除。在做添加操作时，已经封装了删除页面缓存方法，以及清空所有缓存的`action`，这里需要在`src/layout/components/tagsView/index.vue`文件中的删除标签页相关操作的地方，调用删除缓存页面的方法:
```
import { handleAliveRoute, delAliveRoutes } from "@/router/utils";



// 删除动态标签页
function deleteDynamicTag(obj: any, current: any, mode?: string) {
  // 存放被删除的缓存路由
  let delAliveRouteList:any = [];
  // 获取当前所在标签页在标签页缓存数据中的索引值，通过索引值以及删除类行，对数据进行删除操作
  let valueIndex: number = multiTags.value.findIndex((item: any) => {
    return item.path === obj.path;
  });
  // 对标签页缓存数据进行操作的封装方法
  const spliceRoute = (startIndex?: number, length?: number, other?: boolean): void => {
    if (other) {
      // 调用标签页数据的状态管理，将首页和当前标签页作为数据传递
      store.dispatch("multiTags/handleTags",{mode:"equal",value: [
        {
          path: "/Home",
          name:"Home",
          meta: {
            title: "首页",
            icon: "icon-shouye",
            showLink: true
          }
        },
        obj
      ]});
    }else{
      // 当不是关闭其他标签页事件时，通过索引去修改标签页缓存数据
      delAliveRouteList = store.dispatch("multiTags/handleTags",{mode:"splice",value:'',position:{
        startIndex,
        length
      }});
    }
  };
  // 根据当前mode类型，执行标签页缓存数据操作方法
  if (mode === "other") {// 删除其他路由
    spliceRoute(1, 1, true);
  } else if (mode === "left") {// 删除左侧路由
    spliceRoute(1, valueIndex - 1);
  } else if (mode === "right") {// 删除右侧路由
    spliceRoute(valueIndex + 1, multiTags.value.length);
  } else {// 删除当前路由
    // 从当前匹配到的路径中删除
    spliceRoute(valueIndex, 1);
  }
  // 在标签页缓存数据做了删除处理后，还需要提取数据中最后一个路由信息，用于做当前展示路由页面
  store.dispatch("multiTags/handleTags",{mode:"slice"}).then(res=>{
    // 删除缓存路由
    mode ? delAliveRoutes(delAliveRouteList) : handleAliveRoute(route.matched, "delete");
    // 当前路由页面是点击事件依据的标签路由（右键点击的标签路由或是下拉框事件时的当前路由）
    if (current === route.path) {
      // 删除的是当前路由左侧的标签路由，此时当前路由不会发生变化
      if (mode === "left") return;
      // 不是删除左侧路由的情况，右侧删除，当前路由会变成最后一个；而全部、当前这些情况发生后，会发生当前路由被删除，所以直接跳转最后一个标签路标页面
      nextTick(() => {
        router.push({path: res[0].path});
      });
    } else {
      // 删除缓存路由
      mode ? delAliveRoutes(delAliveRouteList) : delAliveRoutes([obj]);
      // 这里主要针对右键菜单不是在当前路由上激活，会发生将当前路由删除的情况
      // 如果标签页缓存数据不存在了，则直接返回
      if (!multiTags.value.length) return;
      // 否则将当前激活的路由找出
      let isHasActiveTag = multiTags.value.some(item => {
        return item.path === route.path;
      });
      // 如果当前激活的路由不存在（被删除）则跳转到最后一个缓存的标签页
      !isHasActiveTag && router.push({ path: res[0].path});
    }
  });
}


// 标签页的点击事件，右键及右侧下拉的点击事件
function onClickDrop(key, item, selectRoute?: tagRouteConfigs) {
  // 当前点击的是禁用状态，直接退出
  if (item && item.disabled) return;
  // 当前路由信息
  switch (key) {
    case 0:
      // 重新加载
      onFresh();
      break;
    case 1:
      // 关闭当前标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu({
            path: selectRoute.path,
            meta: selectRoute.meta,
            name: selectRoute.name
          })
        : deleteMenu({ path: route.path, meta: route.meta });
      break;
    case 2:
      // 关闭左侧标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu(
            {
              path: selectRoute.path,
              meta: selectRoute.meta
            },
            "left"
          )
        : deleteMenu({ path: route.path, meta: route.meta }, "left");
      break;
    case 3:
      // 关闭右侧标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu(
            {
              path: selectRoute.path,
              meta: selectRoute.meta
            },
            "right"
          )
        : deleteMenu({ path: route.path, meta: route.meta }, "right");
      break;
    case 4:
      // 关闭其他标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu(
            {
              path: selectRoute.path,
              meta: selectRoute.meta
            },
            "other"
          )
        : deleteMenu({ path: route.path, meta: route.meta }, "other");
      break;
    case 5:
      // 关闭全部标签页，这里不需要判断是右键还是下拉的点击事件，关闭全部，只会留默认页展示，直接通过删除从索引1到最后，即只留下第一个默认页
      store.dispatch("multiTags/handleTags",{mode:"splice",value: '',position:{
        startIndex: 1,
        length: multiTags.value.length
      }});
      // 清空缓存路由
      store.dispatch("keepAlivePages/clearAllKeepAlivePage");
      router.push("/");
      break;
  }
  setTimeout(() => {
    // 重新渲染点击事件的状态
    showMenuModel(route.fullPath);
  },100);
}
```
以上两个方法中都会有需要删除页面缓存的地方，代码修改后，页面状态缓存的添加和删除功能就完整了，删除功能暂时无法做测试，还有遗留问题未解决，下一章会解决该问题