﻿
# keep-alive遗留问题
上一章实现了`keep-alive`页面状态缓存功能，但此时的状态缓存还存在问题，给以下页面添加一些可操作的代码：

1. 修改`src/views/Nested/Menu1/Menu1-1/index.vue`文件代码：
```
<script lang="ts">
export default {
  name: 'Menu1-1'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="menu1-1-page">
    <h2>Menu1-1</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
2. 修改`src/views/Nested/Menu1/Menu1-2/Menu1-2-1/index.vue`文件代码：
```
<script lang="ts">
export default {
  name: 'Menu1-2-1'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="menu1-2-1-page">
    <h2>Menu1-2-1</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
3. 修改`src/views/Nested/Menu2/index.vue`文件代码：
```
<script lang="ts">
export default {
  name: 'Menu2'
}
</script>

<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="menu2-page">
    <h2>Menu2</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
4. 修改`src/views/System/UserManagement/index.vue`文件代码：
```
<script lang="ts">
export default {
  name: 'UserManagement'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="user-management-page">
    <h2>用户管理</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
以上页面代码修改后，都有了可操作的控件，其中部分页面的`keepAlive`值是为`true`的，但当操作这些页面，再进行回跳，页面状态并未被保留，这是因为**超过三级的路由层级，其页面状态无法被缓存**。而关于这个问题的解决方案也是各种各样，但不管哪种方案，都绕不开的问题是：
1. 空页面问题
2. 面包屑层级展示问题

# 解决方案
需要实现的功能：
1. 页面状态缓存实现全覆盖
2. 清除所有空页面
3. 面包屑层级不能变
4. 菜单层级不不能变

实现以上功能，这里选择将路由层级拍平处理，这里要注意的是**路由的path必须全部以/开头**，嵌套路由的`path`不要写成可继承父级路由的形式。

## 实现页面状态缓存全覆盖
这需要对动态、静态路由都进行处理，先在`src/router/utils.ts`文件中做修改，新增一个将树形结构处理成一维数组的封装方法：
```
/**
 * 将树形结构路由处理成一维数组
 * @param routesList 传入路由
 * @returns 返回处理后的一维路由
 */
const routerTreeToFlattening = (routesList: RouteRecordRaw[]) => {
  if (routesList.length === 0) return routesList;
  let newRouteList = [];
  function flatteningRoute(routesList){
    routesList.forEach(function(item){
        let newItem = cloneDeep(item);
        newItem.children = [];
        newRouteList.push(newItem);
        if(item.children&&item.children.length>0){
            flatteningRoute(item.children)
        };
    });
  }
  flatteningRoute(routesList);
  console.log(newRouteList,"拍平的路由")
  return newRouteList;
}
```
然后对静态路由处理、动态路由转为树形结构、路由初始化的封装方法进行修改：
```
import { cloneDeep } from "lodash-es";


// 处理静态路由
const constantRoutesFilter = (data:any,isAsync:boolean,isMenu:boolean) => {
  let newData = [];
  if(isAsync){
    // 需要的是动态路由子集的部分
    newData = data.filter((item:any)=>{
      return item.meta.pid != 0;
    })
  }else{
    if(isMenu){
      // 作为菜单数据使用，不需要根路由Layout
      newData = data.filter((item:any)=>{
        return item.meta.pid == 0;
      });
    }else{
      // 需要的是可直接实例化的静态路由部分
      newData.push({
        path: "/",
        name: "Layout",
        component: Layout,
        redirect: "/Home",
        children:[],
        meta:{
          sort:1,
          showLink:true,
        }
      });
      let sonData = data.filter((item:any)=>{
        return item.meta.pid == 0;
      });
      newData[0].children = routerTreeToFlattening(sonData);
    }
  }
  return newData;
}


// 将规范路由的一维数组转为树形结构
const routerToTree = (arrRoutes:any) => {
  var parents = arrRoutes.filter(function (item:any) {
      return item.meta.pid == 0;
  });
  var children = arrRoutes.filter(function (item:any) {
      return item.meta.pid != 0;
  });
  // 递归处理动态路由层级
  convert(parents, children);
  return parents;
}

// 初始化路由
const initRouter = (constantNotRootRoutes:any) => {
  return new Promise(resolve => {
    // 调用接口获取动态路由
    getAsyncRoutes().then((data:any) => {
      if (data.data.length > 0) {
        // 获取的动态路由处理成规范路由后，通过map方法，循环加入到路由实例中
        routerTreeToFlattening(routerToTree(addAsyncRoutes(data.data,constantNotRootRoutes)))?.map((v: any) => {
          if(!router.options.routes[0].children){
            router.options.routes[0].children = [];
          }
          // 防止重复添加路由
          if (router.options.routes[0].children.findIndex(value => value.path === v.path) !== -1) {
            return;
          } else {
            // 切记将路由push到routes后还需要使用addRoute，这样路由才能正常跳转
            router.options.routes[0].children.push(v);
            // 这里需要注意 addRouter中的Layout参数，是用来将路由添加到框架层路由下的，如果动态路由有非框架层路由下的，这里需要做判断
            if (!router.hasRoute(v?.name)) router.addRoute('Layout',v);
          }
          resolve(router);
        });
      }
      // 将转化为树形结构的路由数据传递给vuex去处理成菜单数据
      store.dispatch('routeMenus/getWholeRouteMenus',cloneDeep(routerToTree(addAsyncRoutes(data.data,constantNotRootRoutes))));
      console.log(router.options,"最终路由数据");
      // 404匹配规则需在最后加入到路由实例中
      router.addRoute({path: "/:pathMatch(.*)",redirect: "/Error/404"});
    });
  });
};
```
这里路由在被拍平后，可以将`src/views`中那些为了路由嵌套而创建的空页面全部删除了，此时运行项目，打开`用户管理`页面，打开`菜单1-2-1`页面，页面能正常显示，并分别进行日期选择操作，标签栏上再对页面进行切换，可以发现`用户管理`页面未缓存，`菜单1-2-1`可进行缓存，`用户管理`未缓存是因为设置了`meta.keepAlive:false`，该页面不用缓存，而`菜单1-2-1`在之前是比较深的层级，允许缓存状态，但在之前未能缓存，现在已经可以进行缓存，说明已经实现页面状态缓存全覆盖了。

此时不仅已经实现页面状态缓存全覆盖，同时清除了多余的空页面，菜单的层级展现也未变，只有面包屑的层级展现，现在是有问题的，接下来就需要解决面包屑层级展现的问题

## 实现面包屑层级展现
实现面包屑层级展现，这里是在给动态路由做树形结构处理时，将每个路由的层级数据处理成数组，存放在路由的`meta.breadcrumb`字段中，面包屑的渲染则直接获取当前路由的`meta.breadcrumb`进行渲染即可，这里需要注意的是，动态获取的菜单数据本身是一维数组，其本身需要转化为树形结构供菜单使用，而在这个过程中，处理面包屑数据后，供路由使用需要另外复制一份处理好面包屑的数据，再次转为一维数组供路由使用，而静态路由数据，本身前文规定的直接写为树形结构，所以这里需要对静态路由先做拍平处理，再走处理树形结构过程，方便添加面包屑数据，然后再次拍平数据，无论是可直接实例化的静态路由部分，还是静态路由中归属动态路由子集的部分，都需要做这种处理。

修改`src/router/utils.ts`文件中的路由转为树形结构的递归方法：
```
// 处理静态路由
const constantRoutesFilter = (data:any,isAsync:boolean,isMenu:boolean) => {
  let newData = [];
  if(isAsync){
    // 需要的是动态路由子集的部分
    newData = routerTreeToFlattening(data.filter((item:any)=>{
      return item.meta.pid != 0;
    }))
  }else{
    if(isMenu){
      // 作为菜单数据使用，不需要根路由Layout
      newData = data.filter((item:any)=>{
        return item.meta.pid == 0;
      });
    }else{
      // 需要的是可直接实例化的静态路由部分
      newData.push({
        path: "/",
        name: "Layout",
        component: Layout,
        redirect: "/Home",
        children:[],
        meta:{
          sort:1,
          showLink:true,
        }
      });
      let sonData = data.filter((item:any)=>{
        return item.meta.pid == 0;
      });
      newData[0].children = routerTreeToFlattening(routerToTree(routerTreeToFlattening(sonData)));
    }
  }
  return newData;
}


// 递归处理动态路由层级
const convert = (parents:any,children:any) =>{
  parents.forEach(function (item:any) {
    if(!item.meta.breadcrumb){
        item.meta.breadcrumb = [];
        var itemBreadcrumb = {
            path:"",
            title:"",
            redirect:""
        };
        itemBreadcrumb.path = item.path;
        itemBreadcrumb.redirect = item.redirect!=''?item.redirect:'';
        itemBreadcrumb.title = item.meta.title as string;
        if(item.path!=='/Home'){
            item.meta.breadcrumb.push({path:"/Home",title:"首页",redirect:""});
        }
        item.meta.breadcrumb.push(itemBreadcrumb);
    }
    item.children = [];
    children.forEach(function (current:any, index:any) {
      if (current.meta.pid === item.meta.id) {
        current.meta.breadcrumb = cloneDeep(item.meta.breadcrumb);
        var childrenItemBreadcrumb = {
            path:"",
            title:"",
            redirect:""
        };
        childrenItemBreadcrumb.path = current.path;
        childrenItemBreadcrumb.title = current.meta.title as string;
        childrenItemBreadcrumb.redirect = current.redirect!=''?current.redirect:'';
        current.meta.breadcrumb.push(childrenItemBreadcrumb);
        var temp = children.filter(function(v:any,idx:any){
          return idx != index;
        }); // 删除已匹配项，这里未使用JSON.parse(JSON.stringify(children))是因为路由的component已经是函数形式，再格式化后，模块导入功能丢失，会导致找不到模块
        item.children.push(current);
        convert([current], temp); // 递归
      }
    });
  });
}
```
此时运行项目查看打印的路由数据，可以看到在拍平的路由数据中，每一个路由的`meta`中，都多了一个`breadcrumb`字段的数组数据，也是该路由的面包屑层级展示数据，接下来修改`src/layout/components/beadCrumb/index.vue`文件：
```
<script setup lang="ts">
import { useRoute } from "vue-router";
const route = useRoute();
</script>

<template>
  <el-breadcrumb class="app-breadcrumb" separator="/">
    <transition-group appear name="breadcrumb">
      <el-breadcrumb-item v-for="(item, index) in (route.meta.breadcrumb as any)" :key="item.path">
        <span v-if="index == (route.meta.breadcrumb as any).length - 1" class="no-redirect">{{ item.title }}</span>
        <router-link :to="item.redirect&&item.redirect!==''?item.redirect:item.path" v-else> {{ item.title }}</router-link>
      </el-breadcrumb-item>
    </transition-group>
  </el-breadcrumb>
</template>

<style lang="scss" scoped></style>
```
此时运行项目，打开路由页面，面包屑的层级显示就正确了，各路由页面的页面状态缓存也是可以正常缓存的，也不需要因为路由层级原因，而要创建很多的空页面，菜单的层级显示也不存在问题，此时关于`keep-alive`、路由、面包屑冲突的问题就完美解决了。

# 添加静态路由页面
此时项目本身还没有静态路由做测试，可以先创建一个静态路由结构及其对应的文件结构：
1. 在`src/router/modules`文件夹下新建`frontEndLibrary.ts`文件：
```
const modulesRouter = [
    {
      path: "/FrontEndLibrary",
      name: "FrontEndLibrary",
      redirect:"/FrontEndLibrary/ComponentsLibrary/ReIcon",
      meta: {
        id:1000000,
        title: "前端库",
        showLink: true,
        icon:"icon-front-end",
        pid:0,
        sort:100000,
        keepAlive:true,
        transitionName:""
      },
      children:[
        {
            path: "/FrontEndLibrary/ComponentsLibrary",
            name: "ComponentsLibrary",
            redirect:"/FrontEndLibrary/ComponentsLibrary/ReIcon",
            meta: {
              id:1000001,
              title: "组件库",
              showLink: true,
              pid:1000000,
              sort:1,
              keepAlive:true,
              transitionName:""
            },
            children:[
                {
                    path: "/FrontEndLibrary/ComponentsLibrary/ReIcon",
                    name: "ReIcon",
                    component: () => import("@/views/FrontEndLibrary/ComponentsLibrary/ReIcon/index.vue"),
                    meta: {
                      title: "图标组件",
                      showLink: true,
                      pid:1000001,
                      sort:1,
                      keepAlive:true,
                      transitionName:""
                    },
                    children:[]
                },
                {
                    path: "/FrontEndLibrary/ComponentsLibrary/ReDrag",
                    name: "ReDrag",
                    component: () => import("@/views/FrontEndLibrary/ComponentsLibrary/ReDrag/index.vue"),
                    meta: {
                      title: "拖拽组件",
                      showLink: true,
                      pid:1000001,
                      sort:1,
                      keepAlive:true,
                      transitionName:""
                    },
                    children:[]
                }
            ]
        },
        {
            path: "/FrontEndLibrary/ViewsLibrary",
            name: "ViewsLibrary",
            redirect:"/FrontEndLibrary/ViewsLibrary/ReDemo",
            meta: {
                id:1000002,
              title: "视图库",
              showLink: true,
              pid:1000000,
              sort:1,
              keepAlive:true,
              transitionName:""
            },
            children:[
                {
                    path: "/FrontEndLibrary/ViewsLibrary/ReDemo",
                    name: "ReDemo",
                    component: () => import("@/views/FrontEndLibrary/ViewsLibrary/ReDemo/index.vue"),
                    meta: {
                      title: "测试视图",
                      showLink: true,
                      pid:1000002,
                      sort:1,
                      keepAlive:true,
                      transitionName:""
                    },
                    children:[]
                },
                {
                    path: "/FrontEndLibrary/ViewsLibrary/ReEcharts",
                    name: "ReEcharts",
                    component: () => import("@/views/FrontEndLibrary/ViewsLibrary/ReEcharts/index.vue"),
                    meta: {
                      title: "echarts图表视图",
                      showLink: true,
                      pid:1000002,
                      sort:1,
                      keepAlive:true,
                      transitionName:""
                    },
                    children:[]
                },
            ]
        }
      ]
    }
  ]
  export default modulesRouter;
```
静态路由这里并没有直接写死`breadcrumb`面包屑数据，这个直接让代码去实现即可，这里需要注意的是，静态路由也加了`id`和`pid`，可以将`id`的取值无限放大，防止静态路由`id`占用动态路由`id`造成冲突。

2. 创建对应的路由页面文件：

新建`src/views/FrontEndLibrary/ComponentsLibrary/ReDrag/index.vue`路径文件：
```
<script lang="ts">
export default {
  name: 'ReDrag'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="re-drag-page">
    <h2>拖拽组件</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
新建`src/views/FrontEndLibrary/ComponentsLibrary/ReIcon/index.vue`路径文件：
```
<script lang="ts">
export default {
  name: 'ReIcon'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="re-icon-page">
    <h2>图标组件</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
新建`src/views/FrontEndLibrary/ViewsLibrary/ReDemo/index.vue`路径文件：
```
<script lang="ts">
export default {
  name: 'ReDemo'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="re-demo-page">
    <h2>测试专用页面</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
新建`src/views/FrontEndLibrary/ViewsLibrary/ReEcharts/index.vue`路径文件：
```
<script lang="ts">
export default {
  name: 'ReEcharts'
}
</script>
<script setup lang="ts">
import { ref } from 'vue'
const value1 = ref("")
</script>
<template>
  <div class="re-echarts-page">
    <h2>echarts表视图</h2>
    <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  </div>
</template>

<style>
</style>
```
以上文件路径创建好后，重新运行项目，静态路由页面部分，也可以正常显示，页面状态缓存也可以正常缓存，对应面包屑也正常显示，问题解决无漏洞。

# 关闭标签页清空页面状态缓存
在上一章加了关闭标签后，清除关闭页面的状态缓存功能，在当时因为缓存还存在问题，未做测试，此时可以做一下测试看看，可以先把关闭全部标签中的清空页面缓存给注释掉，修改`src/layout/components/tagsView/index.vue`文件中的`onClickDrop`方法：
```

// 标签页的点击事件，右键及右侧下拉的点击事件
function onClickDrop(key, item, selectRoute?: tagRouteConfigs) {
  // 当前点击的是禁用状态，直接退出
  if (item && item.disabled) return;
  // 当前路由信息
  switch (key) {
    case 0:
      // 重新加载
      onFresh();
      break;
    case 1:
      // 关闭当前标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu({
            path: selectRoute.path,
            meta: selectRoute.meta,
            name: selectRoute.name
          })
        : deleteMenu({ path: route.path, meta: route.meta });
      break;
    case 2:
      // 关闭左侧标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu(
            {
              path: selectRoute.path,
              meta: selectRoute.meta
            },
            "left"
          )
        : deleteMenu({ path: route.path, meta: route.meta }, "left");
      break;
    case 3:
      // 关闭右侧标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu(
            {
              path: selectRoute.path,
              meta: selectRoute.meta
            },
            "right"
          )
        : deleteMenu({ path: route.path, meta: route.meta }, "right");
      break;
    case 4:
      // 关闭其他标签页，根据第三个参数是否存在，判断是下拉框或是右键菜单的点击事件
      selectRoute
        ? deleteMenu(
            {
              path: selectRoute.path,
              meta: selectRoute.meta
            },
            "other"
          )
        : deleteMenu({ path: route.path, meta: route.meta }, "other");
      break;
    case 5:
      // 关闭全部标签页，这里不需要判断是右键还是下拉的点击事件，关闭全部，只会留默认页展示，直接通过删除从索引1到最后，即只留下第一个默认页
      store.dispatch("multiTags/handleTags",{mode:"splice",value: '',position:{
        startIndex: 1,
        length: multiTags.value.length
      }});
      // 清空缓存路由
    //   store.dispatch("keepAlivePages/clearAllKeepAlivePage");
      router.push("/");
      break;
  }
  setTimeout(() => {
    // 重新渲染点击事件的状态
    showMenuModel(route.fullPath);
  },100);
}
```
这里将`case 5`中的清空缓存路由注释掉，运行项目，打开`菜单1-1`和`菜单1-2-1`页面，分别做日期选择操作后，执行**关闭全部标签页**命令，然后再次通过侧边栏打开这两个页面，可以发现页面的操作状态依然存在，正常情况应该为关闭页面后，再重新打开，页面需要重新加载，而非保留上次关闭的状态；只有页面是打开状态，再次回到该页面，才应该有保留状态的需要，所以页面状态缓存一般都是与标签页功能配合使用，标签页关闭后，对应页面的状态缓存也应该清除，再将上面注释掉的清空缓存路由撤销注释，重复上面的测试，操作后打开的页面相互切换，页面缓存状态是存在，但在页面关闭后，从侧边菜单栏重新打开，页面状态缓存是没有再保留的。

