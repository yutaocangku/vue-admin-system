﻿# 集成svg图标组件

## 图标库选择
舍弃字体图标的原因：
1. 各大框架均已将自己的图标库转移成`svg`图标，最新版`element-plus`也已经开始使用`svg`图标，字体图标的引入方式已被舍弃
2. 字体图标在生产环境打包过程中，有一定几率会被打包成乱码，造成项目的所有图标乱码

使用哪些已有的`svg`图标库？
1. `element-plus`的图标库
2. 阿里巴巴`iconfont`图标库

本项目只会将以上两种图标库封装进项目，其它如`fontawesome`、`remixion`等图标库也有丰富的`svg`图标，但对于一个项目来说，图标的UI一致性也是需要考虑的问题，封装太多图标库进项目，样式统一性是问题，选择也是问题。

`element-plus`的图标只加载其中比较公用的一部分，选择`iconfont`图标，则是因为其更为灵活，可自己收集图标组成项目下载使用，可进行高度自定义，本项目会以引入`iconfont.js`的方式将其图标封装进图标组件中。

## 按需导入`element-plus`的图标
```
yarn add @element-plus/icons-vue
```
和按需加载`element-plus`组件的操作一样，这里也需要将需要的`element-plus`图标导入，然后在全局注册，修改`src/plugins/element-plus/index.ts`文件：
```
import { App } from "vue";
// 引入element-plus的组件
import {
  ElTag,
  ElAffix,
  ElSkeleton,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElScrollbar,
  ElSubMenu,
  ElButton,
  ElCol,
  ElRow,
  ElSpace,
  ElDivider,
  ElCard,
  ElDropdown,
  ElDialog,
  ElMenu,
  ElMenuItem,
  ElDropdownItem,
  ElDropdownMenu,
  ElIcon,
  ElInput,
  ElDatePicker,
  ElForm,
  ElFormItem,
  ElLoading,
  ElPopover,
  ElPopper,
  ElTooltip,
  ElDrawer,
  ElPagination,
  ElAlert,
  ElRadio,
  ElRadioButton,
  ElRadioGroup,
  ElDescriptions,
  ElDescriptionsItem,
  ElBacktop,
  ElSwitch,
  ElBadge,
  ElTabs,
  ElTabPane,
  ElAvatar,
  ElEmpty,
  ElCollapse,
  ElCollapseItem,
  ElInfiniteScroll
} from "element-plus";

// 声明组件名用来接收引入的element-plus，两者需要一一对应，使用起来才能像直接使用element-plus组件一样
const components:any[] = [
  ElTag,
  ElAffix,
  ElSkeleton,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElScrollbar,
  ElSubMenu,
  ElButton,
  ElCol,
  ElRow,
  ElSpace,
  ElDivider,
  ElCard,
  ElDropdown,
  ElDialog,
  ElMenu,
  ElMenuItem,
  ElDropdownItem,
  ElDropdownMenu,
  ElIcon,
  ElInput,
  ElDatePicker,
  ElForm,
  ElFormItem,
  ElPopover,
  ElPopper,
  ElTooltip,
  ElDrawer,
  ElPagination,
  ElAlert,
  ElRadio,
  ElRadioButton,
  ElRadioGroup,
  ElDescriptions,
  ElDescriptionsItem,
  ElBacktop,
  ElSwitch,
  ElBadge,
  ElTabs,
  ElTabPane,
  ElAvatar,
  ElEmpty,
  ElCollapse,
  ElCollapseItem
];
const plugins = [ElLoading,ElInfiniteScroll];
// 引入element-plus图标
import {
  Aim,
  AlarmClock,
  ArrowDownBold,
  ArrowDown,
  ArrowLeftBold,
  ArrowLeft,
  ArrowRightBold,
  ArrowRight,
  ArrowUpBold,
  ArrowUp,
  Avatar,
  Back,
  BellFilled,
  Bell,
  Bottom,
  BrushFilled,
  Brush,
  Calendar,
  CaretBottom,
  CaretLeft,
  CaretRight,
  CaretTop,
  ChatDotRound,
  ChatDotSquare,
  ChatLineRound,
  ChatLineSquare,
  Check,
  CircleCheckFilled,
  CircleCheck,
  CircleCloseFilled,
  CircleClose,
  CirclePlusFilled,
  CirclePlus,
  Clock,
  CloseBold,
  Close,
  Comment,
  Connection,
  CopyDocument,
  Crop,
  DArrowLeft,
  DArrowRight,
  DCaret,
  DeleteFilled,
  Delete,
  Download,
  Edit,
  Expand,
  Filter,
  Fold,
  FullScreen,
  Histogram,
  HomeFilled,
  InfoFilled,
  Iphone,
  Link,
  Loading,
  Lock,
  Message,
  Minus,
  Monitor,
  Moon,
  MoreFilled,
  More,
  Operation,
  Opportunity,
  PictureFilled,
  PictureRounded,
  Picture,
  PieChart,
  Plus,
  Position,
  QuestionFilled,
  Rank,
  RefreshLeft,
  RefreshRight,
  Refresh,
  RemoveFilled,
  Remove,
  Right,
  Search,
  Select,
  SemiSelect,
  Setting,
  Sort,
  StarFilled,
  Star,
  SuccessFilled,
  Sunny,
  SwitchButton,
  Switch,
  Tools,
  Top,
  Unlock,
  UploadFilled,
  Upload,
  UserFilled,
  User,
  WarningFilled,
  Warning,
  ZoomIn,
  ZoomOut,
} from "@element-plus/icons-vue";
// 声明图标组件名
export const iconComponents = [
  Aim,
  AlarmClock,
  ArrowDownBold,
  ArrowDown,
  ArrowLeftBold,
  ArrowLeft,
  ArrowRightBold,
  ArrowRight,
  ArrowUpBold,
  ArrowUp,
  Avatar,
  Back,
  BellFilled,
  Bell,
  Bottom,
  BrushFilled,
  Brush,
  Calendar,
  CaretBottom,
  CaretLeft,
  CaretRight,
  CaretTop,
  ChatDotRound,
  ChatDotSquare,
  ChatLineRound,
  ChatLineSquare,
  Check,
  CircleCheckFilled,
  CircleCheck,
  CircleCloseFilled,
  CircleClose,
  CirclePlusFilled,
  CirclePlus,
  Clock,
  CloseBold,
  Close,
  Comment,
  Connection,
  CopyDocument,
  Crop,
  DArrowLeft,
  DArrowRight,
  DCaret,
  DeleteFilled,
  Delete,
  Download,
  Edit,
  Expand,
  Filter,
  Fold,
  FullScreen,
  Histogram,
  HomeFilled,
  InfoFilled,
  Iphone,
  Link,
  Loading,
  Lock,
  Message,
  Minus,
  Monitor,
  Moon,
  MoreFilled,
  More,
  Operation,
  Opportunity,
  PictureFilled,
  PictureRounded,
  Picture,
  PieChart,
  Plus,
  Position,
  QuestionFilled,
  Rank,
  RefreshLeft,
  RefreshRight,
  Refresh,
  RemoveFilled,
  Remove,
  Right,
  Search,
  Select,
  SemiSelect,
  Setting,
  Sort,
  StarFilled,
  Star,
  SuccessFilled,
  Sunny,
  SwitchButton,
  Switch,
  Tools,
  Top,
  Unlock,
  UploadFilled,
  Upload,
  UserFilled,
  User,
  WarningFilled,
  Warning,
  ZoomIn,
  ZoomOut,
];
export function useElementPlus(app: App) {
  // 注册组件
  components.forEach((component: any) => {
    app.component(component.name, component);
  });
  // 注册指令
  plugins.forEach(plugin => {
    app.use(plugin);
  });
  // 注册图标
  iconComponents.forEach((component: any) => {
    app.component(component.name, component);
  });
}
```
此时只需在需要用图标的地方以组件的形式调用图标名，就可以将`element-plus`的图标显示出来了，修改`src/App.vue`文件：
```
<script setup lang="ts">
// This starter template is using Vue 3 <script setup> SFCs
// Check out https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup
import HelloWorld from '@/components/HelloWorld.vue';
import { ref } from 'vue'
const value1 = ref("")
</script>

<template>
  <img alt="Vue logo" src="@/assets/logo.png" />
  <HelloWorld msg="Hello Vue 3 + TypeScript + Vite" />
  <p class="demo">测试</p>
  <el-date-picker v-model="value1" type="date" placeholder="Pick a day"></el-date-picker>
  <div class="icon-list">
    <span class="item-icon"><edit></edit></span>
  </div>
  
</template>

<style lang="scss">
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: $primary;
  margin-top: 60px;
}
.icon-list{
  width: 100%;
  text-align: center;
  .item-icon{
    display: inline-block;
    width: 20px;
    height: 20px;
    margin: 5px;
    font-size: 16px;
    color: $pink;
    svg{
      width:100%;
      height: 100%;
      fill: currentColor;
    }
  }
}
</style>
```
运行项目，图标已能在页面内正常显示

## 引入iconfont图标

可自行在`iconfont`官网收集一些图标，新建项目，添加至项目，并将项目图标下载至本地，将其中的`iconfont.js`文件，存放到`src/assets/iconfont`文件夹下，并在`src/main.ts`文件中引入该`js`文件：
```
import { createApp } from 'vue'
import App from '@/App.vue'

// element-plus 按需引入
import { useElementPlus } from "@/plugins/element-plus"; // element-plus

import "@/style/index.scss"
// 导入字体图标
import "@/assets/iconfont/iconfont.js";

const app = createApp(App)

app.use(useElementPlus)
app.mount('#app')
```
此时，以`iconfont`官网提供的`symbol`引用方法，就能将图标正常显示出来了，修改`src/App.vue`文件，在`icon-list`中新增一个图标引用：
```
<span class="item-icon">
  <svg class="icon" aria-hidden="true">
      <use xlink:href="#icon-lock"></use>
  </svg>
</span>
```
这里的`#icon-lock`即为具体的图标名，项目运行能正常显示图标，则说明配置生效了。

## 封装图标组件
以上直接使用的方式在项目中来说，会非常麻烦，且`element-plus`因为直接以图标名作为组件名，无法直接进行动态赋值调用，所以需要将其统一封装到一个组件中，全局使用，且能进行动态赋值，首先需要安装一个插件：
```
yarn add vite-svg-loader -D
```
该插件主要可将`svg`文件作为`vue`组件进行加载，这样就解决了`element-plus`无法动态赋值问题，安装后修改`vite.config.ts`文件：
```
# 文件头部新增：
import svgLoader from "vite-svg-loader"
# plugins中新增：
svgLoader(),
```
在`src/components`文件夹下新建`SvgIcon`文件夹，其内新建`index.vue`文件：
```
<script lang="ts">
import { defineComponent, computed } from 'vue'

interface Props {
	iconClass: string
	className: string
}

export default defineComponent({
	name: 'SvgIcon',
	props: {
		iconClass: {
			type: String,
			required: true
		},
		className: {
			type: String,
			default: () => ''
		}
	},
	setup(props: Props) {
		const iconName = computed((): string => `${props.iconClass}`)
		const svgClass = computed((): string => {
			if (props.className) {
				return 'svg-icon ' + props.className
			} else {
				return 'svg-icon'
			}
		})

		return {
			iconName,
			svgClass
		}
	}
})
</script>

<template>
  <el-icon>
    <svg v-if="iconName.indexOf('icon-')!=-1" :class="svgClass" aria-hidden="true">
      <use :xlink:href="'#'+iconName" />
    </svg>
    <component v-else :is="iconName" :class="svgClass"></component>
  </el-icon>
</template>

<style scoped>
.svg-icon {
	width: 1em;
	height: 1em;
	vertical-align: -0.15em;
	fill: currentColor;
	overflow: hidden;
}
</style>
```
该组件接收两个参数`icon-class`和`class-name`，前者为图标名，后者是为图标添加的类名，这里通过判断图标名是否包含`icon-`来判断图标是`iconfont`，还是来自`element-plus`，因为以`symbol`形式调用的`iconfont`图标，默认图标名都是以`icon-`开头的，而`element-plus`图标名则没有前缀，这里可以通过一点区别，去判断图标来源，然后用其特有的方式去调用。

## 注册全局图标组件
接下来在`src/main.ts`中，将以上封装的图标组件在全局进行注册：
```
import SvgIcon from '@/components/SvgIcon/index.vue';


app.component('svg-icon', SvgIcon); // 全局注册svg图标组件
```
此时，就是可以在项目的任何地方用统一的形式调用两种来源的图标了，修改`src/App.vue`文件，在`icon-list`新增新的调用图标相关的代码：
```
<span class="item-icon">
  <svg-icon icon-class="edit"></svg-icon>
</span>
<span class="item-icon">
  <svg-icon icon-class="icon-lock"></svg-icon>
</span>
```
运行项目，图标能正常显示
## 自定义svg图标文件封装
以上通过`element-plus`的图标，以及`iconfont`中收集图标都封装好了，有时候可能还会需要直接不使用`svg`图标库，而是直接将`svg`文件本身放在项目中进行引用，这中形式也是较为常见的，所以这里也将这种形式的`svg`图标引用，封装到上述组件中，这里需要先在`assets/svg`文件夹下存放一些`svg`文件以供使用，然后在`src/plugins`文件夹下新建`customSvgIcon`文件夹，并在其内新建`index.ts`文件，用来全局注册`svg`文件：
```
import { App } from "vue";
// 引入自定义图标
import close from "@/assets/svg/close.svg";
import closeAll from "@/assets/svg/close-all.svg";
import closeLeft from "@/assets/svg/close-left.svg";
import closeOther from "@/assets/svg/close-other.svg";
import closeRight from "@/assets/svg/close-right.svg";
import exitScreen from "@/assets/svg/exit-screen.svg";
import fullScreen from "@/assets/svg/full-screen.svg";
import globalization from "@/assets/svg/globalization.svg";
import refresh from "@/assets/svg/refresh.svg";
import translate from "@/assets/svg/translate.svg";
// 声明图标组件名
export const iconComponents = [
  close,
  closeAll,
  closeLeft,
  closeOther,
  closeRight,
  exitScreen,
  fullScreen,
  globalization,
  refresh,
  translate
];
const iconName = [
  "close",
  "closeAll",
  "closeLeft",
  "closeOther",
  "closeRight",
  "exitScreen",
  "fullScreen",
  "globalization",
  "refresh",
  "translate",
]

export function useCustomSvgIcon(app: App) {
  // 注册图标
  iconComponents.forEach((component: any,index) => {
    app.component(iconName[index], component);
  });
}
```
然后在`src/main.ts`中引入，并挂载到项目中即可：
```
import { useCustomSvgIcon } from "@/plugins/customSvgIcon";


app.use(useCustomSvgIcon)
```
到此，`svg`图标组件就封装好了，运行项目，修改`src/App.vue`文件，在`icon-list`新增新的调用图标相关的代码：
```
<span class="item-icon">
  <svg-icon icon-class="closeRight"></svg-icon>
</span>
```
运行项目，图标依然能正常显示。