﻿module.exports = {
  '*.{js,jsx,ts,tsx}': ['eslint --fix', 'prettier --write'],
  '{!(package)*.json,.!(browserslist)*rc}': ['prettier --write--parser json'],
  'package.json': ['prettier --write'],
  '*.vue': ['eslint --fix', 'prettier --write', 'stylelint --fix --allow-empty-input'],
  '*.{vue,css,scss,postcss,less}': ['prettier --write', 'stylelint --fix --allow-empty-input'],
  '*.md': ['prettier --write']
}
